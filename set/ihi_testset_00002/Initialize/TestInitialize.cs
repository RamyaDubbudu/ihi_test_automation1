﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using ihi_testlib_modbus;
using NUnit.Framework.Interfaces;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ihi_testset_00002
{
    [SetUpFixture]
    public abstract class TestInitialize
    {
       
        #region Variables 
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);
        public static ExtentReports _extent = new ExtentReports();
        protected ExtentTest _test;
        public string videoDir = TestContext.CurrentContext.TestDirectory + "\\VideoRecordings\\";
        #endregion


        public static string GetPjtParent()
        {
            var filePath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory().ToString()).ToString()).ToString();
            return filePath;
        }


        public static void CleanIIS()
        {
            var Processes = Process.GetProcesses().Where(pr => pr.ProcessName == "iisexpress");
            foreach (var process in Processes)
            {
                process.Kill();
                Processes = Process.GetProcesses().Where(pr => pr.ProcessName == "cmd");
                foreach (var proc in Processes)
                {
                    proc.Kill();

                }
            }
        }

        [OneTimeSetUp]
        protected void Setup()
        {
            CleanIIS();
            string PjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
            var reportDir = PjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString();
            Directory.CreateDirectory(reportDir);
            var fileName = GetType().Namespace + ".html";
            var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
            htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
            htmlReporter.AppendExisting = true;
            _extent.AttachReporter(htmlReporter);
            //Publish Site Data from the DC Sim set 
            Process p = new Process();
            p.StartInfo.FileName = ConfigurationManager.AppSettings["CMDPath"].ToString();
            p.StartInfo.WorkingDirectory = PjtParent + ConfigurationManager.AppSettings["IISExpress"].ToString();
            p.StartInfo.Arguments = @"/K iisexpress /path:" + PjtParent + ConfigurationManager.AppSettings["dcsimPjtPath"].ToString() + " /port:" + ConfigurationManager.AppSettings["Port"].ToString();
            p.Start();
            //1.Open SSH connection to ISO Slave to execute application, 2.Open Modbus Master socket to connect to ISO Slave, 3.Open SSH connection to DC Sim to execute application
            //4.DC Sim reads configuration file, 5.Open publisher socket from DC Sim to ZMQ, 6.ISO Slave connects to subscribe ZMQ socket, 7.Open rep socket from DC Sim to ZMQ
            //8.ISO Slave connects to req socket, 9.Publish default UI_SITE_INFO data from DC Sim to ZMQ, 10.ZMQ publishes data for ISO Slave subscriber
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            SSH.StopDecs(sshInfo);
            SSHAndStartDECSDirectory decsInfo = new SSHAndStartDECSDirectory(sshInfo, GlobalVariables.DECSDirectory);
            var startDECSSuccess = SSH.StartDecs(SSH.DECSLaunchType.Normal, decsInfo, 10000, "");
            System.Threading.Thread.Sleep(GlobalVariables.TimingConstants.DECSLaunch);
            SSH.OpenWrite(ssh, "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_isomb");
            System.Threading.Thread.Sleep(1000);
            SSH.ExecCommand(ssh, "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_isomb -D " + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + " -l 0 &");
            System.Threading.Thread.Sleep(1000);
            ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetTable");

        }
        [OneTimeTearDown]
        protected void TearDown()
        {
            _extent.Flush();
            //stop IIS EXPRESS 
            CleanIIS();
        }


        [SetUp]
            public void BeforeTest()
            {

                _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
                _test.AssignCategory(this.GetType().ToString());
                _test.AssignAuthor("Ramya Dubbudu");
            }

            [TearDown]
            public void AfterTest()
            {
                var status = TestContext.CurrentContext.Result.Outcome.Status;
                var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                        ? ""
                        : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
                Status logstatus;

                switch (status)
                {
                    case TestStatus.Failed:
                        logstatus = Status.Fail;
                        break;
                    case TestStatus.Inconclusive:
                        logstatus = Status.Warning;
                        break;
                    case TestStatus.Skipped:
                        logstatus = Status.Skip;
                        break;
                    default:
                    logstatus = Status.Pass;                   
                    break;
                }

                _test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
                _extent.Flush();
            }
    }
}