﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testset_00002
{
    class Program
    {
        static void Main(string[] args)
        {
            string projectName = "ihi_testset_00002";
            var filePath = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + "\\set\\" + projectName + "\\" + projectName + ".sln";
            var slnLines = File.ReadAllLines(filePath);
            var projGuid = new List<string>();


            for (var i = 0; i < slnLines.Length; i++)
            {
                if (slnLines[i].StartsWith(@"Project(""{"))
                //while (!slnLines[i + 1].Contains("EndProjectSection"))
                {
                    var temp = (slnLines[i].Split(new[] { ',' }, 3, StringSplitOptions.RemoveEmptyEntries));
                    if (temp.Count() > 2)
                    {
                        if (temp.ElementAt(1).Contains("ihi_test_"))
                        {
                            var pjt = temp.ElementAt(1).Replace(" ", "").Replace("\"", "");
                            pjt = pjt.Replace("..\\..\\", ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + "\\");
                            projGuid.Add(pjt);
                        }
                        i++;
                    }
                }
            }
            string S3BucketUploadDir = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + projectName;
            string keyName = "qa/" + System.Environment.UserName + "/" + ConfigurationManager.AppSettings["Component"].ToString() + "/" + DateTime.Today.ToString("yyyyMMdd") + "/" + projectName;
            Directory.CreateDirectory(S3BucketUploadDir);
            using (var file = new StreamWriter(ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + "\\library\\dll\\" + projectName + ".txt"))
            {
                string nunitConsolePath = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + "\\packages\\NUnit.ConsoleRunner.3.9.0\\tools\\nunit3-console.exe ";
                string resultsPath = " --work:" + S3BucketUploadDir + " --result:" + projectName + ".xml";
                string txt = string.Join(" ", projGuid.ToArray());
                file.WriteLine("cd c:\\ && " + nunitConsolePath + txt + resultsPath);

            }
           

        }
    }
}
