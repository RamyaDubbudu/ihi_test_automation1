using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModbus;

namespace ihi_testlib_modbus
{
    public class ModbusServer
    {

        public static void ClearAlarms(int count, List<int> holdingRegister, List<short> value)
        {
            EasyModbus.ModbusServer modbusServer = new EasyModbus.ModbusServer();
            for (int i = 0; i < count; i++)
            {
                int holdingre = holdingRegister[i];
                int val = value[i];
                modbusServer.holdingRegisters[holdingRegister[i]] = value[i];

            }
            modbusServer.UnitIdentifier = 255;
            modbusServer.Port = 502;
            modbusServer.Listen();

            modbusServer.HoldingRegistersChanged += new EasyModbus.ModbusServer.HoldingRegistersChangedHandler(holdingRegistersChanged);
            System.Threading.Thread.Sleep(2000);
            modbusServer.StopListening();
        }

        public static void SetAlarm(int count, List<int> holdingRegister, List<short> value, int setHoldingRegister, short setValue)
        {
            EasyModbus.ModbusServer modbusServer = new EasyModbus.ModbusServer();
            for (int i = 0; i < count; i++)
            {
                int holdingre = holdingRegister[i];
                int val = value[i];
                modbusServer.holdingRegisters[holdingRegister[i]] = value[i];
            }
            modbusServer.holdingRegisters[setHoldingRegister] = setValue;
            modbusServer.UnitIdentifier = 255;
            modbusServer.Port = 502;
            modbusServer.Listen();
            modbusServer.HoldingRegistersChanged += new EasyModbus.ModbusServer.HoldingRegistersChangedHandler(holdingRegistersChanged);
            System.Threading.Thread.Sleep(2000);
            modbusServer.StopListening();
        }



        public static void holdingRegistersChanged(int startingAddress, int quantity)
        {
            Console.WriteLine(startingAddress);
            Console.WriteLine(quantity);
        }


    }
}
