﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_modbus
{
   public class GlobalVariables
    {
        public static string SSHhostname = ConfigurationManager.AppSettings["ESPilotIPAddress"].ToString();
        public static string SSHusername = ConfigurationManager.AppSettings["Username"].ToString();
        public static string SSHpassword = ConfigurationManager.AppSettings["Password"].ToString();
        public static string LocalDCSimIPAddress = GetLocalIPAddress();
        public static string DCDatabaseLocation = @"Data Source=\\UBUNTU\decs\decs_dc.db;Version=3;";
        //DECS File Location on Ubuntu
        public static string DECSDirectory = "/usr/local/ihi-decs/bin/";
        public static string DECSDatabaseDirectory = "/var/run/ihi-decs/";
        public static string DECSDataDirectory = "/usr/local/ihi-decs/data/";
        public static class DECSLoginCredientials
        {
            public static string UserName = ConfigurationManager.AppSettings["Username"].ToString();
            public static string Password = ConfigurationManager.AppSettings["Password"].ToString();
        }
        public static class DECS_DCArguements
        {
            public static string Debug = "-d ";
            public static string Test = "-t ";
        }

        public static class TimingConstants
        {
            public static int DECSLaunch = 10000;
            public static int EnableWait = 5000;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

    }
}
