﻿using ihi_simulator_dc.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_modbus
{
   public static class ConsumeDCSimRest
    {

        public class IHI_DEVICE_ACTION
        {
            public string register { get; set; }
            public string value { get; set; }
        }

        public class IHI_REGISTER_ACTION
        {
            public string type { get; set; }
            public string label { get; set; }
            public string address { get; set; }
            public string value { get; set; }
        }

        public static string GetRestResponseByName(string controllername, string methodname) //GetESSSeq  
        {
            using (var client = new WebClient()) //WebClient  
            {
                client.Headers.Add("Content-Type:application/json"); //Content-Type  
                client.Headers.Add("Accept:application/json");
                string restAddress = string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["Port"].ToString() + "/DCSim/{0}/{1}", controllername, methodname);
                var result = client.DownloadString(restAddress); // RemoteInfo/GetISOSeq"); //URI  
                Console.WriteLine(Environment.NewLine + result);
                return result;
            }
        }

        #region Inverter Simulator Rest API Methods
        public static string InverterRestResponseByName(string controllername, string methodname) //GetESSSeq  
        {
            using (var client = new WebClient()) //WebClient  
            {
                client.Headers.Add("Content-Type:application/json"); //Content-Type  
                client.Headers.Add("Accept:application/json");
                string restAddress = string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["InverterPort"].ToString() + "/{0}/{1}", controllername, methodname);
                var result = client.DownloadString(restAddress); // RemoteInfo/GetISOSeq"); //URI  
                Console.WriteLine(Environment.NewLine + result);
                return result;
            }
        }

        public static string InverterRestResponse() //GetESSSeq  
        {
            using (var client = new WebClient()) //WebClient  
            {
                client.Headers.Add("Content-Type:application/json"); //Content-Type  
                client.Headers.Add("Accept:application/json");
                string restAddress = string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["InverterPort"].ToString() );
                var result = client.DownloadString(restAddress); // RemoteInfo/GetISOSeq"); //URI  
                Console.WriteLine(Environment.NewLine + result);
                return result;
            }
        }
        public static bool UpdateInverterDeviceType (string register, string deviceValue)
        {
            bool IsSuccess = true;
            using (var client = new HttpClient())
            {
                IHI_DEVICE_ACTION action = new IHI_DEVICE_ACTION { register = register, value = deviceValue };
                client.BaseAddress = new Uri(string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["InverterPort"].ToString()));
                var response = client.PutAsJsonAsync("/ihi/Device/Inverter", action).Result;
                if (response.IsSuccessStatusCode)
                {
                    return IsSuccess;                
                }
                else
                    return false;                   
            }
        }

        public static bool UpdateInverterAlarms(string value, string register = "1121", string label = "fault", string type = "holding")
        {
            bool IsSuccess = true;
            using (var client = new HttpClient())
            {
                IHI_REGISTER_ACTION action = new IHI_REGISTER_ACTION { type = type, label = label, address = register, value = value };
                client.BaseAddress = new Uri(string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["InverterPort"].ToString()));
                var response = client.PutAsJsonAsync("/ihi/registers", action).Result;
                if (response.IsSuccessStatusCode)
                {
                    return IsSuccess;
                }
                else
                    return false;
            }
        }

        public static bool UpdateInverterWarnings(string value, string register = "1122", string label = "warning", string type = "holding")
        {
            bool IsSuccess = true;
            using (var client = new HttpClient())
            {
                IHI_REGISTER_ACTION action = new IHI_REGISTER_ACTION { type = type, label = label, address = register, value = value };
                client.BaseAddress = new Uri(string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["InverterPort"].ToString()));
                var response = client.PutAsJsonAsync("/ihi/registers", action).Result;
                if (response.IsSuccessStatusCode)
                {
                    return IsSuccess;
                }
                else
                    return false;
            }
        }


        #endregion
        public static string BatteryRestResponseByName(string controllername, string methodname) //GetESSSeq  
        {
            using (var client = new WebClient()) //WebClient  
            {
                client.Headers.Add("Content-Type:application/json"); //Content-Type  
                client.Headers.Add("Accept:application/json");
                string restAddress = string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["BatteryPort"].ToString() + "/{0}/{1}", controllername, methodname);
                var result = client.DownloadString(restAddress); // RemoteInfo/GetISOSeq"); //URI  
                Console.WriteLine(Environment.NewLine + result);
                return result;
            }
        }

        public static string BatteryRestResponse() //GetESSSeq  
        {
            using (var client = new WebClient()) //WebClient  
            {
                client.Headers.Add("Content-Type:application/json"); //Content-Type  
                client.Headers.Add("Accept:application/json");
                string restAddress = string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["BatteryPort"].ToString());
                var result = client.DownloadString(restAddress); // RemoteInfo/GetISOSeq"); //URI  
                Console.WriteLine(Environment.NewLine + result);
                return result;
            }
        }
        public static bool UpdateBatteryDeviceType(string register, string deviceValue)
        {
            bool IsSuccess = true;
            using (var client = new HttpClient())
            {
                IHI_DEVICE_ACTION action = new IHI_DEVICE_ACTION { register = register, value = deviceValue };
                client.BaseAddress = new Uri(string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["BatteryPort"].ToString()));
                var response = client.PutAsJsonAsync("/ihi/Device/Battery", action).Result;
                if (response.IsSuccessStatusCode)
                {
                    return IsSuccess;
                }
                else
                    return false;
            }
        }

        public static bool UpdateBatteryAlarms(string value, string register, string label, string type = "holding")
        {
            bool IsSuccess = true;
            using (var client = new HttpClient())
            {
                IHI_REGISTER_ACTION action = new IHI_REGISTER_ACTION { type = type, label = label, address = register, value = value };
                client.BaseAddress = new Uri(string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["BatteryPort"].ToString()));
                var response = client.PutAsJsonAsync("/ihi/registers", action).Result;
                if (response.IsSuccessStatusCode)
                {
                    return IsSuccess;
                }
                else
                    return false;
            }
        }

        public static bool UpdateBatteryRegisters(string register, string value, string label, string type = "holding")
        {
            bool IsSuccess = true;
            using (var client = new HttpClient())
            {
                IHI_REGISTER_ACTION action = new IHI_REGISTER_ACTION { type = type, label = label, address = register, value = value };
                client.BaseAddress = new Uri(string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["BatteryPort"].ToString()));
                var response = client.PutAsJsonAsync("/ihi/registers", action).Result;
                if (response.IsSuccessStatusCode)
                {
                    return IsSuccess;
                }
                else
                    return false;
            }
        }
        #region Battery Simulator Rest API Methods


        #endregion
        public static bool UpdateSiteDataa() //Update Event  
        {
            bool IsSuccess = true;
            using (var client = new HttpClient())
            {
                Site s = new Site { rmon_site_id = 19, site_utime = 0, url = "http=//ihi-energystorage.com/", name = "ARLANXEOs", description = "Arlanxeo Warehouse", lat = 42.9384423f, lon = -82.4118958f, address = "610 Churchhill Road", city = "Sarnia", state_province = "Ontario", post_code = "N7T 7M2", country = "US", mc_state = 0, mc_substate = 0, lmp = 0, auto_restart = 0, last_mode = 1, mode_actual = 1410065407, profile_no = 0, mode_demand = 1, available_mode_flags = -43967296, startup_timer = -1, p_demand = 1410065407, p_actual = 1410065407, q_demand = 1410065407, q_actual = 1410065407, p_max = 1410065407, p_min = 1410065407, q_max = 1410065407, q_min = 1410065407, soc = 1001, soh = 1001, kwh_nameplate = 0, kwh_max = 1410065407, kwh_available = 1410065407, kw_nameplate = 1410065407, kvar_nameplate = 1410065407, ref_freq = 0, ref_voltage = 0, ref_v1 = 0, ref_v2 = 0, ref_v3 = 0, ref_p = 0, ref_q = 0, ref_kva = 0, low_soc_warning = 7, low_soc_cutoff = 4, high_soc_warning = 87, high_soc_cutoff = 91, target_soc = 75, target_freq = 0, target_voltage = 0, target_p = 0, target_p_max = 0, target_p_min = -100, target_q = 0, target_q_max = 10, target_q_min = -10, zones_available = 0, zones_enabled = 0, zones_online = 1410065407, zones_online_flags = 1410065407, zones_total = 1410065407, inverters_available = 0, inverters_enabled = 0, inverters_online = 0, inverters_total = 6, racks_available = 0, racks_enabled = 0, racks_online = 0, racks_total = 102, cooling_available = 0, cooling_enabled = 0, cooling_total = 0, cooling_online = 0, low_ambient_threshold = 58, high_ambient_threshold = 91, ambient_temp = 72, rh = 40, io_available = 0, io_total = 0, meters_available = 0, meters_total = 0, epo_active = 0, rpo_active = 0, alarms_critical = 0, alarms_major = 0, alarms_minor = 0, alarms_informational = 0, alarms_total = 1410065407, notifications_total = 0, server_build = 0, server_latest_build = 0, max_zone_temp_f = 0, max_zone_temp_zone = 0, min_zone_temp_f = 0, min_zone_temp_zone = 0, max_rack_temp_f = 0, max_rack_temp_rack = 0, min_rack_temp_f = 0, min_rack_temp_rack = 0, kwh_throughput = 0, total_runtime = 0, current_runtime = 0, ui_flags = 3, ui_build = 0, ui_latest_build = 0, instance_id = 0, ups_status = 1, ups_load_pct = 0, ups_soc = 0, ups_remaining = 0, help_url = "DECS_User_Manual.pdf", ups_url = null, operator_url = "http=//www.caiso.com/pages/pricemaps.aspx", logo_url = "espilot_logo.png", comms_type = 1, voltage = 10000000000, current = 10000000000 };
                client.BaseAddress = new Uri(string.Format(ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + ConfigurationManager.AppSettings["Port"].ToString()));
                var response = client.PutAsJsonAsync("/DCSim/SiteInfo/UpdateSiteData", s).Result;
                if (response.IsSuccessStatusCode)
                {
                    return IsSuccess;
                }
                else
                    return false;
            }
        }


        #region Domain Controller Methods
        public static void UpdateSiteData(string column, string value)
        {
            SiteInfoController controller = new SiteInfoController();
            string[] values = { column, value };
            controller.UpdateSiteByColumn(values);
            System.Threading.Thread.Sleep(4000);
        }

        public static void UpdateRemoteData(string column, string value)
        {
            RemoteInfoController rcontroller = new RemoteInfoController();
            string[] values = { column, value };
            rcontroller.UpdateRemoteByColumn(values);
            System.Threading.Thread.Sleep(4000);
        }
        #endregion


        public static void Connection2_BatterySimulator()
        {
            string batteryConnection = "NOT CONNECTED";
            int n = 0;
            do
            {
                batteryConnection = ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
                System.Threading.Thread.Sleep(1000);
                n++;
            } while ( batteryConnection.Contains("NOT CONNECTED") || n < 10);
            if (batteryConnection.Contains("NOT CONNECTED"))
            {
                throw new Exception("Battery simulator connection cannot be established with ES/Pilot");
            }
           // else 
            //return batteryConnection;
        }

        public static void Connection2_InverterSimulator()
        {
            string InverterConnection = "NOT CONNECTED";
            int n = 0;
            do
            {
                InverterConnection = ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
                System.Threading.Thread.Sleep(1000);
                n++;
            } while (InverterConnection.Contains("NOT CONNECTED") || n < 10);
            if (InverterConnection.Contains("NOT CONNECTED"))
            {
                throw new Exception("Inverter simulator connection cannot be established with ES/Pilot");
            }
            //else
            //    return InverterConnection;
        }
    }
}
