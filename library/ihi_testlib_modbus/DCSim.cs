﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ihi_testlib_modbus
{
    public class Site
    {
        public int rmon_site_id { get; set; }
        public int site_utime { get; set; }
        public String url { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
        public String address { get; set; }
        public String city { get; set; }
        public String state_province { get; set; }
        public String post_code { get; set; }
        public String country { get; set; }
        public int mc_state { get; set; }
        public int mc_substate { get; set; }
        public float lmp { get; set; }
        public int auto_restart { get; set; }
        public int last_mode { get; set; }
        public int mode_actual { get; set; }
        public int profile_no { get; set; }
        public int mode_demand { get; set; }
        public int available_mode_flags { get; set; }
        public int startup_timer { get; set; }
        public int p_demand { get; set; }
        public int p_actual { get; set; }
        public int q_demand { get; set; }
        public int q_actual { get; set; }
        public int p_max { get; set; }
        public int p_min { get; set; }
        public int q_max { get; set; }
        public int q_min { get; set; }
        public float soc { get; set; }
        public int soh { get; set; }
        public int kwh_nameplate { get; set; }
        public int kwh_max { get; set; }
        public int kwh_available { get; set; }
        public int kw_nameplate { get; set; }
        public int kvar_nameplate { get; set; }
        public float ref_freq { get; set; }
        public float ref_voltage { get; set; }
        public float ref_v1 { get; set; }
        public float ref_v2 { get; set; }
        public float ref_v3 { get; set; }
        public int ref_p { get; set; }
        public int ref_q { get; set; }
        public int ref_kva { get; set; }
        public float low_soc_warning { get; set; }
        public float low_soc_cutoff { get; set; }
        public float high_soc_warning { get; set; }
        public float high_soc_cutoff { get; set; }
        public float target_soc { get; set; }
        public float target_freq { get; set; }
        public float target_voltage { get; set; }
        public int target_p { get; set; }
        public int target_p_max { get; set; }
        public int target_p_min { get; set; }
        public int target_q { get; set; }
        public int target_q_max { get; set; }
        public int target_q_min { get; set; }
        public int zones_available { get; set; }
        public int zones_enabled { get; set; }
        public int zones_online { get; set; }
        public int zones_online_flags { get; set; }
        public int zones_total { get; set; }
        public int inverters_available { get; set; }
        public int inverters_enabled { get; set; }
        public int inverters_online { get; set; }
        public int inverters_total { get; set; }
        public int racks_available { get; set; }
        public int racks_enabled { get; set; }
        public int racks_online { get; set; }
        public int racks_total { get; set; }
        public int cooling_available { get; set; }
        public int cooling_enabled { get; set; }
        public int cooling_total { get; set; }
        public int cooling_online { get; set; }
        public int low_ambient_threshold { get; set; }
        public int high_ambient_threshold { get; set; }
        public int ambient_temp { get; set; }
        public int rh { get; set; }
        public int io_available { get; set; }
        public int io_total { get; set; }
        public int meters_available { get; set; }
        public int meters_total { get; set; }
        public int epo_active { get; set; }
        public int rpo_active { get; set; }
        public int alarms_critical { get; set; }
        public int alarms_major { get; set; }
        public int alarms_minor { get; set; }
        public int alarms_informational { get; set; }
        public int alarms_total { get; set; }
        public int notifications_total { get; set; }
        public int server_build { get; set; }
        public int server_latest_build { get; set; }
        public float max_zone_temp_f { get; set; }
        public int max_zone_temp_zone { get; set; }
        public float min_zone_temp_f { get; set; }
        public int min_zone_temp_zone { get; set; }
        public float max_rack_temp_f { get; set; }
        public int max_rack_temp_rack { get; set; }
        public float min_rack_temp_f { get; set; }
        public int min_rack_temp_rack { get; set; }
        public float kwh_throughput { get; set; }
        public float total_runtime { get; set; }
        public float current_runtime { get; set; }
        public int ui_flags { get; set; }
        public int ui_build { get; set; }
        public int ui_latest_build { get; set; }
        public int instance_id { get; set; }
        public int ups_status { get; set; }
        public float ups_load_pct { get; set; }
        public float ups_soc { get; set; }
        public float ups_remaining { get; set; }
        public String help_url { get; set; }
        public String ups_url { get; set; }
        public String operator_url { get; set; }
        public String logo_url { get; set; }
        public int comms_type { get; set; }
        public float voltage { get; set; }
        public float current { get; set; }

    }


    public class batteries
    {
        public int battery_type { get; set; }
        public string manufacturer { get; set; }
        public string description { get; set; }
        public float c_rate { get; set; }
        public float max_current { get; set; }
        public float min_voltage { get; set; }
        public float max_voltage { get; set; }
        public float kwh_nameplate { get; set; }
        public float low_t_warning { get; set; }
        public float high_t_warning { get; set; }
        public float low_t_cutoff { get; set; }
        public float high_t_cutoff { get; set; }
        public int module_ct { get; set; }
        public int provides_limits { get; set; }
        public int supports_rejuvenation { get; set; }
        public string connector_name { get; set; }
        public string image_filename { get; set; }
    }


    public partial class Remote
    {
        public long remote_id { get; set; }
        public long ess_sequence { get; set; }
        public long comms_timeout { get; set; }
        public long ignore_remote { get; set; }
        public long iso_sequence { get; set; }
        public long p_demand { get; set; }
        public long q_demand { get; set; }
        public long mode_demand { get; set; }
        public double soc_target { get; set; }
        public long soc_power_limit { get; set; }
        public long target_p { get; set; }
        public long target_q { get; set; }
        public long ramp_up_p { get; set; }
        public long ramp_down_p { get; set; }
        public long ramp_up_q { get; set; }
        public long ramp_down_q { get; set; }
        public long enabled { get; set; }
    }
}