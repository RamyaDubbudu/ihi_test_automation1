﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modbus.Device;
using System.Net.Sockets;


namespace ihi_testlib_modbus
{
   public class ModbusClient
    {

            public static ModbusIpMaster m_ModbusIPMaster;
            public static TcpClient m_TCP_Client;


            public static bool Connected2ModbusServer(string ipaddress, int portNumber = 1502)
            {
                m_TCP_Client = new TcpClient(ipaddress, portNumber);
                m_ModbusIPMaster = ModbusIpMaster.CreateIp(m_TCP_Client);
                if (m_TCP_Client != null && m_TCP_Client.Connected)
                {
                    return true;
                }
                return false;
            }


            public static void WriteToSingleRegister(string ipaddress, ushort registerAddress, ushort value)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                    try
                    {
                        m_ModbusIPMaster.WriteSingleRegister(registerAddress, value);
                        System.Threading.Thread.Sleep(2000);
                    }
                    catch (Modbus.SlaveException exceptionModbus)
                    {
                       Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                    }
                }
            }

            public static void WriteToMultipleRegister(string ipaddress, ushort startRegisterAddress, ushort[] data2Write)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                    try
                    {
                        m_ModbusIPMaster.WriteMultipleRegisters(startRegisterAddress, data2Write);
                        System.Threading.Thread.Sleep(2000);
                    }
                    catch (Modbus.SlaveException exceptionModbus)
                    {
                        Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                    }
                }

            }
            public static ushort[] ReadInputRegister(string ipaddress, ushort startAddress, ushort noOfPoints2Read)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                    try
                    {
                        ushort[] value = m_ModbusIPMaster.ReadInputRegisters(startAddress, noOfPoints2Read);
                        System.Threading.Thread.Sleep(2000);
                        return value;
                    }
                    catch (Modbus.SlaveException exceptionModbus)
                    {
                        Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                    }
                }
                    return null;
            }

            public static void ReadWriteToMultipleRegister(string ipaddress, ushort startRegisterAddress, ushort noOfPoints2Read, ushort startWriteAddress, ushort[] data2Write)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                try
                {
                    m_ModbusIPMaster.ReadWriteMultipleRegisters(startRegisterAddress, noOfPoints2Read, startWriteAddress, data2Write);
                    System.Threading.Thread.Sleep(2000);
                }
                catch (Modbus.SlaveException exceptionModbus)
                {
                    Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                }
            }
        }

            public static void ReadInputs(string ipaddress, ushort startRegisterAddress, ushort noOfPoints)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                try
                {
                    m_ModbusIPMaster.ReadInputs(startRegisterAddress, noOfPoints);
                    System.Threading.Thread.Sleep(2000);
                }
                catch (Modbus.SlaveException exceptionModbus)
                {
                    Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                }
            }
        }

            public static ushort[] ReadHoldingRegisters(string ipaddress, ushort startRegisterAddress, ushort noOfPoints)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                try
                {
                    ushort[] value = m_ModbusIPMaster.ReadHoldingRegisters(startRegisterAddress, noOfPoints);
                    System.Threading.Thread.Sleep(2000);
                    return value;
                }
                catch (Modbus.SlaveException exceptionModbus)
                {
                    Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                }
            }
            return null;

            }

            public static void ReadCoils(string ipaddress, ushort startRegisterAddress, ushort noOfPoints)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                try
                {
                    m_ModbusIPMaster.ReadCoils(startRegisterAddress, noOfPoints);
                    System.Threading.Thread.Sleep(2000);
                }
                catch (Modbus.SlaveException exceptionModbus)
                {
                    Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                    }
                }
            }

            public static void WriteSingleCoil(string ipaddress, ushort coilAddress, bool value)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                try
                {
                    m_ModbusIPMaster.WriteSingleCoil(coilAddress, value);
                    System.Threading.Thread.Sleep(2000);
                }
                catch (Modbus.SlaveException exceptionModbus)
                {
                    Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                }
            }
        }

            public static void WriteSingleCoil(string ipaddress, ushort startAddress, bool[] data)
            {
                if (Connected2ModbusServer(ipaddress))
                {
                try
                {
                    m_ModbusIPMaster.WriteMultipleCoils(startAddress, data);
                    System.Threading.Thread.Sleep(2000);
                }
                catch (Modbus.SlaveException exceptionModbus)
                {
                    Console.WriteLine("Modbus Exception " + exceptionModbus.Message);
                }
            }
        }



        }
    }
