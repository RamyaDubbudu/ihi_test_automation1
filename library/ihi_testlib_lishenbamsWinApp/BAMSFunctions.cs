﻿using AventStack.ExtentReports;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ihi_testlib_lishenbamsWinApp
{
    public class BAMSFunctions : BAMSSession
    {
        WindowsElement ThresholdViewerElement;
        public string CloseThresholdViewer = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Window[@Name=\"Threshold Viewer\"][@AutomationId=\"ManagRange\"]/TitleBar[@AutomationId=\"TitleBar\"]/Button[@Name=\"Close\"][@AutomationId=\"Close\"]";
        public string xpath_ClickTreeItemBAMSInterf = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Tree[@Name=\"BCMU Information\"][starts-with(@AutomationId,\"treeView\")]/TreeItem[@Name=\"System\"]/TreeItem[@Name=\"BAMS Interface\"]";
        public String xpath_LeftClickTreeItemSystem = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Tree[@Name=\"BCMU Information\"][starts-with(@AutomationId,\"treeView\")]/TreeItem[@Name=\"System\"]";
        public string xpath_textBoxCurMinTemparature = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Group[@AutomationId=\"groupBox_bms\"]/Edit[@AutomationId=\"textBoxCurMinT\"]";
        public string xpath_SystemSetMenu = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/MenuBar[@Name=\"menuStrip1\"][starts-with(@AutomationId,\"menuStrip\")]/MenuItem[@Name=\"System Set\"]";
        public string xpath_SystemWarningCloseButton = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Window[@Name=\"System Warning Set\"][@AutomationId=\"ParaSet\"]/TitleBar[@AutomationId=\"TitleBar\"]/Button[@Name=\"Close\"][@AutomationId=\"Close\"]";
        public string xpath_CellUTWarningTextBox = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Window[@Name=\"System Warning Set\"][@AutomationId=\"ParaSet\"]/Edit[@AutomationId=\"textBoxCellUTW\"]";
        public string xpath_CellUTProtectedTextBox = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Window[@Name=\"System Warning Set\"][@AutomationId=\"ParaSet\"]/Edit[@AutomationId=\"textBoxCellUTP\"]";
        public string xpath_TotalThresholdInButton = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Window[@Name=\"System Warning Set\"][@AutomationId=\"ParaSet\"]/Button[@Name=\"Total Threshold in\"][@AutomationId=\"button_TolWrite\"]";
        public string xpath_WarningTabPointer = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Window[@Name=\"System Warning Set\"][@AutomationId=\"ParaSet\"]/Edit[starts-with(@AutomationId,\"textBox20\")]";


        public void LoginBAMS(WindowsDriver<WindowsElement> bamsSession, ExtentTest _test)
        {
            //// Login to PowerLife
            Thread.Sleep(TimeSpan.FromSeconds(1));
            bamsSession.FindElementByName("Login Rights").Click();       
            if (bamsSession.FindElementByName("Administrator Login").Displayed)
            {
                bamsSession.FindElementByName("Administrator Login").Click();
                bamsSession.FindElementByName("Password:").SendKeys("201314");
                _test.Info("Login with Username and Password: ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(bamsSession)).Build());
                bamsSession.FindElementByName("Login").Click();
            }
        }

        public class ThresholdInfo
        {
            public string ThresholdName { get; set; }
            public string Warning { get; set; }
            public string Protection { get; set; }
        }

        public ThresholdInfo ThresholdViewerByName(WindowsDriver<WindowsElement> bamsSession,string thresholdName, ExtentTest _test)
        {
            ThresholdInfo threInfo = new ThresholdInfo();
            bamsSession.FindElementByName("System ").Click();
            bamsSession.FindElementByName("Threshold Viewer").Click();
            _test.Info("Clicked on Threshold Viewer: ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(bamsSession)).Build());
            ThresholdViewerElement = bamsSession.FindElementByName("DataGridView");
            if (ThresholdViewerElement.Displayed)
            {
                if (thresholdName == "Over total voltage")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 0").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 0").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 0").Text;
                }
                else if (thresholdName == "Under total voltage")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 1").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 1").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 1").Text;
                }
                else if (thresholdName == "Charge over current")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 2").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 2").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 2").Text;
                }
                else if (thresholdName == "Discharge over current")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 3").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 3").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 3").Text;
                }
                else if (thresholdName == "Over cell voltage")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 4").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 4").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 4").Text;
                }
                else if (thresholdName == "Under cell voltage")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 5").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 5").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 5").Text;
                }
                else if (thresholdName == "Over cell temperature")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 6").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 6").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 6").Text;
                }
                else if (thresholdName == "Under cell temperature")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 7").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 7").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 7").Text;
                }
                else if (thresholdName == "transient temperature arising")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 8").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 8").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 8").Text;
                }
                else if (thresholdName == "Temperature differences")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 9").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 9").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 9").Text;
                }
                else if (thresholdName == "Short current")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 10").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 10").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 10").Text;
                }
                else if (thresholdName == "Balance voltage")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 11").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 11").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 11").Text;
                }
                else if (thresholdName == "Balance voltage differences")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 13").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 13").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 13").Text;
                }
                else if (thresholdName == "Conversion balance mode")
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row 14").Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row 14").Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row 14").Text;
                }
            }
             bamsSession.FindElementByName("Threshold Viewer").FindElementByAccessibilityId("TitleBar").FindElementByName("Close").Click();
            //var winElem_CloseThresholdViewer = FindElementByAbsoluteXPath(bamsSession, CloseThresholdViewer);
            //if (winElem_CloseThresholdViewer != null)
            //{
            //    winElem_CloseThresholdViewer.Click();
            //    //Actions action = new Actions(bamsSession);
            //    //action.MoveToElement(winElem_CloseThresholdViewer, 759, 11).Click().Perform();

            //}
            //else
            //{
            //    Console.WriteLine($"Failed to find element using xpath: {CloseThresholdViewer}");

            //}
            return threInfo;
        }

        public List<ThresholdInfo> ThresholdViewer(WindowsDriver<WindowsElement> bamsSession, ExtentTest _test)
        {
            ThresholdInfo threInfo = new ThresholdInfo();
            List<ThresholdInfo> list = new List<ThresholdInfo>();
            bamsSession.FindElementByName("System ").Click();
            bamsSession.FindElementByName("Threshold Viewer").Click();
            _test.Info("Clicked on Threshold Viewer: ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(bamsSession)).Build());
            ThresholdViewerElement = bamsSession.FindElementByName("DataGridView");
            if (ThresholdViewerElement.Displayed)
            {
                for (int i = 0; i < 14; i++) // TODO: get number of rows in the table 
                {
                    threInfo.ThresholdName = bamsSession.FindElementByName("Threshold Name Row " + i).Text;
                    threInfo.Warning = bamsSession.FindElementByName("Warning Row " + i).Text;
                    threInfo.Protection = bamsSession.FindElementByName("Protection Row " + i).Text;
                    list.Add(threInfo);
                }
            }
            var winElem_CloseThresholdViewer = FindElementByAbsoluteXPath(bamsSession, CloseThresholdViewer);
            if (winElem_CloseThresholdViewer != null) 
            {
                winElem_CloseThresholdViewer.Click();
                //Actions action = new Actions(bamsSession);
                //action.MoveToElement(winElem_CloseThresholdViewer, 759, 11).Click().Perform();
            }
            else
            {
                Console.WriteLine($"Failed to find element using xpath: {CloseThresholdViewer}");
            }
            return list;
        }

        public string BAMSInterface(WindowsDriver<WindowsElement> bamsSession, ExtentTest _test)
        {
            var winElem_ClickTreeItemSystem = FindElementByAbsoluteXPath(bamsSession, xpath_LeftClickTreeItemSystem);
            if (winElem_ClickTreeItemSystem != null) // XY coordinates of Close button.
            {
                winElem_ClickTreeItemSystem.Click();
                Actions action = new Actions(bamsSession);
                action.MoveToElement(winElem_ClickTreeItemSystem).DoubleClick().Perform(); //Open System
            }
            else
            {
                Console.WriteLine($"Failed to find element using xpath: {xpath_LeftClickTreeItemSystem}");
            }
            var winElem_LeftClickTreeItemBAMSInterf_19_8 = FindElementByAbsoluteXPath(bamsSession, xpath_ClickTreeItemBAMSInterf);
            if (winElem_LeftClickTreeItemBAMSInterf_19_8 != null)
            {
                winElem_LeftClickTreeItemBAMSInterf_19_8.Click();
                _test.Info("Clicked on BAMS Interface to get the initial values: ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(bamsSession)).Build());
            }
            else
            {
                Console.WriteLine($"Failed to find element using xpath: {xpath_ClickTreeItemBAMSInterf}"); //click on BAMS Interface
                return "";
            }
            string curminTemp = FindElementByAbsoluteXPath(bamsSession, xpath_textBoxCurMinTemparature).Text;
            return curminTemp;
        }

        public void WarningSet(WindowsDriver<WindowsElement> bamsSession, string valuetoChange, ExtentTest _test)
        {
            bamsSession.FindElementByName("System Set").Click();
            Thread.Sleep(TimeSpan.FromSeconds(2));
            string xpath_LeftClickMenuSystemSetD_71_33 = "/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"PowerLife201\"][@AutomationId=\"Form_Main\"]/Menu[starts-with(@ClassName,\"WindowsForms10\")][@Name=\"System SetDropDown\"]";
            var winElem_LeftClickMenuSystemSetD_71_33 = FindElementByAbsoluteXPath(bamsSession, xpath_LeftClickMenuSystemSetD_71_33);
            if (winElem_LeftClickMenuSystemSetD_71_33 != null)
            {
                winElem_LeftClickMenuSystemSetD_71_33.Click();
                Actions action = new Actions(bamsSession);
                action.MoveToElement(winElem_LeftClickMenuSystemSetD_71_33, 71,33).Click().Perform();
            }
            _test.Info("Opened Warning Set screen to change the threshold values: ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(bamsSession)).Build());
            //  string initUTWarning = FindElementByAbsoluteXPath(bamsSession, xpath_CellUTWarningTextBox).Text;
            string initUTProtection = FindElementByAbsoluteXPath(bamsSession, xpath_CellUTProtectedTextBox).Text;
            FindElementByAbsoluteXPath(bamsSession, xpath_CellUTProtectedTextBox).SendKeys(valuetoChange);
            // bamsSession.FindElementByAbsoluteXPath(powerLifeSession, xpath_WarningTabPointer).Click();
            _test.Info("Warning Set screen to change the threshold values with: \"" + valuetoChange + "\"", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(bamsSession)).Build());
            FindElementByAbsoluteXPath(bamsSession, xpath_TotalThresholdInButton).Click();
            FindElementByAbsoluteXPath(bamsSession, xpath_SystemWarningCloseButton).Click();
        }

    }
}
