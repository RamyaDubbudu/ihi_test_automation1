﻿using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Configuration;
using System.Text;
using AventStack.ExtentReports;

namespace ihi_testlib_lishenbamsWinApp
{
    public class BAMSSession
    {
        private string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        private string PowerLifeExePath = ConfigurationManager.AppSettings["PowerLifeExePath"].ToString();
        public string ScreenCapturePath;

        // protected static WindowsDriver<WindowsElement> bamsSession;
        protected static WindowsElement editBox;

        public WindowsDriver<WindowsElement> LaunchLishenBAMSSession(WindowsDriver<WindowsElement> bamsSession)
        {
              // Launch a new instance of PowerLife application
               // System.Diagnostics.Process.Start(PowerLifeExePath);
                // Create a new session to launch PowerLife application
                DesiredCapabilities appCapabilities = new DesiredCapabilities();
                appCapabilities.SetCapability("app", PowerLifeExePath);
                bamsSession = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities, TimeSpan.FromMinutes(10));
                // Set implicit timeout to 1.5 seconds to make element search to retry every 500 ms for at most three times
                bamsSession.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1.5);
                return bamsSession;
        }

        public void CloseBAMSSession(WindowsDriver<WindowsElement> bamsSession)
        {
            // Close the application and delete the session
            if (bamsSession != null)
            {
                bamsSession.Close();
                bamsSession.Quit();
                bamsSession = null;
            }
        }

         public WindowsElement FindElementByAbsoluteXPath(WindowsDriver<WindowsElement> sessionName, string xPath, int nTryCount = 15)
        {
            WindowsElement uiTarget = null;

            while (nTryCount-- > 0)
            {
                try
                {
                    uiTarget = sessionName.FindElementByXPath(xPath);
                }
                catch
                {
                }

                if (uiTarget != null)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(2000);
                }
            }

            return uiTarget;
        }

        public String TakesScreenshot(WindowsDriver<WindowsElement> sessionName)
        {
            var testname = GetType().Namespace;
            ScreenCapturePath = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            System.IO.Directory.CreateDirectory(ScreenCapturePath);
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");
            TimeAndDate.Replace(" ", "_");
            string imageName = TimeAndDate.ToString();
            sessionName.GetScreenshot().SaveAsFile(ScreenCapturePath + "_" + imageName + "." + System.Drawing.Imaging.ImageFormat.Jpeg);
            return ScreenCapturePath + "_" + imageName + "." + "jpeg";
        }
    }
}