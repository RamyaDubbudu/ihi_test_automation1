﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ihi_testlib_selenium;
using System.Configuration;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace ihi_testlib_netgearportswitch
{
    public class Netgear_PortSwitch
    {
        #region Variables
        public static ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();

        #endregion

        #region PageObjects
        public class NetgearPgObj
        {
            public static string SwitchingTab { get { return "//table//td[position() = 3 and @class = 'navTopCell']"; } }
            public static string PortsTab { get { return "//div[@id = 'primaryNav']//tr[@id = 'subTubs']//td[2]"; } }
            public static string SelectPortAndDeviceForEdit { get { return "//table[@id = 'igmpTbl']//td[text() = '{0}' and position() = 2]/../td[text() = '{1}' and position() = 3]/../td/input"; } }
            public static string SwitchPort { get { return "//table[@id = 'igmpTbl']//tr[@id = 'editRow']//td[position() = 2 and text() = '{0}']/../td[position() = 5]/select"; } }
            public static string PortStatus { get { return "//table[@id = 'igmpTbl']//td[text() = '{0}' and position() = 2]/../td[text() = '{1}' and position() = 3]/../td[5 and text() = '{3}']"; } }
        }

        #endregion

       public enum SwitchType
        {
            Enable,
            Disable
        }
        #region

        public static void LoginNetgear()
        {
            webDriver.SetBrowser(Selenium.Browser.Chrome, ConfigurationManager.AppSettings["NetgearURL"].ToString());
            webDriver.WaitForElement(tagTXT:"input", nameTXT: "pwd", classTXT: "input", numberOfTimes:60);
            webDriver.AddTextToTextbox(textToAdd: "password", tagTXT: "input", nameTXT: "pwd", classTXT: "input");
            webDriver.ClickOnHTMLControl(tagTXT: "input", nameTXT: "login");
            webDriver.WaitForElement(tagTXT: "td", classTXT: "logoNetGear space50Percent topAlign", numberOfTimes: 120);
        }

        public static void SwitchPort(string port, string description, string networkConnection = "Enable")
        {
            webDriver.WaitForElement(tagTXT: "td", classTXT: "logoNetGear space50Percent topAlign", numberOfTimes: 120);
            webDriver.ClickOnHTMLControl(XPath: string.Format(NetgearPgObj.SwitchingTab));
            webDriver.ClickOnHTMLControl(XPath: string.Format(NetgearPgObj.PortsTab));
            webDriver.WaitForElement(idTXT: "portConfig",tagTXT: "form",  numberOfTimes: 100);
            webDriver.ClickOnHTMLControl(XPath: string.Format(NetgearPgObj.SelectPortAndDeviceForEdit, port, description));
            if(networkConnection == "Enable")
            new SelectElement(webDriver.browserDriver.FindElement(By.XPath(string.Format(NetgearPgObj.SwitchPort,port)))).SelectByText(SwitchType.Enable.ToString());
            else if(networkConnection == "Disable")
            new SelectElement(webDriver.browserDriver.FindElement(By.XPath(string.Format(NetgearPgObj.SwitchPort, port)))).SelectByText(SwitchType.Disable.ToString());
            webDriver.ClickOnHTMLControl(idTXT: "btn_Apply", tagTXT: "input", nameTXT: "btn_Apply");
            webDriver.Assertion_IsElementVisible(xPath: string.Format(NetgearPgObj.PortStatus, port, description, networkConnection));
        }
        #endregion

    }
}
