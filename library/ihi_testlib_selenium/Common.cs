﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.IO;
using System.Globalization;

namespace ihi_testlib_selenium
{
    public class Common
    {
        
       
        public static string AssemblyPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        static Dictionary<char, int> vinValues = new Dictionary<char, int>() 
        { 
            { '0', 0 }, { '1', 1 }, { '2', 2 }, { '3', 3 } ,{'4',4},{'5',5}
            ,{'6',6},{'7',7},{'8',8},{'9',9},{'A',1},{'B',2},{'C',3},{'D',4},{'E',5}
            ,{'F',6},{'G',7},{'H',8},{'J',1},{'K',2},{'L',3},{'M',4},{'N',5},{'P',7},{'Q',8}
            ,{'R',9},{'S',2},{'T',3},{'U',4},{'V',5},{'W',6},{'X',7},{'Y',8},{'Z',9}
        };
        static int[] weights = new int[] { 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 };
        public static List<string> PublicValidVinList = new List<string>() { "5L5SY144761","5L5TX044741","JKAEJEA11YA","WP0AD2A71CL",
			 "WP0AA2A78DL","44KFT42812W","SCBDR33WX7C","ZFFEW58A760","ZFFYT53A340", "19UYA1243VL","2HHES35811H",
			"19UYA1249VL","19UYA1247VL","19UYA1243VL","5VCEC6MF36H","1BABNCXA0YF",
			"1FCKE33G5MH","JH4DC53096S",
			"1FTWW30P75E","1FTSW20P36E"};

        public static Dictionary<string, string> stateDictionary = new Dictionary<string, string>
                {
                    {"AK", "Alaska"},
                    {"AL", "Alabama"},
                    {"AR", "Arkansas"},
                    {"AZ", "Arizona"},
                    {"CA", "California"},
                    {"CO", "Colorado"},
                    {"CT", "Connecticut"},
                    {"DC", "District of Columbia"},
                    {"DE", "Delaware"},
                    {"FL", "Florida"},
                    {"GA", "Georgia"},
                    {"HI", "Hawaii"},
                    {"IA", "Iowa"},
                    {"ID", "Idaho"},
                    {"IL", "Illinois"},
                    {"IN", "Indiana"},
                    {"KS", "Kansas"},
                    {"KY", "Kentucky"},
                    {"LA", "Louisiana"},
                    {"MA", "Massachusetts"},
                    {"ME", "Maine"},
                    {"MD", "Maryland"},
                    {"MI", "Michigan"},
                    {"MN", "Minnesota"},
                    {"MO", "Missouri"},
                    {"MS", "Mississippi"},
                    {"MT", "Montana"},
                    {"NC", "North Carolina"},
                    {"ND", "North Dakota"},
                    {"NE", "Nebraska"},
                    {"NH", "New Hampshire"},
                    {"NJ", "New Jersey"},
                    {"NM", "New Mexico"},
                    {"NY", "New York"},
                    {"NV", "Nevada"},
                    {"OH", "Ohio"},
                    {"OK", "Oklahoma"},
                    {"OR", "Oregon"},
                    {"PA", "Pennslyvania"},
                    {"RI", "Rhode Island"},
                    {"SC", "South Carolina"},
                    {"SD", "South Dakota"},
                    {"TN", "Tennessee"},
                    {"TX", "Texas"},
                    {"UT", "Utah"},
                    {"VT", "Vermont"},
                    {"VA", "Virginia"},
                    {"WA", "Washington"},
                    {"WI", "Wisconsin"},
                    {"WV", "West Virginia"},
                    {"WY", "Wyoming"}
                };
        public static void SendEmailTo(string[] mailAddress, string TestName, string Environment, string messageBody = "The test might have failed due to missing data. Please check manually.", string mailFrom = null, string attachedFile = null, string emailSubject = null)
        {
            Selenium s = new Selenium();
            
            string domainEmail = "-AutomationTest@iaai.com";
            string from = String.IsNullOrEmpty(mailFrom) ? Environment + domainEmail : mailFrom;
            MailMessage message = new MailMessage();
            message.From = new MailAddress(from);
            for (int i = 0; i < mailAddress.Count(); i++)
            {
                message.To.Add(new MailAddress(mailAddress[i]));
            }

            message.Subject = string.IsNullOrEmpty(emailSubject) ? "DO NOT Reply: " + TestName : emailSubject;
            message.Body = messageBody;
            if (!String.IsNullOrEmpty(attachedFile))
            {
                Attachment attach = new Attachment(attachedFile);
                message.Attachments.Add(attach);
            }
            SmtpClient client = new SmtpClient("hub.iaai.com");
            client.UseDefaultCredentials = true;
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
            }
        }
        public static string GenerateRandomNumber(int intNum)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            Thread.Sleep(50);
            string returnString = string.Empty;
            int counter;

            for (counter = 0; counter < intNum; counter++)
            {
                returnString += random.Next(1, 9).ToString();
            }
            return returnString;
        }
        public static int GetRandomNumberBetweenTwoRanges(int lowest, int highest)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            return random.Next(lowest, highest);
        }
        public static string ToNumericString(string input)
        {
            return new String(input.Where(char.IsDigit).ToArray());
        }

        public static string GenerateRandomAlphaNumeric(int length)
        {

            /////DONT use letter I,O or Q..Vin decode do not work with that letter
            var chars = "ABCDEFGHJKLMNPRSTUVWXYZ123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        public static double RandomDecimalNumberBetween(double minValue, double maxValue)
        {
            Random random = new Random();
            var next = random.NextDouble();

            return minValue + (next * (maxValue - minValue));
        }

        public static void SetVinCheckDigit(ref string vin)
        {
            int checkSum = 0;
            int i = 0;
            string checkValue;
            checkSum = vin.Sum(s => vinValues[s] * weights[i++]);
            checkSum = checkSum % 11;
            if (checkSum == 10)
            {
                checkValue = "X";
            }
            else
            {
                checkValue = checkSum.ToString();
            }
            vin = vin.Remove(8, 1).Insert(8, checkValue);
        }

        public static string GetRandomVin()
        {
            string vin = "";
            vin = PublicValidVinList[new System.Random().Next(PublicValidVinList.Count())] + GenerateRandomAlphaNumeric(6);
            SetVinCheckDigit(ref vin);
            return vin;
        }

        public static string StateConverter(string key)
        {
            
            string val = "";
            stateDictionary.TryGetValue(key.Trim(), out val);
            return val;
        }

        public static string ReturnNumbersOnly(string value)
        {
            string numbersOnlyValue = "";
            Regex r = new Regex(@"[^0-9]");
            numbersOnlyValue = r.Replace(value, String.Empty);
            return numbersOnlyValue;
        }


        public static void SetElementValue(ref XmlDocument document, string element, string value, int index = 0, string nameSpace = "")
        {
            if (document.GetElementsByTagName(element).Count == 0)
            {
                XmlNode newEle = document.CreateNode(XmlNodeType.Element, element, nameSpace);
                document.DocumentElement.AppendChild(newEle);
            }
            XmlElement selectedElement = (XmlElement)document.GetElementsByTagName(element)[index];
            selectedElement.InnerXml = @value;
        }

        public static string GetElementValue(XmlDocument document, string element)
        {
            string value = "";
            XmlElement selectedEle = (XmlElement)document.GetElementsByTagName(element)[0];
            value = selectedEle.InnerXml;
            return value;
        }


          public static decimal RoundUp(decimal number, int places)
            {
                decimal factor = RoundFactor(places);
                number *= factor;
                number = Math.Ceiling(number);
                number /= factor;
                return number;
            }

            public static decimal RoundDown(decimal number, int places)
            {
                decimal factor = RoundFactor(places);
                number *= factor;
                number = Math.Floor(number);
                number /= factor;
                return number;
            }

            internal static decimal RoundFactor(int places)
            {
                decimal factor = 1m;

                if (places < 0)
                {
                    places = -places;
                    for (int i = 0; i < places; i++)
                        factor /= 10m;
                }

                else
                {
                    for (int i = 0; i < places; i++)
                        factor *= 10m;
                }

                return factor;
            }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public static string GetDayWithSuffix(int day)
        {
            string d = day.ToString();
            if (day < 11 || day > 13)
            {
                if (d.EndsWith("1"))
                {
                    d += "st";
                }
                else if (d.EndsWith("2"))
                {
                    d += "nd";
                }
                else if (d.EndsWith("3"))
                {
                    d += "rd";
                }
                else
                {
                    d += "th";
                }
            }
            else
            {
                d += "th";
            }
                return d;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="Path"></param>
        /// <returns></returns>
        public static bool CheckFileDownloaded(string filename, string Path)
        {
            bool exist = false;
            System.Threading.Thread.Sleep(4000);
           // string Path = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Downloads";
            string[] filePaths = Directory.GetFiles(Path);
            foreach (string p in filePaths)
            {
                if (p.Contains(filename))
                {
                    FileInfo thisFile = new FileInfo(p);
                    //Check the file that are downloaded in the last 30 minutes
                    //if (thisFile.LastWriteTime.ToShortTimeString() == DateTime.Now.ToShortTimeString() ||
                    //thisFile.LastWriteTime.AddMinutes(5).ToShortTimeString() == DateTime.Now.ToShortTimeString() ||
                    //thisFile.LastWriteTime.AddMinutes(10).ToShortTimeString() == DateTime.Now.ToShortTimeString() ||
                    //thisFile.LastWriteTime.AddMinutes(30).ToShortTimeString() == DateTime.Now.ToShortTimeString())
                    exist = true;
                    break;
                    //File.Delete(p);
                }
            }
            return exist;
        }

        /// <summary>
        /// 
        /// </summary>
        public class GenericDictionary
        {
            private Dictionary<string, object> _dict = new Dictionary<string, object>();

            public void Add<T>(string key, T value) where T : class
            {
                _dict.Add(key, value);
            }

            public T GetValue<T>(string key) where T : class
            {
                return _dict[key] as T;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data_Array"></param>
        /// <param name="keyvalue"></param>
        /// <returns></returns>
        static String findFirstKeyByValue(Dictionary<string, string> Data_Array, String value)
        {
            if (Data_Array.ContainsValue(value))
            {
                foreach (String key in Data_Array.Keys)
                {
                    if (Data_Array[key].Equals(value))
                        return key;
                }
            }
            return null;
        }




    }
}
