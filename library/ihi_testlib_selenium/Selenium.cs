﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using Microsoft.Win32;
using System.Diagnostics;
using System.Collections.ObjectModel;
using OpenQA.Selenium.Support.Events;
using System.Configuration;
using System.IO;
using System.Drawing.Imaging;
using System.Xml;
using System.Xml.Linq;
using System.Web.UI;

namespace ihi_testlib_selenium
{

    /// <summary>
    /// 
    /// </summary>
    public class Selenium
    {


        /// <summary>
        /// 
        /// </summary>
        public static string IE_DRIVER_PATH = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + "\\library\\dll"; // System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        /// <summary>
        /// 
        /// </summary>
        public static string CHROME_DRIVER_PATH = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + "\\library\\dll";
        /// <summary>
        /// 
        /// </summary>
        public static string XML_LOG_FILE = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\SeleniumLog.xml";
        /// <summary>
        /// 
        /// </summary>
        public IWebDriver browserDriver;
        /// <summary>
        /// 
        /// </summary>
        public string errorCapture = "";
        /// <summary>
        /// 
        /// </summary>
      //  public SeleniumEventListener firingDriver;
        /// <summary>
        /// 
        /// </summary>
        public string LogPath = @"C:\Automation\SeleniumLog.txt";

        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum Contains
        {
            None = 0x0,
            ID = 0x1,
            Name = 0x2,
            Class = 0x4,
            Text = 0x8
        }

        /// <summary>
        /// 
        /// </summary>
        public enum Browser
        {
            IE, FireFox, Chrome
        }


        /// <summary>
        /// 
        /// </summary>
        //public class SeleniumEventListener : EventFiringWebDriver
        //{
        //    /// <summary>
        //    /// 
        //    /// </summary>
        //    /// <param name="driver"></param>
        //    public SeleniumEventListener(IWebDriver driver)
        //        : base(driver)
        //    {


        //        ElementValueChanging += new EventHandler<WebElementEventArgs>((sender, e) =>
        //        {
        //            string message = "Changing element : \r\n" + GetElementInformation(e.Element);
        //            string imageData = TakeScreenshot();
        //            WriteToLog(message, imageData);
        //        });

        //        Navigated += new EventHandler<WebDriverNavigationEventArgs>((sender, e) =>
        //        {
        //            //Console.WriteLine("Navigated to: " + e.Url);

        //        });

        //        ElementClicking += new EventHandler<WebElementEventArgs>((sender, e) =>
        //        {
        //            string message = "Clicking element : \r\n" + GetElementInformation(e.Element);
        //            string imageData = TakeScreenshot();
        //            WriteToLog(message, imageData);
        //        });

        //        ExceptionThrown += new EventHandler<WebDriverExceptionEventArgs>((sender, e) =>
        //        {

        //            string message = "Exception Caught: \r\n" + e.ThrownException;
        //            string imageData = TakeScreenshot();
        //            WriteToLog(message, imageData);
        //        });

        //        FindingElement += new EventHandler<FindElementEventArgs>((sender, e) =>
        //        {
        //            //using (System.IO.StreamWriter file = System.IO.File.AppendText(ConfigurationManager.AppSettings["LogPath"]))
        //            //{
        //            //    file.WriteLine("Searching for element : " + e.FindMethod);
        //            //}
        //        });


        //    }


        //    /// <summary>
        //    /// 
        //    /// </summary>
        //    /// <param name="element"></param>
        //    /// <returns></returns>
        //    public string GetElementInformation(IWebElement element)
        //    {
        //        string info = "";
        //        if (!String.IsNullOrEmpty(element.GetAttribute("id")))
        //        {
        //            info += "ID: " + element.GetAttribute("id");
        //            info += "\r\n";
        //        }
        //        if (!String.IsNullOrEmpty(element.GetAttribute("name")))
        //        {
        //            info += "Name: " + element.GetAttribute("name");
        //            info += "\r\n";
        //        }
        //        if (!String.IsNullOrEmpty(element.GetAttribute("class")))
        //        {
        //            info += "Class: " + element.GetAttribute("class");
        //            info += "\r\n";
        //        }
        //        if (!String.IsNullOrEmpty(element.Text))
        //        {
        //            info += "InnerText: " + element.Text;
        //        }

        //        return info;
        //    }

        //    /// <summary>
        //    /// 
        //    /// </summary>
        //    /// <returns></returns>
        //    public string TakeScreenshot()
        //    {
        //        Screenshot ss = ((ITakesScreenshot)this).GetScreenshot();
        //        string encoded = ss.AsBase64EncodedString;
        //        encoded = @"data:image/png;base64," + encoded;
        //        return encoded;
        //        //byte[] screenshotByteArray = ss.AsByteArray;
        //        //string date = DateTime.Now.ToString("MM-dd-yyyy_hh_mm_ss_fff");
        //        //ss.SaveAsFile(@"C:\Automation\SeleniumScreenShots\Screenshot_" + date + ".jpg", ImageFormat.Png);
        //    }

        //    /// <summary>
        //    /// 
        //    /// </summary>
        //    /// <param name="logMessage"></param>
        //    /// <param name="image"></param>
        //    public void WriteToLog(string logMessage, string image)
        //    {

        //        //using (System.IO.StreamWriter file = System.IO.File.AppendText(@"C:\Automation\SeleniumLog.txt"))
        //        //{
        //        //    file.WriteLine(logMessage);
        //        //}
        //        XDocument doc = XDocument.Load(XML_LOG_FILE);
        //        XElement ele = new XElement("Action", logMessage);
        //        ele.Add(new XElement("Image", image));
        //        XElement lastStep = doc.Element("Test").Elements("Step").LastOrDefault();
        //        lastStep.Add(ele);
        //        doc.Save(XML_LOG_FILE);
        //        //XmlDocument doc = new XmlDocument();
        //        //doc.Load(XML_LOG_FILE);
        //        //XmlNode action = doc.CreateElement("Action");
        //        //action.InnerText = logMessage;
        //        //doc.AppendChild(action);
        //        //doc.Save(XML_LOG_FILE);
        //    }


        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string WriteHtmlLog()
        {
            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
            {
                XDocument doc = XDocument.Load(XML_LOG_FILE);
                string testName = doc.Element("Test").Value;
                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.AddAttribute(HtmlTextWriterAttribute.Value, testName);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                List<XElement> steps = doc.Element("Test").Elements("Step").ToList();
                foreach (XElement step in steps)
                {
                    string stepInfo = step.Value;
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.RenderBeginTag(HtmlTextWriterTag.H2);
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, stepInfo);
                    writer.RenderEndTag();
                    List<XElement> actions = step.Elements("Action").ToList();
                    foreach (XElement action in actions)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Li);
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.AddAttribute(HtmlTextWriterAttribute.Value, action.Value);
                        writer.RenderEndTag();
                        writer.RenderBeginTag(HtmlTextWriterTag.Img);
                        string source = action.Element("Image").Value;
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, source);
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                }
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
            return stringWriter.ToString();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logMessage"></param>
        public void WriteToLog(string logMessage)
        {

            using (System.IO.StreamWriter file = System.IO.File.AppendText(@"C:\Automation\SeleniumLog.txt"))
            {
                file.WriteLine(logMessage);
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(XML_LOG_FILE);
            XmlNode action = doc.GetElementsByTagName("Header")[0];
            action.InnerText = logMessage;
            doc.AppendChild(action);
            doc.Save(XML_LOG_FILE);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stepMessage"></param>
        public void AddStep(string stepMessage)
        {
            XDocument doc = XDocument.Load(XML_LOG_FILE);
            doc.Element("Test").Add(new XElement("Step", stepMessage));
            doc.Save(XML_LOG_FILE);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void ResetLogFile(string message)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Automation\SeleniumLog.txt"))
            {
                file.WriteLine(message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void CreateLog(string message)
        {
            XDocument doc = new XDocument(
                new XElement("Test", new XAttribute("TestName", message)));
            doc.Save(XML_LOG_FILE);

            //XmlDocument doc = new XmlDocument();
            //XmlNode header = doc.CreateElement("Header");
            //header.InnerText = message;
            //doc.AppendChild(header);
            //doc.Save(XML_LOG_FILE);
        }

        /// <summary>
        /// 
        /// </summary>
        public Selenium()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            //browserDriver = new InternetExplorerDriver(IE_DRIVER_PATH,new  InternetExplorerOptions(),TimeSpan.FromSeconds(180));
            browserDriver = new ChromeDriver(CHROME_DRIVER_PATH, new ChromeOptions(), TimeSpan.FromSeconds(180));
        }

        /// <summary>
        /// Creates a new instance of the browserDriver and goes to the url provider.
        /// </summary>
        /// <param name="browser">The browser type that you want to select. IE,Firefox,Chrome</param>
        /// <param name="envirmentalURL">The URL that the driver is going to open up.</param>
        public void SetBrowser(Browser browser, string envirmentalURL)
        {
            switch (browser)
            {
                case Browser.IE:
                    //FindAndKillProcess("iexplore");
                    //FindAndKillProcess("IEDriverServer");

                    var options = new InternetExplorerOptions()
                    {
                        PageLoadStrategy = PageLoadStrategy.Normal,
                        RequireWindowFocus = true,
                        //ElementScrollBehavior = InternetExplorerElementScrollBehavior.Bottom,
                        //IgnoreZoomLevel = true,
                        EnsureCleanSession = true

                    };

                    ///// // / Set the browser to 100% zoom level using registry
                    //Microsoft.Win32.RegistryKey rkHKLM = Microsoft.Win32.Registry.CurrentUser;
                    //Microsoft.Win32.RegistryKey rkRun;

                    //rkRun = rkHKLM.OpenSubKey(@"Software\Microsoft\Internet Explorer\Zoom", true);
                    //if (Convert.ToString(rkRun.GetValue("ZoomFactor")) != "00016378")
                    //{
                    //    rkRun.SetValue("ResetZoomOnStartup ", 0, RegistryValueKind.DWord);
                    //    rkRun = rkHKLM.OpenSubKey(@"Software\Microsoft\Internet Explorer\Zoom", true);
                    //    rkRun.SetValue("ZoomFactor", 100000, RegistryValueKind.DWord);
                    //}

                    //var service = InternetExplorerDriverService.CreateDefaultService(IE_DRIVER_PATH);
                    //service.LogFile = IE_DRIVER_PATH + @"\IE.log";
                    //service.LoggingLevel = InternetExplorerDriverLogLevel.Trace;

                    //browserDriver = new InternetExplorerDriver(service, options, TimeSpan.FromSeconds(30));
                    browserDriver = new InternetExplorerDriver(IE_DRIVER_PATH, options);
                    browserDriver.Manage().Cookies.DeleteAllCookies();

                    //browserDriver.Navigate();
                    //This works for IE and Firefox. 
                    browserDriver.Manage().Window.Maximize();

                    /// // / set global implicit time wait for each test case just once

                    browserDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                    browserDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
                    browserDriver.Navigate().GoToUrl(envirmentalURL);
                    break;

                case Browser.FireFox:
                    //var ffBinary = new FirefoxBinary(@"C:\Program Files\Mozilla Firefox\firefox.exe");
                    //var currentUsername = System.Environment.UserName;
                    // var firefoxProfile = new FirefoxProfile(@"C:\Users\" + currentUsername + @"\AppData\Roaming\Mozilla\Firefox\Profiles\q12vdgwd.default");
                    //browserDriver = new FirefoxDriver(ffBinary, firefoxProfile);

                    browserDriver = new FirefoxDriver();
                    browserDriver.Manage().Cookies.DeleteAllCookies();
                    browserDriver.Navigate().GoToUrl(envirmentalURL);
                    browserDriver.Manage().Window.Maximize();
                    browserDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

                    break;

                case Browser.Chrome:
                    FindAndKillProcess("chrome");
                    browserDriver = new ChromeDriver(CHROME_DRIVER_PATH);
                    browserDriver.Manage().Cookies.DeleteAllCookies();
                    browserDriver.Navigate().GoToUrl(envirmentalURL);
                    browserDriver.Manage().Window.Maximize();
                    browserDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

                    break;

                default:
                    break;
            }
            //firingDriver = new SeleniumEventListener(browserDriver);
            //browserDriver = firingDriver;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool FindAndKillProcess(string name)
        {
            foreach (Process clsProcess in Process.GetProcessesByName(name))
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    try { clsProcess.Kill(); }
                    catch { return false; }
                }
            }
            return false;
        }

        /// <summary>
        /// A more unified way to capture any errors that are caused during a call to the Selenium Library.
        /// </summary>
        /// <param name="ex">The exception that was thrown.</param>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="xpath">The xpath value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void CaptureError(Exception ex, string idTXT = null, string nameTXT = null, string tagTXT = null, string classTXT = null, string xpath = null, string innerTXT = null, int stepNumber = 0)
        {
            errorCapture = "Issue with control. ";
            if (idTXT != null)
                errorCapture += "ID:" + idTXT + " ";
            if (nameTXT != null)
                errorCapture += "Name:" + nameTXT + " ";
            if (tagTXT != null)
                errorCapture += "Tag:" + tagTXT + " ";
            if (classTXT != null)
                errorCapture += "Class:" + classTXT + " ";
            if (innerTXT != null)
                errorCapture += "Inner Text:" + innerTXT + " ";
            if (xpath != null)
                errorCapture += "XPath:" + xpath;

            throw new Exception("Error in Step " + stepNumber + "\r\n" + errorCapture + " : " + ex.Message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeout"></param>
        public void WaitForPageToLoad(int timeout = 30)
        {
            try
            {
                IWait<IWebDriver> wait = new OpenQA.Selenium.Support.UI.WebDriverWait(browserDriver, TimeSpan.FromSeconds(timeout));
                wait.Until(driver1 => ((IJavaScriptExecutor)browserDriver).ExecuteScript("return document.readyState").Equals("complete"));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in page load wait. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CreateAlert()
        {
            string handle = browserDriver.CurrentWindowHandle;
            IJavaScriptExecutor js = (IJavaScriptExecutor)browserDriver;
            js.ExecuteScript("alert('Test')");
            browserDriver.SwitchTo().Alert().Accept();
            browserDriver.SwitchTo().Window(handle);
        }



        #region Selenium Methods

        /// <summary>
        /// This method can be used to select items from a drop down menu 
        /// and select items from drop down lists
        /// </summary>
        /// <param name="textToSelect">The text value you want to select from the drop down</param>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void SelectElement(string textToSelect = null, string valueToSelect = null, int indexToSelect = -1, string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                SelectElement elementToSelect = new SelectElement(FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber));
                try
                {
                    if (valueToSelect != null)
                        elementToSelect.SelectByValue(valueToSelect);

                    else if (textToSelect != null)
                        elementToSelect.SelectByText(textToSelect);
                    else
                        elementToSelect.SelectByIndex(indexToSelect);
                }
                catch (Exception ex)
                {
                    IList<IWebElement> options = elementToSelect.Options;
                    string text = options.Where(n => n.Text.Contains(textToSelect)).First().Text;
                    elementToSelect.SelectByText(text);
                    Console.WriteLine(ex);
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }
        }

        /// <summary>
        /// This method can be used to select items from a drop down menu 
        /// and select items from drop down lists 
        /// </summary>
        /// <param name="textToSelect"></param>
        /// <param name="indexToSelect"></param>
        /// <param name="xpath"></param>
        /// <param name="stepNumber"></param>
        public void SelectElement(string textToSelect = null, int indexToSelect = -1, string xpath = null, int stepNumber = 0)

        {
            try
            {
                SelectElement select = new SelectElement(FindHTMLControl(xPath: xpath, stepNumber: stepNumber));
                if (textToSelect != null)
                    select.SelectByText(textToSelect);
                else
                    select.SelectByIndex(indexToSelect);
            }
            catch (Exception ex)
            {
                CaptureError(ex, xpath: xpath, stepNumber: stepNumber);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textsToSelect"></param>
        /// <param name="valuesToSelect"></param>
        /// <param name="selectAll"></param>
        /// <param name="indexToSelect"></param>
        /// <param name="idTXT"></param>
        /// <param name="nameTXT"></param>
        /// <param name="classTXT"></param>
        /// <param name="innerTXT"></param>
        /// <param name="tagTXT"></param>
        /// <param name="flag"></param>
        /// <param name="index"></param>
        /// <param name="stepNumber"></param>
        public void MultiSelect(List<string> textsToSelect = null, List<string> valuesToSelect = null, bool selectAll = false, int indexToSelect = -1, string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                SelectElement elementToSelect = new SelectElement(FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber));
                try
                {
                    if (selectAll)
                    {
                        var options = elementToSelect.Options;
                        if (options != null && options.Count > 0)
                        {
                            foreach (var option in options)
                                elementToSelect.SelectByText(option.Text);
                        }
                    }
                    else
                    {

                        if (valuesToSelect != null && valuesToSelect.Count > 0)
                        {
                            foreach (var valueToSelect in valuesToSelect)
                                elementToSelect.SelectByValue(valueToSelect);
                        }
                        else if (textsToSelect != null && textsToSelect.Count > 0)
                        {
                            foreach (var textToSelect in textsToSelect)
                                elementToSelect.SelectByText(textToSelect);
                        }
                        else
                            elementToSelect.SelectByIndex(indexToSelect);
                    }
                }
                catch (Exception ex)
                {
                    IList<IWebElement> options = elementToSelect.Options;
                    //string text = options.Where(n => n.Text.Contains(textToSelect)).First().Text;
                    //elementToSelect.SelectByText(text);
                    Console.WriteLine(ex);
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }

        }

        /// <summary>
        /// Find an element.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Html IWebElement</returns>
        public IWebElement FindHTMLControl(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            IWebElement foundElement = null;
            try
            {
                string xpath = BuildXpath(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
                //  IWebElement control = browserdriver.browserDriver.FindElement(By.XPath(xpath));   
                //IWebElement control = WebDriverExtensions.FindElement(browserDriver, By.XPath(xpath), 120);
                //return control;
                WebDriverWait wait = new WebDriverWait(browserDriver, TimeSpan.FromSeconds(120));
                foundElement = wait.Until<IWebElement>((wd) =>
                {
                    return wd.FindElement(By.XPath(xpath));
                });

            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }
            return foundElement;
        }

        /// <summary>
        /// This will click an element and then switch the browser driver to the window that opens due to clicking
        /// on the element.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="xpath">The xpath value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns></returns>
        public string SwitchWindowToPopup(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, string xpath = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                ReadOnlyCollection<string> existingHandles = browserDriver.WindowHandles;

                string originalHandle = browserDriver.CurrentWindowHandle;
                if (xpath == null)
                    ClickOnHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
                else
                    ClickOnHTMLControl(XPath: xpath);
                ReadOnlyCollection<string> currentHandles = browserDriver.WindowHandles;
                int count = 0;
                while (currentHandles.Count <= existingHandles.Count && count < 20)
                {
                    count++;
                    System.Threading.Thread.Sleep(1000);
                    currentHandles = browserDriver.WindowHandles;
                }
                string popupHandle = "";
                for (int i = 0; i < currentHandles.Count; i++)
                {
                    if (!existingHandles.Contains(currentHandles[i]))
                    {
                        popupHandle = currentHandles[i];
                        break;
                    }
                }
                //string popupHandle = currentHandles.Where(handles => handles != originalHandle).First();
                browserDriver.SwitchTo().Window(popupHandle);
                //WaitForPageToLoad();
                browserDriver.Manage().Window.Maximize();
                return originalHandle;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT: idTXT, nameTXT: nameTXT, tagTXT: tagTXT, classTXT: classTXT, xpath: xpath, innerTXT: innerTXT, stepNumber: stepNumber);
                return "";
            }
        }

        /// <summary>
        /// This will switch to any other open window using Selenium.
        /// </summary>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void SwitchWindowToPopup(int stepNumber = 0)
        {
            try
            {
                ReadOnlyCollection<string> existingHandles = browserDriver.WindowHandles;
                int numberOfTImesToTry = 10, counter = 0;
                string originalHandle = browserDriver.CurrentWindowHandle;
                ReadOnlyCollection<string> currentHandles = browserDriver.WindowHandles;
                if (currentHandles.Count < 2)
                {
                    while (currentHandles.Count <= existingHandles.Count)
                    {
                        System.Threading.Thread.Sleep(5000);
                        currentHandles = browserDriver.WindowHandles;
                        counter++;
                        if (counter == numberOfTImesToTry)
                            break;
                    }
                }
                string popupHandle = currentHandles.Where(s => s != originalHandle).First();
                browserDriver.SwitchTo().Window(popupHandle);
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
            }
            //WaitForPageToLoad();
        }

        /// <summary>
        /// Hover and Click on a control.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="hardCodedXpath"></param>
        /// <param name="parent"></param>
        /// <param name="hardCodedParentXpath"></param>
        public void HoverAndClick(IWebElement parent = null, string hardCodedParentXpath = null, IWebElement element = null, string hardCodedXpath = null, int stepNumber = 0)
        {
            /// // / This method allows to hover and click on a control, depending
            /// // / on the parameters this can be done in 1 of 4 ways
            /// // / 1) If the parent and element are parameters, then 
            /// // / parent will be hovered over followed by the control that
            /// // / needs to be clicked.
            /// // / 2) If the hard coded parent xpath and element xpath are
            /// // / parameters, if for some reason the element cannot be located
            /// // / normally, then using those values the parent will be hovered
            /// // / over followed by the child control which will be clicked. 
            /// // / 3) If the parent is not a parameter and only the element is,
            /// // / then the element will be located and hovered over and clicked on.
            /// // / 4) Otherwise, the hard coded xpath value provided as a parameter
            /// // / will be used to locate the element, and perform the hover and 
            /// // / click functionality 
            try
            {
                Actions action = new Actions(browserDriver);
                if (parent != null && element != null)
                    action.MoveToElement(parent).MoveToElement(element).Click().Build().Perform();

                else if (hardCodedParentXpath != null && hardCodedXpath != null)
                {
                    action.MoveToElement(browserDriver.FindElement(By.XPath(hardCodedParentXpath))).Build().Perform();
                    System.Threading.Thread.Sleep(2000);
                    action.MoveToElement(browserDriver.FindElement(By.XPath(hardCodedXpath))).Click().Build().Perform();
                }
                else if (parent == null && element != null)
                    action.MoveToElement(element).Click().Build().Perform();

                else
                    action.MoveToElement(browserDriver.FindElement(By.XPath(hardCodedXpath))).Click().Build().Perform();
            }
            catch (Exception ex)
            {
                CaptureError(ex, xpath: hardCodedParentXpath, stepNumber: stepNumber);
            }
        }


        /// <summary>
        /// Move to the element by ID in order to trigger the hover over window
        /// </summary>
        /// <param name="XPath"></param>
        /// <param name="stepNumber"></param>
        /// <returns></returns>
        public bool HoverOverByControl(string XPath, int stepNumber = 0)
        {
            try
            {
                if (browserDriver.FindElement(By.XPath(XPath)).Displayed)
                {
                    Actions action = new Actions(browserDriver);
                    action.MoveToElement(browserDriver.FindElement(By.XPath(XPath))).Perform();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, XPath, null, stepNumber);
                return false;
            }
        }

        /// <summary>
        /// Move to the element by ID in order to trigger the hover over window
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public bool HoverOverByControl(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                if (control.Displayed)
                {
                    Actions action = new Actions(browserDriver);
                    action.MoveToElement(control).Perform();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return false;
            }
        }
        /// <summary>
        /// This will click on control based off the parameters.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void ClickOnHTMLControl(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                control.Click();
                //WaitForPageToLoad();
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }
        }
        /// <summary>
        /// This will click on the control based off the parameters
        /// </summary>
        /// <param name="XPath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void ClickOnHTMLControl(string XPath, int stepNumber = 0)
        {
            try
            {
                browserDriver.FindElement(By.XPath(XPath)).Click();
                //WaitForPageToLoad();
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, XPath, null, stepNumber);
            }
        }
        /// <summary>
        /// This will try and assert that the control is enabled and will fail the test if it is not enabled.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns></returns>
        public bool Assertion_IsControlEnabled(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                return control.Enabled;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return false;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xPath"></param>
        /// <param name="stepNumber"></param>
        /// <returns></returns>
        public bool Assertion_IsControlEnabled(string xPath, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(xPath: xPath);
                return control.Enabled;
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, xPath, null, stepNumber);
                return false;
            }

        }
        /// <summary>
        /// This checks if the control is enabled.
        /// </summary>
        /// <param name="xpath">The xpath value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>The controls enabled status</returns>
        public bool IsControlEnabled(string xpath, int stepNumber = 0)
        {
            try
            {
                IWebElement control = browserDriver.FindElement(By.XPath(xpath));
                return control.Enabled;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //CaptureError(ex, null, null, null, null, xpath, null, stepNumber);
                return false;
            }
        }
        /// <summary>
        /// This checks if the control is enabled.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>The controls enabled status</returns>
        public bool IsControlEnabled(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                return control.Enabled;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //CaptureError(ex, null, null, null, null, xpath, null, stepNumber);
                return false;
            }
        }

        /// <summary>
        /// This asserts that the checkbox is already checked.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="xpath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void Assertion_IsCheckBoxChecked(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, string xpath = null, int stepNumber = 0)
        {
            try
            {
                IWebElement control;

                if (xpath != null)
                    control = FindHTMLControl(xpath, stepNumber);
                else
                    control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);

                if (!control.Selected)
                {
                    throw new System.ArgumentException("Checkbox was checked!");
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, xpath, innerTXT, stepNumber);
            }
        }
        /// <summary>
        /// This asserts that the checkbox is not checked.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="xpath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void Assertion_IsCheckBoxNotChecked(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, string xpath = null, int stepNumber = 0)
        {
            try
            {
                IWebElement control;

                if (xpath != null)
                    control = FindHTMLControl(xpath, stepNumber);
                else
                    control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);

                if (control.Selected)
                {
                    throw new System.ArgumentException("Checkbox was checked!");
                }
            }
            catch (Exception ex)
            {

                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, xpath, innerTXT, stepNumber);
            }
        }
        /// <summary>
        /// This adds text to the textbox.
        /// </summary>
        /// <param name="textToAdd">Text that is going to be added to the textbox</param>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void AddTextToTextbox(string textToAdd = null, string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                string val = control.GetAttribute("Value");
                if (String.IsNullOrEmpty(val) || val.CompareTo(textToAdd) != 0)
                {
                    try
                    {
                        control.Clear();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    control.SendKeys(textToAdd);
                }
            }
            catch (Exception ex)
            {

                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }
        }
        /// <summary>
        /// This adds text to the textbox.
        /// </summary>
        /// <param name="XPath">The xpath value of the object</param>
        /// <param name="textToAdd">Text that is going to be added to the textbox</param>
        /// <param name="stepNumber">The step number of the test case</param>
        public void AddTextToTextbox(string XPath, string textToAdd, int stepNumber = 0)
        {
            try
            {
                IWebElement control = browserDriver.FindElement(By.XPath(XPath));
                control.Clear();
                control.SendKeys(textToAdd);
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, XPath, null, stepNumber);
            }
        }
        /// <summary>
        /// Get any HTML attribute for an element.
        /// </summary>
        /// <param name="Xpath">The xpath value of the object</param>
        /// <param name="attributeName">The attribute that you want the value of.</param>
        /// <param name="stepNumber">The step number of the test case</param>
        /// <returns>The value of the attribute you wanted</returns>
        public string GetAttribute(string Xpath, string attributeName, int stepNumber = 0)
        {
            try
            {
                IWebElement control = browserDriver.FindElement(By.XPath(Xpath));
                string value = control.GetAttribute(attributeName);
                return value;
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, Xpath, null, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Get any HTML attribute for an element.
        /// </summary>
        /// <param name="attributeName">The attribute that you want the value of.</param>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>The value of the attribute you wanted</returns>
        public string GetAttribute(string attributeName, string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                string value = control.GetAttribute(attributeName);
                return value;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Get the controls inner text value.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <param name="wantHtml">Gets the inner HTML instead of the inner text.</param>
        /// <returns>Gets the inner text value of the control</returns>
        public string ReadControlsInnerText(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, bool wantHtml = false, int stepNumber = 0)
        {
            try
            {
                string text = "";
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                if (wantHtml)
                    text = control.GetAttribute("innerHTML").Trim();
                else
                    text = control.Text.Trim();
                return text;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets the controls inner text value.
        /// </summary>
        /// <param name="xPath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Gets the inner text value of the control</returns>
        public string ReadControlsInnerText(string xPath, int stepNumber = 0)
        {
            try
            {
                return browserDriver.FindElement(By.XPath(xPath)).Text.Trim();
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, xPath, null, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets the text value from the text box.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Text from textbox</returns>
        public string ReadInTextFromTextBox(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                string text = "";
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                text = control.GetAttribute("value");

                return text ?? "";
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets the text value from the text box.
        /// </summary>
        /// <param name="xPath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Text from textbox</returns>
        public string ReadInTextFromTextBox(string xPath, int stepNumber = 0)
        {
            try
            {
                return browserDriver.FindElement(By.XPath(xPath)).GetAttribute("value") ?? "";
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, xPath, null, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets the currently selected value from the dropdown.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Currently selected value from drop down</returns>
        public string GetSelectedItemFromDropDown(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                SelectElement selectedValue = new SelectElement(control);

                return selectedValue.AllSelectedOptions.Count == 0 ? "" : selectedValue.SelectedOption.Text;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets the currently selected value from a combo box.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Currently selected value from combo box</returns>
        public string GetSelectedComboBoxValue(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                string value = "";
                string xpath = BuildXpath(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
                SelectElement element = new SelectElement(browserDriver.FindElement(By.XPath(xpath)));
                value = element.SelectedOption.Text;
                return value;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets the item count from the dropdown.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Item count of drop downs.</returns>
        public int GetItemCountFromDropDown(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                var dropdown = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                int count = dropdown.FindElements(By.TagName(("SELECT"))).Count;
                return count;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return -1;
            }

        }

        /// <summary>
        /// Gets the item count from the dropdown.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>Item count of drop downs.</returns>
        public int GetItemCountFromDropDown(string xPath, int stepNumber = 0)
        {
            try
            {
                var dropdown = FindHTMLControl(xPath, stepNumber);
                int count = dropdown.FindElements(By.TagName(("SELECT"))).Count;
                return count;
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, xPath, null, stepNumber);
                return -1;
            }        

        }
        /// <summary>
        /// Gets the last item from the drop down.
        /// </summary>
        /// <param name="idTXT"></param>
        /// <param name="choiceTagName"></param>
        /// <param name="nameTXT"></param>
        /// <param name="classTXT"></param>
        /// <param name="innerTXT"></param>
        /// <param name="tagTXT"></param>
        /// <param name="flag"></param>
        /// <param name="index"></param>
        /// <param name="stepNumber"></param>
        /// <returns></returns>
        public string GetLastItemFromDropDown(string idTXT = null, string choiceTagName = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                var dropdown = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                var lastItem = dropdown.FindElements(By.TagName((choiceTagName))).LastOrDefault().GetAttribute("text");
                return lastItem.ToString().Trim();
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
                return null;
            }

        }
        /// <summary>
        /// Checks if the element is visible or not.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <param name="xPath">The xpath value of the object</param>
        /// <returns>Whether or not the element is visible.</returns>
        public bool IsElementVisible(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, string xPath = null, int stepNumber = 0)
        {
            bool foundElement = false;
            try
            {
                string xpath = "";
                browserDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 0);
                if (xPath == null)
                    xpath = BuildXpath(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
                else
                    xpath = xPath;
                IWebElement elemen = browserDriver.FindElement(By.XPath(xpath));
                var element = browserDriver.FindElements(By.XPath(xpath)).FirstOrDefault();
                foundElement = element == null ? false : element.Displayed;
                browserDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 120);
            }
            catch { }
            return foundElement;
        }

        /// <summary>
        /// Asserts that the element is visible.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <param name="xPath">The xpath value of the object</param>
        public void Assertion_IsElementVisible(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, string xPath = null, int stepNumber = 0)
        {
            try
            {
                if (!IsElementVisible(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, xPath, stepNumber))
                {
                    throw new ElementNotVisibleException();
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, xPath, innerTXT, stepNumber);
            }
        }
        /// <summary>
        /// Makes sure that the checkbox is either selected or unselected.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <param name="xPath">The xpath value of the object</param>
        /// <param name="Check">The status that you want the checkbox in</param>
        public void SelectCheckBox(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, string xPath = null, bool Check = true, int stepNumber = 0)
        {
            try
            {
                IWebElement control;
                if (xPath != null)
                    control = FindHTMLControl(xPath, stepNumber);
                else
                    control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);

                if (!Check)
                {

                    if (control.Selected)
                        control.Click();
                }
                else
                {
                    if (!control.Selected)
                        control.Click();
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, xPath, innerTXT, stepNumber);
            }
        }
        /// <summary>
        /// This will take the parameters that you want to search on and build the Xpath for you.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <returns>The built xpath value.</returns>
        public string BuildXpath(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1)
        {
            string xpathCommand = "(//";
            if (tagTXT != null)
                xpathCommand += tagTXT;
            else
                xpathCommand += "*";
            xpathCommand += "[";
            if (idTXT != null)
                if ((flag & Contains.ID) == Contains.ID)
                    xpathCommand += "contains(@id,'" + idTXT + "') and ";
                else
                    xpathCommand += "@id='" + idTXT + "' and ";
            if (nameTXT != null)
                if ((flag & Contains.Name) == Contains.Name)
                    xpathCommand += "contains(@name,'" + nameTXT + "') and ";
                else
                    xpathCommand += "@name='" + nameTXT + "' and ";
            if (classTXT != null)
                if ((flag & Contains.Class) == Contains.Class)
                    xpathCommand += "contains(@class,'" + classTXT + "') and ";
                else
                    xpathCommand += "@class='" + classTXT + "' and ";
            if (innerTXT != null)
                if ((flag & Contains.Text) == Contains.Text)
                    xpathCommand += "contains(text(),'" + innerTXT + "') and ";
                else
                    xpathCommand += "text()='" + innerTXT + "' and ";

            xpathCommand = xpathCommand.Remove(xpathCommand.Length - 5, 5);
            xpathCommand += "])";
            if (index != 1)
                xpathCommand += "[" + index.ToString() + "]";
            return xpathCommand;
        }
        /// <summary>
        /// Changes xpath value to make sure that special characters are split up.
        /// </summary>
        /// <param name="text">Original xpath value.</param>
        /// <returns>The concatonated function for xpath</returns>
        public string GetTextConcat(string text)
        {
            string[] split = text.Split('\'', '"');
            char[] textArray = text.ToArray();
            int[] apostIndicies = textArray.Select((b, i) => b == '\'' ? i : -1).Where(i => i != -1).ToArray();
            int[] quoteIndicies = textArray.Select((b, i) => b == '"' ? i : -1).Where(i => i != -1).ToArray();
            string concat = "concat(";
            foreach (string s in split)
            {
                concat += "'" + s + "',";
            }
            concat.Remove(concat.Length - 1);
            concat += ")";
            return concat;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string GetXpathTextSpecialCharacters(string text)
        {
            string[] split = text.Split('\'', '"');
            string xpath = "";
            for (int i = 0; i < split.Count(); i++)
            {
                xpath += "contains(text(),'" + split[i] + "') ";
                if (i != split.Count() - 1)
                {
                    xpath += "and ";
                }
            }
            return xpath;
        }

        /// <summary>
        /// Closes the alert window that is open.
        /// </summary>
        /// <param name="stepNumber">The stepnumber of the test case.</param>
        public void HandleAlertBox(int stepNumber = 0)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(browserDriver, new TimeSpan(0, 0, 20));
                wait.IgnoreExceptionTypes(new Type[] { typeof(NoAlertPresentException) });
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent());
                browserDriver.SwitchTo().Alert().Accept();
            }
            catch (Exception ex) { CaptureError(ex, stepNumber: stepNumber); }
        }

        /// <summary>
        /// Dismisses the alert box by selected "Cancel" or "No"
        /// </summary>
        /// <param name="stepNumber"></param>
        public void DismissAlertBox(int stepNumber = 0)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(browserDriver, new TimeSpan(0, 0, 20));//.Until().IgnoreExceptionTypes(new Type[] { typeof(NoAlertPresentException) });
                wait.IgnoreExceptionTypes(new Type[] { typeof(NoAlertPresentException) });
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent());
                browserDriver.SwitchTo().Alert().Dismiss();
            }
            catch (Exception ex) { CaptureError(ex, stepNumber: stepNumber); }
        }

        /// <summary>
        /// Wait for an element to be visible.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <param name="xpath">The xpath value of the control.</param>
        /// <param name="numberOfTimes">The amount of seconds you will wait for the elemtn to be visible.</param>
        public void WaitForElement(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int numberOfTimes = 60, string xpath = null, int stepNumber = 0)
        {
            bool found = false;
            int count = 0;
            while (!found && count < numberOfTimes)
            {
                try
                {
                    found = IsElementVisible(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, xpath, stepNumber);
                }
                finally
                {
                    count++;
                    System.Threading.Thread.Sleep(1000);
                }
            }

        }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="idTXT"></param>
    /// <param name="nameTXT"></param>
    /// <param name="classTXT"></param>
    /// <param name="innerTXT"></param>
    /// <param name="tagTXT"></param>
    /// <param name="flag"></param>
    /// <param name="index"></param>
    /// <param name="numberOfTimes"></param>
    /// <param name="xpath"></param>
    /// <param name="stepNumber"></param>
        public void WaitForElementNotVisible(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int numberOfTimes = 60, string xpath = null, int stepNumber = 0)
        {

            try
            {
                var wait = new OpenQA.Selenium.Support.UI.WebDriverWait(browserDriver, TimeSpan.FromSeconds(numberOfTimes));
                wait.Until(driver1 => !IsElementVisible(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index));
                Console.WriteLine("Element is not visible..");
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }


        }


        /// <summary>
        /// Wait for an element to be enabled.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <param name="numberOfTimes">The amount of seconds you will wait for the element to be enabled.</param>
        public void WaitForEnabledElement(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int numberOfTimes = 60, int stepNumber = 0)
        {
            try
            {
                for (int i = 0; i < numberOfTimes; i++)
                {
                    if (!Assertion_IsControlEnabled(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber))
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    else
                    {
                        break;
                    }
                    if (i == (numberOfTimes - 1) && !IsElementVisible(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber: stepNumber))
                    {
                        throw new ElementNotVisibleException();
                    }
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xPath"></param>
        /// <param name="numberOfTimes"></param>
        /// <param name="stepNumber"></param>
        public void WaitForEnabledElement(string xPath, int numberOfTimes = 60, int stepNumber = 0)
        {
            try
            {
                for (int i = 0; i < numberOfTimes; i++)
                {
                    if (!Assertion_IsControlEnabled(xPath, stepNumber))
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    else
                    {
                        break;
                    }
                    if (i == (numberOfTimes - 1) && !IsElementVisible(xPath: xPath, stepNumber: stepNumber))
                    {
                        throw new ElementNotVisibleException("Element is not visible in UI:");
                    }
                }
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, xPath, null, stepNumber);
            }
        }

        public void ScrollToBottomofPage()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)browserDriver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);");
        }

        public void ScrollTo(int xPosition = 0, int yPosition = 0)
        {
            var script = String.Format("window.scrollTo({0}, {1})", xPosition, yPosition);
            IJavaScriptExecutor js = (IJavaScriptExecutor)browserDriver;
            js.ExecuteScript(script);
        }

        public IWebElement ScrollToView(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, string xPath = null)
        {
            string xpath = "";
            browserDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 0);
            if (xPath == null)
                xpath = BuildXpath(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
            else
                xpath = xPath;
            var element = browserDriver.FindElements(By.XPath(xpath)).FirstOrDefault();
            ScrollToView(element);
            return element;
        }

        public void ScrollToView(IWebElement element)
        {
            if (element.Location.Y > 200)
            {
                ScrollTo(0, element.Location.Y - 100); // Make sure element is in the view but below the top navigation pane
            }

        }
        /// <summary>
        /// Gets the text that is in the active alert window.
        /// </summary>
        /// <param name="stepNumber">The step number of the test case</param>
        /// <returns></returns>
        public string GetTextFromAlertBox(int stepNumber = 0)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(browserDriver, new TimeSpan(0, 0, 20));
                wait.IgnoreExceptionTypes(new Type[] { typeof(NoAlertPresentException) });
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent());
                // Switch the driver context to the alert
                IAlert alertDialog = browserDriver.SwitchTo().Alert();
                // Get the alert text
                return alertDialog.Text;
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
                return "";
            }
        }
        /// <summary>
        /// Mouse over the element.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void MoveToControl(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            IWebElement ele = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
            try
            {
                Actions action = new Actions(browserDriver);
                action.MoveToElement(ele);
                action.Perform();
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }
        }
        /// <summary>
        /// Mouse over the element.
        /// </summary>
        /// <param name="xPath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void MoveToControl(string xPath = null, int stepNumber = 1)
        {
            IWebElement ele = FindHTMLControl(xPath, stepNumber);
            try
            {
                Actions action = new Actions(browserDriver);
                action.MoveToElement(ele);
                action.Perform();
            }
            catch (Exception ex)
            {
                CaptureError(ex, xpath: xPath, stepNumber: stepNumber);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keysToSend"></param>
        /// <param name="stepNumber"></param>
        public void KeyboardAction(string keysToSend, int stepNumber = 1)
        {
            try
            {
                Actions action = new Actions(browserDriver);
                action.SendKeys(keysToSend);
                action.Perform();
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTXT"></param>
        /// <param name="nameTXT"></param>
        /// <param name="classTXT"></param>
        /// <param name="innerTXT"></param>
        /// <param name="tagTXT"></param>
        /// <param name="flag"></param>
        /// <param name="index"></param>
        /// <param name="xPath"></param>
        /// <param name="stepNumber"></param>
        /// <returns></returns>
        public bool IsElementExist(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, string xPath = null, int stepNumber = 0)
        {
            try
            {
                string xpath = "";
                browserDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 0);
                if (xPath == null)
                    xpath = BuildXpath(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
                else
                    xpath = xPath;
                var element = browserDriver.FindElements(By.XPath(xpath)).FirstOrDefault();
                browserDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 20, 0);
                if (element != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        /// <summary>
        /// Returns the value of any css option of the found element.
        /// </summary>
        /// <param name="cssOption">The css option that you want the value of.</param>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>The value of the asked for css option.</returns>
        public string GetCSSValue(string cssOption, string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 1)
        {
            string cssValue = "";
            try
            {
                IWebElement ele = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index, stepNumber);
                cssValue = ele.GetCssValue(cssOption);
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT, nameTXT, tagTXT, classTXT, null, innerTXT, stepNumber);
            }
            return cssValue;
        }
        /// <summary>
        /// Returns the value of any css option of the found element.
        /// </summary>
        /// <param name="cssOption">The css option that you want the value of.</param>
        /// <param name="xpath">The xpath value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns></returns>
        public string GetCSSValue(string cssOption, string xpath, int stepNumber)
        {
            string cssValue = "";
            try
            {
                IWebElement ele = FindHTMLControl(xpath, stepNumber);
                cssValue = ele.GetCssValue(cssOption);
            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, xpath, null, stepNumber);
            }
            return cssValue;
        }
        /// <summary>
        /// Returns the amount of controls that exists.
        /// </summary>
        /// <param name="xPath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns></returns>
        public int GetControlsTotalCount(string xPath, int stepNumber = 0)
        {
            try
            {
                return browserDriver.FindElements(By.XPath(xPath)).Count;
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
                return -1;
            }
        }
        /// <summary>
        /// Find the control.
        /// </summary>
        /// <param name="xPath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case</param>
        /// <returns>The element that was found</returns>
        public IWebElement FindHTMLControl(string xPath, int stepNumber = 0)
        {
            try
            {

                return browserDriver.FindElement(By.XPath(xPath));

            }
            catch (Exception ex)
            {
                CaptureError(ex, null, null, null, null, xPath, null, stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets a list of all the elements that match the description passed in.
        /// </summary>
        /// <param name="xPath">The xpath value of the objects.</param>
        /// <param name="stepNumber">The step number of the test case</param>
        /// <returns>List of elements found.</returns>
        public ReadOnlyCollection<IWebElement> GetControlCollection(string xPath, int stepNumber = 0)
        {
            try
            {
                return browserDriver.FindElements(By.XPath(xPath));
            }
            catch (Exception ex)
            {
                CaptureError(ex, xpath: xPath, stepNumber: stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Gets a list of all the elements that match the description passed in.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>List of elements found.</returns>
        public ReadOnlyCollection<IWebElement> GetControlCollection(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                string xPath = BuildXpath(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
                return browserDriver.FindElements(By.XPath(xPath));
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT: idTXT, nameTXT: nameTXT, classTXT: classTXT, innerTXT: innerTXT, stepNumber: stepNumber);
                return null;
            }
        }
        /// <summary>
        /// Specifically clicks on a link based off text.
        /// </summary>
        /// <param name="linkText">The text of the link.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void ClickOnLink(string linkText, int stepNumber = 0)
        {
            try
            {
                browserDriver.FindElement(By.LinkText(linkText)).Click();
            }
            catch (Exception ex)
            {
                CaptureError(ex, innerTXT: linkText, stepNumber: stepNumber);
            }
        }
        /// <summary>
        /// Close a webpage or popup that isn't the current one being focused on.
        /// </summary>
        /// <param name="stepNumber">The step number of the test case</param>
        public void FindAndClosePopup(int stepNumber = 0)
        {
            try
            {
                string originalHandle = browserDriver.CurrentWindowHandle;
                ReadOnlyCollection<string> currentHandles = browserDriver.WindowHandles;
                string popupHandle = currentHandles.Where(s => s != originalHandle).First();
                browserDriver.SwitchTo().Window(popupHandle);
                browserDriver.Close();
                browserDriver.SwitchTo().Window(originalHandle);
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
            }
        }
        /// <summary>
        /// Close a webpage that isn't part of the list of windows passed in.
        /// </summary>
        /// <param name="windowHandles">List of windows that you want to stay open.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void FindAndClosePopup(ReadOnlyCollection<string> windowHandles, int stepNumber = 0)
        {
            try
            {
                string originalHandle = browserDriver.CurrentWindowHandle;
                ReadOnlyCollection<string> currentHandles = browserDriver.WindowHandles;
                string popupHandle = currentHandles.Except(windowHandles).First().ToString();
                browserDriver.SwitchTo().Window(popupHandle);
                browserDriver.Close();
                browserDriver.SwitchTo().Window(originalHandle);
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
            }
        }
        /// <summary>
        /// Returns the value of whether the checkbox is checked or not.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns>The checked status of the checkbox</returns>
        public bool IsCheckBoxChecked(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement control = FindHTMLControl(idTXT, nameTXT, classTXT, innerTXT, tagTXT, flag, index);
                return control.Selected;
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT: idTXT, nameTXT: nameTXT, tagTXT: tagTXT, classTXT: classTXT, stepNumber: stepNumber);
                return false;
            }
        }
        /// <summary>
        /// Tries to make sure that the current window has gotten focus.
        /// </summary>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void GetWindowFocus(int stepNumber = 0)
        {
            try
            {
                ((IJavaScriptExecutor)browserDriver).ExecuteScript("window.focus();");
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
            }
        }
        /// <summary>
        /// Returns the value of whether the checkbox is checked or not.
        /// </summary>
        /// <param name="xpath">The xpath value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        /// <returns></returns>
        public bool IsCheckBoxChecked(string xpath, int stepNumber)
        {
            try
            {
                IWebElement control = FindHTMLControl(xPath: xpath);
                return control.Selected;
            }
            catch (Exception ex)
            {
                CaptureError(ex, stepNumber: stepNumber);
                return false;
            }
        }
        /// <summary>
        /// Specifically moves the mouse over the control and then clicks on it.
        /// </summary>
        /// <param name="xPath">The xpath value of the object</param>
        /// <param name="stepNumber">The step number of the test case.</param>
        public void MoveAndClick(string xPath, int stepNumber = 0)
        {
            try
            {
                IWebElement element = FindHTMLControl(xPath: xPath, stepNumber: stepNumber);
                Actions action = new Actions(browserDriver);
                action.MoveToElement(element).Click().Build().Perform();
            }
            catch (Exception ex)
            {
                CaptureError(ex, xpath: xPath, stepNumber: stepNumber);
            }
        }
        /// <summary>
        /// Specifically moves the mouse over the control and then clicks on it.
        /// </summary>
        /// <param name="idTXT">The ID value of the object</param>
        /// <param name="nameTXT">The name value of the object</param>
        /// <param name="classTXT">The class value of the object</param>
        /// <param name="innerTXT">The inner text value of the object</param>
        /// <param name="tagTXT">The tag value of the object</param>
        /// <param name="flag">This is an enumerated flag that helps determine which identifying values are going to be either set to contains or equals.
        ///                    If help is needed please see Microsofts documentation on enumerated flags. https://msdn.microsoft.com/en-us/library/system.flagsattribute(v=vs.110).aspx </param>
        /// <param name="index">The xpath index value of the object.</param>
        /// <param name="stepNumber">The step number of the test case.</param>

        public void MoveAndClick(string idTXT = null, string nameTXT = null, string classTXT = null, string innerTXT = null, string tagTXT = null, Contains flag = Contains.Text | Contains.ID, int index = 1, int stepNumber = 0)
        {
            try
            {
                IWebElement element = FindHTMLControl(idTXT: idTXT, nameTXT: nameTXT, classTXT: classTXT, innerTXT: innerTXT, tagTXT: tagTXT, flag: flag, stepNumber: stepNumber);
                Actions action = new Actions(browserDriver);
                action.MoveToElement(element).Click().Build().Perform();
            }
            catch (Exception ex)
            {
                CaptureError(ex, idTXT: idTXT, nameTXT: nameTXT, tagTXT: tagTXT, classTXT: classTXT, innerTXT: innerTXT, stepNumber: stepNumber);
            }
        }

        /// <summary>
        /// Switches to the window by name
        /// </summary>
        /// <param name="driver">Firefox driver</param>
        /// <param name="windowTitle">string of window title to switch to</param>
        /// <returns>true if window found</returns>
        public bool SwitchtoWindow(ihi_testlib_selenium.Selenium driver, string windowTitle)
        {
            foreach (string handle in driver.browserDriver.WindowHandles)
            {
                IWebDriver popup = driver.browserDriver.SwitchTo().Window(handle);

                if (popup.Title.Contains(windowTitle))
                {
                    //Window found
                    //System.Threading.Thread.Sleep(5000);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// returns a row value from a table in the UI by tableID
        /// </summary>
        /// <param name="xPath">string of tableID name</param>
        /// <param name="index">index of where in the table to get information (add if indexed at 0 or 1 next time I use)</param>
        /// <param name="stepNumber"></param>
        /// <returns></returns>
        //public string ReturnTableRowValue(By TableElement, int index, ihi_testlib_selenium.Selenium driver)
        //{
        //    try
        //    {
        //        if (driver.browserDriver.FindElement(TableElement).Displayed)
        //        {
        //            IWebElement webElementBody = driver.browserDriver.FindElement(TableElement);
        //            IList<IWebElement> ElementCollectionBody = webElementBody.FindElements(TableElement);
        //            return ElementCollectionBody[index].Text;
        //        }
        //        else
        //        {
        //            return string.Empty;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        return string.Empty;
        //    }
        //}

        public string ReturnTableRowValue(string xPath, int index, int stepNumber = 0)
        {
            try
            {
                IWebElement element = FindHTMLControl(xPath: xPath, stepNumber: stepNumber);
                if (element.Displayed)
                {
                    //IWebElement control = browserdriver.browserDriver.FindElement(By.XPath(xPath));
                    IList<IWebElement> ElementCollectionBody = browserDriver.FindElements(By.XPath(xPath));
                    return ElementCollectionBody[index].Text;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }
        }
     
        /// <summary>
        /// cycles through known windows and closes popup when window name is found.  Returns to previous window.
        /// </summary>
        /// <param name="driver">Firefox driver</param>
        /// <param name="windowName">name of popup window to close</param>
        /// <returns>true if popup found and closed</returns>
        public bool ClosePopUp(ihi_testlib_selenium.Selenium driver, string windowName)
        {
            bool WindowFound = false;
            string currentHandle = driver.browserDriver.CurrentWindowHandle;
            foreach (string handle in driver.browserDriver.WindowHandles)
            {
                IWebDriver popup = driver.browserDriver.SwitchTo().Window(handle);

                if (popup.Title.Contains(windowName))
                {
                    driver.browserDriver.Close();
                    driver.browserDriver.SwitchTo().Window(currentHandle);
                    WindowFound = true;
                    break;
                }
            }

            driver.browserDriver.SwitchTo().Window(currentHandle);
            return WindowFound;
        }

        /// <summary>
        /// reloads page
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public void RefreshPage(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.Navigate().Refresh();
            //new Actions(driver.browserDriver).SendKeys(Keys.F5).SendKeys(Keys.Enter).Perform();
            //driver.browserDriver.FindElement(By.XPath("//body")).SendKeys(Keys.F5);
        }

        /// <summary>
        /// Returns list of web elements with the specified text
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<IWebElement> GetAllElementsWithText(ihi_testlib_selenium.Selenium driver, string text)
        {
            string prototype = string.Format("//*[contains(text(), '{0}')]", text);
            return driver.browserDriver.FindElements(By.XPath(prototype)).ToList();
        }

        /// <summary>
        /// Returns list of web elements with the specified color
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public List<IWebElement> GetAllElementsWithColor(ihi_testlib_selenium.Selenium driver, string color)
        {
            string prototype = string.Format("//td[contains(@style,'color: {0};')]", color);
            return driver.browserDriver.FindElements(By.XPath(prototype)).ToList();
        }

        #endregion



    }
}

