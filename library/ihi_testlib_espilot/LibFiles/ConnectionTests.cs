﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public static class ConnectionTests
    {
        public static void DisconnectForLengthOfTime(int port, string ipAddressOfSwitch, int timeInMiliseconds)
        {
            TimeSpan timeToDisconnect = TimeSpan.FromMilliseconds(timeInMiliseconds);
            NetgearTelnetInfo telnetInfo = new NetgearTelnetInfo(ipAddressOfSwitch, NetgearLoginInfo.Login, NetgearLoginInfo.Password);

            NetgearSwitchCommunication.SetSwitchPortOnOff(false, telnetInfo, port);
            Thread.Sleep(timeToDisconnect);
            NetgearSwitchCommunication.SetSwitchPortOnOff(true, telnetInfo, port);
        }
    }
}
