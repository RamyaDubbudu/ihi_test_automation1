﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Reflection;

namespace ihi_testlib_espilot
{
    public static class RacksPopupPage
    {
        /// <summary>
        /// Returns lists with all racks labels
        /// </summary>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <returns></returns>
        public static List<string> GetAllRacksLabels(string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/")
        {
            string zonesstring = SSH.ReadAllFromDatabaseOpenClose("racks", "label", SSHIPAddress, SSHUsername, SSHPassword);

            List<string> AllLabels = zonesstring.Split('\n').ToList<string>();
            AllLabels = AllLabels.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            return AllLabels;
        }
    }

    public class RackPopupInfo
    {
        public string label { get; set; }
        public string description { get; set; }
        public int zones_id { get; set; }
        public string type { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string rack_status { get; set; }
        public double soc { get; set; }
        public double v_actual { get; set; }
        public double i_actual { get; set; }
        public double t_avg { get; set; }
        public double t_max { get; set; }
        public double t_min { get; set; }
        public double v_cellavg { get; set; }
        public double v_cellmax { get; set; }
        public double v_cellmin { get; set; }
        public double v_max { get; set; }
        public double v_min { get; set; }
        public double i_max { get; set; }
        public double i_min { get; set; }

        public RackPopupInfo (ihi_testlib_selenium.Selenium driver)
        {
            try
            {


                label = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.DetailedInformation));
                description = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.Description));
                zones_id = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.ZoneID)));
                type = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.Type));
                available = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.Available));
                enabled = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.Enabled));
                rack_status = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.Status));
                soc = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.SOC)));
                v_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.RackActualVoltage)));
                i_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.RackActualCurrent)));
                t_avg = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.CellAverageTemp)));
                t_max = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.CellMaximumTemp)));
                t_min = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.CellMinimumTemp)));
                v_cellavg = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.CellAverageVoltage)));
                v_cellmax = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.CellMaximumVoltage)));
                v_cellmin = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.CellMinimumVoltage)));
                v_max = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.RackMaximumVoltage)));
                v_min = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.RackMinimumVoltage)));
                i_max = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.RackMaximumCurrent)));
                i_min = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.RackMinmumCurrent)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(RackPopupInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }
    }
}
