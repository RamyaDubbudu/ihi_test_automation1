﻿/*==============================================================================
Name        : RunData.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : RunData class that will house the data and metadata for running runs
 *            of a Test Case.
==============================================================================*/

using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using Excel;

namespace ihi_testlib_espilot
{
    /// <summary>
    ///     Houses the run data taken from an Excel file.
    /// </summary>
    public class RunData
    {
        /// <summary>
        ///     Constructor for creating run data by path of sequence file
        /// </summary>
        /// <param name="relativepath"></param>
        /// <param name="rowcount"></param>
        public void RunDataSheet(string relativepath, int startIndex = 0)
        {
            int rowcount = 0;
            //find path based on where sequence file is located
            if (relativepath.Contains(".seq"))
                ExcelFileLocation = relativepath.Substring(0, relativepath.IndexOf(".seq")) + " Run.xlsx";
            else
                ExcelFileLocation = relativepath;

            //Create file stream with excel file
            var stream = File.Open(ExcelFileLocation, FileMode.Open, FileAccess.Read, FileShare.Read);
            var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            //Ignores fist row as this contain names
            excelReader.IsFirstRowAsColumnNames = true;
            FullDataSet = excelReader.AsDataSet();
            DataTable = FullDataSet.Tables[0];
            rowcount = DataTable.Rows.Count;
            RunRow = startIndex;
            MetaData = ExtractMetaDataFromDataSet();
            FirstRun = true;
           
        }

        /// <summary>
        ///     Includes all data from the excel file
        /// </summary>
        private DataSet FullDataSet { get; set; }

        /// <summary>
        ///     DataTable that includes all information on the from the excel in DataTable format.  This will generally be
        ///     everything relevant for run tests.
        /// </summary>
        public DataTable DataTable { get; private set; }

        /// <summary>
        ///     Includes the metadata parsed out as a list of RunTestMetaData class objects.
        /// </summary>
        public List<RunTestMetaData> MetaData { get; private set; }

        /// <summary>
        /// Current run row in excel file
        /// </summary>
        public int RunRow { get; set; }

        /// <summary>
        /// Is first run?  Will usually decide if setup needs to be preformed.
        /// </summary>
        public bool FirstRun { get; set; }

        /// <summary> 
        /// Number of rows in run file
        /// </summary>
        public int RowCount
        {
            get { return DataTable.Rows.Count; }
        }

        /// <summary>
        ///     Number of columns of run data including the metadata
        /// </summary>
        public int ColumnCount
        {
            get { return DataTable.Columns.Count; }
        }

        public string ExcelFileLocation { get; private set; }

        private DataTable RunInfoTable
        {
            get { return RemoveMetaDataFromDataTable(RunRow); }
        }

        public bool LastRun
        {
            get { return RunRow == RowCount; }
        }

        private List<RunTestMetaData> ExtractMetaDataFromDataSet()
        {
            var metaData = new List<RunTestMetaData>();

            for (var i = 0; i < RowCount; i++)
            {
                var tempMetadata = new RunTestMetaData();
                var type = tempMetadata.GetType();
                var properties = type.GetProperties();
                foreach (var property in properties)
                {
                    //remove "System.String" and spaces with empty.
                    var formatedProperty = property.ToString().Replace("System.String", "").Replace(" ", "");
                    var containsHeader = DataTable.Columns.Contains(formatedProperty);
                    if (containsHeader)
                    {
                        var row = DataTable.Rows[i];
                        var metaDataValue = row[row.Table.Columns[formatedProperty].Ordinal].ToString();
                        var prop = tempMetadata.GetType()
                            .GetProperty(formatedProperty, BindingFlags.Public | BindingFlags.Instance);
                        if (null != prop && prop.CanWrite)
                        {
                            prop.SetValue(tempMetadata, metaDataValue, null);
                        }
                    }
                }
                metaData.Add(tempMetadata);
            }
            return metaData;
        }

        /// <summary>
        ///     Returns the value in the Excel file based on object's RunRow
        /// </summary>
        /// <param name="columnHeader">Case sensitive header name in Excel</param>
        /// <returns></returns>
        public string RunDataValue(string columnHeader)
        {
            var index = RunRow;
            return RunDataValue(columnHeader, index);
        }

        /// <summary>
        ///     Returns the value in the Excel file
        /// </summary>
        /// <param name="columnHeader">Case sensitive header name in Excel</param>
        /// <param name="index">Index of Excel file's rows starting at 0, not including headers</param>
        /// <returns></returns>
        public string RunDataValue(string columnHeader, int index)
        {
            var dataString = "";
            var data = DataTable.Rows[index];
            if (data.Table.Columns[columnHeader] != null)
            {
                var dataObject = data[data.Table.Columns[columnHeader]];
                if (dataObject != null)
                    dataString = dataObject.ToString();
            }
            return dataString;
        }

        /// <summary>
        ///     Returns a string of the metadata by index (starts at 0) formated with value and header.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string FormatMetaData(int index)
        {
            var formatedMetaData = "";
            var currentMetaData = MetaData[index];
            var properties = currentMetaData.GetType().GetProperties();

            foreach (var property in properties)
            {
                var propertyName = property.Name;
                var propertyValue = property.GetValue(currentMetaData, null);
                if (propertyValue != null)
                {
                    formatedMetaData += string.Format("{0}| ", propertyValue);
                }
            }
            return formatedMetaData;
        }

        /// <summary>
        ///     Returns a string of the metadata based object's current RunRow.
        /// </summary>
        /// <returns></returns>
        public string FormatMetaData()
        {
            return FormatMetaData(RunRow);
        }

        /// <summary>
        ///     Returns a string of the rundata by index (starts at 0) formated with value and header.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string FormatRunDataWithoutMeta(int index)
        {
            var formatedData = "";
            var runInfoTable = RunInfoTable;
            var dataRow = runInfoTable.Rows[index];
            foreach (DataColumn dc in dataRow.Table.Columns)
            {
                var columnHeader = dc.ColumnName;
                var value = dataRow[dataRow.Table.Columns[columnHeader]].ToString();
                formatedData += string.Format("\n\t{0}: {1}", columnHeader, value);
            }
            formatedData = formatedData + "\n";
            return formatedData;
        }

        public string FormatRunDataWithoutMeta()
        {
            return FormatRunDataWithoutMeta(RunRow);
        }

        /// <summary>
        ///     Formats a string with metadata for the report and will include the Run data if requested
        /// </summary>
        /// <param name="includeRunInfo"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public string FormatRunData(bool includeRunInfo, int index)
        {
            var returnValue = FormatMetaData(index);
            if (includeRunInfo)
                returnValue += FormatRunDataWithoutMeta(index);
            return returnValue;
        }

        /// <summary>
        /// Formats a string with metadata for the report and will include the Run data if requested.  RunRow is provided by property
        /// </summary>
        /// <param name="includeRunInfo"></param>
        /// <returns></returns>
        public string FormatRunData(bool includeRunInfo)
        {
            return FormatRunData(includeRunInfo, RunRow);
        }

        /// <summary>
        ///     Creates a new datatable with the metadata columns removed
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private DataTable RemoveMetaDataFromDataTable(int index)
        {
            var copyDataTable = FullDataSet.Tables[0];
            var tempMeta = new RunTestMetaData();
            var properties = tempMeta.GetType().GetProperties();

            foreach (var property in properties)
            {
                var propertyName = property.Name;
                if (copyDataTable.Columns.Contains(propertyName))
                    copyDataTable.Columns.Remove(propertyName);
            }
            return copyDataTable;
        }
    }

    /// <summary>
    ///     Data that will be included as metadata.  The property names must match the header in the excel file.  This can be
    ///     expanded for all metadata and
    ///     will be included automatically if the Excel header matches.
    /// </summary>
    public class RunTestMetaData
    {
        public string runID { get; set; }
        public string runDescription { get; set; }
        public string runTest { get; set; }
    }
}