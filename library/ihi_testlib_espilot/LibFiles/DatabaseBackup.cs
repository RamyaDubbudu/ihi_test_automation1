﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using Renci.SshNet.Messages.Authentication;

namespace ihi_testlib_espilot
{
    public class DatabaseBackup
    {
        public readonly string FailedToBackupMessage = "Unable to backup databases on database test.  Test will abort";
        private SSHAndStartDECSDirectory _sshInfo;

        public DatabaseBackup(SSHAndStartDECSDirectory sshInfo)
        {
            this._sshInfo = sshInfo;
        }

        //public DatabaseBackup(string SSHIPaddress, string SSHUsername, string SSHPassword, string DECSLocation)
        //{
        //    this._sshInfo = new SSHAndStartDECSDirectory(SSHIPaddress, SSHUsername, SSHPassword, DECSLocation);
        //}

        public bool SaveBackupOfDatabases()
        {
            try
            {
                var ssh = new SshClient(_sshInfo.SSHIPAddress, _sshInfo.SSHUsername, _sshInfo.SSHPassword);
                var backupCommand = "BACKUP";
                QueryInterface.CommandThroughQuery(backupCommand, ssh, _sshInfo.SSHIPAddress,
                    _sshInfo.SSHStartDECSDirectory);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool RestoreBackupOfDatabases()
        {
            try
            {
                var ssh = new SshClient(_sshInfo.SSHIPAddress, _sshInfo.SSHUsername, _sshInfo.SSHPassword);
                var backupCommand = "RESTORE";
                QueryInterface.CommandThroughQuery(backupCommand, ssh, _sshInfo.SSHIPAddress,
                    _sshInfo.SSHStartDECSDirectory);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
