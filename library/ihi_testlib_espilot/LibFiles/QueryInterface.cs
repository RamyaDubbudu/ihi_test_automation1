﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;

namespace ihi_testlib_espilot
{
    static class QueryInterface
    {
        public static string CommandThroughQuery(string command, SshClient ssh, string ipAddress, string decsLocation, bool removeLineFeed = true, string databaseName = "decs_dc.db")
        {
            ssh = new SshClient(SSH.connectionInfo);
            if (!ssh.IsConnected)
                ssh.Connect();
            var formattedForSSH = string.Format("sqlite3 {0}{2} \"{1}\"", decsLocation, command, databaseName);
            var sqlCommand = string.Empty;
            if (SSH.ExecutingOnDocker())
            {
                sqlCommand = string.Format(string.Format(DockerCommands.SSHCommands.SqliteQuery), formattedForSSH);
            }
            else
            {
                sqlCommand = formattedForSSH;
            }
            var response = ssh.RunCommand(sqlCommand).Result;
            ssh.Disconnect();
            if (removeLineFeed)
                return response.Replace("\n", "");
            return response;
        }
        
    }
}
