﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Methods and properties used with regards to the tree
    /// </summary>
    public static class TreePage
    {
        /// <summary>
        /// Gets all the labels from all the assets.  These labels are what are listed in the tree.
        /// </summary>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <returns></returns>
        public static List<string> getAllLablesFromDatabase(string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/")
        {
            string zonesstring = SSH.ReadAllFromDatabaseOpenClose("zones", "label", SSHIPAddress, SSHUsername, SSHPassword);
            string rackstring = SSH.ReadAllFromDatabaseOpenClose("racks", "label", SSHIPAddress, SSHUsername, SSHPassword);
            string invertersstring = SSH.ReadAllFromDatabaseOpenClose("inverters", "label", SSHIPAddress, SSHUsername, SSHPassword);
            string coolingstring = SSH.ReadAllFromDatabaseOpenClose("cooling", "label", SSHIPAddress, SSHUsername, SSHPassword);

            string allassets = rackstring + zonesstring + invertersstring + coolingstring;

            List<string> AllLabels = allassets.Split('\n').ToList<string>();
            AllLabels = AllLabels.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            return AllLabels;
        }

        /// <summary>
        ///  Double clicks on tree label based on label name
        /// </summary>
        /// <param name="driver">Firefox dirver</param>
        /// <param name="labelName">Name of label which to click on</param>
        /// <param name="singleClick"> This will make the action only preform a single click (default now 2015/09/04)</param>
        public static void DoubleClickOnLabel(ihi_testlib_selenium.Selenium driver, string labelName, bool singleClick = true)
        {
            int failcount = 0;
            string prototype = "//*[contains(text(), '{0}') and contains(@class,'classIHI_UI_TREE__TABLE')]";
            //Tries to click the popup 10 times
            labelName = labelName.Trim(' ');
            while (failcount < 10)
            {
                try
                {
                    driver.browserDriver.SwitchTo().DefaultContent();
                    string xpath = string.Format(prototype, labelName);
                    Actions builder = new Actions(driver.browserDriver);

                    //Will select only element with exact text in name
                    var assetList = driver.browserDriver.FindElements(By.XPath(xpath));
                    IWebElement asset = null;
                    foreach (var element in assetList)
                    {
                        if (element.Text == labelName)
                        {
                            asset = element;
                        }
                        else if (element.Text.StartsWith(labelName))
                        {
                            asset = element;
                        }
                    }
                    if (singleClick)
                        asset.Click();
                    else
                        builder.DoubleClick(asset).Build().Perform();
                    break;
                }
                catch (Exception e)
                {
                    failcount++;
                    continue;
                }
                finally
                {
                    //Displays message if fails 10 times
                    if (failcount == 10)
                    {
                        Console.WriteLine("Failed 10 times: " + labelName);

                    }
                }
            }
        }

        public static IList<IWebElement> GetAssetsByType(Assets type, ihi_testlib_selenium.Selenium driver)
        {
            IList<IWebElement> result = null;

            switch (type)
            {
                case Assets.zones:
                    result = driver.browserDriver.FindElements(By.CssSelector("table.classIHI_UI_TREE__TABLE_ZONE"));
                    break;
                case Assets.racks:
                    result = driver.browserDriver.FindElements(By.CssSelector("table.classIHI_UI_TREE__TABLE_BATTERY"));
                    break;
                case Assets.inverters:
                    result = driver.browserDriver.FindElements(By.CssSelector("table.classIHI_UI_TREE__TABLE_INVERTER"));
                    break;
                case Assets.cooling:
                    result = driver.browserDriver.FindElements(By.CssSelector("table.classIHI_UI_TREE__TABLE_COOLER"));
                    break;
            }
            return result;
        }

        public static bool LaunchAssetById(int assetIndex, Assets type, ref ihi_testlib_selenium.Selenium IWebDriver, ref string reportText, ref bool result)
        {
            var assetList = TreePage.GetAssetsByType(type, IWebDriver);
            int loops = 0;

            while ((assetList.Count<=0) && (loops<10))
            {
                assetList = TreePage.GetAssetsByType(type, IWebDriver);
                System.Threading.Thread.Sleep(1000);
                loops++;
            }

            if ((assetIndex <= assetList.Count) && (assetList.Count > 0))
            {
                assetList[assetIndex].Click();
            }
            else
            {
                result = false;
                reportText += string.Format("Asset ID outside of bounds of identified assets on popup tree. From run file: {0}. Assets found on tree: {1}.", (assetIndex + 1), assetList.Count);
                return false;
            }

            if (!IWebDriver.SwitchtoWindow(IWebDriver, "ES/Pilot - Detail Information"))
            {
                result = false;
                reportText += "Failed to open asset window";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Creates a number of assets and assigns them to zones randomly
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <param name="zonesToAdd"></param>
        /// <param name="racksToAdd"></param>
        /// <param name="invertersToAdd"></param>
        /// <param name="coolingToAdd"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDatabaseDirectory"></param>
        public static void CreateAndRandomAssets(ControlAssetInfo assetInfo, string zonesToAdd, string racksToAdd, string invertersToAdd, string coolingToAdd, SSHAndDatabaseDirectory connectionInfo)
        {
            ZonesPage.CreateMoreZones(int.Parse(zonesToAdd), connectionInfo);
            int updatedZoneNumber = assetInfo.zoneList.Count + int.Parse(zonesToAdd);

            //racks
            Random rnd = new Random();
            for (int i = 0; i < int.Parse(racksToAdd); i++)
            {
                int zone = rnd.Next(1, updatedZoneNumber + 1);
                RacksPage.CreateMoreRacks(1, connectionInfo, zone);
            }

            //inverters
            for (int i = 0; i < int.Parse(invertersToAdd); i++)
            {
                int zone = rnd.Next(1, updatedZoneNumber + 1);
                InvertersPage.CreateMoreInverters(1, connectionInfo, zone);
            }

            //cooling
            for (int i = 0; i < int.Parse(coolingToAdd); i++)
            {
                int zone = rnd.Next(1, updatedZoneNumber + 1);
                EnvironmentPage.CreateMoreCoolingUnits(1, connectionInfo, zone);
            }
        }

        /// <summary>
        /// Check if asset can be located on page by name
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="assetName"></param>
        /// <returns></returns>
        public static bool CheckIfAssetOnList(ihi_testlib_selenium.Selenium driver, string assetName)
        {
            string prototype = "//*[contains(text(), '{0}')]";
            try
            {
                driver.browserDriver.SwitchTo().DefaultContent();
               // string xpath = string.Format(prototype, assetName);
               // By assetBy = By.XPath(xpath);
                if(driver.IsElementVisible(xPath: string.Format(prototype, assetName)))
                    return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return false;
        }

        /// <summary>
        /// Checks tree to see if it contains a zone in specified mode
        /// </summary>
        /// <param name="driver">webdriver</param>
        /// <param name="zoneName">zone name to check</param>
        /// <param name="mode">mode expected</param>
        /// <returns>true if found</returns>
        public static bool CheckModeLabel(ihi_testlib_selenium.Selenium driver, string mode)
        {
            string prototype = "//*[contains(text(), '{0}')]"; ;
            try
            {
                driver.browserDriver.SwitchTo().DefaultContent();
                //string xpath = string.Format(prototype, mode);
                //By assetBy = By.XPath(xpath); **** n/a anymore
                if (driver.IsElementVisible(xPath: string.Format(prototype, mode)))
                    return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return false;
        }

        public static string CheckColorOfRackItem(ihi_testlib_selenium.Selenium driver, string labelName)
        {
            int failcount = 0;
            string prototype = "//*[contains(text(), '{0}')]";
            string humanReadableColor = "";
            //Tries to click the popup 10 times
            while (failcount < 10)
            {
                try
                {
                    labelName = labelName.Trim(' ');
                    driver.browserDriver.SwitchTo().DefaultContent();
                    string xpath = string.Format(prototype, labelName);
                    IWebElement rack = driver.browserDriver.FindElement(By.XPath(xpath));

                    string uiColor = rack.GetCssValue("color");

                    humanReadableColor = StringDictionaries.UITextColors.ContainsValue(uiColor) ? StringDictionaries.UITextColors.FirstOrDefault(x => x.Value == uiColor).Key : "Color not found in dictionary.";

                    return humanReadableColor;
                }
                catch (Exception e)
                {
                    failcount++;
                    humanReadableColor = "Failed to located " + labelName;
                    continue;
                }
                finally
                {
                    //Displays message if fails 10 times
                    if (failcount == 10)
                    {
                        Console.WriteLine("Failed 10 times: " + labelName);
                    }
                }
            }
            return humanReadableColor;
        }
    }
}
