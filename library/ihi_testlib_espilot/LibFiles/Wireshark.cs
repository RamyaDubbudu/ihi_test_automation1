﻿using System.Diagnostics;
using System.IO;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Pipes into a remote Linux system with tcpdump installed and pipes results to Wireshark
    /// </summary>
    public class WiresharkPipe
    {
        private static string _wiresharkLocation = @"C:\Program Files\Wireshark\Wireshark.exe";
        private string _plinkLocation;
        private SSHInfo _ipInfo;
        private Process _wiresharkProcess;
        private string Arguements { get; set; }

        public Process WiresharkProcess
        {
            get { return _wiresharkProcess; }
            private set { _wiresharkProcess = value; }
        }

        /// <summary>
        /// Constructor for Wireshark Pipe with remote system information
        /// </summary>
        /// <param name="ipInfo"></param>
        public WiresharkPipe(SSHInfo ipInfo)
        {
            _plinkLocation = @"C:\3rdPartyPrograms";
            _ipInfo = ipInfo;
        }

        /// <summary>
        /// Constructor for Wireshark Pip with remote system information and location of pLink which opens the SSH session.
        /// </summary>
        /// <param name="ipInfo"></param>
        /// <param name="plinkLocation"></param>
        public WiresharkPipe(SSHInfo ipInfo, string plinkLocation)
        {
            _plinkLocation = plinkLocation;
            _ipInfo = ipInfo;
        }

        public void StartCapture()
        {
            this.Arguements =
                string.Format(
                    @"-ssh -pw {0} {1}@{2} ""echo {0} | sudo -S tcpdump -ni eth0 -s 0 -w - not port 22"" | ""{3}"" -k -i -",
                    _ipInfo.SSHPassword, _ipInfo.SSHUsername, _ipInfo.SSHIPAddress, _wiresharkLocation);

            this.LaunchWireshark();
        }

        /// <summary>
        /// Starts an indefinite capture and exports to output file
        /// </summary>
        /// <param name="outputFile">name and location of output file</param>
        public void StartAndSaveCapture(string outputFile)
        {
            this.Arguements =
                string.Format(
                     @"-ssh -pw {0} {1}@{2} ""echo {0} | sudo -S tcpdump -ni eth0 -s 0 -w - not port 22"" | ""{3}"" -k -i - -w ""{4}""",
                     _ipInfo.SSHPassword, _ipInfo.SSHUsername, _ipInfo.SSHIPAddress, _wiresharkLocation, outputFile);
            this.LaunchWireshark();
        }

        /// <summary>
        /// Starts a capture and exports data to output file.  Wireshark will stop when the capture option argument has been reached
        /// </summary>
        /// <param name="outputFile"></param>
        /// <param name="stopOptions"></param>
        /// <param name="stopOptionArugment">duration: # of seconds; filesize: # of KB; files: # of files </param>
        public void StartAndSaveCapture(string outputFile, CaptureStopOptions stopOptions, int stopOptionArugment)
        {
            var stopingAt = stopOptions.ToString();
            this.Arguements =
                string.Format(
                     @"-ssh -pw {0} {1}@{2} ""echo {0} | sudo -S tcpdump -ni eth0 -s 0 -w - not port 22"" | ""{3}"" -k -i - -w ""{4}"" -a {5}:{6}",
                     _ipInfo.SSHPassword, _ipInfo.SSHUsername, _ipInfo.SSHIPAddress, _wiresharkLocation, outputFile, stopingAt, stopOptionArugment);
            this.LaunchWireshark();
        }

        /// <summary>
        /// Starts an indefinite capture and exports data in a ring buffer output, creating new output files after conditions are met.
        /// </summary>
        /// <param name="outputFile"></param>
        /// <param name="ringBufferOptions"></param>
        /// <param name="ringBufferArgument">duration: # of seconds; filesize: # of KB; files: # of files</param>
        public void StartAndSaveCapture(string outputFile, RingBufferOptions ringBufferOptions, int ringBufferArgument)
        {
            var buffer = ringBufferOptions.ToString();
            this.Arguements =
                string.Format(
                     @"-ssh -pw {0} {1}@{2} ""echo {0} | sudo -S tcpdump -ni eth0 -s 0 -w - not port 22"" | ""{3}"" -k -i - -w ""{4}"" -b {5}:{6}",
                     _ipInfo.SSHPassword, _ipInfo.SSHUsername, _ipInfo.SSHIPAddress, _wiresharkLocation, outputFile, buffer, ringBufferArgument);
            this.LaunchWireshark();
        }

        private void LaunchWireshark()
        {
            var combinedPlink = Path.Combine(_plinkLocation, "plink.exe");
            //var _wiresharkProcess = Process.Start(combinedPlink, Arguements);
            var startInfo = new ProcessStartInfo
            {
                FileName = "cmd.exe",
                RedirectStandardInput = true,
                RedirectStandardOutput = false,
                UseShellExecute = false,
                CreateNoWindow = true,
            };
            _wiresharkProcess = new Process { StartInfo = startInfo };
            _wiresharkProcess.Start();
            var formattedRun = combinedPlink + " " + Arguements;
            _wiresharkProcess.StandardInput.WriteLine(formattedRun);
        }

        /// <summary>
        /// Select options for ring buffer capture.
        /// </summary>
        public enum RingBufferOptions
        {
            duration,
            filesize,
            files
        }

        /// <summary>
        /// Select options for normal capture.
        /// </summary>
        public enum CaptureStopOptions
        {
            duration,
            filesize,
            files

        }
    }
}
