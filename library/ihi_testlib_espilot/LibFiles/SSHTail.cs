﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.IO;
using System.Runtime.CompilerServices;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Class to check last values on a file through ssh Linux "tail" command
    /// </summary>
    class SSHTail
    {
        /// <summary>
        /// ssh client which to connect to
        /// </summary>
        private SshClient _sshClient;
        /// <summary>
        /// sheelStream which is monitoring the information coming from the shell
        /// </summary>
        private ShellStream _shellStream;
        //Command being executed to tail a file.
        private readonly string command = "tail -f {0}";
        //EventHandler that is called when new data is received.
        private string buildTailText;
        public string inputFilePath{ get; set; }
        public string outputFilePath { get; set; }


        /// <summary>
        /// Constructor for SSHtail that creates client and sets input (from Linux) and output (to Windows location) for info
        /// </summary>
        /// <param name="sshInfo"></param>
        /// <param name="inputFilePath"></param>
        /// <param name="outputFilePath"></param>
        public SSHTail(SSHAndDatabaseDirectory sshInfo, string inputFilePath, string outputFilePath)
        {
            _sshClient = new SshClient(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
            this.inputFilePath = inputFilePath;
            this.outputFilePath = outputFilePath;
        }

        /// <summary>
        /// Starts the process of recording information
        /// </summary>
        public void TailFile()
        {
            _sshClient.Connect();

            _shellStream = _sshClient.CreateShellStream("Tail", 0, 0, 0, 0, 1024);

            //Triggers this delegate when data is received on the shellstream event
            _shellStream.DataReceived += delegate
            {
                buildTailText += _shellStream.Read();
                WriteTextAsync(buildTailText);
            };

            _shellStream.WriteLine(string.Format(command, inputFilePath));
        }

        /// <summary>
        /// Stops tailing the file and closes ssh connection
        /// </summary>
        public void StopTailingFile()
        {
            _shellStream.Close();
            _sshClient.Disconnect();
        }

        private async void WriteTextAsync(string text)
        {
            // Write the text asynchronously to a new file named "WriteTextAsync.txt".
            StreamWriter outputFile = new StreamWriter(outputFilePath);
            await outputFile.WriteAsync(text);
        }
    }
}