﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;
using System.Reflection;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Class that houses methods used on the Zones tab in the Control tab page
    /// </summary>
    public class ZonesPage
    {
        /// <summary>
        /// Returns the info on the Zones tab table by row index
        /// </summary>
        /// <param name="index">row index, starts at 1</param>
        /// <param name="driver"></param>
        /// <returns>ZoneInfo class with information on row</returns>
        public static ZoneInfo returnZoneInfo(int index, ihi_testlib_selenium.Selenium driver)
        {
            //xpaths for all tables are formated in this way with id
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Zones.ZoneTableString);
            ZoneInfo zoneInfo = new ZoneInfo();
            string currentColor = "";

            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", GlobalElementsControl.Zones.ZoneTableString, index);
                    string elementString = driver.ReadControlsInnerText(xPath: xpath); 
                    int element;
                    int.TryParse(elementString, out element);
                    zoneInfo.zoneID = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    zoneInfo.description = elementString;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    zoneInfo.available = elementString;
                    //currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color"); // driver.browserDriver.FindElement(By.XPath(xpath), 1).GetCssValue("color");
                    //zoneInfo.available_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    zoneInfo.enabled = elementString;
                    //currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    //zoneInfo.enabled_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    zoneInfo.status = elementString;
                    //currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    //zoneInfo.status_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[6]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double elementDouble;
                    double.TryParse(elementString, out elementDouble);
                    zoneInfo.SOC = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[7]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    zoneInfo.dcVoltage = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[8]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    zoneInfo.kVADemand = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[9]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    zoneInfo.kVAActual = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[10]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    zoneInfo.kVARActual = element;

                    //Added 2015/9/9 ambient temp
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[11]", GlobalElementsControl.Zones.ZoneTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    zoneInfo.ambient_temp = element;

                    return zoneInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (StaleElementReferenceException)
            {
                Console.WriteLine("Stale element found, retrying");
                return returnZoneInfo(index, driver);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        public static bool CheckAndSetAllZonesStatus(string statusCheck, ihi_testlib_selenium.Selenium driver)
        {
            bool setStatus = false;
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.ZonesTab));
            System.Threading.Thread.Sleep(2000);
            //Enable all Zones
            ZoneInfo zInfo = ZonesPage.returnZoneInfo(1, driver);
            if (zInfo.enabled != statusCheck)
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Zones.ZoneEnableButton, zInfo.description));
                driver.WaitForElement(xpath: string.Format(GlobalElementsControl.Zones.ZoneEnabledText, statusCheck));
                driver.Assertion_IsElementVisible(xPath: string.Format(GlobalElementsControl.Zones.ZoneEnabledText, statusCheck));
                setStatus = true;
            }
            else setStatus = true;
            
            return setStatus;
        }

        /// <summary>
        /// Gets all zones information from zone control page
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>List with all Zone information</returns>
        public static List<ZoneInfo> GetAllZoneInfo(ihi_testlib_selenium.Selenium driver)
        {
            List<ZoneInfo> zoneList = new List<ZoneInfo>();
            int countZone = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive)))
            {
                countZone = GetNumberOfZoneRowsOnPage(driver);
                for (int i = 1; i <= countZone; i++)
                {
                    zoneList.Add(returnZoneInfo(i, driver));
                }
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive));
            }
            countZone = GetNumberOfZoneRowsOnPage(driver);
            for (int i = 1; i <= countZone; i++)
            {
                zoneList.Add(returnZoneInfo(i, driver));
            }
            GoToFirstPage(driver);
            return zoneList;
        }

        /// <summary>
        /// Gets Number of zones on page
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>number of zones located on currentpage</returns>
        static int GetNumberOfZoneRowsOnPage(ihi_testlib_selenium.Selenium driver)
        {
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Zones.ZoneTableString);

            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    IWebElement webElementBody = driver.browserDriver.FindElement(By.XPath(xpath));
                    IList<IWebElement> ElementCollectionBody = webElementBody.FindElements(By.XPath(xpath));
                    //Makes sure only entry doesn't say list is empty
                    if (ElementCollectionBody.Count == 1 && ElementCollectionBody[0].Text == "There are no zones!")
                        return 0;

                    return ElementCollectionBody.Count;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        /// <summary>
        /// Returns to first page
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void GoToFirstPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Zones.AllTabsPreviousIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Zones.AllTabsPreviousIsActive));
            }
        }

        /// <summary>
        /// Goes to Last page
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void GoToLastPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive));
            }
        }

        /// <summary>
        /// Compare Zones Database, ZMQ query, and UI to make sure all report same values where applicable.
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <param name="IPAddress">Linux IP address</param>
        /// <param name="userName">Linux username</param>
        /// <param name="password">Linux password</param>
        /// <param name="databaseLocation">Location of decs database</param>
        /// <returns></returns>
        public static List<ControlErrorInfo> CompareZonePageToZMQAndDatabase(ihi_testlib_selenium.Selenium driver, string IPAddress, string userName, string password, string databaseLocation = "/var/run/ihi-decs/")
        {
            List<ControlErrorInfo> errorList = new List<ControlErrorInfo>();
            string temp = "";
            string table = "zones";
            string column = "";
            string ID = "";

            //order by zone id for both lists to make order reported by ZMQ irrelevant
            List<ZoneTable> zoneListZMQ = IHI_ZMQ.UI__GetData_ZoneList(IPAddress);
            zoneListZMQ = zoneListZMQ.OrderBy(o => o.zones_id).ToList();

            List<ZoneInfo> zoneListUI = GetAllZoneInfo(driver);
            zoneListUI = zoneListUI.OrderBy(o => o.zoneID).ToList();

            for (int i = 0; i < zoneListUI.Count; i++)
            {
                int zoneID = zoneListUI[i].zoneID;
                ZoneInfo zoneDatabase = ReadZoneInfoFromDatabase(zoneID, IPAddress, userName, password, databaseLocation);
                ID = table + "_" + zoneID.ToString();
                //zoneID does not match
                column = "zone_id";
                if ((zoneListZMQ[i].zones_id != zoneListUI[i].zoneID) || (zoneListUI[i].zoneID != zoneDatabase.zoneID))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].zoneID.ToString(), zoneListZMQ[i].zones_id.ToString(), zoneDatabase.zoneID.ToString()));
                }

                //description does not match
                column = "description";
                if ((zoneListZMQ[i].label != zoneListUI[i].description) || (zoneListUI[i].description != zoneDatabase.description))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].description.ToString(), zoneListZMQ[i].label.ToString(), zoneDatabase.description.ToString()));
                }

                //available does not match available
                column = "available";
                temp = StringDictionaries.ConvertDatabaseString(table, column, zoneListZMQ[i].available.ToString());
                if ((temp != zoneListUI[i].available) || (zoneListUI[i].available != zoneDatabase.available))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].available.ToString(), temp, zoneDatabase.available.ToString()));
                }

                //enabled does not match
                column = "enabled";
                temp = StringDictionaries.ConvertDatabaseString(table, column, zoneListZMQ[i].enabled.ToString());
                if ((temp != zoneListUI[i].enabled) || (zoneListUI[i].enabled != zoneDatabase.enabled))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].enabled.ToString(), temp, zoneDatabase.enabled.ToString()));
                }

                //status does not match
                column = "state_actual";
                temp = StringDictionaries.ConvertDatabaseString(table, column, zoneListZMQ[i].state_actual.ToString());
                if ((temp != zoneListUI[i].status) || (zoneListUI[i].status != zoneDatabase.status))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].status.ToString(), temp, zoneDatabase.status.ToString()));
                }

                //SOC does not match
                column = "soc";
                if ((zoneListZMQ[i].soc != zoneListUI[i].SOC) || (zoneListUI[i].SOC != zoneDatabase.SOC))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].SOC.ToString(), zoneListZMQ[i].soc.ToString(), zoneDatabase.SOC.ToString()));
                }

                //dcVoltage does not match
                column = "dc_voltage";
                if ((zoneListZMQ[i].dc_voltage != zoneListUI[i].dcVoltage) || (zoneListUI[i].dcVoltage != zoneDatabase.dcVoltage))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].dcVoltage.ToString(), zoneListZMQ[i].dc_voltage.ToString(), zoneDatabase.dcVoltage.ToString()));
                }

                //kVADemand does not match
                column = "p_demand";
                if ((zoneListZMQ[i].p_demand != zoneListUI[i].kVADemand) || (zoneListUI[i].kVADemand != zoneDatabase.kVADemand))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].kVADemand.ToString(), zoneListZMQ[i].p_demand.ToString(), zoneDatabase.kVADemand.ToString()));
                }

                //kVAActual does not match
                column = "p_actual";
                if ((zoneListZMQ[i].p_actual != zoneListUI[i].kVAActual) || (zoneListUI[i].kVAActual != zoneDatabase.kVAActual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].kVAActual.ToString(), zoneListZMQ[i].p_actual.ToString(), zoneDatabase.kVAActual.ToString()));
                }

                //kVARDemand does not match
                column = "ambient_temp";
                if ((zoneListZMQ[i].ambient_temp != zoneListUI[i].ambient_temp) || (zoneListUI[i].ambient_temp != zoneDatabase.ambient_temp))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].ambient_temp.ToString(), zoneListZMQ[i].q_demand.ToString(), zoneDatabase.ambient_temp.ToString()));
                }

                //kVARActual does not match
                column = "q_actual";
                if ((zoneListZMQ[i].q_actual != zoneListUI[i].kVARActual) || (zoneListUI[i].kVARActual != zoneDatabase.kVARActual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, zoneListUI[i].kVARActual.ToString(), zoneListZMQ[i].q_actual.ToString(), zoneDatabase.kVARActual.ToString()));
                }
            }

            return errorList;
        }

        public static ZoneInfo ReadZoneInfoFromDatabase(int zoneID, string IPAddress, string userName, string password, 
            string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            ZoneInfo zoneDatabase = new ZoneInfo();
            string table = "zones";
            string format = "";
            string column = "";
            string temp = "";

            //zoneID
            zoneDatabase.zoneID = zoneID;

            SshClient ssh = new SshClient(IPAddress, userName, password);
            ssh.Connect();

            //description
            column = "label";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.description = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //available
            column = "available";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.available = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //enabled
            column = "enabled";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.enabled = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //status
            column = "state_actual";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.status = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //SOC
            column = "soc";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.SOC = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //dcVoltage
            column = "dc_voltage";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.dcVoltage = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //kVADemand
            column = "p_demand";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.kVADemand = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //kVAActual
            column = "p_actual";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.kVAActual = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //kVARDemand
            column = "ambient_temp";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.ambient_temp = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //kVARActual
            column = "q_actual";
            format = string.Format("SELECT {0} from {1} where zones_id='{2}'", column, table, zoneID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            zoneDatabase.kVARActual = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            return zoneDatabase;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberToCreate">number of zones to make</param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static void CreateMoreZones(int numberToCreate, SSHAndDatabaseDirectory connectionInfo, string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(connectionInfo.SSHIPAddress, connectionInfo.SSHUsername, connectionInfo.SSHPassword);
            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            string table = "zones";

            List<ZoneTable> zoneListZMQ = IHI_ZMQ.UI__GetData_ZoneList(connectionInfo.SSHIPAddress);

            //find min and max values to know where to copy from and where to put new entry
            int maxID = zoneListZMQ.Max(t => t.zones_id);
            int minID = zoneListZMQ.Min(t => t.zones_id);

            ZoneTable zoneTable = new ZoneTable();

            List<string> propertyNames = zoneTable.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }


            for (int i = 0; i < numberToCreate; i++)
            {
                string prototypeSelect = (++maxID) + ", ";
                foreach (string item in propertyNames)
                {
                    if (item == firstItem)
                        continue;

                    if (item != lastItem)
                        prototypeSelect += item + (", ");
                    else
                        prototypeSelect += item + " ";
                }
                string sqlStatement = string.Format("INSERT into {0}{1}SELECT {2}from {0} where {3}={4}", table,
                    prototypeInto, prototypeSelect, firstItem, minID);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
            }
            ssh.Disconnect();
        }

        /// <summary>
        /// Takes in a list of zone tables (usually from zmq), clears the database and loads in only these values.  Useful when making tests non-descructive
        /// </summary>
        /// <param name="zoneList"></param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static void UpdateDatabaseFromZonesTableList(List<ZoneTable> zoneList, string IPAddress, string userName, 
            string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(IPAddress, userName, password);

            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            string table = "zones";

            SSH.ClearSQLTable(databaseLocation, "decs_dc.db", table, IPAddress, userName, password);

            ZoneTable zoneTableTemp = new ZoneTable();
            List<string> propertyNames = zoneTableTemp.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            //the following code is formating the sql insert into select statement
            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }

            foreach (ZoneTable zoneTable in zoneList)
            {
                List<string> objectStrings = zoneTable.GetPropertyValues();
                string last = objectStrings.Last();

                string prototypeSelect = " (";
                int findLastLoop = 1;
                foreach (string value in objectStrings)
                {
                    if (!(findLastLoop == objectStrings.Count))
                        prototypeSelect += value + (", ");
                    else
                        prototypeSelect += value + (") ");
                    findLastLoop++;
                }
                string sqlStatement = string.Format("INSERT INTO {0}{1} VALUES {2}", table, prototypeInto, prototypeSelect);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, IPAddress, decsLocation);
            }
            ssh.Disconnect();
        }

        /// <summary>
        /// Goes through pages and disables all assets. Returns a list of values that did not change by ID.
        /// </summary>
        /// <param name="timeout">Length allowed for change</param>
        /// <param name="driver">Firefox driver</param>
        /// <returns></returns>
        public static List<int> EnableAllZones(int timeout, ihi_testlib_selenium.Selenium driver)
        {
            int page = 0;
            int countAsset = 0;
            bool success = false;
            int elapsed = 0;
            List<int> wrongValue = new List<int>();

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive)))
            {
                page++;
                countAsset = GetNumberOfZoneRowsOnPage(driver);
                for (int i = 1; i <= countAsset; i++)
                {
                    success = false;
                    elapsed = 0;
                    ControlPage.ClickEnableButton(i, "ZONES", driver);

                    while (elapsed < timeout)
                    {
                        var assetInfo = returnZoneInfo(i, driver);

                        //ensures that were able to get information first
                        if ((assetInfo != null) && ("Enabled" == assetInfo.enabled))
                        {
                            success = true;
                            break;
                        }
                        elapsed += 1000;
                        System.Threading.Thread.Sleep(1000);
                    }

                    if (success == false)
                    {
                        //adds zone id to list of values that did not change
                        wrongValue.Add(returnZoneInfo(i, driver).zoneID);
                    }
                }

                //checks that still on current page, returns -1 if not
                int uiPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Zones.CurrentPage)));
                if (uiPage != page)
                    wrongValue.Add(-1);
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive));
            }

            page++;
            //Last page
            countAsset = GetNumberOfZoneRowsOnPage(driver);
            for (int i = 1; i <= countAsset; i++)
            {
                success = false;
                elapsed = 0;
                ControlPage.ClickEnableButton(i, "ZONE", driver);

                while (elapsed < timeout)
                {
                    var assetInfo = returnZoneInfo(i, driver);

                    if ((assetInfo != null) && ("Enabled" == assetInfo.enabled))
                    {
                        success = true;
                        break;
                    }
                    elapsed += 1000;
                    System.Threading.Thread.Sleep(1000);
                }

                //if value is not enabled after timeout
                if (success == false)
                {
                    //adds zone id to list of values that did not change
                    wrongValue.Add(returnZoneInfo(i, driver).zoneID);
                }
            }
            int lastUIPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Zones.CurrentPage)));
            if (lastUIPage != page)
                wrongValue.Add(-1);

            GoToFirstPage(driver);
            return wrongValue;
        }

        /// <summary>
        /// Goes through pages and disables all assets. Returns a list of values that did not change by ID.
        /// </summary>
        /// <param name="timeout">Length allowed for change</param>
        /// <param name="driver">Firefox driver</param>
        /// <returns></returns>
        public static List<int> DisableAllZones(int timeout, ihi_testlib_selenium.Selenium driver)
        {
            int page = 0;
            int countAsset = 0;
            bool success = false;
            int elapsed = 0;
            List<int> wrongValue = new List<int>();

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive)))
            {
                page++;
                countAsset = GetNumberOfZoneRowsOnPage(driver);
                for (int i = 1; i <= countAsset; i++)
                {
                    success = false;
                    elapsed = 0;
                    ControlPage.ClickDisableButton(i, "ZONE", driver);

                    while (elapsed < timeout)
                    {
                        var assetInfo = returnZoneInfo(i, driver);

                        //ensures that were able to get information first
                        if ((assetInfo != null) && ("Disabled" == assetInfo.enabled))
                        {
                            success = true;
                            break;
                        }
                        elapsed += 1000;
                        System.Threading.Thread.Sleep(1000);
                    }

                    if (success == false)
                    {
                        //adds zone id to list of values that did not change
                        wrongValue.Add(returnZoneInfo(i, driver).zoneID);
                    }
                }

                //checks that still on current page, returns -1 if not
                int uiPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Zones.CurrentPage)));
                if (uiPage != page)
                    wrongValue.Add(-1);

                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Zones.AllTabsNextIsActive));
            }

            page++;
            //Last page
            countAsset = GetNumberOfZoneRowsOnPage(driver);
            for (int i = 1; i <= countAsset; i++)
            {
                success = false;
                elapsed = 0;
                ControlPage.ClickDisableButton(i, "ZONE", driver);

                while (elapsed < timeout)
                {
                    var assetInfo = returnZoneInfo(i, driver);

                    if ((assetInfo != null) && ("Disabled" == assetInfo.enabled))
                    {
                        success = true;
                        break;
                    }
                    elapsed += 1000;
                    System.Threading.Thread.Sleep(1000);
                }

                //if value is not enabled after timeout
                if (success == false)
                {
                    //adds zone id to list of values that did not change
                    wrongValue.Add(returnZoneInfo(i, driver).zoneID);
                }
            }

            string currentPageText = driver.ReadControlsInnerText(xPath:string.Format(GlobalElementsControl.Zones.CurrentPage));
            int lastUIPage = int.Parse(currentPageText);
            if (lastUIPage != page)
                wrongValue.Add(-1);

            GoToFirstPage(driver);
            return wrongValue;
        }
    }

    /// <summary>
    /// Class that houses Zone information from the Zones tab table
    /// </summary>
    public class ZoneInfo
    {
        public int zoneID { get; set; }
        public string description { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string status { get; set; }
        public double SOC { get; set; }
        public double dcVoltage { get; set; }
        public int kVADemand { get; set; }
        public int kVAActual { get; set; }
        //public int kVARDemand { get; set; }
        public int kVARActual { get; set; }
        public double ambient_temp { get; set; }
        private string m_available_color;
        public string available_color
        {
            get { return this.m_available_color; }
            set { m_available_color = SetColor(value); }
        }
        private string m_enable_color;
        public string enabled_color
        {
            get { return this.m_enable_color; }
            set { m_enable_color = SetColor(value); }
        }
        private string m_status_color;
        public string status_color
        {
            get {return this.m_status_color;}
            set { m_status_color = SetColor(value);}
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(ZoneInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue == "N/A" || otherValue == "N/A")
                {
                    continue;
                }

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }

        private string SetColor(string color)
        {
            //checks if rbga value, if not sets it to same as value
            if (!color.Contains(","))
                return color;

            //is rbga value
            if (StringDictionaries.UITextColors.ContainsValue(color))
            {
                string updatedColor = StringDictionaries.UITextColors.FirstOrDefault(x => x.Value == color).Key;
                //Console.WriteLine("Found color " + color + " " + updatedColor);
                return updatedColor;
            }
            else
            {
                return "Color not found in dictionary: " + color;
            }
        }
    }
}
