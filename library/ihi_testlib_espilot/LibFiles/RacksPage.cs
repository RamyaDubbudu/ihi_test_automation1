﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;
using System.Reflection;

namespace ihi_testlib_espilot
{
    public class RacksPage
    {
        /// <summary>
        /// Returns rack info on page based on index
        /// </summary>
        /// <param name="index">index of tabe starts at 1</param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static RackInfo returnRackInfo(int index, ihi_testlib_selenium.Selenium driver)
        {
            System.Threading.Thread.Sleep(2000);
            //xpaths for all tables are formated in this way with id
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Racks.RackTableString);
            RackInfo rackInfo = new RackInfo();
            string currentColor = "";

            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    //zoneID
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", GlobalElementsControl.Racks.RackTableString, index);
                    string elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int element;
                    int.TryParse(elementString, out element);
                    rackInfo.zoneID = element;
                    //RackId
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    rackInfo.rackID = element;
                    //Label
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    rackInfo.label = elementString;
                    //Description
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    rackInfo.description = elementString;
                    //Available
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    rackInfo.available = elementString;
                    //currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    //rackInfo.available_color = currentColor;
                    //Enabled
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[6]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    rackInfo.enabled = elementString;
                    //currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    //rackInfo.enabled_color = currentColor;
                    //Status
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[7]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    rackInfo.rackStatus = elementString;
                    //currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    //rackInfo.status_color = currentColor;
                    //SOC
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[8]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double elementDouble;
                    double.TryParse(elementString, out elementDouble);
                    rackInfo.SOC = elementDouble;
                    //Voltage Actual
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[9]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    rackInfo.vActual = elementDouble;
                    //Current Actual
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[10]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    rackInfo.iActual = elementDouble;
                    //temp average
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[11]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    rackInfo.tAVG = elementDouble;
                    //Temp max
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[12]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    rackInfo.tMax = elementDouble;
                    //Temp min
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[13]", GlobalElementsControl.Racks.RackTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    rackInfo.tMin = elementDouble;

                    return rackInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (StaleElementReferenceException)
            {
                Console.WriteLine("Stale element found, retrying");
                return returnRackInfo(index, driver);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }//end of returnRackInfo

        /// <summary>
        /// Cycles through all pages of racks and returns a list with all rack information
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>List with all rack info objects</returns>
        public static List<RackInfo> GetAllRackInfo(ihi_testlib_selenium.Selenium driver)
        {
            List<RackInfo> rackList = new List<RackInfo>();
            int countRacks = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive)))
            {
                countRacks = GetNumberofRackRowsOnPage(driver);
                for (int i = 1; i <= countRacks; i++)
                {
                    rackList.Add(returnRackInfo(i, driver));
                }
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive));
            }
            countRacks = GetNumberofRackRowsOnPage(driver);
            for (int i = 1; i <= countRacks; i++)
            {
                rackList.Add(returnRackInfo(i, driver));
            }
            GoToFirstPage(driver);
            return rackList;
        }

        public static bool CheckAllRacksStatus(string statusCheck, ihi_testlib_selenium.Selenium driver)
        {
            bool setStatus = false;
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.RacksTab));
            System.Threading.Thread.Sleep(2000);
            //Enable all Racks
            RackInfo rackInfo = RacksPage.returnRackInfo(1, driver);
            {
                if (rackInfo.rackStatus != "Connected")
                    setStatus = false;
                else setStatus = true;
            }
            return setStatus;
        }

        /// <summary>
        /// Counts number of rows on table on the current page
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static int GetNumberofRackRowsOnPage(ihi_testlib_selenium.Selenium driver)
        {
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Racks.RackTableString);

            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    IWebElement webElementBody = driver.browserDriver.FindElement(By.XPath(xpath));
                    IList<IWebElement> ElementCollectionBody = webElementBody.FindElements(By.XPath(xpath));
                    //Makes sure only entry doesn't say list is empty
                    if (ElementCollectionBody.Count == 1 && ElementCollectionBody[0].Text == "There are no batteries!")
                        return 0;

                    return ElementCollectionBody.Count;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }

        public static void GoToFirstPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Racks.AllTabsPreviousIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Racks.AllTabsPreviousIsActive));
            }
        }

        public static void GoToLastPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive));
            }
        }

        /// <summary>
        /// Compare Racks Database, ZMQ query, and UI to make sure all report same values where applicable.
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <param name="IPAddress">Linux IP address</param>
        /// <param name="userName">Linux username</param>
        /// <param name="password">Linux password</param>
        /// <param name="databaseLocation">Location of decs database</param>
        /// <returns>ControlError info list which includes ID, column of error and UI, ZMQ, and database values</returns>
        public static List<ControlErrorInfo> CompareRackPageToZMQAndDatabase(ihi_testlib_selenium.Selenium driver, string IPAddress, string userName, string password, string databaseLocation = "/var/run/ihi-decs/")
        {
            List<ControlErrorInfo> errorList = new List<ControlErrorInfo>();
            string temp = "";
            string table = "racks";
            string column = "";
            string ID = "";

            //order by zone id for both lists to make order reported by ZMQ irrelevant
            List<RackTable> rackListZMQ = IHI_ZMQ.UI__GetData_RackList(IPAddress);
            rackListZMQ = rackListZMQ.OrderBy(o => o.racks_id).ToList();

            List<RackInfo> rackListUI = GetAllRackInfo(driver);
            rackListUI = rackListUI.OrderBy(o => o.rackID).ToList();

            for (int i = 0; i < rackListUI.Count; i++)
            {
                int rackID = rackListUI[i].rackID;
                RackInfo inverterDatabase = ReadRackInfoFromDatabase(rackID, IPAddress, userName, password, databaseLocation);
                ID = table + "_" + rackID.ToString();


                //rackID does not match
                column = "racks_id";
                if ((rackListZMQ[i].zones_id != rackListUI[i].zoneID) || (rackListUI[i].zoneID != inverterDatabase.zoneID))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].zoneID.ToString(), rackListZMQ[i].zones_id.ToString(), inverterDatabase.zoneID.ToString()));
                }

                //zoneID does not match
                column = "zone_id";
                if ((rackListZMQ[i].zones_id != rackListUI[i].zoneID) || (rackListUI[i].zoneID != inverterDatabase.zoneID))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].zoneID.ToString(), rackListZMQ[i].zones_id.ToString(), inverterDatabase.zoneID.ToString()));
                }

                column = "label";
                if ((rackListZMQ[i].label != rackListUI[i].label) || (rackListUI[i].label != inverterDatabase.label))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].label.ToString(), rackListZMQ[i].label.ToString(), inverterDatabase.label.ToString()));
                }

                //description does not match
                column = "description";
                if ((rackListZMQ[i].description != rackListUI[i].description) || (rackListUI[i].description != inverterDatabase.description))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].description.ToString(), rackListZMQ[i].description.ToString(), inverterDatabase.description.ToString()));
                }

                //available does not match available
                column = "available";
                temp = StringDictionaries.ConvertDatabaseString(table, column, rackListZMQ[i].available.ToString());
                if ((temp != rackListUI[i].available) || (rackListUI[i].available != inverterDatabase.available))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].available.ToString(), temp, inverterDatabase.available.ToString()));
                }

                //enabled does not match
                column = "enabled";
                temp = StringDictionaries.ConvertDatabaseString(table, column, rackListZMQ[i].enabled.ToString());
                if ((temp != rackListUI[i].enabled) || (rackListUI[i].enabled != inverterDatabase.enabled))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].enabled.ToString(), temp, inverterDatabase.enabled.ToString()));
                }

                //status does not match
                column = "rack_status";
                temp = StringDictionaries.ConvertDatabaseString(table, column, rackListZMQ[i].rack_status.ToString());
                if ((temp != rackListUI[i].rackStatus) || (rackListUI[i].rackStatus != inverterDatabase.rackStatus))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].rackStatus.ToString(), temp, inverterDatabase.rackStatus.ToString()));
                }

                //SOC does not match
                column = "soc";
                if ((rackListZMQ[i].soc != rackListUI[i].SOC) || (rackListUI[i].SOC != inverterDatabase.SOC))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].SOC.ToString(), rackListZMQ[i].soc.ToString(), inverterDatabase.SOC.ToString()));
                }

                //vActual does not match
                column = "vActual";
                if ((rackListZMQ[i].v_actual != rackListUI[i].vActual) || (rackListUI[i].vActual != inverterDatabase.vActual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].vActual.ToString(), rackListZMQ[i].v_actual.ToString(), inverterDatabase.vActual.ToString()));
                }

                //iActual does not match
                column = "iActual";
                if ((rackListZMQ[i].i_actual != rackListUI[i].iActual) || (rackListUI[i].iActual != inverterDatabase.iActual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].iActual.ToString(), rackListZMQ[i].i_actual.ToString(), inverterDatabase.iActual.ToString()));
                }

                //tAVG does not match
                column = "tAVG";
                if ((rackListZMQ[i].t_avg != rackListUI[i].tAVG) || (rackListUI[i].tAVG != inverterDatabase.tAVG))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].tAVG.ToString(), rackListZMQ[i].t_avg.ToString(), inverterDatabase.tAVG.ToString()));
                }

                //tMax does not match
                column = "tMax";
                if ((rackListZMQ[i].t_max != rackListUI[i].tMax) || (rackListUI[i].tMax != inverterDatabase.tMax))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].tMax.ToString(), rackListZMQ[i].t_max.ToString(), inverterDatabase.tMax.ToString()));
                }

                //tMin does not match
                column = "tMin";
                if ((rackListZMQ[i].t_min != rackListUI[i].tMin) || (rackListUI[i].tMin != inverterDatabase.tMin))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, rackListUI[i].tMin.ToString(), rackListZMQ[i].t_min.ToString(), inverterDatabase.tMin.ToString()));
                }
            }

            return errorList;
        }

        /// <summary>
        /// Read all RackInfo values from database and stores them in a RackInfo file
        /// </summary>
        /// <param name="rackID">rack id of which to check</param>
        /// <param name="IPAddress">Linux IP address</param>
        /// <param name="userName">Linux username</param>
        /// <param name="password">Linux password</param>
        /// <param name="databaseLocation">Location of decs database</param>
        /// <returns></returns>
        public static RackInfo ReadRackInfoFromDatabase(int rackID, string IPAddress, 
            string userName, string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            RackInfo rackDatabase = new RackInfo();
            string table = "racks";
            string format = "";
            string column = "";
            string temp = "";

            //rackID - taken from parameters
            rackDatabase.rackID = rackID;

            SshClient ssh = new SshClient(IPAddress, userName, password);
            ssh.Connect();

            //zoneID
            column = "zones_id";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.zoneID = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //description
            column = "label";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.label = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //description
            column = "description";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.description = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //available
            column = "available";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.available = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //enabled
            column = "enabled";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.enabled = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //status
            column = "rack_status";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.rackStatus = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //SOC
            column = "soc";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.SOC = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //vActual
            column = "v_actual";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.vActual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //iActual
            column = "i_actual";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.iActual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //tAVG
            column = "t_avg";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.tAVG = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //tMax
            column = "t_Max";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.tMax = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //tMin
            column = "t_Min";
            format = string.Format("SELECT {0} from {1} where racks_id='{2}'", column, table, rackID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            rackDatabase.tMin = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            ssh.Disconnect();

            return rackDatabase;
        }

        /// <summary>
        /// Create more racks, copying from the first rack
        /// </summary>
        /// <param name="numberToCreate"></param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static void CreateMoreRacks(int numberToCreate, SSHAndDatabaseDirectory connectionInfo, int zoneID = 1, string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(connectionInfo.SSHIPAddress, connectionInfo.SSHUsername, connectionInfo.SSHPassword);
            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            string table = "racks";

            List<RackTable> rackListZMQ = IHI_ZMQ.UI__GetData_RackList(connectionInfo.SSHIPAddress);

            //find min and max values to know where to copy from and where to put new entry
            int maxID = rackListZMQ.Max(t => t.racks_id);
            int minID = rackListZMQ.Min(t => t.racks_id);

            RackTable rackTable = new RackTable();

            List<string> propertyNames = rackTable.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            //the following code is formating the sql insert into select statement
            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }
            for (int i = 0; i < numberToCreate; i++)
            {
                string prototypeSelect = (++maxID) + ", ";
                foreach (string item in propertyNames)
                {
                    if (item == firstItem)
                        prototypeSelect = prototypeSelect;
                    else if (item == lastItem)
                        prototypeSelect += item + " ";
                    else
                        prototypeSelect += item + (", ");
                }

                //creates sql statement that copies the config of the first rack except for rack id which increments
                string sqlStatement = string.Format("INSERT into {0}{1}SELECT {2}from {0} where {3}={4}", table,
                    prototypeInto, prototypeSelect, firstItem, minID);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
                //racks needs to update rack_no too to create unquie id on UI
                sqlStatement = string.Format("UPDATE racks SET rack_no='{0}' WHERE racks_id={0}",  maxID);
                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
                //update zone id
                sqlStatement = string.Format("UPDATE cooling SET zones_id='{0}' WHERE racks_id={1}", zoneID, maxID);
                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
            }
            ssh.Disconnect();
        }

        /// <summary>
        /// Takes in a list of rack tables (usually from zmq), clears the database and loads in only these values.  Useful when making tests non-descructive
        /// </summary>
        /// <param name="rackList"></param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static void UpdateDatabaseFromRacksTableList(List<RackTable> rackList, string IPAddress, 
            string userName, string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(IPAddress, userName, password);

            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            string table = "racks";

            SSH.ClearSQLTable(databaseLocation, "decs_dc.db", table, IPAddress, userName, password);

            RackTable rackTableTemp = new RackTable();
            List<string> propertyNames = rackTableTemp.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            //the following code is formating the sql insert into select statement
            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }

            foreach (RackTable rackTable in rackList)
            {
                List<string> objectStrings = rackTable.GetPropertyValues();
                string last = objectStrings.Last();

                string prototypeSelect = " (";
                int findLastLoop = 1;
                foreach (string value in objectStrings)
                {
                    if (!(findLastLoop==objectStrings.Count))
                        prototypeSelect += value + (", ");
                    else
                        prototypeSelect += value + (") ");
                    findLastLoop++;
                }
                string sqlStatement = string.Format("INSERT INTO {0}{1} VALUES {2}", table, prototypeInto, prototypeSelect);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, IPAddress, decsLocation);
            }
            ssh.Disconnect();
        }

        /// <summary>
        /// Returns a list of the index of LGchem racks that will not have enable/disable buttons
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="numberRacksPerPage"></param>
        /// <param name="lgChemPrismaticList"></param>
        /// <returns></returns>
        public static List<int> FindLGChemRacksOnPage(int pageNumber, int numberRacksPerPage, List<int> lgChemPrismaticList)
        {
            List<int> lgRacksOnPage = new List<int>();

            int lowRange = ((pageNumber - 1) * numberRacksPerPage);
            int highRange = pageNumber * numberRacksPerPage;

            foreach(int lgPris in lgChemPrismaticList)
            {
                if(Enumerable.Range(lowRange, highRange).Contains(lgPris))
                {
                    //adds 1 because lists on UI are indexed starting at 1
                    lgRacksOnPage.Add((lgPris % numberRacksPerPage) + 1);
                }
            }
            return lgRacksOnPage;
        }

        public static List<int> FindLGChemRacksOnLastPage(int pageNumber, int numberRacksPerPage, int numberRackOnLastPage, List<int> lgChemPrismaticList)
        {
            List<int> lgRacksOnPage = new List<int>();

            int lowRange = ((pageNumber - 1) * numberRacksPerPage);
            int highRange = (lowRange + numberRackOnLastPage);

            foreach (int lgPris in lgChemPrismaticList)
            {
                if (Enumerable.Range(lowRange, highRange).Contains(lgPris))
                {
                    lgRacksOnPage.Add((lgPris % numberRacksPerPage) + 1);
                }
            }
            return lgRacksOnPage;
        }

        /// <summary>
        /// Returns list of all LG Chem JH2 Prismatic (type 3) racks
        /// </summary>
        /// <param name="rackList">List of Racks taken from ZMQ</param>
        /// <returns></returns>
        private static List<int> LGChemPrismaticRacks(List<RackTable> rackList)
        {
            List<int> lgChemPrismaticList = new List<int>();
            int index = 0;

            foreach(RackTable rack in rackList)
            {
                if (rack.type == 3)
                    lgChemPrismaticList.Add(index);

                index++;
            }
            return lgChemPrismaticList;
        }

        /// <summary>
        /// Enables all racks.  This will determin if the row contains a LG Chem Prismatic (Type 3) rack and ignore the row if it does.
        /// </summary>
        /// <param name="timeout"></param>
        /// <param name="rackList"></param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static List<int> EnableAllRacks(int timeout, List<RackTable> rackList, ihi_testlib_selenium.Selenium driver)
        {
            int page = 0;
            int countAsset = 0;
            bool success = false;
            int elapsed = 0;
            int pageNumber = 1;
            var lgChemRackList = LGChemPrismaticRacks(rackList);
            List<int> wrongValue = new List<int>();

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive)))
            {
               
            page++;
                countAsset = GetNumberofRackRowsOnPage(driver);
                for (int i = 1; i <= countAsset; i++)
                {
                    //Determines if this row show not have an enable/disable button and skips this loop if it does
                    var listOnPage = FindLGChemRacksOnPage(pageNumber, countAsset, lgChemRackList);
                    //if (listOnPage.Contains(i))
                    //    continue;

                    success = false;
                    elapsed = 0;
                    ControlPage.ClickEnableButton(i, "BATTERY", driver);

                    while (elapsed < timeout)
                    {
                        var assetInfo = returnRackInfo(i, driver);

                        //ensures thay were able to get information first
                        if ((assetInfo != null) && ("Enabled" == assetInfo.enabled))
                        {
                            success = true;
                            break;
                        }
                        elapsed += 1000;
                        System.Threading.Thread.Sleep(1000);
                    }

                    if (success == false)
                    {
                        //adds zone id to list of values that did not change
                        wrongValue.Add(returnRackInfo(i, driver).zoneID);
                    }
                }
                //checks that still on current page, returns null if not
                int uiPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Racks.CurrentPage)));
                if (uiPage != page)
                    wrongValue.Add(-1);


                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive));
                pageNumber++;
            }

            page++;
            //Last page
            countAsset = GetNumberofRackRowsOnPage(driver);
            for (int i = 1; i <= countAsset; i++)
            {
                //Determines if this row should not have an enable/disable button and skips this loop if it does
                var listOnPage = FindLGChemRacksOnLastPage(pageNumber, 10, countAsset, lgChemRackList);
                //if (listOnPage.Contains(i))
                //    continue;

                success = false;
                elapsed = 0;
                ControlPage.ClickEnableButton(i, "BATTERY", driver);

                while (elapsed < timeout)
                {
                    var assetInfo = returnRackInfo(i, driver);

                    if ((assetInfo != null) && ("Enabled" == assetInfo.enabled))
                    {
                        success = true;
                        break;
                    }
                    elapsed += 1000;
                    System.Threading.Thread.Sleep(1000);
                }

                //if value is not enabled after timeout
                if (success == false)
                {
                    //adds zone id to list of values that did not change
                    wrongValue.Add(returnRackInfo(i, driver).zoneID);
                }
            }

            GoToFirstPage(driver);
            return wrongValue;
        }

        /// <summary>
        /// Disables all racks.  This will determin if the row contains a LG Chem Prismatic (Type 3) rack and ignore the row if it does.
        /// </summary>
        /// <param name="timeout"></param>
        /// <param name="rackList"></param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static List<int> DisableAllRacks(int timeout, List<RackTable> rackList, ihi_testlib_selenium.Selenium driver)
        {
            int page = 0;
            int countAsset = 0;
            bool success = false;
            int elapsed = 0;
            int pageNumber = 1;
            var lgChemRackList = LGChemPrismaticRacks(rackList);
            List<int> wrongValue = new List<int>();


            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive)))
            {
                page++;
                countAsset = GetNumberofRackRowsOnPage(driver);
                for (int i = 1; i <= countAsset; i++)
                {
                    //Determines if this row show not have an enable/disable button and skips this loop if it does
                    var listOnPage = FindLGChemRacksOnPage(pageNumber, countAsset, lgChemRackList);
                    //if (listOnPage.Contains(i))
                    //    continue;

                    success = false;
                    elapsed = 0;
                    ControlPage.ClickDisableButton(i, "BATTERY", driver);

                    //creates timeout for finding enable/disable
                    while (elapsed < timeout)
                    {
                        var assetInfo = returnRackInfo(i, driver);

                        //ensures that were able to get information first
                        if ((assetInfo != null) && ("Disabled" == assetInfo.enabled))
                        {
                            success = true;
                            break;
                        }
                        elapsed += 1000;
                        System.Threading.Thread.Sleep(1000);
                    }

                    if (success == false)
                    {
                        //adds zone id to list of values that did not change
                        wrongValue.Add(returnRackInfo(i, driver).rackID);
                    }
                }
                //checks that still on current page, returns -1 if not
                int uiPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Racks.CurrentPage)));
                if (uiPage != page)
                    wrongValue.Add(-1);

                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive));
                pageNumber++;
            }

            page++;
            //Last page
            countAsset = GetNumberofRackRowsOnPage(driver);
            for (int i = 1; i <= countAsset; i++)
            {

                //Determines if this row show not have an enable/disable button and skips this loop if it does
                var listOnPage = FindLGChemRacksOnLastPage(pageNumber, 10, countAsset, lgChemRackList);
                //if (listOnPage.Contains(i))
                //    continue;

                success = false;
                elapsed = 0;
                ControlPage.ClickDisableButton(i, "BATTERY", driver);

                while (elapsed < timeout)
                {
                    var assetInfo = returnRackInfo(i, driver);

                    if ((assetInfo != null) && ("Disabled" == assetInfo.enabled))
                    {
                        success = true;
                        break;
                    }
                    elapsed += 1000;
                    System.Threading.Thread.Sleep(1000);
                }

                //if value is not enabled after timeout
                if (success == false)
                {
                    //adds zone id to list of values that did not change
                    wrongValue.Add(returnRackInfo(i, driver).rackID);
                }
            }

            int lastUIPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Racks.CurrentPage)));

            if (lastUIPage != page)
                wrongValue.Add(-1);

            GoToFirstPage(driver);
            return wrongValue;
        }
    }//End of RacksPage

    /// <summary>
    /// Rack info shown on Control Tab
    /// </summary>
    public class RackInfo
    {
        public int zoneID { get; set; }
        public int rackID { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string rackStatus { get; set; }
        public double SOC { get; set; }
        public double vActual { get; set; }
        public double iActual { get; set; }
        public double tAVG { get; set; }
        public double tMax { get; set; }
        public double tMin { get; set; }
        private string m_available_color;
        public string available_color
        {
            get { return this.m_available_color; }
            set { m_available_color = SetColor(value); }
        }
        private string m_enable_color;
        public string enabled_color
        {
            get { return this.m_available_color; }
            set { m_enable_color = SetColor(value); }
        }
        private string m_status_color;
        public string status_color
        {
            get { return this.m_status_color; }
            set { m_status_color = SetColor(value); }
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(RackInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue == "N/A" || otherValue == "N/A")
                {
                    continue;
                }

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }

        private string SetColor(string color)
        {
            //checks if rbga value, if not sets it to same as value
            if (!color.Contains(","))
                return color;

            //is rbga value
            if (StringDictionaries.UITextColors.ContainsValue(color))
            {
                string updatedColor = StringDictionaries.UITextColors.FirstOrDefault(x => x.Value == color).Key;
                //Console.WriteLine("Found color " + color + " " + updatedColor);
                return updatedColor;
            }
            else
            {
                return "Color not found in dictionary: " + color;
            }
        }
    }
}
