﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Reflection;

namespace ihi_testlib_espilot
{
    public static class InvertersPopupPage
    {

        public static List<string> GetAllInvertersLabels(string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/")
        {
            string zonesstring = SSH.ReadAllFromDatabaseOpenClose("inverters", "label", SSHIPAddress, SSHUsername, SSHPassword);

            List<string> AllLabels = zonesstring.Split('\n').ToList<string>();
            AllLabels = AllLabels.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            return AllLabels;
        }
    }


    public class InvertersPopupInfo
    {
        public string label { get; set; }
        public string description { get; set; }
        public int zones_id { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string state_actual { get; set; }
        public int p_actual { get; set; }
        public int p_demand { get; set; }
        public int q_actual { get; set; }
        public int q_demand { get; set; }
        public double v1_actual { get; set; }
        public double v2_actual { get; set; }
        public double v3_actual { get; set; }
        public double i1_actual { get; set; }
        public double i2_actual { get; set; }
        public double i3_actual { get; set; }
        public double v_dcbus { get; set; }
        public double i_dcbus { get; set; }

        public InvertersPopupInfo(ihi_testlib_selenium.Selenium driver)
        {
            try
            {


                label = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.DetailedInformation));

                description = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Description));
                zones_id = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.ZonesID)));
                available = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Available));
                enabled = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Enabled));
                state_actual = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Status));
                p_demand = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.kWDemand)));
                q_demand = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.kVARDemand)));
                p_actual = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.kWActual)));
                q_actual = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.kVARActual)));
                v1_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Actualv1)));
                v2_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Actualv2)));
                v3_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Actualv3)));
                i1_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Actuali1)));
                i2_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Actuali2)));
                i3_actual = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.Actuali3)));
                v_dcbus = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.VDCBUS)));
                i_dcbus = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsInverterPopup.IDCBUS)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(InvertersPopupInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }

    }
}
