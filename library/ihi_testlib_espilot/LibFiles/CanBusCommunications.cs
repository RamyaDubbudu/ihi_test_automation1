﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;

namespace ihi_testlib_espilot
{
    public static class CanBusCommunications
    {
        /// <summary>
        /// Disables the can0 bus
        /// </summary>
        public static bool DisableCan(string ipAddress, string zcPassword = "")
        {
            try
            {
                zcPassword = zcPassword == "" ? DECSLoginCredientials.Password : zcPassword;
                string formatClose = string.Format("echo \"{0}\" | sudo -S /sbin/ifconfig can0 down", zcPassword);
                SSH.OpenWriteCloseSSH(formatClose, ipAddress, DECSLoginCredientials.UserName, DECSLoginCredientials.Password);
                return true;
            }
            catch (Exception e)
            {
                throw new Exception("Failed to shutdown CAN Bus " + e.Message);
            }
        }

        /// <summary>
        /// Enables the can0 bus
        /// </summary>
        public static bool EnableCan(string ipAddress, int bitRate = 500000, string zcPassword = "")
        {
            zcPassword = zcPassword == "" ? DECSLoginCredientials.Password : zcPassword;
            var ssh = new SshClient(ipAddress, DECSLoginCredientials.UserName, DECSLoginCredientials.Password);
            try
            {
                string prototype = "echo \"{0}\" | sudo -S {1}";
                ssh.Connect();
                ssh.RunCommand(string.Format(prototype, zcPassword, "ip link set can0 type can restart-ms 100"));
                ssh.RunCommand(string.Format(prototype, zcPassword, "ip link set can0 txqueuelen 10000"));
                string format = string.Format("ip link set can0 type can bitrate {0}", bitRate);
                ssh.RunCommand(string.Format(prototype, zcPassword, format));
                ssh.RunCommand(string.Format(prototype, zcPassword, "sudo ip link set can0 up"));
                ssh.Dispose();
                return true;
            }
            catch (Exception e)
            {
                if(ssh.IsConnected)
                    ssh.Dispose();
                throw new Exception("Failed to enable CAN Bus " + e.Message);
            }
        }
    }
}
