﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public static class SeleniumWrapper
    {
        public static ihi_testlib_selenium.Selenium SeleniumDriver = new ihi_testlib_selenium.Selenium();
        public static void SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser browser, ihi_testlib_selenium.Selenium webDriver)
        {

            webDriver.SetBrowser(browser, GlobalsGeneralStrings.homepage);
          
        }

        public static void DriverCleanup(ihi_testlib_selenium.Selenium webDriver)
        {
            webDriver.browserDriver.Quit();
        }

    }
}
