﻿/*==============================================================================
Name        : GlobalElements.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Variables for used throughout the library to store UI page elements for easy modibility.
==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// List of Elements on the Login Page by ID.
    /// </summary>
    public static class GlobalElementsLoginPage
    {
        /// <summary>
        /// Login Username box
        /// </summary>
        public static string LoginUserBox { get { return "//input[@id='idIHI_UI_LOGIN__textboxUsername']"; } }
        /// <summary>
        /// Login Password box
        /// </summary>
        public static string LoginPasswordBox { get { return "//input[@id='idIHI_UI_LOGIN__textboxPassword']"; } }
        /// <summary>
        /// Login Botton location
        /// </summary>
        public static string LoginButton { get { return "//button[contains(@id,'buttonLogin')]"; } }
        /// <summary>
        /// LoginFail error message location
        /// </summary>
        public static string LoginFailElement { get { return "//span[@id = 'idIHI_UI_LOGIN__valueErrorMessage']"; } }

        public static string UILoadWait { get { return "//div[@id = 'idIHI_UI_LOADING__divLoadingOverlay' and @style = 'display: none;']"; } }
    }

    public static class GlobalElementsHeader
    {
        public static string IHILogo { get { return "//img[@class = 'classIHI_UI_HEADER__imageLogo']"; } }
        public static string Help { get { return "//a[@id ='idIHI_HELP__linkHelp']"; } }
    }

    /// <summary>
    /// List of Elements on the Main Page
    /// </summary>
    public static class GlobalElementsMainPage
    {
       // public static string Tabs { get { return "idIHI_UI_CONTENT__iframeTabs"; } }
        public static string DashboardTab { get { return "//div[@id = 'tabs']//a[@id = 'idIHI_UI_TABS__listDashboard']"; } }
        public static string ControlTab { get { return "//div[@id = 'tabs']//a[@id = 'idIHI_UI_TABS__listControl']"; } }
        public static string EventsTab { get { return "//div[@id = 'tabs']//a[@id = 'idIHI_UI_TABS__listEvents']"; } }
        public static string ChartsTab { get { return "//div[@id = 'tabs']//a[@id = 'idIHI_UI_TABS__listCharts']"; } }
        public static string AlarmsTab { get { return "//div[@id = 'tabs']//a[@id = 'idIHI_UI_TABS__listAlarms']"; } }
        public static string AdminTab { get { return "//div[@id = 'tabs']//a[@id = 'idIHI_UI_TABS__listAdmin']"; } }
        public static string UIRevision { get { return "//span[@id='idIHI_UI_FOOTER__valueUI_Build']"; } }
        public static string ServerRevision { get { return "//span[@id='idIHI_UI_FOOTER__valueServer_Build']"; } }
        public static string Time { get { return "//span[@id='idIHI_UI_FOOTER__valueTimer']"; } }
        public static string LoginName { get { return "//span[@id='idIHI_UI_TITLE__valueFullName']"; } }
        public static string LogoutButton { get { return "//button[@id='idIHI_UI_TITLE__buttonLogout']"; } }
    }

    //list of elements on the Dashboard Tab
    public static class GlobalElementsDashboard
    {
        //siteinformation
        public static string SiteName { get { return "//span[@id = 'idIHI_UI_DASHBOARD__SITE__valueName']"; } }
        public static string kWhThroughput { get { return "//td[@id='idIHI_UI_DASHBOARD__SITE__valueThroughput']"; } }
        public static string CurrentLMP { get { return "//a[@id='idIHI_UI_DASHBOARD__SITE__linkLMP']"; } }
        public static string CurrentLMPTitle { get { return "//td[@id='idIHI_UI_DASHBOARD__SITE__valueLMP_Title']"; } }
        public static string Description { get { return "//span[@id='idIHI_UI_DASHBOARD__SITE__valueDescription']"; } }
        public static string Address2 { get { return "//span[@id='idIHI_UI_DASHBOARD__SITE__Address_Line2']"; } }
        public static string Map { get { return "//img[@src='images/IHI_IMAGE_MAP.png']"; } }

        //Status
        public static string CurrentMode { get { return "//td[@id='idIHI_UI_DASHBOARD__DEVICE__STATUS__valueMode_Actual']"; } }
        public static string CurrentModeText { get { return "//td[@id='idIHI_UI_DASHBOARD__DEVICE__STATUS__valueMode_Actual' and contains(text(), '{0}')]"; } }
        public static string SelectedModeLabel { get { return "//td[@id='idIHI_UI_DASHBOARD__DEVICE__STATUS__labelMode_Demand']"; } }
        public static string SelectedMode { get { return "//td[@id='idIHI_UI_DASHBOARD__DEVICE__STATUS__valueMode_Demand']"; } }
        public static string RuntimeCurrent { get { return "//td[@id='idIHI_UI_DASHBOARD__DEVICE__STATUS__valueCurrentRuntime']"; } }
        public static string RuntimeTotal { get { return "//td[@id='idIHI_UI_DASHBOARD__DEVICE__STATUS__valueTotalRuntime']"; } }

        //Power Inverter
        public static string pDemand { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueP_Demand']"; } }
        public static string pDemandText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueP_Demand' and contains(text(), '{0}')]"; } }
        public static string pActual { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueP_Actual']"; } }
        public static string pActualText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueP_Actual' and contains(text(), '{0}')]"; } }
        public static string pAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueP_Max']"; } }
        public static string pAvailableText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueP_Max' and contains(text(), '{0}')]"; } }
        public static string qDemand { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueQ_Demand']"; } }
        public static string qDemandText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueQ_Demand' and contains(text(), '{0}')]"; } }
        public static string qActual { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueQ_Actual']"; } }
        public static string qActualText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueQ_Actual' and contains(text(), '{0}')]"; } }
        public static string qAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueQ_Max']"; } }
        public static string qAvailableText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_INVERTER__valueQ_Max' and contains(text(), '{0}')]"; } }
        //Power Utility
        public static string RefFrequency { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_Frequency']"; } }
        public static string RefFrequencyText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_Frequency' and contains(text(), '{0}')]"; } }
        public static string RefVoltage { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_Voltage']"; } }
        public static string RefVoltageText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_Voltage' and contains(text(), '{0}')]"; } }
        public static string RefpkW { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_P']"; } }
        public static string RefpkWText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_P' and contains(text(), '{0}')]"; } }
        public static string RefqkVAR { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_Q']"; } }
        public static string RefqkVARText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__valueRef_Q' and contains(text(), '{0}')]"; } }
        public static string RefskVA { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__labelRef_S']"; } }
        public static string RefskVAText { get { return "//td[@id='idIHI_UI_DASHBOARD__POWER_UTILITY__labelRef_S' and contains(text(), '{0}')]"; } }

        //Asset Status
        public static string ZonesAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueZones_Available']"; } }
        public static string ZonesEnabled { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueZones_Enabled']"; } }
        public static string ZonesOnline { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueZones_Online']"; } }
        public static string ZonesTotal { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueZones_Total']"; } }
        public static string BatteryRacksAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueRacks_Available']"; } }
        public static string BatteryRacksEnabled { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueRacks_Enabled']"; } }
        public static string BatteryRacksOnline { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueRacks_Online']"; } }
        public static string BatteryRacksTotal { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueRacks_Total']"; } }
        public static string InvertersAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueInverters_Available']"; } }
        public static string InvertersEnabled { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueInverters_Enabled']"; } }
        public static string InvertersOnline { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueInverters_Online']"; } }
        public static string InvertersTotal { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueInverters_Total']"; } }
        public static string CoolingAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueCooling_Available']"; } }
        public static string CoolingEnabled { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueCooling_Enabled']"; } }
        public static string CoolingOnline { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueCooling_Online']"; } }
        public static string CoolingTotal { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueCooling_Total']"; } }
        public static string AverageSOC { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueZones_State_of_Charge']"; } }
        public static string AvailableSOC { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueZones_State_of_Charge']"; } }
        public static string AvailableSOCValue { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__valueZones_State_of_Charge' and contains(text(), '{0}')]"; } }

        //Meters
        public static string IODevicesAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__IO_AVAILABLE']"; } }
        public static string IODevicesTotal { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__IO_TOTAL']"; } }
        public static string MetersAvailable { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__METERS_AVAILABLE']"; } }
        public static string MetersTotal { get { return "//td[@id='idIHI_UI_DASHBOARD__ASSET__METERS_TOTAL']"; } }
    }

    /// <summary>
    /// Class for Startup/Shutdown Elements on Dashboard
    /// </summary>
    public static class GlobalStartupShutdown
    {
        public static string Dropdown { get { return "//select[@id='idIHI_UI_DASHBOARD__MODE__droplistChangeMode']"; } }
        public static string DropdownList { get { return "//select[@id='idIHI_UI_DASHBOARD__MODE__droplistChangeMode']"; } }
        public static string GoButton { get { return "//button[contains(@id,'idIHI_UI_DASHBOARD__MODE__buttonGo')]"; } }
        public static string pField { get { return "//input[@id='idIHI_UI_DASHBOARD__MODE__textboxkVA']"; } }
        public static string qField { get { return "//input[contains(@id,'idIHI_UI_DASHBOARD__MODE__textboxkVAR')]"; } }
        public static string Apply { get { return "//button[@id='idIHI_UI_DASHBOARD__MODE__buttonApply']"; } }
        public static string ErrorMessageP { get { return "//td[@id='idIHI_UI_DASHBOARD__MODE__valuekVA_ErrorMessage']"; } }
        public static string ErrorMessageQ { get { return "//td[@id='idIHI_UI_DASHBOARD__MODE__valuekVAR_ErrorMessage']"; } }
        public static string AutoRestart { get { return "//input[@id = 'idIHI_UI_DASHBOARD__MODE__checkboxAutoRestart']"; } }
        public static string ProfileApplyButton { get { return "//button[@id='idIHI_UI_DASHBOARD__MODE__buttonApply']"; } }
        public static string ProfileDropDown { get { return "//select[@id='idIHI_UI_DASHBOARD__MODE__dropDownProfile']"; } }
    }

    /// <summary>
    /// Class for all subtab's next and previous button
    /// </summary>
    public static class GlobalElementsControl
    {
        public static string ControlFrame { get { return "//div[@id = 'idIHI_UI_TABS__tabControl']"; } }
        public static string ZonesTab { get { return "//a[@id = 'idIHI_UI_CONTROL__listZones']"; } }
        public static string RacksTab { get { return "//a[@id = 'idIHI_UI_CONTROL__listRacks']"; } }
        public static string InvertersTab { get { return "//a[@id = 'idIHI_UI_CONTROL__listInverters']"; } }
        public static string EnvironmentTab { get { return "//a[@id = 'idIHI_UI_CONTROL__listCoolers']"; } }
        /// <summary>
        /// Class for zone subtab specific information
        /// </summary>
        public static class Zones
        {
            /// <summary>
            /// iFrame of Zones
            /// </summary>
            public static string ZonesFrame { get { return "idIHI_UI_CONTROL__iframeZones"; } }
            /// <summary>
            /// String to identify table
            /// </summary>
            public static string ZoneTableString
            {
                get
                {
                    return "idIHI_UI_CONTROL__ZONE__tableData";
                }
            }
            /// <summary>
            /// By to identify table element
            /// </summary>
            public static string ZoneTable { get { return ZoneTableString; } }
            public static string CurrentPage { get { return "#idIHI_UI_CONTROL__ZONE__tableData_paginate > span > a.paginate_button.current"; } }
            public static string AllTabsNextIsActive { get { return "//a[@class='paginate_button next enabled' and @id = 'idIHI_UI_CONTROL__ZONE__tableData_next']"; } }
            public static string AllTabsNextIsInactive { get { return "//a[@class='paginate_button next disabled'][1]"; } }
            public static string AllTabsPreviousIsActive { get { return "//a[@class='paginate_button previous'][1]"; } }
            public static string AllTabsPreviousIsinactive { get { return "//a[@class='paginate_button previous disabled'][1]"; } }
            public static string ZoneId { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[1]"; } }
            public static string ZoneIdText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 1 and contains(text(), '{0}')]"; } }
            public static string ZoneLabel { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[2]"; } }
            public static string ZoneLabelText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 2 and contains(text(), '{0}')]"; } }
            public static string ZoneAvailable { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[3]"; } }
            public static string ZoneAvailableText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 3 and contains(text(), '{0}')]"; } }
            public static string ZoneEnabled { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[4]"; } }
            public static string ZoneEnabledText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 4 and contains(text(), '{0}')]"; } }
            public static string ZoneStatus { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[5]"; } }
            public static string ZoneStatusText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 5 and contains(text(), '{0}')]"; } }
            public static string ZoneSOC { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[6]"; } }
            public static string ZoneSOCText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 6 and contains(text(), '{0}')]"; } }
            public static string ZoneDC { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[7]"; } }
            public static string ZoneDCText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 7 and contains(text(), '{0}')]"; } }
            public static string ZonePDemand { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[8]"; } }
            public static string ZonePDemandText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 8 and contains(text(), '{0}')]"; } }
            public static string ZonePActual { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[9]"; } }
            public static string ZonePActualText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 9 and contains(text(), '{0}')]"; } }
            public static string ZoneQActual { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[10]"; } }
            public static string ZoneQActualText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 10 and contains(text(), '{0}')]"; } }
            public static string ZoneATemp { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[11]"; } }
            public static string ZoneATempText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 11 and contains(text(), '{0}')]"; } }
            public static string ZoneEnableButton { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 2 and text() = '{0}']/../td/button[@id = 'idIHI_UI_CONTROL__ZONE__buttonEnable']"; } }
            public static string ZoneDisableButton { get { return "//div[@id = 'idIHI_UI_CONTROL__tabZones']//table[@id = 'idIHI_UI_CONTROL__ZONE__tableData']//tbody//td[position() = 2 and text() = '{0}']/../td/button[@id = 'idIHI_UI_CONTROL__ZONE__buttonDisable']"; } }
        }

        /// <summary>
        /// Class for Racks subtab specific information
        /// </summary>
        public static class Racks
        {
            /// <summary>
            /// iFrame of Racks
            /// </summary>
            public static string RacksFrame { get { return "idIHI_UI_CONTROL__iframeRacks"; } }
            public static string RackTableString = "idIHI_UI_CONTROL__BATTERY__tableData";
            public static string RackPageEntries = "//select[@name='idIHI_UI_CONTROL__BATTERY__tableData_length']";
            public static string TotalRacks = "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tr";
            public static string RackEnabledStatus = "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tr[position() = {0}]/td[position() = 6 and text() = 'Enabled']";
            public static string RackZoneDropdown = "//select[@id = 'idIHI_UI_CONTROL__BATTERY__droplistZones']";
            public static string EnableAllButton = "//button[@id = 'idIHI_UI_CONTROL__BATTERY__buttonEnableAll']";
            public static string DisableAllButton = "//button[@id = 'idIHI_UI_CONTROL__BATTERY__buttonDisableAll']";
            public static string CurrentPage { get { return "#idIHI_UI_CONTROL__BATTERY__tableData_paginate > span > a.paginate_button.current"; } }
            public static string AllTabsNextIsActive { get { return "//a[@class = 'paginate_button next' and @id='idIHI_UI_CONTROL__BATTERY__tableData_next']"; } }
            public static string AllTabsNextIsInactive { get { return "//a[@class='paginate_button next disabled'][2]"; } }
            public static string AllTabsPreviousIsActive { get { return "//a[@class='paginate_button previous'][2]"; } }
            public static string AllTabsPreviousIsinactive { get { return "//a[@class='paginate_button previous disabled'][2]"; } }
            public static string RackEnableButton { get { return "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tbody//td[position() = 4 and text() = '{0}']/../td/button[@id = 'idIHI_UI_CONTROL__BATTERY__buttonEnable']"; } }
            public static string RackDisableButton { get { return "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tbody//td[position() = 4 and text() = '{0}']/../td/button[@id = 'idIHI_UI_CONTROL__BATTERY__buttonDisable']"; } }
            public static string RackEnabled { get { return "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tbody//td[6]"; } }
            public static string RackEnabledText { get { return "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tbody//td[position() = 6 and contains(text(), '{0}')]"; } }
            public static string RackStatus { get { return "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tbody//td[7]"; } }
            public static string RackStatusText { get { return "//table[@id = 'idIHI_UI_CONTROL__BATTERY__tableData']//tbody//td[position() = 7 and contains(text(), '{0}')]"; } }
        }

        /// <summary>
        /// Class for Inverters subtab specific information
        /// </summary>
        public static class Inverters
        {
            /// <summary>
            /// iFrame of Inverters
            /// </summary>
            public static string InvertersFrame { get { return "idIHI_UI_CONTROL__iframeInverters"; } }
            /// <summary>
            /// String to identify table
            /// </summary>
            public static string InvertersTableString = "idIHI_UI_CONTROL__INVERTER__tableData";
            /// <summary>
            /// By to identify table element
            /// </summary>
            public static string InvertersTable { get { return InvertersTableString; } }
            public static string CurrentPage { get { return "#idIHI_UI_CONTROL__INVERTER__tableData_paginate > span > a.paginate_button.current"; } }
            public static string AllTabsNextIsActive { get { return "//a[@class='paginate_button next'][3]"; } }
            public static string AllTabsNextIsInactive { get { return "//a[@class='paginate_button next disabled'][3]"; } }
            public static string AllTabsPreviousIsActive { get { return "//a[@class='paginate_button previous'][3]"; } }
            public static string AllTabsPreviousIsinactive { get { return "//a[@class='paginate_button previous disabled'][3]"; } }
            public static string InvertersZoneId { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[1]"; } }
            public static string InvertersZoneIdText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 1 and contains(text(), '{0}')]"; } }
            public static string InvertersId { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[2]"; } }
            public static string InvertersIdText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 2 and contains(text(), '{0}')]"; } }
            public static string InvertersLabel { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[3]"; } }
            public static string InvertersLabelText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 3 and contains(text(), '{0}')]"; } }
            public static string InvertersDesc { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[4]"; } }
            public static string InvertersDescText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 4 and contains(text(), '{0}')]"; } }
            public static string InvertersAvlb { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[5]"; } }
            public static string InvertersAvlbText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 5 and contains(text(), '{0}')]"; } }
            public static string InvertersEnbl { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[6]"; } }
            public static string InvertersEnblText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 6 and contains(text(), '{0}')]"; } }
            public static string InvertersStatus { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[7]"; } }
            public static string InvertersStatusText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 7 and contains(text(), '{0}')]"; } }
            public static string InvertersV1 { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[8]"; } }
            public static string InvertersV1Text { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 8 and contains(text(), '{0}')]"; } }
            public static string InvertersV2 { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[9]"; } }
            public static string InvertersV2Text { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 9 and contains(text(), '{0}')]"; } }
            public static string InvertersV3 { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[10]"; } }
            public static string InvertersV3Text { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 10 and contains(text(), '{0}')]"; } }
            public static string InvertersI1 { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[11]"; } }
            public static string InvertersI1Text { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 11 and contains(text(), '{0}')]"; } }
            public static string InvertersI2 { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[12]"; } }
            public static string InvertersI2Text { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 12 and contains(text(), '{0}')]"; } }
            public static string InvertersI3 { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[13]"; } }
            public static string InvertersI3Text { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 13 and contains(text(), '{0}')]"; } }
            public static string InvertersPDemand { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[14]"; } }
            public static string InvertersPDemandText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 14 and contains(text(), '{0}')]"; } }
            public static string InvertersQDemand { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[15]"; } }
            public static string InvertersQDemandText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 15 and contains(text(), '{0}')]"; } }
            public static string InvertersPActual { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[16]"; } }
            public static string InvertersPActualText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 16 and contains(text(), '{0}')]"; } }
            public static string InvertersQActual { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[17]"; } }
            public static string InvertersQActualText { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//tbody//td[position() = 17 and contains(text(), '{0}')]"; } }
            public static string InvertersEnableButton { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//td[position() = 3 and contains(text(), '{0}')]/../td/button[@id = 'idIHI_UI_CONTROL__INVERTERS__buttonEnable']"; } }
            public static string InvertersDisableButton { get { return "//div[@id = 'idIHI_UI_CONTROL__tabInverters']//table[@id = 'idIHI_UI_CONTROL__INVERTER__tableData']//td[position() = 3 and contains(text(), '{0}')]/../td/button[@id = 'idIHI_UI_CONTROL__INVERTERS__buttonDisable']"; } }
        }

        /// <summary>
        /// Class for zone Environment specific information
        /// </summary>
        public static class Environment
        {
            /// <summary>
            /// iFrame of Environment
            /// </summary>
            public static string EnvironmentFrame { get { return "idIHI_UI_CONTROL__iframeCoolers"; } }
            /// <summary>
            /// String to identify table
            /// </summary>
            public static string EnvironmentTableString = "idIHI_UI_CONTROL__COOLERS__tableData";
            /// <summary>
            /// By to identify table element
            /// </summary>
            public static string EnvironmentTable { get { return EnvironmentTableString; } }
            public static string CurrentPage { get { return "#idIHI_UI_CONTROL__COOLERS__tableData_paginate > span > a.paginate_button.current"; } }
            public static string AllTabsNextIsActive { get { return "//a[@class='paginate_button next'][4]"; } }
            public static string AllTabsNextIsInactive { get { return "//a[@class='paginate_button next disabled'][4]"; } }
            public static string AllTabsPreviousIsActive { get { return "//a[@class='paginate_button previous'][4]"; } }
            public static string AllTabsPreviousIsinactive { get { return "//a[@class='paginate_button previous disabled'][4]"; } }

        }
    }

    public static class GlobalElementsCharts
    {
        public static string ChartFrame { get { return "idIHI_UI_TABS__iframeCharts"; } }
        public static string ChartPopupLauncher { get { return "idIHI_UI_CHARTS__linkFullScreen"; } }
    }

    /// <summary>
    /// Class for the IDs on the Alarms tab
    /// </summary>
    public static class GlobalElementsAlarms
    {

        public static string AlarmsFrame { get { return "idIHI_UI_TABS__iframeAlarms"; } }
        public static string AlarmNumber { get { return "//div[@id='idIHI_UI_ALARMS__valueNumberOfAlarms']"; } }
        public static string BottomListingNumber { get { return "//div[@id='idIHI_UI_ALARMS__tableData_info']"; } }
        public static string AlarmsTableString { get { return "idIHI_UI_ALARMS__tableData"; } }
        public static string AlarmsTable { get { return "//table[@id='idIHI_UI_ALARMS__tableData']"; } }
        public static string AlarmsTableRows { get { return "//table[@id='idIHI_UI_ALARMS__tableData']/tbody/tr"; } }
        public static string AcknowledgeAllButton { get { return "//button[@id = 'idIHI_UI_ALARMS__buttonAcknowledgeAll']"; } }
        public static string DismissAllButton { get { return "//button[@id = 'idIHI_UI_ALARMS__buttonDismissAll']"; } }
        public static string NextIsActive { get { return "//a[@class='paginate_button next']"; } }
        public static string NextIsInactive { get { return "//a[@class='paginate_button next disabled']"; } }
        public static string PreviousIsActive { get { return "//a[@class='paginate_button previous']"; } }
        public static string PreviousIsinactive { get { return "//a[@class='paginate_button previous disabled']"; } }
    }

    public static class GlobalElementsEvents
    {
        public static string EventsTableString { get { return "idIHI_UI_EVENTS__tableData"; } }
        public static string EventsFrame { get { return "idIHI_UI_TABS__iframeEvents"; } }
        public static string EventNumber { get { return "//div[@id='idIHI_UI_EVENTS__valueNumberOfEvents']"; } }
        public static string BottomListingNumber { get { return "//div[@id='idIHI_UI_EVENTS__tableData_info']"; } }
        public static string EventsTable { get { return "//table[@id = 'idIHI_UI_EVENTS__tableData']"; } }
        public static string EventsTableRows { get { return "//table[@id='idIHI_UI_EVENTS__tableData']/tbody/tr"; } }
        public static string AcknowledgeAllButton { get { return "//button[@id='idIHI_UI_EVENTS__buttonAcknowledgeAll']"; } }
        public static string DismissAllButton { get { return "//button[@id='idIHI_UI_EVENTS__buttonDismissAll']"; } }
        public static string NextIsActive { get { return "//a[@class='paginate_button next']"; } }
        public static string NextIsInactive { get { return "//a[@class='paginate_button next disabled']"; } }
        public static string PreviousIsActive { get { return "//a[@class='paginate_button previous']"; } }
        public static string PreviousIsinactive { get { return "//a[@class='paginate_button previous disabled']"; } }
    }

    //list of elements on the Admin Tab
    public static class GlobalElementsAdmin
    {
        public static string AdminFrame { get { return "idIHI_UI_TABS__iframeUsers"; } }
        public static string AddNewUserButton { get { return "//button[contains(.,' Add')]"; } }
        public static string NextIsActive { get { return "//a[@class='paginate_button next' and @id = 'idIHI_UI_ADMIN__USER__tableData_next']"; } }
        public static string NextIsInactive { get { return "//a[@class='paginate_button next disabled']"; } }
        public static string UserTable { get { return "//table[@id='idIHI_UI_ADMIN__USER__tableData']"; } }
        public static string UserTableRows { get { return "//table[@id='idIHI_UI_ADMIN__USER__tableData']/tbody/tr"; } }
        public static string PreviousIsActive { get { return "//a[@class='paginate_button previous']"; } }
        public static string PreviousIsinactive { get { return "//a[@class='paginate_button previous disabled']"; } }
        public static string DeleteButtonbyUsername { get { return "//td[text() = '{0}']/..//button[contains(@id, 'idIHI_UI_ADMIN__USER__buttonDelete_')]"; } }
    }

    public static class GlobalElementsUserPopup
    {
        public static string ErrorMessage { get { return "//td[@id='idIHI_UI_USER__POPUP__valueErrorMessage']"; } }
        public static string UserNameField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxUserName']"; } }
        public static string FullNameField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxFullName']"; } }
        public static string EmailField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxEmailAddress']"; } }
        public static string PermissionsDropDown { get { return "//select[@id='idIHI_UI_USER__POPUP__droplistPermissions']"; } }
        public static string PasswordField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxPassword']"; } }
        public static string ConfirmPasswordField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxConfirmPassword']"; } }
        public static string YesButton { get { return "//button[@id='idIHI_UI_USER__POPUP__buttonOK']"; } }
        public static string NoButton { get { return "//button[@id='idIHI_UI_USER__POPUP__buttonCancel']"; } }
    }

    public static class GlobalElementsEditUserPopup
    {
        public static string ErrorMessage { get { return "//td[@id='idIHI_UI_USER__POPUP__valueErrorMessage']"; } }
        public static string UserNameField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxUserName']"; } }
        public static string FullNameField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxFullName']"; } }
        public static string EmailField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxEmailAddress']"; } }
        public static string PermissionsDropDown { get { return "//select[@id='idIHI_UI_USER__POPUP__droplistPermissions']"; } }
        public static string PasswordField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxPassword']"; } }
        public static string ConfirmPasswordField { get { return "//input[@id='idIHI_UI_USER__POPUP__textboxConfirmPassword']"; } }
        public static string YesButton { get { return "//button[@id='idIHI_UI_USER__POPUP__buttonOK']"; } }
        public static string NoButton { get { return "//button[@id='idIHI_UI_USER__POPUP__buttonCancel']"; } }
        public static string ChangePasswordSelect { get { return "//input[@id = 'idIHI_UI_USER__POPUP__checkboxChangePassword']"; } }
    }

    public static class GlobalElementsZonePopup
    {
        public static string DetailedInformation { get { return "//span[@id='idIHI_UI_TREE__POPUP_ZONE__valueDetail']"; } }
        public static string Available { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueAvailable']"; } }
        public static string Enabled { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueEnabled']"; } }
        public static string Status { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueStatus']"; } }
        public static string ZoneID { get { return "//span[@id='idIHI_UI_TREE__POPUP_ZONE__valueZone_ID']"; } }
        public static string SOC { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueAverageStateOfCharge']"; } }
        public static string CurrentRuntime { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueCurrentRuntime']"; } }
        public static string TotalRuntime { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueTotalRuntime']"; } }
        public static string DCVoltage { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueDCVoltage']"; } }
        public static string Demandp { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valuePDemand']"; } }
        public static string Demandq { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueQDemand']"; } }
        public static string Actualp { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valuePActual']"; } }
        public static string Actualq { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueQActual']"; } }
        public static string ChargeAvailablep { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valuePChargeAvailable']"; } }
        public static string DischangeAvailablep { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valuePDischargeAvailable']"; } }
        public static string LeadingAvailableq { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueQLeadingAvailable']"; } }
        public static string LaggingAvailableq { get { return "//td[@id='idIHI_UI_TREE__POPUP_ZONE__valueQLaggingAvailable']"; } }
        public static string UPSStatus { get { return "//span[@id='idIHI_UI_TREE__POPUP_ZONE__valueUPSStatus']"; } }
        public static string UPSSOC { get { return "//span[@id='idIHI_UI_TREE__POPUP_ZONE__valueUPSStateOfCharge']"; } }
        public static string TimeRemaining { get { return "//span[@id='idIHI_UI_TREE__POPUP_ZONE__valueUPSTimeRemaining']"; } }
        public static string PercentLoad { get { return "//span[@id='idIHI_UI_TREE__POPUP_ZONE__valueUPSPercentLoad']"; } }
        public static string AmbientTemp { get { return "//span[@id='idIHI_UI_TREE__POPUP_ZONE__valueUPSTemperature']"; } }

        public static string CameraButton { get { return "idIHI_UI_TREE__POPUP_ZONE__buttonCamera"; } }
    }

    public static class GlobalElementsRackPopup
    {
        public static string BatteryRackPopup { get { return "//body[@onload='IHI_UI_TREE__POPUP_BATTERY__OnLoad()']"; } }
        public static string DetailedInformation { get { return "//span[@id='idIHI_UI_TREE__POPUP_BATTERY__valueDetail']"; } }
        public static string Description { get { return "//span[@id='idIHI_UI_TREE__POPUP_BATTERY__valueDescription']"; } }
        public static string ZoneID { get { return "//span[@id='idIHI_UI_TREE__POPUP_BATTERY__valueZone_ID']"; } }
        public static string RackID { get { return "//span[@id='idIHI_UI_TREE__POPUP_BATTERY__valueRack_ID']"; } }
        public static string Type { get { return "//span[@id='idIHI_UI_TREE__POPUP_BATTERY__valueType']"; } }
        public static string Available { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueAvailable']"; } }
        public static string Enabled { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueEnabled']"; } }
        public static string Status { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueStatus']"; } }
        public static string SOC { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueStateOfCharge']"; } }
        public static string RackActualVoltage { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueActualVoltage']"; } }
        public static string RackActualCurrent { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueActualCurrent']"; } }
        public static string RackMinimumVoltage { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueMinimumVoltage']"; } }
        public static string RackMinmumCurrent { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueMinimumCurrent']"; } }
        public static string RackMaximumVoltage { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueMaximumVoltage']"; } }
        public static string RackMaximumCurrent { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueMaximumCurrent']"; } }
        public static string CellAverageVoltage { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueCellAverageVoltage']"; } }
        public static string CellAverageTemp { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueCellAverageTemperature']"; } }
        public static string CellMinimumVoltage { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueCellMinimumVoltage']"; } }
        public static string CellMinimumTemp { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueCellMinimumTemperature']"; } }
        public static string CellMaximumVoltage { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueCellMaximumVoltage']"; } }
        public static string CellMaximumTemp { get { return "//td[@id='idIHI_UI_TREE__POPUP_BATTERY__valueCellMaximumTemperature']"; } }
    }

    public static class GlobalElementsInverterPopup
    {

        public static string DetailedInformation { get { return "//span[@id='idIHI_UI_TREE__POPUP_INVERTER__valueDetail']"; } }
        public static string Description { get { return "//span[@id='idIHI_UI_TREE__POPUP_INVERTER__valueDescription']"; } }
        public static string ZonesID { get { return "//span[@id='idIHI_UI_TREE__POPUP_INVERTER__valueZone_ID']"; } }
        public static string InverterID { get { return "//span[@id='idIHI_UI_TREE__POPUP_INVERTER__valueInverters_ID']"; } }
        public static string Available { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueAvailable']"; } }
        public static string Enabled { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueEnabled']"; } }
        public static string Status { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueStatus']"; } }
        public static string kWActual { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueP_Actual']"; } }
        public static string kWDemand { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueP_Demand']"; } }
        public static string kVARActual { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueQ_Actual']"; } }
        public static string kVARDemand { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueQ_Demand']"; } }
        public static string Actualv1 { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueVoltage1_Actual']"; } }
        public static string Actualv2 { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueVoltage2_Actual']"; } }
        public static string Actualv3 { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueVoltage3_Actual']"; } }
        public static string Actuali1 { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueCurrent1_Actual']"; } }
        public static string Actuali2 { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueCurrent2_Actual']"; } }
        public static string Actuali3 { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueCurrent3_Actual']"; } }
        public static string VDCBUS { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueV_DCBus']"; } }
        public static string IDCBUS { get { return "//td[@id='idIHI_UI_TREE__POPUP_INVERTER__valueI_DCBus']"; } }
    }

    public static class GlobalElementsCoolingPopup
    {
        public static string DetailedInformation { get { return "//span[@id='idIHI_UI_TREE__POPUP_COOLER__valueDetail']"; } }
        public static string Description { get { return "//span[@id='idIHI_UI_TREE__POPUP_COOLER__valueDescription']"; } }
        public static string ZonesID { get { return "//span[@id='idIHI_UI_TREE__POPUP_COOLER__valueZone_ID']"; } }
        public static string CoolingID { get { return "//span[@id='idIHI_UI_TREE__POPUP_COOLER__valueCooler_ID']"; } }
        public static string Available { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueAvailable']"; } }
        public static string Enabled { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueEnabled']"; } }
        public static string Online { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueStatus']"; } }
        public static string SetPoint { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueSetpoint']"; } }
        public static string ReturnAirTemp { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueReturn_Air_Temp_F']"; } }
        public static string SupplyAirTemp { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueOutput_Air_Temp_F']"; } }
        public static string PercentLoad { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valuePercentLoad']"; } }
        public static string InletTemp { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueInlet_Temp_F']"; } }
        public static string OutletTemp { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueOutlet_Temp_F']"; } }
        public static string FlowRate { get { return "//td[@id='idIHI_UI_TREE__POPUP_COOLER__valueFlow_Rate_GPM']"; } }
    }

    public static class GlobalElementsPowerEnergyChart
    {
        public static string SOCSliderLabelText { get { return "//label[@id = 'idIHI_UI_DASHBOARD__POWER_METER__labelTargetSOC']"; } }
        public static string EnergyAvailableCarat { get { return "//input[@id='idIHI_UI_DASHBOARD__POWER_METER__sliderTargetSOC']"; } }
        public static string EnergyAvailableTick1 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Energy_Available_TICK1']"; } }
        public static string EnergyAvailableTick2 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Energy_Available_TICK2']"; } }
        public static string EnergyAvailableTick3 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Energy_Available_TICK3']"; } }
        public static string EnergyAvailableTick4 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Energy_Available_TICK4']"; } }
        public static string EnergyAvailableTick5 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Energy_Available_TICK5']"; } }
        public static string PTick1 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__P_TICK1']"; } }
        public static string PTick2 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__P_TICK2']"; } }
        public static string PTick3 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__P_TICK3']"; } }
        public static string PTick4 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__P_TICK4']"; } }
        public static string PTick5 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__P_TICK5']"; } }
        public static string POrigin { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__P_ORIGIN']"; } }
        public static string QTick1 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Q_TICK1']"; } }
        public static string QTick2 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Q_TICK2']"; } }
        public static string QTick3 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Q_TICK3']"; } }
        public static string QTick4 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Q_TICK4']"; } }
        public static string QTick5 { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__Q_TICK5']"; } }
        public static string QOrigin { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_METER__P_ORIGIN']"; } }
        public static string PMeterValue { get { return "//span[@id='idIHI_UI_DASHBOARD__POWER_METER__P_ACTUAL']"; } }
        public static string PMeterValueActual { get { return "//span[@id='idIHI_UI_DASHBOARD__POWER_METER__P_ACTUAL' and contains(text(), '{0}')]"; } }
        public static string QMeterValue { get { return "//span[@id='idIHI_UI_DASHBOARD__POWER_METER__Q_ACTUAL']"; } }
        public static string EnergyAvailableMeterValue { get { return "//span[@id='idIHI_UI_DASHBOARD__POWER_METER__Energy_Available_ACTUAL']"; } }
        public static string SliderText { get { return "//label[@class='classIHI_UI_DASHBOARD__POWER_METER__outputSlider']"; } }
        //public static string CaratToolTip { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_Carat']";} }
        public static string CaratToolTip { get { return "idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_caratTargetSOC"; } }
        //public static string PMeterToolTip { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_P_TOOLTIP']";} }
        //public static string QMeterToolTip { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_Q_TOOLTIP']";} }
        public static string PMeterToolTip { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_tooltipkW']"; } }
        public static string QMeterToolTip { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_tooltipkVAR']"; } }
        //public static string EnergyMeterToolTip { get { return "//div[@id='idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_KWH_TOOLTIP']";} }
        public static string EnergyMeterToolTip { get { return "idIHI_UI_DASHBOARD__POWER_UTILITY_METERS_tooltipEnergy_Available"; } }
        public static string PMeterBarGraph { get { return "//span[@id='idIHI_UI_DASHBOARD__POWER_METER__P_PERCENTAGE']"; } }
        public static string QMeterBarGraph { get { return "//span[@id='idIHI_UI_DASHBOARD__POWER_METER__Q_PERCENTAGE']"; } }
        public static string EnergyAvailableBarGraph { get { return "//span[@id='idIHI_UI_DASHBOARD__POWER_METER__KWH_PERCENTAGE']"; } }
    }

    public static class GlobalElementsCameraPopup
    {
        public static string ErrorMessage { get { return "idIHI_CAMERA__valueErrorMessage"; } }
    }

    public static class GlobalElementsDashboardAssets
    {
        public static string InverterTableString { get { return "idIHI_UI_DASHBOARD__DEVICE__tableInverters"; } }
        public static string InverterTable { get { return InverterTableString; } }
        public static string CoolingUnitTableString = "idIHI_UI_DASHBOARD__DEVICE__tableCooling";
        public static string CoolingUnitTable { get { return CoolingUnitTableString; } }
        public static string DomainControllerEnvironmentTableString = "idIHI_UI_DASHBOARD__DEVICE__tableEnvironment";
        public static string DomainControllerEnvironmentTable { get { return DomainControllerEnvironmentTableString; } }
    }

    public static class GlobalDashbaordInverterDetails
    {
        public static string InverterName { get { return "//table[@id = 'idIHI_UI_DASHBOARD__DEVICE__tableInverters']//tr[3]/td[1]"; } }
        public static string ZoneCount { get { return "//table[@id = 'idIHI_UI_DASHBOARD__DEVICE__tableInverters']//tr[3]/td[2]"; } }
        public static string InverterStatus { get { return "//table[@id = 'idIHI_UI_DASHBOARD__DEVICE__tableInverters']//tr[3]/td[3]"; } }
        public static string InverterEnalbed { get { return "//table[@id = 'idIHI_UI_DASHBOARD__DEVICE__tableInverters']//tr[3]/td[4]"; } }
        public static string InverterstatusText { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_INVERTER']//span[text()= '{0}']"; } }
    }

    public static class GlobalDashbaordTreeDetails
    {
        public static string ZoneDetails { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_ZONE']"; } }
        public static string ZoneStatus { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_ZONE']//span"; } }
        public static string ZoneStatusText { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_ZONE']//span[contains(text(), '{0}')]"; } }
        public static string RackDetails { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_BATTERIES']//td[2]"; } }
        public static string InverterStatus { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_INVERTER']//span"; } }
        public static string InverterDetails { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_INVERTER']//td[1]"; } }
        public static string InverterStatusText { get { return "//div[@id = 'idIHI_UI_TREE__Parent']//table[@class = 'classIHI_UI_TREE__TABLE_INVERTER']//span[contains(text(), '{0}')]"; } }
    }
}

