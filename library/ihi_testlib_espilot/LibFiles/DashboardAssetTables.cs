﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;
using System.Reflection;

namespace ihi_testlib_espilot
{
    public class DashboardAssetTables
    {
        public string label { get; set; }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(DashboardAssetTables assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue == "N/A" || otherValue == "N/A")
                    continue;

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }

        public string SetColor(string color)
        {
            //checks if rbga value, if not sets it to same as value
            if (!color.Contains(","))
                return color;

            //is rbga value
            if (StringDictionaries.UITextColors.ContainsValue(color))
            {
                string updatedColor = StringDictionaries.UITextColors.FirstOrDefault(x => x.Value == color).Key;
                //Console.WriteLine("Found color " + color + " " + updatedColor);
                return updatedColor;
            }
            else
            {
                return "Color not found in dictionary: " + color;
            }
        }
    }

    public class UPSInfo : DashboardAssetTables
    {
        public string status { get; set; }
        public string time_remaining { get; set; }
        public string load_percentage { get; set; }
        public string soc_percentage { get; set; }
        public string ambient_temp { get; set; }
        private string _status_color;
        public string status_color
        {
            get { return _status_color; }
            private set { _status_color = SetColor(value); }
        }

        public UPSInfo(string label, string status, string time_remaining, string load_percentage, string soc_percentage, string ambient_temp, string status_color)
        {
            this.label = label;
            this.status = status;
            this.time_remaining = time_remaining;
            this.load_percentage = load_percentage;
            this.soc_percentage = soc_percentage;
            this.ambient_temp = ambient_temp;
            this.status_color = status_color;
        }

        public UPSInfo(int index, ihi_testlib_selenium.Selenium driver)
        {
            index = index + 3;
            string table = GlobalElementsDashboardAssets.DomainControllerEnvironmentTableString;
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", table);
            try
            {
                 driver.browserDriver.SwitchTo().DefaultContent();
                MainPage.navigateToDashboard(driver);
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", table, index);
                    this.label = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", table, index);
                    this.status = driver.ReadControlsInnerText(xPath: xpath);
                    this.status_color = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", table, index);
                    this.time_remaining = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", table, index);
                    this.load_percentage = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", table, index);
                    this.soc_percentage = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[6]", table, index);
                    this.ambient_temp = driver.ReadControlsInnerText(xPath: xpath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }  
        }
    }

    public class CoolingInfoDashboard : DashboardAssetTables
    {
        public string zones_id { get; set; }
        public string online { get; set; }
        public string supply_air_temp { get; set; }
        public string return_air_temp { get; set; }
        private string _online_color;
        public string online_color
        {
            get { return _online_color; }
            private set { _online_color = SetColor(value); }
        }

        public CoolingInfoDashboard (string label, string zones_id, string online, string supply_air_temp, string return_air_temp, string online_color)
        {
            this.label = label;
            this.zones_id = zones_id;
            this.online = online;
            this.supply_air_temp = supply_air_temp;
            this.return_air_temp = return_air_temp;
            this.online_color = online_color;
        }

        public CoolingInfoDashboard (int index, ihi_testlib_selenium.Selenium driver)
        {
            index = index + 3;
            string table = GlobalElementsDashboardAssets.CoolingUnitTableString;
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", table);
            try
            {
                 driver.browserDriver.SwitchTo().DefaultContent();
                MainPage.navigateToDashboard(driver);
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", table, index);
                    this.label = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", table, index);
                    this.zones_id = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", table, index);
                    this.online = driver.ReadControlsInnerText(xPath: xpath);
                    this.online_color = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", table, index);
                    this.supply_air_temp = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", table, index);
                    this.return_air_temp = driver.ReadControlsInnerText(xPath: xpath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }   
        }
    }

    public class InverterInfoDashboard : DashboardAssetTables
    {
        public string zones_id { get; set; }
        public string status { get; set; }
        public string enabled { get; set; }
        private string _status_color;
        public string status_color
        {
            get { return this._status_color; }
            private set { _status_color = SetColor(value); }
        }
        private string _enabled_color;
        public string enabled_color
        {
            get { return this._enabled_color; }
            private set { _enabled_color = SetColor(value); }
        }

        /// <summary>
        /// Constructor to directly create all properties.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="zonesID"></param>
        /// <param name="status"></param>
        /// <param name="enabled"></param>
        /// <param name="status_color"></param>
        /// <param name="enabled_color"></param>
        public InverterInfoDashboard(string label, string zonesID, string status, string enabled, string status_color, string enabled_color)
        {
            this.label = label;
            this.zones_id = zonesID;
            this.status = status;
            this.enabled = enabled;
            this.status_color = status_color;
            this.enabled_color = enabled_color;
        }

        /// <summary>
        /// Constructor to create properties from UI page.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="driver"></param>
        public InverterInfoDashboard(int index, ihi_testlib_selenium.Selenium driver)
        {
            index = index + 3;
            string table = GlobalElementsDashboardAssets.InverterTableString;
            string xpath = string.Format("//table[@id='{0}']/tbody/tr",table);
            try
            {
                 driver.browserDriver.SwitchTo().DefaultContent();
                MainPage.navigateToDashboard(driver);
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", table, index);
                    this.label = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", table, index);
                    this.zones_id = driver.ReadControlsInnerText(xPath: xpath);
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", table, index);
                    this.status = driver.ReadControlsInnerText(xPath: xpath);
                    this.status_color = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", table, index);
                    this.enabled = driver.ReadControlsInnerText(xPath: xpath);
                    this.enabled_color = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
