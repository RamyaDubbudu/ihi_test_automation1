﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;
using System.Reflection;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Class with methods that deal with the inverter page of the UI
    /// </summary>
    public class InvertersPage
    {

        /// <summary>/// Returns the inveter info from the UI
        /// </summary>
        /// <param name="index">index based on list in UI. Index begins at 1.</param>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>InverterInfo object</returns>
        public static InverterInfo returnInverterInfo(int index, ihi_testlib_selenium.Selenium driver)
        {
            //xpaths for all tables are formated in this way with id
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Inverters.InvertersTableString);
            InverterInfo inverterInfo = new InverterInfo();
            string currentColor = "";
            
            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    int columnNumber = 0;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    string elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int element;
                    int.TryParse(elementString, out element);
                    inverterInfo.zoneID = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    inverterInfo.inverterID = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    inverterInfo.label = elementString;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    inverterInfo.description = elementString;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    inverterInfo.available = elementString;
                    currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    inverterInfo.available_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[6]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    inverterInfo.enabled = elementString;
                    currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    inverterInfo.enabled_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[7]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    inverterInfo.status = elementString;
                    currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    inverterInfo.status_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[8]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double elementDouble;
                    double.TryParse(elementString, out elementDouble);
                    inverterInfo.v1Actual = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[9]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    inverterInfo.v2Actual = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[10]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.browserDriver.FindElement(By.XPath(xpath)).Text;
                    double.TryParse(elementString, out elementDouble);
                    inverterInfo.v3Actual = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[11]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    inverterInfo.i1Actual = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[12]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    inverterInfo.i2Actual = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[13]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    inverterInfo.i3Actual = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[14]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    inverterInfo.kVADemand = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[15]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    inverterInfo.kvARDemand = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[16]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    inverterInfo.kVAActual = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[17]", GlobalElementsControl.Inverters.InvertersTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    inverterInfo.kvARActual = element;

                    return inverterInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (StaleElementReferenceException)
            {
                Console.WriteLine("Stale element found, retrying");
                return returnInverterInfo(index, driver);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Returns list of all inverter info from UI
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>List of InverterInfo for all drivers</returns>
        public static List<InverterInfo> GetAllInverterInfo(ihi_testlib_selenium.Selenium driver)
        {
            List<InverterInfo> inverterList = new List<InverterInfo>();
            int countZone = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive)))
            {
                countZone = GetNumberOfInverterRowsOnPage(driver);
                for (int i = 1; i <= countZone; i++)
                {
                    inverterList.Add(returnInverterInfo(i, driver));
                }
                driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive));
            }
            countZone = GetNumberOfInverterRowsOnPage(driver);
            for (int i = 1; i <= countZone; i++)
            {
                inverterList.Add(returnInverterInfo(i, driver));
            }
            GoToFirstPage(driver);
            return inverterList;
        }

        /// <summary>
        /// Checks how many rows (entries) are on the inverter table on current page
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>int number of how many entries are on current page</returns>
        public static int GetNumberOfInverterRowsOnPage(ihi_testlib_selenium.Selenium driver)
        {
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Inverters.InvertersTableString);

            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    IWebElement webElementBody = driver.browserDriver.FindElement(By.XPath(xpath));
                    IList<IWebElement> ElementCollectionBody = webElementBody.FindElements(By.XPath(xpath));
                    //Makes sure only entry doesn't say list is empty
                    if (ElementCollectionBody.Count == 1 && ElementCollectionBody[0].Text == "There are no inverters!")
                        return 0;

                    return ElementCollectionBody.Count;
                }
                return -1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        /// <summary>
        /// Goes to the first page of the table
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void GoToFirstPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsPreviousIsActive)))
            {
                driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsPreviousIsActive));
            }
        }

        /// <summary>
        /// Goes to Last page of the table
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void GoToLastPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive)))
            {
                driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive));
            }
        }

        /// <summary>
        /// Compare Inverter Database, ZMQ query, and UI to make sure all report same values where applicable.
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <param name="IPAddress">Linux IP address</param>
        /// <param name="userName">Linux username</param>
        /// <param name="password">Linux password</param>
        /// <param name="databaseLocation">Location of decs database</param>
        /// <returns>ControlError info list which includes ID, column of error and UI, ZMQ, and database values</returns>
        public static List<ControlErrorInfo> CompareInverterPageToZMQAndDatabase(ihi_testlib_selenium.Selenium driver, string IPAddress, string userName, string password, string databaseLocation = "/var/run/ihi-decs/")
        {
            List<ControlErrorInfo> errorList = new List<ControlErrorInfo>();
            string temp = "";
            string table = "inverters";
            string column = "";
            string ID = "";

            //order by zone id for both lists to make order reported by ZMQ irrelevant
            List<InverterTable> inverterListZMQ = IHI_ZMQ.UI__GetData_InverterList(IPAddress);
            inverterListZMQ = inverterListZMQ.OrderBy(o => o.inverters_id).ToList();

            List<InverterInfo> inverterListUI = GetAllInverterInfo(driver);
            inverterListUI = inverterListUI.OrderBy(o => o.inverterID).ToList();

            for (int i = 0; i < inverterListUI.Count; i++)
            {
                int inverterID = inverterListUI[i].inverterID;
                InverterInfo inverterDatabase = ReadInverterInfoFromDatabase(inverterID, IPAddress, userName, password, databaseLocation);
                ID = table + "_" + inverterID.ToString();


                //inverters_id does not match
                column = "inverters_id";
                if ((inverterListZMQ[i].zones_id != inverterListUI[i].zoneID) || (inverterListUI[i].zoneID != inverterDatabase.zoneID))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].zoneID.ToString(), inverterListZMQ[i].zones_id.ToString(), inverterDatabase.zoneID.ToString()));
                }

                //zoneID does not match
                column = "zone_id";
                if ((inverterListZMQ[i].zones_id != inverterListUI[i].zoneID) || (inverterListUI[i].zoneID != inverterDatabase.zoneID))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].zoneID.ToString(), inverterListZMQ[i].zones_id.ToString(), inverterDatabase.zoneID.ToString()));
                }

                //label does not match
                column = "label";
                if ((inverterListZMQ[i].label != inverterListUI[i].label) || (inverterListUI[i].label != inverterDatabase.label))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].label.ToString(), inverterListZMQ[i].label.ToString(), inverterDatabase.label.ToString()));
                }

                //description does not match
                column = "description";
                if ((inverterListZMQ[i].description != inverterListUI[i].description) || (inverterListUI[i].description != inverterDatabase.description))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].description.ToString(), inverterListZMQ[i].description.ToString(), inverterDatabase.description.ToString()));
                }

                //available does not match available
                column = "available";
                temp = StringDictionaries.ConvertDatabaseString(table, column, inverterListZMQ[i].available.ToString());
                if ((temp != inverterListUI[i].available) || (inverterListUI[i].available != inverterDatabase.available))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].available.ToString(), temp, inverterDatabase.available.ToString()));
                }

                //enabled does not match
                column = "enabled";
                temp = StringDictionaries.ConvertDatabaseString(table, column, inverterListZMQ[i].enabled.ToString());
                if ((temp != inverterListUI[i].enabled) || (inverterListUI[i].enabled != inverterDatabase.enabled))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].enabled.ToString(), temp, inverterDatabase.enabled.ToString()));
                }

                //status does not match
                column = "state_actual";
                temp = StringDictionaries.ConvertDatabaseString(table, column, inverterListZMQ[i].state_actual.ToString());
                if ((temp != inverterListUI[i].status) || (inverterListUI[i].status != inverterDatabase.status))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].status.ToString(), temp, inverterDatabase.status.ToString()));
                }

                //v1Actual does not match
                column = "v1Actual";
                if ((inverterListZMQ[i].v1_actual != inverterListUI[i].v1Actual) || (inverterListUI[i].v1Actual != inverterDatabase.v1Actual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].v1Actual.ToString(), inverterListZMQ[i].v1_actual.ToString(), inverterDatabase.v1Actual.ToString()));
                }

                //v2Actual does not match
                column = "v2Actual";
                if ((inverterListZMQ[i].v2_actual != inverterListUI[i].v2Actual) || (inverterListUI[i].v2Actual != inverterDatabase.v2Actual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].v2Actual.ToString(), inverterListZMQ[i].v2_actual.ToString(), inverterDatabase.v2Actual.ToString()));
                }

                //v3Actual does not match
                column = "v3Actual";
                if ((inverterListZMQ[i].v3_actual != inverterListUI[i].v3Actual) || (inverterListUI[i].v3Actual != inverterDatabase.v3Actual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].v3Actual.ToString(), inverterListZMQ[i].v3_actual.ToString(), inverterDatabase.v3Actual.ToString()));
                }

                //i1Actual does not match
                column = "i1Actual";
                if ((inverterListZMQ[i].i1_actual != inverterListUI[i].i1Actual) || (inverterListUI[i].i1Actual != inverterDatabase.i1Actual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].i1Actual.ToString(), inverterListZMQ[i].i1_actual.ToString(), inverterDatabase.i1Actual.ToString()));
                }

                //i2Actual does not match
                column = "i2Actual";
                if ((inverterListZMQ[i].i2_actual != inverterListUI[i].i2Actual) || (inverterListUI[i].i2Actual != inverterDatabase.i2Actual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].i2Actual.ToString(), inverterListZMQ[i].i2_actual.ToString(), inverterDatabase.i2Actual.ToString()));
                }

                //i3Actual does not match
                column = "i3Actual";
                if ((inverterListZMQ[i].i3_actual != inverterListUI[i].i3Actual) || (inverterListUI[i].i3Actual != inverterDatabase.i3Actual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].i3Actual.ToString(), inverterListZMQ[i].i3_actual.ToString(), inverterDatabase.i3Actual.ToString()));
                }

                //kVADemand does not match
                column = "kVADemand";
                if ((inverterListZMQ[i].p_demand != inverterListUI[i].kVADemand) || (inverterListUI[i].kVADemand != inverterDatabase.kVADemand))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].kVADemand.ToString(), inverterListZMQ[i].p_demand.ToString(), inverterDatabase.kVADemand.ToString()));
                }

                //kvARDemand does not match
                column = "kvARDemand";
                if ((inverterListZMQ[i].q_demand != inverterListUI[i].kvARDemand) || (inverterListUI[i].kvARDemand != inverterDatabase.kvARDemand))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].kvARDemand.ToString(), inverterListZMQ[i].q_demand.ToString(), inverterDatabase.kvARDemand.ToString()));
                }

                //kVAActual does not match
                column = "kVAActual";
                if ((inverterListZMQ[i].p_actual != inverterListUI[i].kVAActual) || (inverterListUI[i].kVAActual != inverterDatabase.kVAActual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].kVAActual.ToString(), inverterListZMQ[i].p_actual.ToString(), inverterDatabase.kVAActual.ToString()));
                }

                //kvARActual does not match
                column = "kvARActual";
                if ((inverterListZMQ[i].q_actual != inverterListUI[i].kvARActual) || (inverterListUI[i].kvARActual != inverterDatabase.kvARActual))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, inverterListUI[i].kvARActual.ToString(), inverterListZMQ[i].q_actual.ToString(), inverterDatabase.kvARActual.ToString()));
                }
            }

            return errorList;
        }

        /// <summary>
        /// Read all InverterInfo values from database and stores them in a RackInfo file
        /// </summary>
        /// <param name="inverterID">rack id of which to check</param>
        /// <param name="IPAddress">Linux IP address</param>
        /// <param name="userName">Linux username</param>
        /// <param name="password">Linux password</param>
        /// <param name="databaseLocation">Location of decs database</param>
        /// <returns></returns>
        public static InverterInfo ReadInverterInfoFromDatabase(int inverterID, string IPAddress, 
            string userName, string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            InverterInfo inverterDatabase = new InverterInfo();
            string table = "inverters";
            string format = "";
            string column = "";
            string temp = "";

            //rackID - taken from parameters
            inverterDatabase.inverterID = inverterID;

            SshClient ssh = new SshClient(IPAddress, userName, password);
            ssh.Connect();

            //zoneID
            column = "zones_id";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.zoneID = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //label
            column = "label";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.label = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //description
            column = "description";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.description = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //available
            column = "available";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.available = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //enabled
            column = "enabled";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.enabled = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //status
            column = "state_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.status = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //v1_actual
            column = "v1_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.v1Actual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //v2_actual
            column = "v2_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.v2Actual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //v3_actual
            column = "v3_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.v3Actual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //i1_actual
            column = "i1_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.i1Actual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //i2_actual
            column = "i2_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.i2Actual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //i3_actual
            column = "i3_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.i3Actual = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //p_demand
            column = "p_demand";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.kVADemand = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //q_demand
            column = "q_demand";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.kvARDemand = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //p_actual
            column = "p_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.kVAActual = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //q_actual
            column = "q_actual";
            format = string.Format("SELECT {0} from {1} where inverters_id='{2}'", column, table, inverterID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            inverterDatabase.kvARActual = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            ssh.Disconnect();

            return inverterDatabase;
        }

        public static void CreateMoreInverters(int numberToCreate, SSHAndDatabaseDirectory connectionInfo, int zoneID = 1, string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(connectionInfo.SSHIPAddress, connectionInfo.SSHUsername, connectionInfo.SSHPassword);
            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            string table = "inverters";

            List<InverterTable> inverterListZMQ = IHI_ZMQ.UI__GetData_InverterList(connectionInfo.SSHIPAddress);

            //find min and max values to know where to copy from and where to put new entry
            int maxID = inverterListZMQ.Max(t => t.inverters_id);
            int minID = inverterListZMQ.Min(t => t.inverters_id);

            InverterTable inverterTable = new InverterTable();

            List<string> propertyNames = inverterTable.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }
            for (int i = 0; i < numberToCreate; i++)
            {
                string prototypeSelect = (++maxID) + ", ";
                foreach (string item in propertyNames)
                {
                    if (item == firstItem)
                        continue;

                    if (item != lastItem)
                        prototypeSelect += item + (", ");
                    else
                        prototypeSelect += item + " ";
                }

                string sqlStatement = string.Format("INSERT into {0}{1}SELECT {2}from {0} where {3}={4}", table,
                    prototypeInto, prototypeSelect, firstItem, minID);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
                //update zone id
                sqlStatement = string.Format("UPDATE inverters SET zones_id='{0}' WHERE inverters_id={1}", zoneID, maxID);
                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
            }
            ssh.Disconnect();
        }

        /// <summary>
        /// Takes in a list of inverter tables (usually from zmq), clears the database and loads in only these values.  Useful when making tests non-descructive
        /// </summary>
        /// <param name="zoneList"></param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static void UpdateDatabaseFromInvertersTableList(List<InverterTable> inverterList, string IPAddress, 
            string userName, string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(IPAddress, userName, password);

            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            string table = "inverters";

            SSH.ClearSQLTable(databaseLocation, "decs_dc.db", table, IPAddress, userName, password);

            InverterTable inverterTableTemp = new InverterTable();
            List<string> propertyNames = inverterTableTemp.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            //the following code is formating the sql insert into select statement
            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }

            foreach (InverterTable inverterTable in inverterList)
            {
                List<string> objectStrings = inverterTable.GetPropertyValues();
                string last = objectStrings.Last();

                string prototypeSelect = " (";
                int findLastLoop = 1;
                foreach (string value in objectStrings)
                {
                    if (!(findLastLoop == objectStrings.Count))
                        prototypeSelect += value + (", ");
                    else
                        prototypeSelect += value + (") ");
                    findLastLoop++;
                }
                string sqlStatement = string.Format("INSERT INTO {0}{1} VALUES {2}", table, prototypeInto, prototypeSelect);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, IPAddress, decsLocation);
            }

            ssh.Disconnect();
        }

        /// <summary>
        /// Goes through pages and disables all assets. Returns a list of values that did not change by ID.
        /// </summary>
        /// <param name="timeout">Length allowed for change</param>
        /// <param name="driver">Firefox driver</param>
        /// <returns></returns>
        public static List<int> EnableAllInverters(int timeout, ihi_testlib_selenium.Selenium driver)
        {
            int page = 0;
            int countAsset = 0;
            bool success = false;
            int elapsed = 0;
            List<int> wrongValue = new List<int>();

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive)))
            {
                page++;
                countAsset = GetNumberOfInverterRowsOnPage(driver);
                for (int i = 1; i <= countAsset; i++)
                {
                    success = false;
                    elapsed = 0;
                    ControlPage.ClickEnableButton(i, "INVERTERS", driver);

                    while (elapsed < timeout)
                    {
                        var assetInfo = returnInverterInfo(i, driver);

                        if ((assetInfo != null) && ("Enabled" == assetInfo.enabled))
                        {
                            success = true;
                            break;
                        }
                        elapsed += 1000;
                        System.Threading.Thread.Sleep(1000);
                    }

                    if (success == false)
                    {
                        //adds zone id to list of values that did not change
                        wrongValue.Add(returnInverterInfo(i, driver).zoneID);
                    }
                }
                //checks that still on current page, returns -1 if not
                int uiPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Inverters.CurrentPage)));
                if (uiPage != page)
                    wrongValue.Add(-1);


                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive));
            }

            page++;
            //Last page
            countAsset = GetNumberOfInverterRowsOnPage(driver);
            for (int i = 1; i <= countAsset; i++)
            {
                success = false;
                elapsed = 0;
                ControlPage.ClickEnableButton(i, "INVERTERS", driver);

                while (elapsed < timeout)
                {
                    var assetInfo = returnInverterInfo(i, driver);

                    if ((assetInfo != null) && ("Enabled" == assetInfo.enabled))
                    {
                        success = true;
                        break;
                    }
                    elapsed += 1000;
                    System.Threading.Thread.Sleep(1000);
                }

                //if value is not enabled after timeout
                if (success == false)
                {
                    //adds zone id to list of values that did not change
                    wrongValue.Add(returnInverterInfo(i, driver).zoneID);
                }
            }
            int lastUIPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Inverters.CurrentPage)));
            if (lastUIPage != page)
                wrongValue.Add(-1);

            GoToFirstPage(driver);
            return wrongValue;
        }

        /// <summary>
        /// Goes through pages and disables all assets. Returns a list of values that did not change by ID.
        /// </summary>
        /// <param name="timeout">Length allowed for change</param>
        /// <param name="driver">Firefox driver</param>
        /// <returns></returns>
        public static List<int> DisableAllInverters(int timeout, ihi_testlib_selenium.Selenium driver)
        {
            int page = 0;
            int countAsset = 0;
            bool success = false;
            int elapsed = 0;
            List<int> wrongValue = new List<int>();

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive)))
            {
                page++;
                countAsset = GetNumberOfInverterRowsOnPage(driver);
                for (int i = 1; i <= countAsset; i++)
                {
                    success = false;
                    elapsed = 0;
                    ControlPage.ClickDisableButton(i, "INVERTERS", driver);

                    while (elapsed < timeout)
                    {
                        var assetInfo = returnInverterInfo(i, driver);
                        //ensures that were able to get information first
                        if ((assetInfo != null) && ("Disabled" == assetInfo.enabled))
                        {
                            success = true;
                            break;
                        }
                        elapsed += 1000;
                        System.Threading.Thread.Sleep(1000);
                    }

                    if (success == false)
                    {
                        //adds zone id to list of values that did not change
                        wrongValue.Add(returnInverterInfo(i, driver).inverterID);
                    }
                }
                //checks that still on current page, returns -1 if not
                int uiPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Inverters.CurrentPage)));
                if (uiPage != page)
                    wrongValue.Add(-1);


                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Inverters.AllTabsNextIsActive));
            }

            page++;
            //Last page
            countAsset = GetNumberOfInverterRowsOnPage(driver);
            for (int i = 1; i <= countAsset; i++)
            {
                success = false;
                elapsed = 0;
                ControlPage.ClickDisableButton(i, "INVERTERS", driver);

                while (elapsed < timeout)
                {
                    var assetInfo = returnInverterInfo(i, driver);
                    //ensures that were able to get information first
                    if ((assetInfo != null) && ("Disabled" == assetInfo.enabled))
                    {
                        success = true;
                        break;
                    }
                    elapsed += 1000;
                    System.Threading.Thread.Sleep(1000);
                }

                //if value is not enabled after timeout
                if (success == false)
                {
                    //adds zone id to list of values that did not change
                    wrongValue.Add(returnInverterInfo(i, driver).inverterID);
                }
            }
            int lastUIPage = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Inverters.CurrentPage)));
            if (lastUIPage != page)
                wrongValue.Add(-1);

            GoToFirstPage(driver);
            return wrongValue;
        }
    }

    public class InverterInfo
    {
        public int zoneID { get; set; }
        public int inverterID { get; set; }
        public string description { get; set; }
        public string label { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string status { get; set; }
        public double v1Actual { get; set; }
        public double v2Actual { get; set; }
        public double v3Actual { get; set; }
        public double i1Actual { get; set; }
        public double i2Actual { get; set; }
        public double i3Actual { get; set; }
        public int kVADemand { get; set; }
        public int kvARDemand { get; set; }
        public int kVAActual { get; set; }
        public int kvARActual { get; set; }
        private string m_available_color;
        public string available_color
        {
            get { return this.m_available_color; }
            set { m_available_color = SetColor(value); }
        }
        private string m_enable_color;
        public string enabled_color
        {
            get { return this.m_available_color; }
            set { m_enable_color = SetColor(value); }
        }
        private string m_status_color;
        public string status_color
        {
            get { return this.m_status_color; }
            set { m_status_color = SetColor(value); }
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(InverterInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue == "N/A" || otherValue == "N/A")
                {
                    continue;
                }

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }

        private string SetColor(string color)
        {
            //checks if rbga value, if not sets it to same as value
            if (!color.Contains(","))
                return color;

            //is rbga value
            if (StringDictionaries.UITextColors.ContainsValue(color))
            {
                string updatedColor = StringDictionaries.UITextColors.FirstOrDefault(x => x.Value == color).Key;
                //Console.WriteLine("Found color " + color + " " + updatedColor);
                return updatedColor;
            }
            else
            {
                return "Color not found in dictionary: " + color;
            }
        }

    }
}
