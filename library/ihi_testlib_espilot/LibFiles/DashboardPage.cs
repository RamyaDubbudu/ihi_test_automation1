﻿//DashboarPage.cs
using System;
using System.Threading;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
//using OpenQA.Selenium.Chrome;

namespace ihi_testlib_espilot
{

    public static class DashboardPage
    {
        /// <summary>
        /// Checks to see if mode changes within timeout
        /// </summary>
        /// <param name="desiredMode"></param>
        /// <param name="timeout"></param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool checkChangeMode(string desiredMode, int timeout, ihi_testlib_selenium.Selenium driver)
        {
            bool success = false;
            int elapsed = 0;
            while ((!success) && (elapsed < timeout))
            {
                Thread.Sleep(1000);
                elapsed += 1000;
                success = (desiredMode == driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.CurrentMode)));
            }
            return success;
        }

        /// <summary>
        /// Checks to see if mode changes within timeout
        /// </summary>
        /// <param name="desiredMode"></param>
        /// <param name="timeout"></param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static string GetCurrenteMode(int timeout, ihi_testlib_selenium.Selenium driver)
        {
            string currentMode = "";
            int elapsed = 0;
            while ((elapsed < timeout))
            {
                Thread.Sleep(2000);
                elapsed += 2000;
                currentMode = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.CurrentMode));
            }
            return currentMode;
        }
        /// <summary>
        /// clicks on map and checks if map opens, closes, and returns true if open and closed properly
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool CheckOpenMap(ihi_testlib_selenium.Selenium driver)
        {

            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsDashboard.Map));
            if (driver.SwitchtoWindow(driver, "DECS - Map"))
            {
                driver.browserDriver.Close();
                driver.SwitchtoWindow(driver, "DECS - Dashboard");
                return true;
            }
            else
            {
              driver.SwitchtoWindow(driver, "DECS - Dashboard");
                return false;
            }
        }

        /// <summary>
        /// Takes in a list of zone tables (usually from zmq), clears the database and loads in only these values.  Useful when making tests non-descructive
        /// </summary>
        /// <param name="zoneList"></param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static bool UpdateDatabaseFromSiteTableList(List<SiteTable> siteList, string IPAddress, string userName, string password, 
            string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(IPAddress, userName, password);

            string table = "site";

            //SiteTable zoneTableTemp = new SiteTable();
            List<string> propertyNames = siteList[0].GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            //the following code is formating the sql insert into select statement
            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }

            foreach (SiteTable siteTable in siteList)
            {
                List<string> objectStrings = siteTable.GetPropertyValues();
                string last = objectStrings.Last();

                string prototypeSelect = "";
                int findLastLoop = 1;

                int nameLoop = 0;
                foreach (string value in objectStrings)
                {
                    if (!(findLastLoop == objectStrings.Count))
                        prototypeSelect += propertyNames[nameLoop] + " = " + value + ", ";
                    else
                        prototypeSelect += propertyNames[nameLoop] + " = " + value;
                    nameLoop++;
                    findLastLoop++;
                }
                string sqlStatement = string.Format("UPDATE {0} SET {1}", table, prototypeSelect);
                //Console.WriteLine(sqlStatement);
                if (propertyNames.Count == objectStrings.Count)
                {
                    //SSH.ClearSQLTable(databaseLocation, "decs_dc.db", table, IPAddress, userName, password);
                    QueryInterface.CommandThroughQuery(sqlStatement, ssh, IPAddress, decsLocation);
                }
                else
                {
                    Console.WriteLine("Name and value tables not equal");
                    string eventsClear = string.Format("echo \"{3}\" | sudo -S sqlite3 {0}{1} \"DELETE FROM {2}\"\n", databaseLocation, "decs_events.db", "events", password);
                    SSH.OpenWrite(ssh, eventsClear);
                    return false;
                }
            }
            //*****Band-aid******, Clears the Events table because this method causes it to fill up.  This should increase test speed and reduce errors, but should probably change how the database updates
            string eventsClearend = string.Format("echo \"{3}\" | sudo -S sqlite3 {0}{1} \"DELETE FROM {2}\"\n", databaseLocation, "decs_events.db", "events", password);
            ssh.Connect();
            SSH.OpenWrite(ssh, eventsClearend);
            ssh.Disconnect();

            return true;
        }

        /// <summary>
        /// Navigate to the power meter bullet graph on the dashboard.
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool NavigateToPowerMeter(ihi_testlib_selenium.Selenium driver)
        {
             driver.browserDriver.SwitchTo().DefaultContent();
            MainPage.navigateToDashboard(driver);
            if (driver.IsElementVisible(idTXT: "idIHI_UI_DASHBOARD__POWER_METER"))
            {
                //////////driver.browserDriver.SwitchTo().Frame(driver.browserDriver.FindElement(By.Id("idIHI_UI_DASHBOARD__POWER_METER")));
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sets the SOC slider by percentage
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="slider"></param>
        /// <param name="percent"></param>
        /// <returns></returns>
        public static bool SetSOCSlider(ihi_testlib_selenium.Selenium driver, int percent)
        {
            MainPage.navigateToDashboard(driver);
            IWebElement slider =driver.FindHTMLControl(xPath: string.Format(GlobalElementsPowerEnergyChart.EnergyAvailableCarat));

            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsDashboard.Description));
            Actions builder = new Actions(driver.browserDriver);

            int maxTab = 300;
            int tabNumber = 0;
            while (!Equals(driver.browserDriver.SwitchTo().ActiveElement(), slider))
            {
                builder.SendKeys(Keys.Tab).Perform();
                tabNumber++;
                if (tabNumber > maxTab)
                    break;
            }

            var arrowUp = builder.SendKeys(Keys.ArrowRight).Build();
            var arrorwDown = builder.SendKeys(Keys.ArrowLeft).Build();

            int uiPercent = GetSliderValue(driver);
            if (uiPercent == -1)
            {
                return false;
            }
            int loopTrack = 0;
            do
            {
                if (uiPercent > percent)
                {
                    arrorwDown.Perform();
                    if (driver.GetType() == typeof (ChromeDriver))
                    {
                        //Chrome will post an alert every time the slider updates,  therefore the slider has to be cleared
                        GeneralDECS.CloseAlertAndGetItsText(driver);
                        Thread.Sleep(200);
                    }
                }
                else if (uiPercent < percent)
                {
                    arrowUp.Perform();
                    //Chrome will post an alert every time the slider updates,  therefore the slider has to be cleared
                    if (driver.GetType() == typeof (ChromeDriver))
                    {
                        GeneralDECS.CloseAlertAndGetItsText(driver);
                        Thread.Sleep(200);
                    }
                }

                uiPercent = GetSliderValue(driver);

                if (uiPercent == percent)
                {
                    builder.SendKeys(Keys.Tab).Perform();
                    if (GeneralDECS.CheckIfAlertIsActive(driver))
                    {
                        GeneralDECS.CloseAlertAndGetItsText(driver);
                    }
                    return true;
                }

                //will fail after 100 tries because percentage
                loopTrack++;
                if (loopTrack > 100)
                    break;

            } while (uiPercent != percent);

            return false;
        }

        /// <summary>
        /// Returns the slider value or -1 if error
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static int GetSliderValue(ihi_testlib_selenium.Selenium driver)
        {
            int uiPercent = -1;
            IWebElement slider = driver.FindHTMLControl(xPath: string.Format(GlobalElementsPowerEnergyChart.EnergyAvailableCarat));
            int.TryParse(slider.GetAttribute("value"), out uiPercent);
            return uiPercent;
        }

        

        public static void ChkInverterOnlineAndUpdateToManualMode(ihi_testlib_selenium.Selenium webDriver)
        {
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.ZonesTab));
            System.Threading.Thread.Sleep(2000);
            //Enable all Zones
            List<ZoneInfo> zoneInfo = ZonesPage.GetAllZoneInfo(webDriver);
            foreach (var zInfo in zoneInfo)
            {
                if (zInfo.enabled != "Enabled")
                {
                    webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Zones.ZoneEnableButton, zInfo.description));
                    webDriver.WaitForElement(xpath: string.Format(GlobalElementsControl.Zones.ZoneEnabledText, "Enabled"));
                }
            }
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.RacksTab));
            System.Threading.Thread.Sleep(2000);
            //Enable all Racks
            RackInfo rackInfos = RacksPage.returnRackInfo(1, webDriver);
            {
                if (rackInfos.enabled != "Enabled")
                {
                    List<RackInfo> rackInfo = RacksPage.GetAllRackInfo(webDriver);
                    foreach (var rInfo in rackInfo)
                    {
                        if (rInfo.enabled != "Enabled")
                        {
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Racks.RackEnableButton, rInfo.description));
                            webDriver.WaitForElement(xpath: string.Format(GlobalElementsControl.Racks.RackEnabledText, "Enabled"));
                        }
                    }
                }
            }
            //Navigate to Dashboard
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.DashboardTab));
            webDriver.WaitForElement(xpath: string.Format(GlobalStartupShutdown.Dropdown));
            //Change the Mode to Manual Mode and click on Go button
            new SelectElement(webDriver.browserDriver.FindElement(By.XPath(GlobalStartupShutdown.DropdownList))).SelectByText("Manual Mode");
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.GoButton));
            webDriver.HandleAlertBox();
            //Validate that the Inverter status is changed to Online
            System.Threading.Thread.Sleep(6000);
            webDriver.WaitForElement(xpath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")), numberOfTimes:250);
            webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.CurrentModeText, "Manual Mode"), numberOfTimes: 250);
            webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.ZoneStatusText, "Online"), numberOfTimes: 120);
            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
        }

        public static void Change2Standby_CheckBatteryConnection(ihi_testlib_selenium.Selenium webDriver)
        {
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.DashboardTab));
            webDriver.WaitForElement(xpath: string.Format(GlobalStartupShutdown.Dropdown));
            //Change the Mode to Manual Mode and click on Go button
            new SelectElement(webDriver.browserDriver.FindElement(By.XPath(GlobalStartupShutdown.DropdownList))).SelectByText("Standby");
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.GoButton));
            webDriver.HandleAlertBox();
            webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.CurrentModeText, "Standby"), numberOfTimes: 120);
            //Validate that the Inverter status is changed to Online
            DashboardPage.checkChangeMode("Standby", 6000, webDriver);
            ZonesPage.CheckAndSetAllZonesStatus("Enabled", webDriver);
            RacksPage.CheckAllRacksStatus("Connected", webDriver);
        }

        public static bool ChangeModeAs(ihi_testlib_selenium.Selenium webDriver, string ModeType)
        {
           bool isModeChanged = false;
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.DashboardTab));
            webDriver.WaitForElement(xpath: string.Format(GlobalStartupShutdown.Dropdown));
            //Change the Mode to Manual Mode and click on Go button
            new SelectElement(webDriver.browserDriver.FindElement(By.XPath(GlobalStartupShutdown.DropdownList))).SelectByText(ModeType);
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.GoButton));
            webDriver.HandleAlertBox();
            //Validate that the Inverter status is changed to Online
            webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.CurrentModeText, ModeType), numberOfTimes: 120);
            isModeChanged = DashboardPage.checkChangeMode(ModeType, 6000, webDriver);
            return isModeChanged;
        }

        public static void ChangeMode(ihi_testlib_selenium.Selenium webDriver, string ModeType)
        {
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.DashboardTab));
            webDriver.WaitForElement(xpath: string.Format(GlobalStartupShutdown.Dropdown));
            //Change the Mode to Manual Mode and click on Go button
            new SelectElement(webDriver.browserDriver.FindElement(By.XPath(GlobalStartupShutdown.DropdownList))).SelectByText(ModeType);
            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.GoButton));
            webDriver.HandleAlertBox();
            System.Threading.Thread.Sleep(3000);
            webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.CurrentModeText, ModeType), numberOfTimes: 120);
        }
    }
}