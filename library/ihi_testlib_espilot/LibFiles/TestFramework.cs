﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public static class TestFramework
    {
        private static string resultFormatTrue = "{0} unexpectedly does not equal {1}. {0}: {2}. {1}: {3}.";
        private static string resultFormatFalse = "{0} unexpectedly is equal to {1}. {0}: {2}. {1}: {3}.";

        //public static bool AssertEquals(string result1, string result2, TestStandResults teststandData, string result1Description = "firstVariable", string result2Description = "secondVariable")
        //{

        //}

        //public static bool AssertEquals(int result1, int result2, TestStandResults teststandData)
        //{

        //}

        //public static bool AssertEquals(double result1, double result2, TestStandResults teststandData)
        //{

        //}

        public static bool AssertEquals(object result1, object result2, TestStandResults teststandData, string result1Description = "firstVariable", string result2Description = "secondVariable")
        {
            if (result1 == result2)
                return true;

            var formatedResult = string.Format(resultFormatTrue, result1Description,
                result2Description);
            teststandData.AppendReportText(formatedResult);
            teststandData.TestFailed();
            return false;
        }

        public static bool AssertFalse(object result1, object result2, TestStandResults teststandData, string result1Description = "firstVariable", string result2Description = "secondVariable")
        {
            if (result1 != result2)
                return true;

            var formatedResult = string.Format(resultFormatFalse, result1Description,
                result2Description);
            teststandData.AppendReportText(formatedResult);
            teststandData.TestFailed();
            return false;
        }
    }
}
