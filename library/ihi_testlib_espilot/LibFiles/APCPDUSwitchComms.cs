﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Timers;
using Renci.SshNet;
using TelnetExpect;

namespace ihi_testlib_espilot
{
    public static class APCPDUSwitchComms
    {
        /// <summary>
        /// Enables/Disables a port on the switch by port number
        /// </summary>
        /// <param name="enableDisable">True = enable, False = disable</param>
        /// <param name="telnetInfo">Class that houses IPAddress, Username and Password of switch</param>
        /// <param name="port">Port number to change</param>
        /// <returns></returns>
        public static bool SetSwitchPortOnOff(bool enableDisable, APCTelnetInfo telnetInfo, int port)
        {
            var enableDisableCommand = enableDisable ? "on" : "off";
            string formatCommand = enableDisableCommand + " " + port + "\r\n";
            TelnetConnection telnet = null;
            try
            {
                telnet = Login(telnetInfo);
                telnet.Write(formatCommand);
                telnet.Read();
                telnet.Write("exit" + "\r\n");
                telnet.tcpSocket.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (telnet != null) 
                    telnet.tcpSocket.Close();
                return false;
            }
        }

        /// <summary>
        /// Logs into the APC PDU
        /// </summary>
        /// <param name="telnetInfo"></param>
        /// <returns></returns>
        private static TelnetConnection Login(APCTelnetInfo telnetInfo)
        {
            TelnetConnection telnet = null;
            try
            {
                telnet = new TelnetConnection(telnetInfo.IPAddress, 23);
                telnet.Read();
                telnet.Write(telnetInfo.Login + "\r\n");
                telnet.Read();
                telnet.Write(telnetInfo.Password + "\r\n");
                telnet.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return telnet; 
        }

        /// <summary>
        /// Returns a list of lists for the enabled and disabled ports
        /// </summary>
        /// <param name="telnetInfo"></param>
        /// <returns></returns>
        public static EnabledDisabed CheckAllPortStatus(APCTelnetInfo telnetInfo)
        {
            EnabledDisabed enabledDisabledCombined = new EnabledDisabed();
            enabledDisabledCombined.Enabled = new List<int>();
            enabledDisabledCombined.Disabled = new List<int>();
            TelnetConnection telnet = null;
            try
            {
                telnet = Login(telnetInfo);

                for (int i = 1; i <= 8; i++)
                {
                    telnet.Write(string.Format("status {0}\r\n", i));
                    var status = telnet.Read();
                    if (status.Contains("ON"))
                        enabledDisabledCombined.Enabled.Add(i);
                    else if (status.Contains("OFF"))
                        enabledDisabledCombined.Disabled.Add(i);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (telnet != null)
                    telnet.tcpSocket.Close();
            }

            return enabledDisabledCombined;
        }

        public class EnabledDisabed
        {
            public List<int> Enabled { get; set; }
            public List<int> Disabled { get; set; }
        }

        public static bool RebootOutlet(APCTelnetInfo telnetInfo, int port)
        {
            TelnetConnection telnet = null;
            string formatCommand = "reboot " + port + "\r\n";
            try
            {
                telnet = Login(telnetInfo);
                telnet.Write(formatCommand);
                telnet.Read();
                telnet.Write("exit" + "\r\n");
                telnet.tcpSocket.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (telnet != null)
                    telnet.tcpSocket.Close();
                return false;
            }
        }

        public static void SetAllPortsOn(APCTelnetInfo telnetInfo)
        {
            for (int i = 1; i <= 8; i++)
            {
                SetSwitchPortOnOff(true, telnetInfo, i);
            }
        }
    }

    /// <summary>
    /// Houses APC 79xx information to connect to telnet
    /// </summary>
    public class APCTelnetInfo
    {
        public string IPAddress { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public APCTelnetInfo(string ipaddress, string login, string password)
        {
            this.IPAddress = ipaddress;
            this.Login = login;
            this.Password = password + " -c";
        }
    }
}
