﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public class Profile
    {
        public readonly int Index;
        public readonly string Name;
        public readonly string Color;

        public Profile(int index, string name, string color)
        {
            this.Index = index;
            this.Name = name;
            this.Color = color;
        }
    }
}
