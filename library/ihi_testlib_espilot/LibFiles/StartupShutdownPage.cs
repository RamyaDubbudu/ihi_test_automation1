﻿//StarupShutdownPage.cs
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Renci.SshNet;
using ihi_testlib_selenium;
using NUnit.Framework;

//using OpenQA.Selenium.Chrome;

namespace ihi_testlib_espilot
{

    public static class StartupShutdownPage
    {
        private static bool acceptNextAlert = true;

        //Selects mode selector drop down, selects mode based on text, and confirms selection
        public static void changeMode(string Modeselection, ihi_testlib_selenium.Selenium driver)
        {
            MainPage.navigateToDashboard(driver);
            new SelectElement(driver.browserDriver.FindElement(By.XPath(GlobalStartupShutdown.Dropdown))).SelectByText(
           Modeselection);
            // new SelectElement(driver.browserDriver.FindElement(GlobalStartupShutdown.Dropdown)).SelectByText(Modeselection);
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.GoButton));
            Assert.IsTrue(Regex.IsMatch(CloseAlertAndGetItsText(driver), "^Are you sure[\\s\\S]$"));
        }

        /// <summary>
        /// Selects profile by name
        /// </summary>
        /// <param name="profileSelection"></param>
        /// <param name="driver"></param>
        public static void ChangeProfile(string profileSelection, ihi_testlib_selenium.Selenium driver)
        {
            //profileSelection = "Profile";
            MainPage.navigateToDashboard(driver);
            new SelectElement(driver.browserDriver.FindElement(By.XPath(GlobalStartupShutdown.ProfileDropDown))).SelectByText(
            profileSelection);
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.ProfileApplyButton));
            //Assert.IsTrue(Regex.IsMatch(CloseAlertAndGetItsText(driver), "^Are you sure[\\s\\S]$"));
            
        }

        /// <summary>
        /// Returns a list of string for all drop down selections for mode
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static List<string> GetAllDropDownOptions(ihi_testlib_selenium.Selenium driver)
        {
            List<string> dropDownStrings = new List<string>();
            int dropdownitems = driver.GetControlsTotalCount(xPath: string.Format(GlobalStartupShutdown.DropdownList));
            if (dropdownitems != 0)
            {
                if (driver.IsElementVisible(xPath: string.Format(GlobalStartupShutdown.Dropdown)))
                {
                    IWebElement dropDownList = driver.FindHTMLControl(xPath: string.Format(GlobalStartupShutdown.Dropdown));
                    SelectElement selectList = new SelectElement(dropDownList);
                    IList<IWebElement> options = selectList.Options;

                    foreach (IWebElement selection in options)
                    {
                        dropDownStrings.Add(selection.Text);
                    }

                }

                return dropDownStrings;
            }
            else
              return  dropDownStrings = null;
        }

        /// <summary>
        /// Enters a new p and q value and confirms selection
        /// </summary>
        /// <param name="p">p value to enter</param>
        /// <param name="q">q value to enter</param>
        /// <param name="driver">Firefox Driver</param>
        public static void inputNewPQ(string p, string q, ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                driver.FindHTMLControl(xPath: string.Format(GlobalStartupShutdown.pField)).Clear();
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.pField));
                driver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.pField), textToAdd: p);
                driver.FindHTMLControl(xPath: string.Format(GlobalStartupShutdown.pField)).Clear();
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.pField));
                driver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.pField), textToAdd: q);
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.Apply));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void inputNewPTarget(string p, ihi_testlib_selenium.Selenium driver)
        {
            try
            {

                driver.FindHTMLControl(xPath: string.Format(GlobalStartupShutdown.pField)).Clear();
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.pField));
                driver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.pField), textToAdd: p);
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.Apply));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void inputNewSOCMaintenance(string pMax, string socTarget, ihi_testlib_selenium.Selenium driver)
        {
            try
            {

                driver.FindHTMLControl(xPath: string.Format(GlobalStartupShutdown.pField)).Clear();
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.pField));
                driver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.pField), textToAdd:pMax);
                driver.FindHTMLControl(xPath: string.Format(GlobalStartupShutdown.pField)).Clear();
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.pField));
                driver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.pField), textToAdd: socTarget);
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.Apply));
                //driver.browserDriver.FindElement(GlobalStartupShutdown.pField).Clear();
                //driver.browserDriver.FindElement(GlobalStartupShutdown.pField).Click();
                //driver.browserDriver.FindElement(GlobalStartupShutdown.pField).SendKeys(pMax);
                //driver.browserDriver.FindElement(GlobalStartupShutdown.qField).Clear();
                //driver.browserDriver.FindElement(GlobalStartupShutdown.qField).Click();
                //driver.browserDriver.FindElement(GlobalStartupShutdown.qField).SendKeys(socTarget);
                //driver.browserDriver.FindElement(GlobalStartupShutdown.Apply).Click();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// Method to handle popup and returns text of alert
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        private static string CloseAlertAndGetItsText(ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                IAlert alert = driver.browserDriver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }

        }

        /// <summary>
        /// Sets mode in database on (true) or off (false) for viewing on UI
        /// </summary>
        /// <param name="available"></param>
        /// <param name="mode"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string ToggleModeOnOff(bool available, Mode mode, SSHAndDatabaseDirectory info, string decsLocation = "/var/run/ihi-decs/")
        {
            var ssh = new SshClient(info.SSHIPAddress, info.SSHUsername, info.SSHPassword);

            string sql = string.Format("UPDATE modes SET available={0} WHERE mode_id={1}",Convert.ToInt32(available), (int)mode);
            
            try
            {
                return QueryInterface.CommandThroughQuery(sql, ssh, info.SSHIPAddress, decsLocation);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Returns list of modes that are available (shown in UI) in database.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static List<string> GetModeOnListFromDatabase(SSHAndDatabaseDirectory info, string decsLocation = "/var/run/ihi-decs/")
        {
            var ssh = new SshClient(info.SSHIPAddress, info.SSHUsername, info.SSHPassword);

            string sql = "SELECT mode_name FROM MODES WHERE available='1'";

            try
            {
                string rawList = QueryInterface.CommandThroughQuery(sql, ssh, info.SSHIPAddress, decsLocation, false);
                var modeList = rawList.Split('\n').ToList();
                if (modeList[0] != "")
                {

                    for (int i = 0; i < modeList.Count; i++)
                    {
                        modeList[i] = modeList[i].Trim();
                    }

                    modeList.RemoveAt(modeList.Count - 1);
                    modeList.RemoveAt(modeList.Count - 1);

                    if (modeList.Count > 0 && modeList[0] == "0")
                        modeList.RemoveAt(modeList.Count - 1);

                    return modeList;
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// Creates a tuple with list two lists. First list: in dropdown but not in database. Second list: in database but not in dropdown.
        /// </summary>
        /// <param name="dropdownList"></param>
        /// <param name="databaseList"></param>
        /// <returns></returns>
        public static Tuple<List<string>,List<string>> CheckListDifferences(List<string> dropdownList, List<string> databaseList)
        {
            if ((dropdownList != null && databaseList != null) || !Enumerable.SequenceEqual(dropdownList.OrderBy(t => t), databaseList.OrderBy(t => t)))
            {
                //if (!Enumerable.SequenceEqual(dropdownList.OrderBy(t => t), databaseList.OrderBy(t => t)))
                //{
                    var inDropdownNotDatabase = dropdownList.Except(databaseList).ToList();
                    var inDatabaseNotDropdown = databaseList.Except(dropdownList).ToList();
                    return Tuple.Create(inDropdownNotDatabase, inDatabaseNotDropdown);
                //}
                //return Tuple.Create(inDropdownNotDatabase, inDatabaseNotDropdown);
            }
            else
                return null;
        }

        /// <summary>
        /// Sets all modes on or off depending on bool "available"
        /// </summary>
        /// <param name="available"></param>
        /// <param name="info"></param>
        public static void SetAllModesOnOff(bool available, SSHAndDatabaseDirectory info)
        {
            foreach (Mode mode in Enum.GetValues(typeof(Mode)))
            {
                ToggleModeOnOff(available, mode, info);
            }
        }

        /// <summary>
        /// Sets mode list to default values in database
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool SetDefaultModes(SSHAndDatabaseDirectory info)
        {
            List<string> expectedDefaults = new List<string>();
            expectedDefaults.Add("Shutdown");
            expectedDefaults.Add("Manual Mode");
            expectedDefaults.Add("Peak Shaving");
            expectedDefaults.Add("Load Leveling");

            try
            {
                SetAllModesOnOff(false, info);
                ToggleModeOnOff(true, Mode.Shutdown, info);
                ToggleModeOnOff(true, Mode.Manual_Mode, info);
                ToggleModeOnOff(true, Mode.Peak_Shaving, info);
                ToggleModeOnOff(true, Mode.Load_leveling, info);

                var databaseList = GetModeOnListFromDatabase(info);

                //falied to set default values
                if(CheckListDifferences(expectedDefaults, databaseList)!=null)
                {
                    Console.WriteLine("Did not set Default Modes");
                    return false;
                }
                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        /// <summary>
        /// Check status of Auto Restart check box on UI.  Returns true if on, false if off
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool CheckAutoRestartStatus(ihi_testlib_selenium.Selenium driver)
        {
            IWebElement autoRestart = driver.FindHTMLControl(xPath: string.Format(GlobalStartupShutdown.AutoRestart));
            var check = autoRestart.GetAttribute("checked");
            if (check == "true")
            {
                return true;
            }
            return false;
        }
    }
}