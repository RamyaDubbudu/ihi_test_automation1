﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;
using System.Reflection;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Class that houses methods used on the Enviroment (Cooling) tab in the Control tab page
    /// </summary>
    public class EnvironmentPage
    {

        /// <summary>
        /// Returns the info on the Environment tab table by index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="driver"></param>
        /// <returns>EnvironmentInfo class with information on row</returns>
        public static EnvironmentInfo returnEnvironmentInfo(int index, ihi_testlib_selenium.Selenium driver)
        {
            //xpaths for all tables are formated in this way with id
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Environment.EnvironmentTableString);
            EnvironmentInfo environmentInfo = new EnvironmentInfo();
            string currentColor = "";

            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    string elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int element;
                    int.TryParse(elementString, out element);
                    environmentInfo.zoneID = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int.TryParse(elementString, out element);
                    environmentInfo.coolingID = element;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    environmentInfo.label = elementString;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    environmentInfo.description = elementString;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    environmentInfo.available = elementString;
                    currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    environmentInfo.available_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[6]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.browserDriver.FindElement(By.XPath(xpath)).Text;
                    environmentInfo.enabled = elementString;
                    currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    environmentInfo.enabled_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[7]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    environmentInfo.online = elementString;
                    currentColor = driver.GetAttribute(Xpath: xpath, attributeName: "color");
                    environmentInfo.online_color = currentColor;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[8]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double elementDouble;
                    double.TryParse(elementString, out elementDouble);
                    environmentInfo.supplyAirTempF = elementDouble;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[9]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    double.TryParse(elementString, out elementDouble);
                    environmentInfo.returnAirTempF = elementDouble;

                    //-1 means string is N/A.  Method will return null if "N/A" nor number is listed
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[10]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    if (double.TryParse(elementString, out elementDouble))
                        environmentInfo.setPoint = elementDouble;
                    else if (elementString == "N/A")
                        environmentInfo.setPoint = -1;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[11]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    if (double.TryParse(elementString, out elementDouble))
                        environmentInfo.percentLoad = elementDouble;
                    else if (elementString == "N/A")
                        environmentInfo.percentLoad = -1;
                    else
                        return null;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[12]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    if (double.TryParse(elementString, out elementDouble))
                        environmentInfo.inletTempF = elementDouble;
                    else if (elementString == "N/A")
                        environmentInfo.inletTempF = -1;
                    else
                        return null;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[13]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    if (double.TryParse(elementString, out elementDouble))
                        environmentInfo.outletTempF = elementDouble;
                    else if (elementString == "N/A")
                        environmentInfo.outletTempF = -1;
                    else
                        return null;

                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[14]", GlobalElementsControl.Environment.EnvironmentTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    if (double.TryParse(elementString, out elementDouble))
                        environmentInfo.flowRateGPM = elementDouble;
                    else if (elementString == "N/A")
                        environmentInfo.flowRateGPM = -1;
                    else
                        return null;

                    return environmentInfo;
                }
                else
                {
                    Console.WriteLine("Table not found");
                    return null;
                }
            }
            catch (StaleElementReferenceException)
            {
                Console.WriteLine("Stale element found, retrying");
                return returnEnvironmentInfo(index, driver);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public static List<EnvironmentInfo> GetAllEnvironmentInfo(ihi_testlib_selenium.Selenium driver)
        {
            List<EnvironmentInfo> environmentList = new List<EnvironmentInfo>();
            int countEnvironment = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Environment.AllTabsNextIsActive)))
            {
                countEnvironment = GetNumberOfEnvironmentRowsOnPage(driver);
                for (int i = 1; i <= countEnvironment; i++)
                {
                    environmentList.Add(returnEnvironmentInfo(i, driver));
                }
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Environment.AllTabsNextIsActive));
            }
            countEnvironment = GetNumberOfEnvironmentRowsOnPage(driver);
            for (int i = 1; i <= countEnvironment; i++)
            {
                environmentList.Add(returnEnvironmentInfo(i, driver));
            }
            GoToFirstPage(driver);
            return environmentList;
        }

        public static int GetNumberOfEnvironmentRowsOnPage(ihi_testlib_selenium.Selenium driver)
        {
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsControl.Environment.EnvironmentTableString);

            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    IWebElement webElementBody = driver.browserDriver.FindElement(By.XPath(xpath));
                    IList<IWebElement> ElementCollectionBody = webElementBody.FindElements(By.XPath(xpath));
                    //Makes sure only entry doesn't say list is empty
                    if (ElementCollectionBody.Count == 1 && ElementCollectionBody[0].Text == "There are no cooling units!")
                        return 0;

                    return ElementCollectionBody.Count;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public static void GoToFirstPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Environment.AllTabsPreviousIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Environment.AllTabsPreviousIsActive));
            }
        }

        public static void GoToLastPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Environment.AllTabsNextIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Environment.AllTabsNextIsActive));
            }
        }


        /// <summary>
        /// Compare Environment (cooling) Database, ZMQ query, and UI to make sure all report same values where applicable.
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <param name="IPAddress">Linux IP address</param>
        /// <param name="userName">Linux username</param>
        /// <param name="password">Linux password</param>
        /// <param name="databaseLocation">Location of decs database</param>
        /// <returns>ControlError info list which includes ID, column of error and UI, ZMQ, and database values</returns>
        public static List<ControlErrorInfo> CompareEnvironmentPageToZMQAndDatabase(ihi_testlib_selenium.Selenium driver, string IPAddress, string userName, string password, string databaseLocation = "/var/run/ihi-decs/")
        {
            List<ControlErrorInfo> errorList = new List<ControlErrorInfo>();
            string temp = "";
            string table = "cooling";
            string column = "";
            string ID = "";

            //order by zone id for both lists to make order reported by ZMQ irrelevant
            List<CoolingTable> environmentListZMQ = IHI_ZMQ.UI__GetData_CoolingList(IPAddress);
            environmentListZMQ = environmentListZMQ.OrderBy(o => o.cooling_id).ToList();

            List<EnvironmentInfo> environmentListUI = GetAllEnvironmentInfo(driver);
            environmentListUI = environmentListUI.OrderBy(o => o.coolingID).ToList();

            for (int i = 0; i < environmentListUI.Count; i++)
            {
                int coolingID = environmentListUI[i].coolingID;
                EnvironmentInfo inverterDatabase = ReadEnvironmentInfoFromDatabase(coolingID, IPAddress, userName, password, databaseLocation);
                ID = table + "_" + coolingID.ToString();


                //inverters_id does not match
                column = "cooling_id";
                if ((environmentListZMQ[i].zones_id != environmentListUI[i].zoneID) || (environmentListUI[i].zoneID != inverterDatabase.zoneID))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].zoneID.ToString(), environmentListZMQ[i].zones_id.ToString(), inverterDatabase.zoneID.ToString()));
                }

                //zoneID does not match
                column = "zone_id";
                if ((environmentListZMQ[i].zones_id != environmentListUI[i].zoneID) || (environmentListUI[i].zoneID != inverterDatabase.zoneID))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].zoneID.ToString(), environmentListZMQ[i].zones_id.ToString(), inverterDatabase.zoneID.ToString()));
                }

                //label does not match
                column = "label";
                if ((environmentListZMQ[i].label != environmentListUI[i].label) || (environmentListUI[i].label != inverterDatabase.label))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].label.ToString(), environmentListZMQ[i].label.ToString(), inverterDatabase.label.ToString()));
                }

                //description does not match
                column = "description";
                if ((environmentListZMQ[i].description != environmentListUI[i].description) || (environmentListUI[i].description != inverterDatabase.description))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].description.ToString(), environmentListZMQ[i].description.ToString(), inverterDatabase.description.ToString()));
                }

                //available does not match available
                column = "available";
                temp = StringDictionaries.ConvertDatabaseString(table, column, environmentListZMQ[i].available.ToString());
                if ((temp != environmentListUI[i].available) || (environmentListUI[i].available != inverterDatabase.available))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].available.ToString(), temp, inverterDatabase.available.ToString()));
                }

                //enabled does not match
                column = "enabled";
                temp = StringDictionaries.ConvertDatabaseString(table, column, environmentListZMQ[i].enabled.ToString());
                if ((temp != environmentListUI[i].enabled) || (environmentListUI[i].enabled != inverterDatabase.enabled))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].enabled.ToString(), temp, inverterDatabase.enabled.ToString()));
                }

                //online does not match
                column = "online";
                temp = StringDictionaries.ConvertDatabaseString(table, column, environmentListZMQ[i].online.ToString());
                if ((temp != environmentListUI[i].online) || (environmentListUI[i].online != inverterDatabase.online))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].online.ToString(), temp, inverterDatabase.online.ToString()));
                }

                //setPoint does not match
                column = "setPoint";
                if ((environmentListZMQ[i].setpoint != environmentListUI[i].setPoint) || (environmentListUI[i].setPoint != inverterDatabase.setPoint))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].setPoint.ToString(), environmentListZMQ[i].setpoint.ToString(), inverterDatabase.setPoint.ToString()));
                }

                //returnAirTempF does not match
                column = "returnAirTempF";
                if ((environmentListZMQ[i].return_air_temp_f != environmentListUI[i].returnAirTempF) || (environmentListUI[i].returnAirTempF != inverterDatabase.returnAirTempF))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].returnAirTempF.ToString(), environmentListZMQ[i].return_air_temp_f.ToString(), inverterDatabase.returnAirTempF.ToString()));
                }

                //supplyAirTempF does not match
                column = "supplyAirTempF";
                if ((environmentListZMQ[i].supply_air_temp_f != environmentListUI[i].supplyAirTempF) || (environmentListUI[i].supplyAirTempF != inverterDatabase.supplyAirTempF))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].supplyAirTempF.ToString(), environmentListZMQ[i].supply_air_temp_f.ToString(), inverterDatabase.supplyAirTempF.ToString()));
                }

                //percentLoad does not match
                column = "percentLoad";
                if ((environmentListZMQ[i].percent_load != environmentListUI[i].percentLoad) || (environmentListUI[i].percentLoad != inverterDatabase.percentLoad))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].percentLoad.ToString(), environmentListZMQ[i].percent_load.ToString(), inverterDatabase.percentLoad.ToString()));
                }

                //inletTempF does not match
                column = "inletTempF";
                if ((environmentListZMQ[i].inlet_temp_f != environmentListUI[i].inletTempF) || (environmentListUI[i].inletTempF != inverterDatabase.inletTempF))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].inletTempF.ToString(), environmentListZMQ[i].inlet_temp_f.ToString(), inverterDatabase.inletTempF.ToString()));
                }

                //outletTempF does not match
                column = "outletTempF";
                if ((environmentListZMQ[i].outlet_temp_f != environmentListUI[i].outletTempF) || (environmentListUI[i].outletTempF != inverterDatabase.outletTempF))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].outletTempF.ToString(), environmentListZMQ[i].outlet_temp_f.ToString(), inverterDatabase.outletTempF.ToString()));
                }

                //flowRateGPM does not match
                column = "flowRateGPM";
                if ((environmentListZMQ[i].flow_rate_gpm != environmentListUI[i].flowRateGPM) || (environmentListUI[i].flowRateGPM != inverterDatabase.flowRateGPM))
                {
                    errorList.Add(new ControlErrorInfo(ID, column, environmentListUI[i].flowRateGPM.ToString(), environmentListZMQ[i].flow_rate_gpm.ToString(), inverterDatabase.flowRateGPM.ToString()));
                }
            }

            return errorList;
        }

        /// <summary>
        /// Read all CoolingInfo values from database and stores them in a CoolingInfo file
        /// </summary>
        /// <param name="coolingID">coolingID of which to check</param>
        /// <param name="IPAddress">Linux IP address</param>
        /// <param name="userName">Linux username</param>
        /// <param name="password">Linux password</param>
        /// <param name="databaseLocation">Location of decs database</param>
        /// <returns></returns>
        public static EnvironmentInfo ReadEnvironmentInfoFromDatabase(int coolingID, string IPAddress, string userName, 
            string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            EnvironmentInfo coolingDatabase = new EnvironmentInfo();
            string table = "cooling";
            string format = "";
            string column = "";
            string temp = "";

            //rackID - taken from parameters
            coolingDatabase.coolingID = coolingID;

            SshClient ssh = new SshClient(IPAddress, userName, password);
            ssh.Connect();

            //zoneID
            column = "zones_id";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.zoneID = int.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //label
            column = "label";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.label = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //description
            column = "description";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.description = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //available
            column = "available";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.available = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //enabled
            column = "enabled";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.enabled = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //online
            column = "online";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.online = StringDictionaries.ConvertDatabaseString(table, column, temp);

            //setpoint
            column = "setpoint";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.setPoint = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //return_air_temp_f
            column = "return_air_temp_f";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.returnAirTempF = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //supply_air_temp_f
            column = "supply_air_temp_f";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.supplyAirTempF = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //percent_load
            column = "percent_load";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.percentLoad = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //inlet_temp_f
            column = "inlet_temp_f";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.inletTempF = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //outlet_temp_f
            column = "outlet_temp_f";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.outletTempF = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            //flow_rate_gpm
            column = "flow_rate_gpm";
            format = string.Format("SELECT {0} from {1} where cooling_id='{2}'", column, table, coolingID);
            temp = QueryInterface.CommandThroughQuery(format, ssh, IPAddress, decsLocation);
            coolingDatabase.flowRateGPM = double.Parse(StringDictionaries.ConvertDatabaseString(table, column, temp));

            return coolingDatabase;
        }

        public static void CreateMoreCoolingUnits(int numberToCreate, SSHAndDatabaseDirectory connectionInfo, 
            int zoneID = 1, string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(connectionInfo.SSHIPAddress, connectionInfo.SSHUsername, connectionInfo.SSHPassword);
            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            string table = "cooling";

            List<CoolingTable> coolingListZMQ = IHI_ZMQ.UI__GetData_CoolingList(connectionInfo.SSHIPAddress);

            //find min and max values to know where to copy from and where to put new entry
            int maxID = coolingListZMQ.Max(t => t.cooling_id);
            int minID = coolingListZMQ.Min(t => t.cooling_id);

            CoolingTable rackTable = new CoolingTable();

            List<string> propertyNames = rackTable.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }
            for (int i = 0; i < numberToCreate; i++)
            {
                string prototypeSelect = (++maxID) + ", ";
                foreach (string item in propertyNames)
                {
                    if (item == firstItem)
                        continue;

                    if (item != lastItem)
                        prototypeSelect += item + (", ");
                    else
                        prototypeSelect += item + " ";
                }

                string sqlStatement = string.Format("INSERT into {0}{1}SELECT {2}from {0} where {3}={4}", table,
                    prototypeInto, prototypeSelect, firstItem, minID);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
                //update zone id
                sqlStatement = string.Format("UPDATE cooling SET zones_id='{0}' WHERE cooling_id={1}", zoneID, maxID);
                QueryInterface.CommandThroughQuery(sqlStatement, ssh, connectionInfo.SSHIPAddress, decsLocation);
            }
        }

        /// <summary>
        /// Takes in a list of inverter tables (usually from zmq), clears the database and loads in only these values.  Useful when making tests non-descructive
        /// </summary>
        /// <param name="zoneList"></param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static void UpdateDatabaseFromCoolingTableList(List<CoolingTable> coolingList, string IPAddress, string userName, 
            string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(IPAddress, userName, password);

            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            string table = "cooling";

            SSH.ClearSQLTable(databaseLocation, "decs_dc.db", table, IPAddress, userName, password);

            CoolingTable coolingTableTemp = new CoolingTable();
            List<string> propertyNames = coolingTableTemp.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            //the following code is formating the sql insert into select statement
            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }

            foreach (CoolingTable coolingTable in coolingList)
            {
                List<string> objectStrings = coolingTable.GetPropertyValues();
                string last = objectStrings.Last();

                string prototypeSelect = " (";
                int findLastLoop = 1;
                foreach (string value in objectStrings)
                {
                    if (!(findLastLoop == objectStrings.Count))
                        prototypeSelect += value + (", ");
                    else
                        prototypeSelect += value + (") ");
                    findLastLoop++;
                }
                string sqlStatement = string.Format("INSERT INTO {0}{1} VALUES {2}", table, prototypeInto, prototypeSelect);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, IPAddress, decsLocation);
            }
            ssh.Disconnect();
        }

    }

    public class EnvironmentInfo
    {
        public int zoneID { get; set; }
        public int coolingID { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string online { get; set; }
        private double _setPoint;
        public double setPoint { get { return this._setPoint; } set { _setPoint = (value < 0) ? -1 : value; } }
        public double returnAirTempF { get; set; }
        public double supplyAirTempF { get; set; }
        private double _percentLoad;
        public double percentLoad { get { return this._percentLoad; } set { _percentLoad = (value < 0) ? -1 : value;  } }
        private double _inletTempF;
        public double inletTempF { get { return this._inletTempF; } set { _inletTempF = (value < 0) ? -1 : value; } }
        private double _outletTempF;
        public double outletTempF { get { return this._outletTempF; } set { _outletTempF = (value < 0) ? -1 : value; } }
        private double _flowRateGPM;
        public double flowRateGPM { get { return this._flowRateGPM; } set { _flowRateGPM = (value < 0) ? -1 : value; } }
        private string m_available_color;
        public string available_color
        {
            get { return this.m_available_color; }
            set { m_available_color = SetColor(value); }
        }
        private string m_enable_color;
        public string enabled_color
        {
            get { return this.m_available_color; }
            set { m_enable_color = SetColor(value); }
        }
        private string m_online_color;
        public string online_color
        {
            get { return this.m_online_color; }
            set { m_online_color = SetColor(value); }
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(EnvironmentInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue == "N/A" || otherValue == "N/A")
                {
                    continue;
                }

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }

        /// <summary>
        /// Checks if ui value and database value are equal or if ui value is equal to -1 (which means UI shows "N/A") check to see if database value is negative
        /// </summary>
        /// <param name="databaseValue"></param>
        /// <param name="uiValue"></param>
        /// <returns></returns>
        private bool CheckIfNAOrEqual(double databaseValue, double uiValue)
        {
            if (databaseValue == uiValue)
                return true;

            if((databaseValue<0)&&(uiValue==-1))
            {
                return true;
            }
            return false;
        }

        private string SetColor(string color)
        {
            //checks if rbga value, if not sets it to same as value
            if (!color.Contains(","))
                return color;

            //is rbga value
            if (StringDictionaries.UITextColors.ContainsValue(color))
            {
                string updatedColor = StringDictionaries.UITextColors.FirstOrDefault(x => x.Value == color).Key;
                //Console.WriteLine("Found color " + color + " " + updatedColor);
                return updatedColor;
            }
            else
            {
                return "Color not found in dictionary: " + color;
            }
        }
    }
}
