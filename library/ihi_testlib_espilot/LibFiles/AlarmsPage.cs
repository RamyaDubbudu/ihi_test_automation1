﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;

namespace ihi_testlib_espilot
{
    


    /// <summary>
    /// Class that contains methods for the UI's Alarms page
    /// </summary>
    public static class AlarmsPage
    {
        public class AlarmsInfo
        {
            public int AlarmID { get; set; }
            public string AlarmTime { get; set; }
            public string AckowledgeTime { get; set; }
            public string ClearTime { get; set; }
            public string Severity { get; set; }
            public string AlarmDescription { get; set; }
        }


        /// <summary>
        /// Returns rack info on page based on index
        /// </summary>
        /// <param name="index">index of tabe starts at 1</param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static AlarmsInfo returnAlarmInfo(int index, ihi_testlib_selenium.Selenium driver)
        {
            System.Threading.Thread.Sleep(2000);
            //xpaths for all tables are formated in this way with id
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsAlarms.AlarmsTableString);
            AlarmsInfo alarmInfo = new AlarmsInfo();
            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    //AlarmID
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", GlobalElementsAlarms.AlarmsTableString, index);
                    string elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int element;
                    int.TryParse(elementString, out element);
                    alarmInfo.AlarmID = element;
                    //AlarmTime
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", GlobalElementsAlarms.AlarmsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    alarmInfo.AlarmTime = elementString;
                    //AckowledgeTime
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", GlobalElementsAlarms.AlarmsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    alarmInfo.AckowledgeTime = elementString;
                    //ClearTime
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", GlobalElementsAlarms.AlarmsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    alarmInfo.ClearTime = elementString;
                    //Severity
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", GlobalElementsAlarms.AlarmsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    alarmInfo.Severity = elementString;
                    //AlarmDescription
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[6]", GlobalElementsAlarms.AlarmsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    alarmInfo.AlarmDescription = elementString;
               
                    return alarmInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (StaleElementReferenceException)
            {
                Console.WriteLine("Stale element found, retrying");
                return returnAlarmInfo(index, driver);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Cycles through all pages of racks and returns a list with all rack information
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>List with all rack info objects</returns>
        public static List<AlarmsInfo> GetAllAlarmInfo(ihi_testlib_selenium.Selenium driver)
        {
            List<AlarmsInfo> alarmList = new List<AlarmsInfo>();
            int countRacks = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive)))
            {
                countRacks = GetNumberofalarmRows(driver);
                for (int i = 1; i <= countRacks; i++)
                {
                    alarmList.Add(returnAlarmInfo(i, driver));
                }
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.Racks.AllTabsNextIsActive));
            }
            countRacks = GetNumberofalarmRows(driver);
            for (int i = 1; i <= countRacks; i++)
            {
                alarmList.Add(returnAlarmInfo(i, driver));
            }
            GoToFirstPage(driver);
            return alarmList;
        }

        /// <summary>
        /// Generates a number of alarms and imports them into the database using sqlite3
        /// </summary>
        /// <param name="numberOfAlarms">Number of alarms you wish to add</param>
        /// <param name="sshIPAddress">IP Address of the Linux system</param>
        /// <param name="sshUserName">Valid login for the Linux system</param>
        /// <param name="sshPassword">Valid password for the Linux system</param>
        /// <param name="runningTally">Tally used to tell the method were to start the event_id. Defaults at "0"</param>
        /// <param name="debug">Turns on debug infromation, usually writing the sqlite command to the console window. Defaults false for off</param>
        /// <param name="eventsDatabaseLocation">Location of the SQL database for events.  Defaults "/var/run/ihi-decs/" </param>
        public static void GenerateNumberOfAlarms(int numberOfAlarms, string sshIPAddress, string sshUserName, string sshPassword, int runningTally = 0, bool debug = false,
            string eventsDatabaseLocation = "/var/run/ihi-decs/")
        {
            //prototype of events adding
            string prototypesql = "(event_id, event_key, severity, is_alarm, active, device_id, additional_info, event_type, create_time, ack_user, ack_notes, ack_time)";
            using (SshClient ssh = SSH.ConnectSSH(sshIPAddress, sshUserName, sshPassword))
            {
                for (int i = 1; i <= numberOfAlarms; i++)
                {
                    string create_time = DateTime.Now.ToString();
                    //sets data and time to "u" format
                    create_time = DateTime.Now.ToString("u");
                    //backend does not use "u" appended onto time/date
                    create_time = create_time.Remove(create_time.Length - 1);
                    int event_id = i + runningTally;
                    int is_alarm = 1;
                    int ievent_key = (i + runningTally);
                    //event_key to be 32 characters
                    string event_key = ievent_key.ToString().PadLeft(32, '0');
                    int severity = i % 4;
                    int active = 0;
                    int device_id = 15;
                    int ack_user = 0;
                    string ack_notes = "";
                    string ack_time = "";
                    int event_type = -19;

                    string additional_info = string.Format("Alarm with Alarm_id {0} and Alarm_key {1}", i, event_key);
                    string createalarmstring = string.Format("echo \"{0}\" | sudo -S sqlite3 {1}{2} \"INSERT INTO events {3} VALUES({4}, '{5}', {6}, {7}, {8}, {9}, '{10}', {11}, '{12}', {13}, '{14}', '{15}')\"",
                        sshPassword, eventsDatabaseLocation, "decs_events.db", prototypesql, event_id, event_key, severity, is_alarm, active, device_id, additional_info, event_type, create_time, ack_user, ack_notes, ack_time);



                    var terminal = ssh.RunCommand(createalarmstring);
                    if (debug)
                    {
                        Console.WriteLine(createalarmstring + "\n");
                        Console.WriteLine(terminal.Result + "\n");
                    }
                }
                ssh.Disconnect();
            }
        }

        /// <summary>
        /// Sends clear time to database to aid in dismissing alarms.  If ID is blank or set to "*" all alarms will be cleared
        /// </summary>
        /// <param name="sshIPAddress">Linux IP address</param>
        /// <param name="sshUserName">Linux User name</param>
        /// <param name="sshPassword">Linux Password</param>
        /// <param name="ID">Id of which event to clear, defaults to "*" which clears them all</param>
        /// <param name="eventsDatabaseLocation">location of events database</param>
        public static void SetAlarmClearTime(string sshIPAddress, string sshUserName, string sshPassword, string ID = "*", string eventsDatabaseLocation = "/var/run/ihi-decs/")
        {
            string createSendString = "";

            using (SshClient ssh = SSH.ConnectSSH(sshIPAddress, sshUserName, sshPassword))
            {
                string clear_time = DateTime.Now.ToString();
                clear_time = DateTime.Now.ToString("u");
                clear_time = clear_time.Remove(clear_time.Length - 1);
                if (ID != "*")
                {
                    createSendString = string.Format("echo \"{0}\" | sudo -S sqlite3 {1}{2} \"UPDATE events SET clear_time='{3}' WHERE event_id='{4}'\"",
                        sshPassword, eventsDatabaseLocation, "decs_events.db", clear_time, ID);
                }
                else
                {
                    createSendString = string.Format("echo \"{0}\" | sudo -S sqlite3 {1}{2} \"UPDATE events SET clear_time='{3}' WHERE is_alarm='1'\"",
                        sshPassword, eventsDatabaseLocation, "decs_events.db", clear_time);
                }
                //Console.WriteLine(createSendString);
                var terminal = ssh.RunCommand(createSendString);
                ssh.Disconnect();
            }
        }

        /// <summary>
        /// receives alarm number from indicator at top of page: "There are x Alarms"
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>Number listed on top of page</returns>
        public static int GetNumberofAlarmsFromTopOfPage(ihi_testlib_selenium.Selenium driver)
        {
            int alarms = -1;
            if(driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.AlarmNumber)))
            { 
            //if (GeneralDECS.IsElementPresent(GlobalElementsAlarms.AlarmNumber, driver))
            //{
                string ThereArealarms = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsAlarms.AlarmNumber));
                string alarmsString = Regex.Match(ThereArealarms, @"\d+").Value;
                alarms = Int32.Parse(alarmsString);
            }
            return alarms;
        }

        /// <summary>
        /// Receives alarm number from indicator at bottom of page
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>number of alarms</returns>
        public static int GetNumberofAlarmsFromBottomOfPage(ihi_testlib_selenium.Selenium driver)
        {
            int alarms = -1;

            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.AlarmNumber)))
            {
                string ThereArealarms = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsAlarms.BottomListingNumber));
                string[] numbers = Regex.Split(ThereArealarms, @"\D+");
                alarms = Int32.Parse(numbers[2]);
            }
            return alarms;
        }

        /// <summary>
        /// Get number of alarms from the table on the current page only
        /// </summary>
        /// <param name="TableID">Name of table</param>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>number of rows on page</returns>
        public static int GetNumberofalarmRows(ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                if (driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.AlarmsTable)))
                {
                    //IWebElement webElementBody = driver.FindHTMLControl(xPath: string.Format(GlobalElementsAlarms.AlarmsTable)); 
                    IList<IWebElement> ElementCollectionBody = driver.GetControlCollection(xPath: string.Format(GlobalElementsAlarms.AlarmsTableRows));
                    //Makes sure only entry doesn't say list is empty, if does, will return "0"
                    if (ElementCollectionBody.Count == 1 && ElementCollectionBody[0].Text == "There are no alarms!")
                        return 0;

                    return ElementCollectionBody.Count;
                }
                else
                {
                    return -1;
                }
            }
            catch (StaleElementReferenceException)
            {
                //Stale element means list no longer exists
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Gets the number of pages will be present based on the number of alarms + number of alarms on last page (remainder)
        /// </summary>
        /// <param name="numberofalarms">Total number of alarms</param>
        /// <param name="numberofpages">gets total number of pages</param>
        /// <param name="lastpagenumber">pass by reference with last page number</param>
        /// <param name="alarmsperpage">number of alarms per page (defaults 15)</param>
        public static void GetNumberofPagesWithLastPage(int numberofalarms, out int numberofpages, out int lastpagenumber, int alarmsperpage = 15)
        {
            numberofpages = (numberofalarms / alarmsperpage) + 1;
            lastpagenumber = numberofalarms % alarmsperpage;
        }

        /// <summary>
        /// cycles through all pages until end and counts how many entries
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns></returns>
        public static int CountNumberofalarms(ihi_testlib_selenium.Selenium driver)
        {
            int countalarms = 0;
            try
            {
                while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.NextIsActive)))
                {
                    countalarms += GetNumberofalarmRows(driver);
                    driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.NextIsActive));
                    //driver.browserDriver.FindElement(GlobalElementsAlarms.NextIsActive).Click();
                }
                countalarms += GetNumberofalarmRows(driver);
                GoToFirstPage(driver);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return countalarms;
        }

        /// <summary>
        /// Goes to the first page of alarms
        /// </summary>
        /// <param name="driver"></param>
        public static void GoToFirstPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.PreviousIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.PreviousIsActive));
                //driver.browserDriver.FindElement(GlobalElementsAlarms.PreviousIsActive).Click();
            }
        }

        /// <summary>
        /// Goes to Last page of alarms
        /// </summary>
        /// <param name="driver"></param>
        public static void GoToLastPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.NextIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.NextIsActive));
                //driver.browserDriver.FindElement(GlobalElementsAlarms.NextIsActive).Click();
            }
        }

        /// <summary>
        /// Checks if all alarms have been achknowledged
        /// </summary>
        /// <param name="driver">Firefox driver</param>
        /// <returns>true of all alarms have been acknowledged</returns>
        public static bool AreAllalarmsAcknowledged(ihi_testlib_selenium.Selenium driver)
        {
            int pagenumber = 1;
            int countalarmsonpage = 0;
            bool allacknowleged = false;
            int acknowledgeCount = 0;
            int notAcknowledged = 0;
            countalarmsonpage = GetNumberofalarmRows(driver);
            List<AlarmsInfo> alarmInfo = GetAllAlarmInfo(driver);
            foreach (var item in alarmInfo)
            {
                if (item.AckowledgeTime.Length != 0)
                {
                    acknowledgeCount++;
                }
                else
                    notAcknowledged++;
            }
            if (acknowledgeCount == countalarmsonpage)
            {
                allacknowleged = true;
            }
            else if (notAcknowledged > 0)
            {
                allacknowleged = false;
            }
            return allacknowleged; 
            ////while (GeneralDECS.IsElementPresent(GlobalElementsAlarms.NextIsActive, driver))
            //while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.NextIsActive)))

            //{
            //    countalarmsonpage = GetNumberofalarmRows(driver);
            //    for (int i = 0; i < (countalarmsonpage); i++)
            //    {
            //        string rowvalue = driver.ReturnTableRowValue(GlobalElementsAlarms.AlarmsTableRows, i);
            //        //Searches for regular expression with more than one 20 (years) and more than 2 ":" (times)
            //        bool numbermorethantwo20s = Regex.Matches(rowvalue,  "/20" ).Count > 1;
            //        bool numbermorethantwocolons = Regex.Matches(rowvalue,  ":" ).Count > 2;
            //        if(!(numbermorethantwo20s&&numbermorethantwocolons))
            //        {
            //            Console.WriteLine(rowvalue);
            //            Console.WriteLine("Row number = " + i);
            //            Console.WriteLine("Page number = " + pagenumber);
            //            allacknowleged = false;
            //            GoToFirstPage(driver);
            //            return allacknowleged;
            //        }
            //    }
            //    driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.NextIsActive));
            //   // driver.browserDriver.FindElement(GlobalElementsAlarms.NextIsActive).Click();
            //    pagenumber++;
            //}
            ////Last page
            //countalarmsonpage = GetNumberofalarmRows(driver);
            //for (int i = 1; i < countalarmsonpage; i++)
            //{
            //    string rowvalue = driver.ReturnTableRowValue(GlobalElementsAlarms.AlarmsTableRows, i);
            //    //Searches for regular expression with more than one 20 (years) and more than 2 ":" (times)
            //    bool numbermorethantwo20s = Regex.Matches(rowvalue, "/20").Count > 1;
            //    bool numbermorethantwocolons = Regex.Matches(rowvalue, ":").Count > 2;
            //    if (!(numbermorethantwo20s && numbermorethantwocolons))
            //    {
            //        GoToFirstPage(driver);
            //        allacknowleged = false;
            //        return allacknowleged;
            //    }
            //}
            ////return back to first page
            //GoToFirstPage(driver);
            //return allacknowleged;          
        }


        public static void AcknowledgeAndDismiss(ihi_testlib_selenium.Selenium driver)
        {
            bool allAcknowledged = AreAllalarmsAcknowledged(driver);
            if(allAcknowledged == false)
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.AcknowledgeAllButton));
                List<AlarmsInfo> alarmInfo = GetAllAlarmInfo(driver);
                foreach (var item in alarmInfo)
                {
                    if (item.AckowledgeTime.Length != 0)
                    {
                        
                    }
                   
                }
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.DismissAllButton));
            }
        }
        public static List<string> GetAllAlarmsText(ihi_testlib_selenium.Selenium driver)
        {
            int pageNumber = 1;
            int countUsersOnPage = 0;
            List<string> alarmsText = new List<string>();

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                countUsersOnPage = GetNumberofalarmRows(driver);

                for (int i = 0; i < (countUsersOnPage); i++)
                {
                    string rowValue = driver.ReturnTableRowValue(GlobalElementsAlarms.AlarmsTableRows, i);
                    rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);
                    alarmsText.Add(rowValue + "\n");
                }

                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.NextIsActive));
                pageNumber++;
            }

            countUsersOnPage = GetNumberofalarmRows(driver);

            for (int i = 0; i < (countUsersOnPage); i++)
            {
                string rowValue = driver.ReturnTableRowValue(GlobalElementsAlarms.AlarmsTableRows, i);
                rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);
                alarmsText.Add(rowValue + "\n");
            }
            GoToFirstPage(driver);
            return alarmsText;
        }

        public static bool SearchForText(string searchedText, ihi_testlib_selenium.Selenium driver)
        {

            int pageNumber = 1;
            int countUsersOnPage = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                countUsersOnPage = GetNumberofalarmRows(driver);

                for (int i = 0; i < (countUsersOnPage); i++)
                {
                    string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                    // string rowValue = GeneralDECS.returnTableRowValue(GlobalElementsAdmin.UserTableRows, i, driver);
                    rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                    if (rowValue.Contains(searchedText));
                    {
                        GoToFirstPage(driver);
                        return true;
                    }
                }
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.NextIsActive));
                pageNumber++;
            }

            countUsersOnPage = GetNumberofalarmRows(driver);

            for (int i = 0; i < (countUsersOnPage); i++)
            {
                string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                if (rowValue.Contains(searchedText))
                {
                    GoToFirstPage(driver);
                    return true;
                }
            }
            GoToFirstPage(driver);
            return false;
        }
    }
}
