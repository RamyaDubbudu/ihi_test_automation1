﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ihi_testlib_espilot
{
    public static class QueryExpected
    {
        public static double EnergyAvailableForSite(string ipAddress)
        {
            var nameplateValue = IHI_ZMQ.UI__GetData_SiteList(ipAddress)[0].kwh_nameplate;
            var rackList = IHI_ZMQ.UI__GetData_RackList(ipAddress);
            var energyAvailableFromRacks = EnergyAvailableForList(rackList);
            var energyAvailable = nameplateValue < energyAvailableFromRacks ? nameplateValue : energyAvailableFromRacks;

            return Math.Floor(energyAvailable);
        }

        public static double EnergyAvailableForZone(string ipAddress, int zone)
        {
            var rackList = IHI_ZMQ.UI__GetData_RackList(ipAddress);
            List<RackTable> zoneListForRacks = rackList.FindAll(rack => rack.zones_id == zone);
            return EnergyAvailableForList(zoneListForRacks);
        }

        private static double EnergyAvailableForList(List<RackTable> rackList)
        {
            var racksConnected = rackList.Where(x => x.rack_status == 1);
            return Math.Floor(racksConnected.Sum(rack => rack.kwh_available * rack.c_rate));
        }
    }
}
