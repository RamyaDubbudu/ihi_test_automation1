﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace ihi_testlib_espilot
{
    public class SiteTable
    {
        public int site_id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state_province { get; set; }
        public string post_code { get; set; }
        public string country { get; set; }
        public double lmp { get; set; }
        public int auto_restart { get; set; }
        public int mode_actual { get; set; }
        public int mode_demand { get; set; }
        public int startup_timer { get; set; }
        public int p_demand { get; set; }
        public int p_actual { get; set; }
        public int q_demand { get; set; }
        public int q_actual { get; set; }
        public int p_max { get; set; }
        public int p_min { get; set; }
        public int q_max { get; set; }
        public int q_min { get; set; }
        public double soc { get; set; }
        public double kwh_nameplate { get; set; }
        public int kwh_max { get; set; }
        public int kvar_nameplate { get; set; }
        public double kwh_available { get; set; }
        public double ref_freq { get; set; }
        public double ref_voltage { get; set; }
        public int ref_p { get; set; }
        public int ref_q { get; set; }
        public int ref_kva { get; set; }
        public double low_soc_warning { get; set; }
        public double low_soc_cutoff { get; set; }
        public double high_soc_warning { get; set; }
        public double high_soc_cutoff { get; set; }
        public double target_soc { get; set; }
        public double target_freq { get; set; }
        public double target_voltage { get; set; }
        public int target_p { get; set; }
        public int target_q { get; set; }
        public int zones_available { get; set; }
        public int zones_enabled { get; set; }
        public int zones_online { get; set; }
        public int zones_total { get; set; }
        public int inverters_available { get; set; }
        public int inverters_enabled { get; set; }
        public int inverters_online { get; set; }
        public int inverters_total { get; set; }
        public int racks_available { get; set; }
        public int racks_enabled { get; set; }
        public int racks_online { get; set; }
        public int racks_total { get; set; }
        public int cooling_available { get; set; }
        public int cooling_enabled { get; set; }
        public int cooling_total { get; set; }
        public int cooling_online { get; set; }
        public int io_available { get; set; }
        public int io_total { get; set; }
        public int meters_available { get; set; }
        public int meters_total { get; set; }
        public int epo_active { get; set; }
        public int rpo_active { get; set; }
        public int alarms_critical { get; set; }
        public int alarms_major { get; set; }
        public int alarms_minor { get; set; }
        public int alarms_informational { get; set; }
        public int alarms_total { get; set; }
        public int notifications_total { get; set; }
        public int server_build { get; set; }
        public int server_latest_build { get; set; }
        public double kwh_throughput { get; set; }
        public double total_runtime { get; set; }
        public double current_runtime { get; set; }
        public int ui_build { get; set; }
        public int ui_latest_build { get; set; }
        public int instance_id { get; set; }
        public int ups_status { get; set; }
        public double ups_load_pct { get; set; }
        public double ups_soc { get; set; }
        public double ups_remaining { get; set; }
        public double ambient_temp { get; set; }
        public string help_url { get; set; }
        public string ups_url { get; set; }
        public string operator_url { get; set; }
        public string logo_url { get; set; }
        public int ui_flags { get; set; }
        public string last_mode { get; set; }
        public int target_p_max { get; set; }
        public int target_p_min { get; set; }
        public int target_q_max { get; set; }
        public int target_q_min { get; set; }
    

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                //Added check for null value due to sql add failing when value is null
                var propertyValue = property.GetValue(this, null);
                if (propertyValue != null)
                {
                    propertyList.Add(property.Name);
                }
                else
                {
                    //Console.WriteLine(property.Name + ": " + "null");
                }
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                //Added check for null value due to sql add failing when value is null
                var propertyValue = property.GetValue(this, null);
                if (propertyValue != null)
                {
                    propertyValues.Add("'" + propertyValue + "'");
                }
                else
                {
                    //Console.WriteLine(property.Name + ": " + "null");
                }
            }
            return propertyValues;

        }
    }

    public class IOTable
    {
        public int io_id { get; set; }
        public string proxy_address { get; set; }
        public string address { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public string image_filename { get; set; }
        public int type { get; set; }
        public int available { get; set; }
        public int maint_mode { get; set; }
        public int di_input { get; set; }
        public int di_epo_mask { get; set; }
        public int di_epo_active_high { get; set; }
        public int di_rpo_mask { get; set; }
        public int di_rpo_active_high { get; set; }
        public int do_output { get; set; }
        public int do_heartbeat_mask { get; set; }
        public int do_epo_indicator_mask { get; set; }
        public int do_rpo_indicator_mask { get; set; }
        public int do_islanded_mask { get; set; }
        public int do_fault_indicator_mask { get; set; }
        public int shutdown_mask { get; set; }
        public int do_startup_indicator_mask { get; set; }
        public int do_manual_mask { get; set; }
        public int do_load_leveling_mask { get; set; }
        public int zones_id { get; set; }
    }

    public class MeterTable
    {
        public int meters_id { get; set; }
        public string proxy_address { get; set; }
        public string address { get; set; }
        public int type { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public int available { get; set; }
        public int is_reference { get; set; }
        public double freq { get; set; }
        public double pf { get; set; }
        public double ia { get; set; }
        public double ib { get; set; }
        public double ic { get; set; }
        public double vab { get; set; }
        public double vbc { get; set; }
        public double vac { get; set; }
        public double kw_3ph { get; set; }
        public double kvar_3ph { get; set; }
        public double kva_3ph { get; set; }
        public string image_filename { get; set; }
        public int zones_id { get; set; }
    }

    public class ZoneTable
    {
        public int zones_id { get; set; }
        public string proxy_address { get; set; }
        public string label { get; set; }
        public int available { get; set; }
        public int maint_mode { get; set; }
        public int enabled { get; set; }
        public int state_actual { get; set; }
        public int state_demand { get; set; }
        public double total_runtime { get; set; }
        public double current_runtime { get; set; }
        public double soc { get; set; }
        public double kwh_max { get; set; }
        public double kwh_available { get; set; }
        public double dc_voltage { get; set; }
        public int p_demand { get; set; }
        public int p_actual { get; set; }
        public int p_max { get; set; }
        public int p_min { get; set; }
        public int q_demand { get; set; }
        public int q_actual { get; set; }
        public int q_max { get; set; }
        public int q_min { get; set; }
        public int racks_total { get; set; }
        public int racks_available { get; set; }
        public int racks_enabled { get; set; }
        public int racks_online { get; set; }
        public int inverters_total { get; set; }
        public int inverters_available { get; set; }
        public int inverters_enabled { get; set; }
        public int inverters_online { get; set; }
        public int cooling_total { get; set; }
        public int cooling_available { get; set; }
        public int cooling_enabled { get; set; }
        public int cooling_online { get; set; }
        public string image_filename { get; set; }
        public string camera1_label { get; set; }
        public string camera1_url { get; set; }
        public string camera2_label { get; set; }
        public string camera2_url { get; set; }
        public string camera3_label { get; set; }
        public string camera3_url { get; set; }
        public string camera4_label { get; set; }
        public string camera4_url { get; set; }
        public int ups_status { get; set; }
        public double ups_load_pct { get; set; }
        public double ups_soc { get; set; }
        public double ups_remaining { get; set; }
        public double ambient_temp { get; set; }
        public int site_id { get; set; }

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyList.Add(property.Name.ToString());
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyValues.Add("'" + property.GetValue(this, null).ToString() + "'");
            }
            return propertyValues;

        }

        /// <summary>
        /// Takes values relivent to the control table and load them into class (can overload for popup)
        /// </summary>
        /// <param name="zoneInfo"></param>
        public void UpdateTable(ZoneInfo zoneInfo)
        {
            label = zoneInfo.description;
            available = int.Parse(StringDictionaries.ConvertStringDatabase("zones", "available", zoneInfo.available));
            enabled = int.Parse(StringDictionaries.ConvertStringDatabase("zones", "enabled", zoneInfo.enabled));
            state_actual = int.Parse(StringDictionaries.ConvertStringDatabase("zones", "state_actual", zoneInfo.status));
            soc = zoneInfo.SOC;
            dc_voltage = zoneInfo.dcVoltage;
            p_demand = zoneInfo.kVADemand;
            p_actual = zoneInfo.kVAActual;
            ambient_temp = zoneInfo.ambient_temp;
            q_actual = zoneInfo.kVARActual;
        }

        /// <summary>
        /// Takes values relavent to the popup table and loads it into class
        /// </summary>
        /// <param name="zoneInfo"></param>
        public void UpdateTable(ZonePopupInfo zoneInfo)
        {
            label = zoneInfo.label;
            available = int.Parse(StringDictionaries.ConvertStringDatabase("zones", "available", zoneInfo.available));
            enabled = int.Parse(StringDictionaries.ConvertStringDatabase("zones", "enabled", zoneInfo.enabled));
            state_actual = int.Parse(StringDictionaries.ConvertStringDatabase("zones", "state_actual", zoneInfo.state_actual));
            soc = zoneInfo.soc;
            total_runtime = zoneInfo.total_runtime;
            current_runtime = zoneInfo.current_runtime;
            p_actual = zoneInfo.p_actual;
            p_demand = zoneInfo.p_demand;
            q_actual = zoneInfo.q_actual;
            q_demand = zoneInfo.q_demand;
            dc_voltage = zoneInfo.dc_voltage;
            p_max = zoneInfo.p_charge_available;
            p_min = zoneInfo.p_discharge_avaiable;
            q_max = zoneInfo.q_leading_available;
            q_min = zoneInfo.q_lagging_avaiable;
            ups_status = int.Parse(StringDictionaries.ConvertStringDatabase("zones", "ups_status", zoneInfo.ups_status));
            ups_soc = zoneInfo.ups_soc;
            ups_load_pct = zoneInfo.ups_load_pct;
            ups_remaining = zoneInfo.ups_remaining;
            ambient_temp = zoneInfo.ambient_temp;
        }
    }

    public class UserTable
    {
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string full_name { get; set; }
        public string email_addr { get; set; }
        public int permissions { get; set; }
        public int alert_types { get; set; }
        public string password_md5 { get; set; }

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyList.Add(property.Name.ToString());
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyValues.Add("'" + property.GetValue(this, null).ToString() + "'");
            }
            return propertyValues;

        }
    }

    public class CoolingTable
    {
        public int cooling_id { get; set; }
        public string proxy_address { get; set; }
        public string address { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public int rating { get; set; }
        public int available { get; set; }
        public int maint_mode { get; set; }
        public int enabled { get; set; }
        public int online { get; set; }
        public double setpoint { get; set; }
        public double return_air_temp_f { get; set; }
        public double supply_air_temp_f { get; set; }
        public double percent_load { get; set; }
        public double inlet_temp_f { get; set; }
        public double outlet_temp_f { get; set; }
        public double flow_rate_gpm { get; set; }
        public string image_filename { get; set; }
        public int zones_id { get; set; }

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyList.Add(property.Name.ToString());
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyValues.Add("'" + property.GetValue(this, null).ToString() + "'");
            }
            return propertyValues;
        }

        /// <summary>
        /// Takes values relivent to the control table and load them into class (can overload for popup)
        /// </summary>
        /// <param name="zoneInfo">info to control table to update</param>
        public void UpdateTable(EnvironmentInfo coolingInfo)
        {
            zones_id = coolingInfo.zoneID;
            label = coolingInfo.label;
            description = coolingInfo.description;
            if (coolingInfo.available != null)
                available = int.Parse(StringDictionaries.ConvertStringDatabase("cooling", "available", coolingInfo.available));
            if (coolingInfo.enabled != null)
                enabled = int.Parse(StringDictionaries.ConvertStringDatabase("cooling", "enabled", coolingInfo.enabled));
            if (coolingInfo.online != null)
                online = int.Parse(StringDictionaries.ConvertStringDatabase("cooling", "online", coolingInfo.online));
            setpoint = coolingInfo.setPoint;
            return_air_temp_f = coolingInfo.returnAirTempF;
            supply_air_temp_f = coolingInfo.supplyAirTempF;
            percent_load = coolingInfo.percentLoad;
            inlet_temp_f = coolingInfo.inletTempF;
            outlet_temp_f = coolingInfo.outletTempF;
            flow_rate_gpm = coolingInfo.flowRateGPM;
        }

        public void UpdateTable(CoolingPopupInfo coolingInfo)
        {
            zones_id = coolingInfo.zones_id;
            description = coolingInfo.description;
            label = coolingInfo.label;
            available = int.Parse(StringDictionaries.ConvertStringDatabase("cooling", "available", coolingInfo.available));
            enabled = int.Parse(StringDictionaries.ConvertStringDatabase("cooling", "enabled", coolingInfo.enabled));
            online = int.Parse(StringDictionaries.ConvertStringDatabase("cooling", "online", coolingInfo.online));
            setpoint = coolingInfo.setpoint;
            return_air_temp_f = coolingInfo.return_air_temp_f;
            supply_air_temp_f = coolingInfo.supply_air_temp_f;
            percent_load = coolingInfo.percent_load;
            inlet_temp_f = coolingInfo.inlet_temp_f;
            outlet_temp_f = coolingInfo.outlet_temp_f;
            flow_rate_gpm = coolingInfo.flow_rate_gpm;
        }
    }

    public class RackTable
    {
        public int racks_id { get; set; }
        public string proxy_address { get; set; }
        public string address { get; set; }
        public int rack_no { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public double v_nominal { get; set; }
        public double v_max { get; set; }
        public double v_min { get; set; }
        public double i_max { get; set; }
        public double i_min { get; set; }
        public int type { get; set; }
        public int available { get; set; }
        public int maint_mode { get; set; }
        public int enabled { get; set; }
        public int online { get; set; }
        public int rack_status { get; set; }
        public int contactor_status { get; set; }
        public int module_status { get; set; }
        public int startup_state { get; set; }
        public int status_flags { get; set; }
        public double soc { get; set; }
        public double kwh_max { get; set; }
        public double kwh_available { get; set; }
        public double v_actual { get; set; }
        public double i_actual { get; set; }
        public double t_avg { get; set; }
        public double t_max { get; set; }
        public double t_min { get; set; }
        public double v_cellavg { get; set; }
        public double v_cellmax { get; set; }
        public double v_cellmin { get; set; }
        public string image_filename { get; set; }
        public int zones_id { get; set; }
        public double c_rate { get; set; }

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyList.Add(property.Name.ToString());
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyValues.Add("'" + property.GetValue(this, null).ToString() + "'");
            }
            return propertyValues;
        }

        /// <summary>
        /// Takes values relivent to the control table and load them into class (can overload for popup)
        /// </summary>
        /// <param name="zoneInfo">info to control table to update</param>
        public void UpdateTable(RackInfo rackInfo)
        {
            zones_id = rackInfo.zoneID;
            label = rackInfo.label;
            description = rackInfo.description;
            available = int.Parse(StringDictionaries.ConvertStringDatabase("racks", "available", rackInfo.available));
            enabled = int.Parse(StringDictionaries.ConvertStringDatabase("racks", "enabled", rackInfo.enabled));
            rack_status = int.Parse(StringDictionaries.ConvertStringDatabase("racks", "rack_status", rackInfo.rackStatus));
            soc = rackInfo.SOC;
            v_actual = rackInfo.vActual;
            i_actual = rackInfo.iActual;
            t_avg = rackInfo.tAVG;
            t_max = rackInfo.tMax;
            t_min = rackInfo.tMin;
        }

        public void UpdateTable(RackPopupInfo rackInfo)
        {
            label = rackInfo.label;
            description = rackInfo.description;
            zones_id = rackInfo.zones_id;
            type = int.Parse(StringDictionaries.ConvertStringDatabase("racks", "type", rackInfo.type));
            available = int.Parse(StringDictionaries.ConvertStringDatabase("racks", "available", rackInfo.available));
            enabled = int.Parse(StringDictionaries.ConvertStringDatabase("racks", "enabled", rackInfo.enabled));
            rack_status = int.Parse(StringDictionaries.ConvertStringDatabase("racks", "rack_status", rackInfo.rack_status));
            soc = rackInfo.soc;
            v_actual = rackInfo.v_actual;
            i_actual = rackInfo.i_actual;
            t_avg = rackInfo.t_avg;
            t_max = rackInfo.t_max;
            t_min = rackInfo.t_min;
            v_cellavg = rackInfo.v_cellavg;
            v_cellmax = rackInfo.v_cellmax;
            v_cellmin = rackInfo.v_cellmin;
            v_max = rackInfo.v_max;
            v_min = rackInfo.v_min;
            i_max = rackInfo.i_max;
            i_min = rackInfo.i_min;
        }
    }

    public class InverterTable
    {
        public int inverters_id { get; set; }
        public string proxy_address { get; set; }
        public string address { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public int p_max { get; set; }
        public int p_min { get; set; }
        public int q_max { get; set; }
        public int q_min { get; set; }
        public int available { get; set; }
        public int maint_mode { get; set; }
        public int enabled { get; set; }
        public int state_demand { get; set; }
        public int state_actual { get; set; }
        public int inverter_mode { get; set; }
        public int status1 { get; set; }
        public int status2 { get; set; }
        public int error_ct { get; set; }
        public double v_avg { get; set; }
        public double v1_actual { get; set; }
        public double v2_actual { get; set; }
        public double v3_actual { get; set; }
        public double i_avg { get; set; }
        public double i1_actual { get; set; }
        public double i2_actual { get; set; }
        public double i3_actual { get; set; }
        public int igbt_temp { get; set; }
        public int p_demand { get; set; }
        public int p_actual { get; set; }
        public int q_demand { get; set; }
        public int q_actual { get; set; }
        public double v_dcbus { get; set; }
        public double i_dcbus { get; set; }
        public string image_filename { get; set; }
        public int zones_id { get; set; }

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyList.Add(property.Name.ToString());
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyValues.Add("'" + property.GetValue(this, null).ToString() + "'");
            }
            return propertyValues;
        }

        /// <summary>
        /// Takes values relivent to the control table and load them into class (can overload for popup)
        /// </summary>
        /// <param name="zoneInfo">info to control table to update</param>
        public void UpdateTable(InverterInfo inverterInfo)
        {
            zones_id = inverterInfo.zoneID;
            description = inverterInfo.description;
            label = inverterInfo.label;
            available = int.Parse(StringDictionaries.ConvertStringDatabase("inverters", "available", inverterInfo.available));
            enabled = int.Parse(StringDictionaries.ConvertStringDatabase("inverters", "enabled", inverterInfo.enabled));
            state_actual = int.Parse(StringDictionaries.ConvertStringDatabase("inverters", "state_actual", inverterInfo.status));
            v1_actual = inverterInfo.v1Actual;
            v2_actual = inverterInfo.v2Actual;
            v3_actual = inverterInfo.v3Actual;
            i1_actual = inverterInfo.i1Actual;
            i2_actual = inverterInfo.i2Actual;
            i3_actual = inverterInfo.i3Actual;
            p_demand = inverterInfo.kVADemand;
            q_demand = inverterInfo.kvARDemand;
            p_actual = inverterInfo.kVAActual;
            q_actual = inverterInfo.kvARActual;
        }

        public void UpdateTable(InvertersPopupInfo inverterInfo)
        {
            label = inverterInfo.label;
            description = inverterInfo.description;
            zones_id = inverterInfo.zones_id;
            available = int.Parse(StringDictionaries.ConvertStringDatabase("inverters", "available", inverterInfo.available));
            enabled = int.Parse(StringDictionaries.ConvertStringDatabase("inverters", "enabled", inverterInfo.enabled));
            state_actual = int.Parse(StringDictionaries.ConvertStringDatabase("inverters", "state_actual", inverterInfo.state_actual));
            p_demand = inverterInfo.p_demand;
            q_demand = inverterInfo.q_demand;
            p_actual = inverterInfo.p_actual;
            q_actual = inverterInfo.q_actual;
            v1_actual = inverterInfo.v1_actual;
            v2_actual = inverterInfo.v2_actual;
            v3_actual = inverterInfo.v3_actual;
            i1_actual = inverterInfo.i1_actual;
            i2_actual = inverterInfo.i2_actual;
            i3_actual = inverterInfo.i3_actual;
            v_dcbus = inverterInfo.v_dcbus;
            i_dcbus = inverterInfo.i_dcbus;
        }
    }

    public class ModuleTable
    {
        public int modules_id { get; set; }
        public int module_no { get; set; }
        public double soc { get; set; }
        public double v_actual { get; set; }
        public int temp_f { get; set; }
        public string install_date { get; set; }
        public int racks_id { get; set; }

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyList.Add(property.Name.ToString());
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyValues.Add("'" + property.GetValue(this, null).ToString() + "'");
            }
            return propertyValues;

        }
    }

    public class EventsTable
    {

        public int event_id { get; set; }
        public string event_key { get; set; }
        public int severity { get; set; }
        public int is_alarm { get; set; }
        public int active { get; set; }
        public int zone_id { get; set; }
        public int device_type { get; set; }
        public int device_id { get; set; }
        public string additional_info { get; set; }
        public string create_time { get; set; }
        public string ack_time { get; set; }
        public int ack_user { get; set; }
        public string ack_notes { get; set; }
        public string clear_time { get; set; }
        public int dismissed { get; set; }
        public int event_type { get; set; }

        public List<string> GetProperyNames()
        {
            List<string> propertyList = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyList.Add(property.Name.ToString());
            }
            return propertyList;
        }

        public List<string> GetPropertyValues()
        {
            List<string> propertyValues = new List<string>();

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                propertyValues.Add("'" + property.GetValue(this, null).ToString() + "'");
            }
            return propertyValues;

        }
    }

    /// <summary>
    /// Class to house all asset information
    /// </summary>
    public class ControlAssetInfo
    {
        public List<ZoneTable> zoneList { get; set; }
        public List<RackTable> rackList { get; set; }
        public List<InverterTable> inverterList { get; set; }
        public List<CoolingTable> coolingList { get; set; }

        /// <summary>
        /// Constructor to get all info from IP address
        /// </summary>
        /// <param name="ipAddress"></param>
        public ControlAssetInfo(string ipAddress)
        {
            zoneList = IHI_ZMQ.UI__GetData_ZoneList(ipAddress);
            rackList = IHI_ZMQ.UI__GetData_RackList(ipAddress);
            inverterList = IHI_ZMQ.UI__GetData_InverterList(ipAddress);
            coolingList = IHI_ZMQ.UI__GetData_CoolingList(ipAddress);
        }

        public ControlAssetInfo() { }

        /// <summary>
        /// Loads all values contained into database
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="decsDirectory"></param>
        public void LoadValuesIntoDatabase(string ipAddress, string username, string password, string decsDirectory)
        {
            ZonesPage.UpdateDatabaseFromZonesTableList(zoneList, ipAddress, username, password, decsDirectory);
            RacksPage.UpdateDatabaseFromRacksTableList(rackList, ipAddress, username, password, decsDirectory);
            InvertersPage.UpdateDatabaseFromInvertersTableList(inverterList, ipAddress, username, password, decsDirectory);
            EnvironmentPage.UpdateDatabaseFromCoolingTableList(coolingList, ipAddress, username, password, decsDirectory);
        }
    }
}
