﻿//using AventStack.ExtentReports;
//using AventStack.ExtentReports.Reporter;
//using ihi_testlib_espilot;
//using ihi_testlib_modbus;
//using ihi_testlib_videorecorder;
//using NUnit.Framework;
//using NUnit.Framework.Interfaces;
//using OpenQA.Selenium;
//using Renci.SshNet;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Diagnostics;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ihi_testlib_espilot.LibFiles
//{
//public class Pilot_Initialization
//{

//    #region Variables 
//    public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
//    public static ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
//    ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(ihi_testlib_espilot.GlobalsGeneralStrings.SSHhostname, ihi_testlib_espilot.GlobalsGeneralStrings.SSHusername, ihi_testlib_espilot.GlobalsGeneralStrings.SSHpassword, ihi_testlib_espilot.GlobalsGeneralStrings.DECSDirectory);

//        #endregion




//        public static void PilotSetup()
//    {
//       // var testname = GetType().Namespace;
//        CleanIIS();
//        string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
//        //var reportDir = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["ExtentReport"].ToString();
//        //Directory.CreateDirectory(reportDir);
//        //var fileName = GetType().Namespace + ".html";
//        //var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
//        //htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
//        //htmlReporter.AppendExisting = true;
//        //_extent.AttachReporter(htmlReporter);
//        //Stoping ESPilot DECS
//        var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
//        ihi_testlib_espilot.SSH.StopDecs(sshInfo);
//        System.Threading.Thread.Sleep(5000);
//        //Starting the Inverter simulator
//        Process p = new Process();
//        p.StartInfo.FileName = ConfigurationManager.AppSettings["CMDPath"].ToString();
//        p.StartInfo.WorkingDirectory = pjtParent + ConfigurationManager.AppSettings["IISExpress"].ToString();
//        p.StartInfo.Arguments = @"/K iisexpress /path:" + pjtParent + ConfigurationManager.AppSettings["invertersimPjtPath"].ToString() + " /port:" + ConfigurationManager.AppSettings["InverterPort"].ToString();
//        p.Start();
//        //Starting the Battery simulator
//        p = new Process();
//        p.StartInfo.FileName = ConfigurationManager.AppSettings["CMDPath"].ToString();
//        p.StartInfo.WorkingDirectory = pjtParent + ConfigurationManager.AppSettings["IISExpress"].ToString();
//        p.StartInfo.Arguments = @"/K iisexpress /path:" + pjtParent + ConfigurationManager.AppSettings["LishenBatterySimPjtPath"].ToString() + " /port:" + ConfigurationManager.AppSettings["BatteryPort"].ToString();
//        p.Start();
//        //Opening the web application of inverter and battery simulator to open the connection
//        System.Threading.Thread.Sleep(3000);
//        webDriver.SetBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, "http://localhost:" + ConfigurationManager.AppSettings["BatteryPort"].ToString());
//        webDriver.SetBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, "http://localhost:" + ConfigurationManager.AppSettings["InverterPort"].ToString());
//        System.Threading.Thread.Sleep(9000);
//        // Start ESPilot DECS
//        try
//        {
//            ihi_testlib_espilot.SSHAndStartDECSDirectory decsInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(sshInfo, GlobalVariables.DECSDirectory);
//            var startDECSSuccess = ihi_testlib_espilot.SSH.StartDecs(ihi_testlib_espilot.DECSLaunchType.Normal, decsInfo, 10000, "");
//            System.Threading.Thread.Sleep(GlobalVariables.TimingConstants.DECSLaunch);
//        }
//        catch (Exception ex)
//        {
//            Console.WriteLine(ex);
//        }
//        // Making sure Battery and Inverter is shown with connected status 
//        string batteryConnection = "";
//        do
//        {
//            batteryConnection = ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
//            System.Threading.Thread.Sleep(1000);
//        } while (!batteryConnection.Contains("CONNECTED"));
//        string InverterConnection = "";
//        do
//        {
//            InverterConnection = ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
//            System.Threading.Thread.Sleep(1000);
//        } while (!InverterConnection.Contains("CONNECTED"));
//        Array.ForEach(Process.GetProcessesByName("chrome"), x => x.Kill());
//    }

//    [OneTimeTearDown]
//    protected void TearDown()
//    {
//        _extent.Flush();
//        //stop IIS EXPRESS 
//        CleanIIS();
//        webDriver.browserDriver.Quit();
//    }

//    public static void CleanIIS()
//    {
//        Array.ForEach(Process.GetProcessesByName("iisexpress"), x => x.Kill());
//        Array.ForEach(Process.GetProcessesByName("cmd"), x => x.Kill());
//        Array.ForEach(Process.GetProcessesByName("chrome"), x => x.Kill());
//    }
//}
//}
