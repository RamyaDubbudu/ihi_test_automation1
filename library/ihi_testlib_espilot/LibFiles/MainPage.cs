﻿/*==============================================================================
Name        : Mainpage.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Class created to navigate easily around Main Page
==============================================================================*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Threading;
//using OpenQA.Selenium.Chrome;

namespace ihi_testlib_espilot
{


    public static class MainPage
    {
        /// <summary>
        /// navigates to Dashboard tab and selects as current frame
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void navigateToDashboard(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.SwitchTo().DefaultContent();
            IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsMainPage.DashboardTab));
            tab.Click();
        }

        /// <summary>
        /// navigates to Control tab and selects as current frame
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void navigateToControl(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.SwitchTo().DefaultContent();
            IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsMainPage.ControlTab));
            tab.Click();
        }

        //navigates to Alarms tab and selects as current frame
        public static void navigatetoAlarms(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.SwitchTo().DefaultContent();
            IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsMainPage.AlarmsTab));
            tab.Click();
        }

        //navigates to Events tab and selects as current frame
        public static void navigateToEvents(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.SwitchTo().DefaultContent();
            IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsMainPage.EventsTab));
            tab.Click();
        }

        //navigates to Charts tab and selects as current frame
        public static void navigateToCharts(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.SwitchTo().DefaultContent();
            IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsMainPage.ChartsTab));
            tab.Click();
        }

        //navigates to Admin tab and selects as current frame
        public static void navigateToAdmin(ihi_testlib_selenium.Selenium driver)
        { 
            
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AdminTab));
        }

        //navigates to Startup/Shutdown window and selects as current frame
        public static void navigateToStartupShutDown(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.SwitchTo().DefaultContent();
            if (driver.IsElementVisible(nameTXT: "tabsFrame")) //GeneralDECS.IsElementPresent(By.Name("tabsFrame"), driver))
            {
                driver.browserDriver.SwitchTo().Frame(driver.browserDriver.FindElement(By.Name("tabsFrame")));
            }
        }

        /// <summary>
        /// Click logout button
        /// </summary>
        /// <param name="driver">Firefox driver</param>
        public static void logout(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.SwitchTo().DefaultContent();
            driver.WaitForElement(xpath: string.Format(GlobalElementsMainPage.LogoutButton));
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.LogoutButton));
            //driver.browserDriver.FindElement(GlobalElementsMainPage.LogoutButton).Click();
            System.Threading.Thread.Sleep(TimingConstants.UILoadAfterDatabaseChange);
        }

        /// <summary>
        /// Returns the UI Revision number located on bottom of page
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static int CheckUIRevision(ihi_testlib_selenium.Selenium driver)
        {
            string BuildString = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.UIRevision));
            return Int32.Parse(BuildString);
        }

        /// <summary>
        /// Returns the Server Revision number located on bottom of page
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static int CheckServerRevision(ihi_testlib_selenium.Selenium driver)
        {
            string BuildString = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.ServerRevision));
            return Int32.Parse(BuildString);
        }

        /// <summary>
        /// Returns the current time from the bottom of the page
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static string CheckTime(ihi_testlib_selenium.Selenium driver)
        {
            return driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.Time));
        }
    }
}