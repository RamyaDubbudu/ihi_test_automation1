﻿/*==============================================================================
Name        : SSH.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Class created to manage methods that talk to a SSH server.
==============================================================================*/

using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Renci.SshNet;
using System.Configuration;
using System.Text.RegularExpressions;
using Renci.SshNet.Common;
using System.IO;

namespace ihi_testlib_espilot
{
    public static class SSH
    {
        static string HostName = ConfigurationManager.AppSettings["ESPilotIPAddress"];
        static string UserName = ConfigurationManager.AppSettings["Username"];
        static string Password = ConfigurationManager.AppSettings["Password"];
        static string port = ConfigurationManager.AppSettings["ContainerPort"];
        static string key =  ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["Privatekey"].ToString();
        static AuthenticationMethod auth;
        static int p = 22;

        public static AuthenticationMethod authentication()
        {
            if (!string.IsNullOrEmpty(port))
                p = int.Parse(port);
            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(UserName))
            {
                Console.WriteLine("Using public key authentication with key {0}", key);
                auth = new PrivateKeyAuthenticationMethod(UserName, new PrivateKeyFile[] { new PrivateKeyFile(key) });
            }
            else if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
            {
                Console.WriteLine("Using password authentication");
                auth = new PasswordAuthenticationMethod(UserName, Password);
            }
            else
            {
                throw new Exception("Please ensure that username, and either PrivateKey or Password setting are defined in the configuration file.");
            }
            return auth;
        }
          
        public static PrivateKeyFile keyFile = new PrivateKeyFile(ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["Privatekey"].ToString());
        public static PrivateKeyFile[] keyFiles = new[] { keyFile };
        public static ConnectionInfo connectionInfo = new ConnectionInfo(ConfigurationManager.AppSettings["ESPilotIPAddress"].ToString(),
                                   ConfigurationManager.AppSettings["Username"].ToString(),
                                    new PasswordAuthenticationMethod(ConfigurationManager.AppSettings["Username"].ToString(), ConfigurationManager.AppSettings["Password"].ToString()),
                                    new PrivateKeyAuthenticationMethod(ConfigurationManager.AppSettings["Username"].ToString(), keyFiles));

        public static ConnectionInfo ConnNfo = new ConnectionInfo(HostName, p, UserName, new AuthenticationMethod[] { auth });

        public static bool ExecutingOnDocker()
        {
            bool isDocker = false;
            if (ConfigurationManager.AppSettings["IsDocker"].ToString() == "True")
                isDocker = true;
            else if (ConfigurationManager.AppSettings["IsDocker"].ToString() == "False")
                isDocker = false;
            return isDocker;
        }
        /// <summary>
        /// Connects to an SSH session and returns the client
        /// </summary>
        /// <param name="hostname">hostname or IP address</param>
        /// <param name="username">ssh session user name</param>
        /// <param name="password">password for session</param>
        /// <returns></returns>
        public static SshClient ConnectSSH(string hostname, string username, string password)
        {
            SshClient ssh = new SshClient(connectionInfo);
            ssh.Connect();
            return ssh;
        }

        public static SshClient ConnectSSHPrivatekey(string hostname, string username)
        {
            SshClient ssh = new SshClient(connectionInfo);
            ssh.Connect();
            return ssh;
        }
        /// <summary>
        /// writes command to SSH by hostname/ip address and reads back the information. Any Linux command can be written
        /// </summary>
        /// <param name="sshcommand">Command to write to the terminal</param>
        /// <param name="hostname"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string OpenWriteCloseSSH(string sshcommand, string hostname, string username, string password)
        {
            var ssh = new SshClient(connectionInfo);
            ssh.Connect();
            
            var terminal = ssh.RunCommand(sshcommand);
            ssh.Disconnect();
            return terminal.Result;
        }
        public static void ExecCommand(SshClient ssh, string sshCommand)
        {
            if (!ssh.IsConnected)
                ssh.Connect();
            Thread thread = null;

            Task task = Task.Run(() =>
            {
                thread = Thread.CurrentThread;
                ssh.RunCommand(sshCommand);
            });
            var terminal = ssh.RunCommand("ps -e | grep decs");
            do
            {
                Thread.Sleep(1000);
                terminal = ssh.RunCommand("ps -e | grep decs");
            } while (terminal.Result.Count() == 6);
        }

        public static void SetDCSimulatorIPAddress(string hostname, string username, string password)
        {
            var ssh = new SshClient(connectionInfo);
            ssh.Connect();
            string killisomb = string.Empty;
            string resetisomb = string.Empty;
            if (ExecutingOnDocker())
            {
                killisomb = string.Format(DockerCommands.SSHCommands.Killisomb);
                resetisomb = string.Format(DockerCommands.SSHCommands.resetisomb, ConfigurationManager.AppSettings["DCIpAddress"].ToString());
                SSH.OpenWrite(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                SSH.OpenWrite(ssh, resetisomb);
                System.Threading.Thread.Sleep(1000);
            }
            else
            {
                killisomb = "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_isomb";
                resetisomb = "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_isomb -D " + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + " -l 0 &";
                SSH.ExecCommand(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                SSH.ExecCommand(ssh, resetisomb);
                System.Threading.Thread.Sleep(1000);
            }
        }

        public static void SetIO_SimulatorIPAddress(string hostname, string username, string password)
        {
            var ssh = new SshClient(connectionInfo);
            ssh.Connect();
            string killio = string.Empty;
            string resetio = string.Empty;
            if (ExecutingOnDocker())
            {
                killio = string.Format(DockerCommands.SSHCommands.Kill_IO);
                resetio = string.Format(DockerCommands.SSHCommands.resetIOIPaddress, ConfigurationManager.AppSettings["IOModbusServer"].ToString());
                ssh.RunCommand(killio);
                System.Threading.Thread.Sleep(1000);
                ssh.RunCommand(resetio);
                System.Threading.Thread.Sleep(1000);
            }
            else
            {
                killio = "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_iod";
                resetio = "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_iod -j 1 -d -i " + ConfigurationManager.AppSettings["IOModbusServer"].ToString() + "-z 1 -c 9";
                SSH.ExecCommand(ssh, killio);
                System.Threading.Thread.Sleep(1000);
                SSH.ExecCommand(ssh, resetio);
                System.Threading.Thread.Sleep(1000);
            }
        }
        /// <summary>
        /// Writes command to open ssh session and returns the response
        /// </summary>
        /// <param name="ssh"></param>
        /// <param name="sshCommand"></param>
        /// <returns></returns>
        public static string OpenWrite(SshClient ssh, string sshCommand)
        {
           // ssh = new SshClient(connectionInfo);
            ssh.Connect();
            var terminal = ssh.RunCommand(sshCommand);
            return terminal.Result.Replace("\n", ""); 
        }

        /// <summary>
        /// Writes command to open ssh session and returns the response
        /// </summary>
        /// <param name="ssh"></param>
        /// <param name="sshCommand"></param>
        /// <returns></returns>
        public static string ExecuteSqlite(SshClient ssh, string sshCommand, string dockerContainerName)
        {
            ssh = new SshClient(connectionInfo);
            ssh.Connect();
            string command = string.Empty;
            if (ExecutingOnDocker())
            {
                command = string.Format(DockerCommands.SSHCommands.DockerSQLiteVolumns, sshCommand, dockerContainerName, ConfigurationManager.AppSettings["DatabaseName"].ToString());
            }
            else
            {
                command = sshCommand;
            }
            var terminal = ssh.RunCommand(command);
            return terminal.Result.Replace("\n", "");
        }
        /// <summary>
        /// Reads data from database by asking domain controller for information (using Query executable).
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="ID"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <returns></returns>
        public static string ReadFromDCOpenClose(string table, string column, int ID, string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/usr/local/ihi-decs/bin/")
        {
            ID = ID - 1;
            var ssh = new SshClient(connectionInfo);
            string sql = string.Format("SELECT {0} FROM {1} LIMIT 1 OFFSET {2}", column, table, ID);
            return QueryInterface.CommandThroughQuery(sql, ssh, SSHIPAddress, DECSDirectory);
        }


        /// <summary>
        /// Updates database through Query program using SSH by table, column and row number in database. Filtered by domain controller.
        /// </summary>
        /// <param name="SSHUpdateValue"></param>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="ID"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        public static void UpdateToDCOpenClose(string SSHUpdateValue, string table, string column, int ID, string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/usr/local/ihi-decs/bin/")
        {
            var ssh = new SshClient(connectionInfo);
            //var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
            string SSHCommand;
            try
            {
                SSHCommand = string.Format("UPDATE {0} SET {1}='{2}' WHERE {0}_ID='{3}'", table, column, SSHUpdateValue, ID.ToString());
            }
            catch
            {
                //user_id is the only column singular where the table is not
                SSHCommand = string.Format("UPDATE {0} SET {1}='{2}' WHERE user_ID='{3}'", table, column, SSHUpdateValue, ID.ToString());
            }
            QueryInterface.CommandThroughQuery(SSHCommand, ssh, SSHIPAddress, DECSDirectory);
        }

        /// <summary>
        /// Read from the database with the option of using the sqlite3 program
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="ID"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <returns></returns>
        public static string ReadFromDatabaseOpenClose(string table, string column, int ID, string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/", bool useQuery = true, string databaseName = "decs_dc.db")
        {
            ID = ID - 1;
            var ssh = new SshClient(connectionInfo);
            //var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
            string SSHCommand = string.Format("SELECT {0} FROM {1} LIMIT 1 OFFSET {2};", column, table, ID.ToString());
            if (useQuery)
            {
                return QueryInterface.CommandThroughQuery(SSHCommand, ssh, SSHIPAddress, decsLocation, databaseName:databaseName);
            }

            ssh.Connect();
            string sDatabaseRead = string.Format("sqlite3 {0}decs_dc.db \"{1}\"", DECSDirectory, SSHCommand);
            //Console.WriteLine(sDatabaseRead);
            string result = ssh.RunCommand(sDatabaseRead).Result;
            ssh.Disconnect();

            return result;
        }

        /// <summary>
        /// Read directly from the decs_dc.db database returns all values using sqlite3 program
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <returns></returns>
        public static string ReadAllFromDatabaseOpenClose(string table, string column, string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/", bool useQuery = false)
        {
            var ssh = new SshClient(connectionInfo);
            //var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
            string SSHCommand = string.Format("SELECT {0} FROM {1};", column, table);
            if (useQuery)
            {
                return QueryInterface.CommandThroughQuery(SSHCommand, ssh, SSHIPAddress, decsLocation, false); 
            }

            ssh.Connect();
            string sDatabaseRead = string.Format("sqlite3 {0}decs_dc.db \"{1}\"", DECSDirectory, SSHCommand);
            //Console.WriteLine(sDatabaseRead);
            string result = ssh.RunCommand(sDatabaseRead).Result;
            ssh.Disconnect();

            return result;
        }

        /// <summary>
        /// Write directly to decs_dc.db database using sqlite3 program
        /// </summary>
        /// <param name="SSHUpdateValue"></param>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="ID"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <param name="debug"></param>
        /// <returns></returns>
        public static string UpdateToDatabaseOpenClose(string SSHUpdateValue, string table, string column, int ID,
            string SSHIPAddress, string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/", string column2value = "", string column2name = "")
        {
            var ssh = new SshClient(connectionInfo);
            string SSHCommand;
            try
            {
                if (column2name == "" && column2value == "")
                {
                    SSHCommand = string.Format(@"UPDATE {0} SET {1}='{2}'", table, column, SSHUpdateValue);
                }
                else
                {
                    SSHCommand = string.Format(@"UPDATE {0} SET {1}='{2}' WHERE {3} ='{4}'", table, column, SSHUpdateValue, column2name, column2value);
                }
            }
            catch
            {
                //user_id is the only column singular where the table is not
                SSHCommand = string.Format(@"UPDATE {0} SET {1}='{2}' WHERE {3} ='{4}'", table, column, SSHUpdateValue, column2name, column2value);
            }
            return QueryInterface.CommandThroughQuery(SSHCommand, ssh, SSHIPAddress, decsLocation);
        }

        /// <summary>
        /// Query CPU/Memory usage of by process name
        /// </summary>
        /// <param name="processname">process name of what information requested</param>
        /// <param name="memory">output of memory used</param>
        /// <param name="CPUPercent"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        public static void QueryMemCPUBYProcess(string processname, out string memory, out string CPUPercent, string SSHIPAddress, string SSHUsername,
            string SSHPassword)
        {
            var ssh = new SshClient(connectionInfo);
            //SshClient ssh = ConnectSSH(SSHIPAddress, SSHUsername, SSHPassword);

            var terminal = ssh.RunCommand("pgrep " + processname + "\n");
            string processNumber = terminal.Result;

            //remove line feed
            processNumber = processNumber.Replace("\n", string.Empty);

            //query CPU use percentage
            terminal = ssh.RunCommand(string.Format("ps -p {0} -o pcpu | grep -v CPU\n", processNumber));
            CPUPercent = terminal.Result;

            //query memory usage stat
            terminal = ssh.RunCommand(string.Format("ps -o vsz -p {0} | grep -v VSZ\n", processNumber));
            memory = terminal.Result;

            //removes whitespace
            CPUPercent = CPUPercent.Replace("\n", string.Empty).Replace(" ", string.Empty);
            memory = memory.Replace("\n", string.Empty).Replace(" ", string.Empty);

            ssh.Disconnect();
        }

        /// <summary>
        /// Clears a SQL Table by table name
        /// </summary>
        /// <param name="tablelocation"></param>
        /// <param name="databasefilename"></param>
        /// <param name="tablename"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        public static void ClearSQLTable(string tablelocation, string databasefilename, string tablename, string SSHIPAddress, 
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/", bool useQuery = true)
        {
            var ssh = new SshClient(connectionInfo);
            //SshClient ssh = ConnectSSH(SSHIPAddress, SSHUsername, SSHPassword);

            //if (useQuery)
            //{
            //    var sql = string.Format("DELETE FROM {0}", tablename);
            //    QueryInterface.CommandThroughQuery(sql, ssh, SSHIPAddress, decsLocation);
            //    return;
            //}

            //string deleteAllRows = string.Format("echo \"{3}\" | sudo -S sqlite3 {0}{1} \"DELETE FROM {2}\"\n", tablelocation, databasefilename, tablename, SSHPassword);
            //var terminal = ssh.RunCommand(deleteAllRows).Execute();
            string createuser = string.Format("DELETE FROM {0}", tablename);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, DECSDirectory);
        }

        /// <summary>
        /// Restarts the apache server with timeout value
        /// </summary>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static bool ResetApache2(string SSHIPAddress, string SSHUsername, string SSHPassword, int timeout)
        {
            string RestartApache2Command = string.Format("echo \"{0}\" | sudo -S service apache2 restart\n", SSHPassword);
            int WaitTimer = 0;
            try
            {
                var ssh = new SshClient(connectionInfo);
                //SshClient ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
                ssh.Connect();
                Console.WriteLine("\nApache reseting, please wait.\n");

                var terminal = ssh.RunCommand(RestartApache2Command);

                while (WaitTimer <= timeout)
                {
                    Console.Write("...");
                    Console.WriteLine(terminal.Result);
                    Thread.Sleep(1000);
                    WaitTimer += 1000;
                    if (terminal.Result != "")
                        break;
                }

                //Console.WriteLine(terminal.Result);

                ssh.Disconnect();
                ssh.Dispose();

                string removespace = terminal.Result;
                removespace = removespace.Replace(" ", String.Empty);
                removespace = removespace.Replace("\n", String.Empty);

                //Console.WriteLine(removespace);
                if (removespace == "*Restartingwebserverapache2...done.")
                {
                    Console.Write("Restart Successful\n");
                    return true;
                }
                else
                {
                    Console.WriteLine("Apache server failed to update!\n");
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        /// <summary>
        /// kills a process in Linux by process name
        /// </summary>
        /// <param name="process"></param>
        /// <param name="hostname"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public static void KillProcessByName(string process, SSHInfo sshInfo)
        {
            try
            {
                string command = string.Format("echo \"{0}\" | sudo -S pkill -f {1}\n", sshInfo.SSHPassword, process);
                OpenWriteCloseSSH(command, sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Determines the type (INT, FLOAT, String...) from Database.  This will not work when decs_dc is running
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <param name="DatabaseFileName"></param>
        /// <returns></returns>
        public static string GetTypeFromDatabase(string table, string column,
            string SSHIPAddress, string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/", string DatabaseFileName = "decs_dc.db")
        {
            string type = "-1";
            //Command to get table info and cut out type
            string command = string.Format("sqlite3 {0}{1} \"PRAGMA table_info({2})\" | grep '|{3}|' | cut -d '|' -f 3",
                DECSDirectory, DatabaseFileName, table, column);
            try
            {
                type = OpenWriteCloseSSH(command, SSHIPAddress, SSHUsername, SSHPassword);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return type;
        }

        /// <summary>
        /// Returns revision info from SNV in string format
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <returns></returns>
        public static string GetRevisionInfoFromDirectory(string directory, string SSHIPAddress, string SSHUsername, string SSHPassword)
        {
            string revisionInfo = "";
            string command = string.Format("svn info -r HEAD {0}\n", directory);

            try
            {
                revisionInfo = OpenWriteCloseSSH(command, SSHIPAddress, SSHUsername, SSHPassword);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                revisionInfo = "-1";
            }
            return revisionInfo;
        }

        /// <summary>
        /// Kills all processes on the backend that contain the string "decs".
        /// </summary>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <returns></returns>
        public static bool KillDecs(SSHInfo sshInfo)
        {
            try
            {
                string killDecsCommand = string.Format("echo \"{0}\" | sudo -S pkill -f decs\n", sshInfo.SSHPassword);
                OpenWriteCloseSSH(killDecsCommand, sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void StopDecs(SSHAndStartDECSDirectory sshInfo)
        {
            var ssh = new SshClient(connectionInfo);
            var stopDecs = "";
            if (ExecutingOnDocker())
            {
                stopDecs = string.Format(DockerCommands.SSHCommands.StopDecs);
            }
            else
            {              
                stopDecs = string.Format("cd {0}&&echo \'{1}\' | sudo -S {0}./stop_decs", sshInfo.SSHStartDECSDirectory, sshInfo.SSHPassword);
            }
                ssh.Connect();
            var terminal = ssh.RunCommand(stopDecs);         
            ssh.Dispose();           
        }

        /// <summary>
        /// Restarts Decs depending on how the enum launchType is selected
        /// </summary>
        /// <param name="launchType"></param>
        /// <param name="directory"></param>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static bool StartDecs(DECSLaunchType launchType, SSHAndStartDECSDirectory sshInfo, int timeout, string arguments = "")
        {
            string startDecs = "";
            string[] processes = null;

            switch (launchType)
            {
                //all of these will have to be updated to included which expected processes will be running.  Only DECSLaunchType.Test is correct.
                    case DECSLaunchType.Normal:
                    processes = new string[] { "decs_dc", "decs_eventd", "decs_pumpd", "decs_upsd", "decs_isomb", "decs_init_dc" }; //"decs_sel7xxd"
                    if (ExecutingOnDocker())
                    {
                        startDecs = string.Format(DockerCommands.SSHCommands.StartDecs);
                    }
                    else
                    {
                        startDecs = string.Format("cd /usr/local/ihi-decs/bin&&echo {0} | ./start_decs", sshInfo.SSHPassword);
                    }
                    break;
                case DECSLaunchType.Debug:
                    processes = new string[] { "decs_dc", "decs_eventd", "decs_pumpd", "decs_upsd", "decs_isomb", "decs_init_dc" }; //"decs_sel7xxd"
                    startDecs = string.Format("cd /usr/local/ihi-decs/bin&&echo {0} | ./start_decs -d", sshInfo.SSHPassword);
                    break;
                case DECSLaunchType.Service:
                    processes = new string[] { "decs_dc", "decs_eventd", "decs_pumpd", "decs_sel7xxd", "decs_init" };
                    var serviceArguement = arguments == "" ? "start" : arguments;
                    startDecs = string.Format("echo {0} | sudo -S service ihi-decs {1}", sshInfo.SSHPassword, serviceArguement);
                    break;
                case DECSLaunchType.Test:
                    processes = new string[] { "decs_dc", "decs_eventd", "decs_pumpd" };
                    startDecs = string.Format("/usr/local/ihi-decs/bin/start_decs -t", sshInfo.SSHStartDECSDirectory, sshInfo.SSHPassword, arguments);
                    break;
                case DECSLaunchType.TestWithoutTSwitch:
                    processes = new string[] { "decs_dc", "decs_eventd", "decs_pumpd" };
                    startDecs = string.Format("cd {0}&&echo \'{1}\' | sudo -S {0}./QA_start_decs_without_test {2}", sshInfo.SSHStartDECSDirectory, sshInfo.SSHPassword, arguments);
                    break;
                default:
                    Console.WriteLine("Unrecognized launch type.");
                    return false;
            }

            try
            {
                var ssh = new SshClient(connectionInfo);
                //SshClient ssh = new SshClient(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
                ssh.Connect();
                // var terminal = ssh.RunCommand(startDecs);
                //Console.WriteLine("\nDECS backend restarting, please wait.\n");


                //task does not complete.  Must run async and kill it.
                Thread thread = null;

                Task task = Task.Run(() =>
                {
                    thread = Thread.CurrentThread;
                    ssh.RunCommand(startDecs);
                });

                //Checks that all processes that are supposed to be running are running
                bool allProcessRunning = false;

                //Begin timeout
                TimeSpan maxDuration = TimeSpan.FromMilliseconds(timeout);
                Stopwatch sw = Stopwatch.StartNew();

                while (sw.Elapsed < maxDuration && !allProcessRunning)
                {
                    var terminal = ssh.RunCommand("ps -e | grep decs");
                    if (processes == null)
                        break;

                    foreach (string process in processes)
                    {
                        if (terminal.Result.Contains(process))
                        {
                            allProcessRunning = true;
                        }
                        else
                        {
                            allProcessRunning = false;
                            break;
                        }
                    }
                }
                thread.Abort();
                ssh.Disconnect();
                if (allProcessRunning)
                    return true;
                else
                    return false;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        /// <summary>
        /// Check that a process is running within a timeout period
        /// </summary>
        /// <param name="process"></param>
        /// <param name="timeout"></param>
        /// <param name="sshInfo"></param>
        /// <returns></returns>
        public static bool CheckProcessRunning(string process, int timeout, SSHInfo sshInfo)
        {
            try
            {
                var ssh = new SshClient(connectionInfo);
                //SshClient ssh = new SshClient(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
                ssh.Connect();

                TimeSpan maxDuration = TimeSpan.FromMilliseconds(timeout);
                Stopwatch sw = Stopwatch.StartNew();

                bool processRunning = false;

                while (sw.Elapsed < maxDuration && !processRunning)
                {
                    var terminal = ssh.RunCommand("ps -e | grep decs");

                    if (terminal.Result.Contains(process))
                    {
                        return true;
                    }
                    Thread.Sleep(100);
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Searches a specfic file for a string.  This is usually used for a log file to check if an even occurred.
        /// </summary>
        /// <param name="expectedString"></param>
        /// <param name="fileName"></param>
        /// <param name="sshInfo"></param>
        /// <returns></returns>
        public static bool SearchLogFilesForString(string expectedString, string logfileName, SSHAndDatabaseDirectory sshInfo, string logfileLocation = "/var/log/")
        {
            string formatString = string.Format("if grep -q {0} \"{1}{2}\"; then echo 1; else echo 0; fi", expectedString, logfileLocation, logfileName);

            try
            {
                string containsString = SSH.OpenWriteCloseSSH(formatString, sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
                if (containsString == "1\n")
                    return true;
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }


        public static string SearchLogFilesForString(string searchstring, string hostname, string username, string password, string filePath = "decs_isomb.log")
        {
            string formatString = string.Empty;
            if (ExecutingOnDocker() && filePath == "decs_isomb.log")
            {
                formatString = string.Format("grep -H -r \"{0}\" {1}", searchstring, "/usr/local/ihi/pilot/dc/runtime/" + filePath);
            }
            else if (ExecutingOnDocker() && filePath == "decs_batteryd.log")
            {
                formatString = string.Format("grep -H -r \"{0}\" {1}", searchstring, "/usr/local/ihi/pilot/zc1/runtime/" + filePath);
            }
            else
            {
                formatString = string.Format("grep -H -r \"{0}\" {1}", searchstring, "/tmp/" + filePath);
            }
            //string formatString = string.Format("if grep -q {0} \"{1}{2}\"; then echo 1; else echo 0; fi", expectedString, logfileLocation, logfileName);
            string containsString = "";
            try
            {
                containsString = SSH.OpenWriteCloseSSH(formatString, hostname, username, password);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return containsString;
        }


        public static void SetAllAssetsAvailableAndEnabled(SSHAndDatabaseDirectory sshInfo, string DECSLocation = "/usr/local/ihi-decs/bin/", bool useQuery = true)
        {
            var ssh = new SshClient(connectionInfo);
           // var ssh = new SshClient(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);

            string sshRacks = "Update racks set available=1, enabled=1";
            var sshInverters = "Update inverters set available=1, enabled=1";
            var sshZones = "Update zones set available=1, enabled=1";

            if (useQuery)
            {
                QueryInterface.CommandThroughQuery(sshRacks, ssh, sshInfo.SSHIPAddress, DECSLocation);
                QueryInterface.CommandThroughQuery(sshInverters, ssh, sshInfo.SSHIPAddress, DECSLocation);
                QueryInterface.CommandThroughQuery(sshZones, ssh, sshInfo.SSHIPAddress, DECSLocation);
                return;
            }

            ssh.Connect();
            string databaseUpdate = string.Format("sqlite3 {0}decs_dc.db \"{1}\"", sshInfo.SSHDatabaseDirectory, sshRacks);
            ssh.RunCommand(databaseUpdate);
            
            databaseUpdate = string.Format("sqlite3 {0}decs_dc.db \"{1}\"", sshInfo.SSHDatabaseDirectory, sshInverters);
            ssh.RunCommand(databaseUpdate);
            
            databaseUpdate = string.Format("sqlite3 {0}decs_dc.db \"{1}\"", sshInfo.SSHDatabaseDirectory, sshZones);
            ssh.RunCommand(databaseUpdate);
            ssh.Disconnect();
        }

        public static void SetAllAssetsAvailableAndEnabled(string ipAddress, string username, string password, string databaseLocation)
        {
            SetAllAssetsAvailableAndEnabled(new SSHAndDatabaseDirectory(ipAddress, username, password, databaseLocation));
        }

        public static bool CheckForAlarmOrEvent(string search, SSHAndDatabaseDirectory sshInfo)
        {
            var ssh = new SshClient(connectionInfo);
           // var ssh = ConnectSSH(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
            string SSHCommand = string.Format("sqlite3 {0}decs_events.db \"SELECT additional_info FROM events\" | grep \"{1}\"", sshInfo.SSHDatabaseDirectory, search);
            var sendCommand = ssh.RunCommand(SSHCommand);
            ssh.Dispose();

            var foundEventAlarm = sendCommand.Result == search + "\n";
            return foundEventAlarm;
        }

        public static double MemoryFreePercent(SSHInfo sshInfo)
        {
            var ssh = new SshClient(connectionInfo);
           // var ssh = ConnectSSH(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword);
            string sshCommand = @"free | awk 'FNR == 3 {print $4/($3+$4)*100}'";
            var response = ssh.RunCommand(sshCommand).Result;
            double memoryFree;
            var parseSuccess = double.TryParse(response, out memoryFree);

            if (parseSuccess)
            {
                return memoryFree;
            }
            throw new Exception("Unsuccessful parse of " + response);
        }

        public static void KillDecsAndLaunchTest(string ipaddress, string username, string password, string decsLocation)
        {
            SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(ipaddress, username, password,  decsLocation);

            KillDecs(sshInfo);
            StartDecs(DECSLaunchType.Test, sshInfo, 10000);
        }

        public static void KillDecsAndLaunchTestWithoutTSwitch(string ipaddress, string username, string password, string decsLocation)
        {
            SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(ipaddress, username, password, decsLocation);

            KillDecs(sshInfo);
            StartDecs(DECSLaunchType.TestWithoutTSwitch, sshInfo, 10000);
        }
    }

    public class SSHInfo
    {
        public string SSHIPAddress { get; set; }
        public string SSHUsername { get; set; }
        public string SSHPassword { get; set; }
        

        public SSHInfo(string ipaddress, string username, string password )
        {
            this.SSHIPAddress = ipaddress;
            this.SSHUsername = username;
            this.SSHPassword = password;
           
        }

        public override string ToString()
        {
            return string.Format("IP Address: {0} User Name: {1} Password: {2}", SSHIPAddress, SSHUsername, SSHPassword);
        }
    }

    /// <summary>
    /// Class that houses relevant ssh information for database changing
    /// </summary>
    public class SSHAndDatabaseDirectory : SSHInfo
    {
        public string SSHDatabaseDirectory { get; set; }

        public SSHAndDatabaseDirectory(string ipaddress, string username, string password, string databaseDirectory)
            : base(ipaddress, username, password)
        {
            this.SSHIPAddress = ipaddress;
            this.SSHUsername = username;
            this.SSHPassword = password;
            this.SSHDatabaseDirectory = databaseDirectory;
           
        }

        public SSHAndDatabaseDirectory(SSHInfo sshInfo, string databaseDirectory)
            : base(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword)
        {
            this.SSHIPAddress = sshInfo.SSHIPAddress;
            this.SSHUsername = sshInfo.SSHUsername;
            this.SSHPassword = sshInfo.SSHPassword;
            this.SSHDatabaseDirectory = databaseDirectory;
            
        }
    }

    public class SSHAndStartDECSDirectory : SSHInfo
    {
        public string SSHStartDECSDirectory { get; set; }

        public SSHAndStartDECSDirectory(string ipaddress, string username, string password, string startDECSDirectory)
            : base(ipaddress, username, password )
        {
            this.SSHIPAddress = ipaddress;
            this.SSHUsername = username;
            this.SSHPassword = password;
            this.SSHStartDECSDirectory = startDECSDirectory;
      
        }

        public SSHAndStartDECSDirectory(SSHInfo sshInfo, string startDECSDirectory)
            : base(sshInfo.SSHIPAddress, sshInfo.SSHUsername, sshInfo.SSHPassword)
        {
            this.SSHIPAddress = sshInfo.SSHIPAddress;
            this.SSHUsername = sshInfo.SSHUsername;
            this.SSHPassword = sshInfo.SSHPassword;
            this.SSHStartDECSDirectory = startDECSDirectory;
         
        }
    }
}
