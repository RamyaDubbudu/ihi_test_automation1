﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace ihi_testlib_espilot
{
    public static class PageCharts
    {
        public static void OpenPopupChart(ihi_testlib_selenium.Selenium driver, bool switchBackToMain = false)
        {
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsCharts.ChartPopupLauncher));
            GeneralDECS.SwitchToTab(driver);
        }
    }
}
