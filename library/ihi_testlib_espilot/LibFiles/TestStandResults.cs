﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public class TestStandResults
    {
        public bool result { get; private set; }
        public string reportText { get; private set; }
        public bool errorOccured { get; set; }
        public string errorMsg { get; set; }
        private int errorCode { get; set; }

        public TestStandResults()
        {
            result = true;
            reportText = String.Empty;
            errorOccured = false;
            errorMsg = String.Empty;
            errorCode = 0;
        }

        public void UpdateTestStandResults(out bool result, out string reportText, out bool errorOccured,
            out string errorMsg, out int errorCode)
        {
            result = this.result;
            reportText = this.reportText;
            errorOccured = this.errorOccured;
            errorMsg = this.errorMsg;
            errorCode = this.errorCode;
        }

        public void AppendReportText(string appendedReport)
        {
            reportText += appendedReport + "\n";
        }

        public void AppendErorMsg(string appendedError)
        {
            reportText += appendedError + "\n";
        }

        public void TestFailed()
        {
            result = false;
        }
    }
}
