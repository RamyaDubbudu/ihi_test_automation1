﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Renci.SshNet;
using System.Reflection;
using System.Text.RegularExpressions;


namespace ihi_testlib_espilot
{
    public static class DashboardPowerGraphs
    {
        /// <summary>
        /// Checks the three bar graph's css style to make sure the justification is not negative which indicates it is outside the graph
        /// </summary>
        /// <param name="driver">Firefox driver</param>
        /// <param name="graphSelection">enum of which of the graphs to use</param>
        /// <param name="style">out style with is the string of the css style</param>
        /// <returns></returns>
        public static bool CheckValidStyle(ihi_testlib_selenium.Selenium driver, DashboardGraphs graphSelection, out string style)
        {
            style = "";
            try
            {
                switch (graphSelection)
                {
                    case DashboardGraphs.EnergyAvailable :
                        style = driver.GetAttribute(Xpath: string.Format(GlobalElementsPowerEnergyChart.EnergyAvailableBarGraph), attributeName:"style");
                        break;
                    case DashboardGraphs.pActual:
                        style = driver.GetAttribute(Xpath: string.Format(GlobalElementsPowerEnergyChart.PMeterBarGraph), attributeName: "style");
                        break;
                    case DashboardGraphs.qActual:
                        style = driver.GetAttribute(Xpath: string.Format(GlobalElementsPowerEnergyChart.QMeterBarGraph), attributeName: "style");
                        break;
                }
                if (CheckStyleContainsNegative(style))
                    return false;

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the justification of the bar graph is negative, returns true if it is
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        private static bool CheckStyleContainsNegative(string style)
        {
            try
            {
                var left = style.IndexOf("left");
                var right = style.IndexOf("right");

                var index = left != -1 ? left : right;

                if (index != -1)
                {
                    var sub = style.Substring(index);
                    //checks for number including negatives
                    var offset = double.Parse(Regex.Replace(sub, @"[^.-?\d]", ""));
                    return ((offset < 0)||(offset > 100)); 
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public enum DashboardGraphs
        {
            EnergyAvailable,
            pActual,
            qActual
        };
    }
}
