﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public static class ModeSelection
    {
        public static void SetMode(string mode, string ipaddress)
        {
            var modeEnum = GetModeEnum(mode);
            SetMode(modeEnum, ipaddress);
        }

        public static void SetMode(Mode mode, string ipAddress)
        {
            IHI_PPD_ZMQ.Set_Mode((int)mode, ipAddress);
        }

        public static Mode GetModeEnum(string modeString)
        {
            modeString = modeString.Replace(" ", "_");
            Mode modeEnum = (Mode)System.Enum.Parse(typeof(Mode), modeString);
            return modeEnum;
        }

        public static string GetModeString(Mode modeEnum)
        {
            string modeString = modeEnum.ToString();
            modeString = modeString.Replace("_", " ");
            return modeString;
        }
    }

    /// <summary>
    /// enum to list the number value associated with a mode
    /// </summary>
    public enum Mode
    {
        Unknown, //0
        Shutdown, //1
        Waiting, //2
        Startup,//3
        Ready,//4
        Standby,//5
        Profile,//6
        Peak_Shaving,//7
        Freq_Support,//8
        Voltage_Support,//9
        Load_leveling,//10
        Manual_Mode,//11
        SOC_Maintenance,//12
        Unused_1,//13
        ISO_Dispatch,//14
        Peak_Shift,//15
        Unused_2,//16
        Maintenance,//17
        Fault//18
    }
}
