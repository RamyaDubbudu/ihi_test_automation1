﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ihi_testlib_espilot
{
    static public class EditUserWindow
    {
        public static void EditUser(string FullName, string Email, string PermissionLevel, string Password, string ConfirmPassword, ihi_testlib_selenium.Selenium driver)
        {
            System.Threading.Thread.Sleep(1000);
            driver.AddTextToTextbox(textToAdd: FullName, XPath: string.Format(GlobalElementsEditUserPopup.FullNameField));
            driver.AddTextToTextbox(textToAdd: Email, XPath: string.Format(GlobalElementsEditUserPopup.EmailField));
            new SelectElement(driver.browserDriver.FindElement(By.XPath(GlobalElementsEditUserPopup.PermissionsDropDown))).SelectByText(
            PermissionLevel);
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEditUserPopup.ChangePasswordSelect));
            driver.AddTextToTextbox(textToAdd: Password, XPath: string.Format(GlobalElementsEditUserPopup.PasswordField));
            driver.AddTextToTextbox(textToAdd: ConfirmPassword, XPath: string.Format(GlobalElementsEditUserPopup.ConfirmPasswordField));
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEditUserPopup.YesButton));
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.FullNameField).Clear();
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.FullNameField).SendKeys(FullName);
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.EmailField).Clear();
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.EmailField).SendKeys(Email);
            //new SelectElement(driver.browserDriver.FindElement(GlobalElementsEditUserPopup.PermissionsDropDown)).SelectByText(PermissionLevel);
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.ChangePasswordSelect).Click();
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.PasswordField).Clear();
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.PasswordField).SendKeys(Password);
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.ConfirmPasswordField).Clear();
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.ConfirmPasswordField).SendKeys(ConfirmPassword);
            
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.YesButton).Click();
        }

        public static void EditUser(string FullName, string Email, string PermissionLevel, ihi_testlib_selenium.Selenium driver)
        {
            System.Threading.Thread.Sleep(1000);
            driver.AddTextToTextbox(textToAdd: FullName, XPath: string.Format(GlobalElementsEditUserPopup.FullNameField));
            driver.AddTextToTextbox(textToAdd: Email, XPath: string.Format(GlobalElementsEditUserPopup.EmailField));
            new SelectElement(driver.browserDriver.FindElement(By.XPath(GlobalElementsEditUserPopup.PermissionsDropDown))).SelectByText(
            PermissionLevel);
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEditUserPopup.YesButton));
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.FullNameField).Clear();
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.FullNameField).SendKeys(FullName);
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.EmailField).Clear();
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.EmailField).SendKeys(Email);
            //new SelectElement(driver.browserDriver.FindElement(GlobalElementsEditUserPopup.PermissionsDropDown)).SelectByText(PermissionLevel);
            
            //driver.browserDriver.FindElement(GlobalElementsEditUserPopup.YesButton).Click();
        }

    }
}
