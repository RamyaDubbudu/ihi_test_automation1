﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Text.RegularExpressions;
using System.Reflection;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Class for zones popup page
    /// </summary>
    public static class ZonesPopupPage
    {
        /// <summary>
        /// Returns lists with all zones labels
        /// </summary>
        /// <param name="SSHIPAddress"></param>
        /// <param name="SSHUsername"></param>
        /// <param name="SSHPassword"></param>
        /// <param name="DECSDirectory"></param>
        /// <returns></returns>
        public static List<string> GetAllZoneLabels(string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/")
        {
            string zonesstring = SSH.ReadAllFromDatabaseOpenClose("zones", "label", SSHIPAddress, SSHUsername, SSHPassword);

            List<string> AllLabels = zonesstring.Split('\n').ToList<string>();
            AllLabels = AllLabels.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            return AllLabels;
        }



        public static void Video()
        {

        }

    }

    public class ZonePopupInfo
    {
        public string label { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string state_actual { get; set; }
        public double soc { get; set; }
        public double current_runtime { get; set; }
        public double total_runtime { get; set; }
        public double dc_voltage { get; set; }
        public int p_demand { get; set; }
        public int q_demand { get; set; }
        public int p_actual { get; set; }
        public int q_actual { get; set; }
        public int p_charge_available { get; set; }
        public int p_discharge_avaiable { get; set; }
        public int q_leading_available { get; set; }
        public int q_lagging_avaiable { get; set; }
        public string ups_status { get; set; }
        public double ups_soc { get; set; }
        public double ups_remaining { get; set; }
        public double ups_load_pct { get; set; }
        public double ambient_temp { get; set; }

        /// <summary>
        /// Constructor for finding information on popup window
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public ZonePopupInfo(ihi_testlib_selenium.Selenium driver)
        {
            try
            {

                label = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.DetailedInformation));
                available = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.Available));
                enabled = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.Enabled));
                state_actual = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.Status));
                soc = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.SOC)));
                current_runtime = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.CurrentRuntime)));
                total_runtime = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.TotalRuntime)));
                dc_voltage = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.DCVoltage)));
                p_demand = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.Demandp)));
                q_demand = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.Demandq)));
                p_actual = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.Actualp)));
                q_actual = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.Actualq)));
                p_charge_available = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.ChargeAvailablep)));
                p_discharge_avaiable = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.DischangeAvailablep)));
                q_leading_available = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.LeadingAvailableq)));
                q_lagging_avaiable = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.LaggingAvailableq)));
                ups_status = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.UPSStatus));
                ups_soc = double.Parse(Regex.Replace(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.UPSSOC)), "[^0-9.,]", ""));
                ups_remaining = double.Parse(Regex.Replace(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.TimeRemaining)), "[^0-9.,]", ""));
                ups_load_pct = double.Parse(Regex.Replace(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.PercentLoad)), "[^0-9.,]", ""));
                ambient_temp = double.Parse(Regex.Replace(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsZonePopup.AmbientTemp)), "[^0-9.,]", ""));


                //label = driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.DetailedInformation).Text;
                //available = driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.Available).Text;
                //enabled = driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.Enabled).Text;
                //state_actual = driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.Status).Text;
                //soc = double.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.SOC).Text);
                //current_runtime = double.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.CurrentRuntime).Text);
                //total_runtime = double.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.TotalRuntime).Text);
                //dc_voltage = double.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.DCVoltage).Text);
                //p_demand = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.Demandp).Text);
                //q_demand = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.Demandq).Text);
                //p_actual = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.Actualp).Text);
                //q_actual = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.Actualq).Text);
                //p_charge_available = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.ChargeAvailablep).Text);
                //p_discharge_avaiable = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.DischangeAvailablep).Text);
                //q_leading_available = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.LeadingAvailableq).Text);
                //q_lagging_avaiable = int.Parse(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.LaggingAvailableq).Text);
                //ups_status = driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.UPSStatus).Text;
                //ups_soc = double.Parse(Regex.Replace(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.UPSSOC).Text, "[^0-9.,]", ""));
                //ups_remaining = double.Parse(Regex.Replace(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.TimeRemaining).Text, "[^0-9.,]", ""));
                //ups_load_pct = double.Parse(Regex.Replace(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.PercentLoad).Text, "[^0-9.,]", ""));
                //ambient_temp = double.Parse(Regex.Replace(driver.browserdriver.browserDriver.FindElement(GlobalElementsZonePopup.AmbientTemp).Text, "[^0-9.,]", ""));
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(ZonePopupInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }
    }
}
