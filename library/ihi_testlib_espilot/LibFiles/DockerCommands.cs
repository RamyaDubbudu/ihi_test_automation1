﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
   public static class DockerCommands
    {
        public static class SSHCommands
        {
            public static string StopDecs { get { return "sudo docker exec decs-dc bash -c \"./usr/local/ihi-decs/bin/stop_decs\" && sudo docker exec decs-zc1 bash -c \"./usr/local/ihi-decs/bin/stop_zone\""; } }
            public static string StartDecs { get { return "sudo docker exec decs-zc1 bash -c \"./usr/local/ihi-decs/bin/do_start_zone.sh 1 \"1\" -d\" && sudo docker exec decs-dc bash -c \"./usr/local/ihi-decs/bin/start_decs\""; } }
            public static string SqliteQuery { get { return "sudo docker exec decs-zc1 bash -c \"{0}\""; } }
            public static string Decs_isombLogFile { get { return "/usr/local/ihi/pilot/dc/runtime/decs_isomb.log"; } }
            public static string Decs_BatterydbLogFile { get { return "/usr/local/ihi/pilot/zc1/runtime/decs_batteryd.log"; } }
            public static string DockerPrefix { get { return "sudo docker exec decs-dc bash -c"; } }
            public static string DockerSQLiteVolumns { get { return "echo \"{0}\" | sqlite3 /usr/local/ihi/pilot/{1}/data/{2}"; } }
            public static string Killisomb { get { return "sudo pkill decs_isomb"; } }
            public static string resetisomb { get { return "sudo docker exec -id decs-dc bash -c \"cd /usr/local/ihi-decs/bin&&echo decs |./decs_isomb -D {0} -l 0 \""; } }
            public static string Kill_IO { get { return "sudo pkill decs_iod"; } }
            public static string resetIOIPaddress { get { return "sudo docker exec -id decs-zc1 bash -c \"cd /usr/local/ihi-decs/bin&&echo decs | decs_iod -j 1 -d -i {0} -z 1 -c 9 \""; } }

        }

    }
}
