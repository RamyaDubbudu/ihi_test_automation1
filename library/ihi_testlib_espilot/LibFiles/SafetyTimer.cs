﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox; //Selenium

namespace ihi_testlib_espilot
{
    public class SafetyTimer
    {
        //Creates a timer that checks p and q against database
        public static Timer PAndQAcutalMaketimer(int timerlength, int PValueLimitHigh, int QValueLimitHigh, int PValueLimitLow, int QValueLimitLow,
            string SSHIPAddress, string SSHUser, string SSHPassword, string DECSDatabaseDirectory = "/var/run/ihi-decs/" )
        {
            Timer timer = new Timer();
            timer.Elapsed += delegate { CheckPANDQActal(timer, PValueLimitHigh, QValueLimitHigh, PValueLimitLow, QValueLimitLow,
                SSHIPAddress, SSHUser, SSHPassword, DECSDatabaseDirectory); };
            timer.Interval = timerlength;
            timer.Start();
            return timer;
        }

        //used by timer to check p and q values and checks them against database. shutsdown if certain values reached
        static void CheckPANDQActal(Timer timer, int PValueLimitHigh, int PValueLimitLow, int QValueLimitHigh, int QValueLimitLow,
            string SSHIPAddress, string SSHUser, string SSHPassword, string DECSDatabaseDirectory)
        {
            int PValueActal = Int32.Parse(SSH.ReadFromDatabaseOpenClose("site", "p_actual", 1, SSHIPAddress, SSHUser, SSHPassword, DECSDatabaseDirectory));
            int QValueActal = Int32.Parse(SSH.ReadFromDatabaseOpenClose("site", "q_actual", 1, SSHIPAddress, SSHUser, SSHPassword, DECSDatabaseDirectory));
            bool PValueHighLimitReached = PValueActal > PValueLimitHigh;
            bool QValueHighLimitReached = QValueActal > QValueLimitHigh;
            bool PValueLowLimitReached = PValueActal < PValueLimitLow;
            bool QValueLowLimitReached = QValueActal < QValueLimitLow;


            if (PValueHighLimitReached || QValueHighLimitReached || PValueLowLimitReached || QValueLowLimitReached)
            {
                timer.Stop();
                Console.WriteLine("Safety Limit Reached!");
                if (PValueHighLimitReached)
                {
                    Console.WriteLine(String.Format("p Value exceeded high limit.  p Limit = {0} p displayed = {1}", PValueLimitHigh, PValueActal));
                }
                else if (QValueHighLimitReached)
                {
                    Console.WriteLine(String.Format("q Value exceeded high limit.  q Limit = {0} q displayed = {1}", QValueLimitHigh, QValueActal));
                }
                else if (PValueLowLimitReached)
                {
                    Console.WriteLine(String.Format("p Value exceeded low limit.  p Limit = {0} p displayed = {1}", PValueLimitLow, PValueActal));
                }
                else if (QValueLowLimitReached)
                {
                    Console.WriteLine(String.Format("q Value exceeded low limit.  q Limit = {0} q displayed = {1}", QValueLimitLow, QValueActal));
                }
                IHI_PPD_ZMQ.Set_Mode((int)Mode.Shutdown, SSHIPAddress);
                
            }
        }

    }
}
