﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox; //Selenium
using System.Reflection;

namespace ihi_testlib_espilot
{

    /// <summary>
    /// Methods that are made use of on the Cooling Popup page
    /// </summary>
    public static class CoolingPopupPage
    {

        /// <summary>
        /// Get a list string with the name of the cooling assets.
        /// </summary>
        /// <param name="SSHIPAddress">Linux IP address</param>
        /// <param name="SSHUsername">Linux User name</param>
        /// <param name="SSHPassword">Linux password</param>
        /// <param name="DECSDirectory">Directory that houses the database</param>
        /// <returns>List of cooling assets</returns>
        public static List<string> GetAllCoolingLabels(string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/var/run/ihi-decs/")
        {
            string zonesstring = SSH.ReadAllFromDatabaseOpenClose("cooling", "label", SSHIPAddress, SSHUsername, SSHPassword);

            List<string> AllLabels = zonesstring.Split('\n').ToList<string>();
            AllLabels = AllLabels.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            return AllLabels;
        }
    }

    /// <summary>
    /// Contains info and methods used in Cooling pop up window
    /// </summary>
    public class CoolingPopupInfo
    {
        public string label { get; set; }
        public string description { get; set; }
        public int zones_id { get; set; }
        public string available { get; set; }
        public string enabled { get; set; }
        public string online { get; set; }
        private double _setpoint;
        public double setpoint { get { return _setpoint; } set { CheckForNA(value.ToString()); } }
        public double return_air_temp_f { get; set; }
        public double supply_air_temp_f { get; set; }
        private double _percent_load;
        public double percent_load { get { return _percent_load; } set { CheckForNA(value.ToString()); } }
        private double _inlet_temp_f;
        public double inlet_temp_f { get { return _inlet_temp_f; } set { CheckForNA(value.ToString()); } }
        private double _outlet_temp_f;
        public double outlet_temp_f { get { return _outlet_temp_f; } set { CheckForNA(value.ToString()); } }
        private double _flow_rate_gpm;
        public double flow_rate_gpm { get { return _flow_rate_gpm; } set { CheckForNA(value.ToString()); } }

        /// <summary>
        /// Constructor for getting information from popup window
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public CoolingPopupInfo(ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                label = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.DetailedInformation));
                description = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.Description));
                zones_id = int.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.ZonesID)));
                available = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.Available));
                enabled = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.Enabled));
                online = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.Online));
                setpoint = CheckForNA(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.SetPoint)));
                return_air_temp_f = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.ReturnAirTemp)));
                supply_air_temp_f = double.Parse(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.SupplyAirTemp)));
                percent_load = CheckForNA(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.PercentLoad)));
                inlet_temp_f = CheckForNA(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.InletTemp)));
                outlet_temp_f = CheckForNA(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.OutletTemp)));
                flow_rate_gpm = CheckForNA(driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsCoolingPopup.FlowRate)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Cycles through all properties and checks if they have the same value as another of same class
        /// </summary>
        /// <param name="assetInfo"></param>
        /// <returns></returns>
        public string AssertEquals(CoolingPopupInfo assetInfo)
        {
            string results = "";

            Type type = this.GetType();
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                string thisValue = property.GetValue(this).ToString();
                string otherValue = property.GetValue(assetInfo).ToString();

                if (thisValue != otherValue)
                {
                    results += string.Format("Difference in {0}: {1} vs {2}\n", property.Name.ToString(), thisValue, otherValue);
                }
            }
            return results;
        }

        /// <summary>
        /// Checks if element has "N/A" in response.  Returns -1 if it does or not a number if other
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private double CheckForNA(string text)
        {
            double result;
            if (double.TryParse(text, out result))
                return (result < 0) ? -1 : result;
            else if (text == "N/A")
                return -1;

            return double.NaN;
        }

        private bool CheckIfNAOrEqual(double databaseValue, double uiValue)
        {
            if (databaseValue == uiValue)
                return true;

            if ((databaseValue < 0) && (uiValue == -1))
            {
                return true;
            }
            return false;
        }
    }
}

