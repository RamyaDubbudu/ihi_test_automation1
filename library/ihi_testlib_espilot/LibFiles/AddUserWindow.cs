﻿/*==============================================================================
Name        : AddUserWindow.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Methods used to control elements on the Add User popup.
==============================================================================*/

using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;


namespace ihi_testlib_espilot
{
    [TestFixture]
    /// <summary>
    ///     Methods and properties that work with the Add user/edit user window
    /// </summary>
    public static class AddUserWindow
    {
        /// <summary>
        ///     Enter user info and hit Yes (should this be "Yes"/"No" or OK/cancel)
        /// </summary>
        /// <param name="UserName">User name to input</param>
        /// <param name="FullName">Full name to input</param>
        /// <param name="Email">E-mail to input</param>
        /// <param name="PermissionLevel">Permission level (in words) to input</param>
        /// <param name="Password">Password to input</param>
        /// <param name="ConfirmPassword">Confirm Password to input</param>
        /// <param name="driver"></param>
        public static void AddUser(string UserName, string FullName, string Email, string PermissionLevel,
            string Password, string ConfirmPassword, ihi_testlib_selenium.Selenium driver)
        {
            driver.AddTextToTextbox(textToAdd: UserName, XPath: string.Format(GlobalElementsUserPopup.UserNameField));
            driver.AddTextToTextbox(textToAdd: FullName, XPath: string.Format(GlobalElementsUserPopup.FullNameField));
            driver.AddTextToTextbox(textToAdd: Email, XPath: string.Format(GlobalElementsUserPopup.EmailField));
           // driver.SelectElement(textToSelect: PermissionLevel, xpath: string.Format(GlobalElementsUserPopup.PermissionsDropDown));
            new SelectElement(driver.browserDriver.FindElement(By.XPath(GlobalElementsUserPopup.PermissionsDropDown))).SelectByText(
            PermissionLevel);
            driver.AddTextToTextbox(textToAdd: Password, XPath: string.Format(GlobalElementsUserPopup.PasswordField));
            driver.AddTextToTextbox(textToAdd: ConfirmPassword, XPath: string.Format(GlobalElementsUserPopup.ConfirmPasswordField));
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsUserPopup.YesButton));
        }

        /// <summary>
        ///     Method will search text for known error messages and create and populate class with errors found
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>Object with user submission errors true/false values</returns>
        public static UserSubmissionErrors DiscoverUserErrors(ihi_testlib_selenium.Selenium driver)
        {
            var userErrors = new UserSubmissionErrors();
            if(driver.IsElementVisible(xPath: string.Format(GlobalElementsUserPopup.ErrorMessage)))
            {
                //if (GeneralDECS.IsElementPresent(GlobalElementsUserPopup.ErrorMessage, driver))
                //{
                var errorMessage = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsUserPopup.ErrorMessage));
                //var errorMessage = driver.browserDriver.FindElement(GlobalElementsUserPopup.ErrorMessage).Text;

                //all strings must be provided.  If new errors are added, put them here
                //along with editing coresponding error type in class UserSubmissionErrors
                if (errorMessage.Contains("A 'User Name' must be entered."))
                    userErrors.userNameMissingError = true;

                if (errorMessage.Contains("A 'Full Name' must be entered."))
                    userErrors.fullNameMissingError = true;

                if (errorMessage.Contains("An 'e-mail address' must be entered."))
                    userErrors.emailMissingError = true;

                if (errorMessage.Contains("A valid 'e-mail address' must be entered."))
                    userErrors.emailAdderssInvalidError = true;

                if (errorMessage.Contains("A 'Permissions Level' must be entered."))
                    userErrors.permissionLevelMissingError = true;

                if (errorMessage.Contains("A 'Password' must be entered."))
                    userErrors.passwordMissingError = true;

                if (errorMessage.Contains("A 'Confirmation Password' must be entered."))
                    userErrors.confirmPasswordMissingError = true;

                if (errorMessage.Contains("The 'Password' and 'Confirmation Password' do not match."))
                    userErrors.passwordMismatchError = true;

                if (errorMessage.Contains("A 'Password' must be at least 6 characters long."))
                    userErrors.passwordUnderSixLettersError = true;

                if (errorMessage.Contains("A 'User Name' should only contain numbers or digits."))
                    userErrors.userNameNumberOrDigits = true;

                if (errorMessage.Contains("A 'User' with that 'User Name' already exists."))
                    userErrors.userNameAlreadyExists = true;
            }

            return userErrors;
        }
    }

    /// <summary>
    ///     Class with errors made for user submission.  Will grow when more found.
    /// </summary>
    public class UserSubmissionErrors
    {
        /// <summary>
        ///     Contructor that defaults all values to false
        /// </summary>
        public UserSubmissionErrors()
        {
            userNameMissingError = false;
            fullNameMissingError = false;
            emailAdderssInvalidError = false;
            permissionLevelMissingError = false;
            passwordMissingError = false;
            confirmPasswordMissingError = false;
            passwordMismatchError = false;
            passwordUnderSixLettersError = false;
            userNameNumberOrDigits = false;
            userNameAlreadyExists = false;
        }

        public bool userNameMissingError { get; set; }
        public bool fullNameMissingError { get; set; }
        public bool emailMissingError { get; set; }
        public bool emailAdderssInvalidError { get; set; }
        public bool permissionLevelMissingError { get; set; }
        public bool passwordMissingError { get; set; }
        public bool confirmPasswordMissingError { get; set; }
        public bool passwordMismatchError { get; set; }
        public bool passwordUnderSixLettersError { get; set; }
        public bool userNameNumberOrDigits { get; set; }
        public bool userNameAlreadyExists { get; set; }

        /// <summary>
        ///     puts all the errors into a user readable string.  This will expand automatically when more errors are added
        /// </summary>
        /// <returns></returns>
        public string ListValues()
        {
            var results = "";
            var type = GetType();
            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                results += "Error: " + property.Name + ", Value: " + property.GetValue(this, null) + "\n";
            }

            return results;
        }

        /// <summary>
        ///     Generates report based on errors, does not need to be changed if more errors are added
        /// </summary>
        /// <returns></returns>
        private string GenerateKnownErrorReport()
        {
            var errorReport = "";
            var type = GetType();
            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                var propertyName = property.GetValue(this, null).ToString().Replace("Boolean", string.Empty);
                if (propertyName == "True")
                {
                    errorReport += property.Name;
                }
            }
            return errorReport;
        }

        /// <summary>
        ///     takes in the expected errors from the run file and compares them to what were actually produced.  Will report back
        ///     with list of errors not expected, and/or if expected error not shown on screen.
        /// </summary>
        /// <param name="expectedErrors">Expected known errors separated by comma</param>
        /// <returns></returns>
        public string ValidateErrors(string expectedErrors)
        {
            var splitExpectedErrors = expectedErrors.Split(',');
            var errorReport = GenerateKnownErrorReport();
            var results = "";
            foreach (var error in splitExpectedErrors)
            {
                if (errorReport.Contains(error))
                {
                    errorReport = errorReport.Replace(error, string.Empty);
                }
                else
                {
                    results += "Error expected by run file not found in UI: " + error + "\n";
                }
            }

            if (errorReport != "")
            {
                results += "Error reported by UI not found in what was expected by Run File: " + errorReport + "\n";
            }

            //Results should be "" (empty) if expected results match UI results
            return results;
        }
    }
}