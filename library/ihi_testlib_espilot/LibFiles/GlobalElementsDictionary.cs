﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace ihi_testlib_espilot
{
    public class GlobalElements
    {
        public static readonly Dictionary<string, string> DashboardDictionary = new Dictionary<string, string>
        {
            { "site+name", GlobalElementsDashboard.SiteName },
            { "site+kwh_throughput", GlobalElementsDashboard.kWhThroughput },
            { "site+lmp", GlobalElementsDashboard.CurrentLMP },
            { "site+description", GlobalElementsDashboard.Description },
            { "site+mode_actual", GlobalElementsDashboard.CurrentMode },
            { "site+selected_mode", GlobalElementsDashboard.SelectedMode },
            { "site+current_runtime", GlobalElementsDashboard.RuntimeCurrent },
            { "site+total_runtime", GlobalElementsDashboard.RuntimeTotal },
            { "site+p_demand", GlobalElementsDashboard.pDemand },
            { "site+p_actual", GlobalElementsDashboard.pActual},
            { "site+p_max", GlobalElementsDashboard.pAvailable },
            { "site+q_demand", GlobalElementsDashboard.qDemand },
            { "site+q_actual", GlobalElementsDashboard.qActual},
            { "site+q_max", GlobalElementsDashboard.qAvailable },
            { "site+ref_freq", GlobalElementsDashboard.RefFrequency },
            { "site+ref_voltage", GlobalElementsDashboard.RefVoltage },
            { "site+ref_p", GlobalElementsDashboard.RefpkW },
            { "site+ref_q", GlobalElementsDashboard.RefqkVAR },
            { "site+ref_kva", GlobalElementsDashboard.RefskVA },
            { "site+zones_available", GlobalElementsDashboard.ZonesAvailable },
            { "site+zones_enabled", GlobalElementsDashboard.ZonesEnabled },
            { "site+zones_online", GlobalElementsDashboard.ZonesOnline },
            { "site+zones_total", GlobalElementsDashboard.ZonesTotal },
            { "site+racks_available", GlobalElementsDashboard.BatteryRacksAvailable },
            { "site+racks_enabled", GlobalElementsDashboard.BatteryRacksEnabled },
            { "site+racks_online", GlobalElementsDashboard.BatteryRacksOnline },
            { "site+racks_total", GlobalElementsDashboard.BatteryRacksTotal },
            { "site+inverters_available", GlobalElementsDashboard.InvertersAvailable },
            { "site+inverters_enabled", GlobalElementsDashboard.InvertersEnabled },
            { "site+inverters_online", GlobalElementsDashboard.InvertersOnline },
            { "site+inverters_total", GlobalElementsDashboard.InvertersTotal },
            { "site+cooling_available", GlobalElementsDashboard.CoolingAvailable },
            { "site+cooling_enabled", GlobalElementsDashboard.CoolingEnabled },
            { "site+cooling_online", GlobalElementsDashboard.CoolingOnline },
            { "site+cooling_total", GlobalElementsDashboard.CoolingTotal },
            { "site+soc", GlobalElementsDashboard.AverageSOC },
            /*{ "inverters+label", GlobalElementsDashboardAssetsTable.InvertersName},
            { "inverters+zones_id", GlobalElementsDashboardAssetsTable.InvertersZone},
            { "inverters+state_actual", GlobalElementsDashboardAssetsTable.InvertersStatus},
            { "inverters+enabled", GlobalElementsDashboardAssetsTable.InvertersEnabled},
            { "cooling+label", GlobalElementsDashboardAssetsTable.CoolingName},
            { "cooling+zones_id", GlobalElementsDashboardAssetsTable.CoolingZone},
            { "cooling+return_air_temp_f", GlobalElementsDashboardAssetsTable.CoolingReturnAirTemp},
            { "site+ups_status", GlobalElementsDashboardAssetsTable.UPSOnline},
            { "site+ups_remaining", GlobalElementsDashboardAssetsTable.UPSTimeRemaining},
            { "site+ups_load_pct", GlobalElementsDashboardAssetsTable.UPSPercentLoad},
            { "site+ups_soc", GlobalElementsDashboardAssetsTable.UPSSOC},
            { "site+ambient_temp", GlobalElementsDashboardAssetsTable.AmbientTemp},
            { "cooling+supply_air_temp_f", GlobalElementsDashboardAssetsTable.CoolingSupplyAirTemp},               */
            { "io_available", GlobalElementsDashboard.IODevicesAvailable},
            { "io_total", GlobalElementsDashboard.IODevicesTotal},
            { "meters_available", GlobalElementsDashboard.MetersAvailable},
            { "meters_total", GlobalElementsDashboard.MetersTotal}
        };

        public static readonly Dictionary<string, string> ZonePopupDictionary = new Dictionary<string, string>
        {
            { "zones+zones_id", GlobalElementsZonePopup.ZoneID },
            { "zones+label", GlobalElementsZonePopup.DetailedInformation},
            { "zones+available", GlobalElementsZonePopup.Available },
            { "zones+enabled", GlobalElementsZonePopup.Enabled },
            { "zones+state_actual", GlobalElementsZonePopup.Status },
            { "zones+soc", GlobalElementsZonePopup.SOC },
            { "zones+total_runtime", GlobalElementsZonePopup.TotalRuntime },
            { "zones+current_runtime", GlobalElementsZonePopup.CurrentRuntime },
            { "zones+p_actual", GlobalElementsZonePopup.Actualp },
            { "zones+p_demand", GlobalElementsZonePopup.Demandp },
            { "zones+q_actual", GlobalElementsZonePopup.Actualq },
            { "zones+q_demand", GlobalElementsZonePopup.Demandq },
            { "zones+dc_voltage", GlobalElementsZonePopup.DCVoltage },
            { "zones+p_max", GlobalElementsZonePopup.ChargeAvailablep },
            { "zones+p_min", GlobalElementsZonePopup.DischangeAvailablep },
            { "zones+q_max", GlobalElementsZonePopup.LeadingAvailableq },
            { "zones+q_min", GlobalElementsZonePopup.LaggingAvailableq },
            { "zones_ups_status", GlobalElementsZonePopup.UPSStatus },
            { "zones+ups_soc", GlobalElementsZonePopup.UPSSOC},
            { "zones+ups_load_pct", GlobalElementsZonePopup.PercentLoad },
            { "zones+ups_remaining", GlobalElementsZonePopup.TimeRemaining },
            { "zones+ambient_temp", GlobalElementsZonePopup.AmbientTemp }
        };

        public static readonly Dictionary<string, string> RackPopupDictionary = new Dictionary<string, string>
        {
            { "racks+label", GlobalElementsRackPopup.DetailedInformation },
            { "racks+description", GlobalElementsRackPopup.Description },
            { "racks+zone_id", GlobalElementsRackPopup.ZoneID },
            { "racks+type", GlobalElementsRackPopup.Type },
            { "racks+available", GlobalElementsRackPopup.Available },
            { "racks+enabled", GlobalElementsRackPopup.Enabled },
            { "racks+rack_status", GlobalElementsRackPopup.Status },
            { "racks+soc", GlobalElementsRackPopup.SOC },
            { "racks+v_actual", GlobalElementsRackPopup.RackActualVoltage },
            { "racks+i_actual", GlobalElementsRackPopup.RackActualCurrent },
            { "racks+t_avg", GlobalElementsRackPopup.CellAverageTemp },
            { "racks+t_max", GlobalElementsRackPopup.CellMaximumTemp },
            { "racks+t_min", GlobalElementsRackPopup.CellMinimumTemp },
            { "racks+v_cellavg", GlobalElementsRackPopup.CellAverageVoltage },
            { "racks+v_cellmax", GlobalElementsRackPopup.CellMaximumVoltage },
            { "racks+v_celmin", GlobalElementsRackPopup.CellMinimumVoltage },
            { "racks+v_max", GlobalElementsRackPopup.RackMaximumVoltage },
            { "racks+v_min", GlobalElementsRackPopup.RackMinimumVoltage },
            { "racks+i_max", GlobalElementsRackPopup.RackMaximumCurrent },
            { "racks+i_min", GlobalElementsRackPopup.RackMinimumVoltage }
        };

        public static readonly Dictionary<string, string> InverterPopupDictionary = new Dictionary<string, string>
        {
            { "inverters+label", GlobalElementsInverterPopup.DetailedInformation },
            { "inverters+description", GlobalElementsInverterPopup.Description },
            { "inverters+zones_id", GlobalElementsInverterPopup.ZonesID },
            { "inverters+inverters_id", GlobalElementsInverterPopup.InverterID},
            { "inverters+available", GlobalElementsInverterPopup.Available },
            { "inverters+enabled", GlobalElementsInverterPopup.Enabled },
            { "inverters+state_actual", GlobalElementsInverterPopup.Status },
            { "inverters+p_demand", GlobalElementsInverterPopup.kWDemand },
            { "inverters+q_demand", GlobalElementsInverterPopup.kVARDemand },
            { "inverters+p_actual", GlobalElementsInverterPopup.kWActual },
            { "inverters+q_actual", GlobalElementsInverterPopup.kVARActual },
            { "inverters+v1_actual", GlobalElementsInverterPopup.Actualv1 },
            { "inverters+v2_actual", GlobalElementsInverterPopup.Actualv2 },
            { "inverters+v3_actual", GlobalElementsInverterPopup.Actualv3 },
            { "inverters+i1_actual", GlobalElementsInverterPopup.Actuali1 },
            { "inverters+i2_actual", GlobalElementsInverterPopup.Actuali2 },
            { "inverters+i3_actual", GlobalElementsInverterPopup.Actuali3 },
            { "inverters+v_dcbus", GlobalElementsInverterPopup.VDCBUS },
            { "inverters+i_dcbus", GlobalElementsInverterPopup.IDCBUS}
        };

        public static readonly Dictionary<string, string> CoolingPopupDictionary = new Dictionary<string, string>
        {
            { "cooling+label", GlobalElementsCoolingPopup.DetailedInformation },
            { "cooling+description", GlobalElementsCoolingPopup.Description },
            { "cooling+zones_id", GlobalElementsCoolingPopup.ZonesID },
            { "cooling+cooling_id", GlobalElementsCoolingPopup.CoolingID },
            { "cooling+available", GlobalElementsCoolingPopup.Available },
            { "cooling+enabled", GlobalElementsCoolingPopup.Enabled },
            { "cooling+online", GlobalElementsCoolingPopup.Online },
            { "cooling+setpoint", GlobalElementsCoolingPopup.SetPoint },
            { "cooling+return_air_temp_f", GlobalElementsCoolingPopup.ReturnAirTemp },
            { "cooling+supply_air_temp_f", GlobalElementsCoolingPopup.SupplyAirTemp },
            { "cooling+percent_load", GlobalElementsCoolingPopup.PercentLoad },
            { "cooling+inlet_temp_f", GlobalElementsCoolingPopup.InletTemp },
            { "cooling+outlet_temp_f", GlobalElementsCoolingPopup.OutletTemp },
            { "cooling+flow_rate_gpm", GlobalElementsCoolingPopup.FlowRate }
        };

        public static readonly Dictionary<string, string> PowerChartDictionary = new Dictionary<string, string>
        {
            { "site+kwh_max", GlobalElementsPowerEnergyChart.EnergyAvailableTick5 },
            { "site+p_min", GlobalElementsPowerEnergyChart.PTick1 },
            { "site+p_max", GlobalElementsPowerEnergyChart.PTick5 },
            { "site+q_min", GlobalElementsPowerEnergyChart.QTick1 },
            { "site+q_max", GlobalElementsPowerEnergyChart.QTick5 },
            { "site+p_actual", GlobalElementsPowerEnergyChart.PMeterValue },
            { "site+q_actual", GlobalElementsPowerEnergyChart.QMeterValue },
            { "site+kwh_available", GlobalElementsPowerEnergyChart.EnergyAvailableMeterValue }
        };
    }
}
