﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public class ZMQFormatingRequest
    {

        /* Message "Hash Table" Definition */
        public static string IHI_PROTOCOL_ZMQ__HASH_TABLE = "00000000";

        /* "Queue" Definitions */
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DOMAIN_CONTROLLER_REQUEST = "1";
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DOMAIN_CONTROLLER_RESPONSE = "2";
        public static string IHI_PROTOCOL_ZMQ__QUEUE__ZONE_CONTROLLER_REQUEST = "3";
        public static string IHI_PROTOCOL_ZMQ__QUEUE__ZONE_CONTROLLER_RESPONSE = "4";
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DEVICE_REQUEST = "5";
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DEVICE_RESPONSE = "6";
        public static string IHI_PROTOCOL_ZMQ__QUEUE__PUBLICATION = "P";

        public static string IHI_PROTOCOL_ZMQ__DOMAIN_CONTROLLER = "00";
        public static string IHI_PROTOCOL_ZMQ__ZONE_CONTROLLER = "01";
        public static string IHI_PROTOCOL_ZMQ__INVERTER = "02";
        public static string IHI_PROTOCOL_ZMQ__METER = "03";
        public static string IHI_PROTOCOL_ZMQ__CHILLER = "04";
        public static string IHI_PROTOCOL_ZMQ__BATTERY = "05";
        public static string IHI_PROTOCOL_ZMQ__UI = "06";
        public static string IHI_PROTOCOL_ZMQ__IO = "07";

        /* Message "Types" Variables */
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_SITE = "600";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_ZONES = "601";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_RACKS = "602";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_MODULES = "603";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_INVERTERS = "604";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_COOLING = "605";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_IO = "606";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_METERS = "607";
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_USER = "608";
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_GET_CURRENT_PROFILE_NAME = "786";
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_GET_PROFILE_LIST = "787";

        // UI TO DC                                                      // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SITE_INFO = "600"; // UI_GET_SITE_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ZONES_INFO = "601"; // UI_GET_ZONES_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_RACKS_INFO = "602"; // UI_GET_RACKS_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_MODULES_INFO = "603"; // UI_GET_MODULES_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_INFO = "604"; // UI_GET_INVERTERS_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_COOLING_INFO = "605"; // UI_GET_COOLING_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_IO_INFO = "606"; // UI_GET_IO_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_METERS_INFO = "607"; // UI_GET_METERS_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_USER_INFO = "608"; // UI_GET_USER_INFO

        // UI commands
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_MODE_DEMAND = "609"; // UI_SET_MODE_DEMAND // user_id, mode
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_PQ_DEMAND = "610"; // UI_SET_PQ_DEMAND   // for manual and load leveling
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_ENABLED = "611"; // UI_SET_RACK_ENABLED// zone #, rack # (0=all), 0=Disable, 1=Enable
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_DISABLED = "611"; // UI_SET_RACK_DISABLED          // zone #, rack # (0=all), 0=Disable, 1=Enable
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_ENABLED = "613"; // UI_SET_INTERVERS_ENABLED      // zone #, inverter # (0=all), 0=Disable, 1=Enable
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_DISABLED = "613"; // UI_SET_INTERVERS_DISABLED     // zone #, inverter # (0=all), 0=Disable, 1=Enable
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ZONE_ENABLED = "614"; // UI_SET_ZONE_ENABLED// zone #, 0=Disable, 1=Enable
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ZONE_DISABLED = "614"; // UI_SET_ZONE_DISABLED          // zone #, 0=Disable, 1=Enable

        // UI user messages
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_UPDATE_USER_DATA = "670"; // UI_UPDATE_USER_DATA
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DELETE_USER = "671"; // UI_DELETE_USER
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ADD_USER = "672"; // UI_ADD_USER
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_USER_LOGIN = "673"; // UI_USER_LOGIN           // user_id
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_USER_LOGOUT = "674"; // UI_USER_LOGOUT          // user_id


        // UI to EVENTD                                                   // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ACKNOWLEDGE_EVENT = "750"; // UI_ACKNOWLEDGE_EVENT    // user_id [,key].      If no key, all un-acked are acked.
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DISMISS_EVENT = "751"; // UI_DISMISS_EVENT        // [key]     If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_EVENT_ALARM = "752"; // UI_EVENT_ALARM          // Returns all alarms that have not been dismissed
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_EVENT = "753"; // UI_EVENT                // [key]                If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ALARM = "754"; // UI_ALARM                // [key]                If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ACKNOWLEDGE_ALARM = "755"; // UI_ACKNOWLEDGE_ALARM    // user_id [,key].      If no key, all un-acked are acked.
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DISMISS_ALARM = "756"; // UI_DISMISS_ALARM        // [key]                If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DATA_LOG = "760"; // UI_DATA_LOG             // parm=# of minutes history (1 to 180).  Default if not specified: 15 minutes

        // UI Lists
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_AVAILABILITY_TYPES_LIST = "790"; // UI_GET_AVAILABILITY_TYPES_LIST    // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_BATTERY_TYPES_LIST = "791"; // UI_GET_BATTERY_TYPES_LIST         // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_COOLING_STATUS_LIST = "792"; // UI_GET_COOLING_STATUS_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ENABLED_STATUS_LIST = "793"; // UI_GET_ENABLED_STATUS_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_AVAILABLE_MODES_LIST = "794"; // UI_GET_AVAILABLE_MODES_LIST       // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTER_STATUS_LIST = "795"; // UI_GET_INVERTER_STATUS_LIST       // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ZONE_STATUS_LIST = "796"; // UI_GET_ZONE_STATUS_LIST           // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_STATUS_LIST = "797"; // UI_GET_RACK_STATUS_LIST           // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTER_MODES_LIST = "798"; // UI_GET_INVERTER_MODES_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_MESSAGE_TYPES = "799"; // UI_GET_MESSAGE_TYPES              // returns JSON_RESPONSE
    }
}
