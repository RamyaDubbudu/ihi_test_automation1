﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Excel;

namespace ihi_testlib_espilot
{
    public static class PanamaxBlueBoltPDU
    {

        public static bool SetSwitchPortOnOff(bool enableDisable, PanamaxTelnetInfo telnetInfo, int port)
        {
            var enableDisableCommand = enableDisable ? "ON" : "OFF";
            string formatCommand = "!SWITCH " + port + " " + enableDisableCommand + "\r\n";
            TelnetConnection telnet = null;
            try
            {
                telnet = new TelnetConnection(telnetInfo.IPAddress, 23);
                telnet.Write(formatCommand);
                telnet.Read(1000);
                telnet.tcpSocket.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (telnet != null)
                    telnet.tcpSocket.Close();
                return false;
            }
        }

        public static EnabledDisabed CheckAllPortStatus(PanamaxTelnetInfo telnetInfo)
        {
            EnabledDisabed enabledDisabledCombined = new EnabledDisabed();
            enabledDisabledCombined.Enabled = new List<int>();
            enabledDisabledCombined.Disabled = new List<int>();
            TelnetConnection telnet = null;
            try
            {
                telnet = new TelnetConnection(telnetInfo.IPAddress, 23);
                telnet.Write("?OUTLETSTAT\r\n");
                var statusFull = telnet.Read(1000);
                var status = statusFull.Split('\r');
                for (int i = 0; i < status.Length; i++)
                {
                    if (status[i].Contains("ON"))
                        enabledDisabledCombined.Enabled.Add(i + 1);
                    else if (status[i].Contains("OFF"))
                        enabledDisabledCombined.Disabled.Add(i + 1);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (telnet != null)
                    telnet.tcpSocket.Close();
            }

            return enabledDisabledCombined;
        }

        public static bool RebootOutlet(PanamaxTelnetInfo telnetInfo, int port)
        {
            TelnetConnection telnet = null;
            try
            {
                telnet = new TelnetConnection(telnetInfo.IPAddress, 23);
                telnet.Write("!SWITCH " + port + " OFF\r\n");
                telnet.Read(1000);
                telnet.Write("!SWITCH " + port + " ON\r\n");
                telnet.Read(1000);
                telnet.tcpSocket.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (telnet != null)
                    telnet.tcpSocket.Close();
                return false;
            }
        }

        public static void SetallPortsOn(PanamaxTelnetInfo telnetInfo)
        {
            TelnetConnection telnet = null;
            telnet = new TelnetConnection(telnetInfo.IPAddress, 23);
            telnet.Write("!ALL_ON\r\n");
            telnet.Read(1000);
            telnet.tcpSocket.Close();
        }

        public static double GetCurrent(PanamaxTelnetInfo telnetInfo)
        {
            TelnetConnection telnet = new TelnetConnection(telnetInfo.IPAddress, 23);
            telnet.Write("?CURRENT\r\n");
            var responseCurrent = telnet.Read(1000);
            responseCurrent = Regex.Match(responseCurrent, @"\d+").Value;
            double current;
            double.TryParse(responseCurrent, out current);
            return current/10;
        }


        public class EnabledDisabed
        {
            public List<int> Enabled { get; set; }
            public List<int> Disabled { get; set; }
        }
    }

    public class PanamaxTelnetInfo
    {
        public string IPAddress { get; set; }

        public PanamaxTelnetInfo(string ipAddress)
        {
            this.IPAddress = ipAddress;
        }
    }
}
