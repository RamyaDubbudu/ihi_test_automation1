﻿/*==============================================================================
Name        : DatabaseCall.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Class created to manage methods that read/write to the DECS database.
==============================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using Renci.SshNet;

namespace ihi_testlib_espilot
{
    public static class DatabaseCall
    {
        //read value at certain table, column and index (by ID) and reads the data.  Returns a string with the data.
        public static string ReadFromDatabase(string table, string column, int ID)
        {
            string DECSDirectory = "/var/run/ihi-decs/";
                //xxxx_id numbers do not start at 0 however offsets in sql start at 0
                ID = ID - 1;
                string data = "";
            var ssh = new SshClient(GlobalsGeneralStrings.SSHhostname, GlobalsGeneralStrings.SSHusername, GlobalsGeneralStrings.SSHpassword);
            string SSHCommand = string.Format("SELECT {0} FROM {1} LIMIT 1 OFFSET {2};", column, table, ID.ToString());
            ssh.Connect();
            string sDatabaseRead = string.Format("sqlite3 {0}decs_dc.db \"{1}\"", DECSDirectory, SSHCommand);
            //Console.WriteLine(sDatabaseRead);
            string result = ssh.RunCommand(sDatabaseRead).Result;
            ssh.Disconnect();
            return data;
        }

        //Goes to a specific table, column and index (by id) and updates the value of the database to the new string
        public static void UpdateToDatabase(string table, string column, int ID, string newvalue, string columnname)
        {
            var ssh = new SshClient(GlobalsGeneralStrings.SSHhostname, GlobalsGeneralStrings.SSHusername, GlobalsGeneralStrings.SSHpassword);
            string SSHCommand;
            try
            {
                SSHCommand = string.Format(@"UPDATE {0} SET {1}='{2}' WHERE {4} ='{3}'", table, column, newvalue, ID, columnname);
            }
            catch
            {
                //user_id is the only column singular where the table is not
                SSHCommand = string.Format(@"UPDATE {0} SET {1}='{2}' WHERE {4} ='{3}'", table, column, newvalue, ID, columnname);
            }
            ssh.Connect();
            string sDatabaseUpdate = string.Format("sqlite3 {0}decs_dc.db \"{1}\"", "/var/run/ihi-decs/", SSHCommand);
            var terminal = ssh.RunCommand(sDatabaseUpdate);
            ssh.Disconnect();

        }


    }
}
