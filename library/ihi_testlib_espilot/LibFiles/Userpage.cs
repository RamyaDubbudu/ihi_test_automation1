﻿/*==============================================================================
Name        : Userpage.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Class for using the Users page and creating users in the database.
==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Security.Cryptography;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace ihi_testlib_espilot
{
    public static class Userpage
    {

        /// <summary>
        /// Click add new user button
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void ClickAddNewUser(ihi_testlib_selenium.Selenium driver)
        {
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.AddNewUserButton));
        }

        /// <summary>
        /// creates md5 hash encryption string
        /// </summary>
        /// <param name="input">password input in regular string</param>
        /// <returns>encrypted string</returns>
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString().ToLower();
        }

        /// <summary>
        /// Create new user in database with specified information
        /// </summary>
        /// <param name="user_id">user id for user</param>
        /// <param name="user_name">user name for user</param>
        /// <param name="full_name">full name for user</param>
        /// <param name="email_addr">e-mail address for user</param>
        /// <param name="permissions">permission level for user (int)</param>
        /// <param name="alert_types">alter types for user (unknown 8/12/2015)</param>
        /// <param name="password">md5 encrypted password for user</param>
        /// <param name="SSHIPAddress">Linux ip address</param>
        /// <param name="SSHUsername">Linux user name</param>
        /// <param name="SSHPassword">Linux password</param>
        /// <param name="DECSDatabaseDirectory">Location for decs database</param>
        /// <param name="debug"></param>
        public static void CreateUserDatabase(int user_id, string user_name, string full_name, string email_addr,
            int permissions, int alert_types, string password, string SSHIPAddress, string SSHUsername,
            string SSHPassword, string DECSDatabaseDirectory, bool debug = false, string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);

            //turns password into MD5 string
            string password_md5 = CalculateMD5Hash(password);

            string prototype = "(user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5)";

            string createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            if (debug)
                Console.WriteLine(createuser);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, DECSDatabaseDirectory);
        }

        public static void DeleteUsers(string SSHIPAddress, string SSHUsername,
           string SSHPassword, string DECSDatabaseDirectory, bool debug = false, string decsLocation = "/usr/local/ihi-decs/bin/", string userNameValue = null, string FullNameValue = null, string emailAddressValue = null)
        {
            var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);

            string createuser = string.Format("Delete From users where user_name like '{0}' and full_name like '{1}' and email_addr like '{2}'",
                userNameValue, FullNameValue, emailAddressValue);
            if (debug)
                Console.WriteLine(createuser);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, DECSDatabaseDirectory);
        }
        /// <summary>
        /// Load user section of database with known user information (from office)
        /// </summary>
        /// <param name="SSHIPAddress">Linux IP address</param>
        /// <param name="SSHUsername">Linux Username</param>
        /// <param name="SSHPassword">Linux password</param>
        /// <param name="DECSDatabaseDirectory"></param>
        public static void LoadDefaultDatabase(string SSHIPAddress, string SSHUsername, string SSHPassword, string DECSDatabaseDirectory = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            int user_id;
            string user_name;
            string full_name;
            string email_addr;
            int permissions;
            int alert_types;
            string password_md5;
            string prototype = "(user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5)";

            var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);

            SSH.ClearSQLTable(DECSDatabaseDirectory, "decs_dc.db", "users", SSHIPAddress, SSHUsername, SSHPassword);

            //create superuser
            user_id = 1;
            user_name = "admin";
            full_name = "Superuser, a.k.a root";
            email_addr = "admin@ihi-energy-storage.com";
            permissions = 3;
            alert_types = 0;
            password_md5 = "9cc1a75896c27bd94e8affa290eb251b";
            string createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            Console.WriteLine(createuser);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, decsLocation);

            //create user
            user_id = 2;
            user_name = "user";
            full_name = "John Q. Public";
            email_addr = "user@ihi-energy-storage.com";
            permissions = 0;
            alert_types = 0;
            password_md5 = "9cc1a75896c27bd94e8affa290eb251b";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            //Console.WriteLine(createuser);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, decsLocation);

            //create jmcnally
            user_id = 3;
            user_name = "jmcnally";
            full_name = "John McNally";
            email_addr = "john_mcnally@ihiinc.ihi.co.jp";
            permissions = 4;
            alert_types = 0;
            password_md5 = "3c5fbbafaa1018d427a6a237f8999b67";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, decsLocation);

            //create gintis
            user_id = 4;
            user_name = "gintis";
            full_name = "Gintis Nedas";
            email_addr = "gintis_nedas@ihiinc.ihi.co.jp";
            permissions = 4;
            alert_types = 0;
            password_md5 = "507d288c24b2bdf4434b389821a73c70";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, decsLocation);

            //create jim
            user_id = 5;
            user_name = "jim";
            full_name = "Jim Cleveland";
            email_addr = "jim_cleveland@ihiinc.ihi.co.jp";
            permissions = 4;
            alert_types = 0;
            password_md5 = "14cb17da05d5ed307e96248b492f3506";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, decsLocation);

            //create user2
            user_id = 6;
            user_name = "user2";
            full_name = "John R. Public";
            email_addr = "user2@ihi-energy-storage.com";
            permissions = 0;
            alert_types = 0;
            password_md5 = "9cc1a75896c27bd94e8affa290eb251b";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, decsLocation);
        }

        /// <summary>
        /// Load database with known admin/superuser values (all IHI users + admin)
        /// </summary>
        /// <param name="SSHIPAddress">Linux IP address</param>
        /// <param name="SSHUsername">Linux Username</param>
        /// <param name="SSHPassword">Linux password</param>
        /// <param name="DECSDatabaseDirectory">Location of desc database</param>
        public static void LoadSuperusersDatabase(string SSHIPAddress, string SSHUsername, 
            string SSHPassword, string DECSDatabaseDirectory = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            int user_id;
            string user_name;
            string full_name;
            string email_addr;
            int permissions;
            int alert_types;
            string password_md5;
            string prototype = "(user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5)";

            var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);

            SSH.ClearSQLTable(DECSDatabaseDirectory, "decs_dc.db", "users", SSHIPAddress, SSHUsername, SSHPassword);

            //create superuser
            user_id = 1;
            user_name = "admin";
            full_name = "Superuser, a.k.a root";
            email_addr = "admin@ihi-energy-storage.com";
            permissions = 3;
            alert_types = 0;
            password_md5 = "9cc1a75896c27bd94e8affa290eb251b";
            string createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, DECSDatabaseDirectory);

            //create jmcnally
            user_id = 2;
            user_name = "jmcnally";
            full_name = "John McNally";
            email_addr = "john_mcnally@ihiinc.ihi.co.jp";
            permissions = 4;
            alert_types = 0;
            password_md5 = "3c5fbbafaa1018d427a6a237f8999b67";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, DECSDatabaseDirectory);

            //create gintis
            user_id = 3;
            user_name = "gintis";
            full_name = "Gintis Nedas";
            email_addr = "gintis_nedas@ihiinc.ihi.co.jp";
            permissions = 4;
            alert_types = 0;
            password_md5 = "507d288c24b2bdf4434b389821a73c70";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, DECSDatabaseDirectory);

            //create jim
            user_id = 4;
            user_name = "jim";
            full_name = "Jim Cleveland";
            email_addr = "jim_cleveland@ihiinc.ihi.co.jp";
            permissions = 4;
            alert_types = 0;
            password_md5 = "14cb17da05d5ed307e96248b492f3506";
            createuser = string.Format("INSERT INTO users {0} VALUES({1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}')",
                prototype, user_id, user_name, full_name, email_addr, permissions, alert_types, password_md5);
            QueryInterface.CommandThroughQuery(createuser, ssh, SSHIPAddress, DECSDatabaseDirectory);
        }

        /// <summary>
        /// Returns the number of rows in a table for a table on screen
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static int GetNumberofUsersRows(ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                if (driver.IsElementVisible(xPath:string.Format(GlobalElementsAdmin.UserTable)))
                {
                    IWebElement webElementBody = driver.FindHTMLControl(xPath:string.Format(GlobalElementsAdmin.UserTable));
                    IList<IWebElement> ElementCollectionBody = webElementBody.FindElements(By.XPath(GlobalElementsAdmin.UserTableRows));
                    //Makes sure only entry doesn't say list is empty
                    if (ElementCollectionBody.Count == 1 && ElementCollectionBody[0].Text == "There are no Users!")
                        return 0;

                    return ElementCollectionBody.Count;
                }
                else
                {
                    //Console.WriteLine("Chart not displayed");
                    return -1;
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.ToString());
                return -1;
            }
        }

        //Gets the number of pages will be present based on the number of Userss + number of Userss on last page (remainder)
        public static void GetNumberofPagesWithLastPage(int numberofUsers, out int numberofpages, out int lastpagenumber, int Userssperpage = 15)
        {
            numberofpages = (numberofUsers / Userssperpage) + 1;
            lastpagenumber = numberofUsers % Userssperpage;
        }

        /// <summary>
        /// cycles through all pages until end and counts how many entries
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>int of number of user</returns>
        public static int CountNumberofUsers(ihi_testlib_selenium.Selenium driver)
        {
            int countUsers = 0;
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                countUsers += GetNumberofUsersRows(driver);
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.NextIsActive));
            }
            countUsers += GetNumberofUsersRows(driver);
            GoToFirstPage(driver);
            return countUsers;
        }

        /// <summary>
        /// Goes to first page of the table
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void GoToFirstPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.PreviousIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.PreviousIsActive));
            }
        }

        /// <summary>
        /// Goes to Last page of the table
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void GoToLastPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.NextIsActive));
            }
        }

        /// <summary>
        /// Searches all pages of the user tables for user with specific information
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="fullName"></param>
        /// <param name="email"></param>
        /// <param name="permission"></param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool FindUserInList(string userName, string fullName, string email, string permission, ihi_testlib_selenium.Selenium driver)
        {
            int pageNumber = 1;
            int countUsersOnPage = 0;
            string matchString = userName + " " + fullName + " " + email + " " + permission + " Edit" + " Delete";

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                countUsersOnPage = GetNumberofUsersRows(driver);

                for (int i = 0; i < (countUsersOnPage); i++)
                {
                    string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                    rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                    if (rowValue == matchString)
                    {
                        GoToFirstPage(driver);
                        return true;
                    }
                }

                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.NextIsActive));
                pageNumber++;
            }

            countUsersOnPage = GetNumberofUsersRows(driver);

            for (int i = 0; i < (countUsersOnPage); i++)
            {
                string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                if (rowValue == matchString)
                {
                    GoToFirstPage(driver);
                    return true;
                }
            }
            GoToFirstPage(driver);
            return false;
        }


        public static bool DeleteUser(string userName, string fullName, string email, string permission, ihi_testlib_selenium.Selenium driver)
        {
            int pageNumber = 1;
            int countUsersOnPage = 0;
            string matchString = userName + " " + fullName + " " + email + " " + permission + " Edit" + " Delete";
            string matchStringAdmin = userName + " " + fullName + " " + email + " " + permission + " Edit";

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                countUsersOnPage = GetNumberofUsersRows(driver);

                for (int i = 0; i < (countUsersOnPage); i++)
                {
                    string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                    rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                    if ((rowValue == matchString) || (rowValue == matchStringAdmin))
                    {
                        //offset due to first admin not having delete button
                        if (pageNumber != 1)
                            i++;
                        //i is used to offset the number of deletes screen
                        driver.browserDriver.FindElement(By.XPath(string.Format("(//button[@action='Delete'])[{0}]", i))).Click();
                        CloseAlertAndGetItsText(driver);
                        GoToFirstPage(driver);
                        return true;
                    }
                }

                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.NextIsActive));
                pageNumber++;
            }

            //The last loop handles the last page (or first page if only one page)
            countUsersOnPage = GetNumberofUsersRows(driver);

            for (int i = 0; i < (countUsersOnPage); i++)
            {
                string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                if ((rowValue == matchString) || (rowValue == matchStringAdmin))
                {
                    //offset due to first admin not having delete button
                    if (pageNumber != 1)
                        i++;
                    //i is used to offset the number of deletes screen
                    driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.DeleteButtonbyUsername, userName));
                    CloseAlertAndGetItsText(driver);
                    GoToFirstPage(driver);
                    return true;
                }
            }
            GoToFirstPage(driver);
            return false;
        }

        /// <summary>
        /// Searches for user to edit and clicks the edit window
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="fullName"></param>
        /// <param name="email"></param>
        /// <param name="permission"></param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool ClickEditUser(string userName, string fullName, string email, string permission, ihi_testlib_selenium.Selenium driver)
        {
            int pageNumber = 1;
            int countUsersOnPage = 0;
            string matchString = userName + " " + fullName + " " + email + " " + permission + " Edit" + " Delete";
            string matchStringAdmin = userName + " " + fullName + " " + email + " " + permission + " Edit";

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                countUsersOnPage = GetNumberofUsersRows(driver);

                for (int i = 0; i < (countUsersOnPage); i++)
                {
                    string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                    rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                    if ((rowValue == matchString) || (rowValue == matchStringAdmin))
                    {
                        //i is used to offset the number of deletes screen
                        driver.browserDriver.FindElement(By.XPath(string.Format("(//button[@action='Edit'])[{0}]", i + 1))).Click();
                        driver.SwitchtoWindow(driver, "ES/Pilot - Edit User");
                        return true;
                    }
                }


                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.NextIsActive));
                pageNumber++;
            }

            //The last loop handles the last page (or first page if only one page)
            countUsersOnPage = GetNumberofUsersRows(driver);

            for (int i = 0; i < (countUsersOnPage); i++)
            {
                string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                if ((rowValue == matchString) || (rowValue == matchStringAdmin))
                {
                    //i is used to offset the number of deletes screen
                    driver.browserDriver.FindElement(By.XPath(string.Format("(//button[@action='Edit'])[{0}]", i + 1))).Click();
                    driver.SwitchtoWindow(driver, "ES/Pilot - Edit User");
                    return true;
                }
            }
            return false;
        }

        //Opens the first edit user on page, helpful for when program just wants to open an edit window
        public static void EditFirstUserOnPage(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.FindElement(By.XPath(string.Format("(//button[@action='Edit'])[{0}]", 1))).Click();
            driver.SwitchtoWindow(driver, "ES/Pilot - Edit User");
        }

        private static bool acceptNextAlert = true;
        //Method to handle popup needed for deleting user
        private static string CloseAlertAndGetItsText(ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                IAlert alert = driver.browserDriver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }

        }

        /// <summary>
        /// Takes in a list of users tables (usually from zmq), clears the database and loads in only these values.  Useful when making tests non-descructive
        /// </summary>
        /// <param name="userList"></param>
        /// <param name="IPAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseLocation"></param>
        public static void UpdateDatabaseFromUsersTableList(List<UserTable> userList, string IPAddress, string userName, 
            string password, string databaseLocation = "/var/run/ihi-decs/", string decsLocation = "/usr/local/ihi-decs/bin/")
        {
            SshClient ssh = new SshClient(IPAddress, userName, password);

            try
            {
                ssh.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            string table = "users";

            SSH.ClearSQLTable(databaseLocation, "decs_dc.db", table, IPAddress, userName, password);

            UserTable userTableTemp = new UserTable();
            List<string> propertyNames = userTableTemp.GetProperyNames();

            string lastItem = propertyNames.Last();
            string firstItem = propertyNames.First();

            //the following code is formating the sql insert into statement using reflection
            string prototypeInto = " (";
            foreach (string item in propertyNames)
            {
                if (item != lastItem)
                    prototypeInto += item + (", ");
                else
                    prototypeInto += item + (") ");
            }

            foreach (UserTable userTable in userList)
            {
                List<string> objectStrings = userTable.GetPropertyValues();
                string last = objectStrings.Last();

                string prototypeSelect = " (";
                int findLastLoop = 1;
                foreach (string value in objectStrings)
                {
                    if (!(findLastLoop == objectStrings.Count))
                        prototypeSelect += value + (", ");
                    else
                        prototypeSelect += value + (") ");
                    findLastLoop++;
                }
                string sqlStatement = string.Format("INSERT INTO {0}{1} VALUES {2}", table, prototypeInto, prototypeSelect);

                QueryInterface.CommandThroughQuery(sqlStatement, ssh, IPAddress, decsLocation);
            }
            ssh.Disconnect();
        }
    }
}
