﻿/*==============================================================================
Name        : DQANavi.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2016 IHI, Inc.  All Rights Reserved
Description : Interface to get and set I/O DAQ Navi device by Avantech
==============================================================================*/

using System;
using Automation.BDaq;

namespace ihi_testlib_espilot
{
    public class DAQNavi : IDisposable
    {
        private readonly InstantDiCtrl _instantDiCtrl;
        private readonly InstantDoCtrl _instantDoCtrl;
        private ErrorCode _errorCode = ErrorCode.Success;

        public DAQNavi(string deviceName)
        {
            _instantDoCtrl = new InstantDoCtrl();
            _instantDiCtrl = new InstantDiCtrl();
            _instantDoCtrl.SelectedDevice = new DeviceInformation(deviceName);
            _instantDiCtrl.SelectedDevice = new DeviceInformation(deviceName);
            if(_instantDiCtrl==null || _instantDoCtrl == null)
                throw new TypeInitializationException("DAQNavi", null);
        }
        
        public ErrorCode GetErrorCode {get { return _errorCode;}}

        public byte GetPort(int portNumber)
        {
            byte[] buffer = new byte[64];
            _errorCode = _instantDiCtrl.Read(0, 1, buffer);
            return buffer[portNumber];
        }

        public void SetPort(byte value, int portNumber)
        {
            var allPorts = GetAllPins(portNumber);
            allPorts[portNumber] = value;
            _errorCode = _instantDoCtrl.Write(portNumber, 1, allPorts);
        }

        public bool GetPin(int portNumber, int pinNumber)
        {
            var port = GetPort(portNumber);
            return (port & (1 << pinNumber)) != 0;
        }

        public void SetPin(bool value, int portNumber, int pinNumber)
        {
            var allPorts = GetAllPins(portNumber);
            byte currentPort = allPorts[portNumber];
            if (value)
                currentPort = (byte) (currentPort | (1 << pinNumber));
            else
            {
                currentPort = (byte) (currentPort & ~(1 << pinNumber));
            }
            SetPort(currentPort, portNumber);
        }

        public void SetAllPins(int portNumber, bool value = false)
        {
            byte setValue = value ? (byte) 0xFF : (byte) 0x00;
            byte[] buffer = new byte[64];
            for (int i = 0; i < buffer.Length; i++)
                buffer[i] = setValue;
            _errorCode = _instantDoCtrl.Write(portNumber, 1, buffer);
        }

        private byte[] GetAllPins(int portNumber)
        {
            byte[] buffer = new byte[64];
            _errorCode = _instantDiCtrl.Read(portNumber, 1, buffer);
            return buffer;
        }

        public void Dispose()
        {
            _instantDoCtrl.Dispose();
            _instantDiCtrl.Dispose();
        }

        [Flags]
        public enum Inputs
        {
            NONE = 0x0,
            FREE = 0x1,
            UPS_LOW_BAT = 0x2,
            UPS_ON_BAT = 0x4,
            FIRE_ARMED = 0x8,
            FIRE_DISCHARGE = 0x10,
            RUN_STOP = 0x20,
            EPO = 0x40,
            DOOR_OPEN = 0x80,
        }

        [Flags]
        public enum Relays
        {            
        }
    }
}
