﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Threading;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Methods and properties that work with the Control tab
    /// </summary>
    public static class ControlPage
    {

        /// <summary>
        /// Navigates to Zones subtab
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void navigateToZones(ihi_testlib_selenium.Selenium driver)
        {
            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.ZonesTab)))
            {
                IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsControl.ZonesTab));
                tab.Click();
                //driver.browserDriver.SwitchTo().Frame(driver.browserDriver.FindElement(GlobalElementsControl.Zones.ZonesFrame));
            }
        }

        /// <summary>
        /// Naviages to Racks subtab
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void navigateToRacks(ihi_testlib_selenium.Selenium driver)
        {
            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.RacksTab)))
             {
                IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsControl.RacksTab));
                 tab.Click();
                 ////driver.browserDriver.SwitchTo().Frame(driver.browserDriver.FindElement(GlobalElementsControl.Racks.RacksFrame));
             }
        }

        /// <summary>
        /// Navigates to Inverters subtab
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void navigateToInverters(ihi_testlib_selenium.Selenium driver)
        {
            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.InvertersTab)))
            {
                IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsControl.InvertersTab));
                tab.Click();
                ////driver.browserDriver.SwitchTo().Frame(driver.browserDriver.FindElement(GlobalElementsControl.Inverters.InvertersFrame));
            }
        }

        /// <summary>
        /// Naviates to Environments (somethinges called "Coolers") subtab
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        public static void navigateToEnvironments(ihi_testlib_selenium.Selenium driver)
        {
            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsControl.EnvironmentTab)))
            {
                IWebElement tab = driver.FindHTMLControl(xPath: string.Format(GlobalElementsControl.EnvironmentTab));
                tab.Click();
                ////driver.browserDriver.SwitchTo().Frame(driver.browserDriver.FindElement(GlobalElementsControl.Environment.EnvironmentFrame));
            }
        }

        /// <summary>
        /// Clicks the button number based on how many enable buttons down from top of screen there are
        /// Note: this will have to be changed somewhat if some rows have enable buttons and some do not
        /// </summary>
        /// <param name="index">How many enables down, starts at 1</param>
        /// <param name="driver"></param>
        public static bool ClickEnableButton(int index, string buttonType, ihi_testlib_selenium.Selenium driver)
        {
            string path = string.Format("(//button[@id='idIHI_UI_CONTROL__{0}__buttonEnable'])[{1}]", buttonType, index);
            IWebElement button = driver.browserDriver.FindElement(By.XPath(path));

            if (driver.IsElementVisible(xPath: string.Format(path)))
            {
                button.Click();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Clicks the button number based on how many enable buttons down from top of screen there are
        /// Note: this will have to be changed somewhat if some rows have enable buttons and some do not
        /// </summary>
        /// <param name="index">How many enables down, starts at 1</param>
        /// <param name="driver"></param>
        public static bool ClickDisableButton(int index, string buttonType, ihi_testlib_selenium.Selenium driver)
        {
            string path = string.Format("(//button[@id='idIHI_UI_CONTROL__{0}__buttonDisable'])[{1}]", buttonType, index);

            if (driver.IsElementVisible(xPath: string.Format(path)))
            {
                IWebElement button = driver.browserDriver.FindElement(By.XPath(path));
                button.Click();
                return true;
            }
            else
                return false;
        }

        public static ControlAssetInfo NameAndOrderAssets(SSHAndDatabaseDirectory connectionInfo, bool includeZone)
        {
            var assetInfo = new ControlAssetInfo(connectionInfo.SSHIPAddress);
            //var updatedAssetInfo = new ControlAssetInfo(ipAddress);

            int zoneNumber = 0;
            foreach (ZoneTable zoneInfo in assetInfo.zoneList)
            {
                zoneInfo.zones_id = ++zoneNumber;
                zoneInfo.label = "Zone " + zoneNumber;
            }

            int rackNumber = 0;
            foreach (RackTable assetTable in assetInfo.rackList)
            {
                assetTable.racks_id = ++rackNumber;
                if(includeZone)
                    assetTable.label = "Rack " + rackNumber + " in Zone " + assetTable.zones_id;
                else
                    assetTable.label = "Rack " + rackNumber;
            }

            int inverterNumber = 0;
            foreach (InverterTable assetTable in assetInfo.inverterList)
            {
                assetTable.inverters_id = ++inverterNumber;
                if (includeZone)
                    assetTable.label = "Inverter " + inverterNumber + " in Zone " + assetTable.zones_id;
                else
                    assetTable.label = "Inverter " + inverterNumber;
            }

            int coolingNumber = 0;
            foreach (CoolingTable assetTable in assetInfo.coolingList)
            {
                assetTable.cooling_id = ++coolingNumber;
                if (includeZone)
                    assetTable.label = "Cooling Unit " + coolingNumber + " in Zone " + assetTable.zones_id;
                else
                    assetTable.label = "Cooling Unit " + coolingNumber;
            }

            try
            {
                assetInfo.LoadValuesIntoDatabase(connectionInfo.SSHIPAddress, connectionInfo.SSHUsername, connectionInfo.SSHPassword, connectionInfo.SSHDatabaseDirectory);
                return assetInfo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }

    /// <summary>
    /// Houses error information and between database, UI, and ZMQ query on all Control tabs
    /// </summary>
    public class ControlErrorInfo
    {
        public string ID { get; set; }
        public string location { get; set; }
        public string uiValue { get; set; }
        public string zmqValue { get; set; }
        public string databaseValue { get; set; }

        /// <summary>
        /// Contstructor for ControlErrorInfo
        /// </summary>
        /// <param name="IDError">what time of data the error was associated with</param>
        /// <param name="locationError">where in the list (number) the error was located</param>
        /// <param name="uiValueError">UI value</param>
        /// <param name="zmqValueError">zmq query value</param>
        /// <param name="databaseValueError">database value</param>
        public ControlErrorInfo(string IDError, string locationError, string uiValueError, string zmqValueError, string databaseValueError)
        {
            this.ID = IDError;
            this.location = locationError;
            this.uiValue = uiValueError;
            this.zmqValue = zmqValueError;
            this.databaseValue = databaseValueError;
        }
    }
}
