﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using TelnetExpect;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace ihi_testlib_espilot
{
    /// <summary>
    /// Class for communication with Netgear M4100-D12G
    /// </summary>
    public static class NetgearSwitchCommunication
    {
        /// <summary>
        /// Enables/Disables a port on the switch by port number
        /// </summary>
        /// <param name="port">Port number to change</param>
        /// <param name="telnetInfo">Class that houses IPAddress, Username and Password of switch</param>
        /// <param name="enableDiable">True = enable, False = disable</param>
        /// <returns></returns>
        public static bool SetSwitchPortOnOff(bool enableDiable, NetgearTelnetInfo telnetInfo, int port)
        {
            string selectPort = "interface 0/" + port;
            string expectedPort = string.Format("(Interface 0/{0})#", port);
            //sets port to be in shutdown or no shutdown mode;
            string portSet = enableDiable ? "no shutdown" : "shutdown";

            //sequence to send and expected feedback from switch.  Program will throw exception if expected is different from results
            string[] commandSequence = { "enable", "configure", selectPort, portSet };
            string[] expectSequence = { "(M4100-D12G) #", "(M4100-D12G) (Config)#", "(M4100-D12G) " + expectedPort, "(M4100-D12G) " + expectedPort };

            //combines command and expected for foreach statement
            var commandAndExpected = commandSequence.Zip(expectSequence, (n, w) => new { Command = n, Expected = w });
            TcpClient client = null;
            try
            {
                client = new TcpClient(telnetInfo.IPAddress, 23);
                var m_xp = ExpectedLogin(client, telnetInfo);

                //sends data with expected sequence
                foreach (var commandexpected in commandAndExpected)
                {
                    m_xp.SendLine(commandexpected.Command);
                    m_xp.Expect(commandexpected.Expected);
                }
                client.Close();
                return true;
            }
            catch (Exception e)
            {
                if(client.Connected)
                    client.Close();
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool SetSwitchPortOnOff(bool enableDisable, NetgearTelnetInfo telnetInfo, SwitchPort port)
        {
            return SetSwitchPortOnOff(enableDisable, telnetInfo, (int) port);
        }

        public static EnabledDisabed CheckAllPortStatus(NetgearTelnetInfo telnetInfo)
        {
            EnabledDisabed enabledDisabledCombined = new EnabledDisabed();
            enabledDisabledCombined.Enabled = new List<int>();
            enabledDisabledCombined.Disabled = new List<int>();
            TelnetConnection telnet = null;

            try
            {
                telnet = NetgearLogin(telnetInfo);
                telnet.tcpSocket.GetStream().Flush();
                telnet.WriteLine("enable");
                telnet.WriteLine("show port all | include \"Enable    Auto\"");
                System.Threading.Thread.Sleep(500);
                string rawEnabled = telnet.Read();
                var rawenabledSplit = rawEnabled.Split(new[] {"0/"}, StringSplitOptions.None);

                for (int i = 1; i < rawenabledSplit.Length; i++)
                {
                    var enabledPort = rawenabledSplit[i].Substring(0, 2).TrimEnd();
                    enabledDisabledCombined.Enabled.Add(int.Parse(enabledPort));
                }
                telnet.tcpSocket.GetStream().Flush();
                telnet.WriteLine("show port all | include \"Disable   Auto\"");
                System.Threading.Thread.Sleep(200);
                string rawDisabled = telnet.Read();
                telnet.tcpSocket.Close();
                var rawDisabledSplit = rawDisabled.Split(new[] { "0/" }, StringSplitOptions.None);

                for (int i = 1; i < rawDisabledSplit.Length; i++)
                {
                    var enabledPort = rawDisabledSplit[i].Substring(0, 2).TrimEnd();
                    enabledDisabledCombined.Disabled.Add(int.Parse(enabledPort));
                }

                return enabledDisabledCombined;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (telnet != null) telnet.tcpSocket.Close();
                return enabledDisabledCombined;
            }
        }

        private static TelnetConnection NetgearLogin(NetgearTelnetInfo telnetInfo)
        {
            TelnetConnection telnet = null;
            try
            {
                telnet = new TelnetConnection(telnetInfo.IPAddress, 23);
                telnet.Read();
                telnet.WriteLine(telnetInfo.Login);
                telnet.Read();
                telnet.WriteLine(telnetInfo.Password);
                telnet.Read();
            }
            catch (Exception e)
            {
               Console.WriteLine(e.Message);
            }
            return telnet;
        }

        public class EnabledDisabed
        {
            public List<int> Enabled { get; set; }
            public List<int> Disabled { get; set; } 
        }

        /// <summary>
        /// Sets up expected object and attempts to log in to the switch
        /// </summary>
        /// <param name="client"></param>
        /// <param name="telnetInfo"></param>
        /// <returns></returns>
        private static Expector ExpectedLogin(TcpClient client, NetgearTelnetInfo telnetInfo)
        {

            TelnetStream telnet = new TelnetStream(client.GetStream());
            telnet.SetRemoteMode(TelnetOption.Echo, false);
            var m_xp = new Expector(telnet);
            //Timeout of 1 second for receiving expected string
            TimeSpan timeout = new TimeSpan(0, 0, 1);
            m_xp.ExpectTimeout = timeout;

            m_xp.Expect("User:");
            m_xp.SendLine(telnetInfo.Login);
            m_xp.Expect("Password:");
            m_xp.SendLine(telnetInfo.Password);

            return m_xp;
        }

        /// <summary>
        /// Sets all ports on in telnet switch
        /// </summary>
        /// <param name="telnetInfo"></param>
        public static void SetAllPortsOn(NetgearTelnetInfo telnetInfo)
        {
            for (int i = 1; i <= 12; i++)
            {
                SetSwitchPortOnOff(true, telnetInfo, i);
            }
        }
    }

    public class NetgearTelnetInfo
    {
        public string IPAddress { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public NetgearTelnetInfo(string ipaddress, string login, string password)
        {
            this.IPAddress = ipaddress;
            this.Login = login;
            this.Password = password;
        }
    }
}
