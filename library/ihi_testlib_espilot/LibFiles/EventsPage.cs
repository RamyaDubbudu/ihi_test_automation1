﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace ihi_testlib_espilot
{
    public static class EventsPage
    {

        public class EventsInfo
        {
            public int EventID { get; set; }
            public string EventTime { get; set; }
            public string AckowledgeTime { get; set; }
            public string Severity { get; set; }
            public string EventDescription { get; set; }
        }

        /// <summary>
        ///     Create events that will show on the events page
        /// </summary>
        /// <param name="numberOfEvents">number of events to create</param>
        /// <param name="sshIPAddress">IP address of backend</param>
        /// <param name="sshUserName"></param>
        /// <param name="sshPassword"></param>
        /// <param name="runningTally">Running tally of where to start the event ID</param>
        /// <param name="debug">set if in debug mode which will have feedback into console</param>
        /// <param name="eventsDatabaseLocation">location of the events database</param>
        public static void GenerateNumberOfEvents(int numberOfEvents, string sshIPAddress, string sshUserName,
            string sshPassword, int runningTally = 0, bool debug = false,
            string eventsDatabaseLocation = "/var/run/ihi-decs/")
        {
            var prototypesql =
                "(event_id, event_key, severity, is_alarm, active, device_id, additional_info, event_type, create_time, ack_user, ack_notes, ack_time)";
            using (var ssh = SSH.ConnectSSH(sshIPAddress, sshUserName, sshPassword))
            {
                for (var i = 1; i <= numberOfEvents; i++)
                {
                    var create_time = DateTime.Now.ToString();
                    create_time = DateTime.Now.ToString("u");
                    create_time = create_time.Remove(create_time.Length - 1);
                    var event_id = i + runningTally;
                    var is_alarm = 0;
                    var ievent_key = i + runningTally;
                    var event_key = ievent_key.ToString().PadLeft(32, '0');
                    var severity = i%4;
                    var active = 0;
                    var device_id = 15;
                    var ack_user = 0;
                    var ack_notes = "";
                    var ack_time = "";
                    var event_type = -19;

                    var additional_info = string.Format("Alarm with Alarm_id {0} and Alarm_key {1}", i, event_key);
                    var createalarmstring =
                        string.Format(
                            "echo \"{0}\" | sudo -S sqlite3 {1}{2} \"INSERT INTO events {3} VALUES({4}, '{5}', {6}, {7}, {8}, {9}, '{10}', {11}, '{12}', {13}, '{14}', '{15}')\"",
                            sshPassword, eventsDatabaseLocation, "decs_events.db", prototypesql, event_id, event_key,
                            severity, is_alarm, active, device_id, additional_info, event_type, create_time, ack_user,
                            ack_notes, ack_time);

                    var terminal = ssh.RunCommand(createalarmstring);
                    if (debug)
                    {
                        Console.WriteLine(createalarmstring + "\n");
                        Console.WriteLine(terminal.Result + "\n");
                    }
                }
                ssh.Disconnect();
            }
        }

        public static int GetNumberofEventsFromTopOfPage(ihi_testlib_selenium.Selenium driver)
        {
            var events = -1;

            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsEvents.EventNumber)))
            {
                var ThereAreEvents = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsEvents.EventNumber));
                var eventsString = Regex.Match(ThereAreEvents, @"\d+").Value;
                events = int.Parse(eventsString);
            }
            return events;
        }

        public static int GetNumberofEventsFromBottomOfPage(ihi_testlib_selenium.Selenium driver)
        {
            var events = -1;

            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsEvents.EventNumber)))
            {
                var ThereAreEvents = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsEvents.BottomListingNumber));
                var numbers = Regex.Split(ThereAreEvents, @"\D+");
                events = int.Parse(numbers[2]);
            }
            return events;
        }

        /// <summary>
        ///     Returns number of events shown on page
        /// </summary>
        /// <param name="TableID"></param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static int GetNumberofEventRows(ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                if (driver.IsElementVisible(xPath: string.Format(GlobalElementsEvents.EventsTable)))
                {
                    var webElementBody = driver.FindHTMLControl(xPath: string.Format(GlobalElementsEvents.EventsTable));
                    IList<IWebElement> ElementCollectionBody = driver.browserDriver.FindElements(By.XPath(GlobalElementsEvents.EventsTableRows));
                    //Makes sure only entry doesn't say list is empty
                    if (ElementCollectionBody.Count == 1 && ElementCollectionBody[0].Text == "There are no events!")
                        return 0;

                    return ElementCollectionBody.Count;
                }
                return -1;
            }
            catch (StaleElementReferenceException)
            {
                //Stale element means list no longer exists
                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }
        }

        /// <summary>
        ///     Gets the number of pages will be present based on the number of events + number of events on last page (remainder)
        /// </summary>
        /// <param name="numberofevents"></param>
        /// <param name="numberofpages"></param>
        /// <param name="lastpagenumber"></param>
        /// <param name="eventsperpage"></param>
        public static void GetNumberofPagesWithLastPage(int numberofevents, out int numberofpages,
            out int lastpagenumber, int eventsperpage = 15)
        {
            numberofpages = numberofevents/eventsperpage + 1;
            lastpagenumber = numberofevents%eventsperpage;
        }

        /// <summary>
        ///     cycles through all pages until end and counts how many entries
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static int CountNumberofEvents(ihi_testlib_selenium.Selenium driver)
        {
            var countevents = 0;
            try
            {
                while (driver.IsElementVisible(xPath: string.Format(GlobalElementsEvents.NextIsActive)))
                {
                    countevents += GetNumberofEventRows(driver);
                    driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEvents.NextIsActive));
                }
                countevents += GetNumberofEventRows(driver);
                GoToFirstPage(driver);
            }
            catch (StaleElementReferenceException)
            {
                Console.WriteLine("Stale element, retrying");
                CountNumberofEvents(driver);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return countevents;
        }

        public static void GoToFirstPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsEvents.PreviousIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEvents.PreviousIsActive));
            }
        }

        public static void GoToLastPage(ihi_testlib_selenium.Selenium driver)
        {
            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsEvents.NextIsActive)))
            {
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEvents.NextIsActive));
            }
        }

        /// <summary>
        ///     check if all events have been achknowledged
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool AreAllEventsAcknowledged(ihi_testlib_selenium.Selenium driver)
        {
            var pagenumber = 1;
            var counteventsonpage = 0;
            var allacknowleged = true;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsEvents.NextIsActive)))
            {
                counteventsonpage = GetNumberofEventRows(driver);
                for (var i = 0; i < counteventsonpage; i++)
                {
                    var rowvalue = driver.ReturnTableRowValue(GlobalElementsEvents.EventsTableRows, i);
                    var numbermorethantwo20s = Regex.Matches(rowvalue, "/20").Count > 1;
                    var numbermorethantwocolons = Regex.Matches(rowvalue, ":").Count > 2;
                    if (!(numbermorethantwo20s && numbermorethantwocolons))
                    {
                        Console.WriteLine(rowvalue);
                        Console.WriteLine("Row number = " + i);
                        Console.WriteLine("Page number = " + pagenumber);
                        allacknowleged = false;
                        GoToFirstPage(driver);
                        return allacknowleged;
                    }
                }

                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEvents.NextIsActive));
                pagenumber++;
            }
            counteventsonpage = GetNumberofEventRows(driver);
            for (var i = 1; i < counteventsonpage; i++)
            {
                var rowvalue = driver.ReturnTableRowValue(GlobalElementsEvents.EventsTableRows, i);
                var numbermorethantwo20s = Regex.Matches(rowvalue, "/20").Count > 1;
                var numbermorethantwocolons = Regex.Matches(rowvalue, ":").Count > 2;
                if (!(numbermorethantwo20s && numbermorethantwocolons))
                {
                    GoToFirstPage(driver);
                    allacknowleged = false;
                    return allacknowleged;
                }
            }
            GoToFirstPage(driver);
            return allacknowleged;
        }

        public static bool SearchForText(string searchedText, ihi_testlib_selenium.Selenium driver)
        {

            int pageNumber = 1;
            int countUsersOnPage = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAdmin.NextIsActive)))
            {
                countUsersOnPage = GetNumberofEventRows(driver);

                for (int i = 0; i < (countUsersOnPage); i++)
                {
                    string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                    rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                    if (rowValue.Contains(searchedText)) ;
                    {
                        GoToFirstPage(driver);
                        return true;
                    }
                }

                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAdmin.NextIsActive));
                pageNumber++;
            }

            countUsersOnPage = GetNumberofEventRows(driver);

            for (int i = 0; i < (countUsersOnPage); i++)
            {
                string rowValue = driver.ReturnTableRowValue(GlobalElementsAdmin.UserTableRows, i);
                rowValue = rowValue.Substring(rowValue.IndexOf(" ") + 1);

                if (rowValue.Contains(searchedText))
                {
                    GoToFirstPage(driver);
                    return true;
                }
            }
            GoToFirstPage(driver);
            return false;
        }

        /// <summary>
        /// Returns rack info on page based on index
        /// </summary>
        /// <param name="index">index of tabe starts at 1</param>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static EventsInfo returnEventInfoInfo(int index, ihi_testlib_selenium.Selenium driver)
        {
            System.Threading.Thread.Sleep(2000);
            //xpaths for all tables are formated in this way with id
            string xpath = string.Format("//table[@id='{0}']/tbody/tr", GlobalElementsEvents.EventsTableString);
            EventsInfo eventInfo = new EventsInfo();
            try
            {
                if (driver.browserDriver.FindElement(By.XPath(xpath)).Displayed)
                {
                    //AlarmID
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[1]", GlobalElementsEvents.EventsTableString, index);
                    string elementString = driver.ReadControlsInnerText(xPath: xpath);
                    int element;
                    int.TryParse(elementString, out element);
                    eventInfo.EventID = element;
                    //AlarmTime
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[2]", GlobalElementsEvents.EventsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    eventInfo.EventTime = elementString;
                    //AckowledgeTime
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[3]", GlobalElementsEvents.EventsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    eventInfo.AckowledgeTime = elementString;
                    //Severity
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[4]", GlobalElementsEvents.EventsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    eventInfo.Severity = elementString;
                    //AlarmDescription
                    xpath = string.Format("//table[@id='{0}']/tbody/tr[{1}]/td[5]", GlobalElementsEvents.EventsTableString, index);
                    elementString = driver.ReadControlsInnerText(xPath: xpath);
                    eventInfo.EventDescription = elementString;

                    return eventInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (StaleElementReferenceException)
            {
                Console.WriteLine("Stale element found, retrying");
                return returnEventInfoInfo(index, driver);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Cycles through all pages of racks and returns a list with all rack information
        /// </summary>
        /// <param name="driver">Firefox Driver</param>
        /// <returns>List with all rack info objects</returns>
        public static List<EventsInfo> GetAllEventsInfo(ihi_testlib_selenium.Selenium driver)
        {
            List<EventsInfo> alarmList = new List<EventsInfo>();
            int countRacks = 0;

            while (driver.IsElementVisible(xPath: string.Format(GlobalElementsAlarms.NextIsActive)))
            {
                countRacks = GetNumberofEventRows(driver);
                for (int i = 1; i <= countRacks; i++)
                {
                    alarmList.Add(returnEventInfoInfo(i, driver));
                }
                driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.NextIsActive));
            }
            countRacks = GetNumberofEventRows(driver);
            for (int i = 1; i <= countRacks; i++)
            {
                alarmList.Add(returnEventInfoInfo(i, driver));
            }
            GoToFirstPage(driver);
            return alarmList;
        }

    }
}