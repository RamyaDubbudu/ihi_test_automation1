﻿/*==============================================================================
Name        : GeneralDECS.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Class created to manage methods are commonly used
==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System.Diagnostics;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Edge;



namespace ihi_testlib_espilot
{

    
    /// <summary>
    /// General Methods that can be applied to almost every Page
    /// </summary>
    public static class GeneralDECS
    {

       

     
        /// <summary>
        /// Creates a new driver and sets defaults
        /// </summary>
        /// <param name="driver">outputs Firefox driver object</param>
        public static void openPage(ihi_testlib_selenium.Selenium driver)
        {
            SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, driver);
            //sets page timeout at 10 seconds (default is 60)
            driver.browserDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            //Will wait 2 seconds for element to be located if stale (might want to change)
            driver.browserDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }

        /// <summary>
        /// closes the page of the driver passed to it.
        /// </summary>
        /// <param name="driver">Firefox driver to be closed</param>
        public static void closePage(ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.Quit();
        }

        /// <summary>
        /// take input of a driver and navigates to the inputted page at max window size.
        /// </summary>
        /// <param name="homepage">url of page to navigate to</param>
        /// <param name="driver">Firefox driver</param>
        public static void openPagewithHomepage(string homepage, ihi_testlib_selenium.Selenium driver)
        {
            driver.browserDriver.Navigate().GoToUrl(homepage);
            //Maximize because selenium has trouble finding information clipped off screen
            driver.browserDriver.Manage().Window.Maximize();
        }


        /// <summary>
        /// waits for object on page to change to specificed value, or timeout
        /// </summary>
        /// <param name="desiredvalue">expected new value string</param>
        /// <param name="inspectedelement">By object that driver is inspecting</param>
        /// <param name="timeout">length of time to wait (in miliseconds) before returning false</param>
        /// <param name="driver">Firefox driver</param>
        /// <returns>bool true if text of webelement changed to expected value</returns>
        public static bool checkChangeElement(string desiredvalue, string inspectedelement, int timeout, ihi_testlib_selenium.Selenium driver)
        {
            bool success = false;
            int elapsed = 0;
            while ((!success) && (elapsed < timeout))
            {
                Thread.Sleep(1000);
                elapsed += 1000;
                //needed to trim strings because UI strings sometimes have leading spaces
                success = (desiredvalue.Trim() == (driver.browserDriver.FindElement(By.XPath(inspectedelement)).Text).Trim());
            }
            return success;
        }

        /// <summary>
        /// switch to UI clock to check if updating
        /// </summary>
        /// <param name="timeout">length of time method will wait before fails</param>
        /// <param name="driver">Firefox driver</param>
        /// <returns>true if clock is updating</returns>
        public static bool CheckUIUpdate(int timeout, ihi_testlib_selenium.Selenium driver)
        {
            bool success = false;
            int elapsed = 0;
            try
            {
                 driver.browserDriver.SwitchTo().DefaultContent();
                string initTime = driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.Time));

                while ((!success) && (elapsed < timeout))
                {
                    Thread.Sleep(1000);
                    elapsed += 1000;
                    success = (initTime != (driver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.Time))));
                }
                return success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        ///// <summary>
        ///// checks to see if element is located on selected page.  Overload includes if there is an altert window found, tries to close it.
        ///// </summary>
        ///// <param name="by">Element which is to be looked for</param>
        ///// <param name="driver">Firefox driver</param>
        ///// <returns>bool true if object found</returns>
        //public static bool IsElementPresent(By by, ihi_testlib_selenium.Selenium driver)
        //{
        //    try
        //    {
        //        driver.browserDriver.FindElement(by);
        //        return true;
        //    }
        //    catch (NoSuchElementException)
        //    {
        //        return false;
        //    }
        //    catch (UnhandledAlertException)
        //    {
        //        IAlert alert = driver.browserDriver.SwitchTo().Alert();
        //        Console.WriteLine("Alert Window Opened.");
        //        Console.WriteLine(alert.Text);
        //        Flash.FlashWindow(Process.GetCurrentProcess().MainWindowHandle);
        //        alert.Dismiss();
        //        return true;
        //    }
        //}

        ///// <summary>
        ///// checks to see if element is located on selected page.  Overload includes if there is an altert window found, tries to close it.
        ///// Overload includes results string for logging alert
        ///// </summary>
        ///// <param name="by">Element which is to be looked for</param>
        ///// <param name="driver">Firefox driver</param>
        ///// <returns>bool true if object found</returns>
        //public static bool IsElementPresent(By by, ihi_testlib_selenium.Selenium driver, ref string results)
        //{
        //    try
        //    {
        //        driver.browserDriver.FindElement(by);
        //        return true;
        //    }
        //    catch (NoSuchElementException)
        //    {
        //        return false;
        //    }
        //    catch (UnhandledAlertException)
        //    {

        //        IAlert alert = driver.browserDriver.SwitchTo().Alert();
        //        Console.WriteLine("Alert Window Opened.");
        //        Console.WriteLine(alert.Text);
        //        results += "  :" + alert.Text + " \r\n ";
        //        Flash.FlashWindow(Process.GetCurrentProcess().MainWindowHandle);
        //        alert.Dismiss();
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //        results += "  :" + ex.ToString() + " \r\n ";
        //        return false;
        //    }
        //}

      

        /// <summary>
        /// Checks the database to see if value updates to expected value, returns false if never does, query should be set to true when using real hardware
        /// </summary>
        /// <param name="desiredvalue"></param>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="timeout"></param>
        /// <param name="sshInfo"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool checkChangeElement(string desiredvalue, string table, string column, int timeout, SSHAndDatabaseDirectory sshInfo, int index = 1, bool usingHardware = false)
        {
            bool success = false;
            int elapsed = 0;

            while ((!success) && (elapsed < timeout))
            {
                Thread.Sleep(1000);
                elapsed += 1000;
                string valueFromDatabase;
                if (usingHardware)
                {
                    valueFromDatabase = SSH.ReadFromDCOpenClose(table, column, index, sshInfo.SSHIPAddress,
                        sshInfo.SSHUsername, sshInfo.SSHPassword);
                }
                else
                {
                    valueFromDatabase = SSH.ReadFromDatabaseOpenClose(table, column, index, sshInfo.SSHIPAddress,
                        sshInfo.SSHUsername, sshInfo.SSHPassword, sshInfo.SSHDatabaseDirectory);
                }

                valueFromDatabase = valueFromDatabase.Replace("\n", "");
                success = (desiredvalue == valueFromDatabase);
            }
            return success;
        }

        public static string CloseAlertAndGetItsText(ihi_testlib_selenium.Selenium driver)
        {
            if (CheckIfAlertIsActive(driver))
            {
                IAlert alert = driver.browserDriver.SwitchTo().Alert();
                string alertText = alert.Text;
                alert.Accept();
                return alertText;
            }
            return "";
        }



        /// <summary>
        /// Checks if alert is active on screen
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool CheckIfAlertIsActive(ihi_testlib_selenium.Selenium driver)
        {
            try
            {
                var alert = driver.browserDriver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        public static void SwitchToTab(ihi_testlib_selenium.Selenium driver, int tabNumber = 0)
        {
            var tabs = driver.browserDriver.WindowHandles;
            driver.browserDriver.SwitchTo().Window(tabs[tabNumber]);
        }

        //public static void RestartTestBackend(string ipaddress, string username, string password, string decsDirectory)
        //{
        //    var sshinfo = new SSHAndStartDECSDirectory(ipaddress, username, password, decsDirectory);
        //    SSH.StopDecs(sshinfo);
        //    SSH.StartDecs(DECSLaunchType.Test, sshinfo, 30000);
        //}
    }
}
