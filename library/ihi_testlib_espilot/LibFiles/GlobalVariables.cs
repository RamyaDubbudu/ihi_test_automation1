﻿/*==============================================================================
Name        : GlobalVariables.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Variables for used throughout the library stored in a one location for easy modability.
==============================================================================*/
using System;
using System.Configuration;

namespace ihi_testlib_espilot
{
    //general global variables
    public static class GlobalsGeneralStrings
    {
        //SSH Values
        public static string SSHhostname = ConfigurationManager.AppSettings["ESPilotIPAddress"].ToString();
        public static string SSHusername = ConfigurationManager.AppSettings["Username"].ToString();
        public static string SSHpassword = ConfigurationManager.AppSettings["Password"].ToString();
        // global for start homepage
        public static string homepage = String.Format(ConfigurationManager.AppSettings["Loginpage"], SSHhostname);
            //"https://10.40.116.100/ihi/IHI_UI_LOGIN.php"; // "https://192.168.1.100/ihi/IHI_UI_MAIN.php"; // "https://10.40.115.100/ihi/IHI_UI_LOGIN.php"; //   "https://172.16.1.100/ihi/IHI_UI_LOGIN.php"; //"https://192.168.1.100/ihi/IHI_UI_MAIN.php"; // 
        //local location
        //public static string DCDatabaseLocation = "Data Source=C:\\SVN\\Databases\\DECS_DC\\decs_dc.db;Version=3;";
        //VM Location
        public static string DCDatabaseLocation = @"Data Source=\\UBUNTU\decs\decs_dc.db;Version=3;";


        //DECS File Location on Ubuntu
        public static string DECSDirectory = "/usr/local/ihi-decs/bin/";
        public static string DECSDatabaseDirectory = "/var/run/ihi-decs/";
        public static string DECSDataDirectory = "/usr/local/ihi-decs/data/";
    }

    //login page global variables
    public static class GlobalsLoginPageStrings
    {
        // valid login credentials
        public static string validuser = "jim";
        public static string validpassword = "cleveland";
    }

    public static class DECSLoginCredientials
    {
        public static string UserName = ConfigurationManager.AppSettings["Username"].ToString();
        public static string Password = ConfigurationManager.AppSettings["Password"].ToString();
    }

    //error strings that may appear on the ui
    public static class Errormessages
    {
        //login fail error message
        public static string loginfailerror = "An invalid 'User Name' or 'Password' was entered.";
        public static string cameraErrorMessage = "Failed to Connect to Camera.";
    }

    public static class DAQNaviDetails
    {
        public static string DAQNaviName = "USB-4761,BID#0";
    }

    public static class TimingConstants
    {
        //length of time to wait for the UI to load after login in [milliseconds]
        
        //length of time to wait for the UI to load after database updated in [milliseconds]
        public static int UILoadAfterDatabaseChange = 2000;
        /// <summary>
        ///     Length of time to wait for a switch to reacquire after port has been turned on [milliseconds]
        /// </summary>
        public static int IPAddressReacquire = 10000;
        public static int UILoadWait = 5000;
        public static int PowerOffCheck = 5000;
        public static int CameraErrorTimeout = 6000;
        public static int ModeSwitch = 120000;
        public static int DECSLaunch = 10000;
        public static int EthernetReconnect = 5000;
        public static int EnableWait = 3000;
        /// <summary>
        /// Time require for ZC to fully boot up
        /// </summary>
        public static int ZCBootUp = 30000;
        /// <summary>
        /// Time required for DC to fully boot up
        /// </summary>
        public static int DCBootup = 60000;
        /// <summary>
        /// Time required for battery to fully boot up
        /// </summary>
        public static int BatteryBootup = 40000;

        public static int IOBootup = 10000;
        public static int SwitchBootup = 10000;
    }

    public static class DECS_DCArguements
    {
        public static string Debug = "-d ";
        public static string Test = "-t ";
    }

    public static class StartDECSArguements
    {
        public static string Cylindrical = "-cylindrical ";
        public static string Prismatic = "-prismatic ";
        public static string TwoSystem = "-lab ";
    }

    public static class DirectoryKnown
    {
        public static string DECS = "/usr/local/ihi-decs/bin/";
        public static string Databases = "/var/run/ihi-decs/";
        public static string UI = "/var/www/html/ihi/";
    }

    public static class IPAddressesKnown
    {
        public static string A123 = "192.168.1.100";    // Z1 with A123 Battery
        public static string LG = "192.168.1.120";    // Z2 with A123 Battery
        public static string A123ZC = "192.168.1.201";
        public static string LGZC = "192.168.1.202";
        public static string A123Switch = "192.168.1.157";
        public static string LGSwitch = "192.168.1.159";
        public static string LGPowerSwitch = "192.168.1.247";
        public static string A123PowerSiwtch = "192.168.1.248";
    }

    //public static class TeststandDefualts
    //{
    //    public static string browserSelection = "firefox";
    //    public static string sURLUI = @"https://192.168.1.245/RELEASE/IHI_UI_LOGIN.php";
    //    public static string sQADirPath = @"C:\SVN\QA";
    //    public static string sSSHUserName = GlobalsGeneralStrings.SSHusername;
    //    public static string sSSHPassword = GlobalsGeneralStrings.SSHpassword;
    //    public static string sSSHIPAddress = @"192.168.1.245";
    //    public static string DECSDirectory = DirectoryKnown.DECS;
    //    public static string DECSDatabaseDirectory = DirectoryKnown.Databases;
    //    public static string UILocation = @"";
    //    public static string netgearSwitchIp = IPAddressesKnown.A123;
    //}

    public enum Color
    {
        Red = 0,
        Blue = 1,
        Red_Green = 2
    }

    public enum Assets
    {
        zones,
        racks,
        inverters,
        cooling
    }

    public enum DECSLaunchType
    {
        Test,
        Service,
        Normal,
        TestWithoutTSwitch,
        Debug
    }

    /// <summary>
    ///     Ports for Netgear M4100-D12G managed switch
    /// </summary>
    public enum SwitchPort
    {
        DECS = 1,
        ZoneController = 2,
        PMU = 3,
        Inverter = 4,
        Battery = 5,
        IO = 6,
        CameraOne = 7,
        CameraTwo = 8,
        CameraThree = 9,
        CameraFour = 10,
        PortEleven = 11,
        OutsideNetwork = 12
    }

    public enum PowerSwitchPort
    {
        DECS = 1,
        ZoneController = 2,
        NetworkSwitch = 3,
        IO = 4,
        BatteryOne = 5,
        BatteryTwo = 6,
        BatteryThree = 7
    }

    public static class NetgearLoginInfo
    {
        public static string Login = "admin";
        public static string Password = "";
    }

    public static class APCLoginInfo
    {
        public static string Login = "apc";
        public static string Password = "apc";
    }
}