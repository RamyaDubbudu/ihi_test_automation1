﻿/*==============================================================================
Name        : IHI_PPD_ZMQ.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Driver created to request data and updates from the backend via ZMQ
==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public class IHI_PPD_ZMQ
    {

        /*==============================================================================
        Function Name : GetData_Chart

        Description: Gets 'Charting Data' from the 'Server'.

        Parameters
             TimeMins = time in minutes for length of data to collect
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Chart(int TimeMins, string IPAddress)
        {
            string format = "70050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_DATA_LOG + TimeMins.ToString();
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1043);
        }

        /*==============================================================================
        Function Name : GetData_Alarm

        Description: Gets 'Alarm Data' from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Alarm(string IPAddress)
        {
            string format = "70050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ALARM;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1043);
        }

        /*==============================================================================
        Function Name : SendAcknowledgeData_Alarm

        Description: Send Acknowledge Data for Alarm

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string SendAcknowledgeData_Alarm(string IPAddress)
        {
            string format = "70000" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ACKNOWLEDGE_ALARM + "1" + ", 0, Acknowledged";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1043);
        }

        /*==============================================================================
        Function Name : SendDismissData_Alarm

        Description: Send Dismiss Data for Alarm

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string SendDismissData_Alarm(string IPAddress)
        {
            string format = "70000" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_DISMISS_ALARM + "1" + ", 0";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1043);
        }

        /*==============================================================================
        Function Name : GetData_Event

        Description: Send Dismiss Data for Alarm

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Event(string IPAddress)
        {
            string format = "70050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_EVENT;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1043);
        }

        /*==============================================================================
        Function Name : SendAcknowledgeData_Event

        Description: Send Acknowledge Data for Events

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string SendAcknowledgeData_Event(string IPAddress)
        {
            string format = "70000" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ACKNOWLEDGE_EVENT + "1" + ", 0, Acknowledged";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1043);
        }

        /*==============================================================================
        Function Name : SendDismissData_Event

        Description: Send Dismiss Data for Events

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string SendDismissData_Event(string IPAddress)
        {
            string format = "70000" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_DISMISS_EVENT + "1" + ", 0";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1043);
        }

        /*==============================================================================
        Function Name : GetData_Zones_Enable

        Description: Enables 'Zones'.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Zones_Enable(int ZonesID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ZONE_DISABLED + ZonesID + ",1";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Zones_Disable

        Description: Enables 'Zones'.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Zones_Disable(int ZonesID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ZONE_DISABLED + ZonesID + ",0";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Battery_EnableAll

        Description: Enables Battery Racks by Zones.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Battery_EnableAll(int ZonesID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_ENABLED + ZonesID + ",0,1";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Battery_DisableAll

        Description: Disables Battery Racks by Zones.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Battery_DisableAll(int ZonesID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_DISABLED + ZonesID + ",0,0";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Battery_Enable

        Description: Enables Battery Racks by Zones + RackID.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Battery_Enable(int ZonesID, int BatteryID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_ENABLED + ZonesID + "," + BatteryID + ",1";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Battery_Disable

        Description: Disables Battery Racks by Zones + RackID.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Battery_Disable(int ZonesID, int BatteryID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_DISABLED + ZonesID + "," + BatteryID + ",0";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Inverters_Enable

        Description: Enables Inverters by Zones + InverterED.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Inverters_Enable(int ZonesID, int InvertersID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_ENABLED + ZonesID + "," + InvertersID + ",1";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Inverters_Disable

        Description: Enables Inverters Racks by Zones + InverterED.

        Parameters:
             IPAddress = IP Address of server
             ZonesID = Zone ID of the zone that will be enabled

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Inverters_Disable(int ZonesID, int InvertersID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_DISABLED + ZonesID + "," + InvertersID + ",0";
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_User

        Description: Gets 'User Data from the Server

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_User(string IPAddress)
        {
            string format = "70050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_USER_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Footer

        Description: Gets 'Footer Data' from the 'Server'. - Same as GetData_Site

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Footer(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_SITE;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Site

        Description: Gets 'Site' data from the 'Server'. - Same as GetData_Footer

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Site(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_SITE;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Zone

        Description: Gets 'Zone' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Zone(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ZONES_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Battery

        Description: Gets 'Battery' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Battery(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_RACKS_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Module

        Description: Gets 'Module' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Module(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_MODULES_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Inverter

        Description: Gets 'Inverter' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Inverter(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Cooler

        Description: Gets 'Cooler' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Cooler(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_COOLING_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_IO

        Description: Gets 'I/O' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_IO(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_IO_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Meter

        Description: Gets 'Meter' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Meter(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_METERS_INFO;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Mode

        Description: Gets 'Mode' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetData_Mode(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_AVAILABLE_MODES_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : Set_Mode

        Description: Gets 'Mode' data from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string Set_Mode(int Mode, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_MODE_DEMAND + "1," + Mode;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : Set_Demand

        Description: Sends a 'Demand Set' command to the 'Server'.

        Parameters:
             IPAddress = IP Address of server
             NewP = Value for p demand
             NewQ = Value for q demand

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string Set_Demand(int NewP, int NewQ, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_PQ_DEMAND + "1," + NewP + "," + NewQ;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetBatteryTypes

        Description: Gets the 'Battery' type list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetBatteryTypes(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_BATTERY_TYPES_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetStatusList_Availability

        Description: Gets the 'Availability' status list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetStatusList_Availability(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_AVAILABILITY_TYPES_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetStatusList_Battery

        Description: Gets the 'Battery' status list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetStatusList_Battery(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_STATUS_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetStatusList_Cooler

        Description: Gets the 'Cooler' status list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetStatusList_Cooler(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_COOLING_STATUS_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetStatusList_Enabled

        Description: Gets the 'Enabled' status list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetStatusList_Enabled(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ENABLED_STATUS_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetStatusList_Inverter

        Description: Gets the 'Inverter' status list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetStatusList_Inverter(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTER_STATUS_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetStatusList_Zone

        Description: Gets the 'Inverter' status list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server

         Return(s):
            The string response returned from the 'Server'.
        ==============================================================================*/
        public static string GetStatusList_Zone(string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ZONE_STATUS_LIST;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Specific_Users

        Description: Gets a specific 'Users' status list from the 'Server'.

        Parameters:
             IPAddress = IP Address of server
             UserID = User id of information which to be requestion

         Return(s):
            Class with user information of specified user
        ==============================================================================*/
        public static UserTable GetData_Specific_Users(int UserID, string IPAddress)
        {
            List<UserTable> userlist = IHI_ZMQ.UI__GetData_UserList(IPAddress);
            for (int i = 0; i < userlist.Count; i++)
            {
                if (userlist[i].user_id == UserID)
                    return userlist[i];
            }
            throw new System.Exception("UserID not found in list.");
        }

        /*==============================================================================
        Function Name : GetData_Delete_Users

        Description: Deletes a specific 'Users' from the 'Server'.

        Parameters:
             IPAddress = IP Address of server
             UserID = User id of information which to be requestion

         Return(s):
            Class with user information of specified user
        ==============================================================================*/
        public static string GetData_Delete_Users(int UserID, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_DELETE_USER + UserID;
            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : Add_Users

        Description: Adds a new 'User' from the 'Server'.

        Parameters:
             IPAddress = IP Address of server
             UserName = user name
             UserFullName = user full name
             Email = user email
             permissions = user permissions
             password = user password
 
         Return(s):
            Respose from server
        ==============================================================================*/
        public static string Add_Users(string UserName, string UserFullName, string Email, int permissions, string password, string IPAddress)
        {
            int MaxUserID = 0;
            List<UserTable> userlist = IHI_ZMQ.UI__GetData_UserList(IPAddress);
            foreach (UserTable user in userlist)
            {
                if (user.user_id > MaxUserID)
                    MaxUserID = user.user_id;
            }
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ADD_USER +
                (MaxUserID + 1) + "," + UserName + "," + UserFullName + "," + Email + "," + permissions + "," + Userpage.CalculateMD5Hash(password);

            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        /*==============================================================================
        Function Name : GetData_Update_Specific_Users

        Description: Edits a specific 'Users' from the 'Server'.

        Parameters:
             IPAddress = IP Address of server
             UserID = ID of the user being edited
             UserFullName = user full name
             Email = user email
             permissions = user permissions
             password = user password
 
         Return(s):
            Respose from server
        ==============================================================================*/
        public static string GetData_Update_Specific_Users(int UserID, string UserFullName, string Email, int permissions, string password, string IPAddress)
        {
            string format = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_UPDATE_USER_DATA +
                UserID + "," + UserFullName + "," + Email + "," + permissions + "," + Userpage.CalculateMD5Hash(password);

            return IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
        }

        public static List<Profile> GetProfileList(string IPAddress)
        {
            string format = "70050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_GET_PROFILE_LIST;
            string rawProfileList = IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
            rawProfileList = rawProfileList.Substring(rawProfileList.IndexOf("[\n\t"));
            rawProfileList = rawProfileList.Substring(0, rawProfileList.Length - 3);
            var splitProfile = rawProfileList.Split('}');
            List<Profile> profileList = new List<Profile>();
            for (int i = 0; i < splitProfile.Length-1; i++)
            {
                string currentProfile = splitProfile[i];

                string[] split = currentProfile.Split('\"');

                var index = int.Parse(split[1]);
                var profileName = split[3];
                var color = split[7];
                profileList.Add(new Profile(index, profileName, color));
            }

            return profileList;
        }

        public static string GetCurrentProfile(string IPAddress)
        {
            string format = "70050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_GET_CURRENT_PROFILE_NAME;
            string profileRaw = IHI_ZMQ.QueryZMQSeverMessage(format, IPAddress, 1041);
            string[] profileArray = profileRaw.Split('\"');
            return profileArray[5];
        }

    }
}
