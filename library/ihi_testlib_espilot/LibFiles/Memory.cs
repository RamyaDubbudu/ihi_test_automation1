﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ihi_testlib_espilot
{
    public static class Memory
    {

        //Checks memory usage on machine by name
        public static long CheckMemoryUsage(string processName, ref bool noerror, ref string results, int memorythreshold = 5000000)
        {
            try
            {
                Process[] targetProcess = Process.GetProcessesByName(processName);
                int proLen = targetProcess.Length;
                targetProcess[0].Refresh();
                long mem = targetProcess[0].WorkingSet64;
                mem /= 1000;
                if (proLen == 0)
                {
                    Console.WriteLine("The process does NOT exist or has exited...");
                    return 0;
                }
                //Console.WriteLine("The process status is: Running");
                Console.WriteLine("Memory used by Firefox: " + mem + "\n\n");

                if (mem > memorythreshold)
                {
                    noerror = false;
                    results += "  :Memory leak: " + mem.ToString() +"\r\n";
                }

                return mem;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\r\n" + ex.StackTrace + "\r\n" + ex.Source);
                noerror = true;
                return -1;
            }
        }
    }
}
