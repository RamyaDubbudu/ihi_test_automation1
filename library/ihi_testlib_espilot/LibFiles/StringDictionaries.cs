﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_espilot
{
    public class StringDictionaries
    {
        /// <summary>
        /// takes in a table and column from the database and checks if it is really a "magic number" for a string value
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="databaseValue"></param>
        /// <returns>expanded string</returns>
        public static string ConvertDatabaseString(string table, string column, string databaseValue)
        {
            string combindedTableColumn = table + "+" + column;

            databaseValue = databaseValue.Replace("\n", string.Empty);

            //This switch statement will go through and check if the value should be expected to be a string
            //lookup.  If it fits with an expected "magic number" table and column it will attempt the lookup.
            //If the value does not match any expected "magic number" (most likely the number is out of range)
            //The switch will return the input value.
                switch (combindedTableColumn)
                {
                    case "site+mode_actual":
                    case "site+mode_demand":
                        return Mode.ContainsKey(databaseValue) ? Mode[databaseValue] : databaseValue;
                    case "zones+available":
                    case "racks+available":
                    case "inverters+available":
                    case "cooling+available":
                        return AvailabilityStatus.ContainsKey(databaseValue) ? AvailabilityStatus[databaseValue] : databaseValue;
                    case "zones+enabled":
                    case "racks+enabled":
                    case "inverters+enabled":
                    case "cooling+enabled":
                        return EnabledStatus.ContainsKey(databaseValue) ? EnabledStatus[databaseValue] : databaseValue;
                    case "zones+state_actual":
                    case "zones+state_demand":
                        return ZoneStatus.ContainsKey(databaseValue) ? ZoneStatus[databaseValue] : databaseValue;
                    case "racks+rack_status":
                        return BatteryStatus.ContainsKey(databaseValue) ? BatteryStatus[databaseValue] : databaseValue;
                    case "inverters+state_actual":
                    case "inverters+state_demand":
                        return InverterStatus.ContainsKey(databaseValue) ? InverterStatus[databaseValue] : databaseValue;
                    case "cooling+online":
                        return CoolingStatus.ContainsKey(databaseValue) ? CoolingStatus[databaseValue] : databaseValue;
                    case "racks+type":
                        return BatteryType.ContainsKey(databaseValue) ? BatteryType[databaseValue] : databaseValue;
                    case "site+ups_status":
                    case "zones+ups_status":
                        return UPS_Status.ContainsKey(databaseValue) ? UPS_Status[databaseValue] : databaseValue;
                    default:
                        return databaseValue;
                }
        }

        /// <summary>
        /// Take table and column and value and checks if the value can be turned into a magic number that can be used by the database.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="column"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertStringDatabase(string table, string column, string value)
        {
            string returnValue = null;
            string combindedTableColumn = table + "+" + column;
                
                switch (combindedTableColumn)
                {
                    case "site+mode_actual":
                    case "site+mode_demand":
                        returnValue = Mode.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "zones+available":
                    case "racks+available":
                    case "inverters+available":
                    case "cooling+available":
                        returnValue = AvailabilityStatus.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "zones+enabled":
                    case "racks+enabled":
                    case "inverters+enabled":
                    case "cooling+enabled":
                        returnValue = EnabledStatus.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "zones+state_actual":
                    case "zones+state_demand":
                        returnValue = ZoneStatus.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "racks+rack_status":
                        returnValue = BatteryStatus.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "inverters+state_actual":
                    case "inverters+state_demand":
                        returnValue = InverterStatus.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "cooling+online":
                        returnValue = CoolingStatus.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "racks+type":
                        returnValue = BatteryType.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    case "site+ups_status":
                    case "zones+ups_status":
                        returnValue = UPS_Status.FirstOrDefault(x => x.Value == value).Key;
                        break;
                    default:
                        returnValue = value;
                        break;
            }

            //Checks to make sure first or defaults return value responses (key is found).  If not returns the input value.
            if(returnValue == null)
            {
                return value;
            }
            else
            {
                return returnValue;
            }
        }

        private static readonly Dictionary<string, string> BatteryType = new Dictionary<string, string>
        {
            { "0", "Unknown" },
            { "1", "Li-Ion Cylindrical" },
            { "2", "NEC LD" },
            { "3", "LG Chem JH2" }
        };

        private static readonly Dictionary<string, string> AvailabilityStatus = new Dictionary<string, string>
        {
            { "0", "Unavailable" },
            { "1", "Available" } 
        };

        private static readonly Dictionary<string, string> CoolingStatus = new Dictionary<string, string>
        {
            { "0", "Offline" },
            { "1", "Online" },
            { "2", "Fault" } 
        };

        private static readonly Dictionary<string, string> EnabledStatus = new Dictionary<string, string>
        {
            { "0", "Disabled" },
            { "1", "Enabled" } 
        };

        private static readonly Dictionary<string, string> InverterStatus = new Dictionary<string, string>
        {
            { "0", "Unknown" },
            { "1", "Unavailable" },
            { "2", "Shutdown" },
            { "3", "Startup" },
            { "4", "Online" },
            { "5", "Maintenance" },
            { "6", "Fault" }
        };

        private static readonly Dictionary<string, string> ZoneStatus = new Dictionary<string, string>
        {
            { "0", "Unknown" },
            { "1", "Shutdown" },
            { "2", "Waiting"},
            { "3", "Startup" },
            { "4", "Standby"},
            { "5", "Online" },
            { "6", "Fault" },
            { "7", "Maintenance" },
            { "8", "Unavailable" },
        };

        private static readonly Dictionary<string, string> ErrorStatus = new Dictionary<string, string>
        {
            { "0", "Unknown" },
        };

        private static readonly Dictionary<string, string> Mode = new Dictionary<string, string>
        {
            {"0", "Unknown"},
            {"1", "Shutdown"},
            {"2", "Waiting"},
            {"3", "Startup"},
            {"4", "Ready"},
            {"5", "Standby"},
            {"6", "Profile"},
            {"7", "Peak Shaving"},
            {"8", "Freq Support"},
            {"9", "Voltage Support"},
            {"10", "Load Leveling"},
            {"11", "Manual Mode"},
            {"12", "SOC Maintenance"},
            {"13", "Unused"},
            {"14", "ISO Dispatch"},
            {"15", "Peak Shift"},
            {"16", "Unused"},
            {"17", "Maintenance"},
            {"18", "Fault"}
         };

        private static readonly Dictionary<string, string> BatteryStatus = new Dictionary<string, string>
        {
            {"0", "Disconnected"},
            {"1", "Connected"},
            {"2", "Fault"}
        };

        public static readonly Dictionary<string, string> UserPermissions = new Dictionary<string, string>
        {
            {"0", "Please select a permission level"},
            {"1", "Read Only"},
            {"2", "Operator"},
            {"3", "Admin"},
            {"4", "IHI"}
        };

        private static readonly Dictionary<string, string> UPS_Status = new Dictionary<string, string>
        {
            {"0", "Loss of Comms"},
            {"1", "Online"},
            {"2", "On Battery" },
            {"3", "Low Battery"},
            {"4", "Alarm"}
        };

        public static readonly Dictionary<string, string> UITextColors = new Dictionary<string, string>
        {
            {"light green", "rgba(0, 128, 0, 1)"},
            {"green", "rgba(0, 100, 0, 1)"},
            {"red", "rgba(255, 0, 0, 1)"},
            {"white", "rgba(255, 255, 255, 1)"},
            {"blue", "rgba(51, 102, 153, 1)"},
            {"yellow", "rgba(136, 106, 8, 1)"},
            {"grey", "rgba(128, 128, 128, 1)"},
            {"brown", "rgba(165, 42, 42, 1)"},
            {"blue rack", "rgba(0, 80, 161, 1)"}
        };

        public static readonly Dictionary<string, string> UIBulletChartColors = new Dictionary<string, string>
        {
            {"yellow", "rgba(255, 255, 0, 1)"},
            {"white", "rgba(248, 236, 224, 1)"},
            {"red", "rgba(149, 14, 14, 1)"},
            {"black", "rgba(0, 0, 0, 1)"}
        };
    }
}
