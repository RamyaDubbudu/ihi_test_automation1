﻿using System.Data;
using System.IO;
using System;
using System.Text;
using Excel;
using System.Collections.Generic;
using System.Reflection;

namespace ihi_testlib_espilot
{
    public static class ExcelReadToStringArray
    {
        public static string[,] ReadRunFile(string relativepath, out int rowcount)
        {
            //find path based on where sequence file is located
            string parsedpath = relativepath.Substring(0, relativepath.IndexOf(".seq")) + " Run.xlsx";
            //Create file stream with excel file
            FileStream stream = File.Open(parsedpath, FileMode.Open, FileAccess.Read, FileShare.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            //Ignores fist row as this contain names
            excelReader.IsFirstRowAsColumnNames = true;
            DataSet results = excelReader.AsDataSet();
            rowcount = results.Tables[0].Rows.Count;
            int columncount = results.Tables[0].Columns.Count;

            //create 2D array based on how many rows and columns in Excel file
            string[,] outputstring = new string[rowcount, columncount];

            for (int i = 0; i < rowcount; i++)
            {
                for (int y = 0; y < columncount; y++)
                {
                    DataRow data = results.Tables[0].Rows[i];
                    string val = data[y].ToString();
                    outputstring[i, y] = val;
                }
            }
            //return 2D array with excel values
            return outputstring;
        }
    }
}
