﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

namespace ihi_testlib_espilot
{
    public static class IHI_ZMQ
    {
        /// <summary>
        /// Sends a message in ZMQ format and returns the response
        /// </summary>
        /// <param name="message">Message to send server</param>
        /// <param name="serverIP">IP address or hostname of server</param>
        /// <param name="port">port number to send IP address</param>
        /// <returns></returns>
        public static string QueryZMQSeverMessage(string message, string serverIP, int port)
        {
          
            using (var client = new RequestSocket())
            {
                try
                {
                    client.Connect(String.Format("tcp://{0}:{1}", serverIP, port));
                    client.SendFrame(message);
                    var msg = client.ReceiveFrameString();
                    return msg;
                    ////serverIP = "127.0.0.1";
                    ////Console.WriteLine(message);
                    ////client.Connect(String.Format("tcp://{0}", serverIP));
                    //client.Connect(String.Format("tcp://{0}:{1}", serverIP, port));
                    //client.SendReady("A").Send(message);
                    ////client.Send(message);
                    //var msg = new NetMQMessage();
                    //msg = client.ReceiveMessage();
                    //return msg.ToString();
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }

        /*==============================================================================
        Function Name : UI__GetData_SiteList

        Description: Gets 'Site Data' from server and returns a list of SiteTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of SiteTable classes (will usually be only one item long
        ==============================================================================*/
        public static List<SiteTable> UI__GetData_SiteList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_SITE;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            string output = test.Substring(test.IndexOf("site_id") - 6); 
            output = output.Replace("\n", "");
            output = output.Replace("\t", "");
            output = output.Remove(output.Length - 3);
            output = "[{" + output + "]";

            //Console.WriteLine(output);
            var results = JsonConvert.DeserializeObject<List<SiteTable>>(output).ToList();
            return results;
        }

        /*==============================================================================
        Function Name : UI__GetData_IOList

        Description: Gets 'IO data' from server and returns a list of IOTalbe classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of IO classes
        ==============================================================================*/
        public static List<IOTable> UI__GetData_IOList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_IO_INFO;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            string output = test.Substring(test.IndexOf("io_id") - 6);
            output = output.Replace("\n", "");
            output = output.Replace("\t", "");
            output = output.Remove(output.Length - 3);
            //Console.WriteLine(output);
            var results = JsonConvert.DeserializeObject<List<IOTable>>(output);
            return results;
        }

        /*==============================================================================
        Function Name : UI__GetData_MeterList

        Description: Gets 'Meter Data' from server and returns a list of MeterTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of MeterTable classes
        ==============================================================================*/
        public static List<MeterTable> UI__GetData_MeterList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_METERS_INFO;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            string output = test.Substring(test.IndexOf("meters_id") - 6);
            output = output.Replace("\n", "");
            output = output.Replace("\t", "");
            output = output.Remove(output.Length - 3);
            //Console.WriteLine(output);
            var results = JsonConvert.DeserializeObject<List<MeterTable>>(output);
            return results;
        }

        /*==============================================================================
        Function Name : UI__GetData_ZoneList

        Description: Gets 'Zone Data' from server and returns a list of ZoneTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of ZoneTable classes
        ==============================================================================*/
        public static List<ZoneTable> UI__GetData_ZoneList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_ZONES_INFO;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            if (test.IndexOf("zones_id") > 0)
            {
                string output = test.Substring(test.IndexOf("zones_id") - 6);
                output = output.Replace("\n", "");
                output = output.Replace("\t", "");
                output = output.Remove(output.Length - 3);
                output = output + "]";
                //Console.WriteLine(output);
                var results = JsonConvert.DeserializeObject<List<ZoneTable>>(output);
                return results;
            }
            else
                return null;
        }

        /*==============================================================================
        Function Name : UI__GetData_UserList

        Description: Gets 'User Data' from server and returns a list of UserTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of UserTable classes
        ==============================================================================*/
        public static List<UserTable> UI__GetData_UserList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__GET_DATA_USER;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            string output = test.Substring(test.IndexOf("user_id") - 6);
            output = output.Replace("\n", "");
            output = output.Replace("\t", "");
            output = output.Remove(output.Length - 3);
            //Console.WriteLine(output);
            var results = JsonConvert.DeserializeObject<List<UserTable>>(output);
            return results;
        }

        /*==============================================================================
        Function Name : UI__GetData_CoolingList

        Description: Gets 'Cooling Data' from server and returns a list of CoolingTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of CoolingTable classes
        ==============================================================================*/
        public static List<CoolingTable> UI__GetData_CoolingList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_COOLING_INFO;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            if (test.IndexOf("cooling_id") > 0)
            {
                string output = test.Substring(test.IndexOf("cooling_id") - 6);
                output = output.Replace("\n", "");
                output = output.Replace("\t", "");
                output = output.Remove(output.Length - 3) + "]";
                //Console.WriteLine(output);
                var results = JsonConvert.DeserializeObject<List<CoolingTable>>(output);
                return results;
            }
            else
                return null;
        }

        /*==============================================================================
        Function Name : UI__GetData_RackList

        Description: Gets 'Rack Data' from server and returns a list of RackTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of RackTable classes
        ==============================================================================*/
        public static List<RackTable> UI__GetData_RackList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_RACKS_INFO;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            string output = test.Substring(test.IndexOf("racks_id") - 6);
            output = output.Replace("\n", "");
            output = output.Replace("\t", "");
            output = output.Remove(output.Length - 3) + "]";
            
            //Console.WriteLine(output);
            var results = JsonConvert.DeserializeObject<List<RackTable>>(output);
            return results;
        }

        /*==============================================================================
        Function Name : UI__GetData_InverterList

        Description: Gets 'Inverter Data' from server and returns a list of InverterTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of InverterTable classes
        ==============================================================================*/
        public static List<InverterTable> UI__GetData_InverterList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_INFO;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            string output = test.Substring(test.IndexOf("inverters_id") - 6);
            output = output.Replace("\n", "");
            output = output.Replace("\t", "");
            output = output.Remove(output.Length - 3) + "]";
            //Console.WriteLine(output);
            var results = JsonConvert.DeserializeObject<List<InverterTable>>(output);
            return results;
        }

        /*==============================================================================
        Function Name : UI__GetData_ModuleList

        Description: Gets 'Module Data' from server and returns a list of ModuleTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of ModuleTable classes
        ==============================================================================*/
        public static List<ModuleTable> UI__GetData_ModuleList(string IPAddress)
        {
            string SendMessage = "10050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_MODULES_INFO;
            string test = QueryZMQSeverMessage(SendMessage, IPAddress, 1041);
            string output = test.Substring(test.IndexOf("modules_id") - 6);
            output = output.Replace("\n", "");
            output = output.Replace("\t", "");
            output = output.Remove(output.Length - 3);
            //Console.WriteLine(output);
            var results = JsonConvert.DeserializeObject<List<ModuleTable>>(output);
            return results;
        }

        /*==============================================================================
        Function Name : UI__GetData_EventsList

        Description: Gets 'Events' from server and returns a list of ModuleTable classes

        Parameters:
             IPAddress = IP Address of the server
             Port = optional port number for data requests (defaults to 1041)

         Return(s):
            List of Events classes
        ==============================================================================*/
        public static List<EventsTable> UI__GetData_EventsList(string IPAddress)
        {
            try
            {
                string sendMessage = "70050" + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__HASH_TABLE + ZMQFormatingRequest.IHI_PROTOCOL_ZMQ__TYPE__UI_EVENT;
                string test = QueryZMQSeverMessage(sendMessage, IPAddress, 1043);
                string output = test.Substring(test.IndexOf("event_id") - 6);
                output = output.Replace("\n", "");
                output = output.Replace("\t", "");
                output = output.Remove(output.Length - 3);
                //Console.WriteLine(output);
                var results = JsonConvert.DeserializeObject<List<EventsTable>>(output);
                return results;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
