﻿/*==============================================================================
Name        : LoginPage.cs
Author      : John O'Connell
Version     : 1.0
Copyright   : Copyright (c) 2015 IHI, Inc.  All Rights Reserved
Description : Class created to navigate easily around Login Page
==============================================================================*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
//using OpenQA.Selenium.Chrome;

namespace ihi_testlib_espilot
{

    public static class LoginPage
    {

        //login with custom name/password
        public static bool login(string username, string password, ihi_testlib_selenium.Selenium driver)
        {
            driver.FindHTMLControl(xPath: string.Format(GlobalElementsLoginPage.LoginUserBox)).Clear();
            driver.AddTextToTextbox(XPath: string.Format(GlobalElementsLoginPage.LoginUserBox), textToAdd: username);
            driver.FindHTMLControl(xPath: string.Format(GlobalElementsLoginPage.LoginPasswordBox)).Clear();
            driver.AddTextToTextbox(XPath: string.Format(GlobalElementsLoginPage.LoginPasswordBox), textToAdd: password);
            driver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsLoginPage.LoginButton));
            System.Threading.Thread.Sleep(1000);

            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsMainPage.DashboardTab))) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //login with valid credientials
        public static void loginvalid(ihi_testlib_selenium.Selenium driver)
        {
            login(GlobalsLoginPageStrings.validuser, GlobalsLoginPageStrings.validpassword, driver);
            System.Threading.Thread.Sleep(TimingConstants.UILoadWait);
        }

        /// <summary>
        /// Checks that the tab element is present which indicates the main page loaded.
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static bool CheckMainPageLoad (ihi_testlib_selenium.Selenium driver)
        {
             driver.browserDriver.SwitchTo().DefaultContent();
            if (driver.IsElementVisible(xPath: string.Format(GlobalElementsMainPage.DashboardTab)))
            {
                return true;
            }
            return false;
        }
    }
}
