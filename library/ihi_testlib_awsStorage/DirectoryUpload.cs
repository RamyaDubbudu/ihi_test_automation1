﻿// Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-s3-developer-guide/blob/master/LICENSE-SAMPLECODE.)

using Amazon.S3;
using Amazon.S3.Transfer;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Configuration;


namespace Amazon.DocSamples.S3
{
  public class DirUpload
    {
        static string Username = System.Environment.UserName;
        private const string bucketName = "ihies-pilot-test";
        private static string keyName = "qa/" + Username + "/" + ConfigurationManager.AppSettings["Component"].ToString() + "/" + DateTime.Today.ToString("yyyyMMdd");
        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;
        private static IAmazonS3 s3Client;
        private const string existingBucketName = "ihies-pilot-test";
        // The example uploads only .txt files.
        private const string wildCard = "*.*";

        public static void Main(string folderName, string keyName)
        {
            s3Client = new AmazonS3Client(bucketRegion);
            UploadDirAsync(folderName, keyName).Wait();
        }

  

        public static async Task UploadDirAsync(string folderName, string keyName)
        {
            try
            {
                var directoryTransferUtility =
                    new TransferUtility(s3Client);
                var request = new TransferUtilityUploadDirectoryRequest
                {
                    BucketName = existingBucketName,
                    Directory = folderName,
                    SearchOption = SearchOption.AllDirectories,
                    SearchPattern = wildCard,
                    KeyPrefix = keyName
                };

                await directoryTransferUtility.UploadDirectoryAsync(request);
                Console.WriteLine("Upload of the following directory is complete" + folderName);
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine(
                        "Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    "Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
        }

        public static async Task UploadFileAsync(string filePath)
        {
            try
            {
                var fileTransferUtility =
                    new TransferUtility(s3Client);

                //// Option 1. Upload a file. The file name is used as the object key name.
                //await fileTransferUtility.UploadAsync(filePath, bucketName);
                //Console.WriteLine("Upload 1 completed");

                // Option 2. Specify object key name explicitly.
                await fileTransferUtility.UploadAsync(filePath, bucketName, keyName);
                Console.WriteLine("Upload 2 completed");

                //// Option 3. Upload data from a type of System.IO.Stream.
                //using (var fileToUpload =
                //    new FileStream(filePath, FileMode.Open, FileAccess.Read))
                //{
                //    await fileTransferUtility.UploadAsync(fileToUpload,
                //                               bucketName, keyName);
                //}
                //Console.WriteLine("Upload 3 completed");

                //// Option 4. Specify advanced settings.
                //var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                //{
                //    BucketName = bucketName,
                //    FilePath = filePath,
                //    StorageClass = S3StorageClass.StandardInfrequentAccess,
                //    PartSize = 6291456, // 6 MB.
                //    Key = keyName,
                //    CannedACL = S3CannedACL.PublicRead
                //};
                //fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
                //fileTransferUtilityRequest.Metadata.Add("param2", "Value2");

                //await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
                //Console.WriteLine("Upload 4 completed");
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }

        }
    }
}