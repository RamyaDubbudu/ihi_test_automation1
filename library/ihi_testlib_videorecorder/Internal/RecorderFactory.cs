using System.IO;
using NUnit.Framework;
using System.Linq;
using ihi_testlib_videorecorder.Internal;
using System.Configuration;

namespace ihi_testlib_videorecorder.Internal
{
    public class RecorderFactory
    {

        private const string DefaultExtension = ".avi";
        private const string VideoFolderName = "Video";
        private readonly string _defaultOutputPath = ConfigurationManager.AppSettings["AutomationDiretory"].ToString()  + "\\XReports\\";
        private readonly VideoConfigurator _configurator;

        private RecorderFactory(VideoConfigurator configurator)
        {
            _configurator = configurator;
        }

        public static RecorderFactory Instance => new RecorderFactory(new VideoConfigurator());

        public Recorder Create(string videoPath)
        {
            var path = videoPath + DefaultExtension;

            var encoder = EncoderProvider.GetAvailableEncoder(_configurator);

            return new Recorder(path, encoder, _configurator);
        }

        private string RemoveForbiddenSymbols(string inputName)
        {
            var fixedName = inputName.Replace(".", "");

            return Path.GetInvalidFileNameChars().Aggregate(fixedName, (symbol, s) => symbol.Replace(s.ToString(), string.Empty));
        }

    }
}