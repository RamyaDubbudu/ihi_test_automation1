﻿using System.Windows.Forms;

namespace ihi_testlib_videorecorder.Internal
{
    public class VideoConfigurator
    {
        public static System.Drawing.Rectangle workingRectangle = Screen.PrimaryScreen.WorkingArea;
        public int Width { get; } = workingRectangle.Width;
        public int Height { get; } = workingRectangle.Height;
        public int FramePerSecond { get; set; } = 8;
        public int FrameCount { get; } = 0;
        public VideoQuality Quality { get; set; } = VideoQuality.Low;
       
        public VideoConfigurator() { }

        public VideoConfigurator(int fps, VideoQuality quality)
        {
            FramePerSecond = fps;
            Quality = quality;
        }     
    }
}
