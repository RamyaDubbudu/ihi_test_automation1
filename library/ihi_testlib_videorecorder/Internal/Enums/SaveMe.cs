﻿namespace ihi_testlib_videorecorder
{
    public enum SaveMe
    {
        Always,
        OnlyWhenFailed
    }
}
