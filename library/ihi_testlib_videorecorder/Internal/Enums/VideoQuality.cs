﻿namespace ihi_testlib_videorecorder
{
    public enum VideoQuality
    {
        Low = 50,
        Medium = 75,
        High = 100
    }
}
