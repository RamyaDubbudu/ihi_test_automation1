﻿using System;
using System.IO;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using ihi_testlib_videorecorder.Internal;

namespace ihi_testlib_videorecorder
{
   // [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class VideoAttribute 
    {
        public string Name { get; set; }
        public SaveMe Mode { get; set; } = SaveMe.Always;

        private bool _saveFailedOnly;
        private Recorder _recording;

        public VideoAttribute() { }

        public void StartRecording(string name, SaveMe mode)
        {
            SetVideoSavingMode(Mode);
            //var videoName = string.IsNullOrWhiteSpace(Name) ? name : Name.Trim();
            _recording = RecorderFactory.Instance.Create(videoPath:name);
            _recording.Start();            
        }

        public void StopRecording()
        {
            _recording?.Stop();
        }

        private void SetVideoSavingMode(SaveMe mode)
        {
            switch (mode)
            {
                case SaveMe.Always:
                {
                    _saveFailedOnly = false;
                    break;
                }
                case SaveMe.OnlyWhenFailed:
                {
                    _saveFailedOnly = true;
                    break;
                }
                default: throw new ArgumentException("Saving mode is not valid!");
            }
        }

        private void DeleteRelatedVideo()
        {
            try
            {
                File.Delete(_recording.OutputFilePath);
            }
            catch (IOException iox)
            {
                Console.WriteLine(iox.Message);
            }
        }

        //public class VideoAttribute : NUnitAttribute, ITestAction
        //{
        //    public string Name { get; set; }
        //    public SaveMe Mode { get; set; } = SaveMe.Always;

        //    private bool _saveFailedOnly;
        //    private Recorder _recording;

        //    public VideoAttribute() { }

        //    public ActionTargets Targets { get; } = ActionTargets.;

        //    public void BeforeTest(ITest test)
        //    {
        //        SetVideoSavingMode(Mode);
        //        var videoName = string.IsNullOrWhiteSpace(Name) ? test.Name : Name.Trim();
        //        _recording = RecorderFactory.Instance.Create(videoName);
        //        _recording.Start();
        //    }

        //    public void AfterTest(ITest test)
        //    {
        //        _recording?.Stop();

        //        if (_saveFailedOnly && Equals(TestContext.CurrentContext.Result.Outcome, ResultState.Success))
        //        {
        //            DeleteRelatedVideo();
        //        }
        //    }

        //    private void SetVideoSavingMode(SaveMe mode)
        //    {
        //        switch (mode)
        //        {
        //            case SaveMe.Always:
        //                {
        //                    _saveFailedOnly = false;
        //                    break;
        //                }
        //            case SaveMe.OnlyWhenFailed:
        //                {
        //                    _saveFailedOnly = true;
        //                    break;
        //                }
        //            default: throw new ArgumentException("Saving mode is not valid!");
        //        }
        //    }

        //    private void DeleteRelatedVideo()
        //    {
        //        try
        //        {
        //            File.Delete(_recording.OutputFilePath);
        //        }
        //        catch (IOException iox)
        //        {
        //            Console.WriteLine(iox.Message);
        //        }
        //    }
        }
}
