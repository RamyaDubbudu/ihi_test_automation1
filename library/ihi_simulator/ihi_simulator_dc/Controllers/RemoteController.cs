﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ihi_simulator_dc;
using ihi_simulator_dc.Models;
using Newtonsoft.Json;

namespace ihi_simulator_dc.Controllers
{
    public class RemoteInfoController : ApiController
    {
        // GET: api/Remote
        public Remote GetTable()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetRemote();
            rmote = rmote.Replace("[", "").Replace("]", "");
            Remote remoteData = JsonConvert.DeserializeObject<Remote>(rmote);
            return remoteData;
        }

        // GET: DCSim/Remote/GetESSSeq
        public string GetESSSeq()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("ess_sequence");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        public string GetISOSeq()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("iso_sequence");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }
        //DCSimAPI/Remote/GetPDemand
        public string GetPDemand()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("p_demand");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        //DCSimAPI/Remote/GetQDemand
        public string GetQDemand()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("q_demand");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        //DCSimAPI/Remote/GetISOModeDemand
        public string GetISOModeDemand()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("mode_demand");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        //DCSimAPI/Remote/GetSOCTarget
        public string GetSOCTarget()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("soc_target");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }
        //DCSimAPI/Remote/GetSOCPower
        public string GetSOCPower()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("soc_power_limit");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        //DCSimAPI/Remote/GetTargetP
        public string GetTargetP()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("target_p");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }
        //DCSimAPI/Remote/GetTargetQ
        public string GetTargetQ()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("target_q");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        //DCSimAPI/Remote/GetRampDownP
        public string GetRampDownP()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("ramp_down_p");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        //DCSimAPI/Remote/GetRampDownQ
        public string GetRampDownQ()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("ramp_down_q");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }
        //DCSimAPI/Remote/GetRampUpQ
        public string GetRampUpQ()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("ramp_up_q");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }

        //DCSimAPI/Remote/GetRampUpP
        public string GetRampUpP()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("ramp_up_p");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }
        //DCSimAPI/Remote/GetEnabled
        public string GetEnabled()
        {
            RemotePersistance rp = new RemotePersistance();
            string rmote = rp.GetByColumnname("enabled");
            rmote = rmote.Replace("[", "").Replace("]", "");
            return rmote;
        }
        

        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>


        // POST: api/Remote
        [HttpPut]
        public HttpResponseMessage UpdateMultipleColumns([FromBody]Remote value)
        {
            RemotePersistance rp = new RemotePersistance();
            bool recordexists = false;
            recordexists = rp.UpdateRemote(value);
            HttpResponseMessage responseMessage;
            if (recordexists)
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.Found);
            }
            else
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return responseMessage;
        }




/// <summary>
/// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// </summary>
/// <param name="id"></param>
/// <param name="value"></param>
/// <returns></returns>
        // PUT: api/Remote/5
        [HttpPut]
        public HttpResponseMessage UpdateRemoteData([FromBody]Remote value)
        {
            RemotePersistance rp = new RemotePersistance();
            bool recordexists = false;
            recordexists = rp.UpdateRemote(value);
            HttpResponseMessage responseMessage;
            if (recordexists)
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.Found);
            }
            else
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return responseMessage;
        }

        [HttpPut]
        public void UpdateRemoteByColumn([FromBody]string[] value)
        {
            RemotePersistance sp = new RemotePersistance();
            bool recordexists = false;
            recordexists = sp.UpdateRemoteColumn(value[0], value[1]);
        }


        // DELETE: api/Remote/5
        public void Delete(int id)
        {
        }
    }
}
