﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Net.Http;
using ihi_simulator_dc.Models;
using Newtonsoft.Json;

namespace ihi_simulator_dc.Controllers
{
    public class SiteInfoController : ApiController
    {

        // GET: DCSimAPI/Site/GetAll
        public Site GetTable()
        {
            SitePersistance sp = new SitePersistance();
            string site = sp.GetSite();
            site = site.Replace("[", "").Replace("]", "");
            Site siteData = JsonConvert.DeserializeObject<Site>(site);
            return siteData;
        }

        // GET: DCSimAPI/Site/GetName
        public string GetName()
        {
            SitePersistance sp = new SitePersistance();
            string siteName = sp.GetByColumnName("name").ToString();
            siteName = siteName.Replace("\"", "");
            return siteName;
        }

       

        // GET: DCSimAPI/Site/GetModeActual
        public int GetModeActual()
        {
            SitePersistance sp = new SitePersistance();
            int ModeActual = Convert.ToInt32(sp.GetByColumnName("mode_actual"));
            return ModeActual;
        }

        // GET: DCSimAPI/Site/GetKWNameplate
        public int GetKWNameplate()
        {
            SitePersistance sp = new SitePersistance();
            int KWNameplate = Convert.ToInt32(sp.GetByColumnName("kw_nameplate"));
            return KWNameplate;
        }

        // GET: DCSimAPI/Site/GetKVARNameplate
        public int GetKVARNameplate()
        {
            SitePersistance sp = new SitePersistance();
            int KVARNameplate = Convert.ToInt32(sp.GetByColumnName("kvar_nameplate"));
            return KVARNameplate;
        }

        // GET: DCSimAPI/Site/GetPMax
        public int GetPMax()
        {
            SitePersistance sp = new SitePersistance();
            int PMax = Convert.ToInt32(sp.GetByColumnName("p_max"));
            return PMax;
        }

        // GET: DCSimAPI/Site/GetPMin
        public int GetPMin()
        {
            SitePersistance sp = new SitePersistance();
            int PMin = Convert.ToInt32(sp.GetByColumnName("p_min"));
            return PMin;
        }
        // GET: DCSimAPI/Site/GetQMax
        public int GetQMax()
        {
            SitePersistance sp = new SitePersistance();
            int QMax = Convert.ToInt32(sp.GetByColumnName("q_max"));
            return QMax;
        }

        // GET: DCSimAPI/Site/GetQMin
        public int GetQMin()
        {
            SitePersistance sp = new SitePersistance();
            int QMin = Convert.ToInt32(sp.GetByColumnName("q_min"));
            return QMin;
        }

        // GET: DCSimAPI/Site/GetZonesOnline
        public int GetZonesOnline()
        {
            SitePersistance sp = new SitePersistance();
            int ZonesOnline = Convert.ToInt32(sp.GetByColumnName("zones_online"));
            return ZonesOnline;
        }

        // GET: DCSimAPI/Site/GetKWHMax
        public int GetKWHMax()
        {
            SitePersistance sp = new SitePersistance();
            int KWHMax = Convert.ToInt32(sp.GetByColumnName("kwh_max"));
            return KWHMax;
        }

        // GET: DCSimAPI/Site/GetSOC
        public int GetSOC()
        {
            SitePersistance sp = new SitePersistance();
            int SOC = Convert.ToInt32(sp.GetByColumnName("soc"));
            return SOC;
        }

        // GET: DCSimAPI/Site/GetKWHAvailable
        public int GetKWHAvailable()
        {
            SitePersistance sp = new SitePersistance();
            int KWHAvailable = Convert.ToInt32(sp.GetByColumnName("kwh_available"));
            return KWHAvailable;
        }

        // GET: DCSimAPI/Site/GetPActual
        public int GetPActual()
        {
            SitePersistance sp = new SitePersistance();
            int PActual = Convert.ToInt32(sp.GetByColumnName("p_actual"));
            return PActual;
        }

        // GET: DCSimAPI/Site/GetQActual
        public int GetQActual()
        {
            SitePersistance sp = new SitePersistance();
            int QActual = Convert.ToInt32(sp.GetByColumnName("q_actual"));
            return QActual;
        }

        // GET: DCSimAPI/Site/GetPActual
        public int GetPDemand()
        {
            SitePersistance sp = new SitePersistance();
            int PDemand = Convert.ToInt32(sp.GetByColumnName("p_demand"));
            return PDemand;
        }

        // GET: DCSimAPI/Site/GetQActual
        public int GetQDemand()
        {
            SitePersistance sp = new SitePersistance();
            int QDemand = Convert.ToInt32(sp.GetByColumnName("q_demand"));
            return QDemand;
        }
        // GET: DCSimAPI/Site/GetAlarmsTotal
        public int GetAlarmsTotal()
        {
            SitePersistance sp = new SitePersistance();
            int AlarmsTotal = Convert.ToInt32(sp.GetByColumnName("alarms_total"));
            return AlarmsTotal;
        }

        // GET: DCSimAPI/Site/GetAlarmsTotal
        public int GetZonesTotal()
        {
            SitePersistance sp = new SitePersistance();
            int ZonesTotal = Convert.ToInt32(sp.GetByColumnName("zones_total"));
            return ZonesTotal;
        }
        // GET: DCSimAPI/Site/GetZonesOnlineFlag
        public int GetZonesOnlineFlag()
        {
            SitePersistance sp = new SitePersistance();
            int ZonesOnlineFlag = Convert.ToInt32(sp.GetByColumnName("zones_online_flags"));
            return ZonesOnlineFlag;
        }

        // GET: DCSimAPI/Site/GetAvailableModeFlag
        public int GetAvailableModeFlag()
        {
            SitePersistance sp = new SitePersistance();
            int AvailableModeFlag = Convert.ToInt32(sp.GetByColumnName("available_mode_flags"));
            return AvailableModeFlag;
        }

        // GET: DCSimAPI/Site/GetSoh
        public int GetSoh()
        {
            SitePersistance sp = new SitePersistance();
            int Soh = Convert.ToInt32(sp.GetByColumnName("soh"));
            return Soh;
        }

        // GET: DCSimAPI/Site/GetVoltage
        public int GetVoltage()
        {
            SitePersistance sp = new SitePersistance();
            int Voltage = Convert.ToInt32(sp.GetByColumnName("voltage"));
            return Voltage;
        }

        // GET: DCSimAPI/Site/GetCurrent
        public int GetCurrent()
        {
            SitePersistance sp = new SitePersistance();
            int Current = Convert.ToInt32(sp.GetByColumnName("current"));
            return Current;
        }


        // GET: DCSimAPI/Site/GetSiteId
        public int GetId()
        {
            SitePersistance sp = new SitePersistance();
            int id = Convert.ToInt32(sp.GetByColumnName("rmon_site_id"));
            return id;
        }

        // GET: DCSimAPI/Site/GetSiteUTime
        public int GetUTime()
        {
            SitePersistance sp = new SitePersistance();
            int UTime = Convert.ToInt32(sp.GetByColumnName("site_utime"));
            return UTime;
        }


        // GET: DCSimAPI/Site/GetSiteURL
        public string GetURL()
        {
            SitePersistance sp = new SitePersistance();
            string url = sp.GetByColumnName("url").ToString();
            return url;
        }

        // GET: DCSimAPI/Site/GetDesc
        public string GetDesc()
        {
            SitePersistance sp = new SitePersistance();
            string desc = sp.GetByColumnName("description").ToString();
            return desc;
        }

        // GET: DCSimAPI/Site/GetLat
        public double GetLat()
        {
            SitePersistance sp = new SitePersistance();
            double lat = Convert.ToDouble(sp.GetByColumnName("lat"));
            return lat;
        }

        // GET: DCSimAPI/Site/GetLon
        public double GetLon()
        {
            SitePersistance sp = new SitePersistance();
            double lon = Convert.ToDouble(sp.GetByColumnName("lon"));
            return lon;
        }
        /// 
        /// <summary>
        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="value"></param>
        // POST: api/DCSim
        public void Post([FromBody]batteries value)
        {
            SitePersistance sp = new SitePersistance();
            long id;
            id = sp.saveBatteries(value);
        }




        /// <summary>
        /// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // PUT: api/DCSim/5
        [HttpPut]
        public HttpResponseMessage UpdateSiteData([FromBody] Site value)
        {
            SitePersistance sp = new SitePersistance();
            bool recordexists = false;
            recordexists = sp.UpdateSite(value);
            HttpResponseMessage responseMessage;
            if (recordexists)
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return responseMessage;

        }

        [HttpPut]
        public void UpdateSiteByColumn([FromBody] string[] value)
        {
            SitePersistance sp = new SitePersistance();
            bool recordexists = false;
            recordexists = sp.UpdateSiteColumn(value[0], value[1]);
            // HttpResponseMessage responseMessage;
            //if (recordexists)
            //{
            //    responseMessage = Request.CreateResponse(HttpStatusCode.Found,  value[0] + " value has been updated");
            //}
            //else
            //{
            //    responseMessage = Request.CreateResponse(HttpStatusCode.NotFound);
            //}
            //return responseMessage;

        }

        // DELETE: api/DCSim/5
        public void Delete(int id)
        {
        }
    }
}
