﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Net.Http;
using ihi_simulator_dc.Models;
using Newtonsoft.Json;

namespace ihi_simulator_dc.Controllers
{
    public class VMSiteController : ApiController
    {

        // GET: DCSimAPI/VMSite/GetAll
        public Site GetAll()
        {
            SqliteData sp = new SqliteData();
            string site = sp.GetSite();
            //site = site.Replace("[", "").Replace("]", "");
            Site siteData = JsonConvert.DeserializeObject<Site>(site);
            return siteData;
        }

        // GET: DCSimAPI/VMSite/GetName
        public string GetName()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("name");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetModeActual
        public string GetModeActual()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("mode_actual");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetKWNameplate
        public string GetKWNameplate()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("kw_nameplate");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetKVARNameplate
        public string GetKVARNameplate()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("kvar_nameplate");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetPMax
        public string GetPMax()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("p_max");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetPMin
        public string GetPMin()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("p_min");
            return siteName;
        }
        // GET: DCSimAPI/VMSite/GetQMax
        public string GetQMax()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("q_max");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetQMin
        public string GetQMin()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("q_min");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetZonesOnline
        public string GetZonesOnline()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("zones_online");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetKWHMax
        public string GetKWHMax()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("kwh_max");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetSOC
        public string GetSOC()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("soc");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetKWHAvailable
        public string GetKWHAvailable()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("kwh_available");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetPActual
        public string GetPActual()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("p_actual");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetQActual
        public string GetQActual()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("q_actual");
            return siteName;
        }


        // GET: DCSimAPI/VMSite/GetAlarmsTotal
        public string GetAlarmsTotal()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("alarms_total");
            return siteName;
        }


        // GET: DCSimAPI/VMSite/GetZonesOnlineFlag
        public string GetZonesOnlineFlag()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("zones_online_flags");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetAvailableModeFlag
        public string GetAvailableModeFlag()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("available_mode_flags");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetSoh
        public string GetSoh()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("soh");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetVoltage
        public string GetVoltage()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("voltage");
            return siteName;
        }

        // GET: DCSimAPI/VMSite/GetCurrent
        public string GetCurrent()
        {
            SqliteData sp = new SqliteData();
            string siteName = sp.GetByColumnName("current");
            return siteName;
        }

        /// <summary>
        /// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // PUT: api/DCSim/VMSite/UpdateSiteData
        [HttpPut]
        public HttpResponseMessage UpdateSiteData([FromBody] Site value)
        {
            SqliteData sp = new SqliteData();
            string recordexists = "";
            recordexists = sp.UpdateSite(value);
            HttpResponseMessage responseMessage;
            if (recordexists != "")
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return responseMessage;

        }

        //  PUT: api/DCSim/VMSite/UpdateSiteByColumn
        [HttpPut]
        public HttpResponseMessage UpdateSiteByColumn([FromBody] string[] value)
        {
            SqliteData sp = new SqliteData();
            string recordexists = "";
            recordexists = sp.UpdateSiteColumn(value[0], value[1]);
            HttpResponseMessage responseMessage;
            if (recordexists != "")
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.Found, value[0] + " value has been updated");
            }
            else
            {
                responseMessage = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return responseMessage;

        }

        // DELETE: api/DCSim/5
        public void Delete(int id)
        {
        }
    }
}
