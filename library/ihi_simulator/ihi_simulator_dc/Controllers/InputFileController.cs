﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ihi_simulator_dc.Controllers
{
    public class InputFileController : ApiController
    {

        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        // GET: api/InputFile/GetSite
        public object GetSite()
        {
            string allText = System.IO.File.ReadAllText(RepositoryPath + "\\DataFiles\\SiteData.json");
            object jsonObject = JsonConvert.DeserializeObject(allText);
            return jsonObject;
        }

        // GET: api/InputFile/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/InputFile
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/InputFile/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/InputFile/5
        public void Delete(int id)
        {
        }
    }
}
