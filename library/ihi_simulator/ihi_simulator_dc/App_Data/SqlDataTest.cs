﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using Newtonsoft.Json;
using Renci.SshNet;
using ihi_simulator_dc.Models;
using System.IO;
using System.Configuration;

namespace ihi_simulator_dc
{
    public class SqliteData
    {
       
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private string SSHIPAddress = ConfigurationManager.AppSettings["targetIpAddress"].ToString();
        private string SSHUsername = ConfigurationManager.AppSettings["Username"].ToString();
        private string SSHPassword = ConfigurationManager.AppSettings["Password"].ToString();


        public SqliteData()
        {
            try
            {
                var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
                ssh.Connect();
            }
            catch (SQLiteException)
            {
                Console.WriteLine("Cannot connect to SQLite");
            }
        }

        public string GetSite()
        {
            string table = "site";
            string column = "*";
            string filename = "testData";
            string valueFromDatabase = ReadFromDCOpenClose(table, column, SSHIPAddress,
                        SSHUsername, SSHPassword, filename: filename);
            valueFromDatabase = ConvertCsvFileToJsonObject("/" + filename);
            string JSONString = JsonConvert.SerializeObject(valueFromDatabase);
            return JSONString;
        }

        public string GetByColumnName(string columnName)
        {
            string table = "site";
            string valueFromDatabase = ReadFromDCOpenClose(table, columnName, SSHIPAddress,
                        SSHUsername, SSHPassword);
            return valueFromDatabase;
        }


        public string UpdateSite(Site site)
        {
            try
            {
                var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
                string DECSDirectory = "/usr/local/ihi-decs/data/";
                ssh.Connect();
                string sql = "UPDATE site SET rmon_site_id ='" + site.rmon_site_id + "', site_utime ='" + site.site_utime + "', url ='" + site.url + "', name ='" + site.name + "', description ='" + site.description + "', lat ='" + site.lat + "', lon ='" + site.lon + "', address ='" + site.address + "',city ='" + site.city + "',state_province ='" + site.state_province + "', post_code ='" + site.post_code + "', country ='" + site.country + "', mc_state ='" + site.mc_state + "', mc_substate ='" + site.mc_substate + "', lmp ='" + site.lmp + "', auto_restart ='" + site.auto_restart + "', last_mode ='" + site.last_mode + "', mode_actual ='" + site.mode_actual + "', profile_no ='" + site.profile_no + "', mode_demand ='" + site.mode_demand + "', available_mode_flags ='" + site.available_mode_flags + "', startup_timer ='" + site.startup_timer + "', p_demand ='" + site.p_demand + "', p_actual ='" + site.p_actual + "', q_demand ='" + site.q_demand + "', q_actual ='" + site.q_actual + "', p_max ='" + site.p_max + "', p_min ='" + site.p_min + "', q_max ='" + site.q_max + "', q_min ='" + site.q_min + "', soc ='" + site.soc + "', soh ='" + site.soh + "', kwh_nameplate ='" + site.kwh_nameplate + "', kwh_max ='" + site.kwh_max + "', kwh_available ='" + site.kwh_available + "', kw_nameplate ='" + site.kw_nameplate + "', kvar_nameplate ='" + site.kvar_nameplate + "', ref_freq ='" + site.ref_freq + "',ref_voltage ='" + site.ref_voltage + "',ref_v1 ='" + site.ref_v1 + "', ref_v2 ='" + site.ref_v2 + "', ref_v3 ='" + site.ref_v3 + "', ref_p ='" + site.ref_p + "', ref_q ='" + site.ref_q + "', ref_kva ='" + site.ref_kva + "', low_soc_warning ='" + site.low_soc_warning + "', low_soc_cutoff ='" + site.low_soc_cutoff + "', high_soc_warning ='" + site.high_soc_warning + "', high_soc_cutoff ='" + site.high_soc_cutoff + "', target_soc ='" + site.target_soc + "', target_freq ='" + site.target_freq + "', target_voltage ='" + site.target_voltage + "', target_p ='" + site.target_p + "', target_p_max ='" + site.target_p_max + "', target_p_min ='" + site.target_p_min + "', target_q ='" + site.target_q + "', target_q_max ='" + site.target_q_max + "', target_q_min ='" + site.target_q_min + "', zones_available ='" + site.zones_available + "', zones_enabled ='" + site.zones_enabled + "', zones_online ='" + site.zones_online + "', zones_online_flags ='" + site.zones_online_flags + "', zones_total ='" + site.zones_total + "', inverters_available ='" + site.inverters_available + "', inverters_enabled ='" + site.inverters_enabled + "', inverters_online ='" + site.inverters_online + "', inverters_total ='" + site.inverters_total + "', racks_available ='" + site.racks_available + "', racks_enabled = '" + site.racks_enabled + "', racks_online = '" + site.racks_online + "', racks_total = '" + site.racks_total + "', cooling_available = '" + site.cooling_available + "', cooling_enabled = '" + site.cooling_enabled + "', cooling_total = '" + site.cooling_total + "', cooling_online = '" + site.cooling_online + "', low_ambient_threshold = '" + site.low_ambient_threshold + "', high_ambient_threshold = '" + site.high_ambient_threshold + "', ambient_temp = '" + site.ambient_temp + "', rh = '" + site.rh + "', io_available = '" + site.io_available + "', io_total = '" + site.io_total + "', meters_available = '" + site.meters_available + "', meters_total = '" + site.meters_total + "', epo_active = '" + site.epo_active + "', rpo_active = '" + site.rpo_active + "', alarms_critical = '" + site.alarms_critical + "', alarms_major = '" + site.alarms_major + "', alarms_minor = '" + site.alarms_minor + "', alarms_informational = '" + site.alarms_informational + "', alarms_total = '" + site.alarms_total + "', notifications_total = '" + site.notifications_total + "', server_build = '" + site.server_build + "', server_latest_build = '" + site.server_latest_build + "', max_zone_temp_f = '" + site.max_zone_temp_f + "', max_zone_temp_zone = '" + site.max_zone_temp_zone + "', min_zone_temp_f = '" + site.min_zone_temp_f + "', min_zone_temp_zone = '" + site.min_zone_temp_zone + "', max_rack_temp_f = '" + site.max_rack_temp_f + "', max_rack_temp_rack = '" + site.max_rack_temp_rack + "', min_rack_temp_f = '" + site.min_rack_temp_f + "', min_rack_temp_rack = '" + site.min_rack_temp_rack + "', kwh_throughput = '" + site.kwh_throughput + "', total_runtime = '" + site.total_runtime + "', current_runtime = '" + site.current_runtime + "', ui_flags = '" + site.ui_flags + "', ui_build = '" + site.ui_build + "', ui_latest_build = '" + site.ui_latest_build + "', instance_id = '" + site.instance_id + "', ups_status = '" + site.ups_status + "', ups_load_pct = '" + site.ups_load_pct + "', ups_soc = '" + site.ups_soc + "', ups_remaining = '" + site.ups_remaining + "', help_url = '" + site.help_url + "', ups_url = '" + site.ups_url + "', operator_url = '" + site.operator_url + "', logo_url = '" + site.logo_url + "', comms_type = '" + site.comms_type + "', voltage = '" + site.voltage + "', current = '" + site.current + "' WHERE NAME = '" + site.name + "'";
                return CommandThroughQuery(sql, ssh, SSHIPAddress, DECSDirectory);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return "";
            }
        }

        public string UpdateSiteColumn(string columnToUpdate, string valueToUpdate)
        {
            string table = "site";
            string column = "*";
            UpdateToDCOpenClose(valueToUpdate, table, columnToUpdate, SSHIPAddress, SSHUsername, SSHPassword);
            System.Threading.Thread.Sleep(2000);
            string valueFromDatabase = ReadFromDCOpenClose(table, column, SSHIPAddress,
                        SSHUsername, SSHPassword);
            string JSONString = JsonConvert.SerializeObject(valueFromDatabase);
            return JSONString;
        }

        public static string CommandThroughQuery(string command, SshClient ssh, string ipAddress, string decsLocation, bool removeLineFeed = true, string filename = "", bool withHeader = false)
        {
            var formattedForSSH = "";
            if (!ssh.IsConnected)
                ssh.Connect();
            if (withHeader == true)
            {
                formattedForSSH = string.Format("sqlite3 -line -header -csv {0}decs_dc.tep2.db \"{1}\"", decsLocation, command);
            }
            else if (withHeader == false)
            {
                formattedForSSH = string.Format("sqlite3 -line -csv {0}decs_dc.tep2.db \"{1}\"", decsLocation, command, filename);
            }
            var response = ssh.RunCommand(formattedForSSH).Result;

            ssh.Disconnect();

            if (removeLineFeed)
                return response.Replace("\n", "");

            return response;
        }


        public static string ReadFromDCOpenClose(string table, string column, string SSHIPAddress,
            string SSHUsername, string SSHPassword, string DECSDirectory = "/usr/local/ihi-decs/data/", string filename = "", bool withHeader = false)
        {
            var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
            ssh.Connect();
            string sql = string.Format("SELECT {0} FROM {1} ", column, table);
            return CommandThroughQuery(sql, ssh, SSHIPAddress, DECSDirectory, filename: filename, withHeader:withHeader);
        }

        public static void UpdateToDCOpenClose(string SSHUpdateValue, string table, string column, string SSHIPAddress, string SSHUsername, string SSHPassword, string DECSDirectory = "/usr/local/ihi-decs/bin/")
        {
            var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
            string SSHCommand;
            try
            {
                SSHCommand = string.Format("UPDATE {0} SET {1}='{2}'", table, column, SSHUpdateValue);
            }
            catch
            {
                //user_id is the only column singular where the table is not
                SSHCommand = string.Format("UPDATE {0} SET {1}='{2}'", table, column, SSHUpdateValue);
            }
            CommandThroughQuery(SSHCommand, ssh, SSHIPAddress, DECSDirectory);
        }

        public string ConvertCsvFileToJsonObject(string path)
        {
            using (SshClient client = new SshClient(SSHIPAddress, SSHUsername, SSHPassword))
            {
                client.Connect();

                using (Stream localFile = File.Create("/home/decs/testData"))
                {
                    //client.dow(remoteFilePath, localFile);
                }
            }
            var ssh = new SshClient(SSHIPAddress, SSHUsername, SSHPassword);
            ssh.Connect();
            path = "\\192.168.112.164:\\home\\decs\\testData";
           // path = "/home/decs/" + path;
           // string result = ssh.RunCommand("cat /home/decs/" + path).Result;
            var csv = new List<string[]>();
            var lines = File.ReadAllLines(path);

            foreach (string line in lines)
                csv.Add(line.Split(','));

            var properties = lines[0].Split(',');

            var listObjResult = new List<Dictionary<string, string>>();

            for (int i = 1; i < lines.Length; i++)
            {
                var objResult = new Dictionary<string, string>();
                for (int j = 0; j < properties.Length; j++)
                    objResult.Add(properties[j], csv[i][j]);

                listObjResult.Add(objResult);
            }

            return JsonConvert.SerializeObject(listObjResult);
        }
    }
    }
