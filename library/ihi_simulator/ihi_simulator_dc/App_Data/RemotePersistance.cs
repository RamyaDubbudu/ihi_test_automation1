﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.Data;
using ihi_simulator_dc.Models;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;

namespace ihi_simulator_dc
{
    public class RemotePersistance
    {

        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);



        public RemotePersistance()
        {
            try
            {
                string sqlDbLoc = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["SqliteDbName"].ToString();
                //string sqlDbLoc = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["SqliteDbName"].ToString();
                sql_con = new SQLiteConnection(@"Data Source=" + sqlDbLoc);
                sql_con.Open();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex);
            }

        }

        public string GetRemote()
        {
            string sqlstrg = "select * from Remote";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlreader);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(dataTable);
            return JSONString;
        }

        public string GetByColumnname(string colummnname)
        {
            string sqlstrg = "select " + colummnname + " from Remote";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlreader);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(dataTable);
            return JSONString;
        }

        public bool UpdateRemote(Remote remote)
        {
            string sqlstrg = "select * from Remote";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            if (sqlreader.Read())

            {
                sqlreader.Close();
                sqlstrg = "UPDATE remote SET remote_id ='" + remote.remote_id + "',ess_sequence ='" + remote.ess_sequence + "',comms_timeout ='" + remote.comms_timeout + "',ignore_remote ='" + remote.ignore_remote + "',iso_sequence ='" + remote.iso_sequence + "',p_demand ='" + remote.p_demand + "',q_demand ='" + remote.q_demand + "',mode_demand ='" + remote.mode_demand + "',soc_target ='" + remote.soc_target + "',soc_power_limit ='" + remote.soc_power_limit + "',target_p ='" + remote.target_p + "',target_q ='" + remote.target_q + "',ramp_up_p ='" + remote.ramp_up_p + "',ramp_down_p ='" + remote.ramp_down_p + "',ramp_up_q ='" + remote.ramp_up_q + "',ramp_down_q ='" + remote.ramp_down_q + "',enabled ='" + remote.enabled + "'";
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = sqlstrg;
                sql_cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool UpdateRemoteColumn(string columnToUpdate, string valueToUpdate)
        {
            string sqlstrg = "select * from Remote ";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            if (sqlreader.Read())

            {
                sqlreader.Close();
                sqlstrg = "UPDATE remote SET " + columnToUpdate + " ='" + valueToUpdate + "'";
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = sqlstrg;
                sql_cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public string UpdateRemoteAndReturntable(string columnname, string message)
        {
            string sqlstrg = "";
            sqlstrg = "UPDATE remote SET " + columnname + " =  '" + message + "'";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            sqlstrg = "select * from remote";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();

            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlreader);

            string JSONString = JsonConvert.SerializeObject(dataTable);
            return JSONString;
        }

    }
}