﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.Data;
using ihi_simulator_dc.Models;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Renci.SshNet;
using System.Configuration;
using System.IO;

namespace ihi_simulator_dc
{
    public class SitePersistance
    {

        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
     



        public SitePersistance()
        {
            try
            {
                string sqlDbLoc = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["SqliteDbName"].ToString();
               //  string sqlDbLoc = (new FileInfo(AppDomain.CurrentDomain.BaseDirectory)).Directory.Parent + ConfigurationManager.AppSettings["SqliteDbName"].ToString();
                sql_con = new SQLiteConnection(@"Data Source=" + sqlDbLoc);
                sql_con.Open();
            }
            catch (SQLiteException)
            {

            }

        }

        public string GetSite()
        {
            string sqlstrg = "select * from Site";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlreader);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(dataTable);
            return JSONString;
        }


        public object GetByColumnName(string columnName)
        {
            var value = "";
            object columnValue = "";
            string sqlstrg = "select " + columnName + " from Site";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            Type v = sqlreader.GetFieldType(0);
            while (sqlreader.Read())
            {
                if (sqlreader.GetFieldType(0) == typeof(string))
                {
                    value = sqlreader.GetString(0);
                    columnValue = value.ToString();
                }
                else if (sqlreader.GetFieldType(0) == typeof(Int32))
                {
                  int value1 = sqlreader.GetInt32(0);
                    columnValue = value1;
                }
                 else if (sqlreader.GetFieldType(0) == typeof(double))
                {
                    double value2 = sqlreader.GetDouble(0);
                    columnValue = value2;
                }
            }
           
            return columnValue;
        }


       

        public bool UpdateSite ( Site site)
        {
            string sqlstrg = "select * from Site";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            if (sqlreader.Read())
            
            {
                sqlreader.Close();
                sqlstrg = "UPDATE site SET rmon_site_id ='" + site.rmon_site_id + "', site_utime ='" + site.site_utime + "', url ='" + site.url + "', name ='" + site.name + "', description ='" + site.description + "', lat ='" + site.lat + "', lon ='" + site.lon + "', address ='" + site.address + "',city ='" + site.city + "',state_province ='" + site.state_province + "', post_code ='" + site.post_code + "', country ='" + site.country + "', mc_state ='" + site.mc_state + "', mc_substate ='" + site.mc_substate + "', lmp ='" + site.lmp + "', auto_restart ='" + site.auto_restart + "', last_mode ='" + site.last_mode + "', mode_actual ='" + site.mode_actual + "', profile_no ='" + site.profile_no + "', mode_demand ='" + site.mode_demand + "', available_mode_flags ='" + site.available_mode_flags + "', startup_timer ='" + site.startup_timer + "', p_demand ='" + site.p_demand + "', p_actual ='" + site.p_actual + "', q_demand ='" + site.q_demand + "', q_actual ='" + site.q_actual + "', p_max ='" + site.p_max + "', p_min ='" + site.p_min + "', q_max ='" + site.q_max + "', q_min ='" + site.q_min + "', soc ='" + site.soc + "', soh ='" + site.soh + "', kwh_nameplate ='" + site.kwh_nameplate + "', kwh_max ='" + site.kwh_max + "', kwh_available ='" + site.kwh_available + "', kw_nameplate ='" + site.kw_nameplate + "', kvar_nameplate ='" + site.kvar_nameplate + "', ref_freq ='" + site.ref_freq + "',ref_voltage ='" + site.ref_voltage + "',ref_v1 ='" + site.ref_v1 + "', ref_v2 ='" + site.ref_v2 + "', ref_v3 ='" + site.ref_v3 + "', ref_p ='" + site.ref_p + "', ref_q ='" + site.ref_q + "', ref_kva ='" + site.ref_kva + "', low_soc_warning ='" + site.low_soc_warning + "', low_soc_cutoff ='" + site.low_soc_cutoff + "', high_soc_warning ='" + site.high_soc_warning + "', high_soc_cutoff ='" + site.high_soc_cutoff + "', target_soc ='" + site.target_soc + "', target_freq ='" + site.target_freq + "', target_voltage ='" + site.target_voltage + "', target_p ='" + site.target_p + "', target_p_max ='" + site.target_p_max + "', target_p_min ='" + site.target_p_min + "', target_q ='" + site.target_q + "', target_q_max ='" + site.target_q_max + "', target_q_min ='" + site.target_q_min + "', zones_available ='" + site.zones_available + "', zones_enabled ='" + site.zones_enabled + "', zones_online ='" + site.zones_online + "', zones_online_flags ='" + site.zones_online_flags + "', zones_total ='" + site.zones_total + "', inverters_available ='" + site.inverters_available + "', inverters_enabled ='" + site.inverters_enabled + "', inverters_online ='" + site.inverters_online + "', inverters_total ='" + site.inverters_total + "', racks_available ='" + site.racks_available + "', racks_enabled = '" + site.racks_enabled + "', racks_online = '" + site.racks_online + "', racks_total = '" + site.racks_total + "', cooling_available = '" + site.cooling_available + "', cooling_enabled = '" + site.cooling_enabled + "', cooling_total = '" + site.cooling_total + "', cooling_online = '" + site.cooling_online + "', low_ambient_threshold = '" + site.low_ambient_threshold + "', high_ambient_threshold = '" + site.high_ambient_threshold + "', ambient_temp = '" + site.ambient_temp + "', rh = '" + site.rh + "', io_available = '" + site.io_available + "', io_total = '" + site.io_total + "', meters_available = '" + site.meters_available + "', meters_total = '" + site.meters_total + "', epo_active = '" + site.epo_active + "', rpo_active = '" + site.rpo_active + "', alarms_critical = '" + site.alarms_critical + "', alarms_major = '" + site.alarms_major + "', alarms_minor = '" + site.alarms_minor + "', alarms_informational = '" + site.alarms_informational + "', alarms_total = '" + site.alarms_total + "', notifications_total = '" + site.notifications_total + "', server_build = '" + site.server_build + "', server_latest_build = '" + site.server_latest_build + "', max_zone_temp_f = '" + site.max_zone_temp_f + "', max_zone_temp_zone = '" + site.max_zone_temp_zone + "', min_zone_temp_f = '" + site.min_zone_temp_f + "', min_zone_temp_zone = '" + site.min_zone_temp_zone + "', max_rack_temp_f = '" + site.max_rack_temp_f + "', max_rack_temp_rack = '" + site.max_rack_temp_rack + "', min_rack_temp_f = '" + site.min_rack_temp_f + "', min_rack_temp_rack = '" + site.min_rack_temp_rack + "', kwh_throughput = '" + site.kwh_throughput + "', total_runtime = '" + site.total_runtime + "', current_runtime = '" + site.current_runtime + "', ui_flags = '" + site.ui_flags + "', ui_build = '" + site.ui_build + "', ui_latest_build = '" + site.ui_latest_build + "', instance_id = '" + site.instance_id + "', ups_status = '" + site.ups_status + "', ups_load_pct = '" + site.ups_load_pct + "', ups_soc = '" + site.ups_soc + "', ups_remaining = '" + site.ups_remaining + "', help_url = '" + site.help_url + "', ups_url = '" + site.ups_url + "', operator_url = '" + site.operator_url + "', logo_url = '" + site.logo_url + "', comms_type = '" + site.comms_type + "', voltage = '" + site.voltage + "', current = '" + site.current + "' WHERE NAME = '" + site.name + "'";
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = sqlstrg;
                sql_cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool UpdateSiteColumn(string columnToUpdate, string valueToUpdate)
        {
            string sqlstrg = "select * from Site ";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlreader = sql_cmd.ExecuteReader();
            if (sqlreader.Read())

            {
                sqlreader.Close();
                sqlstrg = "UPDATE site SET " + columnToUpdate + " ='" + valueToUpdate + "'";
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = sqlstrg;
                sql_cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public long saveBatteries (batteries battery)
        {
            batteries battry = new batteries();
            SQLiteDataReader sqlreader = null;
            string sqlstrg = "select * from batteries order by battery_type desc";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            sqlreader = sql_cmd.ExecuteReader();
            if (sqlreader.Read())
            {
                battry.battery_type = sqlreader.GetInt16(0);
            }
            battery.battery_type = battry.battery_type + 1;
            sqlstrg = "INSERT INTO batteries (battery_type, manufacturer,description, c_rate,max_current,min_voltage,max_voltage, kwh_nameplate,low_t_warning, high_t_warning,low_t_cutoff, high_t_cutoff, module_ct,provides_limits, supports_rejuvenation, connector_name, image_filename ) VALUES('"
               + battery.battery_type + "','" +  battery.manufacturer + "','" + battery.description + "','" + battery.c_rate + "','" + battery.max_current + "','" + battery.min_voltage + "','" + battery.max_voltage + "','" + battery.kwh_nameplate + "','" + battery.low_t_warning + "','" + battery.high_t_warning
                + "','" + battery.low_t_cutoff + "','" + battery.high_t_cutoff + "','" + battery.module_ct + "','" + battery.provides_limits + "','" + battery.supports_rejuvenation + "','" + battery.connector_name + "','" + battery.image_filename + "')";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = sqlstrg;
            sql_cmd.ExecuteNonQuery();
            SQLiteDataReader sqlrdr = null;
            sqlrdr = sql_cmd.ExecuteReader();
            long id = Convert.ToInt64(sqlrdr.GetFloat(0));
            return id;
        }


    }
}