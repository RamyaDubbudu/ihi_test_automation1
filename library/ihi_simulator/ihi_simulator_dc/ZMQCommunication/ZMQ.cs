﻿using ihi_simulator_dc;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_simulator_dc
{
    public static class ZMQ
    {
        public static void ZMQResponse(NetMQSocket responseSocket)
        {
            string strResponse = "";

            RemotePersistance remote = new RemotePersistance();
            Console.WriteLine("responseSocket : response opened '{0}'");
            var message = responseSocket.ReceiveFrameString();
            Console.WriteLine("responseSocket : Server Received '{0}'", message);

            // parse the message to get just message type
            message = message.Substring(13);
            string seqno = message.Substring(0, 3); //Get seqno

            if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_SEQNO == seqno)
            {
                // Parse for the iso seq number
                var iso_seqno = message.Substring(3, message.Length - 3); // message.Substring(3);
                strResponse = remote.UpdateRemoteAndReturntable("iso_sequence", iso_seqno);
                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_SEQNO'");
            }
            else if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_PQ_DEMAND == seqno)
            {
                message = message.Substring(3, message.Length - 3);
                string[] values = SplitFunction(message);

                if (values[0].Length != 0)
                {
                    strResponse = remote.UpdateRemoteAndReturntable("p_demand", values[0]);
                }

                if (values[1].Length != 0)
                {
                    strResponse = remote.UpdateRemoteAndReturntable("q_demand", values[1]);
                }

                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_PQ_DEMAND'");
            }
            else if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_MODE_DEMAND == seqno)
            {
                var mode_Demand = message.Substring(3, message.Length - 3); // message.Substring(3);
                strResponse = remote.UpdateRemoteAndReturntable("mode_demand", mode_Demand);
                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_MODE_DEMAND'");
            }
            else if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_SOC_TARGET == seqno)
            {
                var soc_target = message.Substring(3, message.Length - 3); // message.Substring(3);
                strResponse = remote.UpdateRemoteAndReturntable("soc_target", soc_target);
                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_SOC_TARGET'");
            }
            else if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_SOC_POWER == seqno)
            {
                var soc_power_limit = message.Substring(3, message.Length - 3); // message.Substring(3);
                strResponse = remote.UpdateRemoteAndReturntable("soc_power_limit", soc_power_limit);
                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_SOC_POWER'");
            }
            else if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_MODE_PQ_TARGETS == seqno)
            {
                message = message.Substring(3, message.Length - 3); // message.Substring(5);

                var target_p = message.Split(',').First();
                strResponse = remote.UpdateRemoteAndReturntable("target_p", target_p);

                var target_q = message.Split(',').Last();
                strResponse = remote.UpdateRemoteAndReturntable("target_q", target_q);

                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_PQ_TARGETS'");
            }
            else if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_ENABLE == seqno)
            {
                var enabled = message.Substring(3, message.Length - 3); // message.Substring(3);
                strResponse = remote.UpdateRemoteAndReturntable("enabled", enabled);
                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_ENABLE'");
            }
            else if (ZMQFormating.IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_RAMP_RATES == seqno)
            {
                message = message.Substring(3, message.Length - 3); // message.Substring(message.IndexOf(',') + 1);
                string[] values = SplitFunction(message);

                // values = values.Select(str => { if (str.Length == 0) str = null; return str; }).ToArray();
                if (values[0].Length != 0)
                    strResponse = remote.UpdateRemoteAndReturntable("ramp_up_p", values[0]);

                if (values[1].Length != 0)
                    strResponse = remote.UpdateRemoteAndReturntable("ramp_down_p", values[1]);

                if (values[2].Length != 0)
                    strResponse = remote.UpdateRemoteAndReturntable("ramp_up_q", values[2]);

                if (values[3].Length != 0)
                    strResponse = remote.UpdateRemoteAndReturntable("ramp_down_q", values[3]);

                Console.WriteLine("responseSocket Sending 'Sending Response Back with ISO_RAMP_RATES'");
            }
            else
            {
                strResponse = "Unhandled message type";
                Console.WriteLine(strResponse);
            }

            // Send a Response regardless of the Message Type
            responseSocket.SendFrame(strResponse);
            Console.WriteLine("Response = " + strResponse);
        }

        public static string[] SplitFunction(string text)
        {
            char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
            string[] words = text.Split(delimiterChars, StringSplitOptions.None);
            return words;
        }

        public static void ZMQPublisher()
        {
            SitePersistance site = new SitePersistance();

            using (var pubSocket = new PublisherSocket())
            {
                pubSocket.Options.SendHighWatermark = 1000;
                pubSocket.Bind("tcp://*:1042");
                Console.WriteLine("Publisher socket binding...");
                do
                {
                    string siteJson = site.GetSite();
                    var msg = "P000000000000600 {\"get_site_data_response\": { \"site_table\":" + siteJson + "}}";
                    pubSocket.SendMoreFrame("SiteData").SendFrame(msg);
                    System.Threading.Thread.Sleep(1000);
                } while (true);
            }
        } // En

        //P00000000000099
    } // End of class MainMethod

} // End of namespace ZMQResponseApp
