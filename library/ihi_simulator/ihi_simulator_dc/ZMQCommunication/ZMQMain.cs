﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetMQ;
using NetMQ.Sockets;

namespace ihi_simulator_dc.ZMQCommunication
{
    public class ZMQMain
    {
        public static void ZMQProcess()
        {
            string connection = "tcp://*:1039";
            using (var poller = new NetMQPoller())
            {
                using (var server = new ResponseSocket())
                {
                    try
                    {
                        //ZMQ Response 
                        Console.WriteLine("Response Socket is ready");
                        server.ReceiveReady += Server_ReceiveReady;
                        poller.Add(server);
                        poller.RunAsync();
                        server.Bind(connection);
                        //ZMQ Publisher
                        ZMQ.ZMQPublisher();
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
        }

        private static void Server_ReceiveReady(object sender, NetMQSocketEventArgs e)
        {
            ZMQ.ZMQResponse(e.Socket);
        }

    }
}