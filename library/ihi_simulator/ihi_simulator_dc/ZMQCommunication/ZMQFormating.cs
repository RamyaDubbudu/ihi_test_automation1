﻿/*=============================================================================
 Name        : ihi_protocol_zmq.cs
 Version     : 1.0
 Copyright   : Copyright (c) 2018 IHI, Inc.  All Rights Reserved
 Description : IHI protocol definition for ZeroMQ (ZMQ) communications.
 ============================================================================*/

/******************************************************************************
 This is the IHI ZMQ communication protocol definition.= "
 This string based protocol will appear in the following format:

 -----------------------------------------------------------------------------
 |QUEUE TYPE | SOURCE | DESTINATION | MSG_TYPE | HASH VALUE |      PAYLOAD   |
 -----------------------------------------------------------------------------
 |   1 BYTE  | 4 BYTES|   4 BYTES   | 3 BYTES  |   8 BYTES  | variable length|
 -----------------------------------------------------------------------------
 *****************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_simulator_dc
{
    public class ZMQFormating
    {

        /* Length Definitions */
        public static string IHI_PROTOCOL_ZMQ__LENGTH__ZONE_ID = "2";
        public static string IHI_PROTOCOL_ZMQ__LENGTH__SOURCE = "2";
        public static string IHI_PROTOCOL_ZMQ__LENGTH__HASH_TABLE = "8";
        public static string IHI_PROTOCOL_ZMQ__LENGTH__MESSAGE_TYPE = "3";

        /* Valid/Invalid Definitions */
        public static string IHI_PROTOCOL_ZMQ__INVALID = "0";
        public static string IHI_PROTOCOL_ZMQ__VALID = "(!IHI_PROTOCOL_ZMQ__INVALID)";

        /* Default Message Size */
        public static string IHI_PROTOCOL_ZMQ__DEFAULT_MESSAGE_SIZE = "96";  // DECS_MESSAGES__DEFAULT_MESSAGE_SIZE
        public static string IHI_PROTOCOL_ZMQ__MESSAGE_NO_CONTENTS_SIZE = "16";  // Message size with no pcsContents field - no payload
        public static string IHI_PROTOCOL_ZMQ__MESSAGE_CONTENTS_SIZE = "(IHI_PROTOCOL_ZMQ__DEFAULT_MESSAGE_SIZE - IHI_PROTOCOL_ZMQ__MESSAGE_NO_CONTENTS_SIZE)";

        /* Max size Definitions */
        public static string IHI_PROTOCOL_ZMQ__MAX_SIZE__FILEPATH = "255";
        public static string IHI_PROTOCOL_ZMQ__MAX_SIZE__ZONE_COUNT = "64";
        public static string IHI_PROTOCOL_ZMQ__MAX_MESSAGE_TYPES = "800";

        /* 'Queue Type' Message Definitions */
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DOMAIN_CONTROLLER_REQUEST = "'1'"; // DECS_MESSAGES__DOMAIN_CONTROLLER_REQUEST
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DOMAIN_CONTROLLER_RESPONSE = "'2'"; // DECS_MESSAGES__DOMAIN_CONTROLLER_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__QUEUE__ZONE_CONTROLLER_REQUEST = "'3'"; // DECS_MESSAGES__ZONE_CONTROLLER_REQUEST
        public static string IHI_PROTOCOL_ZMQ__QUEUE__ZONE_CONTROLLER_RESPONSE = "'4'"; // DECS_MESSAGES__ZONE_CONTROLLER_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DEVICE_REQUEST = "'5'"; // DECS_MESSAGES__CONNECTOR_REQUEST
        public static string IHI_PROTOCOL_ZMQ__QUEUE__DEVICE_RESPONSE = "'6'"; // DECS_MESSAGES__CONNECTOR_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__QUEUE__PUBLICATION = "'P'"; // DECS_MESSAGES__PUBLICATION

        /* proposed
        public static string  IHI_PROTOCOL_ZMQ__QUEUE__PUBLICATION                    'P'
        public static string  IHI_PROTOCOL_ZMQ__QUEUE__REQUEST                        'Q'
        public static string  IHI_PROTOCOL_ZMQ__QUEUE__RESPONSE                       'R'
        */

        /***************************************************************************************
         * Source and Destination identify who the originator (source) of the message is and the
         * receiver (destination) of the message. The first two bytes of the source/destination
         * item is the module id, the second two bytes is the zone id. For modules that are
         * not associated with a specific zone, but site level modules, their "zone id" is '00'.
         *****************************************************************************************/
        //public static string  source[2]";      //  '00' = "ZONE CONTROLLER (OR DOMAIN CONTROLLER IF ZONE='00', OR MODE CONTROLLER IF ZONE='98')

        /* 'Zone ID' Definitions */
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__DOMAIN_CONTROLLER = "0";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__CANBUS_CONNECTOR_1 = "1";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__CANBUS_CONNECTOR_2 = "2";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__MODBUS_CONNECTOR_1 = "3";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__MODBUS_CONNECTOR_2 = "4";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__REF_METER_CONNECTOR_1 = "5";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__REF_METER_CONNECTOR_2 = "6";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__RS232_CONNECTOR_1 = "7";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__RS232_CONNECTOR_2 = "8";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__IO_1 = "9";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__IO_2 = "10";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__MODE_CONTROLLER = "98";
        public static string IHI_PROTOCOL_ZMQ__ZONE_ID__USER_INTERFACE = "99";

        /* 'ID' Definitions */
        public static string IHI_PROTOCOL_ZMQ__ID__DOMAIN_CONTROLLER = "00";
        public static string IHI_PROTOCOL_ZMQ__ID__ZONE_CONTROLLER = "01";
        public static string IHI_PROTOCOL_ZMQ__ID__INVERTER = "02";
        public static string IHI_PROTOCOL_ZMQ__ID__METER = "03";
        public static string IHI_PROTOCOL_ZMQ__ID__CHILLER = "04";
        public static string IHI_PROTOCOL_ZMQ__ID__BATTERY = "05";
        public static string IHI_PROTOCOL_ZMQ__ID__UI = "06";
        public static string IHI_PROTOCOL_ZMQ__ID__IO = "07";

        public static string IHI_PROTOCOL_ZMQ__ID_LENGTH = "2";


        /* **************************************************************************************
        * Message type (MSG_TYPE) is the heart of the message being sent. Each message commands
        * the module to perform a specific act, provides information. This is the heart of the
        * protocol.
        *****************************************************************************************/
        // Common Requests
        public static string IHI_PROTOCOL_ZMQ__TYPE__REGISTER_ZONE = "50"; // REGISTER_ZONE
        public static string IHI_PROTOCOL_ZMQ__TYPE__REGISTER_CANBUS = "51"; // REGISTER_CANBUS
        public static string IHI_PROTOCOL_ZMQ__TYPE__REGISTER_MODBUS = "52"; // REGISTER_MODBUS
        public static string IHI_PROTOCOL_ZMQ__TYPE__REGISTER_METER = "55"; // REGISTER_METER
        public static string IHI_PROTOCOL_ZMQ__TYPE__REGISTER_IO = "58"; // REGISTER_IO
        public static string IHI_PROTOCOL_ZMQ__TYPE__EMERGENCY_POWER_OFF = "99"; // EPO

        public static string IHI_PROTOCOL_ZMQ__TYPE__HEARTBEAT = "100"; // HEARTBEAT
        public static string IHI_PROTOCOL_ZMQ__TYPE__PQ_DEMAND = "101";
        public static string IHI_PROTOCOL_ZMQ__TYPE__PQ_ACTUAL = "102";

        // COMMON MESSAGES                                             // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__DATA_LOG_ENTRY = "103";
        public static string IHI_PROTOCOL_ZMQ__TYPE__ZONE_DATA_LOG_ENTRY = "104";

        public static string IHI_PROTOCOL_ZMQ__TYPE__SHUTDOWN = "149";       // zone #
        public static string IHI_PROTOCOL_ZMQ__TYPE__REGISTER = "50";       // zone #,pub_ip:pub_port,responder_ip:responder_port
        public static string IHI_PROTOCOL_ZMQ__TYPE__REJUVENATE_ZONE = "106";
        public static string IHI_PROTOCOL_ZMQ__TYPE__REOPEN_REQUEST_QUEUE = "107";

        // Domain Controller                                    // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__MODE_DEMAND_SET = "105"; // UPDATE_MODE_DEMAND
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_DEBUG_LEVEL = "150";
        public static string IHI_PROTOCOL_ZMQ__TYPE__MODE_CONTROLLER_INPUTS = "160"; // UPDATE_PQ_DEMAND    
        public static string IHI_PROTOCOL_ZMQ__TYPE__MODE_CONTROLLER_ZONE_INPUTS = "161"; // 
        public static string IHI_PROTOCOL_ZMQ__TYPE__MODE_CONTROLLER_OUTPUTS = "300";// MODE_CONTROLLER_OUTPUTS
        public static string IHI_PROTOCOL_ZMQ__TYPE__MODE_CONTROLLER_ZONE_ALLOCATIONS = "301";

        public static string IHI_PROTOCOL_ZMQ__TYPE__SITE_SOC = "152"; // SITE_SOC_UPDATE     // displayed on site-level SEL735
        public static string IHI_PROTOCOL_ZMQ__TYPE__DOMAIN_CONTROLLER_UPDATE_ENABLED_BATTERIES = "153"; // DC_UPDATE_RACKS_ENABLED
        public static string IHI_PROTOCOL_ZMQ__TYPE__DC_UPDATE_INVERTER_ENABLED = "154";    // zone #, inverter #, enabled=1 disabled=0
        public static string IHI_PROTOCOL_ZMQ__TYPE__UPDATE_DISCRETE_OUTPUTS = "155";     // UPDATE_DISCRETE_OUTPUTS 
        public static string IHI_PROTOCOL_ZMQ__TYPE__COMM_DEMAND = "157"; // Modbus/Analog communication
        public static string IHI_PROTOCOL_ZMQ__TYPE__METER_AVAILABILITY = "227"; // CONN_METER_AVAILABILITY    //    rack_no, 0=unavailable | 1=available [communicating]
        public static string IHI_PROTOCOL_ZMQ__CONN_IO_AVAILABILITY = "228";
        public static string IHI_PROTOCOL_ZMQ__TYPE__SEL735_STATUS = "231"; // CONN_SEL735_DATA    //     meter #, freq, power_factor, ia, ib, ic, vab, vbc, vac, va_3ph, var_3ph
        public static string IHI_PROTOCOL_ZMQ__TYPE__CONN_ESU_STATE_OF_HEALTH = "247";
        // Note the IHI_PROTOCOL_ZMQ__TYPE__INVERTER_POWER_CAPACITY was being set to 301 not 248 -- garyc
        public static string IHI_PROTOCOL_ZMQ__TYPE__INVERTER_POWER_CAPACITY = "248";

        // Zone controller                    // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__ZONE_HB_MASK = "156";  // zone #, io #, mask
        public static string IHI_PROTOCOL_ZMQ__TYPE__HB_MASK_BIT = "205"; // io #. returns bit_mask for the heart beat for output to IO 
        public static string IHI_PROTOCOL_ZMQ__TYPE__ZONE_MODE = "208"; // UPDATE_ZONE_MODE


        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_ENABLE = "209"; // GET_ENABLED_RACKS    // returns REPONSE message with public static string  bitfield mask for enabled racks
        public static string IHI_PROTOCOL_ZMQ__REQUEST_ENABLED_RACKS_MASK = "211";
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_CONTACTORS_CLOSE = "250"; // CLOSE_CONTACTORS
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_CONTACTORS_OPEN = "256"; // OPEN_CONTACTORS
                                                                                      // Note not 251 in decs_message_types its 252 
                                                                                      //public static string  IHI_PROTOCOL_ZMQ__TYPE__BATTERY_REJUVENATION                            = "251"; // REJUVENATION	 // 1=Rejuvenation, 0=Cancel
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_REJUVENATION = "252"; // REJUVENATION	 // 1=Rejuvenation, 0=Cancel

        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_CONTACTORS_STATUS = "251"; // CONN_GET_RACK_STATUS
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_ONLINE = "290"; // RACK_STATUS_ONLINE
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_STATE_OF_CHARGE = "291"; // RACK_SOC_ACTUAL
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_EQUALIZATION = "206"; // SET_EQUALIZATION    // 0=OFF, 1=ON
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_AVAILABILITY = "226"; // CONN_RACK_AVAILABILITY    //    rack_no, 0=unavailable | 1=available [communicating]
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_STATUS = "231"; // CONN_RACK_STATUS    //     rack #, status index (see decs.h)
        // IHI_PROTOCOL_ZMQ__TYPE__BATTERY_VI is defined as 233 in decs_message_types.h
        // Note::Misalignment -- garyc  
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_VI = "233"; // CONN_RACK_VI    //     rack #, v, i
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_DISABLED = "234"; // CONN_RACK_DISABLED    //  rack # of disabled rack
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_TEMPERATURE_AVERAGE = "235"; // CONN_RACK_TEMPERATURE_AVERAGE                 //  rack #, temperature
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_TEMPERATURE_MINIMUM = "236"; // CONN_RACK_TEMPERATURE_MINIMUM                 //  rack #, temperature
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_TEMPERATURE_MAXIMUM = "237"; // CONN_RACK_TEMPERATURE_MAXIMUM    //  rack #, temperature
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_CELL_VOLTAGE_AVERAGE = "238"; // CONN_RACK_CELL_VOLTAGE_AVERAGE    //  rack #, Voltage
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_CELL_VOLTAGE_MINIMUM = "239"; // CONN_RACK_CELL_VOLTAGE_MINIMUM     //  rack #, Voltage
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_CELL_VOLTAGE_MAXIMUM = "240";// CONN_RACK_CELL_VOLTAGE_MAXIMUM    //  rack #, Voltage
        public static string IHI_PROTOCOL_ZMQ__TYPE__BATTERY_CONN_ESU_REJUVENATION_MODE = "245"; // CONN_ESU_REJUVENATION_MODE    //  rack #, Voltage

        // ZC TO MODBUS CONNECTOR REQUESTS                    // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__INVERTER_STATUS_WORD1 = "302"; // GET_INVERTER_STATUS_WORD
        public static string IHI_PROTOCOL_ZMQ__TYPE__INVERTER_STATUS_WORD = "302";

        public static string IHI_PROTOCOL_ZMQ__TYPE__INVERTER_ENABLE = "308"; // SET_INVERTER_ONLINE
        public static string IHI_PROTOCOL_ZMQ__TYPE__INVERTER_DISABLE = "309"; // SET_INVERTER_OFFLINE
        // I think the below is wrong value based on 248 in decs_dc -- garyc 
        //public static string  IHI_PROTOCOL_ZMQ__TYPE__INVERTER_POWER_CAPACITY                         = "310 // GET_INVERTER_PWR_CAPACITY        310        // inverter_no, p_max, p_min, q_max, q_min

        public static string IHI_PROTOCOL_ZMQ__CONN_METER_AVAILABILITY = "227";
        public static string IHI_PROTOCOL_ZMQ__TYPE__INVERTER_AVAILABILITY = "229"; /* CONN_INVERTER_AVAILABILITY        229        //    inverter_no, 0=unavailable | 1=available [communicating] */
        public static string IHI_PROTOCOL_ZMQ__TYPE__INVERTER_VI = "230"; /* CONN_INVERTER_VI_TEMP            230        //  inverter_no, dc_bus, ac_v1, ac_v2, ac_v3 */
        public static string IHI_PROTOCOL_ZMQ__CONN_SEL735_DATA = "232";     //  meter #, freq,
        public static string IHI_PROTOCOL_ZMQ__CONN_IO_INPUTS_DATA = "241";
        public static string IHI_PROTOCOL_ZMQ__CONN_IO_OUTPUTS_DATA = "242";
        public static string IHI_PROTOCOL_ZMQ__CONN_ESU_LIMITS = "246";

        // ISO CONNECTOR TO DC REQUESTS
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_REMOTE_TABLE = "350";  /* GET_REMOTE_TABLE    public static string  remote_id, public static string  ess_sequence, public static string  iso_sequence, public static string  p_demand, public static string  q_demand, public static string  ignore, public static string  comms_timeout in seconds */
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_ESS_SEQNO = "351";  /* GET_ESS_SEQNO       ess_sequence # */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_SEQNO = "352";  /* SET_ISO_SEQNO       iso_sequence # */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_PQ_DEMAND = "353";  /* SET_ISO_PQ_DEMAND   p_demand, q_demand */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_MODE_DEMAND = "354"; /* mode # */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_SOC_TARGET = "355";  /* SOC target %.1f */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_SOC_POWER = "356";  /* +/- KW LIMIT FOR SOC RECAPTURE */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_MODE_PQ_TARGETS = "357";  /* P,Q TARGETS.  NULL PARM (e.g.: ",2") = DO NOT CHANGE */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_ENABLE = "358";  /* 1=ENABLED 0=GO TO ZERO P,Q */
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_ISO_STATUS_REGISTERS = "359";  /* RETURNS REGISTERS 0 THRU 22 16-BIT VALUES IN DECIMAL  SEE MB V1.1 MAP */
        public static string IHI_PROTOCOL_ZMQ__TYPE__GET_ISO_CONTROL_REGISTERS = "360";  /* RETURNS REGISTERS 0 THRU 12 SEE MB V1.1 MAP */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_RAMP_RATES = "361";  /* p_up, p_down, q_up, q_down in kW(kVAR) per second */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_IDLE_DELAY = "362";  /* Idle Delay */
        public static string IHI_PROTOCOL_ZMQ__TYPE__SET_ISO_ENABLE_ZONES = "363";  /* Enable Zones */

        // UI TO DC                                        // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SITE_INFO = "600"; // UI_GET_SITE_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ZONES_INFO = "601"; // UI_GET_ZONES_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_RACKS_INFO = "602"; // UI_GET_RACKS_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_MODULES_INFO = "603"; // UI_GET_MODULES_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTERS_INFO = "604"; // UI_GET_INVERTERS_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_COOLING_INFO = "605"; // UI_GET_COOLING_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_IO_INFO = "606"; // UI_GET_IO_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_METERS_INFO = "607"; // UI_GET_METERS_INFO
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_USER_INFO = "608"; // UI_GET_USER_INFO

        // UI commands
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_MODE_DEMAND = "609";    // UI_SET_MODE_DEMAND    // user_id, mode
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_PQ_DEMAND = "610";    // UI_SET_PQ_DEMAND    // for manual and load leveling
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_ENABLED = "611";    // UI_SET_RACK_ENABLED    // zone #, rack # (0=all), 0=Disable, 1=Enable
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SET_TARGET_SOC = "612";     // SOC in %. Range is 0 to 100.
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SET_INVERTER_ENABLED = "613";
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SET_ZONE_ENABLED = "614"; // UI_SET_ZONE_ENABLED
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SET_SOC_TARGET = "615";     // 0=disabled, else enabled
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SET_AUTO_RESTART = "616";     // 0=disabled, else enabled
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SET_PROFILE_NUMBER = "617";     // 0=none, else # of the profile to us
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_APPLY_SOFTWARE_UPDATE = "618"; //  UI_APPLY_SOFTWARE_UPDATE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_SET_RAMP_LIMITS = "619"; //  UI_SET_RAMP_LIMITS 
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_EXPORT_DATA_LOG = "630"; // UI_EXPORT_DATA_LOG  
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_EXPORT_EVENT_LOG = "631"; // UI_EXPORT_EVENT_LOG
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_REJUVENATE_ZONE = "640";    // Zone #, [1=rejuvenate,0=cancel]
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_GET_ALERT_RECIPIENTS_INFO = "660";
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ADD_ALERT_RECIPIENT = "661";
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_UPDATE_ALERT_RECIPIENT_INFO = "662"; // sms_user #,{'999-999-9999'}, {alert_informational}, {alert_minor}, {alert_major}, {alert_critical};
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DELETE_ALERT_RECIPIENT = "663";

        // UI user messages
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_UPDATE_USER_DATA = "670"; // UI_UPDATE_USER_DATA
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DELETE_USER = "671"; // UI_DELETE_USER
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ADD_USER = "672"; // UI_ADD_USER
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_USER_LOGIN = "673"; // UI_USER_LOGIN    // user_id
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_USER_LOGOUT = "674"; // UI_USER_LOGOUT    // user_id
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_GET_PROFILE_NAME_LIST = "787"; // UI_GET_PROFILE_NAME_LIST
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_GET_TARGET_LABEL_LIST = "789"; // UI_GET_TARGET_LABEL_LIST        


        // UI to EVENTD                                    // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ACKNOWLEDGE_EVENT = "750"; // UI_ACKNOWLEDGE_EVENT        // user_id [,key].         If no key, all un-acked are acked.
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DISMISS_EVENT = "751"; // UI_DISMISS_EVENT        // [key]                If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_EVENT_ALARM = "752"; // UI_EVENT_ALARM        //                         Returns all alarms that have not been dismissed
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_EVENT = "753 // UI_EVENT";    // [key]                If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ALARM = "754 // UI_ALARM";    // [key]                If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ACKNOWLEDGE_ALARM = "755"; // UI_ACKNOWLEDGE_ALARM     // user_id [,key].         If no key, all un-acked are acked.
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DISMISS_ALARM = "756"; // UI_DISMISS_ALARM    // [key]                If no key, all
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ADD_COMMENT_ENTRY = "757"; // UI_ADD_COMMENT_ENTRY			757
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_DATA_LOG = "760"; // UI_DATA_LOG    // parm=# of minutes history (1 to 180).  Default if not specified: 15 minutes
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_CLEAR_DATA_LOG = "761"; // UI_CLEAR_DATA_LOG    // parm=# of minutes history (1 to 180).  Default if not specified: 15 minutes

        // UI Lists
        public static string IHI_PROTCOL__ZMQ__TYPE__UI_GET_META_DATA = "785"; // UI_GET_META_DATA
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_GET_CURRENT_PROFILE_NAME = "786"; // UI_GET_CURRENT_PROFILE_NAME
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_GET_UPS_STATUS_LIST = "788"; // UI_GET_UPS_STATUS_LIST
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_AVAILABILITY_TYPES_LIST = "790"; // UI_GET_AVAILABILITY_TYPES_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_BATTERY_TYPES_LIST = "791"; // UI_GET_BATTERY_TYPES_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_COOLING_STATUS_LIST = "792"; // UI_GET_COOLING_STATUS_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ENABLED_STATUS_LIST = "793"; // UI_GET_ENABLED_STATUS_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_AVAILABLE_MODES_LIST = "794"; // UI_GET_AVAILABLE_MODES_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTER_STATUS_LIST = "795"; // UI_GET_INVERTER_STATUS_LIST    // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_ZONE_STATUS_LIST = "796"; // UI_GET_ZONE_STATUS_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_RACK_STATUS_LIST = "797"; // UI_GET_RACK_STATUS_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_INVERTER_MODES_LIST = "798"; // UI_GET_INVERTER_MODES_LIST        // returns JSON_RESPONSE
        public static string IHI_PROTOCOL_ZMQ__TYPE__UI_MESSAGE_TYPES = "799"; // UI_GET_MESSAGE_TYPES        // returns JSON_RESPONSE

        /* ************************************************************************************
         * Hash value will be implemented in the future as a security feature. For now we have
         * a placeholder for the hash value
         **************************************************************************************/
        /* Message 'Hash Table' Definition */
        public static string IHI_PROTOCOL_ZMQ__HASH_VALUE = "00000000";

        /* ************************************************************************************
         * User public static string erface definitions
         **************************************************************************************/

        // RESPONSES TO REQUESTS                            // parameters (ASCIIZ)
        public static string IHI_PROTOCOL_ZMQ__RESPONSE = "0"; // RESPONSE    // returns data
        public static string IHI_PROTOCOL_ZMQ__STATUS = "1"; // STATUS    // returns 0 for Okay, non-zero for error code
        public static string IHI_PROTOCOL_ZMQ__JSON_RESPONSE = "10"; // JSON_RESPONSE    // of the form: { \"request_type_response\": {\"status\":%d} }

        /* End of ihi_protocol_zmq.cs */
    }
}