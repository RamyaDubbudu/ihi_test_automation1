﻿using ihi_simulator_dc.ZMQCommunication;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ihi_simulator_dc
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            try
            {
                Task task = Task.Run((Action)ZMQMain.ZMQProcess);
                // Web API routes
                config.MapHttpAttributeRoutes();

                config.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "DCSim/{controller}/{action}/{id}",
                    defaults: new { id = RouteParameter.Optional }
                );
                //Starting ZMQ Process 
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


    }
}
