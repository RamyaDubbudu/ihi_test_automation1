﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IHI_SIMULATOR_INVERTER_TMEIC.Models;
using Newtonsoft.Json.Linq;
using static IHI_SIMULATOR_INVERTER_TMEIC.Ihi_inverter_modbus_class;

namespace IHI_SIMULATOR_INVERTER_TMEIC.Controllers
{
    public class AlarmController : Controller
    {
        // GET: Alarm
        public ActionResult Index()
        {
            return View();
        }

        // GET: Alarm/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
      
        public ActionResult ShowAlarms(FormCollection collection)
        {
            int iRegCounter = 0;

            Alarm[] auAlarms = new Alarm[Current_AlarmList.Count];

            // Cycle through all the alarms in the list
            for (int i = 0; i < Current_AlarmList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_AlarmList[i];

                // Create an Inverter Register Object
                Alarm obj           = new Alarm();
                obj.id              = (string)oTemp["id"];
                obj.bitpos          = (string)oTemp["bitpos"];
                obj.register        = (string)oTemp["register"];
                obj.description     = (string)oTemp["description"];
                obj.state           = (string)oTemp["state"];

                // Increment alarm counter
                auAlarms[iRegCounter] = obj;
                iRegCounter++;
            }

            // Resize the alarm array
            Array.Resize(ref auAlarms, iRegCounter);
            return View(auAlarms);
        }

        // GET: Alarm/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Alarm/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Alarm/Edit/5
        public ActionResult Edit(int id)
        {
            int iRegCounter = 0;

            Alarm[] auAlarms = new Alarm[Current_AlarmList.Count];

            // Cycle through all the alarms in the list
            for (int i = 0; i < Current_AlarmList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_AlarmList[i];

                // Create an Inverter Register Object
                Alarm obj       = new Alarm();
                obj.id          = (string)oTemp["id"];
                obj.bitpos      = (string)oTemp["bitpos"];
                obj.register    = (string)oTemp["register"];
                obj.description = (string)oTemp["description"];
                obj.state       = (string)oTemp["state"];

                if (obj.state == "OFF")
                {
                    JObject oTargetAlarm = new JObject();

                    oTargetAlarm["alarmId"] = id.ToString();

                    var alarmId= Current_AlarmList[i]["id"];

                    if (alarmId.ToString() == oTargetAlarm["alarmId"].ToString())
                    {
                        obj.state = "ON";
                        Current_AlarmList[i]["state"] = "ON";

                        int posbit = 0;
                        bool result=int.TryParse(obj.bitpos, out posbit);
                        IHI_SET_REGISTER_VALUE("input", "fault", "", "1");
                        if (posbit > 15)
                        {
                            posbit = 31 - posbit;
                            posbit = 16 - (posbit + 1);
                            int alarmValue = 1 << posbit;

                            IHI_SET_REGISTER_VALUE("input", obj.register+"(H)", "", alarmValue.ToString());
                        }
                        else
                        {
                            int alarmValue = 1 << posbit;
                            IHI_SET_REGISTER_VALUE("input", obj.register + "(L)", "", alarmValue.ToString());
                        }
                    }
                }
              
                // Increment alarm counter
                auAlarms[iRegCounter] = obj;
                iRegCounter++;
            }

            // Resize the alarm array
            Array.Resize(ref auAlarms, iRegCounter);
            return View(auAlarms);
        }

        // POST: Alarm/Edit/5
        [HttpPost]
        public ActionResult Trigger(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Alarm/Clear
        public ActionResult Clear(int id)
        {
            //System.Diagnostics.Trace.WriteLine("Enter  Alarm:Clear");
            int iRegCounter = 0;
            try
            {
                // Clear the fault
                IHI_SET_REGISTER_VALUE("input", "statusbit3word0(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word0(L)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word1(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word1(L)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word2(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word2(L)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word3(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word3(L)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word4(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word4(L)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word5(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word5(L)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word6(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word6(L)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word7(H)", "", "0");
                IHI_SET_REGISTER_VALUE("input", "statusbit3word7(L)", "", "0");

                // Reduce to correct index
                Alarm[] auAlarms = new Alarm[Current_AlarmList.Count];

                // Cycle through all the alarms in the list
                for (int i = 0; i < Current_AlarmList.Count; i++)
                {
                    JObject oTemp = new JObject();
                    oTemp = Current_AlarmList[i];

                    // Set initial state of alarm to OFF
                    Current_AlarmList[i]["state"] = "OFF";

                    // Create an Inverter Register Object
                    Alarm obj       = new Alarm();
                    obj.id          = (string)oTemp["id"];
                    obj.description = (string)oTemp["description"];
                    obj.state       = (string)oTemp["state"];
              
                    // Increment alarm counter
                    auAlarms[iRegCounter] = obj;
                    iRegCounter++;
                }

                // Resize the alarm array
                Array.Resize(ref auAlarms, iRegCounter);
                return View(auAlarms);
            }
            catch
            {
                return View();
            }
        }

        // GET: Alarm/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Alarm/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
