﻿using IHI_SIMULATOR_INVERTER_TMEIC;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHI_SIMULATOR_INVERTER_TMEIC.Controllers
{
    public class UIGeneralController : Controller
    {
        // GET: UIGeneral
        public ActionResult Index()
        {
            JObject oSystemObject = Ihi_inverter_modbus_class.IHI_OBTAIN_MODBUS_INVERTER_SYSTEM_STATE();
         
            return View(oSystemObject);
        }

        // GET: UIGeneral/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UIGeneral/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UIGeneral/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UIGeneral/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UIGeneral/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UIGeneral/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UIGeneral/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
