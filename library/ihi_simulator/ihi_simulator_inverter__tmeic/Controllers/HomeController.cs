﻿using System.Web.Mvc;
using IHI_SIMULATOR_INVERTER_TMEIC.Models;
using IHI_SIMULATOR_INVERTER_TMEIC;
using Newtonsoft.Json.Linq;

namespace IHI_SIMULATOR_INVERTER_TMEIC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "IHI Simulator Power Electronics";
            JObject oSystemObject = Ihi_inverter_modbus_class.IHI_OBTAIN_MODBUS_INVERTER_SYSTEM_STATE();
            InverterSystem oGeneralInformation  = new InverterSystem();
            oGeneralInformation.company         = (string)oSystemObject["company"];
            oGeneralInformation.product         = (string)oSystemObject["product"];
            oGeneralInformation.status          = (string)oSystemObject["status"];
            oGeneralInformation.alarmed         = (string)oSystemObject["alarmed"];
            oGeneralInformation.communication   = (string)oSystemObject["communication"];
            return View(oGeneralInformation);
        }

        // GET: test/Create
        public ActionResult Create()
        {
            return View();
        }
    }
}
