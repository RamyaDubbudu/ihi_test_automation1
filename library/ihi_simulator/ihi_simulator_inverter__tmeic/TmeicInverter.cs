﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IHI_SIMULATOR_INVERTER_TMEIC
{
    // Pure virtual class
    public abstract class Inverter
    {
        public abstract double getActualRealPower(short sRealPowerDemand);
        public abstract double getActualReactivePower(short sReactivePowerDemand);
        public abstract void setNamePlate(ushort usInverterNamePlate);
        public abstract void setNominalACVoltage(ushort usInverterNominalVoltage);
        public abstract ushort getPhaseAVoltage();
        public abstract ushort getPhaseBVoltage();
        public abstract ushort getPhaseCVoltage();
        public abstract double getPhaseACurrent();
        public abstract double getPhaseBCurrent();
        public abstract double getPhaseCCurrent();
        public abstract void setPLimit(ushort plimit);
        public abstract void setQLimit(ushort qlimit);
        public abstract ushort getFaultedState();
        public abstract ushort getEnabledValue();
        public abstract ushort getDisabledValue();

        public abstract ushort getStartCommand();
        public abstract void initialInverterConfiguration(Ihi_inverter_modbus_class oModClass);

        // Set methods to overload from REST API
        public abstract JObject getDemandInverterRealPower();
        public abstract JObject getDemandInverterReactivePower();
        public abstract JObject getActualInverterRealPower();
        public abstract JObject getActualInverterReactivePower();
        public abstract JObject getFrequency();
        public abstract JObject getDCVoltage();
        public abstract JObject getACVoltage();
        public abstract JObject getVoltagePhaseA();
        public abstract JObject getVoltagePhaseB();
        public abstract JObject getVoltagePhaseC();
        public abstract JObject getCurrentPhaseA();
        public abstract JObject getCurrentPhaseB();
        public abstract JObject getCurrentPhaseC();
        public abstract JObject getOperationalState();
        public abstract JObject getAlarm();
        public abstract JObject getWarningAlarm();

        // Set methods to overload from REST API
        public abstract void setDemandInverterRealPower(string strRealPowerActual);
        public abstract void setDemandInverterReactivePower(string strReactivePowerDemand);
        public abstract void updateActualInverterRealPower(string strRealPowerActual);
        public abstract void updateActualInverterReactivePower(string strReactivePowerActual);
        public abstract void updateFrequency(string strReactivePowerActual);
        public abstract void updateDCVoltage(string strDCVoltage);
        public abstract void updatePhaseAVoltage(string strPhaseAVoltage);
        public abstract void updatePhaseBVoltage(string strPhaseBVoltage);
        public abstract void updatePhaseCVoltage(string strPhaseCVoltage);
        public abstract void updatePhaseACurrent(string strPhaseACurrent);
        public abstract void updatePhaseBCurrent(string strPhaseBCurrent);
        public abstract void updatePhaseCCurrent(string strPhaseCCurrent);
        public abstract void updateOperationalState(string strState);
        public abstract void updateModelType(string strValue);
        public abstract void updateAlarm(string strAlarm);
        public abstract void updateAlarm(string registerLabel,string strAlarm);
        public abstract void updateWarningAlarm(string strAlarm);
        public abstract void clearAlarms();
    }

    public class PowerElectronicsInverter : Inverter
    {
        private double fActualRealPowerValue;
        private double fActualReactivePowerValue;
        private ushort usNamePlate;
        private ushort usNominalACVoltage;
        private double fPhaseAVoltage;
        private double fPhaseBVoltage;
        private double fPhaseCVoltage;
        private double fPhaseACurrent;
        private double fPhaseBCurrent;
        private double fPhaseCCurrent;
        private ushort usPLimit;
        private ushort usQLimit;

        private static ushort FAULTED = 9;
        private static ushort RUNNING = 6;
        private static ushort STOPPED = 2;
        private static ushort STARTED = 1;
        private Ihi_inverter_modbus_class _oModClass;

        // Obtain Real/Active Power Demand
        public override JObject getDemandInverterRealPower()
        {
            JObject oTempRealPowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("Wspt", "1");
            string strRealPowerCommand = (string)oTempRealPowerCommand["value"];
            short sRealPowerCommand = short.Parse(strRealPowerCommand);
            double scaling = 10000;
            double fRealPowerCommand = (double)sRealPowerCommand;
            double fActualRealPowerDemand = fRealPowerCommand/ scaling;
            fActualRealPowerDemand = fActualRealPowerDemand * usNamePlate;
            short sReturnRealPower = (short)fActualRealPowerDemand;
            oTempRealPowerCommand["value"] = sReturnRealPower.ToString();
            return oTempRealPowerCommand;
        }

        // Obtain Reactive Power Demand
        public override JObject getDemandInverterReactivePower()
        {
            //return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("reactivepowercmd");
            JObject oTempReactivePowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("VarSpt","1");
            string strReactivePowerCommand = (string)oTempReactivePowerCommand["value"];
            short sReactivePowerCommand = short.Parse(strReactivePowerCommand);
            double scaling = 10000;
            double fReactivePowerCommand = (double)sReactivePowerCommand;
            double fActualReactivePowerDemand = fReactivePowerCommand / scaling;
            fActualReactivePowerDemand = fActualReactivePowerDemand * usNamePlate;
            short sReturnReactivePower = (short)fActualReactivePowerDemand;
            oTempReactivePowerCommand["value"] = sReturnReactivePower.ToString();
            return oTempReactivePowerCommand;
        }

        // Obtain Actual Inverter Real Power
        public override JObject getActualInverterRealPower()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("InvMs.TotW(L)","1");
        }

        // Obtain Actual Reactive Power
        public override JObject getActualInverterReactivePower()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("InvMs.TotVar","1");
        }

        // Obtain Frequency
        public override JObject getFrequency()
        {
            JObject oTempFrequency =_oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("GriMs.Hz","1");
            string strFrequency = (string)oTempFrequency["value"];
            double sFrequency = double.Parse(strFrequency);
            if (sFrequency != 0)
            {
                sFrequency = sFrequency / 100;
            }
            oTempFrequency["value"] = sFrequency.ToString();
            return oTempFrequency;
        }

        // Obtain DC Voltage
        public override JObject getDCVoltage()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("dcvoltage","1");
        }

        // Obtain AC Voltage
        public override JObject getACVoltage()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("nominalacvoltage","1");
        }

        // Obtain Phase A Voltage
        public override JObject getVoltagePhaseA()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridvoltageab","1");
        }

        // Obtain Phase Voltage
        public override JObject getVoltagePhaseB()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridvoltagebc","1");
        }

        // Obtain Phase C Voltage
        public override JObject getVoltagePhaseC()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridvoltageca","1");
        }

        // Obtain Phase Current A
        public override JObject getCurrentPhaseA()
        {  
            JObject oTempPhaseA = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currentr","1");
            string strPhaseA = (string)oTempPhaseA["value"];
            short sPhaseA = short.Parse(strPhaseA);
            double scaling = 10;
            double fPhaseA = (double)sPhaseA;
            fPhaseA = fPhaseA / scaling;
           
            oTempPhaseA["value"] = fPhaseA.ToString();
            return oTempPhaseA;
        }

        // Obtain Phase Current B
        public override JObject getCurrentPhaseB()
        {
            JObject oTempPhaseB = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currents","1");
            string strPhaseB = (string)oTempPhaseB["value"];
            short sPhaseB = short.Parse(strPhaseB);
            double scaling = 10;
            double fPhaseB = (double)sPhaseB;
            fPhaseB = fPhaseB / scaling;
           
            oTempPhaseB["value"] = fPhaseB.ToString();
            return oTempPhaseB;
        }

        // Obtain Phase Current C
        public override JObject getCurrentPhaseC()
        {
            JObject oTempPhaseC = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currentt","1");
            string strPhaseC = (string)oTempPhaseC[""];
            short sPhaseC = short.Parse(strPhaseC);
            double scaling = 10;
            double fPhaseC = (double)sPhaseC;
            fPhaseC = fPhaseC / scaling;
            
            oTempPhaseC["value"] = fPhaseC.ToString();
            return oTempPhaseC;
        }

        // Obtain Operational State
        public override JObject getOperationalState()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currentstatus","1");
        }

        // Obtain Alarm value
        public override JObject getAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("fault","1");
        }

        // Obtain Warning Alarm
        public override JObject getWarningAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("warning","1");
        }

        // Set actual real power sent over REST API
        public override void updateActualInverterRealPower(string sValue)
        {
            // Set Actual Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            float value = (float)usValue / 100;
            int ActualValue = (int) value * 2000;
            System.Diagnostics.Trace.WriteLine("Enter ***************updateActualInverterRealPower:" + ActualValue);

            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("gridactivepower", usValue);
        }

        // Set demand real power sent over REST API
        public override void setDemandInverterRealPower(string sValue)
        {
            // Set Demand Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
          
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("realpowercmd", usValue);
        }

        // Set actual reactive power sent over REST API
        public override void updateActualInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Real Power

            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("gridreactivepower", usValue);
        }

        // Set demand reactive power sent over REST API
        public override void setDemandInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Reactive Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("reactivepowercmd", usValue);
        }

        // Set frequency
        public override void updateFrequency(string sValue)
        {
            // Set Actual Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            int iValue = usValue * 100;
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("hz", (ushort)iValue);
        }

        // Set DC Voltage
        public override void updateDCVoltage(string strDCVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strDCVoltage);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("dcvoltage", usValue);
        }

        // Set Phase A voltage
        public override void updatePhaseAVoltage(string strPhaseAVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseAVoltage);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("gridvoltageab", usValue);
        }

        // Set Phase B Voltage
        public override void updatePhaseBVoltage(string strPhaseBVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseBVoltage);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("gridvoltagebc", usValue);
        }

        // Phase C Voltage
        public override void updatePhaseCVoltage(string strPhaseCVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseCVoltage);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("gridvoltageca", usValue);
        }

        // Set A->B Current
        public override void updatePhaseACurrent(string strPhaseACurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseACurrent);
            int iValue = usValue * 10;
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("currentr", (ushort)iValue);
        }

        // Set B->C Current
        public override void updatePhaseBCurrent(string strPhaseBCurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseBCurrent);
            int iValue = usValue * 10;
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("currents", (ushort)iValue);
        }

        // Phase C->A Current
        public override void updatePhaseCCurrent(string strPhaseCCurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseCCurrent);
            int iValue = usValue * 10;
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("currentt", (ushort) iValue);
        }

        // Set Operational status of inverter
        public override void updateOperationalState(string strState)
        {
            ushort usValue = (ushort)Int16.Parse(strState);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("currentstatus", usValue);
        }

        // Set Alarm
        public override void updateAlarm(string strAlarm)
        {
            ushort usValue = (ushort)Int16.Parse(strAlarm);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("fault", usValue);
        }

        public override void updateAlarm(string registerLabel, string strAlarm)
        {

        }
        // Set Warning Alarm
        public override void updateWarningAlarm(string strAlarm)
        {
            ushort usValue = (ushort)Int16.Parse(strAlarm);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("warning", usValue);
        }

        // Update Model Type
        public override void updateModelType(string strModelType)
        {
            // Model Type
            ushort usValue = (ushort)Int16.Parse(strModelType);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("modeltype", usValue);
        }

        // Clear all PE alarms
        public override void clearAlarms()
        {
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("erroNp", 0);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("warning", 0);
        }

        public override void initialInverterConfiguration(Ihi_inverter_modbus_class oModClass)
        {
            // Set initial nominal voltage to 
            usNominalACVoltage = 750;
            _oModClass = oModClass;
            oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("nominalacvoltage", usNominalACVoltage);

            // Set P Limit 100%
            oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("plimit", 100);

            // Set Q Limit 100%
            oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("qlimit", 100);
        }

        public override ushort getFaultedState()
        {
            return FAULTED;
        }

        public override ushort getEnabledValue()
        {
            return RUNNING;
        }

        public override ushort getDisabledValue()
        {
            return STOPPED;
        }

        public override ushort getStartCommand()
        {
            return STARTED;
        }

        // Determine Actual Real Power
        public override double getActualRealPower(short sRealPowerDemand)
        {
            double scaling = 10000;
            fActualRealPowerValue = sRealPowerDemand / scaling;

            // Nameplate is 2000kw for Power Electronics
            fActualRealPowerValue = fActualRealPowerValue * usNamePlate;

            // The PLimit and QLimits is a %. If a module on the inverter is disabled
            // this affect the limit
            double fpLimitDouble = (double)usPLimit;

            double fRealPowerRatio = (fpLimitDouble / 100);

            fActualRealPowerValue = fActualRealPowerValue * fRealPowerRatio;

            // Update the actual Real Power
            return fActualRealPowerValue;
        }

        // get Actual Reactive Power
        public override double getActualReactivePower(short sReactivePowerDemand)
        {
            double scaling = 10000;
            fActualReactivePowerValue = sReactivePowerDemand / scaling;

            // Nameplate is 2000kw for Power Electronics
            fActualReactivePowerValue = fActualReactivePowerValue * usNamePlate;

            // The PLimit and QLimits is a %. If a module on the inverter is disabled
            // this affect the limit
            double fqLimitDouble = (double)usQLimit;

            double fReactivePowerRatio = (fqLimitDouble / 100);

            fActualReactivePowerValue = fActualReactivePowerValue * fReactivePowerRatio;

            // Update the actual Reactie Power
            return fActualReactivePowerValue;
        }

        // Set the nameplate of the system
        public override void setNamePlate(ushort usInverterNamePlate)
        {
            usNamePlate = usInverterNamePlate;
        }

        //Set Nominal Voltage
        public override void setNominalACVoltage(ushort usInverterNominalVoltage)
        {
            usNominalACVoltage = usInverterNominalVoltage;
        }

        // Obtain Phase A Voltage
        public override ushort getPhaseAVoltage()
        {
            fPhaseAVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseAVoltage);
        }

        // Obtain Phase B Voltage
        public override ushort getPhaseBVoltage()
        {
            fPhaseBVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseBVoltage);
        }

        // Obtain Phase C Voltage
        public override ushort getPhaseCVoltage()
        {
            fPhaseCVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseCVoltage);
        }

        // Obtain Phase A Current
        public override double getPhaseACurrent()
        {
            fPhaseACurrent = ((fActualRealPowerValue * 1000) / fPhaseAVoltage);
            fPhaseACurrent /= Math.Sqrt(3);
            fPhaseACurrent *= 10; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseACurrent *= -1;
            }
            return fPhaseACurrent;
        }

        // Obtain Phase B Current
        public override double getPhaseBCurrent()
        {
            fPhaseBCurrent = ((fActualRealPowerValue * 1000) / fPhaseBVoltage);
            fPhaseBCurrent /= Math.Sqrt(3);
            fPhaseBCurrent *= 10; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseBCurrent *= -1;
            }
            return fPhaseBCurrent;
        }

        // Obtain Phase C Current
        public override double getPhaseCCurrent()
        {
            fPhaseCCurrent = ((fActualRealPowerValue * 1000) / fPhaseCVoltage);
            fPhaseCCurrent /= Math.Sqrt(3);
            fPhaseCCurrent *= 10; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseCCurrent *= -1;
            }
            return fPhaseCCurrent;
        }

        // Set P Limit
        public override void setPLimit(ushort plimit)
        {
            usPLimit = plimit;
        }

        // Set Q Limit
        public override void setQLimit(ushort qlimit)
        {
            usQLimit = qlimit;
        }
    }

    public class SmaInverter : Inverter
    {
        private double fActualRealPowerValue;
        private double fActualReactivePowerValue;
        private ushort usNamePlate;
        private ushort usNominalACVoltage;
        private double fPhaseAVoltage;
        private double fPhaseBVoltage;
        private double fPhaseCVoltage;
        private double fPhaseACurrent;
        private double fPhaseBCurrent;
        private double fPhaseCCurrent;
       
        private static ushort FAULTED = 1392;
        private static ushort RUNNING = 3526;
        private static ushort STOPPED = 381;
        private static ushort STARTED = 2;

        private static ushort NOMINAL_VOLTAGE = 374;
        private Ihi_inverter_modbus_class _oModClass;

        // Obtain Real/Active Power Demand
        public override JObject getDemandInverterRealPower()
        {
            JObject oTempRealPowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("Wspt","2");
            string strRealPowerCommand = (string)oTempRealPowerCommand["value"];
            short sRealPowerCommand = short.Parse(strRealPowerCommand);
            double scaling = 10000;
            double fRealPowerCommand = (double)sRealPowerCommand;
            double fActualRealPowerDemand = fRealPowerCommand / scaling;
            fActualRealPowerDemand = fActualRealPowerDemand * usNamePlate;
            short sReturnRealPower = (short)fActualRealPowerDemand;
            oTempRealPowerCommand["value"] = sReturnRealPower.ToString();
            return oTempRealPowerCommand;
        }

        // Obtain Reactive Power Demand
        public override JObject getDemandInverterReactivePower()
        {
            JObject oTempReactivePowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("VarSpt","2");
            string strReactivePowerCommand = (string)oTempReactivePowerCommand["value"];
            short sReactivePowerCommand = short.Parse(strReactivePowerCommand);
            double scaling = 10000;
            double fReactivePowerCommand = (double)sReactivePowerCommand;
            double fActualReactivePowerDemand = fReactivePowerCommand / scaling;
            fActualReactivePowerDemand = fActualReactivePowerDemand * usNamePlate;
            short sReturnReactivePower = (short)fActualReactivePowerDemand;
            oTempReactivePowerCommand["value"] = sReturnReactivePower.ToString();
            return oTempReactivePowerCommand;
        }

        // Obtain Actual Inverter Real Power
        public override JObject getActualInverterRealPower()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("InvMs.TotW(L)","3");
        }

        // Obtain Actual Reactive Power
        public override JObject getActualInverterReactivePower()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("InvMs.TotVar","3");
        }

        // Obtain Frequency
        public override JObject getFrequency()
        {
            JObject oTempFrequency = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("GriMs.Hz(L)","3");
            string strFrequency = (string)oTempFrequency["value"];
            double sFrequency = double.Parse(strFrequency);
            if (sFrequency != 0)
            {
                sFrequency = sFrequency / 100;
            }
            oTempFrequency["value"] = sFrequency.ToString();
            return oTempFrequency;
        }
       
        // Obtain DC Voltage
        public override JObject getDCVoltage()
        {
            JObject oTempDCVoltage = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("DcMs.Vol(L)", "3");
            string strDCVoltage = (string)oTempDCVoltage["value"];
            short sDCVoltage = short.Parse(strDCVoltage);
            double scaling = 10;
            double fDCVoltage = (double)sDCVoltage;
            fDCVoltage = fDCVoltage / scaling;
            oTempDCVoltage["value"] = fDCVoltage.ToString();

            return oTempDCVoltage;
        }

        // Obtain AC Voltage
        public override JObject getACVoltage()
        {
            dynamic ihi_register = new JObject();
            ihi_register.value  =  NOMINAL_VOLTAGE;

            return ihi_register;
        }

        // Obtain Phase A Voltage
        public override JObject getVoltagePhaseA()
        {
            JObject oTempPhaseAB = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("GriMs.V.PhsAB(L)", "3");
            string strPhaseAB = (string)oTempPhaseAB["value"];
            short sPhaseAB = short.Parse(strPhaseAB);
            double scaling = 10;
            double fPhaseAB = (double)sPhaseAB;
            fPhaseAB = fPhaseAB / scaling;
            oTempPhaseAB["value"] = fPhaseAB.ToString();

            return oTempPhaseAB;
        }
        // Obtain Phase Voltage
        public override JObject getVoltagePhaseB()
        {
            JObject oTempPhaseBC = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("GriMs.V.PhsBC(L)", "3");
            string strPhaseBC = (string)oTempPhaseBC["value"];
            short sPhaseBC = short.Parse(strPhaseBC);
            double scaling = 10;
            double fPhaseBC = (double)sPhaseBC;
            fPhaseBC = fPhaseBC / scaling;

            oTempPhaseBC["value"] = fPhaseBC.ToString();

            return oTempPhaseBC;
        }

        // Obtain Phase C Voltage
        public override JObject getVoltagePhaseC()
        {
            JObject oTempPhaseCA = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("GriMs.V.PhsCA(L)", "3");
            string strPhaseCA = (string)oTempPhaseCA["value"];
            short sPhaseCA = short.Parse(strPhaseCA);
            double scaling = 10;
            double fPhaseCA = (double)sPhaseCA;
            fPhaseCA = fPhaseCA / scaling;

            oTempPhaseCA["value"] = fPhaseCA.ToString();

            return oTempPhaseCA;
        }

        // Obtain Phase Current A
        public override JObject getCurrentPhaseA()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("InvMs.TotA.PhsA(L)", "3");
        }

        // Obtain Phase Current B
        public override JObject getCurrentPhaseB()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("InvMs.TotA.PhsB(L)", "3");
        }

        // Obtain Phase Current C
        public override JObject getCurrentPhaseC()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("InvMs.TotA.PhsC(L)", "3");
        }

        // Obtain Operational State
        public override JObject getOperationalState()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("OpStt(L)","3");
        }

        // Obtain Alarm value
        public override JObject getAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("ErrNo(L)","3");
        }

        // Obtain Warning Alarm
        public override JObject getWarningAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("warning","3");
        }

        // Set actual real power sent over REST API
        public override void updateActualInverterRealPower(string sValue)
        {
            // Set Actual Inverter Real Power   
            ushort usValue = (ushort)Int16.Parse(sValue);
            float value = (float)usValue / 100;
            int ActualValue = (ushort)value * 2000;
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotW(L)", usValue);
        }

        // Set demand real power sent over REST API
        public override void setDemandInverterRealPower(string sValue)
        {
            // Set Demand Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("Wspt", usValue);
        }

        // Set actual reactive power sent over REST API
        public override void updateActualInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Reactive Power
            ushort usValue = (ushort)Int16.Parse(sValue);

            float value = (float)usValue / 100;
            int ActualValue = (ushort)value * 2000;
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotVar(L)", usValue);
        }

        // Set demand reactive power sent over REST API
        public override void setDemandInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Reactive Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotVA(L)", usValue);
        }

        // Set frequency
        public override void updateFrequency(string sValue)
        {
            // Set Actual Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            int iValue = usValue * 100;
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.Hz(L)", (ushort)iValue);
        }

        // Set DC Voltage
        public override void updateDCVoltage(string strDCVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strDCVoltage);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("DcMs.Vol(L)", usValue);
        }

        // Set Phase A voltage
        public override void updatePhaseAVoltage(string strPhaseAVoltage)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseAVoltage);
            ushort usPhaseVoltageAB = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.V.PhsAB(L)", usPhaseVoltageAB);
        }

        // Set Phase B Voltage
        public override void updatePhaseBVoltage(string strPhaseBVoltage)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseBVoltage);
            ushort usPhaseVoltageBC = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.V.PhsBC(L)", usPhaseVoltageBC);
        }

        // Phase C Voltage
        public override void updatePhaseCVoltage(string strPhaseCVoltage)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseCVoltage);
            ushort usPhaseVoltageBC = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.V.PhsCA(L)", usPhaseVoltageBC);
        }

        // Set A->B Current
        public override void updatePhaseACurrent(string strPhaseACurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseACurrent);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotA.PhsA(L)", usValue);
        }

        // Set B->C Current
        public override void updatePhaseBCurrent(string strPhaseBCurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseBCurrent);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotA.PhsB(L)", usValue);
        }

        // Phase C->A Current
        public override void updatePhaseCCurrent(string strPhaseCCurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseCCurrent);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotA.PhsC(L)", usValue);
        }

        // Set Operational status of inverter
        public override void updateOperationalState(string strState)
        {
            ushort usValue = (ushort)Int16.Parse(strState);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("OpStt(L)", usValue);
        }

        // Set Alarm
        public override void updateAlarm(string strAlarm)
        {
            ushort usValue = (ushort)Int16.Parse(strAlarm);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("ErrNo(L)", usValue);
        }

        // Set Alarm
        public override void updateAlarm(string registerLabel, string strAlarm)
        {
         
        }

        // Set Warning Alarm
        public override void updateWarningAlarm(string strAlarm)
        {
            ushort usValue = (ushort)Int16.Parse(strAlarm);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("warning", usValue);
        }

        // Update Model Type
        public override void updateModelType(string strModelType)
        {
           
        }

        // Clear all PE alarms
        public override void clearAlarms()
        {
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("errNo(L)", 0);
        }
       
        public override void initialInverterConfiguration(Ihi_inverter_modbus_class oModClass)
        {
            // Set initial nominal voltage to 
            usNominalACVoltage = 348;
            _oModClass = oModClass;
        }

        public override ushort getFaultedState()
        {
            return FAULTED;
        }

        public override ushort getEnabledValue()
        {
            return RUNNING;
        }

        public override ushort getDisabledValue()
        {
            return STOPPED;
        }

        public override ushort getStartCommand()
        {
            return STARTED;
        }

        // Determine Actual Real Power
        public override double getActualRealPower(short sRealPowerDemand)
        {
            double scaling = 10000;
            fActualRealPowerValue = sRealPowerDemand / scaling;

            // Nameplate is 2000kw for Power Electronics
            fActualRealPowerValue = fActualRealPowerValue * usNamePlate;

            // Update the actual Real Power
            return fActualRealPowerValue;
        }

        // get Actual Reactive Power
        public override double getActualReactivePower(short sReactivePowerDemand)
        {
            double scaling = 10000;
            fActualReactivePowerValue = sReactivePowerDemand / scaling;
            fActualReactivePowerValue = fActualReactivePowerValue * usNamePlate;

            return fActualReactivePowerValue;
        }

        // Set the nameplate of the system
        public override void setNamePlate(ushort usInverterNamePlate)
        {
            usNamePlate = usInverterNamePlate;
        }

        //Set Nominal Voltage
        public override void setNominalACVoltage(ushort usInverterNominalVoltage)
        {
            usNominalACVoltage = usInverterNominalVoltage;
        }

        // Obtain Phase A Voltage
        public override ushort getPhaseAVoltage()
        {
            fPhaseAVoltage = (usNominalACVoltage) / Math.Sqrt(2);

            return Convert.ToUInt16(fPhaseAVoltage * 10);
        }

        // Obtain Phase B Voltage
        public override ushort getPhaseBVoltage()
        {
            fPhaseBVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseBVoltage * 10);
        }

        // Obtain Phase C Voltage
        public override ushort getPhaseCVoltage()
        {
            fPhaseCVoltage = (usNominalACVoltage) / Math.Sqrt(2);

            return Convert.ToUInt16(fPhaseCVoltage * 10 );
        }

        // Obtain Phase A Current
        public override double getPhaseACurrent()
        {
            fPhaseACurrent = ((fActualRealPowerValue * 1000) / fPhaseAVoltage);
            fPhaseACurrent /= Math.Sqrt(3);
            fPhaseACurrent *= 1; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseACurrent *= -1;
            }

            return fPhaseACurrent;
        }

        // Obtain Phase B Current
        public override double getPhaseBCurrent()
        {
            fPhaseBCurrent = ((fActualRealPowerValue * 1000) / fPhaseBVoltage);
            fPhaseBCurrent /= Math.Sqrt(3);
            fPhaseBCurrent *= 1; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseBCurrent *= -1;
            }

            return fPhaseBCurrent;
        }

        // Obtain Phase C Current
        public override double getPhaseCCurrent()
        {
            fPhaseCCurrent = ((fActualRealPowerValue * 1000) / fPhaseCVoltage);
            fPhaseCCurrent /= Math.Sqrt(3);
            fPhaseCCurrent *= 1; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseCCurrent *= -1;
            }

            return fPhaseCCurrent;
        }

        // Set P Limit
        public override void setPLimit(ushort plimit)
        {
        }

        // Set Q Limit
        public override void setQLimit(ushort qlimit)
        {
        }
    }
    public class TmeicInverter : Inverter
    {
        private double fActualRealPowerValue;
        private double fActualReactivePowerValue;
        private ushort usNamePlate;
        private ushort usNominalACVoltage;
        private double fPhaseAVoltage;
        private double fPhaseBVoltage;
        private double fPhaseCVoltage;
        private double fPhaseACurrent;
        private double fPhaseBCurrent;
        private double fPhaseCCurrent;

        private static ushort FAULTED = 1392;
        private static ushort RUNNING = 0x1;
        private static ushort STOPPED = 0x0;
        private static ushort STARTED = 2;

        private static ushort NOMINAL_VOLTAGE = 450;
        private Ihi_inverter_modbus_class _oModClass;

        // Obtain Real/Active Power Demand
        public override JObject getDemandInverterRealPower()
        {
            JObject oTempRealPowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("realpwrcmd", "1");
            string strRealPowerCommand = (string)oTempRealPowerCommand["value"];
            short sRealPowerCommand = short.Parse(strRealPowerCommand);
            double scaling = 10;
            double fRealPowerCommand = (double)sRealPowerCommand;
            double fActualRealPowerDemand = fRealPowerCommand / scaling;
           
            short sReturnRealPower = (short)fActualRealPowerDemand;
            oTempRealPowerCommand["value"] = sReturnRealPower.ToString();
            return oTempRealPowerCommand;
        }

        // Obtain Reactive Power Demand
        public override JObject getDemandInverterReactivePower()
        {
            JObject oTempReactivePowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("reactivepwrcmd", "1");
            string strReactivePowerCommand = (string)oTempReactivePowerCommand["value"];
            short sReactivePowerCommand = short.Parse(strReactivePowerCommand);
            double scaling = 10;
            double fReactivePowerCommand = (double)sReactivePowerCommand;
            double fActualReactivePowerDemand = fReactivePowerCommand / scaling;
            
            short sReturnReactivePower = (short)fActualReactivePowerDemand;
            oTempReactivePowerCommand["value"] = sReturnReactivePower.ToString();
            return oTempReactivePowerCommand;
        }

        // Obtain Actual Inverter Real Power
        public override JObject getActualInverterRealPower()
        {
            JObject oRealPower=_oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("realpwrcmd", "1");
            string strRealPower = (string)oRealPower["value"];
            double sRealPower = double.Parse(strRealPower);
            if (sRealPower != 0)
            {
                sRealPower = sRealPower / 10;
            }
            oRealPower["value"] = sRealPower.ToString();
            return oRealPower;
        }

        // Obtain Actual Reactive Power
        public override JObject getActualInverterReactivePower()
        {
            
            JObject oReactivePower = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("reactivepwrcmd", "1");
            string strReactivePower = (string)oReactivePower["value"];
            double sReactivePower = double.Parse(strReactivePower);
            if (sReactivePower != 0)
            {
                sReactivePower = sReactivePower / 10;
            }
            oReactivePower["value"] = sReactivePower.ToString();
            return oReactivePower;
        }

        // Obtain Frequency
        public override JObject getFrequency()
        {
            JObject oTempFrequency = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("hz", "1");
            string strFrequency = (string)oTempFrequency["value"];
            double sFrequency = double.Parse(strFrequency);
            if (sFrequency != 0)
            {
                sFrequency = sFrequency / 100;
            }
            oTempFrequency["value"] = sFrequency.ToString();
            return oTempFrequency;
        }

        // Obtain DC Voltage
        public override JObject getDCVoltage()
        {
            JObject oTempDCVoltage = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("dcvoltage", "1");
            string strDCVoltage = (string)oTempDCVoltage["value"];
            short sDCVoltage = short.Parse(strDCVoltage);
            double scaling = 10;
            double fDCVoltage = (double)sDCVoltage;
            fDCVoltage = fDCVoltage / scaling;
            oTempDCVoltage["value"] = fDCVoltage.ToString();

            return oTempDCVoltage;
        }

        // Obtain AC Voltage
        public override JObject getACVoltage()
        {
            dynamic ihi_register = new JObject();
            ihi_register.value = NOMINAL_VOLTAGE;

            return ihi_register;
        }

        // Obtain Phase A Voltage
        public override JObject getVoltagePhaseA()
        {
            JObject oTempPhaseAB = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("gridvoltageab", "1");
            string strPhaseAB = (string)oTempPhaseAB["value"];
            short sPhaseAB = short.Parse(strPhaseAB);
            double scaling = 10;
            double fPhaseAB = (double)sPhaseAB;
            fPhaseAB = fPhaseAB / scaling;
            oTempPhaseAB["value"] = fPhaseAB.ToString();

            return oTempPhaseAB;
        }
        // Obtain Phase Voltage
        public override JObject getVoltagePhaseB()
        {
            JObject oTempPhaseBC = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("gridvoltagebc", "1");
            string strPhaseBC = (string)oTempPhaseBC["value"];
            short sPhaseBC = short.Parse(strPhaseBC);
            double scaling = 10;
            double fPhaseBC = (double)sPhaseBC;
            fPhaseBC = fPhaseBC / scaling;

            oTempPhaseBC["value"] = fPhaseBC.ToString();

            return oTempPhaseBC;
        }

        // Obtain Phase C Voltage
        public override JObject getVoltagePhaseC()
        {
            JObject oTempPhaseCA = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("gridvoltageca", "1");
            string strPhaseCA = (string)oTempPhaseCA["value"];
            short sPhaseCA = short.Parse(strPhaseCA);
            double scaling = 10;
            double fPhaseCA = (double)sPhaseCA;
            fPhaseCA = fPhaseCA / scaling;

            oTempPhaseCA["value"] = fPhaseCA.ToString();

            return oTempPhaseCA;
        }

        // Obtain Phase Current A
        public override JObject getCurrentPhaseA()
        {
           
            JObject oTempCurrentA = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("gridcurrenta", "1");
            string strCurrentA = (string)oTempCurrentA["value"];
            short sPhaseA = short.Parse(strCurrentA);
            double scaling = 10;
            double fPhaseA = (double)sPhaseA;
            fPhaseA = fPhaseA / scaling;

            oTempCurrentA["value"] = fPhaseA.ToString();

            return oTempCurrentA;

        }

        // Obtain Phase Current B
        public override JObject getCurrentPhaseB()
        {
            JObject oTempCurrentB = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("gridcurrentb", "1");
            string strCurrentB = (string)oTempCurrentB["value"];
            short sPhaseB  = short.Parse(strCurrentB);
            double scaling = 10;
            double fPhaseB = (double)sPhaseB;
            fPhaseB = fPhaseB / scaling;

            oTempCurrentB["value"] = fPhaseB.ToString();

            return oTempCurrentB;
        }

        // Obtain Phase Current C
        public override JObject getCurrentPhaseC()
        {
            JObject oTempCurrentC = _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("gridcurrentc", "1");
            string strCurrentC = (string)oTempCurrentC["value"];
            short sPhaseC = short.Parse(strCurrentC);
            double scaling = 10;
            double fPhaseC = (double)sPhaseC;
            fPhaseC = fPhaseC / scaling;

            oTempCurrentC["value"] = fPhaseC.ToString();

            return oTempCurrentC;
        }

        // Obtain Operational State
        public override JObject getOperationalState()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("statusbit1(L)", "1");
        }

        // Obtain Alarm value
        public override JObject getAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("fault", "1");
        }

        // Obtain Warning Alarm
        public override JObject getWarningAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_INPUT_REGISTER("warning", "1");
        }

        // Set actual real power sent over REST API
        public override void updateActualInverterRealPower(string sValue)
        {
            // Set Actual Inverter Real Power   
            int usValue = Int16.Parse(sValue);
            int actualRealPowerValue = usValue * 10;
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("activepowercmd", (ushort)actualRealPowerValue);
        }

        // Set demand real power sent over REST API
        public override void setDemandInverterRealPower(string sValue)
        {
            // Set Demand Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("reactivepowercmd", usValue);
        }

        // Set actual reactive power sent over REST API
        public override void updateActualInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Reactive Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            int actualReactivePowerValue = usValue * 10;
       
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("reactivepowercmd", (ushort)actualReactivePowerValue);
        }

        // Set demand reactive power sent over REST API
        public override void setDemandInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Reactive Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("reactivepowercmd", usValue);
        }

        // Set frequency
        public override void updateFrequency(string sValue)
        {
            // Set Actual Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            int iValue = usValue * 100;
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("hz", (ushort)iValue);
        }

        // Set DC Voltage
        public override void updateDCVoltage(string strDCVoltage)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strDCVoltage);
            ushort usPhaseVoltageAB = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("dcvoltage", usPhaseVoltageAB);
        }

        // Set Phase A voltage
        public override void updatePhaseAVoltage(string strPhaseAVoltage)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseAVoltage);
            ushort usPhaseVoltageAB = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("gridvoltageab", usPhaseVoltageAB);
        }

        // Set Phase B Voltage
        public override void updatePhaseBVoltage(string strPhaseBVoltage)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseBVoltage);
            ushort usPhaseVoltageBC = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("gridvoltagebc", usPhaseVoltageBC);
        }

        // Phase C Voltage
        public override void updatePhaseCVoltage(string strPhaseCVoltage)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseCVoltage);
            ushort usPhaseVoltageBC = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("gridvoltageca", usPhaseVoltageBC);
        }

        // Set A->B Current
        public override void updatePhaseACurrent(string strPhaseACurrent)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseACurrent);
            ushort usPhaseCurrentA = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("gridcurrenta", usPhaseCurrentA);
        }

        // Set B->C Current
        public override void updatePhaseBCurrent(string strPhaseBCurrent)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseBCurrent);
            ushort usPhaseCurrentB = (ushort)(usValue * scaling);
           
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("gridcurrentb", usPhaseCurrentB);
        }

        // Phase C->A Current
        public override void updatePhaseCCurrent(string strPhaseCCurrent)
        {
            ushort scaling = 10;
            ushort usValue = (ushort)Int16.Parse(strPhaseCCurrent);
            ushort usPhaseCurrentC = (ushort)(usValue * scaling);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("gridcurrentc", usPhaseCurrentC);
        }

        // Set Operational status of inverter
        public override void updateOperationalState(string strState)
        {
            ushort usValue = (ushort)Int16.Parse(strState);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit1", usValue);
        }

        // Set Alarm
        public override void updateAlarm(string registerLabel, string strAlarm)
        {
            string strActualRegisterLabel = "";

            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("fault", 1);
            switch (registerLabel)
            {
                case "alarm0":
                    {
                        strActualRegisterLabel = "statusbit3word0";
                        break;
                    }
                case "alarm1":
                    {
                        strActualRegisterLabel = "statusbit3word1";
                        break;
                    }
                case "alarm2":
                    {
                        strActualRegisterLabel = "statusbit3word2";
                        break;
                    }
                case "alarm3":
                    {
                        strActualRegisterLabel = "statusbit3word3";
                        break;
                    }
                case "alarm4":
                    {
                        strActualRegisterLabel = "statusbit3word4";
                        break;
                    }
                case "alarm5":
                    {
                        strActualRegisterLabel = "statusbit3word5";
                        break;
                    }
                case "alarm6":
                    {
                        strActualRegisterLabel = "statusbit3word6";
                        break;
                    }
                case "alarm7":
                    {
                        strActualRegisterLabel = "statusbit3word7";
                        break;
                    }
                default:
                    break;
            }

            // Register Offset in Label
            int iOffsetBitWiseShift = int.Parse(strAlarm);
            if (iOffsetBitWiseShift > 15)
            {
                iOffsetBitWiseShift = 31- iOffsetBitWiseShift;
                iOffsetBitWiseShift = 16 - (iOffsetBitWiseShift + 1);
                int i32uAlarmValue = 1 << iOffsetBitWiseShift;
                _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL(strActualRegisterLabel + "(H)", (ushort)i32uAlarmValue);
            }
            else
            {
                int i32uAlarmValue = 1 << iOffsetBitWiseShift;
                _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL( strActualRegisterLabel+ "(L)", (ushort)i32uAlarmValue);
            }
        }

        // Set Alarm
        public override void updateAlarm(string strAlarm)
        { 
         
        }

        // Set Warning Alarm
        public override void updateWarningAlarm(string strAlarm)
        {
            ushort usValue = (ushort)Int16.Parse(strAlarm);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("warning", usValue);
        }

        // Update Model Type
        public override void updateModelType(string strModelType)
        {

        }

        // Clear all PE alarms
        public override void clearAlarms()
        {
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("fault", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word0(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word0(L)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word1(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word1(L)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word2(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word2(L)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word3(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word3(L)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word4(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word4(L)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word5(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word5(L)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word6(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word6(L)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word7(H)", 0);
            _oModClass.IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("statusbit3word7(L)", 0);
        }

        public override void initialInverterConfiguration(Ihi_inverter_modbus_class oModClass)
        {
            // Set initial nominal voltage to 
            usNominalACVoltage = 348;
            _oModClass = oModClass;
        }

        public override ushort getFaultedState()
        {
            return FAULTED;
        }

        public override ushort getEnabledValue()
        {
            return RUNNING;
        }

        public override ushort getDisabledValue()
        {
            return STOPPED;
        }

        public override ushort getStartCommand()
        {
            return STARTED;
        }

        // Determine Actual Real Power
        public override double getActualRealPower(short sRealPowerDemand)
        {
            fActualRealPowerValue = sRealPowerDemand;

            // Update the actual Real Power
            return fActualRealPowerValue;
        }

        // get Actual Reactive Power
        public override double getActualReactivePower(short sReactivePowerDemand)
        {
            fActualReactivePowerValue = sReactivePowerDemand;
            
            return fActualReactivePowerValue;
        }

        // Set the nameplate of the system
        public override void setNamePlate(ushort usInverterNamePlate)
        {
            usNamePlate = usInverterNamePlate;
        }

        //Set Nominal Voltage
        public override void setNominalACVoltage(ushort usInverterNominalVoltage)
        {
            usNominalACVoltage = usInverterNominalVoltage;
        }

        // Obtain Phase A Voltage
        public override ushort getPhaseAVoltage()
        {
            fPhaseAVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseAVoltage * 10);
        }

        // Obtain Phase B Voltage
        public override ushort getPhaseBVoltage()
        {
            fPhaseBVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseBVoltage * 10);
        }

        // Obtain Phase C Voltage
        public override ushort getPhaseCVoltage()
        {
            fPhaseCVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseCVoltage * 10);
        }

        // Obtain Phase A Current
        public override double getPhaseACurrent()
        {
            fPhaseACurrent = ((fActualRealPowerValue * 1000) / fPhaseAVoltage);
            fPhaseACurrent /= Math.Sqrt(3);
            fPhaseACurrent *= 1; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseACurrent *= -1;
            }

            return fPhaseACurrent;
        }

        // Obtain Phase B Current
        public override double getPhaseBCurrent()
        {
            fPhaseBCurrent = ((fActualRealPowerValue * 1000) / fPhaseBVoltage);
            fPhaseBCurrent /= Math.Sqrt(3);
            fPhaseBCurrent *= 1; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseBCurrent *= -1;
            }

            return fPhaseBCurrent;
        }

        // Obtain Phase C Current
        public override double getPhaseCCurrent()
        {
            fPhaseCCurrent = ((fActualRealPowerValue * 1000) / fPhaseCVoltage);
            fPhaseCCurrent /= Math.Sqrt(3);
            fPhaseCCurrent *= 1; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseCCurrent *= -1;
            }

            return fPhaseCCurrent;
        }

        // Set P Limit
        public override void setPLimit(ushort plimit)
        {
        }

        // Set Q Limit
        public override void setQLimit(ushort qlimit)
        {
        }
    }
}