﻿using System.Web;
using System.Web.Mvc;

namespace IHI_SIMULATOR_INVERTER_TMEIC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
