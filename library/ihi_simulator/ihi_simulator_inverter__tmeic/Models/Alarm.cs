﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IHI_SIMULATOR_INVERTER_TMEIC.Models
{
    public class Alarm
    {
        public string   id           { get; set; }   // Alarm Index
        public string   bitpos       { get; set; }
        public string   register     { get; set; }
        public string   description  { get; set; }   // Description
        public string   state        { get; set; }  // Trigger Alarm
    }
}