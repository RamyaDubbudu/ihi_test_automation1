﻿/*=============================================================================
Name        : Ihi_inverter_simulator.cs
Author      : G.Clark
Version     : 1.0
Copyright   : Copyright (c) 2016 IHI, Inc.  All Rights Reserved
Description : The Ihi_inverter_simulator.

This class maps incomming REST API requests GET/PUT to a modbus register
map. It communicates to the inverter connector via modbus.
By default it acts like the inverter and simulates actions that
the inverter would make to bring it online/offline and returns
active real and reactive power values based in demand from ES Pilot.
External REST API request can still be processed while still connected 
to ES Pilot.
=============================================================================*/

using System;
using System.Collections.Generic;
using System.Threading;
using System.Net.Sockets;
using Nerdle.AutoConfig;
using Modbus.Device;
using Modbus.Data;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using IHI_SIMULATOR_INVERTER_SMA.Models;

using NModbus;

#pragma warning disable 1591

namespace IHI_SIMULATOR_INVERTER_SMA
{

    public class Ihi_inverter_modbus_class
    {
        public static string strChannelState;
        public static Boolean bConnectToSlave = false;
        private System.Timers.Timer m_timerSequenceNumber;

        // Get properties of ihiModbusObject
        public interface IihiModbusObject
        {
            int index { get; set; }   // Modbus Address
            string type { get; set; }   // Holding registers for Power Electronics 
            string slaveid { get; set; }   // Current Slave Id of the system
            string label { get; set; }   // Modbus Address Label
            string mandatory { get; set; }   // Modbus Adddress shows if used by the system
        }

        // Get properties of Modbus Input Object Collection
        public interface IIhiModbusHoldingObjectCollection
        {
            IEnumerable<IihiModbusObject> ihiModbusObjects { get; }
        }

        // Get properties of Modbus Output Object Collection
        public interface IIhiModbusInputObjectCollection
        {
            IEnumerable<IihiModbusObject> ihiModbusObjects { get; }
        }

        // Get properties of ihiAlarmObject
        public interface IihiAlarmObject
        {
            string id { get; set; }   // Alarm id
            string description { get; set; }   // Alarm Description 
            string severity { get; set; }   // Alarm Severity
        }
        // Get properties of ihiAlarmObject
        public interface IihiAlarmWarningObject
        {
            string id { get; set; }   // Alarm id
            string description { get; set; }   // Alarm Description 
            string severity { get; set; }   // Alarm Severity
        }


        // Get properties of Alarm Object Collection
        public interface IIhiAlarmWarningObjectCollection
        {
            IEnumerable<IihiAlarmWarningObject> ihiAlarmWarningObjects { get; }
        }
      
        // Get properties of Alarm Object Collection
        public interface IIhiAlarmObjectCollection
        {
            IEnumerable<IihiAlarmObject> ihiAlarmObjects { get; }
        }

        public static ModbusSlave[] ModbusSlave = new ModbusSlave[1];
        public Thread[] slaveThreads = new Thread[1];

        public static TcpListener server;
        public static int iSlaveCount;
        public static string m_StrCurrentSlaveId;
        public static List<JObject> Current_RegisterHoldingList;
        public static List<JObject> current_registerHoldingList;
        public static List<JObject> Current_RegisterInputList;
        public static List<JObject> current_registerInputList;
        public static JObject oIHI_Registerlists;
        public static JObject oIhiInverterRegisterValue;
        public static List<JObject> Current_AlarmList;
        public static List<JObject> Current_AlarmWarningList;
        public static ushort usHeartbeat;
        public static ushort usLastheartbeat;
        public static UInt32 i32uTimeOutCounter;
        private static UInt32 usModbusCommunicationTimeout;
        public static InverterSystem oInverterSystemObject = new InverterSystem();
        private static string strHeartBeatCheckingCtrl; // Determine whether to take action on heartbeat from inverter connector ON/OFF
        public static string strAutonomous;

        private ushort usNew_Command;
        private ushort usOld_Command;
        private ushort usNew_ResetCommand;
        private ushort old_ResetCommand;
        private short old_RealPowerCommand;
        private short sCurrent_RealPowerCommand;
        private short old_ReactivePowerCommand;
        private short sCurrent_ReactivePowerCommand;
        private ushort usNew_CurrentR;
        private ushort usNew_CurrentS;
        private ushort usNew_CurrentT;
        private ushort usNew_PhaseVoltageAB;
        private ushort usNew_PhaseVoltageBC;
        private ushort usNew_PhaseVoltageCA;
        private ushort usNew_usFrequency;
        private ushort usNew_DCVoltage;
        private ushort usOld_CurrentR;
        private ushort usOld_CurrentS;
        private ushort usOld_CurrentT;
        private ushort usOld_VoltageAB;
        private ushort usOld_VoltageBC;
        private ushort usOld_VoltageCA;
        private ushort usOld_Frequency;
        private ushort usNew_NominalACVoltage;
        private ushort usOld_CurrentStatus;
        private ushort usNew_CurrentStatus;
        private ushort usInverterNameplate;
        private static Inverter oInverterInstance;
        private ushort usCurrentModelType;
      
        IModbusSlave slave1;
        IModbusSlave slave2;
        static IModbusFactory factory;
        static IModbusSlaveNetwork modbusSlaveNetwork;

        public class ModbusRegister
        {
            public bool bChanged;
            public ushort usValue { get; set; }
            public ushort iOffset { get; set; }
            public byte i32uSlaveId { get; set; }
            public string strLabel { get; set; }
        }

        public static List<ModbusRegister> auInputRegisters = new List<ModbusRegister>();
        public static List<ModbusRegister> auHoldingRegisters = new List<ModbusRegister>();

        // Initallize the Modbus Server
        public void IHI_INVERTER_SIMULATION_INITIALIZATION()
        {
            System.Diagnostics.Trace.WriteLine("Enter IHI_INVERTER_SIMULATION_INITIALIZATION");

            // Reset any write holding/Input Register List
            auHoldingRegisters.Clear();
            auInputRegisters.Clear();

            var settings = System.Configuration.ConfigurationManager.AppSettings;
            oInverterSystemObject.company   = settings["company"];
            oInverterSystemObject.product   = settings["product"];
            oInverterSystemObject.nameplate = settings["nameplate"];
            oInverterSystemObject.modeltype = settings["modeltype"];
            string strModbusId              = settings["modbusport"];
           
            // Assign modbus port id
            ushort usModbusPortId = ushort.Parse(strModbusId);

            // Model Type
            usCurrentModelType = ushort.Parse(oInverterSystemObject.modeltype);
          
            // Instantiate Power Electronics Inverter Object
            oInverterInstance = new SmaInverter();

            // Create a Tcp Listener and bind that to the Modbus Slave
            server = new TcpListener(System.Net.IPAddress.Any, usModbusPortId);

            // Start Modbus Server
            server.Start();

            // Initialize the Timeout Counters
            i32uTimeOutCounter = 0;
            usHeartbeat = 0;
            usLastheartbeat = 0;

            // Turn heartbeat off
            strChannelState = "NOT CONNECTED";

            // Default turn heartbeat checking off
            strHeartBeatCheckingCtrl = "OFF";

            usModbusCommunicationTimeout = 10; // Number of seconds before comms considered gone
            oInverterSystemObject.alarmed = "NOFAULT";
            oInverterSystemObject.communication = "NOT CONNECTED";
            oInverterSystemObject.status = "SHUTDOWN";

            // Inverter State Autonomous meaning it will running and 
            // can go online without any intervention from user
            strAutonomous = "TRUE";

            // Power Commands
            sCurrent_RealPowerCommand = 0;
            old_RealPowerCommand = 0;
            sCurrent_ReactivePowerCommand = 0;
            old_ReactivePowerCommand = 0;

            // New Add Variation
            usNew_CurrentR = 0;
            usNew_CurrentS = 0;
            usNew_CurrentT = 0;
            usNew_PhaseVoltageAB = 530; // Volts
            usNew_PhaseVoltageBC = 530; // Volts
            usNew_PhaseVoltageCA = 530; // Volts
            usNew_DCVoltage = 0;
            usNew_usFrequency = 6000;

            // Create TCP Client
            server.BeginAcceptTcpClient(new AsyncCallback(IHI_ACCEPT_CLIENT), this);

            // Bind the TcpListener to the modbus slave
            ModbusSlave[0] = ModbusTcpSlave.CreateTcp(1, server);

            factory = new ModbusFactory();

            modbusSlaveNetwork = factory.CreateSlaveNetwork(server);

            // Create the 2 slaves
            slave1 = factory.CreateSlave(2);
            slave2 = factory.CreateSlave(3);


            modbusSlaveNetwork.AddSlave(slave1);
            modbusSlaveNetwork.AddSlave(slave2);
            modbusSlaveNetwork.ListenAsync();

            // Create the data store
            //ModbusSlave[0].DataStore = DataStoreFactory.CreateDefaultDataStore();
            //ModbusSlave[0].DataStore.DataStoreReadFrom += new EventHandler<DataStoreEventArgs>(Modbus_DataStoreReadFrom);

            // Obtain the modbus slave thread
            //slaveThreads[0] = new Thread(modbusSlaveNetwork.);

            // Start the modbus start listening
            //slaveThreads[0].Start();

            // Create JSON Object register lists
            Current_RegisterHoldingList = new List<JObject>();

            Current_RegisterInputList = new List<JObject>();
            Current_AlarmList = new List<JObject>();
            Current_AlarmWarningList = new List<JObject>();
            oIHI_Registerlists = new JObject();
            oIhiInverterRegisterValue = new JObject();

            // Add Holding Registers
            // Obtain the Modbus Holding collection from the appConfig
            var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

            // Cycle through the Holding Modbus Collection
            foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
            {
                // Determine modbus offset
                int i32uModbusOffset = modbusObject.index;
                byte bSlaveId = 0;
                bool result =byte.TryParse(modbusObject.slaveid, out bSlaveId);

                // Assign the modbus register value at given offset

                ModbusRegister modbusReg = new ModbusRegister();
                modbusReg.usValue       = 0;
                modbusReg.iOffset       = (ushort) i32uModbusOffset;
                modbusReg.bChanged      = false;
                modbusReg.strLabel      = modbusObject.label;
                modbusReg.i32uSlaveId   = bSlaveId;

                // Append onto the list
                auHoldingRegisters.Add(modbusReg);
            }

            // Add Holding Registers
            // Obtain the Modbus Holding collection from the appConfig
            var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

            // Cycle through the Input Modbus Collection
            foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
            {
                // Determine modbus offset
                int i32uModbusOffset = modbusObject.index;
                byte bSlaveId = 0;
                bool result = byte.TryParse(modbusObject.slaveid, out bSlaveId);

                // Assign the modbus register value at given offset

                ModbusRegister modbusReg = new ModbusRegister();
                modbusReg.usValue = 0;
                modbusReg.iOffset = (ushort)i32uModbusOffset;
                modbusReg.bChanged = false;
                modbusReg.strLabel = modbusObject.label;
                modbusReg.i32uSlaveId = bSlaveId;

                // Append onto the list
                auInputRegisters.Add(modbusReg);
            }

            // save Name Plate
            usInverterNameplate = ushort.Parse(oInverterSystemObject.nameplate);
            oInverterInstance.setNamePlate(usInverterNameplate);

            // Cycle through the alarms and obtain a description   
            var alarmObjectCollection = AutoConfig.Map<IIhiAlarmObjectCollection>();

            // Cycle through the Alarm Register List
            foreach (IihiAlarmObject alarmObject in alarmObjectCollection.ihiAlarmObjects)
            {
                JObject oAlarm = new JObject();
                oAlarm["id"] = alarmObject.id;
                oAlarm["description"] = alarmObject.description;
                oAlarm["state"] = "OFF";
                Current_AlarmList.Add(oAlarm);
            }

            // Cycle through the alarms and obtain a description   
            var warningObjectCollection = AutoConfig.Map<IIhiAlarmWarningObjectCollection>();

            // Cycle through the Alarm Register List
            foreach (IihiAlarmWarningObject alarmObject in warningObjectCollection.ihiAlarmWarningObjects)
            {
                JObject oAlarm = new JObject();
                oAlarm["id"] = alarmObject.id;
                oAlarm["description"] = alarmObject.description;
                oAlarm["state"] = "OFF";
                Current_AlarmWarningList.Add(oAlarm);
            }
        }

        // Pass the label of the register you want to update
        private int getModbusRegisterOffset(string strLabel, string type)
        {
            // Cycle through the Holding Modbus Collection
            int i32sOffset = -1;

            // Obtain configuration registers lists
            if (type == "holding")
            {
                var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();
                foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                {
                    // Determine if matched register label
                    if (strLabel == modbusObject.label)
                    {
                        // Set offset to matched index
                        i32sOffset = modbusObject.index;
                        break;
                    }
                }
                if (i32sOffset == -1)
                {
                    System.Diagnostics.Trace.WriteLine("Enter getModbusRegisterOffset Holding Not found:" + strLabel);
                }
            }
            else
            {
                var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();
                foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
                {
                    // Determine if matched register label
                    if (strLabel == modbusObject.label)
                    {
                        // Set offset to matched index
                        i32sOffset = modbusObject.index;
                        break;
                    }
                }
                if (i32sOffset == -1)
                {
                    System.Diagnostics.Trace.WriteLine("Enter getModbusRegisterOffset Input Not found:" + strLabel);
                }
            }
            return i32sOffset;
        }

        // Pass the label of the register you want to update
        private byte getModbusRegisterSlaveId(string strLabel, string type)
        {
            // Cycle through the Holding Modbus Collection
            byte bSlaveId = 0;

            // Obtain configuration registers lists
            if (type == "holding")
            {
                var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();
                foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                {
                    // Determine if matched register label
                    if (strLabel == modbusObject.label)
                    {
                        // Set offset to matched index
                       
                        bool result = byte.TryParse(modbusObject.slaveid, out bSlaveId);
                        break;
                    }
                }
                if (bSlaveId == 0)
                {
                    System.Diagnostics.Trace.WriteLine("Enter getModbusRegisterOffset Holding Not found:" + strLabel);
                }
            }
            else
            {
                var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();
                foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
                {
                    // Determine if matched register label
                    if (strLabel == modbusObject.label)
                    {
                        // Set offset to matched index
                        bool result = byte.TryParse(modbusObject.slaveid, out bSlaveId);
                        break;
                    }
                }
                if (bSlaveId == 0)
                {
                    System.Diagnostics.Trace.WriteLine("Enter getModbusRegisterOffset Input Not found:" + strLabel);
                }
            }
            return bSlaveId;
        }

        // Obtain slave id of register
        private byte getModbusRegisterSlaveId(string strLabel)
        {
            // Cycle through the Holding Modbus Collection
            byte i32sSlaveId = 0;

            // Obtain configuration registers lists
            var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();
            foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
            {
                // Determine if matched register label
                if (strLabel == modbusObject.label)
                {
                    // Set offset to matched index
                    bool result= byte.TryParse(modbusObject.slaveid, out i32sSlaveId);
                    break;
                }
            }
            if (i32sSlaveId == 0)
            {
                System.Diagnostics.Trace.WriteLine("Enter getModbusRegisterOffset Not found:" + strLabel);
            }

            return i32sSlaveId;
        }

        // Obtain slave id of register
        private byte getModbusInputRegisterSlaveId(string strLabel)
        {
            // Cycle through the Holding Modbus Collection
            byte i32sSlaveId = 0;

            // Obtain configuration registers lists
            var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();
            foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
            {
                // Determine if matched register label
                if (strLabel == modbusObject.label)
                {
                    // Set offset to matched index
                    bool result = byte.TryParse(modbusObject.slaveid, out i32sSlaveId);
                    break;
                }
            }
            if (i32sSlaveId == 0)
            {
                System.Diagnostics.Trace.WriteLine("Enter getModbusInputRegisterSlaveId Not found:" + strLabel);
            }
            return i32sSlaveId;
        }

        // Control the writes to the modbus interface
        private void IHI_READ_WRITE_HOLDING_MODBUS_REGISTER(ushort offset, string action, ushort usTargetValue, byte slaveId, ref ushort outValue)
        {
            IModbusSlave slave = modbusSlaveNetwork.GetSlave(slaveId);
            if (action == "UPDATE")
            {
                try
                {
                    IPointSource<ushort> holdingRegisters = slave.DataStore.HoldingRegisters;
                    ushort[] point = new ushort[1];
                    point[0] = usTargetValue;
                    holdingRegisters.WritePoints(offset, point);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("Enter WRITE actOnModbusRegister FAILED:" + e.Message);
                }
            }
            if (action == "READ")
            {
                try
                {
                    IPointSource<ushort> holdingRegisters = slave.DataStore.HoldingRegisters;
                    outValue = ModbusSlave[0].DataStore.HoldingRegisters[offset];
                    ushort[] readlist = holdingRegisters.ReadPoints(offset, 1);
                    outValue = readlist[0];
                }

                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("Enter READ Holding Register:FAILED" + e.Message);
                }
            }
        }

        // Control the writes to the modbus interface
        private void IHI_READ_WRITE_INPUT_MODBUS_REGISTER(ushort offset, string action, ushort usTargetValue, byte slaveId, ref ushort outValue)
        {
            IModbusSlave slave = modbusSlaveNetwork.GetSlave(slaveId);
            if (action == "UPDATE")
            {
                try
                { 
                    IPointSource<ushort> inputRegisters = slave.DataStore.InputRegisters;

                    ushort[] point = new ushort[1];
                    point[0] = usTargetValue;
                    inputRegisters.WritePoints(offset, point);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("Enter WRITE actOnModbusRegister FAILED:" + e.Message);
                }
            }
            if (action == "READ")
            {
                try
                {
                    IPointSource<ushort> inputRegisters = slave.DataStore.InputRegisters;
                   
                    ushort[] point = new ushort[1];
                    ushort[] readlist = inputRegisters.ReadPoints(offset, 1);
                    outValue = readlist[0];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("Enter READ Input Register:FAILED" + e.Message);
                }
            }
        }

        // Update the modbus register
        public void IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL(string registerLabel, ushort usValue)
        {
            int i32uModbusOffset = getModbusRegisterOffset(registerLabel,"holding");
            byte i32uSlaveId = getModbusRegisterSlaveId(registerLabel);
          
            // Determine if already in List
            int index = auHoldingRegisters.FindIndex(item => (item.iOffset == i32uModbusOffset && item.i32uSlaveId ==i32uSlaveId));

            // Register exists in the Holding Register List
            if (index >= 0)
            {
                auHoldingRegisters[index].usValue = usValue;
                auHoldingRegisters[index].bChanged = true;
            }
            else
            {
                // Assign the modbus register value at given offset
                ModbusRegister modbusReg = new ModbusRegister();
                modbusReg.usValue = usValue;
                modbusReg.iOffset = (ushort)i32uModbusOffset;
                modbusReg.i32uSlaveId = i32uSlaveId;
                modbusReg.strLabel = registerLabel;
                modbusReg.bChanged = true;

                // Append onto the list
                auHoldingRegisters.Add(modbusReg);
            }                  
        }

        // Update the modbus register
        public void IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL(string registerLabel, ushort usValue)
        {
            int i32uModbusOffset = getModbusRegisterOffset(registerLabel,"input");
            byte i32uSlaveId     = getModbusInputRegisterSlaveId(registerLabel);

            // Determine if already in List
            int index = auInputRegisters.FindIndex(item => (item.iOffset == i32uModbusOffset && item.i32uSlaveId == i32uSlaveId));

            // Register exists in the Holding Register List
            if (index >= 0)
            {
                auInputRegisters[index].usValue = usValue;
                auInputRegisters[index].bChanged = true;
                System.Diagnostics.Trace.WriteLine("Enter IHI_UPDATE_MODBUS_REGISTER_AT_LABEL Changed registerLabel:" + registerLabel + " Value:"+usValue);
            }
            else
            {
                // Assign the modbus register value at given offset
                ModbusRegister modbusReg    = new ModbusRegister();
                modbusReg.usValue           = usValue;
                modbusReg.iOffset           = (ushort)i32uModbusOffset;
                modbusReg.i32uSlaveId       = i32uSlaveId;
                modbusReg.strLabel          = registerLabel;
                modbusReg.bChanged          = true;

                // Append onto the list
                auInputRegisters.Add(modbusReg);
            }
        }

        // Obtain the register Value
        private short IHI_OBTAIN_MODBUS_REGISTER_VALUE(string strLabel, string type)
        {
            ushort usValue = 0;

            if (type == "holding")
            {
                ushort i32uModbusOffset = (ushort)getModbusRegisterOffset(strLabel, type);
                byte i32uSlaveId = getModbusRegisterSlaveId(strLabel);
                IHI_READ_WRITE_HOLDING_MODBUS_REGISTER(i32uModbusOffset, "READ", 0, i32uSlaveId, ref usValue);
            }
            else
            {
                ushort i32uModbusOffset = (ushort)getModbusRegisterOffset(strLabel, type);
                byte i32uSlaveId = getModbusInputRegisterSlaveId(strLabel);
                IHI_READ_WRITE_INPUT_MODBUS_REGISTER(i32uModbusOffset, "READ", 0, i32uSlaveId, ref usValue);
            }

            return (short)usValue;
        }

        // Cycle through the alarms and obtain a description
        private string IHI_OBTAIN_ALARM_DESCRIPTION(string strId)
        {
            string match = "UNKNOWN";
            var alarmObjectCollection = AutoConfig.Map<IIhiAlarmObjectCollection>();

            // Cycle through the Alarm Register List
            foreach (IihiAlarmObject alarmObject in alarmObjectCollection.ihiAlarmObjects)
            {
                // Determine matched alarmed
                if (alarmObject.id == strId)
                {
                    match = alarmObject.description;
                    break;
                }
            }

            return match;
        }

        // Used to put inverter into active mode
        private void IHI_CHECK_AUTONOMOUS_REGISTERS()
        {
            try
            {
                // Real Power Demand
                sCurrent_RealPowerCommand = IHI_OBTAIN_MODBUS_REGISTER_VALUE("Wspt","holding");

                // Reactive Power Demand
                sCurrent_ReactivePowerCommand = IHI_OBTAIN_MODBUS_REGISTER_VALUE("VarSpt","holding");

                // Determine if the system has faulted
                short errorNumber = IHI_OBTAIN_MODBUS_REGISTER_VALUE("ErrNo(L)", "input");
                if (errorNumber > 0)
                {
                    // Cycle through all the alarms and find fault
                    usNew_CurrentStatus = oInverterInstance.getFaultedState();

                    if (usOld_CurrentStatus != usNew_CurrentStatus)
                    {
                        oInverterSystemObject.alarmed = "FAULT";
                        oInverterSystemObject.alarmed = IHI_OBTAIN_ALARM_DESCRIPTION(errorNumber.ToString());
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("ErrStt(L)", oInverterInstance.getFaultedState());
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotW(L)", 0);
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotVar(L)", 0);
                        usOld_CurrentStatus = oInverterInstance.getFaultedState();
                    }
                }
                else
                {
                    oInverterSystemObject.alarmed = "NOFAULT";

                    // Determine if current status was fault and all faults been removed
                    if (usOld_CurrentStatus == oInverterInstance.getFaultedState())
                    {
                        if (usOld_Command != 381)
                        {
                            // Return to Enabled and Running 
                            usOld_CurrentStatus = oInverterInstance.getEnabledValue();
                        }
                        else
                        {
                            // Return to disabled state
                            usOld_CurrentStatus = oInverterInstance.getDisabledValue();
                        }

                        // Update the current status
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("OpStt(L)", usOld_CurrentStatus);
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("ErrStt(L)", 0);
                    }
                }
               
                // Determine if START Command has been changed to START and no fault
                usNew_Command = (ushort)IHI_OBTAIN_MODBUS_REGISTER_VALUE("AuxCtl.SCSOpCmd","holding");
                if (usOld_Command != usNew_Command)
                {
                    old_ResetCommand = 0;

                    if (usNew_Command == 381)
                    {
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("OpStt(L)", oInverterInstance.getDisabledValue());
                        usOld_Command = usNew_Command;
                        usNew_DCVoltage = 0;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("DcMs.Vol(L)", usNew_DCVoltage);
                        oInverterSystemObject.status = "OFFLINE";
                    }
                    else
                    {
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("OpStt(L)", oInverterInstance.getEnabledValue());
                        usOld_Command = usNew_Command;

                        // Set Initial DC Voltage
                        usNew_DCVoltage = 9600;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("DcMs.Vol(L)", usNew_DCVoltage);
                        oInverterSystemObject.status = "ONLINE";
                    }

                    usNew_PhaseVoltageAB = oInverterInstance.getPhaseAVoltage();
                    if (usOld_VoltageAB != usNew_PhaseVoltageAB)
                    {
                        usOld_VoltageAB = usNew_PhaseVoltageAB;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.V.PhsAB(L)", usOld_VoltageAB);
                    }

                    usNew_PhaseVoltageBC = oInverterInstance.getPhaseBVoltage();
                    if (usOld_VoltageBC != usNew_PhaseVoltageBC)
                    {
                        usOld_VoltageBC = usNew_PhaseVoltageBC;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.V.PhsBC(L)", usOld_VoltageBC);
                    }

                    // Check Phase Voltage C-A has changed
                    usNew_PhaseVoltageCA = oInverterInstance.getPhaseCVoltage();
                    if (usOld_VoltageCA != usNew_PhaseVoltageCA)
                    {
                        usOld_VoltageCA = usNew_PhaseVoltageCA;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.V.PhsCA(L)", usOld_VoltageCA);
                    }
                }

                // Determine if RESET command has been received if so stop the inverter 
                if (old_ResetCommand != usNew_ResetCommand)
                {
                    usOld_Command = 0;
                   
                    IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("OpStt(L)", oInverterInstance.getDisabledValue());
                    old_ResetCommand = usNew_ResetCommand;
                    oInverterSystemObject.status = "SHUTDOWN";
                    
                    // Set Initial DC Voltage
                    usNew_DCVoltage = 0;
                    IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("DcMs.Vol(L)", usNew_DCVoltage);
                }

                // Determine if real power command has changed
                // If so update actual realpower
                if (old_RealPowerCommand != sCurrent_RealPowerCommand)
                {
                    old_RealPowerCommand = sCurrent_RealPowerCommand;
                    double fActualRealPower = oInverterInstance.getActualRealPower(old_RealPowerCommand);

                    // Update the actual Real Power
                    IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotW(L)", (ushort)fActualRealPower);

                    // Determine the total current
                    // At beld the minimum voltage is 750v hence 750v / root of 2 to get Phase Voltage is 530V
                    // Actual Power / Phase Voltage / root of 3 to give phase current
                    // 750 V is the nominal ac voltage
                    usNew_NominalACVoltage = 374;
                    oInverterInstance.setNominalACVoltage(usNew_NominalACVoltage);

                    // Assign new Phase Currents
                    usNew_CurrentR = (ushort)oInverterInstance.getPhaseACurrent();
                    usNew_CurrentS = (ushort)oInverterInstance.getPhaseBCurrent();
                    usNew_CurrentT = (ushort)oInverterInstance.getPhaseCCurrent();

                    // Determine if old Current R
                    if (usOld_CurrentR != usNew_CurrentR)
                    {
                        usOld_CurrentR = usNew_CurrentR;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotA.PhsA(L)", usOld_CurrentR);
                    }

                    // Determine if New Current S
                    if (usOld_CurrentS != usNew_CurrentS)
                    {
                        usOld_CurrentS = usNew_CurrentS;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotA.PhsB(L)", usOld_CurrentS);
                    }

                    // Determine if change in current
                    if (usOld_CurrentT != usNew_CurrentT)
                    {
                        usOld_CurrentT = usNew_CurrentT;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotA.PhsC(L)", usOld_CurrentT);
                    }

                    // Determine if change if Frequency
                    if (usOld_Frequency != usNew_usFrequency)
                    {
                        usOld_Frequency = usNew_usFrequency;
                        IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("GriMs.Hz(L)", usOld_Frequency);
                    }
                }

                // Determine if reactive power command has change
                // If so update actual reactive power
                if (old_ReactivePowerCommand != sCurrent_ReactivePowerCommand)
                {
                    // Set old Reactive Command to the current value
                    old_ReactivePowerCommand = sCurrent_ReactivePowerCommand;
                    double fActualReactivePowerValue = oInverterInstance.getActualReactivePower(old_ReactivePowerCommand);
                    
                    // Udate Reactive Actual Power
                    IHI_UPDATE_INPUT_MODBUS_REGISTER_AT_LABEL("InvMs.TotVar(L)", (ushort)fActualReactivePowerValue);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine("Failed checkAutonmonous register write!!" + e.Message);
            }
        }

        // Set Communication Timeout and Heartbeat Ctrl
        public static void IHI_SET_MODBUS_COMMUNICATION_TIMEOUT(string strCommunicationTimeout, string strHBOn)
        {
            usModbusCommunicationTimeout = UInt32.Parse(strCommunicationTimeout);

            // Determine if heartbeat control is ON/OFF
            // The inverter connector sends a heartbeat to ensure communication is still good
            strHeartBeatCheckingCtrl = strHBOn;
        }

        // Set a Register Value in the modbus register
        public static void IHI_SET_REGISTER_VALUE(string RegisterType, string RegisterLabel, string RegisterAddress, string RegisterValue)
        {
            try
            {
                dynamic ihi_register = new JObject();
                if (RegisterType == "holding")
                {
                    // Obtain the Modbus Holding collection from the appConfig
                    var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                    // Cycle through the Holding Modbus Collection
                    foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                    {
                        // Determine if matched register address/label
                        if (RegisterAddress == modbusObject.index.ToString()
                                            ||
                            RegisterLabel == modbusObject.label)
                        {
                            // Determine modbus offset
                            int i32uModbusOffset = modbusObject.index;
                            byte bSlaveId = 0;
                            byte.TryParse(modbusObject.slaveid, out bSlaveId);

                            // Assign the value to Input Register
                            ushort usValue = (ushort)int.Parse(RegisterValue);

                            // Determine if already in List
                            int index = auHoldingRegisters.FindIndex(item => (item.iOffset == i32uModbusOffset && item.i32uSlaveId == bSlaveId));

                            // Register exists in the Holding Register List
                            if (index >= 0)
                            {
                                auHoldingRegisters[index].usValue   = usValue;
                                auHoldingRegisters[index].bChanged  = true;
                            }
                            else
                            {
                                // Assign the modbus register value at given offset
                                //IHI_READ_WRITE_MODBUS_REGISER(i32uModbusOffset, "UPDATE", uvalue);
                                ModbusRegister modbusReg    = new ModbusRegister();
                                modbusReg.usValue           = usValue;
                                modbusReg.iOffset           = (ushort)i32uModbusOffset;
                                modbusReg.bChanged          = true;
                                modbusReg.i32uSlaveId       = bSlaveId;

                                // Append onto the list
                                auHoldingRegisters.Add(modbusReg);
                            }
                            break;
                        }
                    }
                }
                else
                {
                    // Determine if modbus input register
                    if (RegisterType == "input")
                    {
                        // Obtain the Modbus input collection from the appConfig
                        var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

                        // Cycle through the Holding Modbus Collection
                        foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
                        {
                            // Determine if matched register address
                            if (RegisterAddress == modbusObject.index.ToString()
                                                ||
                                RegisterLabel == modbusObject.label)
                            {
                                // Determine the offset
                                int i32uModbusOffset = modbusObject.index;
                                byte bSlaveId = 0;
                                byte.TryParse(modbusObject.slaveid, out bSlaveId);

                                ushort usValue = ushort.Parse(RegisterValue);

                                // Determine if register already in List
                                int index = auInputRegisters.FindIndex(item => (item.iOffset == i32uModbusOffset && item.i32uSlaveId == bSlaveId));

                                // Register exists in the Incomming Regiser List
                                if (index >= 0)
                                {
                                    auInputRegisters[index].usValue = usValue;
                                    auInputRegisters[index].bChanged = true;
                                }
                                else
                                {
                                    // Update the registers at Modbus Offset
                                    ModbusRegister modbusReg = new ModbusRegister();

                                    // Assign value and address
                                    modbusReg.usValue       = usValue;
                                    modbusReg.iOffset       = (ushort) i32uModbusOffset;
                                    modbusReg.i32uSlaveId   = bSlaveId;
                                    modbusReg.strLabel      = RegisterLabel;
                                    modbusReg.bChanged      = true;

                                    // Add the new register
                                    auInputRegisters.Add(modbusReg);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
            }
        }

        // Return the JSON object containing the communication state
        public static JObject IHI_OBTAIN_MODBUS_REGISTER_COMMS()
        {
            dynamic ihi_modbus_comms = new JObject();
            ihi_modbus_comms.communication = strChannelState;
            oIhiInverterRegisterValue = ihi_modbus_comms;
            return oIhiInverterRegisterValue;
        }

        // Obtain the Inverter System High Level Informaton
        // Inverter Company
        // Inverter Version
        // Modbus Communication
        // Running/Not Running
        // Faulted/Not Faulted
        // Nameplate added to the inverter
        public static JObject IHI_OBTAIN_MODBUS_INVERTER_SYSTEM_STATE()
        {
            JObject oGeneralInformation = new JObject();
            // General High Level Information properties
            oGeneralInformation["alarmed"]       = oInverterSystemObject.alarmed;
            oGeneralInformation["company"]       = oInverterSystemObject.company;
            oGeneralInformation["status"]        = oInverterSystemObject.status;
            oGeneralInformation["communication"] = oInverterSystemObject.communication;
            oGeneralInformation["product"]       = oInverterSystemObject.product;
            oGeneralInformation["nameplate"]     = oInverterSystemObject.nameplate;
            return oGeneralInformation;
        }

        // Set Inverter to operate independently
        public static void IHI_SET_INVERTER_AUTONOMOUS(string autonomous)
        {
            strAutonomous = autonomous;
        }

        // Perform an action based on string
        public static JObject IHI_GET_INVERTER_SIMULATOR_PROPERTY(string property)
        {
            JObject oRegisterValue = null;
            switch (property)
            {
                // Get real power demand
                case "real_power_demand":
                    {
                        oRegisterValue = oInverterInstance.getDemandInverterRealPower();
                        break;
                    }

                // Get reactive power demand
                case "reactive_power_demand":
                    {
                        oRegisterValue = oInverterInstance.getDemandInverterReactivePower();
                        break;
                    }
                // Get real power actual
                case "real_power_actual":
                    {
                        oRegisterValue = oInverterInstance.getActualInverterRealPower();
                        break;
                    }

                // Get reactive power actual
                case "reactive_power_actual":
                    {
                        oRegisterValue = oInverterInstance.getActualInverterReactivePower();
                        break;
                    }

                // Get frequency
                case "frequency":
                    {
                        oRegisterValue = oInverterInstance.getFrequency();
                        break;
                    }

                // Get dc voltage
                case "voltage_dc":
                    {
                        oRegisterValue = oInverterInstance.getDCVoltage();
                        break;
                    }

                // Get ac voltage
                case "voltage_ac":
                    {

                        oRegisterValue = oInverterInstance.getACVoltage();
                        break;
                    }

                // Set artificial phase A voltage
                case "phase_voltage_a":
                    {
                        oRegisterValue = oInverterInstance.getVoltagePhaseA();
                        break;
                    }

                // Set artificial phase B voltage
                case "phase_voltage_b":
                    {
                        oRegisterValue = oInverterInstance.getVoltagePhaseB();
                        break;
                    }

                // Set artificial phase C voltage
                case "phase_voltage_c":
                    {
                        oRegisterValue = oInverterInstance.getVoltagePhaseC();
                        break;
                    }

                // Set artificial phase A current
                case "phase_current_a":
                    {
                        oRegisterValue = oInverterInstance.getCurrentPhaseA();
                        break;
                    }

                // Set artificial phase B current
                case "phase_current_b":
                    {
                        oRegisterValue = oInverterInstance.getCurrentPhaseB();
                        break;
                    }

                // Set artificial phase C current
                case "phase_current_c":
                    {
                        oRegisterValue = oInverterInstance.getCurrentPhaseC();
                        break;
                    }

                // Change Operational State
                case "operational_state":
                    {
                        oRegisterValue = oInverterInstance.getOperationalState();
                        break;
                    }

                // Set Alarm
                case "alarm":
                    {
                        oRegisterValue = oInverterInstance.getAlarm();
                        break;
                    }
                // Set Alarm
                case "warning":
                    {
                        oRegisterValue = oInverterInstance.getWarningAlarm();
                        break;
                    }
                default:
                    break;
            }
            return oRegisterValue;
        }

        // Update a register that is captured by the inverter connector
        public static void IHI_SET_INVERTER_UPDATE_MANDATORY_REGISTER(string action, string value)
        {
            switch (action)
            {
                // Update real power demand
                case "real_power_demand":
                    {
                        oInverterInstance.setDemandInverterRealPower(value);
                        break;
                    }

                // Update reactive power demand
                case "reactive_power_demand":
                    {
                        oInverterInstance.setDemandInverterReactivePower(value);
                        break;
                    }
                // Update actual real power
                case "real_power_actual":
                    {
                        oInverterInstance.updateActualInverterRealPower(value);
                        break;
                    }

                // Update actual reactive power
                case "reactive_power_actual":
                    {
                        oInverterInstance.updateActualInverterReactivePower(value);
                        break;
                    }

                // Update frequency
                case "frequency":
                    {
                        oInverterInstance.updateFrequency(value);
                        break;
                    }

                // Update dc voltage
                case "voltage_dc":
                    {
                        oInverterInstance.updateDCVoltage(value);

                        break;
                    }

                // Update phase A voltage
                case "phase_voltage_a":
                    {
                        oInverterInstance.updatePhaseAVoltage(value);
                        break;
                    }

                // Update phase B voltage
                case "phase_voltage_b":
                    {
                        oInverterInstance.updatePhaseBVoltage(value);
                        break;
                    }

                // Update phase C voltage
                case "phase_voltage_c":
                    {
                        oInverterInstance.updatePhaseCVoltage(value);
                        break;
                    }

                // Update phase A current
                case "phase_current_a":
                    {
                        oInverterInstance.updatePhaseACurrent(value);
                        break;
                    }

                // Update phase B current
                case "phase_current_b":
                    {
                        oInverterInstance.updatePhaseBCurrent(value);
                        break;
                    }

                // Update phase C current
                case "phase_current_c":
                    {
                        oInverterInstance.updatePhaseCCurrent(value);
                        break;
                    }

                // Update Operational State
                case "operational_state":
                    {
                        oInverterInstance.updateOperationalState(value);
                        break;
                    }

                // Update Alarm
                case "alarm":
                    {
                        oInverterInstance.updateAlarm(value);
                        break;
                    }

                // Update Alarm
                case "warning":
                    {
                        oInverterInstance.updateWarningAlarm(value);
                        break;
                    }
                default:
                    break;
            }
        }

        // Obtain the modbus register value providing communication is good
        public static JObject IHI_OBTAIN_MODBUS_REGISTER(string RegisterType, string RegisterAddress, string SlaveId)
        {
            try
            {
                // Determine if the IHI Inverter Connector and IHI Inverter simulator
                // are communicating
                if (strChannelState == "CONNECTED")
                {
                    string strFoundRegister = "FALSE";
                    dynamic ihi_register = new JObject();

                    if (RegisterType == "holding")
                    {
                        // Obtain the Modbus input collection from the appConfig
                        var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                        // Cycle through the Holding Modbus Collection
                        foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                        {
                            // Determine if matched register address/label
                            if ((RegisterAddress == modbusObject.index.ToString() && SlaveId == modbusObject.slaveid)
                                                 ||
                                (RegisterAddress == modbusObject.label && SlaveId == modbusObject.slaveid))
                            {
                                // Match Found, determine offset
                                int i32uModbusOffset = modbusObject.index;
                                int i32uSlaveId = 0;
                                bool result = int.TryParse(SlaveId, out i32uSlaveId);
                                int index = auHoldingRegisters.FindIndex(item => (item.iOffset == i32uModbusOffset && item.i32uSlaveId == i32uSlaveId));

                                // Create a new register JSON object and populate
                                // register fields
                                ihi_register.label          = modbusObject.label;
                                ihi_register.type           = modbusObject.type;
                                ihi_register.address        = modbusObject.index;
                                ihi_register.value          = (short)auHoldingRegisters[index].usValue;
                                ihi_register.slaveid        = modbusObject.slaveid;
                                ihi_register.date           = DateTime.Now;
                                oIhiInverterRegisterValue   = ihi_register;
                                strFoundRegister            = "TRUE";
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (RegisterType == "input")
                        {
                            // Obtain the Modbus Collection from the appConfig
                            var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

                            // Cycle through the Modbus Collection
                            foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
                            {
                                // Determine if matched register address/label
                                if ((RegisterAddress == modbusObject.index.ToString() && SlaveId == modbusObject.slaveid)
                                                     ||
                                    (RegisterAddress == modbusObject.label && SlaveId == modbusObject.slaveid))
                                {
                                    // Determine modbus Offset
                                    int i32uModbusOffset = modbusObject.index;
                                    int i32uSlaveId = 0;
                                    bool result = int.TryParse(SlaveId, out i32uSlaveId);
                                    int index = auInputRegisters.FindIndex(item => (item.iOffset == i32uModbusOffset && item.i32uSlaveId == i32uSlaveId));
                                    ihi_register.label          = modbusObject.label;
                                    ihi_register.type           = modbusObject.type;
                                    ihi_register.address        = modbusObject.index;
                                    ihi_register.slaveid        = modbusObject.slaveid;
                                    ihi_register.value          = (short)auInputRegisters[index].usValue;
                                    ihi_register.date           = DateTime.Now;
                                    oIhiInverterRegisterValue   = ihi_register;
                                    strFoundRegister = "TRUE";
                                    break;
                                }
                            }
                        }
                    }
                    // Determine if register was found in list
                    if (strFoundRegister == "NOTFOUND")
                    {
                        ihi_register.address = RegisterAddress;
                        ihi_register.value = "NOT FOUND";
                        oIhiInverterRegisterValue = ihi_register;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                strChannelState = "NOT CONNECTED";
                server.Stop();
            }
            // Return register Value
            return oIhiInverterRegisterValue;
        }

        // Obtain the heartbeat from the IHI Inverter Connector
        private ushort IHI_OBTAIN_HEARTBEAT(string strHeartbeatLabel)
        {
            ushort usHeartbeatValue = 0;
            byte bSlaveId = 0;
            try
            {
                // Obtain the Modbus Holding register collection from the appConfig
                var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                // Cycle through the Holding Modbus Collection
                foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                {
                    // Determine if matched slave
                    if (strHeartbeatLabel == modbusObject.label)
                    {
                        byte i32uModbusOffset = (byte) modbusObject.index;
                        ushort usValue = 0;
                      
                        bool result=byte.TryParse(modbusObject.slaveid, out bSlaveId);
                        IHI_READ_WRITE_HOLDING_MODBUS_REGISTER(i32uModbusOffset, "READ", 0, bSlaveId, ref usValue);
                        usHeartbeatValue = usValue;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine("IHI_ObtainHeartbeat Exception=" + e.Message);
            }
            //System.Diagnostics.Trace.WriteLine("heartbeat=" + usHeartbeatValue);
            return usHeartbeatValue;
        }

        // Obtain the modbus register value providing communication is good
        public JObject IHI_OBTAIN_MODBUS_HOLDING_REGISTER(string RegisterAddress, string SlaveId)
        {
            try
            {
                // Determine if the IHI Inverter Connector and IHI Inverter simulator
                // are communicating
                if (strChannelState == "CONNECTED")
                {
                    string strFoundRegister = "FALSE";
                    dynamic ihi_register = new JObject();

                    // Obtain the Modbus input collection from the appConfig
                    var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                    // Cycle through the Holding Modbus Collection
                    foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                    {
                        // Determine if matched register address/label
                        if ((RegisterAddress == modbusObject.index.ToString() && SlaveId == modbusObject.slaveid)
                                             ||
                            (RegisterAddress == modbusObject.label && SlaveId == modbusObject.slaveid))
                        {
                            // Match Found, determine offset
                            int i32uModbusOffset = modbusObject.index;
                            int i32uSlaveId = 0;
                            bool result = int.TryParse(SlaveId, out i32uSlaveId);
                            int index = auHoldingRegisters.FindIndex(item => (item.strLabel == RegisterAddress && item.i32uSlaveId == i32uSlaveId));

                            // Create a new register JSON object and populate
                            // register fields
                            ihi_register.value = (short)auHoldingRegisters[index].usValue;
                            oIhiInverterRegisterValue = ihi_register;
                            strFoundRegister = "TRUE";
                            break;
                        }
                    }

                    // Determine if register was found in list
                    if (strFoundRegister == "NOTFOUND")
                    {
                        ihi_register.address = RegisterAddress;
                        ihi_register.value = "NOT FOUND";
                        oIhiInverterRegisterValue = ihi_register;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                strChannelState = "NOT CONNECTED";
                server.Stop();
            }

            // Return register Value
            return oIhiInverterRegisterValue;
        }

        // Obtain the modbus register value providing communication is good
        public JObject IHI_OBTAIN_MODBUS_INPUT_REGISTER(string RegisterAddress, string SlaveId)
        {
            try
            {
                // Determine if the IHI Inverter Connector and IHI Inverter simulator
                // are communicating
                if (strChannelState == "CONNECTED")
                {
                    string strFoundRegister = "FALSE";
                    dynamic ihi_register = new JObject();

                    // Obtain the Modbus input collection from the appConfig
                    var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

                    // Cycle through the Holding Modbus Collection
                    foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
                    {
                        // Determine if matched register address/label
                        if ((RegisterAddress == modbusObject.index.ToString() && SlaveId == modbusObject.slaveid)
                                             ||
                            (RegisterAddress == modbusObject.label && SlaveId == modbusObject.slaveid))
                        {
                            // Match Found, determine offset
                            int i32uModbusOffset = modbusObject.index;
                            int i32uSlaveId = 0;
                            bool result = int.TryParse(SlaveId, out i32uSlaveId);
                            int index = auInputRegisters.FindIndex(item => (item.iOffset == i32uModbusOffset && item.i32uSlaveId == i32uSlaveId));

                            // Create a new register JSON object and populate
                            // register fields
                            ihi_register.value = (short)auInputRegisters[index].usValue;
                            oIhiInverterRegisterValue = ihi_register;
                            strFoundRegister = "TRUE";
                            break;
                        }
                    }

                    // Determine if register was found in list
                    if (strFoundRegister == "NOTFOUND")
                    {
                        ihi_register.address = RegisterAddress;
                        ihi_register.value = "NOT FOUND";
                        oIhiInverterRegisterValue = ihi_register;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                strChannelState = "NOT CONNECTED";
                server.Stop();
            }

            // Return register Value
            return oIhiInverterRegisterValue;
        }

        // Assign the modbus holding registers
        private void IHI_SET_HOLDING_REGISTERS()
        {

            try
            {
                // Reset old register list
                Current_RegisterHoldingList.Clear();
                // Obtain the Modbus input collection from the appConfig
                var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                // Cycle through the Holding Modbus Collection
                foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                {
                    // Determine if matched slave
                    ushort i32uModbusOffset = (ushort)modbusObject.index;

                    ushort usValue = 0;
                    byte bSlaveId = 0;
                    bool result = byte.TryParse(modbusObject.slaveid, out bSlaveId);
                    IHI_READ_WRITE_HOLDING_MODBUS_REGISTER(i32uModbusOffset, "READ", 0, bSlaveId, ref usValue);
                  
                    short sValue = (short)usValue;
                    int index = auHoldingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset && item.i32uSlaveId == bSlaveId);

                    // Only update if user not updating register
                    if (auHoldingRegisters[index].bChanged == false)
                    {
                        auHoldingRegisters[index].usValue = usValue;
                    }

                    // Update the holding register
                    Ihi_UPDATE_MODBUS_HOLDING_REGISTER_TABLE(modbusObject.type, modbusObject.index.ToString(), modbusObject.slaveid, sValue.ToString(), modbusObject.label, modbusObject.mandatory);
                }
               
                // Populate the registers list with the holding registers received from
                // postman/ IHI ES Test Harness
                oIHI_Registerlists["holding_registers"] = JToken.FromObject(Current_RegisterHoldingList);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                strChannelState = "NOT CONNECTED";
            }
        }

        // Kick off the timer thread every 2 seconds
        // This will update any Holding/Input register changes 
        // 
        private void IHI_START_MODBUS_PROCESSING()
        {
            // Wait for a client connection
            m_timerSequenceNumber           = new System.Timers.Timer();
            m_timerSequenceNumber.Interval  = 2000;
            m_timerSequenceNumber.AutoReset = true;
            m_timerSequenceNumber.Elapsed   += IHI_ONTIMEREVENT_UPDATE_TIMER;
            m_timerSequenceNumber.Enabled   = true;
        }

        // Change the state of the connection
        private void IHI_ACCEPT_CLIENT(IAsyncResult asyncResult)
        {
            if (server.Server.IsBound)
            {
                // A modbus client has connected to this server application
                strChannelState = "CONNECTED";
                System.Diagnostics.Trace.WriteLine("CONNECTED CLIENT!!!!!");
                Ihi_inverter_modbus_class oModClass = (Ihi_inverter_modbus_class)asyncResult.AsyncState;

                // Add a 2 second delay before commencing processing of read/writes
                int milliseconds = 2000;
                Thread.Sleep(milliseconds);

                // Update initial configuratuon values
                oInverterInstance.initialInverterConfiguration(oModClass);

                // Commence Modbus Processing
                oModClass.IHI_START_MODBUS_PROCESSING();
            }
        }

        // Assign the modbus input registers
        private void IHI_SET_INPUT_REGISTERS()
        {
            ushort usSlaveIndex = 0;

            //  Obtain the slave id
            int iSlaveId = ModbusSlave[usSlaveIndex].UnitId;

            // Obtain the Modbus input collection from the appConfig
            var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

            // Reset the ihi registers
            Current_RegisterInputList.Clear();

            // Cycle through the modbus object list
            foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
            {
                // Obtain the modbus offset
                ushort i32uModbusOffset = (ushort)modbusObject.index;
               
                // Obtain Modbus Register Value at Offset
                ushort usValue = 0;
                int index = auInputRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);
                IHI_READ_WRITE_INPUT_MODBUS_REGISTER(i32uModbusOffset, "UPDATE", auInputRegisters[index].usValue, 3, ref usValue);
              
                if (usValue > 0)
                {
                    System.Diagnostics.Trace.WriteLine("Read Input List index=" + i32uModbusOffset + " value=" + auInputRegisters[index]);
                }
                short sValue = (short)auInputRegisters[index].usValue;

                // Create a new register JSON object and populate
                dynamic ihi_inverter_register = new JObject();

                ihi_inverter_register.label     = modbusObject.label;
                ihi_inverter_register.type      = modbusObject.type;
                ihi_inverter_register.address   = modbusObject.index;
                ihi_inverter_register.value     = sValue;
                ihi_inverter_register.slaveid   = modbusObject.slaveid;
                ihi_inverter_register.mandatory = modbusObject.mandatory;
                ihi_inverter_register.date      = DateTime.Now;

                // Save register to list
                Current_RegisterInputList.Add(ihi_inverter_register);

                // Populate input register list
                oIHI_Registerlists["input_registers"] = JToken.FromObject(Current_RegisterInputList);
            }
        }

        // The inverter connector is attempting to read data. This callback is fired
        // when it receives that event. Populate the modbus registers.
        private void Modbus_DataStoreReadFrom(object sender, DataStoreEventArgs e)
        {
            //System.Diagnostics.Trace.WriteLine("Enter Modbus_DataStoreReadFrom Count is " + e.Data.B.Count);
            IHI_UPDATE_MODBUS_WRITE_REQUESTS();
        }

        // All the holding registers in the modbus register map
        public void Ihi_UPDATE_MODBUS_HOLDING_REGISTER_TABLE(String type, string usIndex, String slaveid, String value, String label, String mandatory)
        {
            try
            {
                // Update Modbus Holding Register
                dynamic ihi_inverter_register = new JObject();
                ihi_inverter_register.label = label;
                ihi_inverter_register.type = type;
                ihi_inverter_register.address = usIndex;
                ihi_inverter_register.slaveid = slaveid;
                ihi_inverter_register.value = value.ToString();
                ihi_inverter_register.mandatory = mandatory;
                ihi_inverter_register.date = DateTime.Now;

                // Add the inverter register to the list of registers
                Current_RegisterHoldingList.Add(ihi_inverter_register);
            }
            catch (SystemException error)
            {
                System.Diagnostics.Trace.WriteLine(error);
            }
        }

        // Update any write requests
        public void IHI_UPDATE_MODBUS_WRITE_REQUESTS()
        {
            for (int i = 0; i < auHoldingRegisters.Count; i++)
            {
                if (auHoldingRegisters[i].bChanged == true)
                {
                    ushort empty = 0;

                    IHI_READ_WRITE_HOLDING_MODBUS_REGISTER(auHoldingRegisters[i].iOffset, "UPDATE", auHoldingRegisters[i].usValue, auHoldingRegisters[i].i32uSlaveId, ref empty);

                    // After updating register mark change state to false
                    auHoldingRegisters[i].bChanged = false;
                }
            }

            for (int i = 0; i < auInputRegisters.Count; i++)
            {
                if (auInputRegisters[i].bChanged == true)
                {
                    ushort empty = 0;

                    IHI_READ_WRITE_INPUT_MODBUS_REGISTER(auHoldingRegisters[i].iOffset, "UPDATE", auInputRegisters[i].usValue, auInputRegisters[i].i32uSlaveId, ref empty);

                    // After updating register mark change state to false
                    auInputRegisters[i].bChanged = false;
                }
            }
        }

        // Called periodically to update incomming/holding registers
        public void IHI_ONTIMEREVENT_UPDATE_TIMER(Object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                // Check if modbus port connected
                if (server.Server.IsBound)
                {
                    // Get the heartbeat state   
                    usHeartbeat = IHI_OBTAIN_HEARTBEAT("heartbeat");

                    // Determine if heartbeat changed
                    if (usHeartbeat == usLastheartbeat && strHeartBeatCheckingCtrl == "ON")
                    {
                        // Expires after 10 seconds. configurable via Communication controller REST API PUT
                        if (i32uTimeOutCounter > usModbusCommunicationTimeout)
                        {
                            System.Diagnostics.Trace.WriteLine("NOT CONNECTED Last Known heartbeat=" + usLastheartbeat + " Timeout Counter=" + i32uTimeOutCounter + " usModbudCommunicationTimeout=" + usModbusCommunicationTimeout + "usHearbeat=" + usHeartbeat);
                            // Lost Communication
                            i32uTimeOutCounter = 0;
                            strChannelState = "NOT CONNECTED";

                            oInverterSystemObject.communication = strChannelState;

                            // Operational State
                            IHI_UPDATE_HOLDING_MODBUS_REGISTER_AT_LABEL("OpStt(L)", oInverterInstance.getFaultedState());

                            // Set watchdog alarm
                            //IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("fault", 1);
                        }
                        else
                        {
                            // Increment the timer by 1
                            i32uTimeOutCounter++;
                        }
                    }
                    else
                    {
                        oInverterSystemObject.communication = "CONNECTED";

                        // Reset the counter
                        i32uTimeOutCounter = 0;

                        // Save heartbeat value
                        usLastheartbeat = usHeartbeat;

                        // Update Modbus Holding registers
                        IHI_SET_HOLDING_REGISTERS();

                        // Update Modbus Input registers
                        IHI_SET_INPUT_REGISTERS();

                        // If autonomous mode then the simulator is going 
                        // to populate registers if the inverter is active
                        if (strAutonomous == "TRUE")
                        {
                            // Check for changes from the inverter connector
                            IHI_CHECK_AUTONOMOUS_REGISTERS();
                        }
                    }
                }
                else
                {
                    strChannelState = "NOT CONNECTED";
                    oInverterSystemObject.communication = strChannelState;
                }
            }
            catch (SystemException error)
            {
                System.Diagnostics.Trace.WriteLine(error.Message);
            }
        }
    }
}