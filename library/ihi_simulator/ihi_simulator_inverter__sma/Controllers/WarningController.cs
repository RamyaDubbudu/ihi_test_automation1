﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IHI_SIMULATOR_INVERTER_SMA.Models;
using Newtonsoft.Json.Linq;
using static IHI_SIMULATOR_INVERTER_SMA.Ihi_inverter_modbus_class;

namespace ihi_simulator_inverter__pe.Controllers
{
    public class WarningController : Controller
    {
        // GET: Warning
        public ActionResult Index()
        {
            return View();
        }
        // GET: Alarm/Edit/5
        public ActionResult Edit(int id)
        {
            int iRegCounter = 0;
            IHI_SET_REGISTER_VALUE("holding", "warning", "", id.ToString());

            id = id - 1;
            Alarm[] auAlarms = new Alarm[Current_AlarmWarningList.Count];

            // Cycle through all the alarms in the list
            for (int i = 0; i < Current_AlarmWarningList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_AlarmWarningList[i];

                // Create an Inverter Register Object
                Alarm obj = new Alarm();
                obj.id = (string)oTemp["id"];
                obj.description = (string)oTemp["description"];
                obj.state = (string)oTemp["state"];
                if (obj.state == "OFF")
                {
                    if (id == i)
                    {
                        obj.state = "ON";
                        Current_AlarmWarningList[i]["state"] = "ON";
                    }
                }

                // Increment alarm counter
                auAlarms[iRegCounter] = obj;
                iRegCounter++;
            }

            // Resize the alarm array
            Array.Resize(ref auAlarms, iRegCounter);
            return View(auAlarms);
        }
        // GET: Alarm/Clear
        public ActionResult Clear(int id)
        {
            System.Diagnostics.Trace.WriteLine("Enter  Warning:Clear");
            int iRegCounter = 0;
            try
            {
                // Clear the fault
                IHI_SET_REGISTER_VALUE("holding", "warning", "", "0");

                // Reduce to correct index
                id = id - 1;
                Alarm[] auAlarms = new Alarm[Current_AlarmWarningList.Count];

                // Cycle through all the alarms in the list
                for (int i = 0; i < Current_AlarmWarningList.Count; i++)
                {
                    JObject oTemp = new JObject();
                    oTemp = Current_AlarmWarningList[i];

                    // Set initial state of alarm to OFF
                    Current_AlarmWarningList[i]["state"] = "OFF";

                    // Create an Inverter Register Object
                    Alarm obj = new Alarm();
                    obj.id = (string)oTemp["id"];
                    obj.description = (string)oTemp["description"];
                    obj.state = (string)oTemp["state"];

                    // Increment alarm counter
                    auAlarms[iRegCounter] = obj;
                    iRegCounter++;
                }

                // Resize the alarm array
                Array.Resize(ref auAlarms, iRegCounter);
                return View(auAlarms);
            }
            catch
            {
                return View();
            }

        }
        public ActionResult ShowAlarms(FormCollection collection)
        {
            int iRegCounter = 0;

            Alarm[] auAlarms = new Alarm[Current_AlarmWarningList.Count];

            // Cycle through all the alarms in the list
            for (int i = 0; i < Current_AlarmWarningList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_AlarmWarningList[i];

                // Create an Inverter Register Object
                Alarm obj = new Alarm();
                obj.id = (string)oTemp["id"];
                obj.description = (string)oTemp["description"];
                obj.state = (string)oTemp["state"];

                // Increment warning counter
                auAlarms[iRegCounter] = obj;
                iRegCounter++;
            }  // Resize the warning array
            Array.Resize(ref auAlarms, iRegCounter);
            return View(auAlarms);
        }


        // GET: Warning/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Warning/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Warning/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: Warning/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Warning/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Warning/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
