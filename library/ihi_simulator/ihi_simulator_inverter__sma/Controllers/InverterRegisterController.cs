﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IHI_SIMULATOR_INVERTER_SMA.Ihi_inverter_modbus_class;
using Newtonsoft.Json.Linq;
using IHI_SIMULATOR_INVERTER_SMA.Models;

namespace IHI_SIMULATOR_INVERTER_SMA.Controllers
{
    public class InverterRegisterController : Controller
    {
        // GET: InverterRegister
        public ActionResult Index()
        {
            return View();
        }

        // GET: InverterRegister/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: InverterRegister/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InverterRegister/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: InverterRegister/Edit/5
        public ActionResult Edit(int id)
        {
            for (int i = 0; i < Current_RegisterHoldingList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_RegisterHoldingList[i];

                InverterRegister obj    =   new InverterRegister();
                obj.label               =   (string) oTemp["label"];
                obj.index               =   (int)    oTemp["address"];
                obj.value               =   (string) oTemp["value"];
                obj.type                =   (string) oTemp["type"];
                obj.slaveid             =   (string) oTemp["slaveid"];

                // Pass register to be edited
                if (id.ToString() == obj.index.ToString())
                {
                    return View(obj);
                }
            }
            return View();
        }

        // POST: InverterRegister/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // Obtain the register from collection
                string label = Convert.ToString(collection["label"]);
                string value = Convert.ToString(collection["value"]);
                string type  = Convert.ToString(collection["type"]);
                string index = Convert.ToString(collection["index"]);

                IHI_SET_REGISTER_VALUE(type, label, index, value);

                return RedirectToAction("ShowRegisters");
            }
            catch
            {
                return View();
            }
        }

        // GET: InverterRegister/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }
        public ActionResult ShowRegisters(FormCollection collection)
        {
            int iRegCounter = 0;
            string strSearchPattern = Convert.ToString(collection["RegisterId"]);
           
            InverterRegister[] auRegisters = new InverterRegister[Current_RegisterHoldingList.Count];

            for (int i = 0; i < Current_RegisterHoldingList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_RegisterHoldingList[i];

                // Create an Inverter Register Object
                InverterRegister obj = new InverterRegister();
                obj.label       = (string)oTemp["label"];
                obj.index       = (int)   oTemp["address"];
                obj.value       = (string)oTemp["value"];
                obj.type        = (string)oTemp["type"];
                obj.slaveid     = (string)oTemp["slaveid"];
               
                // Compare patterns of strings
                if (strSearchPattern != null)
                {
                    if (strSearchPattern.Length > 0)
                    {
                        // If no match on label and address then continue
                        if (!obj.label.Contains(strSearchPattern)
                                 &&
                            (!obj.index.ToString().Contains(strSearchPattern)))
                        {
                            continue;
                        }
                    }
                }
               
                // Increment register counter
                auRegisters[iRegCounter] = obj;
                iRegCounter++;
            }
            // Resize the array
            Array.Resize(ref auRegisters,iRegCounter);
            return View(auRegisters);
        }

        // POST: InverterRegister/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: InverterRegister/Delete/5
        [HttpPost]
        public ActionResult Save(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("ShowRegisters");
            }
            catch
            {
                return View();
            }
        }
    }
}
