﻿using IHI_SIMULATOR_INVERTER_SMA;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ihi_simulator_inverter_power_electronics.Controllers
{
    // Command Value sent from the 
    public class IHI_DEVICE_ACTION
    {
        public string register { get; set; }
        public string value { get; set; }
    }

    public class DeviceController : ApiController
    {

        /// <summary>
        /// Get a specific inverter simulator register either holding/input
        /// </summary>
        [Route("ihi/Device/Inverter/{register}")]
        public JObject GetRegisterByTypeAndLabel(string register)
        {
            JObject value = Ihi_inverter_modbus_class.IHI_GET_INVERTER_SIMULATOR_PROPERTY(register);
            return value;
        }


        /// <summary>
        /// Update a mandatory register needed by the inverter connector
        ///  </summary>
        [HttpPut]
        [Route("ihi/Device/Inverter")]
        // PUT: ihi/Device/Inverter
        public void Put()
        {
            HttpContent requestContent = Request.Content;
            string jsonContent = requestContent.ReadAsStringAsync().Result;

            IHI_DEVICE_ACTION oAction = Newtonsoft.Json.JsonConvert.DeserializeObject<IHI_DEVICE_ACTION>(jsonContent);
            System.Diagnostics.Trace.WriteLine("Register=" + oAction.register + " Value=" + oAction.value);
            Ihi_inverter_modbus_class.IHI_SET_INVERTER_UPDATE_MANDATORY_REGISTER(oAction.register, oAction.value);
        }
    }
}
