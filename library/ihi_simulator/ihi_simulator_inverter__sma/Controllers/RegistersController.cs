﻿/*=============================================================================
Name        : RegistersController.cs
Version     : 1.0
Copyright   : Copyright (c) 2016 IHI, Inc.  All Rights Reserved
Description : The Inverter Siimulator Controller.
This controller handles GET/POST REST APIs for the inverter simulator.

=============================================================================*/
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IHI_SIMULATOR_INVERTER_SMA.Models;

namespace IHI_SIMULATOR_INVERTER_SMA.Controllers
{
    // Register Object
    public class IHI_REGISTER
    {
        public string type { get; set; }
        public string label { get; set; }
        public string address { get; set; }
        public string value { get; set; }
    }

    // Communication Object
    public class IHI_COMMS
    {
        public string heartbeattimeout { get; set; }
        public string heartbeaton { get; set; }
    }

    // API Controller
    public class RegistersController : ApiController
    {
        // PUT: ihi/registers
        /// <summary>
        /// Update a specific inverter simulated register.
        /// </summary>
        [HttpPut]
        [Route("ihi/Registers")]
        public void Put()
        {
            // Obtain the Request Body 
            HttpContent requestContent = Request.Content;
            string jsonContent = requestContent.ReadAsStringAsync().Result;

            IHI_REGISTER oRegister = Newtonsoft.Json.JsonConvert.DeserializeObject<IHI_REGISTER>(jsonContent);
            System.Diagnostics.Trace.WriteLine("Input setRegisterValue:RegisterAddress=" + oRegister.address + " RegisterAddress=" + oRegister.value);
            Ihi_inverter_modbus_class.IHI_SET_REGISTER_VALUE(oRegister.type, oRegister.label, oRegister.address, oRegister.value);
        }

        // GET: ihi/registers// Obtain the register based on type and address
        /// <summary>
        /// Get all the inverter simulator registers
        /// </summary>
        [Route("ihi/Registers")]
        public JObject Get()
        {
            JObject list = Ihi_inverter_modbus_class.oIHI_Registerlists;
            return list;
        }

        // Obtain the register based on type and address
        /// <summary>
        /// Get a specific inverter simulator register either holding/input
        /// </summary>
        [Route("ihi/Registers/{registertype}/{registerlabel}/{slaveid}")]
        public JObject GetRegisterByTypeAndLabel(string registertype, string registerlabel, string slaveid)
        {
            JObject value = Ihi_inverter_modbus_class.IHI_OBTAIN_MODBUS_REGISTER(registertype, registerlabel, slaveid);
            return value;
        }
    }
}
