using System.Collections.ObjectModel;
#pragma warning disable 1591
namespace IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS.Areas.HelpPage.ModelDescriptions
{
    public class ComplexTypeModelDescription : ModelDescription
    {
        public ComplexTypeModelDescription()
        {
            Properties = new Collection<ParameterDescription>();
        }

        public Collection<ParameterDescription> Properties { get; private set; }
    }
}