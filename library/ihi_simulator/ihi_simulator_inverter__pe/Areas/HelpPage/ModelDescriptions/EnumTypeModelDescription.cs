using System.Collections.Generic;
using System.Collections.ObjectModel;
#pragma warning disable 1591
namespace IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS.Areas.HelpPage.ModelDescriptions
{
    public class EnumTypeModelDescription : ModelDescription
    {
        public EnumTypeModelDescription()
        {
            Values = new Collection<EnumValueDescription>();
        }

        public Collection<EnumValueDescription> Values { get; private set; }
    }
}