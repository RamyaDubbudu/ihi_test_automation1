﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS.Models
{
    public class InverterSystem
    {
        public string company { get; set; }
        public string product { get; set; }
        public string communication { get; set; }
        public string status { get; set; }
        public string alarmed { get; set; } 
        public string nameplate { get; set; }
        public string modeltype { get; set; }
    }
}