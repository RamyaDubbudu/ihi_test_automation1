﻿using IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS.Controllers
{
    public class InfoController : ApiController
    {
        // GET: ihi/Info
        // Obtain the register based on type and address
        /// <summary>
        /// Get Company Name, Product Name, Modbus Communication (CONNEDCTED/NOT CONNECTED), State (ONLINE/SHUTDOWN), 
        /// Alarmed (NOALARM/ALARM DESCRIPTION), Nameplate of Simulated Inverter
        /// </summary>
        public JObject Get()
        {
            JObject oSystemObject = Ihi_inverter_modbus_class.IHI_OBTAIN_MODBUS_INVERTER_SYSTEM_STATE();
            return oSystemObject;
        }
    }
}
