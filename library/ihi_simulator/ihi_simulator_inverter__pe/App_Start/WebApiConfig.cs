﻿using IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Web API routes
            Ihi_inverter_modbus_class ihi_inverter_sim = new Ihi_inverter_modbus_class();
            ihi_inverter_sim.IHI_INVERTER_SIMULATION_INITIALIZATION();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "ihi/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
