﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IHI_SIMULATOR_INVERTER_POWER_ELECTRONICS
{
    // Pure virtual class
    public abstract class Inverter
    {
        public abstract double getActualRealPower(short sRealPowerDemand);
        public abstract double getActualReactivePower(short sReactivePowerDemand);
        public abstract void setNamePlate(ushort usInverterNamePlate);
        public abstract void setNominalACVoltage(ushort usInverterNominalVoltage);
        public abstract ushort getPhaseAVoltage();
        public abstract ushort getPhaseBVoltage();
        public abstract ushort getPhaseCVoltage();
        public abstract double getPhaseACurrent();
        public abstract double getPhaseBCurrent();
        public abstract double getPhaseCCurrent();
        public abstract void setPLimit(ushort plimit);
        public abstract void setQLimit(ushort qlimit);
        public abstract ushort getFaultedState();
        public abstract ushort getEnabledValue();
        public abstract ushort getDisabledValue();

        public abstract ushort getStartCommand();
        public abstract void initialInverterConfiguration(Ihi_inverter_modbus_class oModClass);

        // Set methods to overload from REST API
        public abstract JObject getDemandInverterRealPower();
        public abstract JObject getDemandInverterReactivePower();
        public abstract JObject getActualInverterRealPower();
        public abstract JObject getActualInverterReactivePower();
        public abstract JObject getFrequency();
        public abstract JObject getDCVoltage();
        public abstract JObject getACVoltage();
        public abstract JObject getVoltagePhaseA();
        public abstract JObject getVoltagePhaseB();
        public abstract JObject getVoltagePhaseC();
        public abstract JObject getCurrentPhaseA();
        public abstract JObject getCurrentPhaseB();
        public abstract JObject getCurrentPhaseC();
        public abstract JObject getOperationalState();
        public abstract JObject getAlarm();
        public abstract JObject getWarningAlarm();

        // Set methods to overload from REST API
        public abstract void setDemandInverterRealPower(string strRealPowerActual);
        public abstract void setDemandInverterReactivePower(string strReactivePowerDemand);
        public abstract void updateActualInverterRealPower(string strRealPowerActual);
        public abstract void updateActualInverterReactivePower(string strReactivePowerActual);
        public abstract void updateFrequency(string strReactivePowerActual);
        public abstract void updateDCVoltage(string strDCVoltage);
        public abstract void updatePhaseAVoltage(string strPhaseAVoltage);
        public abstract void updatePhaseBVoltage(string strPhaseBVoltage);
        public abstract void updatePhaseCVoltage(string strPhaseCVoltage);
        public abstract void updatePhaseACurrent(string strPhaseACurrent);
        public abstract void updatePhaseBCurrent(string strPhaseBCurrent);
        public abstract void updatePhaseCCurrent(string strPhaseCCurrent);
        public abstract void updateOperationalState(string strState);
        public abstract void updateModelType(string strValue);
        public abstract void updateAlarm(string strAlarm);
        public abstract void updateWarningAlarm(string strAlarm);
        public abstract void clearAlarms();
    }

    public class PowerElectronicsInverter : Inverter
    {
        private double fActualRealPowerValue;
        private double fActualReactivePowerValue;
        private ushort usNamePlate;
        private ushort usNominalACVoltage;
        private double fPhaseAVoltage;
        private double fPhaseBVoltage;
        private double fPhaseCVoltage;
        private double fPhaseACurrent;
        private double fPhaseBCurrent;
        private double fPhaseCCurrent;
        private ushort usPLimit;
        private ushort usQLimit;

        private static ushort FAULTED = 9;
        private static ushort RUNNING = 6;
        private static ushort STOPPED = 2;
        private static ushort STARTED = 1;
        private Ihi_inverter_modbus_class _oModClass;

        // Obtain Real/Active Power Demand
        public override JObject getDemandInverterRealPower()
        {
            JObject oTempRealPowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("realpowercmd");
            string strRealPowerCommand = (string)oTempRealPowerCommand["value"];
            short sRealPowerCommand = short.Parse(strRealPowerCommand);
            double scaling = 10000;
            double fRealPowerCommand = (double)sRealPowerCommand;
            double fActualRealPowerDemand = fRealPowerCommand/ scaling;
            fActualRealPowerDemand = fActualRealPowerDemand * usNamePlate;
            short sReturnRealPower = (short)fActualRealPowerDemand;
            oTempRealPowerCommand["value"] = sReturnRealPower.ToString();
            return oTempRealPowerCommand;
        }

        // Obtain Reactive Power Demand
        public override JObject getDemandInverterReactivePower()
        {
            //return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("reactivepowercmd");
            JObject oTempReactivePowerCommand = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("reactivepowercmd");
            string strReactivePowerCommand = (string)oTempReactivePowerCommand["value"];
            short sReactivePowerCommand = short.Parse(strReactivePowerCommand);
            double scaling = 10000;
            double fReactivePowerCommand = (double)sReactivePowerCommand;
            double fActualReactivePowerDemand = fReactivePowerCommand / scaling;
            fActualReactivePowerDemand = fActualReactivePowerDemand * usNamePlate;
            short sReturnReactivePower = (short)fActualReactivePowerDemand;
            oTempReactivePowerCommand["value"] = sReturnReactivePower.ToString();
            return oTempReactivePowerCommand;
        }

        // Obtain Actual Inverter Real Power
        public override JObject getActualInverterRealPower()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridactivepower");
        }
        // Obtain Actual Reactive Power
        public override JObject getActualInverterReactivePower()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridreactivepower");
        }
        // Obtain Frequency
        public override JObject getFrequency()
        {
            JObject oTempFrequency =_oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("hz");
            string strFrequency = (string)oTempFrequency["value"];
            double sFrequency = double.Parse(strFrequency);
            if (sFrequency != 0)
            {
                sFrequency = sFrequency / 100;
            }
            oTempFrequency["value"] = sFrequency.ToString();
            return oTempFrequency;
        }

        // Obtain DC Voltage
        public override JObject getDCVoltage()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("dcvoltage");
        }
        // Obtain AC Voltage
        public override JObject getACVoltage()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("nominalacvoltage");
        }
        // Obtain Phase A Voltage
        public override JObject getVoltagePhaseA()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridvoltageab");
        }
        // Obtain Phase Voltage
        public override JObject getVoltagePhaseB()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridvoltagebc");
        }
        // Obtain Phase C Voltage
        public override JObject getVoltagePhaseC()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("gridvoltageca");
        }
        // Obtain Phase Current A
        public override JObject getCurrentPhaseA()
        {  
            JObject oTempPhaseA = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currentr");
            string strPhaseA = (string)oTempPhaseA["value"];
            short sPhaseA = short.Parse(strPhaseA);
            double scaling = 10;
            double fPhaseA = (double)sPhaseA;
            fPhaseA = fPhaseA / scaling;
           
            oTempPhaseA["value"] = fPhaseA.ToString();
            return oTempPhaseA;
        }

        // Obtain Phase Current B
        public override JObject getCurrentPhaseB()
        {
            JObject oTempPhaseB = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currents");
            string strPhaseB = (string)oTempPhaseB["value"];
            short sPhaseB = short.Parse(strPhaseB);
            double scaling = 10;
            double fPhaseB = (double)sPhaseB;
            fPhaseB = fPhaseB / scaling;
           
            oTempPhaseB["value"] = fPhaseB.ToString();
            return oTempPhaseB;
        }

        // Obtain Phase Current C
        public override JObject getCurrentPhaseC()
        {
            JObject oTempPhaseC = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currentt");
            string strPhaseC = (string)oTempPhaseC["value"];
            short sPhaseC = short.Parse(strPhaseC);
            double scaling = 10;
            double fPhaseC = (double)sPhaseC;
            fPhaseC = fPhaseC / scaling;
            
            oTempPhaseC["value"] = fPhaseC.ToString();
            return oTempPhaseC;
        }

        // Obtain Operational State
        public override JObject getOperationalState()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("currentstatus");
        }

        // Obtain Alarm value
        public override JObject getAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("fault");
        }

        // Obtain Warning Alarm
        public override JObject getWarningAlarm()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("warning");
        }

        // Set actual real power sent over REST API
        public override void updateActualInverterRealPower(string sValue)
        {
            // Set Actual Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("gridactivepower", usValue);
        }

        // Set demand real power sent over REST API
        public override void setDemandInverterRealPower(string sValue)
        {
            // Set Demand Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
          
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("realpowercmd", usValue);
        }

        // Set actual reactive power sent over REST API
        public override void updateActualInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Real Power

            ushort usValue = (ushort)Int16.Parse(sValue);
           
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("gridreactivepower", usValue);
        }

        // Set demand reactive power sent over REST API
        public override void setDemandInverterReactivePower(string sValue)
        {
            // Set Actual Inverter Reactive Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("reactivepowercmd", usValue);
        }

        // Set frequency
        public override void updateFrequency(string sValue)
        {
            // Set Actual Inverter Real Power
            ushort usValue = (ushort)Int16.Parse(sValue);
            int iValue = usValue * 100;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("hz", (ushort)iValue);
        }

        // Set DC Voltage
        public override void updateDCVoltage(string strDCVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strDCVoltage);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("dcvoltage", usValue);
        }

        // Set Phase A voltage
        public override void updatePhaseAVoltage(string strPhaseAVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseAVoltage);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("gridvoltageab", usValue);
        }

        // Set Phase B Voltage
        public override void updatePhaseBVoltage(string strPhaseBVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseBVoltage);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("gridvoltagebc", usValue);
        }

        // Phase C Voltage
        public override void updatePhaseCVoltage(string strPhaseCVoltage)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseCVoltage);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("gridvoltageca", usValue);
        }

        // Set A->B Current
        public override void updatePhaseACurrent(string strPhaseACurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseACurrent);
            int iValue = usValue * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("currentr", (ushort)iValue);
        }

        // Set B->C Current
        public override void updatePhaseBCurrent(string strPhaseBCurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseBCurrent);
            int iValue = usValue * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("currents", (ushort)iValue);
        }

        // Phase C->A Current
        public override void updatePhaseCCurrent(string strPhaseCCurrent)
        {
            ushort usValue = (ushort)Int16.Parse(strPhaseCCurrent);
            int iValue = usValue * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("currentt", (ushort) iValue);
        }

        // Set Operational status of inverter
        public override void updateOperationalState(string strState)
        {
            ushort usValue = (ushort)Int16.Parse(strState);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("currentstatus", usValue);
        }

        // Set Alarm
        public override void updateAlarm(string strAlarm)
        {
            ushort usValue = (ushort)Int16.Parse(strAlarm);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("fault", usValue);
        }

        // Set Warning Alarm
        public override void updateWarningAlarm(string strAlarm)
        {
            ushort usValue = (ushort)Int16.Parse(strAlarm);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("warning", usValue);
        }

        // Update Model Type
        public override void updateModelType(string strModelType)
        {
            // Model Type
            ushort usValue = (ushort)Int16.Parse(strModelType);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("modeltype", usValue);
        }

        // Clear all PE alarms
        public override void clearAlarms()
        {
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("fault", 0);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("warning", 0);
        }

        public override void initialInverterConfiguration(Ihi_inverter_modbus_class oModClass)
        {
            // Set initial nominal voltage to 
            usNominalACVoltage = 750;
            _oModClass = oModClass;
            oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("nominalacvoltage", usNominalACVoltage);

            // Set P Limit 100%
            oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("plimit", 100);

            // Set Q Limit 100%
            oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("qlimit", 100);
        }

        public override ushort getFaultedState()
        {
            return FAULTED;
        }

        public override ushort getEnabledValue()
        {
            return RUNNING;
        }

        public override ushort getDisabledValue()
        {
            return STOPPED;
        }

        public override ushort getStartCommand()
        {
            return STARTED;
        }

        // Determine Actual Real Power
        public override double getActualRealPower(short sRealPowerDemand)
        {
            double scaling = 10000;
            fActualRealPowerValue = sRealPowerDemand / scaling;

            // Nameplate is 2000kw for Power Electronics
            fActualRealPowerValue = fActualRealPowerValue * usNamePlate;

            // The PLimit and QLimits is a %. If a module on the inverter is disabled
            // this affect the limit
            double fpLimitDouble = (double)usPLimit;

            double fRealPowerRatio = (fpLimitDouble / 100);

            fActualRealPowerValue = fActualRealPowerValue * fRealPowerRatio;

            // Update the actual Real Power
            return fActualRealPowerValue;
        }

        // get Actual Reactive Power
        public override double getActualReactivePower(short sReactivePowerDemand)
        {
            double scaling = 10000;
            fActualReactivePowerValue = sReactivePowerDemand / scaling;

            // Nameplate is 2000kw for Power Electronics
            fActualReactivePowerValue = fActualReactivePowerValue * usNamePlate;

            // The PLimit and QLimits is a %. If a module on the inverter is disabled
            // this affect the limit
            double fqLimitDouble = (double)usQLimit;

            double fReactivePowerRatio = (fqLimitDouble / 100);

            fActualReactivePowerValue = fActualReactivePowerValue * fReactivePowerRatio;

            // Update the actual Reactie Power
            return fActualReactivePowerValue;
        }

        // Set the nameplate of the system
        public override void setNamePlate(ushort usInverterNamePlate)
        {
            usNamePlate = usInverterNamePlate;
        }

        //Set Nominal Voltage
        public override void setNominalACVoltage(ushort usInverterNominalVoltage)
        {
            usNominalACVoltage = usInverterNominalVoltage;
        }

        // Obtain Phase A Voltage
        public override ushort getPhaseAVoltage()
        {
            fPhaseAVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseAVoltage);
        }

        // Obtain Phase B Voltage
        public override ushort getPhaseBVoltage()
        {
            fPhaseBVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseBVoltage);
        }

        // Obtain Phase C Voltage
        public override ushort getPhaseCVoltage()
        {
            fPhaseCVoltage = (usNominalACVoltage) / Math.Sqrt(2);
            return Convert.ToUInt16(fPhaseCVoltage);
        }

        // Obtain Phase A Current
        public override double getPhaseACurrent()
        {
            fPhaseACurrent = ((fActualRealPowerValue * 1000) / fPhaseAVoltage);
            //System.Diagnostics.Trace.WriteLine("fActualRealPowerValue:"+fActualRealPowerValue+ "fPhaseVoltage:"+fPhaseAVoltage);
            //System.Diagnostics.Trace.WriteLine("fPhaseACurrent before" + fPhaseACurrent);
            fPhaseACurrent /= Math.Sqrt(3);
            fPhaseACurrent *= 10; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseACurrent *= -1;
            }
            //System.Diagnostics.Trace.WriteLine("fPhaseACurrent final" + fPhaseACurrent);
            return fPhaseACurrent;
        }

        // Obtain Phase B Current
        public override double getPhaseBCurrent()
        {
            fPhaseBCurrent = ((fActualRealPowerValue * 1000) / fPhaseBVoltage);
            fPhaseBCurrent /= Math.Sqrt(3);
            fPhaseBCurrent *= 10; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseBCurrent *= -1;
            }
            return fPhaseBCurrent;
        }

        // Obtain Phase C Current
        public override double getPhaseCCurrent()
        {
            fPhaseCCurrent = ((fActualRealPowerValue * 1000) / fPhaseCVoltage);
            fPhaseCCurrent /= Math.Sqrt(3);
            fPhaseCCurrent *= 10; // Scaled Current Value for decs

            if (fActualRealPowerValue < 0)
            {
                fPhaseCCurrent *= -1;
            }
            return fPhaseCCurrent;
        }

        // Set P Limit
        public override void setPLimit(ushort plimit)
        {
            usPLimit = plimit;
        }

        // Set Q Limit
        public override void setQLimit(ushort qlimit)
        {
            usQLimit = qlimit;
        }
    }
}