﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace IHI_SIMULATOR_BATTERY_LISHEN
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            Ihi_battery_modbus_class ihi_inverter_sim = new Ihi_battery_modbus_class();
            ihi_inverter_sim.IHI_BATTERY_SIMULATION_INITIALIZATION();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "ihi/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
         }
    }
}
