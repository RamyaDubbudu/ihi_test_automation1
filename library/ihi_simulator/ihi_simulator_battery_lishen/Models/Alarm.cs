﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IHI_SIMULATOR_BATTERY_LISHEN.Models
{
    public class Alarm
    {
        public string   id       { get; set; }   // Alarm Index
        public string   description { get; set; }   // Description
        public string   state      { get; set; }  // Trigger Alarm
    }
}