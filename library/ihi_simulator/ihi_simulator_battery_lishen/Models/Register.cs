﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IHI_SIMULATOR_BATTERY_LISHEN.Models
{
    public class BatteryRegister
    { 
       public int index        { get; set;  }   // Modbus Address
       public string type      { get; set; }   // Holding registers for Power Electronics 
       public string slaveid   { get; set; }   // Current Slave Id of the system
       public string label     { get; set; }   // Modbus Address Label
       public string value     { get; set; }   // Value
    }
}