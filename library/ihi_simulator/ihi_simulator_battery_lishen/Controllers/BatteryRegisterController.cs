﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IHI_SIMULATOR_BATTERY_LISHEN.Ihi_battery_modbus_class;
using Newtonsoft.Json.Linq;
using IHI_SIMULATOR_BATTERY_LISHEN.Models;

namespace IHI_SIMULATOR_BATTERY_LISHEN.Controllers
{
    public class BatteryRegisterController : Controller
    {
        // GET: BatteryRegister
        public ActionResult Index()
        {
            System.Diagnostics.Trace.WriteLine("Enter Battery Register Controller Index");
            return View();
        }

        // GET: BatteryRegister/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BatteryRegister/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BatteryRegister/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                System.Diagnostics.Trace.WriteLine("Enter Battery Register Controller Create");
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BatteryRegister/Edit/5
        public ActionResult Edit(int id)
        {
            for (int i = 0; i < Current_RegisterHoldingList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_RegisterHoldingList[i];

                BatteryRegister obj    = new BatteryRegister();
                obj.label               = (string)oTemp["label"];
                obj.index               = (int)oTemp["address"];
                obj.value               = (string)oTemp["value"];
                obj.type                = (string)oTemp["type"];
                obj.slaveid             = "1";

                // Pass register to be edited
                if (id.ToString()==obj.index.ToString())
                {
                    return View(obj);
                }
            }
            return View();
        }

        // POST: BatteryRegister/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // Obtain the register from collection
                string label = Convert.ToString(collection["label"]);
                string value = Convert.ToString(collection["value"]);
                string type  = Convert.ToString(collection["type"]);
                string index = Convert.ToString(collection["index"]);

                System.Diagnostics.Trace.WriteLine("Edited Value Label:"+label +" Value is"+value);
                IHI_SET_REGISTER_VALUE(type, label, index, value);

                return RedirectToAction("ShowRegisters");
            }
            catch
            {
                return View();
            }
        }

        // GET: InverterRegister/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }
        public ActionResult ShowRegisters(FormCollection collection)
        {
            int iRegCounter = 0;
            string strSearchPattern = Convert.ToString(collection["RegisterId"]);
           
            BatteryRegister[] auRegisters = new BatteryRegister[Current_RegisterHoldingList.Count];
            
            System.Diagnostics.Trace.WriteLine("Enter Show Registers:"+collection);

            for (int i = 0; i < Current_RegisterHoldingList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_RegisterHoldingList[i];

                // Create an Inverter Register Object
                BatteryRegister obj = new BatteryRegister();
                obj.label       = (string)oTemp["label"];
                obj.index       = (int)oTemp["address"];
                obj.value       = (string)oTemp["value"];
                obj.type        = (string)oTemp["type"];
                obj.slaveid     = "1";
               
                // Compare patterns of strings
                if (strSearchPattern != null)
                {
                    if (strSearchPattern.Length > 0)
                    {
                        // If no match on label and address then continue
                        if (!obj.label.Contains(strSearchPattern)
                                 &&
                            (!obj.index.ToString().Contains(strSearchPattern)))

                        {
                            continue;
                        }
                    }
                }
               
                // Increment register counter
                auRegisters[iRegCounter] = obj;
                iRegCounter++;
            }
            // Resize the array
            Array.Resize(ref auRegisters,iRegCounter);
            return View(auRegisters);
        }

        // POST: BatteryRegister/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        // POST: BatteryRegister/Delete/5
        [HttpPost]
        public ActionResult Save(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("ShowRegisters");
            }
            catch
            {
                return View();
            }
        }
    }
}
