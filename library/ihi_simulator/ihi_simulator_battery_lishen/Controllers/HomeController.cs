﻿using System.Web.Mvc;
using IHI_SIMULATOR_BATTERY_LISHEN.Models;
using IHI_SIMULATOR_BATTERY_LISHEN;
using Newtonsoft.Json.Linq;

namespace IHI_SIMULATOR_BATTERY_LISHEN.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "IHI Simulator Power Electronics";
            JObject oSystemObject = Ihi_battery_modbus_class.IHI_OBTAIN_MODBUS_BATTERY_SYSTEM_STATE();
            BatterySystem oGeneralInformation = new BatterySystem();
            oGeneralInformation.company = (string)oSystemObject["company"];
            oGeneralInformation.product = (string)oSystemObject["product"];
            oGeneralInformation.status = (string)oSystemObject["status"];
            oGeneralInformation.alarmed = (string)oSystemObject["alarmed"];
            oGeneralInformation.communication = (string)oSystemObject["communication"];
            return View(oGeneralInformation);
        }

        // GET: test/Create
        public ActionResult Create()
        {
            return View();
        }
    }
}
