﻿/*=============================================================================
Name        : CommunicationController.cs
Version     : 1.0
Copyright   : Copyright (c) 2016 IHI, Inc.  All Rights Reserved
Description : The Inverter Siimulator Controller.
This controller handles GET/POST REST API to determine whether
modbus communication to established between the inverter connector and 
inverter simulator. 
CONNECTED=Established communication
NOT CONNEDCTED= No Communication/Lost Communication
=============================================================================*/
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IHI_SIMULATOR_BATTERY_LISHEN.Controllers
{
    // Get Modbus Communication Status
    /// <summary>
    /// Get modbus communication status CONNECTED/NOT CONNECTED
    /// </summary>
    public class CommunicationController : ApiController
    {
        // Obtain the modbus communication object
        // PUT
        /// <summary>
        /// Get modbus commmunication state (CONNECTED/NOT CONNECTED)
        /// </summary>
        public JObject Get()
        {        
            JObject value = Ihi_battery_modbus_class.IHI_OBTAIN_MODBUS_REGISTER_COMMS();
            return value;
        }

        // PUT
        /// <summary>
        /// Set heartbeat timeout and whether heartbeat control is ON/OFF
        /// </summary>

        // PUT: ihi/communication
        public void Put()
        {
            // Obtain the Request Body 
            HttpContent requestContent = Request.Content;
            string jsonContent = requestContent.ReadAsStringAsync().Result;
            System.Diagnostics.Trace.WriteLine(jsonContent);
          
            IHI_COMMS oComms= Newtonsoft.Json.JsonConvert.DeserializeObject<IHI_COMMS>(jsonContent);
            Ihi_battery_modbus_class.IHI_SET_MODBUS_COMMUNICATION_TIMEOUT(oComms.heartbeattimeout, oComms.heartbeaton);
        }
    }
}
