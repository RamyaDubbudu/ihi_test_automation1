﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IHI_SIMULATOR_BATTERY_LISHEN.Models;
using Newtonsoft.Json.Linq;
using static IHI_SIMULATOR_BATTERY_LISHEN.Ihi_battery_modbus_class;

namespace IHI_SIMULATOR_BATTERY_LISHEN.Controllers
{
    public class AlarmController : Controller
    {
        // GET: Alarm
        public ActionResult Index()
        {
            return View(); 
        }

        // GET: Alarm/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
      
        public ActionResult ShowAlarms(FormCollection collection)
        {
            int iRegCounter = 0;

            Alarm[] auAlarms = new Alarm[Current_AlarmList.Count];

            // Cycle through all the alarms in the list
            for (int i = 0; i < Current_AlarmList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_AlarmList[i];

                // Create an Inverter Register Object
                Alarm obj       = new Alarm();
                obj.id          = (string)oTemp["id"];
                obj.description = (string)oTemp["description"];
                obj.state       = (string)oTemp["state"];

                // Increment alarm counter
                auAlarms[iRegCounter] = obj;
                iRegCounter++;
            }

            // Resize the alarm array
            Array.Resize(ref auAlarms, iRegCounter);
            return View(auAlarms);
        }

        // GET: Alarm/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Alarm/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Alarm/Edit/5
        public ActionResult Edit(int id)
        {
            int iRegCounter = 0;
 
            IHI_SET_FAULT_REGISTER_VALUE(id.ToString());

            id = id - 1;
            Alarm[] auAlarms = new Alarm[Current_AlarmList.Count];

            // Cycle through all the alarms in the list
            for (int i = 0; i < Current_AlarmList.Count; i++)
            {
                JObject oTemp = new JObject();
                oTemp = Current_AlarmList[i];

                // Create an Inverter Register Object
                Alarm obj = new Alarm();
                obj.id = (string)oTemp["id"];
                obj.description = (string)oTemp["description"];
                obj.state = (string)oTemp["state"];
                if (obj.state == "OFF")
                {
                    if (id == i)
                    {
                        obj.state = "ON";
                        Current_AlarmList[i]["state"] = "ON";
                    }
                }
              
                // Increment alarm counter
                auAlarms[iRegCounter] = obj;
                iRegCounter++;
            }

            // Resize the alarm array
            Array.Resize(ref auAlarms, iRegCounter);
            return View(auAlarms);
        }

        // POST: Alarm/Edit/5
        [HttpPost]
        public ActionResult Trigger(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
           
        }
        // GET: Alarm/Clear
        public ActionResult Clear(int id)
        {
            System.Diagnostics.Trace.WriteLine("Enter  Alarm:Clear");
            int iRegCounter = 0;
            try
            {
                // Clear the fault
               
                IHI_CLEAR_FAULT_REGISTER_VALUE();
                // Reduce to correct index
                id = id - 1;
                Alarm[] auAlarms = new Alarm[Current_AlarmList.Count];

                // Cycle through all the alarms in the list
                for (int i = 0; i < Current_AlarmList.Count; i++)
                {
                    JObject oTemp = new JObject();
                    oTemp = Current_AlarmList[i];

                    // Set initial state of alarm to OFF
                    Current_AlarmList[i]["state"] = "OFF";

                    // Create an Battery Register Object
                    Alarm obj       = new Alarm();
                    obj.id          = (string)oTemp["id"];
                    obj.description = (string)oTemp["description"];
                    obj.state       = (string)oTemp["state"];
              
                    // Increment alarm counter
                    auAlarms[iRegCounter] = obj;
                    iRegCounter++;
                }

                // Resize the alarm array
                Array.Resize(ref auAlarms, iRegCounter);
                return View(auAlarms);
            }
            catch
            {
                return View();
            }
        }

        // GET: Alarm/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Alarm/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
