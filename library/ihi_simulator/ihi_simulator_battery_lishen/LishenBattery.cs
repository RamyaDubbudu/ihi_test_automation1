﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IHI_SIMULATOR_BATTERY_LISHEN
{
    // Pure virtual class
    public abstract class Battery
    {
        public abstract void initialBatteryConfiguration(Ihi_battery_modbus_class oModClass);

        // Get methods to overload from REST API
        public abstract JObject getOperationalState();
        public abstract JObject getAlarm(string register);
        public abstract JObject getTotalVoltage();
        public abstract JObject getTotalCurrent();
        public abstract JObject getSoc();
        public abstract JObject getSoh();
        public abstract JObject getMinTemperature();
        public abstract JObject getMaxTemperature();
        public abstract JObject getAvgTemperature();
        public abstract JObject getMinTemperatureFah();
        public abstract JObject getMaxTemperatureFah();
        public abstract JObject getAvgTemperatureFah();
        public abstract JObject getChargeLimit();
        public abstract JObject getDischargeLimit();

        // Alarm Handling
        public abstract void clearAlarms();
        public abstract short getBatteryFault();

        // Perform updates
        public abstract void updateTotalVoltage(string strTotalVoltage);
        public abstract void updateTotalCurrent(string strTotalCurrent);
        public abstract void updateSoc(string strSoc);
        public abstract void updateSoh(string strSoh);
        public abstract void updateAvgTemperature(string strAvgTemperature);
        public abstract void updateMinTemperature(string strMinTemperature);
        public abstract void updateMaxTemperature(string strMinTemperature);
        public abstract void updateAlarm(string strRegister, string strAlarm);
        public abstract void updateOperationalState(string strOperational);
        public abstract ushort updateHeartbeat(string strRegister);
        public abstract void updateJoinRacks(string strValue);
        public abstract void updateChargeLimit(string strValue);
        public abstract void updateDischargeLimit(string strValue);
        public abstract void updateAvgTemperatureFah(string strAvgTemperature);
        public abstract void updateMinTemperatureFah(string strMinTemperature);
        public abstract void updateMaxTemperatureFah(string strMaxTemperature);
    }

    public class LishenBattery : Battery
    {
        private Ihi_battery_modbus_class _oModClass;
        private static ushort usHeartbeat;
        private static ushort join;
        private static ushort oldStatus;
        private static ushort newStatus;

        // Obtain Battery Alarm
        public override short getBatteryFault()
        {
            short sError = 0;
            //return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("reactivepowercmd");
            JObject oErrorBit1Fault = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("errorbit1");

            string strErrorBit1Fault = (string)oErrorBit1Fault["value"];
            short sErrorBit1Fault = short.Parse(strErrorBit1Fault);

            if (sErrorBit1Fault == 0)
            {
                JObject oErrorBit2Fault = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("errorbit2");

                string strErrorBit2Fault = (string)oErrorBit1Fault["value"];
                short sErrorBit2Fault = short.Parse(strErrorBit2Fault);
                if (sErrorBit2Fault == 0)
                {
                    JObject oErrorBit3Fault = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("errorbit3");

                    string strErrorBit3Fault = (string)oErrorBit1Fault["value"];
                    short sErrorBit3Fault = short.Parse(strErrorBit3Fault);
                    if (sErrorBit3Fault == 0)
                    {
                        sError = 0;
                    }
                    else
                    {
                        sError = sErrorBit3Fault;
                    }
                }
                else
                {
                    sError = sErrorBit2Fault;
                }
            }
            else
            {
                sError = sErrorBit1Fault;
            }

            return sError;
        }

        // Update the join racks value
        public override void updateJoinRacks(string strValue)
        {
            join = ushort.Parse(strValue);
        }

        // Update heartbeat 
        public override ushort updateHeartbeat(string strRegister)
        {
            if (usHeartbeat > 15)
            {
                usHeartbeat = 0;
            }
            else
            {
                usHeartbeat = (ushort)(usHeartbeat + 1);
            }
            int iHeartbeat = usHeartbeat << 4;

            JObject oStatus = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("status");

            string strStatus = (string)oStatus["value"];

            newStatus = ushort.Parse(strStatus);
            newStatus = (ushort)(newStatus & 0xC000);
            newStatus = (ushort)(iHeartbeat | (int)newStatus | join);
            if (oldStatus != newStatus)
            {
                _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL(strRegister, newStatus);
                oldStatus = newStatus;
            }
            return usHeartbeat;
        }

        // Update Charge Allowed
        public override void updateChargeLimit(string strValue)
        {
            ushort sChargeLimit = ushort.Parse(strValue);
            int iChargeLimit = sChargeLimit * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("allowchargepower", (ushort)iChargeLimit);
        }

        // Update Discharge Allowed
        public override void updateDischargeLimit(string strValue)
        {
            ushort sDischargeLimit = ushort.Parse(strValue);
            int iDischargeLimit = sDischargeLimit * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("allowdischargepower", (ushort)iDischargeLimit);
        }

        // Update Alarm
        public override void updateAlarm(string strRegister, string strAlarm)
        {
            ushort sAlarm = ushort.Parse(strAlarm);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL(strRegister, sAlarm);
        }

        // Update Total Voltage
        public override void updateTotalVoltage(string strTotalVoltage)
        {
            short sTotalVoltage = short.Parse(strTotalVoltage);
            // 10th of Units
            int iTempVoltage = sTotalVoltage * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("totalvoltage", (ushort)iTempVoltage);
        }

        // Update Total Current
        public override void updateTotalCurrent(string strTotalCurrent)
        {
            short sTotalCurrent = short.Parse(strTotalCurrent);
            // 10th of Units
            int iTempCurrent = sTotalCurrent * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("totalcurrent", (ushort)iTempCurrent);
        }

        // Update SOC
        public override void updateSoc(string strSoc)
        {
            short sSoc = short.Parse(strSoc);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("soc", (ushort)sSoc);
        }

        // Update SOH
        public override void updateSoh(string strSoh)
        {
            short sSoh = short.Parse(strSoh);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("soh", (ushort)sSoh);
        }

        // Update Average Temperature
        public override void updateAvgTemperature(string strAvgTemperature)
        {
            short sAvgTemperature = short.Parse(strAvgTemperature);
            int iTempAvg = sAvgTemperature * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("avgtemperature", (ushort)iTempAvg);
        }

        // Update Average Temperature Fahrenheit temperature
        public override void updateAvgTemperatureFah(string strAvgTemperature)
        {
            float sAvgTemperature = float.Parse(strAvgTemperature);
            // Temperature received is in fahreneit need to convert to Celsius and scale
            double fTemp = (5.0 / 9.0) * (sAvgTemperature - 32);
            fTemp = fTemp * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("avgtemperature", (ushort)fTemp);
        }

        // Update Min Temperature Fahrenheit
        public override void updateMinTemperatureFah(string strMinTemperature)
        {
            float sMinTemperature = float.Parse(strMinTemperature);
            // Temperature received is in fahreneit need to convert to Celsius and scale
            double fTemp=(5.0 / 9.0) * ( sMinTemperature- 32);           
            fTemp = fTemp * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("mintemperature", (ushort)fTemp);
        }

        // Update Max Temperature Fahreneit
        public override void updateMaxTemperatureFah(string strMaxTemperature)
        {
            float sMaxTemperature = float.Parse(strMaxTemperature);
            // Temperature received is in fahreneit need to convert to Celsius and scale
            double fTemp = (5.0 / 9.0) * (sMaxTemperature - 32);
            fTemp = fTemp * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("maxtemperature", (ushort)fTemp);
        }

        // Update Min Temperature
        public override void updateMinTemperature(string strMinTemperature)
        {
            short sMinTemperature = short.Parse(strMinTemperature);
            int iTempMin = sMinTemperature * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("mintemperature", (ushort)iTempMin);
        }

        // Update Max Temperature
        public override void updateMaxTemperature(string strMinTemperature)
        {
            short sMaxTemperature = short.Parse(strMinTemperature);
            int iTempMax = sMaxTemperature * 10;
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("maxtemperature", (ushort)iTempMax);
        }

        // Update operational state
        public override void updateOperationalState(string strContactors)
        {
            short sConnected = short.Parse(strContactors);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("contactors", (ushort)sConnected);
        }

        // Obtain Total Voltage and scale if needed
        public override JObject getTotalVoltage()
        {
            JObject oTempTotalVoltage = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("totalvoltage");
            string strTotalVoltage = (string)oTempTotalVoltage["value"];
            short sTotalVoltage = short.Parse(strTotalVoltage);

            int scaling = 10; // 10th of Units
            int iTempValue = sTotalVoltage / scaling;
            sTotalVoltage = (short)iTempValue;

            oTempTotalVoltage["value"] = sTotalVoltage.ToString();
            return oTempTotalVoltage;
        }

        // Obtain Discharge Allowed
        public override JObject getDischargeLimit()
        {
            JObject oTempDischargePower = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("allowdischargepower");
            string strDischargePower = (string)oTempDischargePower["value"];
            short sDischargePower = short.Parse(strDischargePower);

            int scaling = 10; // 10th of Units
            int iTempValue = sDischargePower / scaling;

            oTempDischargePower["value"] = iTempValue.ToString();
            return oTempDischargePower;
        }

        // Obtain Charge Allowed
        public override JObject getChargeLimit()
        {
            JObject oTempChargePower = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("allowchargepower");
            string strChargePower = (string)oTempChargePower["value"];
            short sChargePower = short.Parse(strChargePower);

            int scaling = 10; // 10th of Units
            int iTempValue = sChargePower / scaling;

            oTempChargePower["value"] = iTempValue.ToString();
            return oTempChargePower;
        }

        // Obtain Total Current
        public override JObject getTotalCurrent()
        {
            JObject oTempTotalCurrent = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("totalcurrent");
            string strTotalVoltage = (string)oTempTotalCurrent["value"];
            short sTotalCurrent = short.Parse(strTotalVoltage);

            int scaling = 10; // 10th of Units
            int iTempValue = sTotalCurrent / scaling;
            sTotalCurrent = (short)iTempValue;
            oTempTotalCurrent["value"] = sTotalCurrent.ToString();
            return oTempTotalCurrent;
        }

        // Obtain SOC (State of Charge)
        public override JObject getSoc()
        {
            JObject oTempSoc = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("soc");
            return oTempSoc;
        }

        // Obtain SOH (State of Health)
        public override JObject getSoh()
        {
            JObject oTempSoh = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("soh");
            return oTempSoh;
        }

        // Obtain Average Temperature
        public override JObject getAvgTemperature()
        {
            JObject oTempAvgTemperature = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("avgtemperature");
            string strAvgTemp = (string)oTempAvgTemperature["value"];
            short sAvgTemp = short.Parse(strAvgTemp);

            int scaling = 10; // 10th of Units
            int iTempValue = sAvgTemp / scaling;
            oTempAvgTemperature["value"] = iTempValue.ToString();
            return oTempAvgTemperature;
        }

        // Obtain Max Battery Temperature
        public override JObject getMaxTemperature()
        {
            JObject oTempMaxTemperature = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("maxtemperature");
            string strMaxTemp = (string)oTempMaxTemperature["value"];
            short sMaxTemp = short.Parse(strMaxTemp);

            int scaling = 10; // 10th of Units
            int iTempValue = sMaxTemp / scaling;
            oTempMaxTemperature["value"] = iTempValue.ToString();
            return oTempMaxTemperature;
        }

        // Obtain Min Battery Temperature
        public override JObject getMinTemperature()
        {
            JObject oTempMinTemperature = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("mintemperature");
            string strMinTemp = (string)oTempMinTemperature["value"];
            short sMaxTemp = short.Parse(strMinTemp);

            int scaling = 10; // 10th of Units
            int iTempValue = sMaxTemp / scaling;
            oTempMinTemperature["value"] = iTempValue.ToString();
            return oTempMinTemperature;
        }

        // Obtain Average Temperature Fahrenheit
        public override JObject getAvgTemperatureFah()
        {
            JObject oTempAvgTemperature = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("avgtemperature");
            string strAvgTemp = (string)oTempAvgTemperature["value"];
            short sAvgTemp = short.Parse(strAvgTemp);

            int scaling = 10; // 10th of Units
            double fAvgTemp = sAvgTemp / scaling;
            fAvgTemp = (fAvgTemp * 1.8) + 32;

            oTempAvgTemperature["value"] = fAvgTemp.ToString();
            return oTempAvgTemperature;
        }

        // Obtain Max Battery Temperature Fahrenheit
        public override JObject getMaxTemperatureFah()
        {
            JObject oTempMaxTemperature = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("maxtemperature");
            string strMaxTemp = (string)oTempMaxTemperature["value"];
            short sMaxTemp = short.Parse(strMaxTemp);

            int scaling = 10; // 10th of Units
            double fMaxTemp = sMaxTemp / scaling;
            fMaxTemp = (fMaxTemp * 1.8) + 32;
            oTempMaxTemperature["value"] = fMaxTemp.ToString();
            return oTempMaxTemperature;
        }

        // Obtain Min Battery Temperature
        public override JObject getMinTemperatureFah()
        {
            JObject oTempMinTemperature = _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("mintemperature");
            string strMinTemp = (string)oTempMinTemperature["value"];
            short sMinTemp = short.Parse(strMinTemp);

            int scaling = 10; // 10th of Units
            double fMinTemp = sMinTemp / scaling;
            fMinTemp = (fMinTemp * 1.8) + 32;
            oTempMinTemperature["value"] = fMinTemp.ToString();
            return oTempMinTemperature;
        }

        // Obtain Operational State
        public override JObject getOperationalState()
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER("status");
        }

        // Obtain Alarm value
        public override JObject getAlarm(string register)
        {
            return _oModClass.IHI_OBTAIN_MODBUS_HOLDING_REGISTER(register);
        }

        // Clear all Battery alarms
        public override void clearAlarms()
        {
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("errorbit1", 0);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("errorbit2", 0);
            _oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("errorbit3", 0);
        }

        // Initialize battery configuration
        public override void initialBatteryConfiguration(Ihi_battery_modbus_class oModClass)
        {
            // Set initial nominal voltage to 
            _oModClass = oModClass;
            oModClass.IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("status", 0x4000);
        }
    }
}