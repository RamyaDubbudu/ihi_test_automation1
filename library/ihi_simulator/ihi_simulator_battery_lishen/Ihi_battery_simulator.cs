﻿/*=============================================================================
Name        : Ihi_battery_simulator.cs
Author      : G.Clark
Version     : 1.0
Copyright   : Copyright (c) 2016 IHI, Inc.  All Rights Reserved
Description : The Ihi_Batteery_simulator.

This class maps incomming REST API requests GET/PUT to a modbus register
map. It communicates to the battery connector via modbus.
By default it acts like the battery and simulates actions that
the battery would make to bring it online/offline and returns
voltage, current and soc, soh values of the system,
External REST API request can still be processed while still connected 
to ES Pilot.
=============================================================================*/

using IHI_SIMULATOR_BATTERY_LISHEN.Models;
using Modbus.Data;
using Modbus.Device;
using Nerdle.AutoConfig;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

#pragma warning disable 1591

namespace IHI_SIMULATOR_BATTERY_LISHEN
{

    public class Ihi_battery_modbus_class
    {
        public static string strChannelState;
        public static Boolean bConnectToSlave = false;
        private System.Timers.Timer m_timerSequenceNumber;

        // Get properties of ihiModbusObject
        public interface IihiModbusObject
        {
            int index { get; set; }   // Modbus Address
            string type { get; set; }   // Holding registers for Power Electronics 
            string slaveid { get; set; }   // Current Slave Id of the system
            string label { get; set; }   // Modbus Address Label
            string mandatory { get; set; }   // Modbus Adddress shows if used by the system
        }

        // Get properties of Modbus Input Object Collection
        public interface IIhiModbusHoldingObjectCollection
        {
            IEnumerable<IihiModbusObject> ihiModbusObjects { get; }
        }

        // Get properties of Modbus Output Object Collection
        public interface IIhiModbusInputObjectCollection
        {
            IEnumerable<IihiModbusObject> ihiModbusObjects { get; }
        }

        // Get properties of ihiAlarmObject
        public interface IihiAlarmObject
        {
            string id { get; set; }   // Alarm id
            string description { get; set; }   // Alarm Description
            string value { get; set; }
            string register { get; set; }
            string severity { get; set; }   // Alarm Severity
        }

        // Get properties of Alarm Object Collection
        public interface IIhiAlarmObjectCollection
        {
            IEnumerable<IihiAlarmObject> ihiAlarmObjects { get; }
        }

        public static ModbusSlave[] ModbusSlave = new ModbusSlave[1];
        public Thread[] slaveThreads = new Thread[1];

        public static TcpListener server;
        public static int iSlaveCount;
        public static string m_StrCurrentSlaveId;
        public static List<JObject> Current_RegisterHoldingList;
        public static List<JObject> current_registerHoldingList;
        public static List<JObject> Current_RegisterInputList;
        public static List<JObject> current_registerInputList;
        public static JObject oIHI_Registerlists;
        public static JObject oIhiBatteryRegisterValue;
        public static List<JObject> Current_AlarmList;

        public static ushort usHeartbeat;
        public static ushort usLastheartbeat;
        public static UInt32 i32uTimeOutCounter;
        private static UInt32 usModbusCommunicationTimeout;
        public static BatterySystem oBatterySystemObject = new BatterySystem();
        private static string strHeartBeatCheckingCtrl; // Determine whether to take action on heartbeat from Battery connector ON/OFF
        public static string strAutonomous;

        private ushort usNew_ContactorsCommand;
        private ushort usOld_ContactorsCommand;
        private short usOld_CurrentStatus;
        private ushort usNew_CurrentStatus;

        private short usNew_Status;
        private static Battery oBatteryInstance;

        public class ModbusRegister
        {
            public bool bChanged;
            public ushort usValue { get; set; }
            public int iOffset { get; set; }
        }

        public static List<ModbusRegister> auIncommingRegisters = new List<ModbusRegister>();
        public static List<ModbusRegister> auHoldingRegisters = new List<ModbusRegister>();

        // A write request from battery connector
        private void Modbus_DataStoreWriteTo(object sender, DataStoreEventArgs e)
        {
            usHeartbeat = IHI_UPDATE_HEARTBEAT("status");

            switch (e.ModbusDataType)
            {
                // Set A0

                case ModbusDataType.HoldingRegister:
                    {
                        for (int i = 0; i < e.Data.B.Count; i++)
                        {
                            System.Diagnostics.Trace.WriteLine("Modbus_DataStoreWriteTo e.Data.B is " + e.Data.B[0]);
                            System.Diagnostics.Trace.WriteLine("Enter Modbus_DataStoreWriteTo getModbusRegisterOffset Address:" + e.StartAddress + 1 + "Value=" + ModbusSlave[0].DataStore.HoldingRegisters[e.StartAddress + 1 + i]);
                        }

                    }
                    break;
            }
        }

        // The battery connector is attempting to read data. This callback is fired
        // when it receives that event. Populate the registers
        private void Modbus_DataStoreReadFrom(object sender, DataStoreEventArgs e)
        {
            if (strChannelState == "CONNECTED")
            {
                // Update heartbeat
                usHeartbeat = IHI_UPDATE_HEARTBEAT("status");
            }

            //System.Diagnostics.Trace.WriteLine("Enter Modbus_DataStoreReadFrom Count is " + e.Data.B.Count);
            IHI_UPDATE_MODBUS_WRITE_REQUESTS();
        }

        // Initallize the IHI battery simulator web server
        public void IHI_BATTERY_SIMULATION_INITIALIZATION()
        {
            // Reset any write holding/Input Register List
            auHoldingRegisters.Clear();
            auIncommingRegisters.Clear();

            var settings = System.Configuration.ConfigurationManager.AppSettings;
            oBatterySystemObject.company = settings["company"];
            oBatterySystemObject.product = settings["product"];
            oBatterySystemObject.racks = settings["racks"];
            string strModbusId = settings["modbusport"];

            // Instantiate Power Electronics Battery Object
            oBatteryInstance = new LishenBattery();

            // Obtain modbus port id
            ushort usModbusPort = ushort.Parse(strModbusId);

            // Create a Tcp Listener and bind that to the Modbus Slave
            server = new TcpListener(System.Net.IPAddress.Any, usModbusPort);

            // Start Modbus Server
            server.Start();

            // Initialize the Timeout Counters
            i32uTimeOutCounter = 0;
            usHeartbeat = 0;
            usLastheartbeat = 0;

            // Turn heartbeat off
            strChannelState = "NOT CONNECTED";

            // Default turn heartbeat checking off
            strHeartBeatCheckingCtrl = "OFF";

            usModbusCommunicationTimeout = 10; // Number of seconds before comms considered gone
            oBatterySystemObject.alarmed = "NOFAULT";
            oBatterySystemObject.communication = "NOT CONNECTED";
            oBatterySystemObject.status = "SHUTDOWN";

            // Battery State Autonomous meaning it will running and 
            // can go online without any intervention from user
            strAutonomous = "TRUE";

            // New Add Variation
            usNew_Status = -1;

            // Create TCP Client
            server.BeginAcceptTcpClient(new AsyncCallback(IHI_ACCEPT_CLIENT), this);

            // Bind the TcpListener to the modbus slave
            ModbusSlave[0] = ModbusTcpSlave.CreateTcp(1, server);

            // Create the data store
            ModbusSlave[0].DataStore = DataStoreFactory.CreateDefaultDataStore();

            // Added event handler requests from IHI battery connector
            ModbusSlave[0].DataStore.DataStoreWrittenTo += new EventHandler<DataStoreEventArgs>(Modbus_DataStoreWriteTo);
            ModbusSlave[0].DataStore.DataStoreReadFrom += new EventHandler<DataStoreEventArgs>(Modbus_DataStoreReadFrom);

            // Obtain the modbus slave thread
            slaveThreads[0] = new Thread(ModbusSlave[0].Listen);

            // Start the modbus start listening
            slaveThreads[0].Start();

            // Create JSON Object register lists
            Current_RegisterHoldingList = new List<JObject>();
            Current_RegisterInputList = new List<JObject>();
            Current_AlarmList = new List<JObject>();

            oIHI_Registerlists = new JObject();
            oIhiBatteryRegisterValue = new JObject();

            // Add Holding Registers
            // Obtain the Modbus Holding collection from the appConfig
            var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

            // Cycle through the Holding Modbus Collection
            foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
            {
                // Determine modbus offset
                int i32uModbusOffset = modbusObject.index;

                // Assign the modbus register value at given offset
                ModbusRegister modbusReg = new ModbusRegister();
                modbusReg.usValue = 0;
                modbusReg.iOffset = i32uModbusOffset;
                modbusReg.bChanged = false;

                // Append onto the list
                auHoldingRegisters.Add(modbusReg);
            }

            // Cycle through the alarms and obtain a description   
            var alarmObjectCollection = AutoConfig.Map<IIhiAlarmObjectCollection>();

            // Cycle through the Alarm Register List
            foreach (IihiAlarmObject alarmObject in alarmObjectCollection.ihiAlarmObjects)
            {
                JObject oAlarm = new JObject();
                oAlarm["id"] = alarmObject.id;
                oAlarm["description"] = alarmObject.description;
                oAlarm["state"] = "OFF";
                Current_AlarmList.Add(oAlarm);
            }
        }

        // Pass the label of the register you want to update
        private int getModbusRegisterOffset(string strLabel)
        {
            // Cycle through the Holding Modbus Collection
            int i32sOffset = 0;

            // Obtain configuration registers lists
            var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();
            foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
            {
                // Determine if matched register label
                if (strLabel == modbusObject.label)
                {
                    // Set offset to matched index
                    i32sOffset = modbusObject.index;
                    break;
                }
            }
            if (i32sOffset == 0)
            {
                System.Diagnostics.Trace.WriteLine("Enter getModbusRegisterOffset Not found:" + strLabel);
            }
            return i32sOffset;
        }

        // Control the writes to the modbus interface
        private void IHI_READ_WRITE_MODBUS_REGISTER(int offset, string action, ushort usTargetValue, ref ushort outValue)
        {
            if (action == "UPDATE")
            {
                try
                {
                    ModbusSlave[0].DataStore.HoldingRegisters[offset] = usTargetValue;
                }

                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("Enter WRITE actOnModbusRegister FAILED:" + e.Message);
                }
            }
            if (action == "READ")
            {
                try
                {
                    outValue = ModbusSlave[0].DataStore.HoldingRegisters[offset];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("Enter READ Register:FAILED" + e.Message);
                }
            }
        }

        // Update the modbus register
        public void IHI_UPDATE_MODBUS_REGISTER_AT_LABEL(string registerLabel, ushort usValue)
        {
            int i32uModbusOffset = getModbusRegisterOffset(registerLabel);

            // Determine if already in List
            int index = auHoldingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);

            // Register exists in the Holding Register List
            if (index >= 0)
            {
                auHoldingRegisters[index].usValue = usValue;
                auHoldingRegisters[index].bChanged = true;
                //System.Diagnostics.Trace.WriteLine("Enter IHI_UPDATE_MODBUS_REGISTER_AT_LABEL Changed registerLabel:" + registerLabel + "Value:"+usValue);
            }
            else
            {
                // Assign the modbus register value at given offset

                ModbusRegister modbusReg = new ModbusRegister();
                modbusReg.usValue = usValue;
                modbusReg.iOffset = i32uModbusOffset;
                modbusReg.bChanged = true;

                // Append onto the list
                auHoldingRegisters.Add(modbusReg);
            }
        }

        // Obtain the register Value
        private short IHI_OBTAIN_MODBUS_REGISTER_VALUE(string strLabel)
        {
            int i32uModbusOffset = getModbusRegisterOffset(strLabel);
            ushort usValue = 0;
            IHI_READ_WRITE_MODBUS_REGISTER(i32uModbusOffset, "READ", 0, ref usValue);
            return (short)usValue;
        }

        // Cycle through the alarms and obtain a description
        private string IHI_OBTAIN_ALARM_DESCRIPTION(string strId)
        {
            string match = "UNKNOWN";
            var alarmObjectCollection = AutoConfig.Map<IIhiAlarmObjectCollection>();

            // Cycle through the Alarm Register List
            foreach (IihiAlarmObject alarmObject in alarmObjectCollection.ihiAlarmObjects)
            {
                // Determine matched alarmed
                if (alarmObject.id == strId)
                {
                    match = alarmObject.description;
                    break;
                }
            }
            return match;
        }

        // Used check if battery is running state and act accordingly i.e check contactors
        private void IHI_CHECK_AUTONOMOUS_REGISTERS()
        {
            try
            {
                // Check Contactors command close/open
                JObject oContactorsCommand = IHI_OBTAIN_MODBUS_HOLDING_REGISTER("contactors");
                string strContactorsCommand = (string)oContactorsCommand["value"];
                usNew_ContactorsCommand = ushort.Parse(strContactorsCommand);

                JObject oStatus = IHI_OBTAIN_MODBUS_HOLDING_REGISTER("status");
                string strStatus = (string)oContactorsCommand["value"];
                usNew_Status = short.Parse(strStatus);

                // Determine if the system has faulted
                short fault = oBatteryInstance.getBatteryFault();

                if (fault > 0)
                {
                    IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("status", 0x0000);
                    usNew_Status = IHI_OBTAIN_MODBUS_REGISTER_VALUE("status");
                    if (usOld_CurrentStatus != usNew_Status)
                    {
                        oBatterySystemObject.alarmed = IHI_OBTAIN_ALARM_DESCRIPTION(fault.ToString());
                        usOld_CurrentStatus = usNew_Status;
                    }
                }
                else
                {
                    oBatterySystemObject.alarmed = "NOFAULT";
                    // Determine if current status was fault and all faults been removed
                }

                //   Determine if Close Contactors Command and no fault
                if (usOld_ContactorsCommand != usNew_ContactorsCommand && fault == 0)
                {
                    if (usNew_ContactorsCommand == 0x3c3c)// 0x3c3c
                    {
                        System.Diagnostics.Trace.WriteLine("Contactors Command" + usNew_ContactorsCommand);
                        oBatteryInstance.updateJoinRacks("12"); // 0xc to join racks

                        // Set Status to ONLINE
                        oBatterySystemObject.status = "ONLINE";
                    }
                    else
                    {
                        usOld_ContactorsCommand = 0;
                        oBatteryInstance.updateJoinRacks("0");
                        IHI_UPDATE_MODBUS_REGISTER_AT_LABEL("status", 0x4000);
                        oBatterySystemObject.status = "SHUTDOWN";
                    }
                    usOld_ContactorsCommand = usNew_ContactorsCommand;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine("Failed checkAutonmonous register write!!" + e.Message);
            }
        }

        // Set Communication Timeout and Heartbeat Ctrl
        public static void IHI_SET_MODBUS_COMMUNICATION_TIMEOUT(string strCommunicationTimeout, string strHBOn)
        {
            usModbusCommunicationTimeout = UInt32.Parse(strCommunicationTimeout);

            // Determine if heartbeat control is ON/OFF
            // The battery connector sends a heartbeat to ensure communication is still good
            strHeartBeatCheckingCtrl = strHBOn;
        }

        // Clear All alarms
        public static void IHI_CLEAR_FAULT_REGISTER_VALUE()
        {
            oBatteryInstance.clearAlarms();
        }

        // An alarm has been triggered and the modbus register needs to be updated.
        public static void IHI_SET_FAULT_REGISTER_VALUE(string RegisterId)
        {
            var ihiAlarmObjectCollection = AutoConfig.Map<IIhiAlarmObjectCollection>();

            // Cycle through the Alarms and match the alarm
            foreach (IihiAlarmObject alarmObject in ihiAlarmObjectCollection.ihiAlarmObjects)
            {
                if (RegisterId == alarmObject.id)
                {
                    // Matches alarm object
                    oBatteryInstance.updateAlarm(alarmObject.register, alarmObject.value);
                }
            }
        }

        // Set a Register Value in the modbus register
        public static void IHI_SET_REGISTER_VALUE(string RegisterType, string RegisterLabel, string RegisterAddress, string RegisterValue)
        {
            try
            {
                dynamic ihi_register = new JObject();
                if (RegisterType == "holding")
                {
                    // Obtain the Modbus Holding collection from the appConfig
                    var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                    // Cycle through the Holding Modbus Collection
                    foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                    {
                        // Determine if matched register address/label
                        if (RegisterAddress == modbusObject.index.ToString()
                                            ||
                            RegisterLabel == modbusObject.label)
                        {
                            // Determine modbus offset
                            int i32uModbusOffset = modbusObject.index;

                            // Assign the value to Input Register
                            ushort usValue = (ushort)int.Parse(RegisterValue);

                            // Determine if already in List
                            int index = auHoldingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);

                            // Register exists in the Holding Register List
                            if (index >= 0)
                            {
                                auHoldingRegisters[index].usValue = usValue;
                                auHoldingRegisters[index].bChanged = true;

                                //System.Diagnostics.Trace.WriteLine("IHI_SET_REGISTER_VALUE Found Register:Address:" + index + " Value:" + usValue + "Offset ="+ auHoldingRegisters[index].iOffset);
                            }
                            else
                            {
                                // Assign the modbus register value at given of offset
                                ModbusRegister modbusReg = new ModbusRegister();
                                modbusReg.usValue = usValue;
                                modbusReg.iOffset = i32uModbusOffset;
                                modbusReg.bChanged = true;
                                //System.Diagnostics.Trace.WriteLine("IHI_SET_REGISTER_VALUE New Register:Address:" + index + " Value:" + usValue);
                                // Append onto the list
                                auHoldingRegisters.Add(modbusReg);
                            }
                            break;
                        }
                    }
                }
                else
                {
                    // Determine if modbus input register
                    if (RegisterType == "input")
                    {
                        // Obtain the Modbus input collection from the appConfig
                        var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

                        // Cycle through the Holding Modbus Collection
                        foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
                        {
                            // Determine if matched register address
                            if (RegisterAddress == modbusObject.index.ToString()
                                                ||
                                RegisterLabel == modbusObject.label)
                            {
                                // Determine the offset
                                int i32uModbusOffset = modbusObject.index;

                                ushort usValue = (ushort)int.Parse(RegisterValue);

                                // Determine if register already in List
                                int index = auIncommingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);

                                // Register exists in the Incomming Regiser List
                                if (index >= 0)
                                {
                                    auIncommingRegisters[index].usValue = usValue;
                                    auIncommingRegisters[index].bChanged = true;
                                    System.Diagnostics.Trace.WriteLine("Found Register:Address:" + index + " Value:" + usValue);
                                }
                                else
                                {
                                    // Update the registers at Modbus Offset
                                    ModbusRegister modbusReg = new ModbusRegister();

                                    // Assign value and address
                                    modbusReg.usValue = usValue;
                                    modbusReg.iOffset = i32uModbusOffset;
                                    modbusReg.bChanged = true;
                                    System.Diagnostics.Trace.WriteLine("New Register:Address:" + index + " Value:" + usValue);
                                    auIncommingRegisters.Add(modbusReg);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
            }
        }

        // Return the JSON object containing the communication state
        public static JObject IHI_OBTAIN_MODBUS_REGISTER_COMMS()
        {
            dynamic ihi_modbus_comms = new JObject();
            ihi_modbus_comms.communication = strChannelState;
            oIhiBatteryRegisterValue = ihi_modbus_comms;
            return oIhiBatteryRegisterValue;
        }

        // Obtain the Battery System High Level Informaton
        // Battery Company
        // Battery Version
        // Modbus Communication
        // Running/Not Running
        // Faulted/Not Faulted
        // Nameplate added to the battery
        public static JObject IHI_OBTAIN_MODBUS_BATTERY_SYSTEM_STATE()
        {
            JObject oGeneralInformation = new JObject();
            // General High Level Information properties
            oGeneralInformation["alarmed"] = oBatterySystemObject.alarmed;
            oGeneralInformation["company"] = oBatterySystemObject.company;
            oGeneralInformation["status"] = oBatterySystemObject.status;
            oGeneralInformation["communication"] = oBatterySystemObject.communication;
            oGeneralInformation["product"] = oBatterySystemObject.product;
            oGeneralInformation["racks"] = oBatterySystemObject.racks;
            return oGeneralInformation;
        }

        // Set Battery to operate independently
        public static void IHI_SET_BATTERY_AUTONOMOUS(string autonomous)
        {
            strAutonomous = autonomous;
        }

        // Perform an action based on string
        public static JObject IHI_GET_BATTERY_SIMULATOR_PROPERTY(string property)
        {
            JObject oRegisterValue = null;
            switch (property)
            {
                // Get totalvoltage
                case "total_voltage":
                    {
                        oRegisterValue = oBatteryInstance.getTotalVoltage();
                        break;
                    }

                // Get total current
                case "total_current":
                    {
                        oRegisterValue = oBatteryInstance.getTotalCurrent();
                        break;
                    }

                // Get soc
                case "soc":
                    {
                        oRegisterValue = oBatteryInstance.getSoc();
                        break;
                    }

                // Update soh
                case "soh":
                    {
                        oRegisterValue = oBatteryInstance.getSoh();
                        break;
                    }

                // Get Minimum Temperature
                case "mintemperature":
                    {
                        oRegisterValue = oBatteryInstance.getMinTemperature();
                        break;
                    }

                // Get Average Temperature
                case "avgtemperature":
                    {
                        oRegisterValue = oBatteryInstance.getAvgTemperature();
                        break;
                    }

                // Get dc voltage
                case "maxtemperature":
                    {
                        oRegisterValue = oBatteryInstance.getMaxTemperature();
                        break;
                    }

                // Get Minimum Fahrenheit Temperature
                case "mintemperaturefah":
                    {
                        oRegisterValue = oBatteryInstance.getMinTemperatureFah();
                        break;
                    }

                // Get Average Temperature
                case "avgtemperaturefah":
                    {
                        oRegisterValue = oBatteryInstance.getAvgTemperatureFah();
                        break;
                    }

                // Get dc voltage
                case "maxtemperaturefah":
                    {
                        oRegisterValue = oBatteryInstance.getMaxTemperatureFah();
                        break;
                    }

                // Change Operational State
                case "operational_state":
                    {
                        oRegisterValue = oBatteryInstance.getOperationalState();
                        break;
                    }

                // Get Alarms
                case "fault1":
                    {
                        oRegisterValue = oBatteryInstance.getAlarm("errorbit1");
                        break;
                    }
                case "fault2":
                    {
                        oRegisterValue = oBatteryInstance.getAlarm("errorbit2");
                        break;
                    }
                case "fault3":
                    {
                        oRegisterValue = oBatteryInstance.getAlarm("errorbit3");
                        break;
                    }

                // Charge Limit
                case "charge_limit":
                    {
                        oRegisterValue = oBatteryInstance.getChargeLimit();
                        break;
                    }

                // Discharge Limit
                case "discharge_limit":
                    {
                        oRegisterValue = oBatteryInstance.getDischargeLimit();
                        break;
                    }
                default:
                    break;
            }
            return oRegisterValue;
        }

        // Update a register that is captured by the battery connector
        public static void IHI_SET_BATTERY_UPDATE_MANDATORY_REGISTER(string action, string value)
        {
            switch (action)
            {
                // Update total voltage
                case "total_voltage":
                    {
                        oBatteryInstance.updateTotalVoltage(value);
                        break;
                    }

                // Update total current
                case "total_current":
                    {
                        oBatteryInstance.updateTotalCurrent(value);
                        break;
                    }
                // Update SOC
                case "soc":
                    {
                        oBatteryInstance.updateSoc(value);
                        break;
                    }
                // Update SOH
                case "soh":
                    {
                        oBatteryInstance.updateSoh(value);
                        break;
                    }
                // Update Min temperature Fahrenheit
                case "mintemperaturefah":
                    {
                        oBatteryInstance.updateMinTemperatureFah(value);
                        break;
                    }

                // Update Average Temperature Fahrenheit
                case "avgtemperaturefah":
                    {
                        oBatteryInstance.updateAvgTemperatureFah(value);
                        break;
                    }

                // Update Max Temperature Fahrenheit
                case "maxtemperaturefah":
                    {
                        oBatteryInstance.updateMaxTemperatureFah(value);
                        break;
                    }

                // Update Min temperature
                case "mintemperature":
                    {
                        oBatteryInstance.updateMinTemperature(value);
                        break;
                    }

                // Update Average Temperature
                case "avgtemperature":
                    {
                        oBatteryInstance.updateAvgTemperature(value);
                        break;
                    }

                // Update Max Temperature
                case "maxtemperature":
                    {
                        oBatteryInstance.updateMaxTemperature(value);
                        break;
                    }

                // Update Operational State
                case "operational_state":
                    {
                        oBatteryInstance.updateOperationalState(value);
                        break;
                    }

                // Update Alarms
                case "fault1":
                    {
                        oBatteryInstance.updateAlarm("errorbit1", value);
                        break;
                    }
                case "fault2":
                    {
                        oBatteryInstance.updateAlarm("errorbit2", value);
                        break;
                    }
                case "fault3":
                    {
                        oBatteryInstance.updateAlarm("errorbit3", value);
                        break;
                    }
                case "charge_limit":
                    {
                        oBatteryInstance.updateChargeLimit(value);
                        break;
                    }
                case "discharge_limit":
                    {
                        oBatteryInstance.updateDischargeLimit(value);
                        break;
                    }
                default:
                    break;
            }
        }

        // Obtain the modbus register value providing communication is good
        public static JObject IHI_OBTAIN_MODBUS_REGISTER(string RegisterType, string RegisterAddress)
        {
            try
            {
                // Determine if the IHI Battery Connector and IHI Battery simulator
                // are communicating
                if (strChannelState == "CONNECTED")
                {
                    string strFoundRegister = "FALSE";
                    dynamic ihi_register = new JObject();

                    if (RegisterType == "holding")
                    {
                        // Obtain the Modbus input collection from the appConfig
                        var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                        // Cycle through the Holding Modbus Collection
                        foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                        {
                            // Determine if matched register address/label
                            if (RegisterAddress == modbusObject.index.ToString()
                                                 ||
                                RegisterAddress == modbusObject.label)
                            {
                                // Match Found, determine offset
                                int i32uModbusOffset = modbusObject.index;
                                int index = auHoldingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);

                                // Create a new register JSON object and populate
                                // register fields
                                ihi_register.label = modbusObject.label;
                                ihi_register.type = modbusObject.type;
                                ihi_register.address = modbusObject.index;
                                ihi_register.value = (short)auHoldingRegisters[index].usValue;
                                ihi_register.date = DateTime.Now;
                                oIhiBatteryRegisterValue = ihi_register;
                                strFoundRegister = "TRUE";
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (RegisterType == "input")
                        {
                            // Obtain the Modbus Collection from the appConfig
                            var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

                            // Cycle through the Modbus Collection
                            foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
                            {
                                // Determine if matched register address
                                if (RegisterAddress == modbusObject.index.ToString()
                                                    ||
                                    RegisterAddress == modbusObject.label)

                                {
                                    // Determine modbus Offset
                                    int i32uModbusOffset = modbusObject.index;
                                    int index = auIncommingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);
                                    ihi_register.label = modbusObject.label;
                                    ihi_register.type = modbusObject.type;
                                    ihi_register.address = modbusObject.index;
                                    ihi_register.value = (short)auIncommingRegisters[index].usValue;
                                    ihi_register.date = DateTime.Now;
                                    oIhiBatteryRegisterValue = ihi_register;
                                    strFoundRegister = "TRUE";
                                    break;
                                }
                            }
                        }
                    }
                    // Determine if register was found in list
                    if (strFoundRegister == "NOTFOUND")
                    {
                        ihi_register.address = RegisterAddress;
                        ihi_register.value = "NOT FOUND";
                        oIhiBatteryRegisterValue = ihi_register;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                strChannelState = "NOT CONNECTED";
                server.Stop();
            }
            // Return register Value
            return oIhiBatteryRegisterValue;
        }

        // Update the heartbeat of the IHI Battery
        private ushort IHI_UPDATE_HEARTBEAT(string strHeartbeat)
        {
            return oBatteryInstance.updateHeartbeat(strHeartbeat);
        }

        // Obtain the modbus register value providing communication is good
        public JObject IHI_OBTAIN_MODBUS_HOLDING_REGISTER(string RegisterAddress)
        {
            try
            {
                // Determine if the IHI Battery Connector and IHI Battery simulator
                // are communicating
                if (strChannelState == "CONNECTED")
                {
                    string strFoundRegister = "FALSE";
                    dynamic ihi_register = new JObject();

                    // Obtain the Modbus input collection from the appConfig
                    var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                    // Cycle through the Holding Modbus Collection
                    foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                    {
                        // Determine if matched register address/label
                        if (RegisterAddress == modbusObject.index.ToString()
                                                 ||
                                RegisterAddress == modbusObject.label)
                        {
                            // Match Found, determine offset
                            int i32uModbusOffset = modbusObject.index;
                            int index = auHoldingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);

                            // Create a new register JSON object and populate
                            // register fields
                            ihi_register.value = (short)auHoldingRegisters[index].usValue;
                            oIhiBatteryRegisterValue = ihi_register;
                            strFoundRegister = "TRUE";
                            break;
                        }
                    }

                    // Determine if register was found in list
                    if (strFoundRegister == "NOTFOUND")
                    {
                        ihi_register.address = RegisterAddress;
                        ihi_register.value = "NOT FOUND";
                        oIhiBatteryRegisterValue = ihi_register;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                strChannelState = "NOT CONNECTED";
                server.Stop();
            }
            // Return register Value
            return oIhiBatteryRegisterValue;
        }

        // Assign the modbus holding registers
        private void IHI_READ_AND_SET_HOLDING_REGISTERS()
        {
            try
            {
                // Reset old register list
                Current_RegisterHoldingList.Clear();

                // Set slave Id to be 1
                ModbusSlave[0].UnitId = 1;

                // Obtain the Modbus input collection from the appConfig
                var modbusHoldingObjectCollection = AutoConfig.Map<IIhiModbusHoldingObjectCollection>();

                // Cycle through the Holding Modbus Collection
                foreach (IihiModbusObject modbusObject in modbusHoldingObjectCollection.ihiModbusObjects)
                {
                    // Determine if matched slave
                    int i32uModbusOffset = modbusObject.index;

                    ushort usValue = 0;
                    IHI_READ_WRITE_MODBUS_REGISTER(i32uModbusOffset, "READ", 0, ref usValue);
                    short sValue = (short)usValue;
                    int index = auHoldingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);

                    // Do not update if user has an update in progress
                    if (auHoldingRegisters[index].bChanged == false)
                    {
                        auHoldingRegisters[index].usValue = usValue;
                    }

                    // Update the holding register
                    Ihi_UPDATE_MODBUS_HOLDING_REGISTER_TABLE(modbusObject.type, modbusObject.index.ToString(), modbusObject.slaveid, sValue.ToString(), modbusObject.label, modbusObject.mandatory);
                }

                // Populate the registers list with the holding registers received from
                // postman/ IHI ES Test Harness
                oIHI_Registerlists["holding_registers"] = JToken.FromObject(Current_RegisterHoldingList);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                strChannelState = "NOT CONNECTED";
            }
        }

        // Kick off the timer thread every 2 seconds
        // This will update any Holding/Input register changes 
        // 
        private void IHI_START_MODBUS_PROCESSING()
        {
            // Wait for a client connection
            m_timerSequenceNumber = new System.Timers.Timer();
            m_timerSequenceNumber.Interval = 1000;
            m_timerSequenceNumber.AutoReset = true;
            m_timerSequenceNumber.Elapsed += IHI_ONTIMEREVENT_UPDATE_TIMER;
            m_timerSequenceNumber.Enabled = true;
        }

        // Change the state of the connection
        private void IHI_ACCEPT_CLIENT(IAsyncResult asyncResult)
        {
            if (server.Server.IsBound)
            {
                // A modbus client has connected to this server application

                Ihi_battery_modbus_class oModClass = (Ihi_battery_modbus_class)asyncResult.AsyncState;

                // Add a 2 second delay before commencing processing of read/writes
                int milliseconds = 2000;
                Thread.Sleep(milliseconds);

                // Update initial configuratuon values
                oBatteryInstance.initialBatteryConfiguration(oModClass);

                strChannelState = "CONNECTED";
                System.Diagnostics.Trace.WriteLine("CONNECTED CLIENT!!!!!");

                // Commence Modbus Processing
                oModClass.IHI_START_MODBUS_PROCESSING();
            }
        }

        // Assign the modbus input registers
        private void IHI_SET_INPUT_REGISTERS()
        {
            ushort usSlaveIndex = 0;

            //  Obtain the slave id
            int iSlaveId = ModbusSlave[usSlaveIndex].UnitId;

            // Obtain the Modbus input collection from the appConfig
            var modbusInputObjectCollection = AutoConfig.Map<IIhiModbusInputObjectCollection>();

            // Reset the ihi registers
            Current_RegisterInputList.Clear();

            // Cycle through the modbus object list
            foreach (IihiModbusObject modbusObject in modbusInputObjectCollection.ihiModbusObjects)
            {
                // Obtain the modbus offset
                int i32uModbusOffset = modbusObject.index;

                // Obtain Modbus Register Value at Offset
                ushort usValue = 0;
                IHI_READ_WRITE_MODBUS_REGISTER(i32uModbusOffset, "READ", 0, ref usValue);
                short sValue = (short)usValue;

                int index = auIncommingRegisters.FindIndex(item => item.iOffset == i32uModbusOffset);
                auIncommingRegisters[index].usValue = usValue;

                // Create a new register JSON object and populate
                dynamic ihi_battery_register = new JObject();

                ihi_battery_register.label = modbusObject.label;
                ihi_battery_register.type = modbusObject.type;
                ihi_battery_register.address = modbusObject.index;
                ihi_battery_register.value = sValue;
                ihi_battery_register.mandatory = modbusObject.mandatory;
                ihi_battery_register.date = DateTime.Now;

                // Save register to list
                Current_RegisterInputList.Add(ihi_battery_register);

                // Populate input register list
                oIHI_Registerlists["input_registers"] = JToken.FromObject(Current_RegisterInputList);
            }
        }

        // All the holding registers in the modbus register map
        public void Ihi_UPDATE_MODBUS_HOLDING_REGISTER_TABLE(String type, string usIndex, String slaveid, String value, String label, String mandatory)
        {
            try
            {
                // Update Modbus Holding Register
                dynamic ihi_batttery_register = new JObject();
                ihi_batttery_register.label = label;
                ihi_batttery_register.type = type;
                ihi_batttery_register.address = usIndex;
                ihi_batttery_register.value = value.ToString();
                ihi_batttery_register.mandatory = mandatory;
                ihi_batttery_register.date = DateTime.Now;

                // Add the Battery register to the list of registers
                Current_RegisterHoldingList.Add(ihi_batttery_register);
            }
            catch (SystemException error)
            {
                System.Diagnostics.Trace.WriteLine(error);
            }
        }

        // Update any write requests
        public void IHI_UPDATE_MODBUS_WRITE_REQUESTS()
        {
            for (int i = 0; i < auHoldingRegisters.Count; i++)
            {
                if (auHoldingRegisters[i].bChanged == true)
                {
                    ushort empty = 0;
                    IHI_READ_WRITE_MODBUS_REGISTER(auHoldingRegisters[i].iOffset, "UPDATE", auHoldingRegisters[i].usValue, ref empty);

                    // After updating register mark change state to false
                    auHoldingRegisters[i].bChanged = false;
                }
            }
        }

        // Called periodically to update incomming/holding registers
        public void IHI_ONTIMEREVENT_UPDATE_TIMER(Object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                // Check if modbus port connected
                if (server.Server.IsBound)
                {
                    // Determine if heartbeat changed
                    if (usHeartbeat == usLastheartbeat && strHeartBeatCheckingCtrl == "ON")
                    {

                        // Expires after 10 seconds. configurable via Communication controller REST API PUT
                        if (i32uTimeOutCounter > usModbusCommunicationTimeout)
                        {
                            System.Diagnostics.Trace.WriteLine("NOT CONNECTED Last Known heartbeat=" + usLastheartbeat + " Timeout Counter=" + i32uTimeOutCounter + " usModbudCommunicationTimeout=" + usModbusCommunicationTimeout + "usHearbeat=" + usHeartbeat);
                            // Lost Communication
                            i32uTimeOutCounter = 0;
                            strChannelState = "NOT CONNECTED";

                            oBatterySystemObject.communication = strChannelState;
                        }
                        else
                        {
                            // Increment the timer by 1
                            i32uTimeOutCounter++;
                        }
                    }
                    else
                    {
                        oBatterySystemObject.communication = "CONNECTED";

                        // Reset the counter
                        i32uTimeOutCounter = 0;

                        // Save heartbeat value
                        usLastheartbeat = usHeartbeat;

                        // Update Modbus Holding registers
                        IHI_READ_AND_SET_HOLDING_REGISTERS();

                        // Update Modbus Input registers
                        IHI_SET_INPUT_REGISTERS();

                        // If autonomous mode then the simulator is going 
                        // to populate registers if the battery if connected
                        if (strAutonomous == "TRUE")
                        {
                            // Check for changes from the battery connector
                            IHI_CHECK_AUTONOMOUS_REGISTERS();
                        }
                    }
                }
                else
                {
                    strChannelState = "NOT CONNECTED";
                    oBatterySystemObject.communication = strChannelState;
                }
            }
            catch (SystemException error)
            {
                System.Diagnostics.Trace.WriteLine(error.Message);
            }
        }
    }
}