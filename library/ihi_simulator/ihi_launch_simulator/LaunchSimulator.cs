﻿using System.Diagnostics;
using System.Configuration;
using System;

namespace ihi_launch_simulator
{
    public class LaunchSimulator
    {
        public static string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
        public static ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
        public static void RunSimulator(string simulatorPjtPath, string simulatorPort)
        {
            Process p = new Process();
            p.StartInfo.FileName = ConfigurationManager.AppSettings["CMDPath"].ToString();
            p.StartInfo.WorkingDirectory = pjtParent + ConfigurationManager.AppSettings["IISExpress"].ToString();
            p.StartInfo.Arguments = @"/K iisexpress /path:" + pjtParent + simulatorPjtPath + " /port:" + simulatorPort;
            p.Start();
            System.Threading.Thread.Sleep(3000);
            webDriver.SetBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, ConfigurationManager.AppSettings["APIHostname"].ToString() + ":" + simulatorPort);
            System.Threading.Thread.Sleep(10000);
            webDriver.browserDriver.Quit();
        }
        public static void RunDomainControllerSimulator(string simulatorPjtPath, string simulatorPort)
        {
            Process p = new Process();
            p.StartInfo.FileName = ConfigurationManager.AppSettings["CMDPath"].ToString();
            p.StartInfo.WorkingDirectory = pjtParent + ConfigurationManager.AppSettings["IISExpress"].ToString();
            p.StartInfo.Arguments = @"/K iisexpress /path:" + pjtParent + simulatorPjtPath + " /port:" + simulatorPort;
            p.Start();
            System.Threading.Thread.Sleep(10000);
        }

        public static void CleanIIS()
        {
            Array.ForEach(Process.GetProcessesByName("iisexpress"), x => x.Kill());
            Array.ForEach(Process.GetProcessesByName("cmd"), x => x.Kill());
            Array.ForEach(Process.GetProcessesByName("chrome"), x => x.Kill());
            Array.ForEach(Process.GetProcessesByName("chromedriver"), x => x.Kill());
        }

        public static void DomainControllerCleanUp()
        {
            Array.ForEach(Process.GetProcessesByName("iisexpress"), x => x.Kill());
            Array.ForEach(Process.GetProcessesByName("cmd"), x => x.Kill());
        }
    }
}
