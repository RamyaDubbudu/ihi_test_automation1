﻿using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_testlib_modbusClientWinApp
{
    public class ModbusFunctions: ModbusSession
    {       
        #region EasyModbusXpaths
        //"/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@Name=\"EasyModbus Client\"][@AutomationId=\"MainForm\"]/Button[@Name=\"connect\"][starts-with(@AutomationId,\"button\")]";
        public string ModbusConnectButton = "/Window[@Name=\"EasyModbus Client\"][@AutomationId=\"MainForm\"]/Button[@Name=\"connect\"]";
        public string ModbusDisconnectButon = "/Window[@Name=\"EasyModbus Client\"][@AutomationId=\"MainForm\"]/Button[@Name=\"disconnect\"][starts-with(@AutomationId,\"button\")]";
        public string StartAddressInput = "/Window[@Name=\"EasyModbus Client\"][@AutomationId=\"MainForm\"]/Edit[@AutomationId=\"txtStartingAddressInput\"]";
        public string ReadHoldingRegisterButton = "/Window[@Name=\"EasyModbus Client\"][@AutomationId=\"MainForm\"]/Button[@Name=\"Read Holding Registers - FC3\"][@AutomationId=\"btnReadHoldingRegisters\"]";
        public string ModbusOutputTextBox = "/Window[@Name=\"EasyModbus Client\"][@AutomationId=\"MainForm\"]/Edit[@Name=\"Slave ID\"][starts-with(@AutomationId,\"textBox\")]";

        #endregion
        public string ReadOutputFromModbus (WindowsDriver<WindowsElement> modbusSession, string registerAddress)
        {
            string output = string.Empty;
            FindElementByAbsoluteXPath(modbusSession, StartAddressInput).Clear();
            FindElementByAbsoluteXPath(modbusSession,StartAddressInput).SendKeys(registerAddress);
            FindElementByAbsoluteXPath(modbusSession, ReadHoldingRegisterButton).Click();
            output = FindElementByAbsoluteXPath(modbusSession, ModbusOutputTextBox).Text;
            return output;
        }
        public WindowsElement FindElementByAbsoluteXPath(WindowsDriver<WindowsElement> modbusSession, string xPath, int nTryCount = 15)
        {
            WindowsElement uiTarget = null;
            while (nTryCount-- > 0)
            {
                try
                {
                    uiTarget = modbusSession.FindElementByXPath(xPath);
                }
                catch
                {
                }
                if (uiTarget != null)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(2000);
                }
            }
            return uiTarget;
        }

    }
}
