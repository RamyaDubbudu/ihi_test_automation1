﻿using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Configuration;

namespace ihi_testlib_modbusClientWinApp
{
    public class ModbusSession
    {
        private string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";// ConfigurationManager.AppSettings["WindowsApplicationDriverUrl"].ToString();
        private string ModbusClientExePath = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["ModbusClientExePath"].ToString();

        // protected static WindowsDriver<WindowsElement> modbusSession;
        protected static WindowsElement editBox;

        public WindowsDriver<WindowsElement> LaunchModbusSession(WindowsDriver<WindowsElement> modbusSession)
        {
            // Launch a new instance of PowerLife application
            DesiredCapabilities appCapabilities = new DesiredCapabilities();
            appCapabilities.SetCapability("app", ModbusClientExePath);
            modbusSession = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities, TimeSpan.FromMinutes(10));
            return modbusSession;
        }

        public void CloseModbusSession(WindowsDriver<WindowsElement> modbusSession)
        {
            // Close the application and delete the session
            if (modbusSession != null)
            {
                modbusSession.Close();
                modbusSession.Quit();
                modbusSession = null;
            }
        }

    }
}