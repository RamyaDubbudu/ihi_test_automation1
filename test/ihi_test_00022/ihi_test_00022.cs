using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00022
{
    [TestFixture]
    public class ihi_test_00022 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-22")]
        public void TEST_00022__Read_ESS_Q_ACTUAL_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00022__rundata.xlsx", 0);
            runData.RunRow = 0;
            short ess_Q_Actual = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_Ess_Q_ACTUAL = runData.RunDataValue("ESS_Q_ACTUAL");
                    ushort registerAddress = 13;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("q_actual", runData_Ess_Q_ACTUAL);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ess_Q_Actual = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_Ess_Q_ACTUAL == ess_Q_Actual.ToString())
                        {
                            Assert.IsTrue(runData_Ess_Q_ACTUAL == ess_Q_Actual.ToString(), "response Modbus message register value " + ess_Q_Actual + " is equal to the Run ID" + runData_Ess_Q_ACTUAL + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetQActual");
                            StringAssert.Contains(runData_Ess_Q_ACTUAL.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Actual ESS_Q_ACTUAL-" + ess_Q_Actual + "response Modbus message register value is equal to the Run ID" + runData_Ess_Q_ACTUAL + "column value");

                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "  Actual ESS_Q_ACTUAL-" + ess_Q_Actual + "response Modbus message register value is not equal to the Run ID" + runData_Ess_Q_ACTUAL + "column value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "  Actual ESS_Q_ACTUAL-" + ess_Q_Actual + "response Modbus message register value is not equal to the Run ID" + runData_Ess_Q_ACTUAL + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runData_Ess_Q_ACTUAL != ess_Q_Actual.ToString())
                        {
                            Assert.IsTrue(runData_Ess_Q_ACTUAL != ess_Q_Actual.ToString(), "Run ID ESS_Q_ACTUAL invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Ess_Q_ACTUAL + " is invalid, so does not equal to Run Value");

                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Ess_Q_ACTUAL + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Ess_Q_ACTUAL + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }


    }
}