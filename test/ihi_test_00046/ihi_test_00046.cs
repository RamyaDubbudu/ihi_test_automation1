using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00046
{
    [TestFixture]
    public class ihi_test_00046 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-46")]
        public void TEST_00046__Write_ISO_DEMAND_P_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00046__rundata.xlsx", 0);
            runData.RunRow = 0;
            short iSO_Demand_P = 500;
            short p_min = -32768;
            short p_max = 32767;
            ConsumeDCSimRest.UpdateSiteData("p_min", p_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("p_max", p_max.ToString());
            ushort registerAddress = 1;
            //Send a Modbus request to the ISO slave to write the "ISO_DEMAND_P" holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iSO_Demand_P));
            //Send a Modbus request to the ISO slave to read the "ISO_DEMAND_P" holding register
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            iSO_Demand_P = (Int16)values[0];
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {

                        var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                        short runData_Iso_DemandP = short.Parse(runData.RunDataValue("ISO_DEMAND_P"));
                        string RID_p_min = runData.RunDataValue("ESS_PMIN");
                        string RID_p_max = runData.RunDataValue("ESS_PMAX");
                        //Publish ESS_PMIN and ESS_PMAX data from DC Sim for the Run ID	"ESS_PMIN = Run IDESS_PMINESS_PMAX =Run ID ESS_PMAX"
                        ConsumeDCSimRest.UpdateSiteData("p_min", RID_p_min);
                        ConsumeDCSimRest.UpdateSiteData("p_max", RID_p_max);
                        //Send a Modbus to the ISO server to read the input registers (ESS_PMAX, ESS_PMIN)
                        values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, 4, 2);
                        p_max = (Int16)values[0];
                        p_min = (Int16)values[1];
                        //Ensure response Modbus message has Run ID ESS_PMIN and ESS_PMAX column values
                        Assert.IsTrue(RID_p_min == p_min.ToString(), "response Modbus message register value " + p_min + " is equal to the Run Value" + RID_p_min + " column value");
                        Assert.IsTrue(RID_p_max == p_max.ToString(), "response Modbus message register value p_max " + p_max + " is equal to the Run Value RID_p_max " + RID_p_max + " row " + runData.RunRow + " column value");
                        //Send a Modbus request to the ISO slave to write the "ISO_DEMAND_P" holding register            
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, (UInt16)runData_Iso_DemandP);
                        //Send a Modbus request to the ISO slave to read the "ISO_DEMAND_P" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iSO_Demand_P = (Int16)values[0];
                        if (iSO_Demand_P.ToString() == runData.RunDataValue("ISO_DEMAND_P_EXPECTED"))
                        {
                            Assert.IsTrue(iSO_Demand_P.ToString() == runData.RunDataValue("ISO_DEMAND_P_EXPECTED"), "response Modbus message register value " + iSO_Demand_P + " is equal to the Run Value" + runData.RunDataValue("ISO_DEMAND_P_EXPECTED") + " row " + runData.RunRow + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetPDemand");
                            StringAssert.Contains(iSO_Demand_P.ToString(), RestGetresult);
                            _test.Pass("response Modbus message register value " + iSO_Demand_P + " is equal to the Run Value" + runData_Iso_DemandP + " column value");

                        }
                        else
                        {
                            _test.Fail("response Modbus message register value " + iSO_Demand_P + " is not equal to the Run Value" + runData_Iso_DemandP + " column value");
                            throw new Exception("response Modbus message register value " + iSO_Demand_P + " is not equal to the Run Value" + runData_Iso_DemandP + " column value");
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }


    }
}