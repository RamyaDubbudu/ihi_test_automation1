using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00009
{
    [TestFixture]
    public class ihi_test_00009 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
       
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-9")]
        public void TEST_00009__Read_ESS_KVAR_NAMEPLATE_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00009__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort KVAR_Nameplte = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ESS_KVAR_Namplate = runData.RunDataValue("ESS_KVAR_NAMEPLATE");
                    ushort registerAddress = 3;

                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("kvar_nameplate", runData_ESS_KVAR_Namplate);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    KVAR_Nameplte = (UInt16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (runData_ESS_KVAR_Namplate == KVAR_Nameplte.ToString()) //Valid Scenarios
                    {
                        Assert.IsTrue(runData_ESS_KVAR_Namplate == KVAR_Nameplte.ToString(), "response Modbus message register value " + KVAR_Nameplte + " is equal to the Run ID" + runData_ESS_KVAR_Namplate + "column value");
                        string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetKVARNameplate");
                        StringAssert.Contains(runData_ESS_KVAR_Namplate.ToString(), RestGetresult);
                        string getSiteinfo = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetTable");
                        _test.Pass("RunID-" + runData.RunRow + " Expected ESS_KVAR_NAMEPLATE-" + runData_ESS_KVAR_Namplate + " Actual KVAR_Nameplte-" + KVAR_Nameplte + "response Modbus message register value is equal to the Run ID" + runData_ESS_KVAR_Namplate + "column value" + getSiteinfo);
                    }
                    else
                    {
                        _test.Fail("RunID-" + runData.RunRow + " Expected ESS_KVAR_NAMEPLATE-" + runData_ESS_KVAR_Namplate + " Actual KVAR_Nameplte-" + KVAR_Nameplte + "response Modbus message register value is not equal to the Run ID" + runData_ESS_KVAR_Namplate + "column value");
                        throw new Exception("RunID-" + runData.RunRow + " Expected ESS_KVAR_NAMEPLATE-" + runData_ESS_KVAR_Namplate + " Actual KVAR_Nameplte-" + KVAR_Nameplte + "response Modbus message register value is not equal to the Run ID" + runData_ESS_KVAR_Namplate + "column value");
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}