using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;
using System.Collections.Generic;

namespace ihi_test_00189
{
    [TestFixture]
    public class ihi_test_00189 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Inverter_Alarms"), Property("Test", "TEST-189")]
        public void TEST_00189_Trigger_InverterAlarm()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            //Login to ESPilot 
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00189__rundata.xlsx", 0);
            runData.RunRow = 0;
            string registerValue = "";
            string clearAlarm = "";
            int waitTimetoset = 0;
            int waitTimetoclear = 0;
            SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
            LoginPage.loginvalid(webDriver);
            webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
            ConsumeDCSimRest.UpdateInverterDeviceType("voltage_dc", "0");
            if ("Shutdown" == DashboardPage.GetCurrenteMode(1000, webDriver))
            {
                // Function will check the status and change mode to Manual mode
                DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
            }
            else if ("Online" == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalDashbaordInverterDetails.InverterStatus)))
            {
                webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
            }
            for (int i = runData.RunRow; i < 44; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        ConsumeDCSimRest.UpdateInverterDeviceType("voltage_dc", "0");
                        registerValue = runData.RunDataValue("Value");
                        clearAlarm = runData.RunDataValue("ClearAlarm");
                        waitTimetoset = Convert.ToInt32(runData.RunDataValue("Waittimetoshow"));
                        waitTimetoclear = Convert.ToInt32(runData.RunDataValue("Waittimetoclear"));
                        ConsumeDCSimRest.UpdateInverterAlarms(registerValue);
                        // ConsumeDCSimRest.UpdateInverterAlarms(clearAlarm);
                        System.Threading.Thread.Sleep(waitTimetoset);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                        List<AlarmsPage.AlarmsInfo> alarmInfo = AlarmsPage.GetAllAlarmInfo(webDriver);
                        string alarmDescription = alarmInfo[0].AlarmDescription;
                        if (alarmInfo.Count == 0)
                        {
                            DashboardPage.ChangeModeAs(webDriver, "Shutdown");
                            DashboardPage.ChangeMode(webDriver, "Manual Mode");
                            ConsumeDCSimRest.UpdateInverterAlarms(registerValue);
                            System.Threading.Thread.Sleep(30000);
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                            webDriver.WaitForElement(xpath: string.Format(GlobalElementsMainPage.AlarmsTab), numberOfTimes: waitTime);
                            alarmInfo = AlarmsPage.GetAllAlarmInfo(webDriver);
                        }
                        if (alarmInfo.Count != 0)
                        {
                            if (alarmInfo[0].AlarmDescription.Contains(runData.RunDataValue("Description").ToString()))
                            {
                                _test.Pass("Alarm is shown in the Alarms Page with the following description:" + alarmInfo[0] + " Matching to the Rest setting alarm as:  " + runData.RunDataValue("Description").ToString(), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            }
                            else
                            {
                                _test.Fail("Alarm is not shown in the Alarms Page with the following description: " + alarmInfo[0] + " Matching to the Rest setting alarm as: " + runData.RunDataValue("Description").ToString(), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                                // throw new Exception("Alarm is shown in the Alarms Page with the following description: " + runData.RunDataValue("Description").ToString());
                            }
                        }
                        else
                        {
                            _test.Fail("Alarm is not shown in the Alarms Page as: " + runData.RunDataValue("Description").ToString(), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        //                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                        if (AlarmsPage.AreAllalarmsAcknowledged(webDriver) == false)
                        {
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.AcknowledgeAllButton));
                        }
                        ConsumeDCSimRest.UpdateInverterAlarms(clearAlarm);
                        List<AlarmsPage.AlarmsInfo> alarmInfo = AlarmsPage.GetAllAlarmInfo(webDriver);
                        System.Threading.Thread.Sleep(waitTimetoclear);
                        _test.Info("Alarm is been Cleared:" + runData.RunDataValue("Description").ToString(), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.DismissAllButton));
                        System.Threading.Thread.Sleep(3000);
                        runData.RunRow++;
                    }
                }
            }
            SeleniumWrapper.DriverCleanup(webDriver);
            //video.StopRecording();
        }

    }
}