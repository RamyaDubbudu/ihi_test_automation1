using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;
using System.Configuration;

namespace ihi_test_00026
{
    [TestFixture]
    public class ihi_test_00026 : TestInitialize
    {

        #region Variables 
        SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ZMQCommunicationTests"), Property("Test", "TEST-26")]
        public void TEST_00026__ZMQSite_infoMessageTypes()
        {
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            // SSH.SearchLogFilesForString("Received 'Get Site Information (600)' publication from the DC: JSON", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            ushort iso_sequence = 500;
            ushort registerAddress = 0;
            //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
            System.Threading.Thread.Sleep(2000);
            //Verify ISO Server issued ZMQ Reconnect request
            string logFile = SSH.SearchLogFilesForString("Log File Opened", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Log File Opened", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            //ZMQ Reconnect request issued from ISO Server
            logFile = SSH.SearchLogFilesForString("Command-Line: -D", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString(), logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            logFile = SSH.SearchLogFilesForString("Command-Line: -l", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Command-Line: -l ['Logging Level'] = 0. (File:"))
            {
                _test.Pass("ISO Server wrote a log entry containing the logging level. Details shown in the log file as: " + logFile);
            }
            logFile = SSH.SearchLogFilesForString(".ZeroMQ .ZMQ. Requester Socket. Opened on", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("'ZeroMQ (ZMQ) Requester Socket' Opened on (" + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + ":1039).", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            int siteId = Convert.ToInt32(ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetId"));
            int siteUTime = Convert.ToInt32(ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetUTime"));
            string siteUrl = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetURL");
            string siteName = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetName");
            string siteDesc = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetDesc");
            double lat = Convert.ToDouble(ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetLat"));
           // string lon = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetLon");
            logFile = SSH.SearchLogFilesForString("Received .Get Site Information .600.. publication from the DC: JSON  ..", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            string eee = "Received 'Get Site Information (600)' publication from the DC: JSON  {\"get_site_data_response\": { \"site_table\":[{\"rmon_site_id\":" + siteId + ",\"site_utime\":" + siteUTime + ",\"url\":" + siteUrl + ",\"name\":" + siteName + ",\"description\":" + siteDesc + ",\"lat\":" + lat + ",\"lon\" (File: ihi_zmq_iso.c [Line: 631])";
            StringAssert.Contains("Received 'Get Site Information (600)' publication from the DC: JSON  {\"get_site_data_response\": { \"site_table\":[{\"rmon_site_id\":" + siteId + ",\"site_utime\":" + siteUTime + ",\"url\":" + siteUrl + ",\"name\":" + siteName + ",\"description\":" + siteDesc + ",\"lat\":" + lat + ",\"lon\" (File: ihi_zmq_iso.c [Line: 631])", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            if (logFile.Contains("Received 'Get Site Information (600)' publication from the DC: JSON  {\"get_site_data_response\": { \"site_table\":[{\"rmon_site_id\":" + siteId + ",\"site_utime\":" + siteUTime + ",\"url\":" + siteUrl + ",\"name\":" + siteName + ",\"description\":" + siteDesc + ",\"lat\":" + lat + ",\"lon\" (File: ihi_zmq_iso.c [Line: 631])"))
            {
                _test.Pass("ISO Server wrote a log entry containing formatted Site_info message value. Details shown in the log file as Received 'Get Site Information (600)' publication from the DC: JSON  {\"get_site_data_response\": { \"site_table\":[{\"rmon_site_id\":" + siteId + ",\"site_utime\":" + siteUTime + ",\"url\":" + siteUrl + ",\"name\":" + siteName + ",\"description\":" + siteDesc + ",\"lat\":" + lat + ",\"lon\"");
            }
            else
            {
                _test.Fail("ISO Server Does not have a log entry containing the logging level and formatted Site_info message value");
                throw new Exception("ISO Server Does not have a log entry containing the logging level and formatted Site_info message value");
            }
            
        }
    }
}