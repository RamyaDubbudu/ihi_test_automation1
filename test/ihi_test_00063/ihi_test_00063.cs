﻿using System;
using ihi_test_00063.Initialize;
using NUnit.Framework;
using AventStack.ExtentReports;
using ihi_testlib_espilot;
using System.Configuration;
using ihi_testlib_videorecorder;

namespace ihi_test_00063
{
    [TestFixture]
    public class test_00063 : TestInitialize
    {
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        [Test, Category("LoginScenarios")]
        [Property("Requirement", ""), Property("Test", "TEST-63")]
        [Video(Name = "ihi_test_00063", Mode = SaveMe.Always)]
        public void TEST_00063__LoginSuccess()
        {

            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\test_00063__rundata.xlsx", 0);
            //skip test if runTest is false;
            for (int k = 0; k < runData.RowCount; k++)
            {
                if (runData.MetaData[runData.RunRow].runTest == "False")
                {
                    _test.Pass(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    string shownname = "";
                    try
                    {
                        //SSH.KillDecs(sshInfo);
                        //SSH.StartDecs(DECSLaunchType.Test, sshInfo, 10000);
                        var username = runData.RunDataValue("username");
                        var password = runData.RunDataValue("password");
                        var expectedname = runData.RunDataValue("expected result");

                        // string needs space at end
                        expectedname = expectedname + " ";
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        LoginPage.login(username, password, webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsMainPage.LoginName));
                        if (webDriver.IsElementVisible(xPath: string.Format(GlobalElementsMainPage.LoginName)))
                        {
                            shownname = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.LoginName));
                        }

                        shownname = shownname.Replace(" ", string.Empty);
                        expectedname = expectedname.Replace(" ", string.Empty);
                        _test.Pass("Login success and Username is shown in the main page:", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        if (expectedname != shownname)
                        {
                            _test.Fail("Login name Assertion Failed", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            //Test Failed
                            Console.WriteLine("Test Failed");
                            //Is a Run test
                            Console.WriteLine(
                                    "Login success fail Shown name: {0}. Run Info: username: {1}, password: {2}, expectedname: {3}\n",
                                    shownname, username, password, expectedname);
                        }
                    }
                    catch (Exception ex)
                    {
                        //Test sends fail and records fail chain
                        // throw new Exception("Error at Step: " + "\r\n" + ex.Message);
                        _test.Fail(ex);
                        _test.Fail("Login name Assertion Failed", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        throw;
                    }
                    finally
                    {
                        //reportText = runData.FormatRunData(!result) + reportText;
                        //increment for next run
                        runData.RunRow++;
                        MainPage.logout(webDriver);
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }
            }
        }
    }
}
