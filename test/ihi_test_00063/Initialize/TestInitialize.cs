﻿using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Reflection;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework.Interfaces;
using ihi_testlib_selenium;
using OpenQA.Selenium;
using System.Diagnostics;
using System.IO;
using System.Configuration;

namespace ihi_test_00063.Initialize
{
    [SetUpFixture]
    public abstract class TestInitialize
    {

        public static string GetPjtParent()
        {
            var filePath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory().ToString()).ToString()).ToString();
            return filePath;
        }

        public static ExtentReports _extent = new ExtentReports();
        //ScreenCaptureJob scj = new ScreenCaptureJob();
        protected ExtentTest _test;
        public string videoDir = TestContext.CurrentContext.TestDirectory + "\\VideoRecordings\\";

        [OneTimeSetUp]
        protected void Setup()
        {
            //_extent = new ExtentReports();
            string pjtParent = GetPjtParent();
            var reportDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString();
            Directory.CreateDirectory(reportDir);
            var fileName = GetType().Namespace + ".html";
            var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
            htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
            htmlReporter.AppendExisting = true;
            _extent.AttachReporter(htmlReporter);
        }

        [OneTimeTearDown]
        protected void TearDown()
        {
            _extent.Flush();
            var Processes = Process.GetProcesses().Where(pr => pr.ProcessName == "chromedriver" || pr.ProcessName == "IEDriverServer" || pr.ProcessName == "geckodriver");
            if (Processes.Count() != 0)
            {
                foreach (var proc in Processes)
                {
                    proc.Kill();

                }
            }
        }

        public static String TakesScreenshot(ihi_testlib_selenium.Selenium driver, string FileName)
        {
            string pjtParent = GetPjtParent();
            string path = pjtParent + "\\XReports\\TestOutputImages\\";
            System.IO.Directory.CreateDirectory(path);
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");
            TimeAndDate.Replace(" ", "_");

            string imageName = TestContext.CurrentContext.Test.Name + FileName + TimeAndDate.ToString();

            ((ITakesScreenshot)driver.browserDriver).GetScreenshot().SaveAsFile(path + "_" + imageName + "." + System.Drawing.Imaging.ImageFormat.Jpeg);

            return path + "_" + imageName + "." + "jpeg";
        }

        [SetUp]
        public void BeforeTest()
        {

            // Create a instance of ScreenCaptureJob
            //  scj = new ScreenCaptureJob();
            //string testMethodName = TestContext.CurrentContext.Test.Name;
            //// Specify the path & file name in which you want to save  
            //Assembly _assembly;
            //_assembly = Assembly.GetExecutingAssembly();
            //string path = videoDir + testMethodName + DateTime.Now.ToString("dd_MM_hh_mm_ss_fff") + "ScreenRecording.xesc";
            //scj.OutputScreenCaptureFileName = path;
            //// Start the Screen Capture Job
            //scj.Start();

            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
            _test.AssignCategory(this.GetType().ToString());
            _test.AssignAuthor("Ramya Dubbudu");
        }

        [TearDown]
        public void AfterTest()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }

            _test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
            //_test.Info("Video Recording Filepath: " + scj.OutputScreenCaptureFileName);
            _extent.Flush();
            //Stop the Screen Captureing
            //scj.Stop();
        }
    }
}
