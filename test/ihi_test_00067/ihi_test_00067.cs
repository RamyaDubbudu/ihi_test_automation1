using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using System.Configuration;
using Newtonsoft.Json.Linq;
using ihi_testlib_videorecorder;
namespace ihi_test_00067
{
    [TestFixture]
    public class ihi_test_00067 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        public class IHI_REGISTER
        {
            public string value { get; set; }
        }
        [Test, Category("Inverter_Command"), Property("Test", "TEST-67")]
        public void TEST_00067__Command_Inverter_PDemand()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00067__rundata.xlsx", 0);
            runData.RunRow = 0;
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string RID_P_Demand = runData.RunDataValue("P_DEMAND").ToString();
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(5000);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(5000, webDriver))
                        {
                            // Function will check the status and change mode to Manual mode
                            DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
                        }
                        else if ("Online" == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalDashbaordInverterDetails.InverterStatus)))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
                        }
                        //Set Run data value for P Demand in the Pilot dashboard page  
                        System.Threading.Thread.Sleep(5000);
                        webDriver.AddTextToTextbox(textToAdd: RID_P_Demand, XPath: string.Format(GlobalStartupShutdown.pField));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.Apply));
                        System.Threading.Thread.Sleep(10000);
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.pDemandText, RID_P_Demand), numberOfTimes: waitTime);
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.pActualText, RID_P_Demand), numberOfTimes: waitTime);
                        if (RID_P_Demand == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.pDemand)))
                        {
                            _test.Info(RID_P_Demand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        string getPDemand = ConsumeDCSimRest.InverterRestResponseByName("ihi", "Device/Inverter/real_power_demand");
                        var jsonObj = JObject.Parse(getPDemand);
                        getPDemand = (string)jsonObj["value"];
                        //Validate the P Actual value on dashboard
                        if (getPDemand == RID_P_Demand)
                        {
                            _test.Pass("P Demand value: " + RID_P_Demand + " sent thru the ESPilot Dashboard is equal to Rest API Get for PDemand : " + getPDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.InvertersTab));
                            webDriver.WaitForElement(xpath: string.Format(GlobalElementsControl.Inverters.InvertersPDemandText, getPDemand), numberOfTimes: waitTime);
                            getPDemand = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Inverters.InvertersPDemand));
                            if (getPDemand == RID_P_Demand)
                            {
                                _test.Pass("P Demand value: " + RID_P_Demand + " sent thru the ESPilot Control Tab > Inverter Tab is equal to Rest API Get for PDemand : " + getPDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                                Assert.AreEqual(getPDemand, RID_P_Demand);
                            }
                            else
                            {
                                _test.Fail("P Demand value in Inverter tab: " + RID_P_Demand + " is not equal to the registervalue in Rest API " + getPDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                                throw new Exception("P Demand value in Inverter tab: " + RID_P_Demand + " is not equal to the registervalue in Rest API " + getPDemand);
                            }
                        }
                        else
                        {
                            _test.Fail("P Demand value in dashboard page: " + RID_P_Demand + " is not equal to the registervalue in Rest API " + getPDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception("P Demand value in Inverter tab: " + RID_P_Demand + " is not equal to the registervalue in Rest API " + getPDemand);
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }
            }
            //video.StopRecording();
        }

    }
}
