﻿using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;

namespace ihi_test_00102
{
    [TestFixture]
    public class ihi_test_00102 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("BatteryVersion"), Property("Test", "TEST-102")]
        public void TEST_00102_Battery_BMSVerion()
        {
            var testName = GetType().Namespace;
            video.StartRecording(videoDir, SaveMe.Always);
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00102__rundata.xlsx", 0);
            runData.RunRow = 0;
            for (int i = runData.RunRow; i < 4; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string versionNumber1 = runData.RunDataValue("Actual_Version1");
                        string versionNumber2 = runData.RunDataValue("Actual_Version2");
                        string versionNumber3 = runData.RunDataValue("Actual_Version3");
                        //Send a Rest API Put to inverter simulator with Actual Power P value
                        ConsumeDCSimRest.UpdateBatteryAlarms(versionNumber1, register: runData.RunDataValue("Registerversion1").ToString(), label: "bmsversionlevel1");
                        ConsumeDCSimRest.UpdateBatteryAlarms(versionNumber2, register: runData.RunDataValue("Registerversion2").ToString(), label: "bmsversionlevel2");
                        ConsumeDCSimRest.UpdateBatteryAlarms(versionNumber3, register: runData.RunDataValue("Registerversion3").ToString(), label: "bmsversionlevel3");
                        ConsumeDCSimRest.UpdateBatteryAlarms("0", register: "57349", label: "status");
                        System.Threading.Thread.Sleep(4000);
                        ConsumeDCSimRest.UpdateBatteryAlarms("16384", register: "57349", label: "status");
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(5000);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(1000, webDriver))
                        {
                            DashboardPage.ChangeModeAs(webDriver, "Standby");
                            webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.ZoneStatusText, "Standby"), numberOfTimes: waitTime);
                        }
                        System.Threading.Thread.Sleep(15000);
                        var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                        //When system is in Standby, Racks are Green then Contacters should be closed state 
                        string logFile = ihi_testlib_espilot.SSH.SearchLogFilesForString("Current Interface Version .LMU.BCMU.BAMS. " + runData.RunDataValue("Expected_Version").ToString(), GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, filePath: "decs_batteryd.log");
                        if (logFile.Contains("Current Interface Version (LMU.BCMU.BAMS) " + runData.RunDataValue("Expected_Version").ToString()))
                        {
                            _test.Pass("Log file is created with contents requested:  " + logFile);
                        }
                        else
                        {
                            _test.Fail("Log file is not shown with the required text Current Interface Version!!!");
                            throw new Exception("Log file is not shown with the required text Current Interface Version!!!");
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of UI when error occured" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {

                        runData.RunRow++;
                    }
                }              
            }
            SeleniumWrapper.DriverCleanup(webDriver);
            video.StopRecording();
        }

    }
}