﻿using System;
using ihi_test_00202.Initialize;
using NUnit.Framework;
using AventStack.ExtentReports;
using ihi_testlib_espilot;
using System.Configuration;
using ihi_testlib_videorecorder;
using ihi_testlib_modbus;

namespace ihi_test_00202
{
    [TestFixture]
    public class ihi_test_00202 : TestInitialize
    {
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        [Test, Category("ES/Pilot_UI")]
        [Property("Test", "TEST-202")]
        public void TEST_00202__UsersShow_UI()
        {
            var testName = GetType().Namespace;
            video.StartRecording(testName, SaveMe.Always);
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00202__rundata.xlsx", 0);
            runData.RunRow = 0;
            bool result = false;
            //skip test if runTest is false;
            if (runData.MetaData[runData.RunRow].runTest == "False")
            {
                _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                runData.RunRow++;
                return;
            }

            
            runData.RunRow = 0;
            for (int j = runData.RunRow; j < runData.RowCount; j++)
            {
                //SSH.KillDecs(sshInfo);
                //SSH.StartDecs(DECSLaunchType.Test, sshInfo, 10000);
                try
                {
                    //intial variables
                    var usersnumber = runData.RunDataValue("usersNumber");

                    //set event number to int
                    int iLoop;
                    int.TryParse(usersnumber, out iLoop);

                    //Step 1: Delete all users from the database
                    Userpage.LoadSuperusersDatabase(GlobalsGeneralStrings.SSHhostname, GlobalsGeneralStrings.SSHusername, GlobalsGeneralStrings.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

                    //Step 2: Create number of users
                    for (var i = 5; i <= iLoop + 4; i++)
                    {
                        Userpage.CreateUserDatabase(i, "login" + i, i + " A. Test", i + "FakeTest@gmail.com",
                            1 + i % 3, 0, i.ToString(), GlobalsGeneralStrings.SSHhostname, GlobalsGeneralStrings.SSHusername, GlobalsGeneralStrings.SSHpassword, GlobalVariables.DECSDatabaseDirectory, false);
                    }

                    //Step 3: Launch UI and login
                    SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                    LoginPage.loginvalid(webDriver);
                    runData.FirstRun = false;
                    System.Threading.Thread.Sleep(TimingConstants.UILoadWait);

                    //Step 4: Select Users tab
                    MainPage.navigateToAdmin(webDriver);
                    System.Threading.Thread.Sleep(TimingConstants.UILoadAfterDatabaseChange);

                    //Step 5: Verify that number of events are displayed
                    var UsersNumberOnPage = Userpage.CountNumberofUsers(webDriver);

                    //Added four due to 3 ihi users expected + admin
                    if (UsersNumberOnPage != int.Parse(usersnumber) + 4)
                    {
                        result = false;
                        _test.Fail(
                            string.Format(
                                "Users page does not have correct number of users.  Number on Page: {0}, expected: {1}.\n",
                                UsersNumberOnPage, int.Parse(usersnumber) + 4), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        return;
                    }

                    _test.Info("The test has passed.  Run info: " + usersnumber);
                    _test.Pass(
                     string.Format(
                         "Users page have correct number of users.  Number on Page: {0}, expected: {1}.\n",
                         UsersNumberOnPage, int.Parse(usersnumber) + 4), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                    // Delete all users created for testing 
                    Userpage.DeleteUsers(GlobalsGeneralStrings.SSHhostname, GlobalsGeneralStrings.SSHusername, GlobalsGeneralStrings.SSHpassword, GlobalVariables.DECSDatabaseDirectory,
                        userNameValue: "login%", FullNameValue: "%Test", emailAddressValue: "%FakeTest@gmail.com");
                }
                catch (Exception e)
                {
                    //Test sends fail and records fail chain
                    _test.Fail(e);
                }
                finally
                {
                    //Step 6: Restore default values into database
                    runData.RunRow++;
                    //Logs out to ready for next Run Test
                    MainPage.logout(webDriver);
                    SeleniumWrapper.DriverCleanup(webDriver);
                }
            }
            video.StopRecording();
        }


    }
}
