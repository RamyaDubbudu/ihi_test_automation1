using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using System.Configuration;
using Newtonsoft.Json.Linq;
using ihi_testlib_videorecorder;

namespace ihi_test_00188
{
    [TestFixture]
    public class ihi_test_00188 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Category("Inverter_Command"), Property("Test", "TEST-188")]
        public void TEST_00188__Command_Inverter_QDemand()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00188__rundata.xlsx", 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");

            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string runData_QDemand = runData.RunDataValue("Q_DEMAND").ToString();
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(5000, webDriver))
                        {
                            // Function will check the status and change mode to Manual mode
                            DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
                        }
                        else if ("Online" == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalDashbaordInverterDetails.InverterStatus)))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
                        }
                        //Set Run data value for Q Demand in the Pilot dashboard page  
                        webDriver.AddTextToTextbox(textToAdd: runData_QDemand, XPath: string.Format(GlobalStartupShutdown.qField));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.Apply));
                        System.Threading.Thread.Sleep(5000);
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.qDemandText, runData_QDemand), numberOfTimes: waitTime);
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.qActualText, runData_QDemand), numberOfTimes: waitTime);
                        if (runData_QDemand == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.qDemand)))
                        {
                            _test.Pass(runData_QDemand + "As shown in the Dashboard page ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        string getQDemand = ConsumeDCSimRest.InverterRestResponseByName("ihi", "Device/Inverter/reactive_power_demand");
                        var jsonObj = JObject.Parse(getQDemand);
                        getQDemand = (string)jsonObj["value"];
                        //Validate the QDemand value on dashboard
                        if (getQDemand == runData_QDemand)
                        {
                            _test.Pass("Q Demand value: " + runData_QDemand + " sent thru the ESPilot Dashboard is equal to Rest API Get for PDemand : " + getQDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.InvertersTab));
                            webDriver.WaitForElement(xpath: string.Format(GlobalElementsControl.InvertersTab), numberOfTimes: waitTime);
                            getQDemand = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Inverters.InvertersQDemand));
                            if (getQDemand == runData_QDemand)
                            {
                                _test.Pass("Q Demand value: " + runData_QDemand + " sent thru the ESPilot Control Tab > Inverter Tab is equal to Rest API Get for PDemand : " + getQDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                                Assert.AreEqual(getQDemand, runData_QDemand);
                            }
                            else
                            {
                                _test.Fail("Q Demand value in Inverter tab: " + runData_QDemand + " is not equal to the registervalue in Rest API " + getQDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                                throw new Exception("Q Demand value in Inverter tab: " + runData_QDemand + " is not equal to the registervalue in Rest API " + getQDemand);
                            }
                        }
                        else
                        {
                            _test.Fail("Q Demand value in dashboard page: " + runData_QDemand + " is not equal to the registervalue in Rest API " + getQDemand, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception("Q Demand value in Inverter tab: " + runData_QDemand + " is not equal to the registervalue in Rest API " + getQDemand);
                        }
                    }
                    catch (Exception ex)
                    {
                         _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }                
            }
            //video.StopRecording();
        }

    }
}

