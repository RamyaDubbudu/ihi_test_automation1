using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;
using System.Configuration;

namespace ihi_test_00030
{
    [TestFixture]
    public class ihi_test_00030 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("ZMQCommunicationTests"), Property("Test", "TEST-30")]
        public void TEST_00030__Valid_ZMQReply()
        {
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            ushort iso_sequence = 500;
            ushort registerAddress = 0;
            //Issue write single register request for ISO_SEQ holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
            System.Threading.Thread.Sleep(2000);
            //Verify ISO Server issued ZMQ Reconnect request
            string logFile = SSH.SearchLogFilesForString("Log File Opened", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Log File Opened", logFile);
            _test.Info("Here is the message shown in the isomb log file:" + logFile);
            //ZMQ Reconnect request issued from ISO Server
            logFile = SSH.SearchLogFilesForString("Command-Line: -D", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString(), logFile);
            _test.Info("Here is the message shown in the isomb log file:" + logFile);
            logFile = SSH.SearchLogFilesForString("Command-Line: -l", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -l ['Logging Level'] = 0. (File:", logFile);
            _test.Info("Here is the message shown in the isomb log file:" + logFile);
            logFile = SSH.SearchLogFilesForString(".ZeroMQ .ZMQ. Requester Socket. Opened on", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("'ZeroMQ (ZMQ) Requester Socket' Opened on (" + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + ":1039).", logFile);
            _test.Info("Here is the message shown in the isomb log file:" + logFile);
            int siteId = Convert.ToInt32(ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetId"));
            int siteUTime = Convert.ToInt32(ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetUTime"));
            string siteUrl = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetURL");
            string siteName = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetName");
            string siteDesc = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetDesc");
            double lat = Convert.ToDouble(ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetLat"));
            // string lon = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetLon");
            logFile = SSH.SearchLogFilesForString("Received .Get Site Information .600.. publication from the DC: JSON  ..", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Received 'Get Site Information (600)' publication from the DC: JSON  {\"get_site_data_response\": { \"site_table\":[{\"rmon_site_id\":" + siteId + ",\"site_utime\":" + siteUTime + ",\"url\":" + siteUrl + ",\"name\":" + siteName + ",\"description\":" + siteDesc + ",\"lat\":" + lat + ",\"lon\""))
            {
                _test.Pass("Log entry exists that indicates ZMQ Site_Info message has been received and unpacked. Details shown in the log file as Received 'Get Site Information (600)' publication from the DC: JSON  {\"get_site_data_response\": { \"site_table\":[{\"rmon_site_id\":" + siteId + ",\"site_utime\":" + siteUTime + ",\"url\":" + siteUrl + ",\"name\":" + siteName + ",\"description\":" + siteDesc + ",\"lat\":" + lat + ",\"lon\"");
            }
            else
            {
                _test.Fail("Log entry does not indicate ZMQ Site_Info message");
                throw new Exception("Log entry does not indicate ZMQ Site_Info message");
            }
            
        }
    }
}