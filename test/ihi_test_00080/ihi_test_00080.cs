using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace ihi_test_00080
{
    [TestFixture]
    public class ihi_test_00080 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Battery_Status"), Property("Test", "TEST-80")]
        public void TEST_00080_BatterySOC_Status()
        {
            var testname = GetType().Namespace;
            video.StartRecording(videoDir, SaveMe.Always);
            string registerName = "soc";
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00080__rundata.xlsx", 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        //Send a Rest API Put to BATTERY simulator with Actual Power P value
                        string runData_SOC = runData.RunDataValue("SOC").ToString();

                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(1000);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(1000, webDriver))
                        {
                            // Function will change status to standby and checks Racks connection 
                            DashboardPage.Change2Standby_CheckBatteryConnection(webDriver);
                        }
                        else if ("Standby" == DashboardPage.GetCurrenteMode(6000, webDriver))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.ZoneStatusText, "Standby")));
                        }
                        //Send a Rest API Put to Battery simulator with SOC value 
                        ConsumeDCSimRest.UpdateBatteryDeviceType(registerName, runData_SOC);
                        //Validate the soc value on Control tab > Racks page
                        webDriver.RefreshPage(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.RackDetails), numberOfTimes: waitTime);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(1000);
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsMainPage.ControlTab));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.RacksTab));
                        RackInfo rackInfo =  RacksPage.returnRackInfo(1, webDriver);
                        if (rackInfo.SOC.ToString() == runData_SOC)
                            {
                                _test.Pass("Current Phase C value: " + runData_SOC + " sent thru Rest API is same in the ESPilot Control > Inverters Tab: " + runData_SOC, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            }
                            else
                                _test.Fail("Current Phase C value in Control > Inverters Tab page: " + rackInfo.SOC + " is not equal to the registervalue sent thru Rest API " + runData_SOC, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                       // }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of UI when error occured" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }
            }
            video.StopRecording();
        }

    }
}