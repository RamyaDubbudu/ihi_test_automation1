using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00012
{
    [TestFixture]
    public class ihi_test_00012 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        
         [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-12")]
        public void TEST_00012__Write_ISO_RAMP_RATE_UP_Q_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00012__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort iso_Ramp_Rate_Up_Q = 500;
            ushort registerAddress = 10;
            short q_min = -32768;
            short q_max = 32767;
            ConsumeDCSimRest.UpdateSiteData("q_min", q_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("q_max", q_max.ToString());
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_Ramp_Rate_Up_Q));
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            iso_Ramp_Rate_Up_Q = (UInt16)values[0];
            Assert.IsTrue(iso_Ramp_Rate_Up_Q.ToString() == "500", "ISO_Ramp_Rate_Up_P is equal to the initial value");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {

                        short runData_ISO_Ramp_Rate_Up_Q = short.Parse(runData.RunDataValue("ISO_RAMP_RATE_UP_Q"));
                        string RID_q_min = runData.RunDataValue("ESS_QMIN");
                        string RID_q_max = runData.RunDataValue("ESS_QMAX");
                        // Publish ESS_QMIN and ESS_QMAX data from DC Sim for the Run ID
                        ConsumeDCSimRest.UpdateSiteData("q_min", RID_q_min);
                        ConsumeDCSimRest.UpdateSiteData("q_max", RID_q_max);
                        //Send a Modbus request to the ISO slave to write the "ISO_RAMP_RATE_UP_P" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(runData_ISO_Ramp_Rate_Up_Q));
                        System.Threading.Thread.Sleep(2000);
                        //Send a Modbus request to the ISO slave to read the "ISO_SOC_Q_TGT" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iso_Ramp_Rate_Up_Q = (UInt16)values[0];
                        if (iso_Ramp_Rate_Up_Q.ToString() == runData.RunDataValue("ISO_RAMP_RATE_UP_Q_EXPECTED"))
                        {
                            Assert.IsTrue(iso_Ramp_Rate_Up_Q.ToString() == runData.RunDataValue("ISO_RAMP_RATE_UP_Q_EXPECTED"), "response Modbus message register value " + iso_Ramp_Rate_Up_Q + " is equal to the Run Value" + runData.RunDataValue("ISO_RAMP_RATE_UP_Q_EXPECTED") + " row " + runData.RunRow + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetRampUpQ");
                            StringAssert.Contains(iso_Ramp_Rate_Up_Q.ToString(), RestGetresult);
                            _test.Pass("response Modbus message register value " + iso_Ramp_Rate_Up_Q + " is equal to the Run Value" + runData_ISO_Ramp_Rate_Up_Q + " column value");
                        }
                        else
                        {
                            _test.Fail("response Modbus message register value " + iso_Ramp_Rate_Up_Q + " is not equal to the Run Value" + runData_ISO_Ramp_Rate_Up_Q + " column value");
                            throw new Exception("response Modbus message register value " + iso_Ramp_Rate_Up_Q + " is not equal to the Run Value" + runData_ISO_Ramp_Rate_Up_Q + " column value");
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }
    }
}