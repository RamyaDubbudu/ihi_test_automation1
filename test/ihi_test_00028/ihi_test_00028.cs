using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00028
{
    [TestFixture]
    public class ihi_test_00028 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-28")]
        public void TEST_00028__Read_ESS_AVAILABLE_MODES_FLAGS_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00028__rundata.xlsx", 0);
            runData.RunRow = 0;
            UInt32 available_ModeFlags1 = 0;
            UInt32 available_ModeFlags2 = 0;
            UInt32 available_ModeFlags = 0;
            for (int i = runData.RunRow; i < 3; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    UInt32 runData_EssAvlbModesFlags = Convert.ToUInt32(runData.RunDataValue("ESS_AVAILABLE_MODES_FLAGS"));
                    ushort registerAddress = 20;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("available_mode_flags", runData_EssAvlbModesFlags.ToString());
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 2);
                    available_ModeFlags1 = (UInt32)values[0];
                    available_ModeFlags2 = (UInt32)values[1];
                    //UInt32 u32LabelModesAvailable = (UInt32)((u16slabelModesAvailable_2 << 16) | u16slabelModesAvailable_1);
                    available_ModeFlags = (UInt32)available_ModeFlags2 << 16 | available_ModeFlags1;
                    int modeFlags = (Int16)available_ModeFlags;
                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (runData_EssAvlbModesFlags.ToString() == available_ModeFlags.ToString()) // invalid Scenarios
                    {
                        Assert.IsTrue(runData_EssAvlbModesFlags.ToString() == available_ModeFlags.ToString(), "response Modbus message register value is equal to the Run ID" + available_ModeFlags + "column value");
                        string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetAvailableModeFlag");
                        StringAssert.Contains(modeFlags.ToString(), RestGetresult);
                        _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_AVAILABLE_MODES_FLAGS-" + runData_EssAvlbModesFlags + "response Modbus message register value is equal to the Run ID" + available_ModeFlags + "column value");
                    }
                    else
                    {
                        _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_AVAILABLE_MODES_FLAGS-" + runData_EssAvlbModesFlags + "response Modbus message register value is not equal to the Run ID" + available_ModeFlags + "column value");
                        throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_AVAILABLE_MODES_FLAGS-" + runData_EssAvlbModesFlags + "response Modbus message register value is not equal to the Run ID" + available_ModeFlags + "column value");
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }


    }
}