using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;

namespace ihi_test_00014
{
    [TestFixture]
    public class ihi_test_00014 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("ZMQCommunicationTests"), Property("Test", "TEST-14")]
        public void TEST_00014__UpdateUnknownHoldingRegister()
        {
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            ushort iso_sequence = 500;
            ushort registerAddress = 0;
            ushort invalidRegisterAddress = 25;
            //Issue write single register request for ISO_SEQ holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
            System.Threading.Thread.Sleep(2000);
            //Verify ISO Server issued ZMQ Reconnect request
            string logFile = SSH.SearchLogFilesForString("Log File Opened", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Log File Opened", logFile);
            //ZMQ Reconnect request issued from ISO Server
            logFile = SSH.SearchLogFilesForString("Command-Line: -D", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString(), logFile);
            logFile = SSH.SearchLogFilesForString("Command-Line: -l", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -l ['Logging Level'] = 0. (File:", logFile);
            logFile = SSH.SearchLogFilesForString(".ZeroMQ .ZMQ. Requester Socket. Opened on", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("'ZeroMQ (ZMQ) Requester Socket' Opened on (" + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + ":1039).", logFile);
            logFile = SSH.SearchLogFilesForString("New connection from ..", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("New connection from", logFile);
            //Verify log file contains entry that indicates Reply Message.
            logFile = SSH.SearchLogFilesForString("modbus_receive .Socket = 1., Length = 12.   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500 .", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("modbus_receive (Socket = 16, Length = 12)   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500", logFile);
            //Issue invalid request from Modbus client
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, invalidRegisterAddress, Convert.ToUInt16(iso_sequence));
            logFile = SSH.SearchLogFilesForString("Warning: Unknown .Holding Register. ." + invalidRegisterAddress + ". .", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Warning: Unknown 'Holding Register' [" + invalidRegisterAddress + "]", logFile);
            if (logFile.Contains("Warning: Unknown 'Holding Register' [" + invalidRegisterAddress + "]"))
            {
                _test.Pass("Log entry generated indicating warning that user is requesting to update an unknown holding register");
            }
            else
            {
                _test.Fail("Warning for Unknown holding register request is not shown in log file");
                throw new Exception("Warning for Unknown holding register request is not shown in log file");
            }
            
        }
    }
}