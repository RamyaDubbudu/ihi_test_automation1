using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

// Allow Charge Power Test 94
namespace ihi_test_00094                                                                                                                // Rename Entry
{
    [TestFixture]
    public class ihi_test_00094 : TestInitialize
    {

        #region Variables                                                                                                               
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        public readonly int ThreadSleepTime = 1000;
        const string TestIdentifier = "ihi_test_00094";                                                                                 // Rename Entry
        public const string PropertyDescriptor = "TEST-94";                                                                             // Rename Entry
        public readonly string ExcelColumnHeader = "ALLOWCHARGEPOWER";                                                                  // This is an Excel column header for test parameters 
        public readonly string registerName = "charge_limit";                                                                           // This is the REST Api name

        public readonly string ExcelInputDataFile = "\\" + TestIdentifier + "__rundata.xlsx";                                                  //ihi_test_00082__rundata.xlsx
        public const string CategoryDescriptor = "Battery_Status";
        #endregion                                                                                                                      

        [Test, Category(CategoryDescriptor), Property("Test", PropertyDescriptor)]
        public void TEST_00094_Allow_Charge_power_Test()                                                                                    // Rename Entry
        {
            var testname = GetType().Namespace;
            video.StartRecording(videoDir, SaveMe.Always);
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + ExcelInputDataFile, 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string TestParam = runData.RunDataValue(ExcelColumnHeader).ToString();

                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(ThreadSleepTime);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(1000, webDriver))
                        {
                            // Function will change status to standby and checks Racks connection 
                            DashboardPage.Change2Standby_CheckBatteryConnection(webDriver);
                        }
                        else if ("Standby" == DashboardPage.GetCurrenteMode(6000, webDriver))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.ZoneStatusText, "Standby")));
                        }
                        ConsumeDCSimRest.UpdateBatteryDeviceType(registerName, TestParam);
                        System.Threading.Thread.Sleep(8000);
                        var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                        //When system is in Standby, Racks are Green then Contacters should be closed state 
                        string logFile = ihi_testlib_espilot.SSH.SearchLogFilesForString("Charge Limit = -" + TestParam + ".000000", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, filePath: "decs_batteryd.log");
                        if (logFile.Contains("Charge Limit = -" + TestParam + ".000000"))
                        {
                            _test.Pass("Log file is created with contents requested:  " + logFile);
                        }
                        else
                        {
                            _test.Fail("Log file is not shown with the required text Contactor Close Request started!!!");
                            throw new Exception("Log file is not shown with the required text - Charge Limit = " + TestParam + ".000000!!!");
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of UI when error occured" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }
            }
            video.StopRecording();
        }

    }
}