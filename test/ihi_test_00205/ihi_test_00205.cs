using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;


namespace ihi_test_00205
{
    [TestFixture]
    public class ihi_test_00205 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Category("CustomModbusPortNumber"), Property("Test", "TEST-205")]
        public void TEST_00205__ISOServerwithMultipleServerSockets()
        {
            int numberOfServerSockets = 10;
            var ssh = new SshClient(connectionInfo:SSH.connectionInfo);
            ihi_testlib_espilot.SSH.SetDCSimulatorIPAddress(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            System.Threading.Thread.Sleep(3000);
            string logFile = SSH.SearchLogFilesForString("Command-Line: -D .'Domain Controller(DC) - IP Address'. = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString(), GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString()))
            {
                for (int i = 0; i < numberOfServerSockets; i++)
                {
                    bool isConnected = ihi_testlib_modbus.ModbusClient.Connected2ModbusServer(GlobalVariables.SSHhostname);
                    if (isConnected)
                    {
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, 3, Convert.ToUInt16(3));
                        ushort[] list = ihi_testlib_modbus.ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, 1, 10);
                        _test.Pass("Modbus client is connected for the " + i + "time");
                    }
                    else
                    {
                        _test.Fail("Modbus client is not connected even before exceeding the multiple server sockets limit!!!!");
                        throw new Exception("Modbus client is not connected even before exceeding the multiple server sockets limit!!!!");
                    }
                }              
            }
           bool shouldNotBeConnected = ihi_testlib_modbus.ModbusClient.Connected2ModbusServer(GlobalVariables.SSHhostname);
            if (shouldNotBeConnected == true)
            {
                _test.Fail("Modbus client should not be connected after exceeding the multiple server sockets limit 10!!!!");
                throw new Exception("Modbus client should not be connected after exceeding the multiple server sockets limit 10!!!!");               
            }
            else
            {
                _test.Pass("Modbus client is not connected after exceeding the multiple server sockets limit!!!!");
            }

        }
    }
}