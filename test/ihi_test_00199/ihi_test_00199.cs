﻿using System;
using ihi_test_00199.Initialize;
using NUnit.Framework;
using AventStack.ExtentReports;
using ihi_testlib_espilot;
using System.Configuration;
using ihi_testlib_videorecorder;
using ihi_testlib_modbus;

namespace ihi_test_00199
{
    [TestFixture]
    public class ihi_test_00199 : TestInitialize
    {
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        [Test, Category("ES/Pilot_UI")]
        [Property("Test", "TEST-199")]
        public void TEST_00199__HoveroverTextAvailableConsistentforBulletGraph()
        {
            var testName = GetType().Namespace;
            bool result = false;
            video.StartRecording(testName, SaveMe.Always);
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            var backup = new DatabaseBackup(sshInfo);
            var backupSuccess = backup.SaveBackupOfDatabases();
            if (!backupSuccess)
            {
                result = false;
                _test.Skip(backup.FailedToBackupMessage + "Failed to backup database");
                return;
            }
            try
            {
                SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                LoginPage.loginvalid(webDriver);

                System.Threading.Thread.Sleep(TimingConstants.UILoadWait);

                //Step 2: Navigate to Chart
                DashboardPage.NavigateToPowerMeter(webDriver);
                //Step 3: Check "carat" tooltip
                ihi_testlib_espilot.SSH.SetAllAssetsAvailableAndEnabled(new ihi_testlib_espilot.SSHAndDatabaseDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory));
                ihi_testlib_espilot.SSH.UpdateToDatabaseOpenClose("100", "site", "kwh_max", 1, GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);
                ihi_testlib_espilot.SSH.UpdateToDatabaseOpenClose("50", "site", "target_soc", 1, GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);
                ihi_testlib_espilot.SSH.UpdateToDatabaseOpenClose(((int)Mode.Peak_Shaving).ToString(), "site", "mode_demand", 1, GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);
                System.Threading.Thread.Sleep(TimingConstants.UILoadAfterDatabaseChange);
                webDriver.HoverOverByControl(XPath: string.Format(GlobalElementsPowerEnergyChart.SliderText));
                System.Threading.Thread.Sleep(1000);
                var caratTootip = webDriver.ReadControlsInnerText(idTXT: "aToolTip");
                var expectedCaratTooltip = "Target SOC";
                if (caratTootip != expectedCaratTooltip)
                {
                    result = false;
                    _test.Fail(string.Format("Carat tooltip unexpected value. UI Message: {0}. Expected Value: {1}.\n",
                            caratTootip, expectedCaratTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                }
                _test.Pass(string.Format("Carat tooltip unexpected value.UI Message: {0}. Expected Value: {1}.\n",
                         caratTootip, expectedCaratTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                //Step 4: Check p meter tooltip
                webDriver.HoverOverByControl(XPath: string.Format(GlobalElementsPowerEnergyChart.PMeterValue));
                System.Threading.Thread.Sleep(1000);
                var pMeterTootip = webDriver.ReadControlsInnerText(idTXT: "aToolTip");
                var expectedPMeterTooltip = "Inverter Power Output (Negative=Charging)";
                if (pMeterTootip != expectedPMeterTooltip)
                {
                    result = false;
                    _test.Fail(string.Format("p Meter tooltip unexpected value. UI Message: {0}. Expected Value: {1}.\n",
                               pMeterTootip, expectedPMeterTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                }
                _test.Pass(string.Format("p Meter tooltip unexpected value. UI Message: {0}. Expected Value: {1}.\n",
                           pMeterTootip, expectedPMeterTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                //Step 5: Check q meter tooltip
                webDriver.HoverOverByControl(XPath: string.Format(GlobalElementsPowerEnergyChart.QMeterValue));
                System.Threading.Thread.Sleep(1000);
                var qMeterTootip = webDriver.ReadControlsInnerText(idTXT: "aToolTip");
                var expectedQMeterTooltip = "Inverter kVAR Output (-Lagging, +Leading)";
                if (qMeterTootip != expectedQMeterTooltip)
                {
                    result = false;
                    _test.Fail(string.Format("p Meter tooltip unexpected value. UI Message: {0}. Expected Value: {1}.\n",
                            pMeterTootip, expectedPMeterTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                }
                _test.Pass(string.Format("p Meter tooltip unexpected value. UI Message: {0}. Expected Value: {1}.\n",
                   pMeterTootip, expectedPMeterTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                //Step 6: Check energy meter tooltip
                webDriver.HoverOverByControl(XPath: string.Format(GlobalElementsPowerEnergyChart.EnergyAvailableMeterValue));
                System.Threading.Thread.Sleep(1000);
                var energyAvailableTootip = webDriver.ReadControlsInnerText(idTXT: "aToolTip");
                var expectedEnergyAvailableTooltip = "Amount of Energy Available in Batteries";
                if (qMeterTootip != expectedQMeterTooltip)
                {
                    result = false;
                    _test.Fail(string.Format("Energy Available tooltip unexpected value. UI Message: {0}. Expected Value: {1}.\n",
                               energyAvailableTootip, expectedEnergyAvailableTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                }
                _test.Pass(string.Format("Energy Available tooltip unexpected value. UI Message: {0}. Expected Value: {1}.\n",
                                energyAvailableTootip, expectedEnergyAvailableTooltip), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
            }
            catch (Exception e)
            {
                //Test sends fail and records fail chain
                _test.Fail(e);
            }
            finally
            {
                backup.RestoreBackupOfDatabases();
                //Step 7: Logs out
                MainPage.logout(webDriver);
                SeleniumWrapper.DriverCleanup(webDriver);
            }
            video.StopRecording();
        }


    }
}
