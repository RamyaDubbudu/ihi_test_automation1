using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00050
{
    [TestFixture]
    public class ihi_test_00050 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-50")]
        public void TEST_00050__Write_ISO_RAMP_RATE_UP_P_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00050__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort iSO_Ramp_Rate_Up_P = 500;
            ushort registerAddress = 8;
            short p_min = -32768;
            short p_max = 32767;
            ConsumeDCSimRest.UpdateSiteData("p_min", p_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("p_max", p_max.ToString());
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iSO_Ramp_Rate_Up_P));
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            iSO_Ramp_Rate_Up_P = (UInt16)values[0];
            Assert.IsTrue(iSO_Ramp_Rate_Up_P.ToString() == "500", "ISO_Ramp_Rate_Up_P is equal to the initial value");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {

                        short runData_ISO_RAMP_RATE_UP_P = short.Parse(runData.RunDataValue("ISO_RAMP_RATE_UP_P"));
                        string RID_p_min = runData.RunDataValue("ESS_PMIN");
                        string RID_p_max = runData.RunDataValue("ESS_PMAX");
                        // Publish ESS_QMIN and ESS_QMAX data from DC Sim for the Run ID
                        ConsumeDCSimRest.UpdateSiteData("p_min", RID_p_min);
                        ConsumeDCSimRest.UpdateSiteData("p_max", RID_p_max);
                        //Send a Modbus request to the ISO slave to write the "ISO_RAMP_RATE_UP_P" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(runData_ISO_RAMP_RATE_UP_P));
                        System.Threading.Thread.Sleep(2000);
                        //Send a Modbus request to the ISO slave to read the "ISO_SOC_Q_TGT" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iSO_Ramp_Rate_Up_P = (UInt16)values[0];
                        if (iSO_Ramp_Rate_Up_P.ToString() == runData.RunDataValue("ISO_RAMP_RATE_UP_P_EXPECTED"))
                        {
                            Assert.IsTrue(iSO_Ramp_Rate_Up_P.ToString() == runData.RunDataValue("ISO_RAMP_RATE_UP_P_EXPECTED"), "response Modbus message register value " + iSO_Ramp_Rate_Up_P + " is equal to the Run Value" + runData.RunDataValue("ISO_RAMP_RATE_UP_P_EXPECTED") + " row " + runData.RunRow + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetRampUpP");
                            StringAssert.Contains(iSO_Ramp_Rate_Up_P.ToString(), RestGetresult);
                            _test.Pass("response Modbus message register value " + iSO_Ramp_Rate_Up_P + " is equal to the Run Value" + runData_ISO_RAMP_RATE_UP_P + " column value");
                        }
                        else
                        {
                            _test.Fail("response Modbus message register value " + iSO_Ramp_Rate_Up_P + " is not equal to the Run Value" + runData_ISO_RAMP_RATE_UP_P + " column value");
                            throw new Exception("response Modbus message register value " + iSO_Ramp_Rate_Up_P + " is not equal to the Run Value" + runData_ISO_RAMP_RATE_UP_P + " column value");
                        }
                        }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }


    }
}