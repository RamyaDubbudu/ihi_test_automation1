using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00004
{
    
    [TestFixture]
    public class ihi_test_00004 : TestInitialize
    {
       
        #region Variables 
        SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-4")]
        public void TEST_00004__Read_ESS_TOTAL_ALARMS_CT_InputRegister()
        {
           
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00004__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort ess_TotalAlarmct = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    ushort runData_EssTotalAlarmsCt = Convert.ToUInt16(runData.RunDataValue("ESS_TOTAL_ALARMS_CT"));
                    ushort registerAddress = 14;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("alarms_total", runData_EssTotalAlarmsCt.ToString());
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ess_TotalAlarmct = values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                   if (runData_EssTotalAlarmsCt == ess_TotalAlarmct) //Valid Scenarios
                    {
                        Assert.IsTrue(runData_EssTotalAlarmsCt == ess_TotalAlarmct, "response Modbus message register value " + ess_TotalAlarmct + " is equal to the Run ID" + ess_TotalAlarmct + "column value");
                        string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetAlarmsTotal");
                        StringAssert.Contains(runData_EssTotalAlarmsCt.ToString(), RestGetresult);
                        string getSiteData = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetTable");
                        _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_TOTAL_ALARMS_CT-" + runData_EssTotalAlarmsCt + " Actual ESS_TOTAL_ALARMS_CT-" + runData_EssTotalAlarmsCt + "response Modbus message register value is equal to the Run ID" + ess_TotalAlarmct + "column value" + getSiteData);
                    }
                    else
                    {
                        _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_TOTAL_ALARMS_CT-" + runData_EssTotalAlarmsCt + " Actual ESS_TOTAL_ALARMS_CT-" + runData_EssTotalAlarmsCt + "response Modbus message register value is not equal to the Run ID" + ess_TotalAlarmct + "column value");
                        throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_TOTAL_ALARMS_CT-" + runData_EssTotalAlarmsCt + " Actual ESS_TOTAL_ALARMS_CT-" + runData_EssTotalAlarmsCt + "response Modbus message register value is not equal to the Run ID" + ess_TotalAlarmct + "column value");
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }

    }
}