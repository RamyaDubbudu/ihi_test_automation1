using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;


namespace ihi_test_00210
{
    [TestFixture]
    public class ihi_test_00210 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        public static string GenerateRandomNumber(int intNum)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            Thread.Sleep(50);
            string returnString = string.Empty;
            int counter;

            for (counter = 0; counter < intNum; counter++)
            {
                returnString += random.Next(1, 9).ToString();
            }
            return returnString;
        }

        [Test, Category("CustomDomainControllerIP"), Property("Test", "TEST-210")]
        public void TEST_00210__CustomDomainControllerIP()
        {
            string ipAddress = ConfigurationManager.AppSettings["DCIpAddress"].ToString();
            var ssh = new SshClient(connectionInfo:SSH.connectionInfo);
            string killisomb = string.Empty;
            string resetisomb = string.Empty;
            ssh.Connect();
            if (ConfigurationManager.AppSettings["IsDocker"].ToString() == "True")
            {
                killisomb = string.Format(ihi_testlib_espilot.DockerCommands.SSHCommands.Killisomb);
                resetisomb = "sudo docker exec -id decs-dc bash -c \"cd /usr/local/ihi-decs/bin&&echo decs |./decs_isomb -D " + ipAddress + "\"";
                SSH.OpenWrite(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                SSH.OpenWrite(ssh, resetisomb);
                System.Threading.Thread.Sleep(3000);
            }
            else
            {
                killisomb = "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_isomb";
                resetisomb = "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_isomb -D " + ipAddress;
                ihi_testlib_espilot.SSH.ExecCommand(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                ihi_testlib_espilot.SSH.ExecCommand(ssh, resetisomb);
                System.Threading.Thread.Sleep(1000);
            }
            System.Threading.Thread.Sleep(3000);
            string logFile = SSH.SearchLogFilesForString("Command-Line: -D .'Domain Controller(DC) - IP Address'. = " + ipAddress + ".", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ipAddress + "."))
            {
                _test.Pass("Log file is shown with the random domain controller IP Address number user set:  " + logFile);
            }
            else
            {
                _test.Fail("Log file is not shown with the random domain controller IP Address number user set!!! " + logFile);
                throw new Exception("Log file is not shown with the random domain controller IP Address number user set!!!!!!");
            }
                   
        }
    }
}