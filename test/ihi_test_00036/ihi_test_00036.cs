using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_videorecorder;
using ihi_testlib_modbus;

namespace ihi_test_00036
{
    [TestFixture]
    public class ihi_test_00036 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-36")]
        public void TEST_00036__Read_ESS_ZONES_ONLINE_FLAGS_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00036__rundata.xlsx", 0);
            runData.RunRow = 0;
            UInt32 zONES_OnlineFlags = 0;
            UInt32 zONES_OnlineFlags1 = 0;
            UInt32 zONES_OnlineFlags2 = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ZonesOnlineFlags = runData.RunDataValue("ESS_ZONES_ONLINE_FLAGS");
                    ushort registerAddress = 18;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ZonesOnlineFlags data from DC Sim for the Run ID  ZonesOnlineFlags =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("zones_online_flags", runData_ZonesOnlineFlags);
                    //Send a Modbus request to the ISO server to read the input register (ZonesOnlineFlags)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 2);
                    zONES_OnlineFlags1 = (UInt32)values[0];
                    zONES_OnlineFlags2 = (UInt32)values[1];
                    zONES_OnlineFlags = (UInt32)zONES_OnlineFlags2 << 16 | zONES_OnlineFlags1;
                    Int16 Zone_onlineflag = (Int16)(zONES_OnlineFlags);
                    //Verify response Modbus message register value is equal to the Run ID *ZonesOnlineFlags*column value
                    // If Run ID ZonesOnlineFlags valid:   Run id ZonesOnlineFlags , if Run ID ZonesOnlineFlags invalid:   equal to initial ZonesOnlineFlags

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_ZonesOnlineFlags == zONES_OnlineFlags.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_ZonesOnlineFlags == zONES_OnlineFlags.ToString(), "response Modbus message register value is equal to the Run ID" + runData_ZonesOnlineFlags + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetZonesOnlineFlag");
                            StringAssert.Contains(Zone_onlineflag.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZONES_ONLINE_FLAGS-" + runData_ZonesOnlineFlags + " Actual ESS_ZONES_ONLINE_FLAGS-" + runData_ZonesOnlineFlags + "response Modbus message register value is equal to the Run ID" + zONES_OnlineFlags + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZONES_ONLINE_FLAGS-" + runData_ZonesOnlineFlags + " Actual ESS_ZONES_ONLINE_FLAGS-" + runData_ZonesOnlineFlags + "response Modbus message register value is not equal to the Run ID" + zONES_OnlineFlags + "column value");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZONES_ONLINE_FLAGS-" + runData_ZonesOnlineFlags + " Actual ESS_ZONES_ONLINE_FLAGS-" + runData_ZonesOnlineFlags + "response Modbus message register value is not equal to the Run ID" + zONES_OnlineFlags + "column value");
                        }
                    }

                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}