using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00006
{
    [TestFixture]
    public class ihi_test_00006 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-6")]
        public void TEST_00006__Write_ISO_MODE_DEMAND_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00006__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort iso_Mode_Demand = 0;
            ushort registerAddress = 3;
            //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_Mode_Demand));
            //Send a Modbus request to the ISO slave to read the "ISO_SEQ" holding register
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            iso_Mode_Demand = (UInt16)values[0];
            for (int i = runData.RunRow; i < 18; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        ushort runData_ISO_Mode_Demand = ushort.Parse(runData.RunDataValue("ISO_Mode_Demand"));
                        //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, runData_ISO_Mode_Demand);
                        //Send a Modbus request to the ISO slave to read the "ISO_SEQ" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iso_Mode_Demand = (UInt16)values[0];
                        if (runData_ISO_Mode_Demand.ToString() == iso_Mode_Demand.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_ISO_Mode_Demand.ToString() == iso_Mode_Demand.ToString(), "response Modbus message register value " + iso_Mode_Demand + " is equal to the Run Value" + runData_ISO_Mode_Demand + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetISOModeDemand");
                            StringAssert.Contains(iso_Mode_Demand.ToString(), RestGetresult);
                            string getRemoteData = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetTable");
                            _test.Pass("response Modbus message register value " + iso_Mode_Demand + " is equal to the Run Value" + runData_ISO_Mode_Demand + " column value" + getRemoteData);
                        }
                        else
                        {
                            _test.Fail("RunID-" + runData.RunRow + " Expected ISO_Mode_Demand: " + iso_Mode_Demand + " Actual ISO_Mode_Demand: " + runData_ISO_Mode_Demand + "response Modbus message register value is not equal to the Run ID" + iso_Mode_Demand + "column value");
                            throw new Exception("RunID-" + runData.RunRow + " Expected ISO_Mode_Demand: " + iso_Mode_Demand + " Actual ISO_Mode_Demand: " + runData_ISO_Mode_Demand + "response Modbus message register value is not equal to the Run ID" + iso_Mode_Demand + "column value");
                        }                              
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }

    }
}