using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using Newtonsoft.Json.Linq;
using ihi_testlib_videorecorder;
using System.Configuration;

namespace ihi_test_00156
{
    [TestFixture]
    public class ihi_test_00155 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Inverter_Status"), Property("Test", "TEST-156")]
        public void TEST_00156__Frequency_Status()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            string registerName = "frequency";
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00156__rundata.xlsx", 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        //Send a Rest API Put to inverter simulator with frequency value
                        string runData_Frequency = runData.RunDataValue("Frequency").ToString();
                        ConsumeDCSimRest.UpdateInverterDeviceType(registerName, runData_Frequency);
                        // Get the Value of AC_FREQ columns from Inverters table from database
                        var ssh = new SshClient(ihi_testlib_espilot.SSH.connectionInfo);
                        string frequencyValue = "";
                        int j = 0;
                        //do
                        //{
                            //string command = "select ac_freq from inverters";
                            //frequencyValue = ihi_testlib_espilot.SSH.ExecuteSqlite(ssh, command, dockerContainerName: "dc");
                            string command = "sudo docker exec decs-dc bash -c 'cd /usr/local/ihi-decs/bin&&echo \"select ac_freq from inverters\" | ./query'";
                            frequencyValue = ihi_testlib_espilot.SSH.OpenWrite(ssh, command);
                            j++;
                        //} while (!frequencyValue.Contains(runData_Frequency) || j < 5);
                        //Validate the Frequency value in table is equal to the value set thru REST API
                        if (frequencyValue.Contains(runData_Frequency))

                        {
                            _test.Pass("Frequncy value: " + runData_Frequency + " sent thru Rest API is same shown in ac_freq column in Inverters table " + runData_Frequency);
                        }
                        else
                        {
                            _test.Fail("ac_freq column in Inverters table: " + frequencyValue + " is not equal to the registervalue sent thru Rest API " + runData_Frequency);
                            throw new Exception("ac_freq column in Inverters table: " + frequencyValue + " is not equal to the registervalue sent thru Rest API " + runData_Frequency);

                        }
                    }
                    catch (Exception ex)
                    {
                         _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
            }
                //video.StopRecording();
        }

    }
}