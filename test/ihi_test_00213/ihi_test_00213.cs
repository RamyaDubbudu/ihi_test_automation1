using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;


namespace ihi_test_00213
{
    [TestFixture]
    public class ihi_test_00213 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Category("NoIP_Modbus"), Property("Test", "TEST-213")]
        public void TEST_00213__InitializeNoMasterClientModbusIP()
        {
            string ipAddress = "192.168.2.83";
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            SSH.OpenWrite(ssh, "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_isomb");
            System.Threading.Thread.Sleep(3000);
            SSH.ExecCommand(ssh, "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_isomb -M " + ipAddress);
            System.Threading.Thread.Sleep(3000);
            string logFile = SSH.SearchLogFilesForString("Command-Line: -M .ISO Master - IP Address'. = " + ipAddress + ".", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Command-Line: -M [ISO Master - IP Address'] = " + ipAddress + "."))
            {
                bool isConnected = ihi_testlib_modbus.ModbusClient.Connected2ModbusServer(GlobalVariables.SSHhostname);
                if (isConnected == false)
                {
                    _test.Pass("Log file is shown with the Modbus IP address set by User:  " + logFile);
                }
                else
                {
                    _test.Fail("Log file is not shown with the Modbus IP address set by User!!! " + logFile);
                    throw new Exception("Log file is not shown with the Modbus IP address set by User!!!!!!");
                }
            }      
        }
    }
}