using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;
using System.Configuration;

namespace ihi_test_00037
{
    [TestFixture]
    public class ihi_test_00037 : TestInitialize
    {

        #region Variables 
        SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ZMQCommunicationTests"), Property("Test", "TEST-37"), Ignore("Process Issue")]
        public void TEST_00037__ZMQEmergencyPowerOffMessage()
        {
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            ushort iso_sequence = 500;
            ushort registerAddress = 0;
            //Issue write single register request for ISO_SEQ holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
            System.Threading.Thread.Sleep(2000);
            //Verify ISO Server issued ZMQ Reconnect request
            string logFile = SSH.SearchLogFilesForString("Log File Opened", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Log File Opened", logFile);
            _test.Info("Here is the message shown in the isomb log: " + logFile);
            //ZMQ Reconnect request issued from ISO Server
            logFile = SSH.SearchLogFilesForString("Command-Line: -D", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString(), logFile);
            _test.Info("Here is the message shown in the isomb log fiole: " + logFile);
            logFile = SSH.SearchLogFilesForString("Command-Line: -l", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -l ['Logging Level'] = 0. (File:", logFile);
            _test.Info("Here is the message shown in the isomb log fiole: " + logFile);
            logFile = SSH.SearchLogFilesForString(".ZeroMQ .ZMQ. Requester Socket. Opened on", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("'ZeroMQ (ZMQ) Requester Socket' Opened on (" + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + ":1039).", logFile);
            _test.Info("Here is the message shown in the isomb log fiole: " + logFile);
            logFile = SSH.SearchLogFilesForString("New connection from ..", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("New connection from :..", logFile);
            _test.Info("Here is the message shown in the isomb log fiole: " + logFile);
            //Verify log file contains entry that indicates Reply Message.
            logFile = SSH.SearchLogFilesForString("modbus_receive .Socket = 1., Length = 1..   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("modbus_receive (Socket = 16, Length = 12)   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500"))
            {
                _test.Pass("Log file contains ZMQ Reply message for the ZMQ Requester Socket.Details of logfile are shown as:  " + logFile);
            }
            else
            {
                _test.Fail("Log file does not contains ZMQ Reply message for the ZMQ Requester Socket.");
                throw new Exception("Log file does not contains ZMQ Reply message for the ZMQ Requester Socket.");
            }
            
        }
    }
}