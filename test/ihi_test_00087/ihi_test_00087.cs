using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

// Average Temperature Test 86
namespace ihi_test_00087                                                                                                                // Rename Entry
{
    [TestFixture]
    public class ihi_test_00087 : TestInitialize
    {

        #region Variables                                                                                                               
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        public readonly int ThreadSleepTime = 1000;
        const string TestIdentifier = "ihi_test_00087";                                                                                 // Rename Entry
        public const string PropertyDescriptor = "TEST-87";                                                                             // Rename Entry
        public readonly string ExcelColumnHeader = "AVGVOLTAGE";                                                                    // This is an Excel column header for test parameters 
        public readonly string registerName = "averagevoltage";                                                                         // This is the REST Api name

        public readonly string ExcelInputDataFile = "\\" + TestIdentifier + "__rundata.xlsx";                                                  //ihi_test_00082__rundata.xlsx
        public const string CategoryDescriptor = "Battery_Status";
        #endregion                                                                                                                      

        [Test, Category(CategoryDescriptor), Property("Test", PropertyDescriptor)]
        public void TEST_00087_Average_Voltage_Test()                                                                                    // Rename Entry
        {
            var testname = GetType().Namespace;
            video.StartRecording(videoDir, SaveMe.Always);
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + ExcelInputDataFile, 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string TestParam = runData.RunDataValue(ExcelColumnHeader).ToString();

                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(ThreadSleepTime);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(1000, webDriver))
                        {
                            // Function will change status to standby and checks Racks connection 
                            DashboardPage.Change2Standby_CheckBatteryConnection(webDriver);
                        }
                        else if ("Standby" == DashboardPage.GetCurrenteMode(6000, webDriver))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.ZoneStatusText, "Standby")));
                        }
                        
                        //Send a Rest API Put to Battery simulator with TestParam value 
                        ConsumeDCSimRest.UpdateBatteryAlarms(value: TestParam, register: "57358", label: registerName);
                        
                        //Validate the soc value on Control tab > Racks page
                        webDriver.RefreshPage(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.RackDetails), numberOfTimes: waitTime);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(ThreadSleepTime);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.RackDetails));
                        bool refresult = false;
                        string refreporttext = "";
                        TreePage.LaunchAssetById(1, Assets.racks, ref webDriver,result:ref refresult, reportText: ref refreporttext);
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsRackPopup.BatteryRackPopup));
                        System.Threading.Thread.Sleep(ThreadSleepTime);
                        string avgVoltage =  webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsRackPopup.CellAverageVoltage));
                        string expectedVoltage = runData.RunDataValue("EXPECTED_AVGVOLTAGE").ToString() + ".000";
                        if (avgVoltage == expectedVoltage)
                        {
                            _test.Pass("Avg Voltage: " + expectedVoltage + " sent thru Rest API is same in the Racks Pop-up Window Cell Section: " + TestParam, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        else
                        {
                            _test.Fail("Avg Voltage:  > Racks Pop-up Window Cell Section: " + avgVoltage + " is not equal to the registervalue sent thru Rest API " + expectedVoltage, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of UI when error occured" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }              
            }
            video.StopRecording();
        }

    }
}