using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;

namespace ihi_test_00017
{
    [TestFixture]
    public class ihi_test_00017 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("ZMQCommunicationTests"), Property("Test", "TEST-17")]
        public void TEST_00017__ZMQHeartbeatMessage()
        {
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            ushort iso_sequence = 500;
            ushort registerAddress = 0;
            //Issue write single register request for ISO_SEQ holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
            System.Threading.Thread.Sleep(2000);
            //Verify ISO Server issued ZMQ Reconnect request
            string logFile = SSH.SearchLogFilesForString("Log File Opened", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Log File Opened", logFile);
            //ZMQ Reconnect request issued from ISO Server
            logFile = SSH.SearchLogFilesForString("Command-Line: -D", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString(), logFile);
            _test.Info("Log File Shows the following message: " + logFile );
            logFile = SSH.SearchLogFilesForString("Command-Line: -l", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -l ['Logging Level'] = 0. (File:", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            //StringAssert.Contains("'ZeroMQ (ZMQ) Requester Socket' Opened on (" + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + ":1039). (File: ihi_zmq_iso.c [Line: 246])", logFile);
            //_test.Info("Log File Shows the following message: " + logFile);
            //StringAssert.Contains("New connection from ..", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            logFile = SSH.SearchLogFilesForString("modbus_receive .Socket = 1., Length = 12.   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("modbus_receive (Socket = 16, Length = 12)   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            
        }
    }
}