﻿using System;
using ihi_test_00062.Initialize;
using NUnit.Framework;
using AventStack.ExtentReports;
using ihi_testlib_espilot;
using System.Configuration;
using ihi_testlib_videorecorder;


namespace ihi_test_00062
{
    [TestFixture]
    public class ihi_test_00062 : TestInitialize
    {
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        [Test, Category("LoginScenarios"), Property("Test", "TEST-62")]
        [Video(Name = "ihi_test_00062", Mode = SaveMe.Always)]
        [Property("Requirement", "")]
        public void TEST_00062__LogoutSuccess()
        {

            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            try
            {
                SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                LoginPage.loginvalid(webDriver);
                webDriver.WaitForElement(xpath: string.Format(GlobalElementsMainPage.LoginName));
                //webDriver.WaitForElement(xpath: string.Format(GlobalElementsLoginPage.UILoadWait));
                MainPage.logout(webDriver);
                Assert.AreEqual(GlobalsGeneralStrings.homepage, webDriver.browserDriver.Url);
                if ("" == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsLoginPage.LoginFailElement)))
                {
                    Console.WriteLine("Test Pass");
                    _test.Pass("Login page shown after logout from application", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                }
                else
                {
                    _test.Fail("User was able to login with invalid credentials", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                    throw new Exception("Logout success fail");
                }
            }
            catch (Exception ex)
            {
                _test.Fail(ex);
                _test.Fail("Login name Assertion Failed", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                throw ex;
            }
            finally
            {
                SeleniumWrapper.DriverCleanup(webDriver);
            }
        }
    }
}
