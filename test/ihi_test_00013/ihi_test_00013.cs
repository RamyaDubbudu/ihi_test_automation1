using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00013
{
    [TestFixture]
    public class ihi_test_00013 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ZMQCommunicationTests"), Property("Test", "TEST-13")]
        public void TEST_00013__CreateWriteLogFiles()
        {
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            // SSH.SearchLogFilesForString("Received 'Get Site Information (600)' publication from the DC: JSON", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            short iso_sequence = 500;
            ushort registerAddress = 0;
            //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
            System.Threading.Thread.Sleep(2000);
            //Verify ISO Server issued ZMQ Reconnect request
            string logFile = SSH.SearchLogFilesForString(".ZeroMQ .ZMQ. Requester Socket. Opened on ..", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            //ZMQ Reconnect request issued from ISO Server
            logFile = SSH.SearchLogFilesForString("modbus_receive (Socket = 16, Length = 12)   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("modbus_receive (Socket = 16, Length = 12)   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500"))
            {
                _test.Pass("Log file is created with contents requested:  " + logFile);
            }
            else
            {
                _test.Fail("Log file is not created!!!");
               throw new Exception("Log file is not created!!!");
            }
            
        }
    }
}