using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00038
{
    [TestFixture]
    public class ihi_test_00038 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-38")]
        public void TEST_00038__Read_ESS_VOLTAGE_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00038__rundata.xlsx", 0);
            runData.RunRow = 0;
            short ess_Voltage = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ESS_Voltage = runData.RunDataValue("ESS_VOLTAGE");
                    ushort registerAddress = 23;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("voltage", runData_ESS_Voltage);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ess_Voltage = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_ESS_Voltage == ess_Voltage.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_Voltage == ess_Voltage.ToString(), "response Modbus message register value is equal to the Run ID" + runData_ESS_Voltage + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetVoltage");
                            StringAssert.Contains(ess_Voltage.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_VOLTAGE-" + runData_ESS_Voltage + "response Modbus message register value is equal to the Run ID" + ess_Voltage + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_VOLTAGE-" + runData_ESS_Voltage + "response Modbus message register value is not equal to the Run ID" + ess_Voltage + "column value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_VOLTAGE-" + runData_ESS_Voltage + "response Modbus message register value is not equal to the Run ID" + ess_Voltage + "column value");
                        }
                    }
                    if (scenarioType == "Invalid")// invalid Scenarios
                    {
                        if (runData_ESS_Voltage != ess_Voltage.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_Voltage != ess_Voltage.ToString(), "Run ID ESS_VOLTAGE invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Voltage + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Voltage + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Voltage + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}