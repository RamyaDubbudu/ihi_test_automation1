using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00044
{
    [TestFixture]
    public class ihi_test_00044 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
       
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-44")]
        public void TEST_00044__Read_ESS_MODE_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00044__rundata.xlsx", 0);
            runData.RunRow = 0;
            string ESS_Mode = "0";
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ESS_Mode = runData.RunDataValue("ESS_MODE");
                    ushort registerAddress = 1;
                    var scenarioType = runData.RunDataValue("Scenarios");

                    //Publish ESS_KW_NAMEPLATE data from DC Sim for the Run ID  ESS_KW_NAMEPLATE =  Run ID ESS_KW_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("mode_actual", runData_ESS_Mode);
                    System.Threading.Thread.Sleep(2000);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KW_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ESS_Mode = values[0].ToString();

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KW_NAMEPLATEis valid:   Run IDESS_KW_NAMEPLATEElse , if Run ID ESS_KW_NAMEPLATEis invalid:   equal to initial ESS_KW_NAMEPLATE
                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_ESS_Mode == ESS_Mode) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_Mode == ESS_Mode, "response Modbus message register value " + ESS_Mode + " is equal to the Run Value " + ESS_Mode + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetModeActual");
                            StringAssert.Contains(runData_ESS_Mode.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_MODE-" + ESS_Mode + "  response Modbus message register value is equal to the Run Value" + ESS_Mode + " column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_MODE-" + ESS_Mode + "  response Modbus message register value is NOT equal to the Run Value" + ESS_Mode + " column value");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual ESS_MODE-" + ESS_Mode + "  response Modbus message register value is NOT equal to the Run Value" + ESS_Mode + " column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runData_ESS_Mode != ESS_Mode) // invalid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_Mode != ESS_Mode, "Run ID ESS_MODE " + runData_ESS_Mode + " is invalid, so equal to initial ESS_Mode " + ESS_Mode + " value and does not match to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Mode + " is invalid, bec Run Value should not be equal to invalid mode number");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Mode + " is invalid, so does not equal to Run Value");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Mode + " is invalid, so does not equal to Run Value");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }


    }
}