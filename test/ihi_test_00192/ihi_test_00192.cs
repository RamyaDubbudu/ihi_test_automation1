using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;
using Newtonsoft.Json.Linq;

namespace ihi_test_00192
{
    [TestFixture]
    public class ihi_test_00192 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Inverter_Enable/Disable"), Property("Test", "TEST-192")]
        public void TEST_00192_Enable_Disable_Inverter()
        {
            //video.StartRecording(videoDir, SaveMe.Always);      
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00192__rundata.xlsx", 0);
            runData.RunRow = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string uiInverterstatus = runData.RunDataValue("UIInverterstatus");
                        string currentStatus = runData.RunDataValue("Inverterstatus");
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(1000);
                        if (uiInverterstatus == "Online")
                        {
                            // Function will check the status and change mode to Manual mode
                            DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
                            string operationalState = ConsumeDCSimRest.InverterRestResponseByName("ihi", "Device/Inverter/operational_state");
                            var jsonObj = JObject.Parse(operationalState);
                            operationalState = (string)jsonObj["value"];
                            Assert.AreEqual(currentStatus, operationalState);
                            if (webDriver.IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online"))))
                            {
                                _test.Pass("Inverter is enabled properly", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            }
                            else
                            {
                                _test.Fail("Inverter cannot be enabled", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                                throw new Exception("Inverter cannot be enabled");
                            }
                        }
                        if ("Shutdown" == uiInverterstatus)
                        {
                            DashboardPage.ChangeModeAs(webDriver, "Shutdown");
                            DashboardPage.checkChangeMode("Shutdown", 5000, webDriver);
                            webDriver.WaitForElement(xpath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Shutdown")));
                            webDriver.WaitForElement(xpath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Shutdown")));
                            string operationalState = ConsumeDCSimRest.InverterRestResponseByName("ihi", "Device/Inverter/operational_state");
                            var jsonObj = JObject.Parse(operationalState);
                            operationalState = (string)jsonObj["value"];
                            Assert.AreEqual(currentStatus, operationalState);
                            webDriver.WaitForElement(xpath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Shutdown")), numberOfTimes: waitTime);
                            if (webDriver.IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Shutdown"))))
                            {
                                _test.Pass("Inverter is Disabled properly", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            }
                            else
                            {
                                _test.Fail("Inverter cannot be disabled", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                                throw new Exception("Inverter cannot be disabled");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                         _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        SeleniumWrapper.DriverCleanup(webDriver);
                        runData.RunRow++;
                    }
                }
            }
            //video.StopRecording();
        }

    }
}