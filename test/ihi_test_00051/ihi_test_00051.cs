using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00051
{
    [TestFixture]
    public class ihi_test_00051 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-51")]
        public void TEST051_Write_ISO_ENABLE_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00051__rundata.xlsx", 0);
            runData.RunRow = 0;
            short enabled = 1;
            ushort registerAddress = 12;
            short runData_IsoEnable = short.Parse(runData.RunDataValue("ISO_ENABLE"));
            //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(enabled));
            //Send a Modbus request to the ISO slave to read the "ISO_SEQ" holding register
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            enabled = (Int16)values[0];
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(runData_IsoEnable));
                        //Send a Modbus request to the ISO slave to read the "ISO_SEQ" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        enabled = (Int16)values[0];
                        if (runData_IsoEnable.ToString() == enabled.ToString())
                        {
                            Assert.IsTrue(runData_IsoEnable.ToString() == enabled.ToString(), "response Modbus message register value " + enabled + " is equal to the Run Value" + runData_IsoEnable + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetEnabled");
                            StringAssert.Contains(enabled.ToString(), RestGetresult);
                            _test.Pass("response Modbus message register value " + enabled + " is equal to the Run Value" + runData_IsoEnable + " column value");
                        }
                        else
                        {
                            _test.Fail("response Modbus message register value " + enabled + " is not equal to the Run Value" + runData_IsoEnable + " column value");
                            throw new Exception("response Modbus message register value " + enabled + " is not equal to the Run Value" + runData_IsoEnable + " column value");
                        }
                    }

                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }

    }
}