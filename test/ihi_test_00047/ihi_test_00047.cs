using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00047
{
    [TestFixture]
    public class ihi_test_00047 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-47")]
        public void TEST_00047__Read_ESS_QMIN_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00047__rundata.xlsx", 0);
            runData.RunRow = 0;
            short q_min = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string ESS_QMIN = runData.RunDataValue("ESS_QMIN");
                    ushort registerAddress = 7;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("q_min", ESS_QMIN);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    q_min = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (ESS_QMIN == q_min.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(ESS_QMIN == q_min.ToString(), "response Modbus message register value " + q_min + " is equal to the Run ID" + ESS_QMIN + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetQMin");
                            StringAssert.Contains(ESS_QMIN.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_QMIN-" + ESS_QMIN + " Actual ESS_QMIN-" + q_min + "response Modbus message register value is equal to the Run ID" + ESS_QMIN + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_QMIN-" + ESS_QMIN + " Actual ESS_QMIN-" + q_min + "response Modbus message register value is not equal to the Run ID" + ESS_QMIN + "column value");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_QMIN-" + ESS_QMIN + " Actual ESS_QMIN-" + q_min + "response Modbus message register value is not equal to the Run ID" + ESS_QMIN + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (ESS_QMIN != q_min.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(ESS_QMIN != q_min.ToString(), "Run ID ESS_QMIN invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_QMIN + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_QMIN + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_QMIN + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }


    }
}