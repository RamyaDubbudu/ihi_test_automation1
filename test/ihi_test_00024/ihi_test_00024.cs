using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_videorecorder;
using ihi_testlib_modbus;

namespace ihi_test_00024
{
    [TestFixture]
    public class ihi_test_00024 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-24")]
        public void TEST_00024__Read_ESS_PMIN_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00024__rundata.xlsx", 0);
            runData.RunRow = 0;
            short p_Min = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_Ess_Pmin = runData.RunDataValue("ESS_PMIN");
                    ushort registerAddress = 5;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("p_min", runData_Ess_Pmin);

                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    p_Min = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_Ess_Pmin == p_Min.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_Ess_Pmin == p_Min.ToString(), "response Modbus message register value " + p_Min + " is equal to the Run value " + runData_Ess_Pmin + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetPMin");
                            StringAssert.Contains(runData_Ess_Pmin.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_PMIN-" + runData_Ess_Pmin + " Actual ESS_PMIN-" + p_Min + "response Modbus message register value is equal to the Run ID" + runData_Ess_Pmin + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_PMIN-" + runData_Ess_Pmin + " Actual ESS_PMIN-" + p_Min + "response Modbus message register value is not equal to the Run ID" + runData_Ess_Pmin + "column value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_PMIN-" + runData_Ess_Pmin + " Actual ESS_PMIN-" + p_Min + "response Modbus message register value is not equal to the Run ID" + runData_Ess_Pmin + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runData_Ess_Pmin != p_Min.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(runData_Ess_Pmin != p_Min.ToString(), "Run ID ESS_PMIN " + runData_Ess_Pmin + " invalid: so does not equal to Run Value " + p_Min);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Ess_Pmin + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Ess_Pmin + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Ess_Pmin + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }

    }
}