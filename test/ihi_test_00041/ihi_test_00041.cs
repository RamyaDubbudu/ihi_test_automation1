using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00041
{
    [TestFixture]
    public class ihi_test_00041 : TestInitialize
    {

        #region Variables 
       //SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
       
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-41")]
        public void TEST_00041__Read_ESS_PMAX_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00041__rundata.xlsx", 0);
            runData.RunRow = 0;
            short p_Max = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string ESS_PMAX = runData.RunDataValue("ESS_PMAX");
                    ushort registerAddress = 4;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                   ConsumeDCSimRest.UpdateSiteData("p_max", ESS_PMAX);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    p_Max = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (ESS_PMAX == p_Max.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(ESS_PMAX == p_Max.ToString(), "response Modbus message register value " + p_Max + " is equal to the Run ID" + ESS_PMAX + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetPMax");
                            StringAssert.Contains(ESS_PMAX.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_PMAX-" + ESS_PMAX + " Actual ESS_PMAX-" + p_Max + "response Modbus message register value is equal to the Run ID" + ESS_PMAX + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_PMAX-" + ESS_PMAX + " Actual ESS_PMAX-" + p_Max + "response Modbus message register value is not equal to the Run ID" + ESS_PMAX + "column value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_PMAX-" + ESS_PMAX + " Actual ESS_PMAX-" + p_Max + "response Modbus message register value is not equal to the Run ID" + ESS_PMAX + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (ESS_PMAX != p_Max.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(ESS_PMAX != p_Max.ToString(), "Run ID ESS_PMAX invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_PMAX + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_PMAX + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_PMAX + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                        }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }

    }
}