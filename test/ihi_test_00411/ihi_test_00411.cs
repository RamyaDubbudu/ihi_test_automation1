using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;
using System.Collections.Generic;

namespace ihi_test_00411
{
    [TestFixture]
    public class ihi_test_00411 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Inverter_Warnings"), Property("Test", "TEST-411")]
        public void TEST_00411_Trigger_InverterWarnings()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            //Login to ESPilot 
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00411__rundata.xlsx", 0);
            runData.RunRow = 0;
            string warningDesription = string.Empty;
            string registerValue = "";
            int waitTimetoset = 0;
            SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
            LoginPage.loginvalid(webDriver);
            webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
            ConsumeDCSimRest.UpdateInverterDeviceType("voltage_dc", "0");
            if ("Shutdown" == DashboardPage.GetCurrenteMode(1000, webDriver))
            {
                // Function will check the status and change mode to Manual mode
                DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
            }
            else if ("Online" == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalDashbaordInverterDetails.InverterStatus)))
            {
                webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
            }
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        ConsumeDCSimRest.UpdateInverterDeviceType("voltage_dc", "0");
                        registerValue = runData.RunDataValue("Value");
                        waitTimetoset = Convert.ToInt32(runData.RunDataValue("Waittimetoshow"));
                        warningDesription = runData.RunDataValue("Description").ToString();
                        ConsumeDCSimRest.UpdateInverterWarnings(registerValue);
                        System.Threading.Thread.Sleep(8000);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.EventsTab));
                        List<EventsPage.EventsInfo> eventInfo = EventsPage.GetAllEventsInfo(webDriver);
                        int tryCount = 0;
                        do
                        {
                          eventInfo = EventsPage.GetAllEventsInfo(webDriver);
                            if (eventInfo[0].EventDescription.Contains(warningDesription))
                            {
                                break;
                            }
                            else
                            {
                                System.Threading.Thread.Sleep(2000);
                                tryCount++;
                            }
                        } while (!eventInfo[0].EventDescription.Contains(warningDesription) || tryCount == 15);
                        string eventDescription = eventInfo[0].EventDescription;
                        if (eventInfo.Count != 0)
                        {
                            if (eventDescription.Contains(warningDesription))
                            {
                                _test.Pass("Alarm is shown in the Alarms Page with the following description:" + eventDescription + " Matching to the Rest setting alarm as:  " + warningDesription, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            }
                            else
                            {
                                _test.Fail("Alarm is not shown in the Alarms Page with the following description: " + eventDescription + " Matching to the Rest setting alarm as: " + warningDesription, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            }
                        }
                        else
                        {
                            _test.Fail("Alarm is not shown in the Alarms Page as: " + runData.RunDataValue("Description").ToString(), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        //                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.EventsTab));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEvents.AcknowledgeAllButton));
                        System.Threading.Thread.Sleep(2000);
                        _test.Info("Warning is Acknowledged:" + warningDesription, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsEvents.DismissAllButton));
                        System.Threading.Thread.Sleep(2000);
                        _test.Info("Warning is cleared:" + warningDesription, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        runData.RunRow++;
                    }
                }
            }
            SeleniumWrapper.DriverCleanup(webDriver);
            //video.StopRecording();
        }

    }
}