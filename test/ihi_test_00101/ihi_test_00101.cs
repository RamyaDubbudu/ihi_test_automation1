using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;

namespace ihi_test_00101
{
    [TestFixture]
    public class ihi_test_00101 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("BatteryContactors_For2DayTimePeriod"), Property("Test", "TEST-101")]
        public void TEST_00101_Battery_OpenCloseContactors_RunFor_2Days()
        {
            int runTest_noOfTimes = 5;
            for (int i = 0; i < runTest_noOfTimes; i++)
            {
                try
                {
                    //Send a Rest API Put to inverter simulator with Actual Power P value
                    SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                    //Login to ESPilot
                    LoginPage.loginvalid(webDriver);
                    webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                    System.Threading.Thread.Sleep(5000);
                    if ("Shutdown" == DashboardPage.GetCurrenteMode(1000, webDriver))
                    {
                        DashboardPage.ChangeModeAs(webDriver, "Standby");
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.CurrentModeText, "Standby"), numberOfTimes: waitTime);
                        var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                        //When system is in Standby, Racks are Green then Contacters should be closed state 
                        string logFile = ihi_testlib_espilot.SSH.SearchLogFilesForString("Contactor Close Request started", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, filePath: "decs_batteryd.log");
                        if (logFile.Contains("Contactor Close Request started"))
                        {
                            _test.Pass("Log file is created with contents requested:  " + logFile);
                        }
                        else
                        {
                            _test.Fail("Log file is not shown with the required text Contactor Close Request started!!!");
                            throw new Exception("Log file is not shown with the required text Contactor Close Request started!!!");
                        }
                    }
                    if ("Standby" == DashboardPage.GetCurrenteMode(1000, webDriver))
                    {
                        DashboardPage.ChangeModeAs(webDriver, "Shutdown");
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.CurrentModeText, "Shutdown"), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(15000);
                        string logFile = ihi_testlib_espilot.SSH.SearchLogFilesForString("Contactor Open Request started", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, filePath: "decs_batteryd.log");
                        if (logFile.Contains("Contactor Open Request started"))
                        {
                            _test.Pass("Log file is created with contents requested:  " + logFile);
                        }
                        else
                        {
                            _test.Fail("Log file is not shown with the required text Contactor Open Request started!!!");
                            throw new Exception("Log file is not shown with the required text Contactor Open Request started!!!");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail("Current state of UI when error occured" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                    Assert.Fail("Failed" + ex);
                }
                finally
                {
                    SeleniumWrapper.DriverCleanup(webDriver);
                }
            }
        }

    }
}