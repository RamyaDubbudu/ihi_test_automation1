﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ihi_testlib_videorecorder;
using ihi_testlib_lishenbamsWinApp;
using ihi_testlib_modbusClientWinApp;
using OpenQA.Selenium.Appium.Windows;

namespace ihi_test_00231
{
    [SetUpFixture]
    public abstract class TestInitialize
    {

        #region Variables 
       // public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public static ExtentReports _extent = new ExtentReports();
        public ExtentTest _test;
        public static ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
        public string ScreenCapturePath;
        public VideoAttribute video = new VideoAttribute();
        public string videoDir;
        Process winAppDriverProcess = new Process();
        public ModbusSession modbusSession = new ModbusSession();
        public ModbusFunctions modbusFunc = new ModbusFunctions();
        public BAMSSession bamsSession = new BAMSSession();
        public BAMSFunctions bamsFunc = new BAMSFunctions();
        public static WindowsDriver<WindowsElement> powerLifeSession;
        public static WindowsDriver<WindowsElement> modClientSession;
        #endregion



        [OneTimeSetUp]
        protected void Setup()
        {
            var testname = GetType().Namespace;
            string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
            videoDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\" + testname;
            var reportDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            Directory.CreateDirectory(reportDir);
            var fileName = GetType().Namespace + ".html";
            var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
            htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
            htmlReporter.AppendExisting = true;
            _extent.AttachReporter(htmlReporter);
            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
            winAppDriverProcess.StartInfo.FileName = ConfigurationManager.AppSettings["CMDPath"].ToString();
            winAppDriverProcess.StartInfo.WorkingDirectory = ConfigurationManager.AppSettings["WinAppDriver"].ToString();
            winAppDriverProcess.StartInfo.Arguments = @"/K WinAppDriver.exe " + ConfigurationManager.AppSettings["WinAppDriverPort"].ToString();
            winAppDriverProcess.Start();
            powerLifeSession = bamsSession.LaunchLishenBAMSSession(powerLifeSession);
            bamsFunc.LoginBAMS(powerLifeSession, _test);
          
        }

        [OneTimeTearDown]
        protected void TearDown()
        {
            bamsSession.CloseBAMSSession(powerLifeSession);
            modbusSession.CloseModbusSession(modClientSession);
            winAppDriverProcess.Kill();

        }



        public String TakesScreenshot(WindowsDriver<WindowsElement> sessionName)
        {
            var testname = GetType().Namespace;
            ScreenCapturePath = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            System.IO.Directory.CreateDirectory(ScreenCapturePath);
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");
            TimeAndDate.Replace(" ", "_");
            string imageName = TestContext.CurrentContext.Test.Name + TimeAndDate.ToString();
            sessionName.GetScreenshot().SaveAsFile(ScreenCapturePath + "_" + imageName + "." + System.Drawing.Imaging.ImageFormat.Jpeg);
            return ScreenCapturePath + "_" + imageName + "." + "jpeg";
        }

        [SetUp]
        public void BeforeTest()
        {
            
            _test.AssignCategory(this.GetType().ToString());
            _test.AssignAuthor("Ramya Dubbudu");
        }

        [TearDown]
        public void AfterTest()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }
            _test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
            _extent.Flush();

        }
    }
}