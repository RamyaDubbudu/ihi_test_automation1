﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using ihi_testlib_lishenbamsWinApp;
using ihi_testlib_modbusClientWinApp;
using AventStack.ExtentReports;

namespace ihi_test_00231
{
    [TestFixture]
    public class ScenarioEditor : TestInitialize
    {

        [Test]
        public void TC_00231_UnderCellTemperatureProtectionFault()
        {
            ModbusSession modbusSession = new ModbusSession();
            BAMSSession bamsSession = new BAMSSession();
            _test.Info("Login Successful to PowerLife", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(powerLifeSession)).Build());
            Assert.AreEqual("PowerLife201", powerLifeSession.Title);
            BAMSFunctions.ThresholdInfo thresholdvalues = bamsFunc.ThresholdViewerByName(powerLifeSession, "Under cell temperature", _test);
            string curminTemp = bamsFunc.BAMSInterface(powerLifeSession, _test);
            modClientSession = modbusSession.LaunchModbusSession(modClientSession);
            string outputFromModbus = modbusFunc.ReadOutputFromModbus(modClientSession, "57351");
            _test.Info("Opened Modbus Client and getting value from the register address: ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(modClientSession)).Build());
            StringAssert.Contains("Rx: 00 01 00 00 00 05 01 03 02 00 00", outputFromModbus);
            bamsFunc.WarningSet(powerLifeSession, "20", _test);
            string newoutput = modbusFunc.ReadOutputFromModbus(modClientSession, "57351");
            if (outputFromModbus.Contains("Rx: 00 01 00 00 00 05 01 03 02 00 01"))
                _test.Pass("Modbus register shows the expected output : ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(modClientSession)).Build());
            else
            {
                Assert.Fail();
                _test.Fail("Modbus register shows the expected output : ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(modClientSession)).Build());
            }
        }

    }
}
