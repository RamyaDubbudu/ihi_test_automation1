using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00043
{
    [TestFixture]
    public class ihi_test_00043 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-43")]
        public void TEST_00043__Write_ISO_DEMAND_Q_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00043__rundata.xlsx", 0);
            runData.RunRow = 0;
            short iSO_Demand_Q = 500;
            short q_min = -32768;
            short q_max = 32767;
            ConsumeDCSimRest.UpdateSiteData("q_min", q_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("q_max", q_max.ToString());
            ushort registerAddress = 2;
            //Send a Modbus request to the ISO slave to write the "ISO_DEMAND_Q" holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iSO_Demand_Q));
            //Send a Modbus request to the ISO slave to read the "ISO_DEMAND_P" holding register
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            iSO_Demand_Q = (Int16)values[0];
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                        short runData_Iso_DemandQ = short.Parse(runData.RunDataValue("ISO_DEMAND_Q"));
                        string RID_q_min = runData.RunDataValue("ESS_QMIN");
                        string RID_q_max = runData.RunDataValue("ESS_QMAX");
                        //Publish ESS_QMIN and ESS_QMAX data from DC Sim for the Run ID	"ESS_QMIN = Run IDESS_PMINESS_PMAX =Run ID ESS_QMAX"
                        ConsumeDCSimRest.UpdateSiteData("q_min", RID_q_min);
                        ConsumeDCSimRest.UpdateSiteData("q_max", RID_q_max);
                        //Send a Modbus to the ISO server to read the input registers (ESS_QMAX, ESS_QMIN)
                        values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, 6, 2);
                        q_max = (Int16)values[0];
                        q_min = (Int16)values[1];
                        //Ensure response Modbus message has Run ID ESS_QMIN and ESS_QMAX column values
                        Assert.IsTrue(RID_q_min == q_min.ToString(), "response Modbus message register value " + q_min + " is equal to the Run Value" + RID_q_min + " column value");
                        Assert.IsTrue(RID_q_max == q_max.ToString(), "response Modbus message register value p_max " + q_max + " is equal to the Run Value RID_p_max " + RID_q_max + " row " + runData.RunRow + " column value");
                        //Send a Modbus request to the ISO slave to write the "ISO_DEMAND_Q" holding register            
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, (UInt16)runData_Iso_DemandQ);
                        //Send a Modbus request to the ISO slave to read the "ISO_DEMAND_Q" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iSO_Demand_Q = (Int16)values[0];
                        if (iSO_Demand_Q.ToString() == runData.RunDataValue("ISO_DEMAND_Q_EXPECTED"))
                        {
                            Assert.IsTrue(iSO_Demand_Q.ToString() == runData.RunDataValue("ISO_DEMAND_Q_EXPECTED"), "response Modbus message register value " + iSO_Demand_Q + " is equal to the Run Value" + runData.RunDataValue("ISO_DEMAND_Q_EXPECTED") + " row " + runData.RunRow + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetQDemand");
                            StringAssert.Contains(iSO_Demand_Q.ToString(), RestGetresult);
                            _test.Pass("response Modbus message register value " + iSO_Demand_Q + " is equal to the Run Value" + runData_Iso_DemandQ + " column value");
                        }
                        else
                        {
                            _test.Fail("response Modbus message register value " + iSO_Demand_Q + " is not equal to the Run Value" + runData_Iso_DemandQ + " column value");
                            throw new Exception("response Modbus message register value " + iSO_Demand_Q + " is not equal to the Run Value" + runData_Iso_DemandQ + " column value");
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }

    }
}