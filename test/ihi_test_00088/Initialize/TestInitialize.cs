﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using ihi_testlib_espilot;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_test_00088
{
    [SetUpFixture]
    public abstract class TestInitialize
    {

        #region Variables 
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDirectory);
        public static ExtentReports _extent = new ExtentReports();
        protected ExtentTest _test;
        public static ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
        public VideoAttribute video = new VideoAttribute();
        public string videoDir;
        public string S3BucketUploadDir;
        private static string keyName = "qa/" + System.Environment.UserName + "/" + ConfigurationManager.AppSettings["Component"].ToString() + "/" + DateTime.Today.ToString("yyyyMMdd");
        public static string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
        public static int waitTime = Convert.ToInt16(ConfigurationManager.AppSettings["ImplicitWaits"].ToString());
        #endregion

        [SetUp]
        protected void Setup()
        {
            var testname = GetType().Namespace;
            ihi_launch_simulator.LaunchSimulator.CleanIIS();
            videoDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\" + testname;
            var reportDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            Directory.CreateDirectory(reportDir);
            S3BucketUploadDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname;
            keyName = keyName + "/" + testname;
            var fileName = GetType().Namespace + ".html";
            var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
            htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
            htmlReporter.AppendExisting = true;
            _extent.AttachReporter(htmlReporter);
            //Stoping ESPilot DECS
            ihi_testlib_espilot.SSH.StopDecs(sshInfo);
            System.Threading.Thread.Sleep(5000);
            ////Starting the Inverter simulator AND Battery simulator
            //Opening the web application of inverter and battery simulator to open the connection
            ihi_launch_simulator.LaunchSimulator.RunSimulator(ConfigurationManager.AppSettings["invertersimPjtPath"].ToString(), ConfigurationManager.AppSettings["InverterPort"].ToString());
            ihi_launch_simulator.LaunchSimulator.RunSimulator(ConfigurationManager.AppSettings["LishenBatterySimPjtPath"].ToString(), ConfigurationManager.AppSettings["BatteryPort"].ToString());
            // Start ESPilot DECS
            ihi_testlib_espilot.SSHAndStartDECSDirectory decsInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(sshInfo, GlobalVariables.DECSDirectory);
            var startDECSSuccess = ihi_testlib_espilot.SSH.StartDecs(ihi_testlib_espilot.DECSLaunchType.Normal, decsInfo, 10000, "");
            System.Threading.Thread.Sleep(GlobalVariables.TimingConstants.DECSLaunch);
            // Making sure Battery and Inverter simulators are connected to ES/Pilot 
            ConsumeDCSimRest.Connection2_BatterySimulator();
            ConsumeDCSimRest.Connection2_InverterSimulator();
        }

        [OneTimeTearDown]
        protected void TearDown()
        {
            _extent.Flush();
            //stop IIS EXPRESS 
            ihi_launch_simulator.LaunchSimulator.CleanIIS();
        }

        public String TakesScreenshot(ihi_testlib_selenium.Selenium driver, string FileName)
        {
            var testname = GetType().Namespace;
            string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
            string path = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            System.IO.Directory.CreateDirectory(path);
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");
            TimeAndDate.Replace(" ", "_");
            string imageName = TestContext.CurrentContext.Test.Name + FileName + TimeAndDate.ToString();
            ((ITakesScreenshot)driver.browserDriver).GetScreenshot().SaveAsFile(path + "_" + imageName + "." + System.Drawing.Imaging.ImageFormat.Jpeg);
            return path + "_" + imageName + "." + "jpeg";
        }

        [SetUp]
        public void BeforeTest()
        {
            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
            _test.AssignCategory(this.GetType().ToString());
            _test.AssignAuthor("Ramya Dubbudu");
        }

        [TearDown]
        public void AfterTest()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }
            _test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
            _extent.Flush();

        }
    }
}