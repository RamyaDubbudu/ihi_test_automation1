using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;


namespace ihi_test_00207
{
    [TestFixture]
    public class ihi_test_00207 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Category("NoIP_Modbus"), Property("Test", "TEST-207")]
        public void TEST_00207__InitializeNoModbusIP()
        {
            var ssh = new SshClient(connectionInfo:SSH.connectionInfo);
            string killisomb = string.Empty;
            string resetisomb = string.Empty;
            ssh.Connect();
            if (ConfigurationManager.AppSettings["IsDocker"].ToString() == "True")
            {
                killisomb = string.Format(ihi_testlib_espilot.DockerCommands.SSHCommands.Killisomb);
                resetisomb = "sudo docker exec -id decs-dc bash -c \"cd /usr/local/ihi-decs/bin&&echo decs |./decs_isomb -M\"";
                SSH.OpenWrite(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                SSH.OpenWrite(ssh, resetisomb);
                System.Threading.Thread.Sleep(3000);
            }
            else
            {
                killisomb = "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_isomb";
                resetisomb = "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_isomb -M";
                ihi_testlib_espilot.SSH.ExecCommand(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                ihi_testlib_espilot.SSH.ExecCommand(ssh, resetisomb);
                System.Threading.Thread.Sleep(1000);
            }
            string logFile = SSH.SearchLogFilesForString("Command-Line Error: -M .'ISO Master - IP Address'., but no value was specified.", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Command-Line Error: -M ['ISO Master - IP Address'], but no value was specified."))
            {
                _test.Pass("Log file is shown with the error for ISO Master missing IP Address:  " + logFile);
            }
            else
            {
                _test.Fail("Log file is not shown with the error for ISO Master missing IP Address!!! " + logFile);
                throw new Exception("Log file is not shown with the error for ISO Master missing IP Address!!!!!!");
            }
                   
        }
    }
}