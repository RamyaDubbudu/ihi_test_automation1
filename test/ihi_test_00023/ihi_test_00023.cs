using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;
using System.Configuration;

namespace ihi_test_00023
{
    [TestFixture]
    public class ihi_test_00023 : TestInitialize
    {

        #region Variables 
        SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("ZMQCommunicationTests"), Property("Test", "TEST-23")]
        public void TEST_00023__UnsupportedModbusRequest()
        {
            var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            ushort iso_sequence = 500;
            ushort registerAddress = 0;
            ushort invalidRegisterAddress = 25;
            //Issue write single register request for ISO_SEQ holding register
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
            System.Threading.Thread.Sleep(2000);
            //Verify ISO Server issued ZMQ Reconnect request
            string logFile = SSH.SearchLogFilesForString("Log File Opened", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Log File Opened", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            //ZMQ Reconnect request issued from ISO Server 
            logFile = SSH.SearchLogFilesForString("Command-Line: -D", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -D ['Domain Controller(DC) - IP Address'] = " + ConfigurationManager.AppSettings["DCIpAddress"].ToString(), logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            logFile = SSH.SearchLogFilesForString("Command-Line: -l", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("Command-Line: -l ['Logging Level'] = 0. (File:", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            logFile = SSH.SearchLogFilesForString(".ZeroMQ .ZMQ. Requester Socket. Opened on", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("'ZeroMQ (ZMQ) Requester Socket' Opened on (" + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + ":1039).", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            logFile = SSH.SearchLogFilesForString("New connection from ..", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("New connection from", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            //Verify log file contains entry that indicates Reply Message.
            logFile = SSH.SearchLogFilesForString("modbus_receive .Socket = 1., Length = 12.   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            StringAssert.Contains("modbus_receive (Socket = 16, Length = 12)   0   1   0   0   0   6   0   6   0   0   1 244  ISO_SEQ = 500", logFile);
            _test.Info("Log File Shows the following message: " + logFile);
            //Issue invalid request from Modbus client
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, invalidRegisterAddress, Convert.ToUInt16(iso_sequence));
            logFile = SSH.SearchLogFilesForString("Warning: Unknown .Holding Register. ." + invalidRegisterAddress + ".", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Warning: Unknown 'Holding Register' [" + invalidRegisterAddress + "]"))
            {
                _test.Pass("Log entry generated indicating warning that user is requesting to update an unknown holding register: " + logFile);
            }
            else
            {
                _test.Fail("Warning for Unknown holding register request is not shown in log file");
                throw new Exception("Warning for Unknown holding register request is not shown in log file");
            }

            
        }
    }
}