using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using Newtonsoft.Json.Linq;
using ihi_testlib_videorecorder;
using System.Collections.Generic;

namespace ihi_test_00256
{
    [TestFixture]
    public class ihi_test_00256 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Capacity Test with ES/Pilot SOCMaintenance"), Property("Test", "TEST-256")]
        public void TEST_00256_CapacityTest_withESPilot_in_SOCMaintenanceMode()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00256__rundata.xlsx", 0);
            runData.RunRow = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        //Step 1: Login to ESPilot
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        LoginPage.loginvalid(webDriver);
                        string preferredMode = runData.RunDataValue("ModeType");
                        string socTargetCharge = runData.RunDataValue("ChargeSOCTarget");
                        string socTargetDisCharge = runData.RunDataValue("DischargeSOCTarget");
                        int waitTimeBetween = Convert.ToInt16(runData.RunDataValue("WaitTime"));
                        //Step 2: Navigate to Dashboard page
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        
                        DashboardPage.ChangeModeAs(webDriver, "Shutdown");
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(5000, webDriver))
                        {
                            // Step 3: Pilot to SOC Maintenance mode
                            DashboardPage.ChangeModeAs(webDriver, preferredMode);
                            DashboardPage.checkChangeMode(preferredMode, 10, webDriver);
                        }
                        else if (preferredMode == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalDashbaordInverterDetails.InverterStatus)))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, preferredMode)));
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, preferredMode)));
                        }
                        System.Threading.Thread.Sleep(2000);

                        // Step 4: Get Nameplate reading from Site table from column "kw_nameplate"
                        var ssh = new SshClient(connectionInfo: ihi_testlib_espilot.SSH.connectionInfo);
                        string kw_nameplateInitialValue = "350"; // ihi_testlib_espilot.SSH.ExecuteSqlite(ssh, "SELECT kw_nameplate FROM site", "dc");

                        // Step 5: Enter SOC % = 100 and pmax = kw_nameplate and click on Apply
                        webDriver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.qField), textToAdd: socTargetCharge);
                        webDriver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.pField), textToAdd: kw_nameplateInitialValue);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.Apply));
                        string socnow = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsPowerEnergyChart.SOCSliderLabelText, socTargetDisCharge));
                        string socavailable = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.AvailableSOC));
                        do
                        {
                          System.Threading.Thread.Sleep(5000);
                        } while(webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.AvailableSOC)) != socTargetCharge + ".0");
                        double socChargeTargetReach = Convert.ToDouble(webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.AvailableSOC)));
                        Assert.AreEqual(Convert.ToDouble(socTargetCharge + ".0"), socChargeTargetReach, 5);
                        if(SOCTolerance(Convert.ToDouble(socTargetCharge + ".0"), socChargeTargetReach, 5))
                        {
                            _test.Pass(socChargeTargetReach + " SOC Charging Target is within the tolerance level of what was asked for :" + socTargetCharge + ".0", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        else
                        {
                            _test.Fail(socChargeTargetReach + " SOC Charging Target is not within the tolerance level of what was asked for :" + socTargetCharge + ".0", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        }
                        // Step 6: Put Pilot on Standby mode and wait for 30 min when system reaches 100% SOC without Alarms 
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsMainPage.AlarmsTab), numberOfTimes: waitTime);
                        AlarmsPage.AcknowledgeAndDismiss(webDriver);
                        int alarmCount = AlarmsPage.CountNumberofalarms(webDriver);
                        if (alarmCount == 0)
                        {
                            DashboardPage.ChangeModeAs(webDriver, "Standby");
                            System.Threading.Thread.Sleep(waitTime); //wait 30 min on standby
                        }
                        // Step 7: Put Pilot again in SOC Maintenance Mode
                        DashboardPage.ChangeModeAs(webDriver, preferredMode);
                        DashboardPage.checkChangeMode(preferredMode, 10, webDriver);

                        //Step 8: Enter SOC = 0% and Pmax = KW_Nameplate and Click Apply
                        webDriver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.qField), textToAdd: socTargetDisCharge);
                        webDriver.AddTextToTextbox(XPath: string.Format(GlobalStartupShutdown.pField), textToAdd: kw_nameplateInitialValue);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalStartupShutdown.Apply));
                        do
                        {                        
                                System.Threading.Thread.Sleep(5000);
                        } while (webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.AvailableSOC)) != socTargetDisCharge + ".0");
                        double socDischargeTargetReach = Convert.ToDouble(webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.AvailableSOC)));
                        Assert.AreEqual(Convert.ToDouble(socTargetDisCharge + ".0"), socDischargeTargetReach, 5);
                        if (SOCTolerance(Convert.ToDouble(socTargetDisCharge + ".0"), socDischargeTargetReach, 5))
                        {
                            _test.Pass(socDischargeTargetReach + " SOC Discharge Target is within the tolerance level of what was asked for :" + socTargetDisCharge + ".0", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        else
                        {
                            _test.Fail(socDischargeTargetReach + " SOC Discharge Target is not within the tolerance level of what was asked for :" + socTargetDisCharge + ".0", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {

                        SeleniumWrapper.DriverCleanup(webDriver);
                        runData.RunRow++;
                    }

                    //video.StopRecording();
                }
            }
        }


        public static bool SOCTolerance(double value1, double value2, double acceptableDifference)
        {
            return Math.Abs(value1 - value2) <= acceptableDifference;
        }
    }
}