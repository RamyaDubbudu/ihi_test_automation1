﻿using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Reflection;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using ihi_testlib_videorecorder;
using ihi_testlib_modbus;
using Renci.SshNet;

namespace ihi_test_00060.Initialize
{
    [SetUpFixture]
    public abstract class TestInitialize
    {
        #region Variables 
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        ihi_testlib_modbus.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_modbus.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDirectory);
        public static ExtentReports _extent = new ExtentReports();
        protected ExtentTest _test;
        public string videoDir = TestContext.CurrentContext.TestDirectory + "\\VideoRecordings\\";
        public static ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
        public VideoAttribute videoRecording = new VideoAttribute();

        #endregion
        [OneTimeSetUp]
        protected void Setup()
        {
            var testName = GetType().Namespace;
            CleanIIS();
            string PjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
            var reportDir = PjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString();
            Directory.CreateDirectory(reportDir);
            var fileName = testName + ".html";
            var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
            htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
            htmlReporter.AppendExisting = true;
            _extent.AttachReporter(htmlReporter);
            ////Stoping ESPilot DECS
            //var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            //ihi_testlib_modbus.SSH.StopDecs(sshInfo);
            //System.Threading.Thread.Sleep(5000);
            //// Start ESPilot DECS
            //ihi_testlib_modbus.SSHAndStartDECSDirectory decsInfo = new ihi_testlib_modbus.SSHAndStartDECSDirectory(sshInfo, GlobalVariables.DECSDirectory);
            //var startDECSSuccess = ihi_testlib_modbus.SSH.StartDecs(ihi_testlib_modbus.SSH.DECSLaunchType.Normal, decsInfo, 10000, "");
            //System.Threading.Thread.Sleep(GlobalVariables.TimingConstants.DECSLaunch);
        }

        [OneTimeTearDown]
        protected void TearDown()
        {
            _extent.Flush();
            //stop IIS EXPRESS 
            CleanIIS();
        }

        public static void CleanIIS()
        {
            Array.ForEach(Process.GetProcessesByName("iisexpress"), x => x.Kill());
            Array.ForEach(Process.GetProcessesByName("cmd"), x => x.Kill());
            Array.ForEach(Process.GetProcessesByName("chromedriver"), x => x.Kill());
            Array.ForEach(Process.GetProcessesByName("chrome"), x => x.Kill());
        }

        public static String TakesScreenshot(ihi_testlib_selenium.Selenium driver, string FileName)
        {

            string path = ConfigurationManager.AppSettings["AutomationDiretory"].ToString() + "\\XReports\\TestOutputImages\\";
            System.IO.Directory.CreateDirectory(path);
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");
            TimeAndDate.Replace(" ", "_");

            string imageName = TestContext.CurrentContext.Test.Name + FileName + TimeAndDate.ToString();

            ((ITakesScreenshot)driver.browserDriver).GetScreenshot().SaveAsFile(path + "_" + imageName + "." + System.Drawing.Imaging.ImageFormat.Jpeg);

            return path + "_" + imageName + "." + "jpeg";
        }

        [SetUp]
        public void BeforeTest()
        {

            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
            _test.AssignCategory(this.GetType().ToString());
            _test.AssignAuthor("Ramya Dubbudu");
        }

        [TearDown]
        public void AfterTest()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }

            _test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
            _extent.Flush();
            videoRecording.StopRecording();
        }
    }
}
