﻿using System;
using ihi_test_00060.Initialize;
using NUnit.Framework;
using AventStack.ExtentReports;
using ihi_testlib_espilot;
using System.Configuration;
using ihi_testlib_videorecorder;
using ihi_testlib_modbus;

namespace ihi_test_00060
{
    [TestFixture]
    public class ihi_test_00060 : TestInitialize
    {
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        [Test, Category("LoginScenarios")]
        [Property("Test", "TEST-60")]
        public void TEST_00060__CreateNewUser_Edit_Delete()
        {
            var testName = GetType().Namespace;
            videoRecording.StartRecording(testName, SaveMe.Always);
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00060__rundata.xlsx", 0);
            //skip test if runTest is false;
            for (int k = 0; k < runData.RowCount; k++)
            {
                if (runData.MetaData[runData.RunRow].runTest == "False")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        var userName = runData.RunDataValue("userName");
                        var fullName = runData.RunDataValue("fullName");
                        var email = runData.RunDataValue("email");
                        var permissionLevel = runData.RunDataValue("permissionLevel");
                        var password = runData.RunDataValue("password");
                        var editPassword = runData.RunDataValue("editPassword");

                        //Step 1: Launches website and logs in as admin
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForEnabledElement(xPath: string.Format(GlobalElementsMainPage.LogoutButton), numberOfTimes: 20);
                        //System.Threading.Thread.Sleep(TimingConstants.UILoadWait);
                        //Step 2: Navigates to Admin tab
                        MainPage.navigateToAdmin(webDriver);
                        var mainWindowName = webDriver.browserDriver.Title;
                        //Step 3 Clicks on add new user button and waits for window to open
                        Userpage.ClickAddNewUser(webDriver);
                        webDriver.SwitchtoWindow(webDriver, "ES/Pilot - Add User");

                        //Step 4: Enters new users info
                        AddUserWindow.AddUser(userName, fullName, email, permissionLevel, password, password, webDriver);
                        webDriver.SwitchtoWindow(webDriver, mainWindowName);
                        System.Threading.Thread.Sleep(3000);
                        //Step 5: Verifies new users appears in user list
                        GeneralDECS.CheckUIUpdate(5000, webDriver);
                        MainPage.navigateToAdmin(webDriver);
                        if (!Userpage.FindUserInList(userName, fullName, email, permissionLevel, webDriver))
                        {
                            _test.Fail(string.Format("User was not found in user list after added. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                    userName, fullName, email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception();
                        }
                        _test.Pass(string.Format("User was found in user list after added. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                    userName, fullName, email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        //Step 6: Logs Out
                        MainPage.logout(webDriver);

                        //Step 7: Logs in under new user name
                        var loginSuccessful = LoginPage.login(userName, password, webDriver);
                        System.Threading.Thread.Sleep(TimingConstants.UILoadWait);
                        if (!loginSuccessful)
                        {
                            _test.Fail(string.Format("Could not login as new user. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                    userName, fullName, email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception();
                        }
                        _test.Pass(string.Format("Could login successfully as new user. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                   userName, fullName, email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        //Step 8: Verifies name matches expected value
                        System.Threading.Thread.Sleep(3000);
                        var showName = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.LoginName));
                        //this also gets text "Logout" from the logout button
                        if (showName != fullName)
                        {
                            _test.Fail(string.Format("Name on login does not match. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}. Value shown on UI: {5}.",
                                    userName, fullName, email, permissionLevel, password, showName), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception();
                        }
                        _test.Pass(string.Format("Name on login match. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}. Value shown on UI: {5}.",
                                   userName, fullName, email, permissionLevel, password, showName), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        //Step 9: Navigates to Admin tab
                        //GeneralDECS.CheckUIUpdate(2000, webDriver);
                        MainPage.navigateToAdmin(webDriver);

                        //Step 10: Edit user's name to verify they are editable

                        Userpage.ClickEditUser(userName, fullName, email, permissionLevel, webDriver);
                        if (editPassword.ToLower() == "yes")
                        {
                            password = password + "2";
                            EditUserWindow.EditUser(fullName + "2", email, permissionLevel, password, password, webDriver);
                        }
                        else if (editPassword.ToLower() == "no")
                        {
                            EditUserWindow.EditUser(fullName + "2", email, permissionLevel, webDriver);
                        }
                        webDriver.SwitchtoWindow(webDriver, mainWindowName);

                        //Step 11: Refresh Page
                        System.Threading.Thread.Sleep(TimingConstants.UILoadWait);

                        //Step 12: Verifies edit has taken place by checking user's name
                        showName = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsMainPage.LoginName));
                        if (showName != fullName + "2")
                        {
                            _test.Fail(string.Format("Name on login does not match after edit. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}. Value shown on UI: {5}.",
                                    userName + "2", fullName, email, permissionLevel, password, showName), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception();
                        }
                        _test.Pass(string.Format("Name on login match after edit. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}. Value shown on UI: {5}.",
                                  userName + "2", fullName, email, permissionLevel, password, showName), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        //Step 13: Login to check password still valid
                        MainPage.logout(webDriver);

                        var successfulLogin = LoginPage.login(userName, password, webDriver);
                        if (!successfulLogin)
                        {
                            _test.Fail(string.Format("Failed to login after edited user login name. Username: {0}. Password: {1}.",
                                    userName, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception();
                        }
                        _test.Pass(string.Format("Login Successful after edited user login name. Username: {0}. Password: {1}.",
                                  userName, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        //Step 13: Deletes created user
                        System.Threading.Thread.Sleep(TimingConstants.UILoadWait);
                        MainPage.navigateToAdmin(webDriver);
                        System.Threading.Thread.Sleep(TimingConstants.UILoadAfterDatabaseChange);
                        if (!Userpage.DeleteUser(userName, fullName + "2", email, permissionLevel, webDriver))
                        {
                            _test.Fail(string.Format("Could not find user in list while trying to delete. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                    userName, fullName + "2", email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception();
                        }
                        _test.Pass(string.Format("Was able to find user in list while trying to delete. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                   userName, fullName + "2", email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        System.Threading.Thread.Sleep(TimingConstants.UILoadWait);
                        //Step 14: Verifies user is deleted
                        if (Userpage.FindUserInList(userName, fullName + "2", email, permissionLevel, webDriver))
                        {
                            _test.Fail(string.Format("User was not successfully deleted. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                    userName, fullName, email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "fAILURE")).Build());
                            throw new Exception();
                        }
                        _test.Pass(string.Format("User was successfully deleted. User name: {0} Full Name: {1} E-mail: {2} Permission: {3} Password: {4}",
                                   userName, fullName, email, permissionLevel, password), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        continue;
                    }
                    catch (Exception ex)
                    {
                        //Test sends fail and records fail chain
                        _test.Fail(ex);
                        throw;
                    }
                    finally
                    {
                        //increment for next run
                        //_test.Pass(string.Format(runData.RunRow + runData.RunDataValue("runDescription") + ": Given Run Description was executed"));
                        // backup.RestoreBackupOfDatabases();
                        //Step 15: Logs out
                        MainPage.logout(webDriver);
                        SeleniumWrapper.DriverCleanup(webDriver);
                        runData.RunRow++;
                    }
                }
            }
            videoRecording.StopRecording();
        }


    }
}
