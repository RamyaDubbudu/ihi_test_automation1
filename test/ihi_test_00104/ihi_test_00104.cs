using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;
using System.Collections.Generic;

namespace ihi_test_00104
{
    [TestFixture]
    public class ihi_test_00104 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("BatteryAlarms"), Property("Test", "TEST-104")]
        public void TEST_00104_TriggerBatteryAlarms()
        {
            var testName = GetType().Namespace;
            video.StartRecording(videoDir, SaveMe.Always);
            //Login to ESPilot 
            SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
            LoginPage.loginvalid(webDriver);
            webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00104__rundata.xlsx", 0);
            runData.RunRow = 0;
            string registerName = "";
            string registerValue = "";
            string clearAlarm = "";
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        //Send a Rest API Put to inverter simulator with Actual Power P value
                       
                        registerName = runData.RunDataValue("RegisterName");
                        registerValue = runData.RunDataValue("Value");
                        clearAlarm = runData.RunDataValue("ClearAlarm");                                       
                        ConsumeDCSimRest.UpdateBatteryDeviceType(registerName, registerValue);
                        System.Threading.Thread.Sleep(3000);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsAlarms.AlarmsTable), numberOfTimes:250);
                        List <AlarmsPage.AlarmsInfo> alarmInfo = AlarmsPage.GetAllAlarmInfo(webDriver);
                        if (alarmInfo.Count != 0)
                        {
                            if (alarmInfo[0].AlarmDescription.Contains(runData.RunDataValue("Description").ToString()))
                            {
                                _test.Pass("Alarm is shown in the Alarms Page with the following description:  \""+ alarmInfo[0].AlarmDescription + "\" contains the following alarm text as expected: \""  + runData.RunDataValue("Description").ToString() + "\"", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                            }
                        }
                        else
                        {
                            _test.Fail("Alarm is not shown in the Alarms Page with the following description: " + runData.RunDataValue("Description").ToString(), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            // throw new Exception("Alarm is shown in the Alarms Page with the following description: " + runData.RunDataValue("Description").ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of UI when error occured" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                        if (AlarmsPage.AreAllalarmsAcknowledged(webDriver) == false)
                        {
                            webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.AcknowledgeAllButton));
                        }
                        ConsumeDCSimRest.UpdateBatteryDeviceType(registerName, clearAlarm);
                        System.Threading.Thread.Sleep(2000);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.DismissAllButton));
                        System.Threading.Thread.Sleep(3000);
                      //  Assert.AreEqual(AlarmsPage.GetNumberofalarmRows(webDriver), 0);                   
                        runData.RunRow++;
                    }
                }               
            }
            SeleniumWrapper.DriverCleanup(webDriver);
            video.StopRecording();
       }

    }
}