using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;


namespace ihi_test_00204
{
    [TestFixture]
    public class ihi_test_00204 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Category("ISOMasterLogging"), Property("Test", "TEST-204")]
        public void TEST_00204__LoggingLevels_decs_isomb()
        {
            ihi_testlib_modbus.RunData runData = new ihi_testlib_modbus.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00204__rundata.xlsx", 0);
            runData.RunRow = 0;
            for (int i = runData.RunRow; i < 9; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string loggingLevel = runData.RunDataValue("loggingLevel");
                        string actualMessageInLogfile = runData.RunDataValue("actualMessage");
                        string expectedMessageInLogfile = runData.RunDataValue("expectedMessage");
                        var ssh = new SshClient(connectionInfo:SSH.connectionInfo);
                        string killisomb = string.Empty;
                        string resetisomb = string.Empty;
                        ssh.Connect();
                        if (ConfigurationManager.AppSettings["IsDocker"].ToString() == "True")
                        {
                            killisomb = string.Format(ihi_testlib_espilot.DockerCommands.SSHCommands.Killisomb);
                            resetisomb = "sudo docker exec -id decs-dc bash -c \"cd /usr/local/ihi-decs/bin&&echo decs |./decs_isomb -D "+ ConfigurationManager.AppSettings["DCIpAddress"].ToString() +" -l "+ loggingLevel +"\"";
                            SSH.OpenWrite(ssh, killisomb);
                            System.Threading.Thread.Sleep(1000);
                            SSH.OpenWrite(ssh, resetisomb);
                            System.Threading.Thread.Sleep(1000);
                        }
                        else
                        {
                            killisomb = "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_isomb";
                            resetisomb = "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_isomb -D " + ConfigurationManager.AppSettings["DCIpAddress"].ToString() + " -l " + loggingLevel + " &";
                            ihi_testlib_espilot.SSH.ExecCommand(ssh, killisomb);
                            System.Threading.Thread.Sleep(1000);
                            ihi_testlib_espilot.SSH.ExecCommand(ssh, resetisomb);
                            System.Threading.Thread.Sleep(1000);
                        }
                        System.Threading.Thread.Sleep(3000);
                        string logFile = SSH.SearchLogFilesForString(actualMessageInLogfile, GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                        if (logFile.Contains(expectedMessageInLogfile))
                        {
                            _test.Pass("Log file is shown with the actual logging level details set by the user:  " + logFile);
                        }
                        else
                        {
                            _test.Fail("Log file is not shown with the expected logging level!!! " + actualMessageInLogfile);
                            throw new Exception("Log file is not shown with the expected logging level!!!!!!");
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
            }
        }
    }
}