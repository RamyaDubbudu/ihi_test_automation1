using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00007
{
    [TestFixture]
    public class ihi_test_00007 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-7")]
        public void TEST_00007__Read_ESS_ENERGY_AVAILABLE_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00007__rundata.xlsx", 0);
            runData.RunRow = 0;
            string ess_EnergyAvbl = "0";
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ESS_EnergyAvailable = runData.RunDataValue("ESS_ENERGY_AVAILABLE");
                    ushort registerAddress = 11;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                   ConsumeDCSimRest.UpdateSiteData("kwh_available", runData_ESS_EnergyAvailable);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ess_EnergyAvbl = values[0].ToString();

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_ESS_EnergyAvailable == ess_EnergyAvbl) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_EnergyAvailable == ess_EnergyAvbl, "response Modbus message register value " + ess_EnergyAvbl + " is equal to the Run ID" + runData_ESS_EnergyAvailable + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetKWHAvailable");
                            StringAssert.Contains(runData_ESS_EnergyAvailable.ToString(), RestGetresult);
                            string getSiteinfo = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetTable");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_EnergyAvailable-" + runData_ESS_EnergyAvailable + " Actual ESS_EnergyAvailable-" + runData_ESS_EnergyAvailable + "response Modbus message register value is equal to the Run ID" + ess_EnergyAvbl + "column value" + getSiteinfo);
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_EnergyAvailable-" + runData_ESS_EnergyAvailable + " Actual ESS_EnergyAvailable-" + runData_ESS_EnergyAvailable + "response Modbus message register value is not equal to the Run ID" + ess_EnergyAvbl + "column value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_EnergyAvailable-" + runData_ESS_EnergyAvailable + " Actual ESS_EnergyAvailable-" + runData_ESS_EnergyAvailable + "response Modbus message register value is not equal to the Run ID" + ess_EnergyAvbl + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runData_ESS_EnergyAvailable != ess_EnergyAvbl) // invalid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_EnergyAvailable != ess_EnergyAvbl, "Run ID ESS_EnergyAvailable invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_EnergyAvailable + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_EnergyAvailable + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_EnergyAvailable + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}