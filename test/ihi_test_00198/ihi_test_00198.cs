﻿using System;
using ihi_test_00198.Initialize;
using NUnit.Framework;
using AventStack.ExtentReports;
using ihi_testlib_espilot;
using System.Configuration;
using ihi_testlib_videorecorder;
using ihi_testlib_modbus;

namespace ihi_test_00198
{
    [TestFixture]
    public class ihi_test_00198 : TestInitialize
    {
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        [Test, Category("LoginScenarios")]
        [Property("Test", "TEST-198")]
        public void TEST_00060__SwitchfromShutdowntoOtherMode()
        {
            var testName = GetType().Namespace;
            video.StartRecording(testName, SaveMe.Always);
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00198__rundata.xlsx", 0);
            runData.RunRow = 0;
            bool result = false;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var desiredState = runData.RunDataValue("Mode");

                    // Launches webserver and logs in
                    SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                    LoginPage.loginvalid(webDriver);
                    MainPage.navigateToDashboard(webDriver);
                    string sitename = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.SiteName));
                    if (!GeneralDECS.checkChangeElement("Shutdown", GlobalElementsDashboard.CurrentMode, 3000, webDriver))
                    {
                        result = false;
                        _test.Info("System not in Shutdown state");
                        return;
                    }

                    //Switch to Startup/Shutdown frame and switch to selected mode
                    MainPage.navigateToStartupShutDown(webDriver);
                    if (desiredState == "Manual Mode") 
                    DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
                    else if (desiredState == "Standby")
                        DashboardPage.Change2Standby_CheckBatteryConnection(webDriver);
                    else if (desiredState == "SOC Maintenance")
                        DashboardPage.ChangeModeAs(webDriver, desiredState);
                    MainPage.navigateToDashboard(webDriver);

                    //Verifies selected mode is now manual
                    if (DashboardPage.GetCurrenteMode(3000, webDriver) != desiredState)
                    {
                        result = false;
                        _test.Fail("System not in " + desiredState, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        return;
                    }

                    //Give 40 seconds to switch to manual mode
                    if (DashboardPage.checkChangeMode(desiredState, 40000, webDriver))
                    {
                        result = true;
                        _test.Pass(desiredState + " achieved", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                    }
                    else
                    {
                        result = false;
                        _test.Fail(desiredState + " not achieved", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        return;
                    }

                    //switches back to shutdown, gives 10 seconds
                    DashboardPage.ChangeMode(webDriver, "Shutdown");
                    if (DashboardPage.GetCurrenteMode(3000, webDriver) == "Shutdown")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                        _test.Fail("Shutdown" + " not achieved", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        return;
                    }
                }
                catch (Exception e)
                {
                    //Test sends fail and records fail chain
                    _test.Fail(e);
                }
                finally
                {
                    runData.RunRow++;
                    MainPage.logout(webDriver);
                    SeleniumWrapper.DriverCleanup(webDriver);
                }
            }
            video.StopRecording();
        }


    }
}
