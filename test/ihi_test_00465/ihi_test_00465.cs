using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Collections.Generic;
using ihi_testlib_espilot;
using AventStack.ExtentReports;
using OpenQA.Selenium;

namespace ihi_test_00465
{

    [TestFixture]
    public class ihi_test_00465 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Trigger_IO_Alarms_HIL"), Property("Test", "TEST-465")]
        public void TEST_00465__Trigger_IO_Alarms_TYPHOONHIL()
        {

            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            List<int> holdingregisters = new List<int>();
            List<short> clearAlarms = new List<short>();
            int triggerAlarm;
            short setAlarm;
            string runData_AlarmDescription = string.Empty;
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00465__rundata.xlsx", 0);
            runData.RunRow = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                runData_AlarmDescription = runData.RunDataValue("AlarmName");
                holdingregisters.Add(Convert.ToInt32(runData.RunDataValue("RegisterAddress")));
                clearAlarms.Add(short.Parse(runData.RunDataValue("ClearAlarm")));
                runData.RunRow++;
            }
            ihi_testlib_modbus.ModbusServer.ClearAlarms(runData.RowCount, holdingregisters, clearAlarms);
            SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
            LoginPage.loginvalid(webDriver);
            ihi_testlib_modbus.ModbusServer.ClearAlarms(runData.RowCount, holdingregisters, clearAlarms);
            webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: 60);
            runData.RunRow = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    runData_AlarmDescription = runData.RunDataValue("AlarmName");
                    triggerAlarm = Convert.ToInt32(runData.RunDataValue("RegisterAddress"));
                    setAlarm = short.Parse(runData.RunDataValue("SetAlarm"));
                    //Setting alarm
                    ihi_testlib_modbus.ModbusServer.SetAlarm(runData.RowCount, holdingregisters, clearAlarms, triggerAlarm, setAlarm);
                    webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                    AlarmsPage.AlarmsInfo alarmInfo = new AlarmsPage.AlarmsInfo();
                    alarmInfo = AlarmsPage.returnAlarmInfo(1, webDriver);
                    if (alarmInfo.AlarmDescription.Contains(runData_AlarmDescription))
                    {
                        _test.Pass("Alarm is shown in the Alarms Page with the following description:" + alarmInfo.AlarmDescription + " Matching to the RunData setting alarm as:  " + runData_AlarmDescription, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());

                    }
                    else
                    {
                        _test.Fail("Alarm is not shown in the Alarms Page with the following description: " + alarmInfo.AlarmDescription + " NOT Matching to the Rest setting alarm as: " + runData_AlarmDescription, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                    }
                    //clearing alarm
                    ihi_testlib_modbus.ModbusServer.ClearAlarms(runData.RowCount, holdingregisters, clearAlarms);
                    webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.AlarmsTab));
                    alarmInfo = AlarmsPage.returnAlarmInfo(1, webDriver);
                    if (alarmInfo.ClearTime != null)
                    {
                        string clearTimeOfAlarm = alarmInfo.ClearTime;
                        _test.Pass(alarmInfo.AlarmDescription + " The given Alarm is Cleared from the Alarms Page at :" + clearTimeOfAlarm, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.AcknowledgeAllButton));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsAlarms.DismissAllButton));
                    }
                    else
                    {
                        _test.Fail(alarmInfo.AlarmDescription + " This Alarm cannot be Cleared from the Alarms Page ", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                    }
                    runData.RunRow++;
                }
            }
            SeleniumWrapper.DriverCleanup(webDriver);
        }

    }
}