using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00010
{
    [TestFixture]
    public class ihi_test_00010 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-10")]
        public void TEST_00010__Read_ESS_P_DEMAND_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00010__rundata.xlsx", 0);
            runData.RunRow = 0;
            short ESS_P_Demand = 0; 
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string ESS_P_DEMAND = runData.RunDataValue("ESS_P_DEMAND");
                    ushort registerAddress = 15;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("p_demand", ESS_P_DEMAND);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ESS_P_Demand = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (ESS_P_DEMAND == ESS_P_Demand.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(ESS_P_DEMAND == ESS_P_Demand.ToString(), "response Modbus message register value is " + ESS_P_Demand + " equal to the Run ID" + ESS_P_DEMAND + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetPDemand");
                            StringAssert.Contains(ESS_P_DEMAND.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_P_DEMAND-" + ESS_P_DEMAND + " Actual ESS_P_DEMAND-" + ESS_P_DEMAND + "response Modbus message register value is equal to the Run ID" + ESS_P_Demand + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_P_DEMAND-" + ESS_P_DEMAND + " Actual ESS_P_DEMAND-" + ESS_P_DEMAND + "response Modbus message register value is not equal to the Run ID" + ESS_P_Demand + "column value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_P_DEMAND-" + ESS_P_DEMAND + " Actual ESS_P_DEMAND-" + ESS_P_DEMAND + "response Modbus message register value is not equal to the Run ID" + ESS_P_Demand + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (ESS_P_DEMAND != ESS_P_Demand.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(ESS_P_DEMAND != ESS_P_Demand.ToString(), "Run ID ESS_P_DEMAND invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_P_DEMAND + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_P_DEMAND + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_P_DEMAND + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}