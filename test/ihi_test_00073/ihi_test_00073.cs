using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;

namespace ihi_test_00073
{
    [TestFixture]
    public class ihi_test_00073 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Inverter_Status"), Property("Test", "TEST-73")]
        public void TEST_00073_Inverter_DC_BusVoltage_Status()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            string registerName = "voltage_dc";
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00073__rundata.xlsx", 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateInverterDeviceType(registerName, "0");
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        //Send a Rest API Put to inverter simulator with Actual Power P value
                        string runData_Voltage_DC = runData.RunDataValue("VOLTAGE_DC").ToString();
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(5000);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(8000, webDriver))
                        {
                            // Function will check the status and change mode to Manual mode
                            DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
                        }
                        else if ("Online" == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalDashbaordInverterDetails.InverterStatus)))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
                        }
                        ConsumeDCSimRest.UpdateInverterDeviceType(registerName, runData_Voltage_DC);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.ZonesTab));
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsControl.ZonesTab), numberOfTimes: waitTime);
                        string VoltageDC_OnUI = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Zones.ZoneDC));
                        //Validate the p demand value on dashboard
                        if (VoltageDC_OnUI == runData_Voltage_DC + ".0")
                        {
                            _test.Pass("DC Voltage value: " + runData.RunDataValue("VOLTAGE_DC_EXPECTED") + " sent thru Rest API is same in the ESPilot Controls > Zones Tab: " + runData.RunDataValue("VOLTAGE_DC_EXPECTED"), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        else
                        {
                            _test.Fail("DC Voltage value in dashboard page: " + VoltageDC_OnUI + " is not equal to the registervalue sent thru Rest API " + runData.RunDataValue("VOLTAGE_DC_EXPECTED"), MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception("DC Voltage value in dashboard page: " + VoltageDC_OnUI + " is not equal to the registervalue sent thru Rest API " + runData.RunDataValue("VOLTAGE_DC_EXPECTED"));
                        }
                    }
                    catch (Exception ex)
                    {
                         _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                        //Resetting DC_Voltage back to initial state
                        ConsumeDCSimRest.UpdateInverterDeviceType(registerName, "0");
                    }
                }
            }
                //video.StopRecording();          
        }

    }
}