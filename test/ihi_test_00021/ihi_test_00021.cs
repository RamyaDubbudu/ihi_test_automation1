using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00021
{
    [TestFixture]
    public class ihi_test_00021 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
       
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-21")]
        public void TEST_00021__Read_ESS_Q_DEMAND_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00021__rundata.xlsx", 0);
            runData.RunRow = 0;
            short eSS_Q_Demand = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ESS_Q_Demand = runData.RunDataValue("ESS_Q_DEMAND");
                    ushort registerAddress = 16;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("q_demand", runData_ESS_Q_Demand);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    eSS_Q_Demand = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_ESS_Q_Demand == eSS_Q_Demand.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_Q_Demand == eSS_Q_Demand.ToString(), "response Modbus message register value  " + eSS_Q_Demand + " is equal to the Run ID" + runData_ESS_Q_Demand + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetQDemand");
                            StringAssert.Contains(runData_ESS_Q_Demand.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Actual ESS_Q_DEMAND-" + eSS_Q_Demand + "response Modbus message register value is equal to the Run ID" + runData_ESS_Q_Demand + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Actual ESS_Q_DEMAND-" + eSS_Q_Demand + "response Modbus message register value is not equal to the Run ID" + runData_ESS_Q_Demand + "column value");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Actual ESS_Q_DEMAND-" + eSS_Q_Demand + "response Modbus message register value is not equal to the Run ID" + runData_ESS_Q_Demand + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runData_ESS_Q_Demand != eSS_Q_Demand.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_Q_Demand != eSS_Q_Demand.ToString(), "Run ID ESS_Q_DEMAND invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Q_Demand + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Q_Demand + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_Q_Demand + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }

    }
}