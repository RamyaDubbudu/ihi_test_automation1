using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00045
{
    [TestFixture]
    public class ihi_test_00045 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-45")]
        public void TEST_00045__Read_ESS_ZonesOnline_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00045__rundata.xlsx", 0);
            runData.RunRow = 0;
            string zones_online = "0";
            for (int i = runData.RunRow; i < 3; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string ESS_ZonesOnline = runData.RunDataValue("ESS_ZONES_ONLINE");
                    ushort registerAddress = 8;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("zones_online", ESS_ZonesOnline);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    zones_online = values[0].ToString();

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (ESS_ZonesOnline == zones_online) //Valid Scenarios
                        {
                            Assert.IsTrue(ESS_ZonesOnline == zones_online, "response Modbus message register value " + zones_online + " is equal to the Run ID" + ESS_ZonesOnline + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetZonesOnline");
                            StringAssert.Contains(ESS_ZonesOnline.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZonesOnline-" + ESS_ZonesOnline + " Actual ESS_ZonesOnline-" + zones_online + "response Modbus message register value is equal to the Run ID" + ESS_ZonesOnline + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZonesOnline-" + ESS_ZonesOnline + " Actual ESS_ZonesOnline-" + zones_online + "response Modbus message register value is not equal to the Run ID" + ESS_ZonesOnline + "column value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZonesOnline-" + ESS_ZonesOnline + " Actual ESS_ZonesOnline-" + zones_online + "response Modbus message register value is not equal to the Run ID" + ESS_ZonesOnline + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (ESS_ZonesOnline == zones_online) // invalid Scenarios
                        {
                            Assert.IsTrue(ESS_ZonesOnline == zones_online, "response Modbus message register value " + zones_online + " is equal to the Run ID" + ESS_ZonesOnline + "column value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZonesOnline-" + ESS_ZonesOnline + " Actual ESS_ZonesOnline-" + zones_online + "response Modbus message register value is equal to the Run ID" + ESS_ZonesOnline + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZonesOnline-" + ESS_ZonesOnline + " Actual ESS_ZonesOnline-" + zones_online + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZonesOnline-" + ESS_ZonesOnline + " Actual ESS_ZonesOnline-" + zones_online + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }

    }
}