using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using System.Configuration;


namespace ihi_test_00215
{
    [TestFixture]
    public class ihi_test_00215 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Category("ISOServerwithUseZMQflag"), Property("Test", "TEST-215")]
        public void TEST_00215__ISOServerwithUseZMQflag()
        {
            var ssh = new SshClient(connectionInfo:SSH.connectionInfo);
            string killisomb = string.Empty;
            string resetisomb = string.Empty;
            ssh.Connect();
            if (ConfigurationManager.AppSettings["IsDocker"].ToString() == "True")
            {
                killisomb = string.Format(ihi_testlib_espilot.DockerCommands.SSHCommands.Killisomb);
                resetisomb = "sudo docker exec -id decs-dc bash -c \"cd /usr/local/ihi-decs/bin&&echo decs |./decs_isomb -n\"";
                SSH.OpenWrite(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                SSH.OpenWrite(ssh, resetisomb);
                System.Threading.Thread.Sleep(1000);
            }
            else
            {
                killisomb = "cd /usr/local/ihi-decs/bin&&echo decs |pkill decs_isomb";
                resetisomb = "cd /usr/local/ihi-decs/bin&&echo decs | ./decs_isomb -n";
                ihi_testlib_espilot.SSH.ExecCommand(ssh, killisomb);
                System.Threading.Thread.Sleep(1000);
                ihi_testlib_espilot.SSH.ExecCommand(ssh, resetisomb);
                System.Threading.Thread.Sleep(1000);
            }
            System.Threading.Thread.Sleep(3000);
            string logFile = SSH.SearchLogFilesForString("Command-Line: -n, No ZeroMQ (ZMQ) communications.", GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            if (logFile.Contains("Command-Line: -n, No ZeroMQ (ZMQ) communications."))
            {
                _test.Pass("Log file is shown with error for no zmq communication:  " + logFile);
            }
            else
            {
                _test.Fail("Log file is not shown with error for no zmq communication!!! " + logFile);
                throw new Exception("Log file is not shown with error for no zmq communication!!!!!!");
            }
                   
        }
    }
}