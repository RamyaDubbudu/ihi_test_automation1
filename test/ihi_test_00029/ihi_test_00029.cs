using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00029
{
    [TestFixture]
    public class ihi_test_00029 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-29")]
        public void TEST_00029__Write_ISO_SOC_TGT_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00029__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort soc_target = 500;
            short q_min = -32768;
            short q_max = 32767;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        ushort registerAddress = 4;
                        short runData_SocTrgt = short.Parse(runData.RunDataValue("ISO_SOC_TGT"));
                        string RID_q_min = runData.RunDataValue("ESS_QMIN");
                        string RID_q_max = runData.RunDataValue("ESS_QMAX");
                        // Publish ESS_QMIN and ESS_QMAX data from DC Sim for the Run ID
                        ConsumeDCSimRest.UpdateSiteData("q_min", RID_q_min);
                        ConsumeDCSimRest.UpdateSiteData("q_max", RID_q_max);
                        //Send a Modbus to the ISO server to read the input registers (ESS_QMAX)
                        ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, 6, 2);
                        q_max = (Int16)values[0];
                        q_min = (Int16)values[1];
                        //Ensure response Modbus message has Run ID ESS_QMIN*and *ESS_QMAX column values
                        Assert.IsTrue(RID_q_min == q_min.ToString(), "response Modbus message register value " + q_min + " is equal to the Run Value" + RID_q_min + " column value");
                        Assert.IsTrue(RID_q_max == q_max.ToString(), "response Modbus message register value p_max " + q_max + " is equal to the Run Value RID_p_max " + RID_q_max + " row " + runData.RunRow + " column value");
                        //Send a Modbus request to the ISO slave to write the "ISO_SOC_TGT" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(runData_SocTrgt));
                        //Send a Modbus request to the ISO slave to read the "ISO_SOC_TGT" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        soc_target = (UInt16)values[0];
                        if (soc_target.ToString() == runData.RunDataValue("ISO_SOC_TGT_EXPECTED"))
                        {
                            Assert.IsTrue(soc_target.ToString() == runData.RunDataValue("ISO_SOC_TGT_EXPECTED"), "response Modbus message register value " + soc_target + " is equal to the Run Value" + runData.RunDataValue("ISO_SOC_TGT_EXPECTED") + " row " + runData.RunRow + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetSOCTarget");
                            StringAssert.Contains(soc_target.ToString(), RestGetresult);
                            _test.Pass("response Modbus message register value " + soc_target + " is equal to the Run Value" + runData_SocTrgt + " column value");
                        }
                        else
                        {
                            _test.Fail("response Modbus message register value " + soc_target + " is not equal to the Run Value" + runData_SocTrgt + " column value");
                            throw new Exception("response Modbus message register value " + soc_target + " is not equal to the Run Value" + runData_SocTrgt + " column value");
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
            }
        }

    }
}