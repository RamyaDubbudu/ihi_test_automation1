using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00049
{
    [TestFixture]
    public class ihi_test_00049 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-49")]
        public void TEST_00049__Write_ISO_SEQ_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00049__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort iso_sequence = 500;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        ushort registerAddress = 0;
                        ushort runData_ISO_SEQ = ushort.Parse(runData.RunDataValue("ISO_SEQ"));
                        var scenarioType = runData.RunDataValue("Scenarios");
                        //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_sequence));
                        //Send a Modbus request to the ISO slave to read the "ISO_SEQ" holding register
                        ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iso_sequence = (UInt16)values[0];
                        //Send a Modbus request to the ISO slave to write the "ISO_SEQ" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, runData_ISO_SEQ);
                        //Send a Modbus request to the ISO slave to read the "ISO_SEQ" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iso_sequence = (UInt16)values[0];
                        if (scenarioType == "Valid") //Valid Scenarios
                        {
                            if (runData_ISO_SEQ.ToString() == iso_sequence.ToString()) //Valid Scenarios
                            {
                                Assert.IsTrue(runData_ISO_SEQ.ToString() == iso_sequence.ToString(), "response Modbus message register value " + iso_sequence + " is equal to the Run Value" + runData_ISO_SEQ + " column value");
                                _test.Pass("response Modbus message register value " + iso_sequence + " is equal to the Run Value" + runData_ISO_SEQ + " column value");
                            }
                            else
                            {
                                _test.Fail("response Modbus message register value " + iso_sequence + " is not equal to the Run Value" + runData_ISO_SEQ + " column value");
                                throw new Exception("response Modbus message register value " + iso_sequence + " is not equal to the Run Value" + runData_ISO_SEQ + " column value");
                            }
                        }

                        string result = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetISOSeq");
                        StringAssert.Contains(iso_sequence.ToString(), result);
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }


    }
}