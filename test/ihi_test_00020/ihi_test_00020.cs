using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00020
{
    [TestFixture]
    public class ihi_test_00020 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
       
       [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-20")]
        public void TEST_00020__Read_ESS_SOH_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00020__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort ess_Soh = 0;
            int ESS_SOHPercent = 0;
            for (int i = runData.RunRow; i < 3; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ESS_SOH = runData.RunDataValue("ESS_SOH");
                    ushort registerAddress = 22;
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("soh", runData_ESS_SOH);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ess_Soh = (UInt16)values[0];
                    ESS_SOHPercent = Convert.ToInt32(runData_ESS_SOH) * 10;
                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE
                    if (ESS_SOHPercent.ToString() == ess_Soh.ToString()) //Valid Scenarios
                    {
                        Assert.IsTrue(ESS_SOHPercent.ToString() == ess_Soh.ToString(), "response Modbus message register value is equal to the Run ID" + runData_ESS_SOH + "column value");
                        string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetSoh");
                        StringAssert.Contains(runData_ESS_SOH.ToString(), RestGetresult);
                        _test.Pass(" RunID-" + runData.RunRow + " Expected ESS_SOH-" + ess_Soh + " Actual ESS_SOH-" + ess_Soh + "response Modbus message register value is equal to the Run ID" + runData_ESS_SOH + "column value");
                    }
                    else
                    {
                        _test.Fail(" RunID-" + runData.RunRow + " Expected ESS_SOH-" + ess_Soh + " Actual ESS_SOH-" + ess_Soh + "response Modbus message register value is not equal to the Run ID" + runData_ESS_SOH + "column value");
                        throw new Exception(" RunID-" + runData.RunRow + " Expected ESS_SOH-" + ess_Soh + " Actual ESS_SOH-" + ess_Soh + "response Modbus message register value is not equal to the Run ID" + runData_ESS_SOH + "column value");
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}