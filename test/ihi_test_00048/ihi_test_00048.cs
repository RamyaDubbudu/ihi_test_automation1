using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00048
{
    [TestFixture]
    public class ihi_test_00048 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-48")]
        public void TEST_00048__Read_ESS_SOC_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00048__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort ESS_Soc = 0;
            int ESS_SOCPercent = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    ushort ESS_SOC = Convert.ToUInt16(runData.RunDataValue("ESS_SOC"));
                    ushort registerAddress = 10;
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("soc", ESS_SOC.ToString());
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ESS_Soc = (UInt16)values[0];
                    ESS_SOCPercent = Convert.ToInt32(ESS_SOC) * 10;
                    //  soc = soc.Substring(3);
                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE                   
                    if (ESS_SOCPercent.ToString() == ESS_Soc.ToString()) //Valid Scenarios
                    {
                        Assert.IsTrue(ESS_SOCPercent.ToString() == ESS_Soc.ToString(), "response Modbus message register value is " + ESS_Soc + " equal to the Run ID" + ESS_SOC + "column value");
                        //string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetSOC");
                        //StringAssert.Contains(string.Format("0.0", soc), RestGetresult);
                        _test.Pass(" RunID-" + runData.RunRow + " Expected ESS_SOC-" + ESS_SOC + " Actual ESS_SOC-" + ESS_SOC + "response Modbus message register value is equal to the Run ID" + ESS_SOC + "column value");
                    }
                    else
                        _test.Fail(" RunID-" + runData.RunRow + " Expected ESS_SOC-" + ESS_SOC + " Actual ESS_SOC-" + ESS_SOC + "response Modbus message register value is not equal to the Run ID" + ESS_SOC + "column value");
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }


    }
}