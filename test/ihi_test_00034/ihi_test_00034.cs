using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00034
{
    [TestFixture]
    public class ihi_test_00034 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-34")]
        public void TEST_00034__Write_ISO_MODE_Q_TGT_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00034__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort iSO_Mode_Q_Tgt = 500;
            short q_min = -32768;
            short q_max = 32767;
            ushort registerAddress = 7;
            ConsumeDCSimRest.UpdateSiteData("q_min", q_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("q_max", q_max.ToString());
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iSO_Mode_Q_Tgt));
            System.Threading.Thread.Sleep(2000);
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            iSO_Mode_Q_Tgt = (UInt16)values[0];
            Assert.IsTrue(iSO_Mode_Q_Tgt.ToString() == "500", "ISO_MODE_Q_TARGET is equal to the initial value");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        short runData_Iso_Mode_QTrgt = short.Parse(runData.RunDataValue("ISO_MODE_Q_TGT"));
                        string RID_q_min = runData.RunDataValue("ESS_QMIN");
                        string RID_q_max = runData.RunDataValue("ESS_QMAX");
                        var scenarioType = runData.RunDataValue("Scenarios");
                        // Publish ESS_QMIN and ESS_QMAX data from DC Sim for the Run ID
                        ConsumeDCSimRest.UpdateSiteData("q_min", RID_q_min);
                        ConsumeDCSimRest.UpdateSiteData("q_max", RID_q_max);
                        //Send a Modbus request to the ISO slave to write the "ISO_SOC_P_TGT" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(runData_Iso_Mode_QTrgt));
                        System.Threading.Thread.Sleep(2000);
                        //Send a Modbus request to the ISO slave to read the "ISO_SOC_P_TGT" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iSO_Mode_Q_Tgt = (UInt16)values[0];
                        if (scenarioType == "Valid") //Valid Scenarios
                        {
                            if (iSO_Mode_Q_Tgt.ToString() == runData.RunDataValue("ISO_MODE_Q_TGT_EXPECTED")) //Valid Scenarios
                            {
                                Assert.IsTrue(iSO_Mode_Q_Tgt.ToString() == runData.RunDataValue("ISO_MODE_Q_TGT_EXPECTED"), "response Modbus message register value " + iSO_Mode_Q_Tgt + " is equal to the Run Value" + runData.RunDataValue("ISO_MODE_Q_TGT_EXPECTED") + " row " + runData.RunRow + " column value");
                                string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetTargetQ");
                                StringAssert.Contains(iSO_Mode_Q_Tgt.ToString(), RestGetresult);
                                _test.Pass("response Modbus message register value " + iSO_Mode_Q_Tgt + " is equal to the Run Value" + runData_Iso_Mode_QTrgt + " column value");
                            }
                            else
                            {
                                _test.Fail("response Modbus message register value " + iSO_Mode_Q_Tgt + " is not equal to the Run Value" + runData_Iso_Mode_QTrgt + " column value");
                                throw new Exception("response Modbus message register value " + iSO_Mode_Q_Tgt + " is not equal to the Run Value" + runData_Iso_Mode_QTrgt + " column value");
                            }
                            }
                        if (scenarioType == "Invalid") // invalid Scenarios
                        {
                            if (iSO_Mode_Q_Tgt.ToString() != runData.RunDataValue("ISO_MODE_Q_TGT_EXPECTED")) // invalid Scenarios
                            {
                                Assert.IsTrue(iSO_Mode_Q_Tgt.ToString() != runData.RunDataValue("ISO_MODE_Q_TGT_EXPECTED"), "response Modbus message register value " + iSO_Mode_Q_Tgt + " is equal to the Run Value" + runData.RunDataValue("ISO_MODE_Q_TGT_EXPECTED") + " row " + runData.RunRow + " column value");
                                _test.Pass("Run ID ISO_Mode_Q_Tgt " + iSO_Mode_Q_Tgt + " is invalid, so equal to initial ISO_Mode_Q_Tgt " + iSO_Mode_Q_Tgt + " value and does not match to Run Value");
                            }
                            else
                            {
                                _test.Fail("Run ID ISO_Mode_Q_Tgt " + iSO_Mode_Q_Tgt + " is invalid, so should not be equal to initial ISO_Mode_Q_Tgt " + iSO_Mode_Q_Tgt + " value and does not match to Run Value");
                               throw new Exception("Run ID ISO_Mode_Q_Tgt " + iSO_Mode_Q_Tgt + " is invalid, so should not be equal to initial ISO_Mode_Q_Tgt " + iSO_Mode_Q_Tgt + " value and does not match to Run Value");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }

    }
}