﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_test_00034
{
    [SetUpFixture]
    public abstract class TestInitialize
    {

        #region Variables 
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDirectory);
        public static ExtentReports _extent = new ExtentReports();
        protected ExtentTest _test;
        public string S3BucketUploadDir;
        public static string keyName = "qa/" + System.Environment.UserName + "/" + ConfigurationManager.AppSettings["Component"].ToString() + "/" + DateTime.Today.ToString("yyyyMMdd");
        #endregion



        [OneTimeSetUp]
        protected void Setup()
        {
            var testname = GetType().Namespace;
            ihi_launch_simulator.LaunchSimulator.DomainControllerCleanUp();
            string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
            var reportDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            Directory.CreateDirectory(reportDir);
            S3BucketUploadDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname;
            keyName = keyName + "/" + testname;
            var fileName = GetType().Namespace + ".html";
            var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
            htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
            htmlReporter.AppendExisting = true;
            _extent.AttachReporter(htmlReporter);
            //Stoping ESPilot DECS
            ihi_testlib_espilot.SSH.StopDecs(sshInfo);
            System.Threading.Thread.Sleep(5000);
            ////Starting the Inverter simulator AND Battery simulator
            //Opening the web application of inverter and battery simulator to open the connection
            ihi_launch_simulator.LaunchSimulator.RunDomainControllerSimulator(ConfigurationManager.AppSettings["dcsimPjtPath"].ToString(), ConfigurationManager.AppSettings["Port"].ToString());
            //Start ESPilot DECS
            try
            {
                var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                ihi_testlib_espilot.SSHAndStartDECSDirectory decsInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(sshInfo, GlobalVariables.DECSDirectory);
                var startDECSSuccess = ihi_testlib_espilot.SSH.StartDecs(ihi_testlib_espilot.DECSLaunchType.Normal, decsInfo, 10000, "");
                System.Threading.Thread.Sleep(GlobalVariables.TimingConstants.DECSLaunch);
                ihi_testlib_espilot.SSH.SetDCSimulatorIPAddress(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetTable");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [OneTimeTearDown]
        protected void TearDown()
        {
            _extent.Flush();
            //stop IIS EXPRESS 
            ihi_launch_simulator.LaunchSimulator.DomainControllerCleanUp();
            //   webDriver.browserDriver.Quit();
        }

        [SetUp]
        public void BeforeTest()
        {
            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
            _test.AssignCategory(this.GetType().ToString());
            _test.AssignAuthor("Ramya Dubbudu");
        }

        [TearDown]
        public void AfterTest()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }
            _test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
            _extent.Flush();

        }
    }
}