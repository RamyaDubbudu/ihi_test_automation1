using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;

namespace ihi_test_00040
{
    [TestFixture]
    public class ihi_test_00040 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
       
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-40")]
        public void TEST_00040__Read_ESS_KW_NAMEPLATE_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00040__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort kW_Nameplte = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_ESS_KW_Nameplate = runData.RunDataValue("ESS_KW_NAMEPLATE");
                    ushort registerAddress = 2;
                    var scenarioType = runData.RunDataValue("Scenarios");

                    //Publish ESS_KW_NAMEPLATE data from DC Sim for the Run ID  ESS_KW_NAMEPLATE =  Run ID ESS_KW_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("kw_nameplate", runData_ESS_KW_Nameplate);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KW_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    kW_Nameplte = (UInt16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KW_NAMEPLATEis valid:   Run IDESS_KW_NAMEPLATEElse , if Run ID ESS_KW_NAMEPLATEis invalid:   equal to initial ESS_KW_NAMEPLATE

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_ESS_KW_Nameplate == kW_Nameplte.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_KW_Nameplate == kW_Nameplte.ToString(), "response Modbus message register value " + kW_Nameplte + " is equal to the Run Value" + kW_Nameplte + " column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetKWNameplate");
                            StringAssert.Contains(runData_ESS_KW_Nameplate.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual KW_Nameplte-" + runData_ESS_KW_Nameplate + "  response Modbus message register value is equal to the Run Value" + kW_Nameplte + " column value");
                        }
                        else
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Actual KW_Nameplte-" + runData_ESS_KW_Nameplate + "  response Modbus message register value is not equal to the Run Value" + kW_Nameplte + " column value");
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runData_ESS_KW_Nameplate != kW_Nameplte.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(runData_ESS_KW_Nameplate != kW_Nameplte.ToString(), "Run ID ESS_KW_NAMEPLATEis invalid, so equal to initial ESS_KW_NAMEPLATE value and does not match to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_KW_Nameplate + " is invalid, so does not equal to Run Value");
                        }
                        else
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_ESS_KW_Nameplate + " is invalid, bec Run Value should not be equal to invalid user input");
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}