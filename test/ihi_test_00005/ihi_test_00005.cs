using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00005
{
    [TestFixture]
    public class ihi_test_00005 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-5")]
        public void TEST_00005__Write_ISO_MODE_P_TGT_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00005__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort iso_ModePTgt = 500;
            short p_min = -32768;
            short p_max = 32767;
            ushort registerAddress = 6;
            ConsumeDCSimRest.UpdateSiteData("p_min", p_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("p_max", p_max.ToString());
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(iso_ModePTgt));
            System.Threading.Thread.Sleep(2000);
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            iso_ModePTgt = (UInt16)values[0];
            Assert.IsTrue(iso_ModePTgt.ToString() == "500", "ISO_SOC_POWER is equal to the initial value");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {

                        short runData_ISOMODE_PTGT = short.Parse(runData.RunDataValue("ISO_MODE_P_TGT"));
                        var scenarioType = runData.RunDataValue("Scenarios");
                        //Send a Modbus request to the ISO slave to write the "ISO_SOC_P_TGT" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(runData_ISOMODE_PTGT));
                        System.Threading.Thread.Sleep(2000);
                        //Send a Modbus request to the ISO slave to read the "ISO_SOC_P_TGT" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        iso_ModePTgt = (UInt16)values[0];
                        if (scenarioType == "Valid") //Valid Scenarios
                        {
                            if (iso_ModePTgt.ToString() == runData.RunDataValue("ISO_MODE_P_TGT_EXPECTED")) //Valid Scenarios
                            {
                                Assert.IsTrue(iso_ModePTgt.ToString() == runData.RunDataValue("ISO_MODE_P_TGT_EXPECTED"), "response Modbus message register value " + iso_ModePTgt + " is equal to the Run Value" + runData.RunDataValue("ISO_MODE_P_TGT_EXPECTED") + " row " + runData.RunRow + " column value");
                                string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetTargetP");
                                StringAssert.Contains(iso_ModePTgt.ToString(), RestGetresult);
                                string getRemoteData = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetTable");
                                _test.Pass("response Modbus message register value " + iso_ModePTgt + " is equal to the Run Value" + runData_ISOMODE_PTGT + " column value" + getRemoteData);
                            }
                            else
                            {
                                _test.Fail("response Modbus message register value " + iso_ModePTgt + " is not equal to the Run Value" + runData_ISOMODE_PTGT + " column value");
                                throw new Exception("response Modbus message register value " + iso_ModePTgt + " is not equal to the Run Value" + runData_ISOMODE_PTGT + " column value");
                            }
                        }
                        if (scenarioType == "Invalid") // invalid Scenarios
                        {
                            if (iso_ModePTgt.ToString() != runData.RunDataValue("ISO_MODE_P_TGT_EXPECTED")) // invalid Scenarios
                            {
                                Assert.IsTrue(iso_ModePTgt.ToString() != runData.RunDataValue("ISO_MODE_P_TGT_EXPECTED"), "response Modbus message register value " + iso_ModePTgt + " is equal to the Run Value" + runData.RunDataValue("ISO_MODE_P_TGT_EXPECTED") + " row " + runData.RunRow + " column value");
                                _test.Pass("Run ID ISO_Mode_P_Tgt " + iso_ModePTgt + " is invalid, so equal to initial ISO_Mode_P_Tgt " + iso_ModePTgt + " value and does not match to Run Value");
                            }
                            else
                            {
                                _test.Fail("Run ID ISO_Mode_P_Tgt " + iso_ModePTgt + " is invalid, so equal to initial ISO_Mode_P_Tgt " + iso_ModePTgt + " value should not match to Run Value");
                                throw new Exception("Run ID ISO_Mode_P_Tgt " + iso_ModePTgt + " is invalid, so equal to initial ISO_Mode_P_Tgt " + iso_ModePTgt + " value should not match to Run Value");
                           }
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }

    }
}