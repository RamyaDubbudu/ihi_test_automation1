using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using ihi_testlib_videorecorder;

namespace ihi_test_00066
{
    [TestFixture]
    public class ihi_test_00066 : TestInitialize
    {

        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        [Test, Category("Inverter_Status"), Property("Test", "TEST-66")]
        public void TEST_00066_Inverter_Real_Power_PActual_Status()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            string registerName = "real_power_actual";
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00066__rundata.xlsx", 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        //Send a Rest API Put to inverter simulator with Actual Power P value
                        string runDataPActual = runData.RunDataValue("P_ACTUAL").ToString();
                        ConsumeDCSimRest.UpdateInverterDeviceType(registerName, "0");
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(5000);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(5000, webDriver))
                        {
                            // Function will check the status and change mode to Manual mode
                            DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
                        }
                        else if ("Online" == DashboardPage.GetCurrenteMode(5000, webDriver))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
                        }
                        System.Threading.Thread.Sleep(5000);
                        ConsumeDCSimRest.UpdateInverterDeviceType(registerName, runDataPActual);                      
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsDashboard.pActualText, runDataPActual), numberOfTimes: waitTime);
                        string pActualOnUI = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsDashboard.pActual));
                        //Validate the P Actual value on dashboard
                        if (pActualOnUI == runDataPActual)
                        {
                            Assert.AreEqual(pActualOnUI, runDataPActual);
                            _test.Pass("P Actual value: " + runDataPActual + " sent thru Rest API is same in the ESPilot Dashboard: " + runDataPActual, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        else
                        {
                            _test.Fail("P Actual value in dashboard page: " + pActualOnUI + " is not equal to the registervalue sent thru Rest API " + runDataPActual, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception("P Actual value in dashboard page: " + pActualOnUI + " is not equal to the registervalue sent thru Rest API " + runDataPActual);
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }         
            }
             //video.StopRecording();
        }

    }
}