using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00003
{
    [TestFixture]
    public class ihi_test_00003 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-3"), Ignore("Process Issue")]
        public void TEST_00003__Read_ESS_SEQ_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00003__rundata.xlsx", 0);
            runData.RunRow = 0;
            string ESS_Seq = "0";
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                string ESS_SEQ = runData.RunDataValue("ESS_SEQ");
                ushort registerAddress = 0;
                var scenarioType = runData.RunDataValue("Scenarios");

                //Publish ESS_KW_NAMEPLATE data from DC Sim for the Run ID  ESS_KW_NAMEPLATE =  Run ID ESS_KW_NAMEPLATE
                ConsumeDCSimRest.UpdateRemoteData("ess_sequence", ESS_SEQ);
                System.Threading.Thread.Sleep(2000);
                //Send a Modbus request to the ISO server to read the input register (ESS_KW_NAMEPLATE)
                ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                ESS_Seq = values[0].ToString();

                //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                // If Run ID ESS_KW_NAMEPLATEis valid:   Run IDESS_KW_NAMEPLATEElse , if Run ID ESS_KW_NAMEPLATEis invalid:   equal to initial ESS_KW_NAMEPLATE
                if (scenarioType == "Valid") //Valid Scenarios
                {
                    if (ESS_SEQ == ESS_Seq) //Valid Scenarios
                    {
                        Assert.IsTrue(ESS_SEQ == ESS_Seq, "response Modbus message register value " + ESS_Seq + " is equal to the Run Value " + ESS_SEQ + " column value");
                        string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetESSSeq");
                        StringAssert.Contains(ESS_SEQ.ToString(), RestGetresult);
                        string remoteData = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetTable");
                        _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_Seq-" + ESS_SEQ + " Actual ESS_Seq-" + ESS_Seq + "  response Modbus message register value is equal to the Run Value" + ESS_Seq + " column value" + remoteData);
                    }
                    else
                    {
                        Assert.IsTrue(ESS_SEQ == ESS_Seq, "response Modbus message register value " + ESS_Seq + " is equal to the Run Value " + ESS_SEQ + " column value");
                        _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_Seq-" + ESS_SEQ + " Actual ESS_Seq-" + ESS_Seq + "  response Modbus message register value is NOT equal to the Run Value" + ESS_Seq + " column value");
                    }
                }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (ESS_SEQ != ESS_Seq) // invalid Scenarios
                        {
                            Assert.IsTrue(ESS_SEQ != ESS_Seq, "Run ID ESS_MODE " + ESS_SEQ + " is invalid, so equal to initial ESS_Seq " + ESS_Seq + " value and does not match to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_SEQ + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_SEQ + " is invalid, bec Run Value should not be equal to seq value");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + ESS_SEQ + " is invalid, bec Run Value should not be equal to seq value");

                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }

}
