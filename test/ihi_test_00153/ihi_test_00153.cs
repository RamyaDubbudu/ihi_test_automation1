using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;
using ihi_testlib_espilot;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using Newtonsoft.Json.Linq;
using ihi_testlib_videorecorder;

namespace ihi_test_00153
{
    [TestFixture]
    public class ihi_test_00153 : TestInitialize
    {

      
        #region Variables 
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Category("Inverter_Status"), Property("Test", "TEST-153")]
        public void TEST_00153_Inverter_PhaseCurrent_A_Status()
        {
            //video.StartRecording(videoDir, SaveMe.Always);
            string registerName = "phase_current_a";
            ihi_testlib_espilot.RunData runData = new ihi_testlib_espilot.RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00153__rundata.xlsx", 0);
            runData.RunRow = 0;           
            ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
            ConsumeDCSimRest.InverterRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.BatteryRestResponseByName("ihi", "Communication");
            ConsumeDCSimRest.UpdateBatteryRegisters("57364", "20000", "allowchargepower", type: "holding");
            ConsumeDCSimRest.UpdateBatteryRegisters("57366", "20000", "allowdischargepower", type: "holding");
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {
                        string runDataCurrent_PhaseA = runData.RunDataValue("Current_PhaseA").ToString();
                        SeleniumWrapper.SelectWebDriverBrowser(ihi_testlib_selenium.Selenium.Browser.Chrome, webDriver);
                        //Login to ESPilot
                        LoginPage.loginvalid(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        System.Threading.Thread.Sleep(5000);
                        if ("Shutdown" == DashboardPage.GetCurrenteMode(5000, webDriver))
                        {
                            // Function will check the status and change mode to Manual mode
                            DashboardPage.ChkInverterOnlineAndUpdateToManualMode(webDriver);
                        }
                        else if ("Online" == webDriver.ReadControlsInnerText(xPath: string.Format(GlobalDashbaordInverterDetails.InverterStatus)))
                        {
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordInverterDetails.InverterstatusText, "Online")));
                            webDriver.Assertion_IsElementVisible(xPath: string.Format(string.Format(GlobalDashbaordTreeDetails.InverterStatusText, "Online")));
                        }
                        System.Threading.Thread.Sleep(2000);
                        //Send a Rest API Put to inverter simulator with Actual Power P value
                        ConsumeDCSimRest.UpdateInverterDeviceType(registerName, runDataCurrent_PhaseA);
                        webDriver.RefreshPage(webDriver);
                        webDriver.WaitForElement(xpath: string.Format(GlobalDashbaordTreeDetails.InverterStatus), numberOfTimes: waitTime);
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsMainPage.ControlTab));
                        webDriver.ClickOnHTMLControl(XPath: string.Format(GlobalElementsControl.InvertersTab));
                        webDriver.WaitForElement(xpath: string.Format(GlobalElementsControl.Inverters.InvertersI1Text, runDataCurrent_PhaseA), numberOfTimes: waitTime);
                        string CurrentPhaseA_OnUI = webDriver.ReadControlsInnerText(xPath: string.Format(GlobalElementsControl.Inverters.InvertersI1));
                        //Validate the Phase C VOLTAGE value on Inverter Tab
                        if (runDataCurrent_PhaseA + ".0" == CurrentPhaseA_OnUI)
                        {
                            Assert.AreEqual(runDataCurrent_PhaseA + ".0", CurrentPhaseA_OnUI);
                            _test.Pass("Current Phase A value: " + CurrentPhaseA_OnUI + " sent thru Rest API is same in the ESPilot Control > Inverters Tab: " + runDataCurrent_PhaseA + ".0", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Success")).Build());
                        }
                        else
                        {
                            _test.Fail("Current Phase A value in Control > Inverters Tab page: " + CurrentPhaseA_OnUI + " is not equal to the registervalue sent thru Rest API " + runDataCurrent_PhaseA + ".0", MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                            throw new Exception("Current Phase A value in Control > Inverters Tab page: " + CurrentPhaseA_OnUI + " is not equal to the registervalue sent thru Rest API " + runDataCurrent_PhaseA + ".0");
                        }
                    }
                    catch (Exception ex)
                    {
                         _test.Fail("Current state of ESPilot when erro caused" + ex, MediaEntityBuilder.CreateScreenCaptureFromPath(TakesScreenshot(webDriver, "Failure")).Build());
                        Assert.Fail("Failed" + ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                        SeleniumWrapper.DriverCleanup(webDriver);
                    }
                }
            }
                //video.StopRecording();
        }

    }
}