using ihi_simulator_dc.Controllers;
using NUnit.Framework;
using ihi_testlib_videorecorder;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00027
{
    [TestFixture]
    public class ihi_test_00027 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion


        [Test, Retry(2), Category("WriteHoldingRegisters"), Property("Test", "TEST-27")]
        public void TEST_00027__Write_ISO_SOC_POWER_HoldingRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00027__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort soc_Power = 5;
            short q_min = -32768;
            short q_max = 32767;
            short p_min = -32768;
            short p_max = 32767;
            ushort registerAddress = 5;
            ConsumeDCSimRest.UpdateSiteData("q_min", q_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("q_max", q_max.ToString());
            ConsumeDCSimRest.UpdateSiteData("p_min", p_min.ToString());
            ConsumeDCSimRest.UpdateSiteData("p_max", p_max.ToString());
            ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, Convert.ToUInt16(soc_Power));
            ushort[] values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
            soc_Power = (UInt16)values[0];
            Assert.IsTrue(soc_Power.ToString() == "5", "ISO_SOC_POWER is equal to the initial value");
            for (int i = runData.RunRow; i < 8; i++)
            {
                if (runData.RunDataValue("runTest") == "FALSE")
                {
                    _test.Skip(runData.RunRow + " -This run was skipped due to run settings.");
                    runData.RunRow++;
                }
                else
                {
                    try
                    {

                        short runData_SocPower = short.Parse(runData.RunDataValue("ISO_SOC_POWER"));
                        string RID_p_min = runData.RunDataValue("ESS_PMIN");
                        string RID_p_max = runData.RunDataValue("ESS_PMAX");
                        var scenarioType = runData.RunDataValue("Scenarios");
                        // Publish ESS_QMIN and ESS_QMAX data from DC Sim for the Run ID
                        ConsumeDCSimRest.UpdateSiteData("p_min", RID_p_min);
                        ConsumeDCSimRest.UpdateSiteData("p_max", RID_p_max);
                        //Send a Modbus request to the ISO slave to write the "ISO_SOC_POWER" holding register Run ID ISO_SEQ
                        ModbusClient.WriteToSingleRegister(GlobalVariables.SSHhostname, registerAddress, (ushort)(runData_SocPower));
                        System.Threading.Thread.Sleep(2000);
                        //Send a Modbus request to the ISO slave to read the "ISO_SOC_POWER" holding register
                        values = ModbusClient.ReadHoldingRegisters(GlobalVariables.SSHhostname, registerAddress, 1);
                        soc_Power = (UInt16)values[0];
                        if (scenarioType == "Valid") //Valid Scenarios
                        {
                            if (soc_Power.ToString() == runData.RunDataValue("ISO_SOC_POWER_EXPECTED")) //Valid Scenarios
                            {
                                Assert.IsTrue(soc_Power.ToString() == runData.RunDataValue("ISO_SOC_POWER_EXPECTED"), "response Modbus message register value " + soc_Power + " is equal to the Run Value" + runData.RunDataValue("ISO_SOC_POWER_EXPECTED") + " row " + runData.RunRow + " column value");
                                string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetSOCPower");
                                StringAssert.Contains(soc_Power.ToString(), RestGetresult);
                                string remoteData = ConsumeDCSimRest.GetRestResponseByName("RemoteInfo", "GetTable");
                                _test.Pass("response Modbus message register value " + soc_Power + " is equal to the Run Value" + runData_SocPower + " column value, Here is the Remote Data information from Rest API after updating " + remoteData);
                            }
                            else
                            {
                                _test.Fail("response Modbus message register value " + soc_Power + " is not equal to the Run Value" + runData_SocPower + " column value");
                               throw new Exception("response Modbus message register value " + soc_Power + " is not equal to the Run Value" + runData_SocPower + " column value");
                            }
                        }
                        if (scenarioType == "Invalid") // invalid Scenarios
                        {
                            if (soc_Power.ToString() != runData.RunDataValue("ISO_SOC_POWER_EXPECTED")) // invalid Scenarios
                            {
                                Assert.IsTrue(soc_Power.ToString() != runData.RunDataValue("ISO_SOC_POWER_EXPECTED"), "response Modbus message register value " + soc_Power + " is equal to the Run Value" + runData.RunDataValue("ISO_SOC_POWER_EXPECTED") + " row " + runData.RunRow + " column value");
                                _test.Pass("Run ID soc_Power " + soc_Power + " is invalid, so equal to initial soc_Power " + soc_Power + " value and does not match to Run Value");
                            }
                            else
                            {
                                _test.Fail("Run ID soc_Power " + soc_Power + " is invalid, so equal to initial soc_Power " + soc_Power + " value should not match to Run Value");
                                throw new Exception("Run ID soc_Power " + soc_Power + " is invalid, so equal to initial soc_Power " + soc_Power + " value should not match to Run Value");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _test.Fail(ex);
                    }
                    finally
                    {
                        runData.RunRow++;
                    }
                }
                
            }
        }
    }
}