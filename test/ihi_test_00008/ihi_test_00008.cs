using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00008
{
    [TestFixture]
    public class ihi_test_00008 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion

        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-8")]
        public void TEST_00008__Read_ESS_ZonesTotal_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00008__rundata.xlsx", 0);
            runData.RunRow = 0;
            ushort zones_Total = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runData_Zones_Total = runData.RunDataValue("ESS_ZONES_TOTAL");
                    ushort registerAddress = 17;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("zones_total", runData_Zones_Total);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    zones_Total = (UInt16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runData_Zones_Total == zones_Total.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runData_Zones_Total == zones_Total.ToString(), "response Modbus message register value " + zones_Total + " is equal to the Run ID" + runData_Zones_Total + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetZonesTotal");
                            StringAssert.Contains(runData_Zones_Total.ToString(), RestGetresult);
                            string getSiteinfo = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetTable");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZONES_TOTAL-" + runData_Zones_Total + " Actual ESS_ZONES_TOTAL-" + runData_Zones_Total + "response Modbus message register value is equal to the Run ID" + zones_Total + "column value" + getSiteinfo);
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZONES_TOTAL-" + runData_Zones_Total + " Actual ESS_ZONES_TOTAL-" + runData_Zones_Total + "response Modbus message register value is not equal to the Run ID" + zones_Total + "column value");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_ZONES_TOTAL-" + runData_Zones_Total + " Actual ESS_ZONES_TOTAL-" + runData_Zones_Total + "response Modbus message register value is not equal to the Run ID" + zones_Total + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runData_Zones_Total != zones_Total.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(runData_Zones_Total != zones_Total.ToString(), "Run ID ESS_ZONES_TOTAL invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Zones_Total + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Zones_Total + " is invalid, bec Run Value should not be equal to invalid user input");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runData_Zones_Total + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }

    }
}