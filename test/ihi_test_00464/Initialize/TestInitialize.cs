﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using ihi_testlib_modbus;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ihi_test_00464
{
    [SetUpFixture]
    public abstract class TestInitialize
    {

        #region Variables 
        public static string RepositoryPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        ihi_testlib_espilot.SSHAndStartDECSDirectory sshInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDirectory);
        public static ExtentReports _extent = new ExtentReports();
        protected ExtentTest _test;
        public static ihi_testlib_selenium.Selenium webDriver = new ihi_testlib_selenium.Selenium();
        #endregion



        [OneTimeSetUp]
        protected void Setup()
        {
            var testname = GetType().Namespace;
            ihi_launch_simulator.LaunchSimulator.DomainControllerCleanUp();
            string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
            var reportDir = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            Directory.CreateDirectory(reportDir);
            var fileName = GetType().Namespace + ".html";
            var htmlReporter = new ExtentHtmlReporter(reportDir + fileName);
            htmlReporter.LoadConfig(TestContext.CurrentContext.TestDirectory + "\\extent-config.xml");
            htmlReporter.AppendExisting = true;
            _extent.AttachReporter(htmlReporter);
            //Stoping ESPilot DECS
            ihi_testlib_espilot.SSH.StopDecs(sshInfo);
            System.Threading.Thread.Sleep(5000);
            //Start ESPilot DECS
            try
            {
                var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                ihi_testlib_espilot.SSHAndStartDECSDirectory decsInfo = new ihi_testlib_espilot.SSHAndStartDECSDirectory(sshInfo, GlobalVariables.DECSDirectory);
                var startDECSSuccess = ihi_testlib_espilot.SSH.StartDecs(ihi_testlib_espilot.DECSLaunchType.Normal, decsInfo, 10000, "");
                //   ihi_testlib_espilot.SSH.SetIO_SimulatorIPAddress(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public String TakesScreenshot(ihi_testlib_selenium.Selenium driver, string FileName)
        {
            var testname = GetType().Namespace;
            string pjtParent = ConfigurationManager.AppSettings["AutomationDiretory"].ToString();
            string path = pjtParent + ConfigurationManager.AppSettings["ExtentReport"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\" + testname + "\\";
            System.IO.Directory.CreateDirectory(path);
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");
            TimeAndDate.Replace(" ", "_");
            string imageName = TestContext.CurrentContext.Test.Name + FileName + TimeAndDate.ToString();
            ((ITakesScreenshot)driver.browserDriver).GetScreenshot().SaveAsFile(path + "_" + imageName + "." + System.Drawing.Imaging.ImageFormat.Jpeg);
            return path + "_" + imageName + "." + "jpeg";
        }

        //public void SetupTyphoon()
        //{
        //    //ProcessStartInfo ProcessInfo;
        //    //Process Process;
        //    //ProcessInfo = new ProcessStartInfo("C:\\Windows\\System32\\cmd.exe");
        //    ////ProcessInfo.CreateNoWindow = true;
        //    //ProcessInfo.UseShellExecute = true;
        //    //ProcessInfo.Verb = "runas";

        //    //Process = Process.Start(ProcessInfo);
        //    ////Process p = new Process("C:\Windows\System32\cmd.exe");
        //    ////p.StartInfo.FileName = ConfigurationManager.AppSettings["CMDPath"].ToString();
        //    ////p.StartInfo.Arguments = @"/K ssh typhoon@192.168.10.210";
        //    ////p.Start();
        //    ///static void Main(string[] args)
        //    //Process cmd = new Process();
        //    //cmd.StartInfo.FileName = @"C:\Program Files\PuTTY\plink.exe";
        //    //cmd.StartInfo.UseShellExecute = false;
        //    ////cmd.StartInfo.RedirectStandardInput = true;
        //    ////cmd.StartInfo.RedirectStandardOutput = true;
        //    //cmd.StartInfo.Arguments = "-ssh typhoon@192.168.10.210 22 -pw P@ssw0rd0816";
        //    //cmd.Start();
        //    //cmd.StandardInput.WriteLine("./myscript.sh");
        //    //cmd.StandardInput.WriteLine("exit");
        //    //string output = cmd.StandardOutput.ReadToEnd();
        //    SshClient ssh = new SshClient(host: "192.168.10.210", username: "typhoon", password: "P@ssw0rd0816");
        //    ssh.Connect();
        //    ssh.RunCommand(@"cd C:\Models Under Development\Python && python alarms.py");

       // }

        [OneTimeTearDown]
        protected void TearDown()
        {
            _extent.Flush();
            //stop IIS EXPRESS 
        }

        [SetUp]
        public void BeforeTest()
        {
            _test = _extent.CreateTest(TestContext.CurrentContext.Test.Name);
            _test.AssignCategory(this.GetType().ToString());
            _test.AssignAuthor("Ramya Dubbudu");
        }

        [TearDown]
        public void AfterTest()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }
            _test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
            _extent.Flush();

        }
    }
}