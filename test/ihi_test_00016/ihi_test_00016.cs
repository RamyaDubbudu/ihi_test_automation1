using ihi_simulator_dc.Controllers;
using ihi_testlib_videorecorder;
using NUnit.Framework;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ihi_testlib_modbus;

namespace ihi_test_00016
{
    [TestFixture]
    public class ihi_test_00016 : TestInitialize
    {

        #region Variables 
       SSHAndStartDECSDirectory sshInfo = new SSHAndStartDECSDirectory(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword, GlobalVariables.DECSDatabaseDirectory);

        #endregion
        
        [Test, Retry(2), Category("ReadInputRegisters"), Property("Test", "TEST-16")]
        public void TEST_00016__Read_ESS_CURRENT_InputRegister()
        {
            RunData runData = new RunData();
            runData.RunDataSheet(RepositoryPath + "\\ihi_test_00016__rundata.xlsx", 0);
            runData.RunRow = 0;
            short ESS_current = 0;
            for (int i = runData.RunRow; i < runData.RowCount; i++)
            {
                try
                {
                    var ssh = new SshClient(GlobalVariables.SSHhostname, GlobalVariables.SSHusername, GlobalVariables.SSHpassword);
                    string runDataEss_Current = runData.RunDataValue("ESS_CURRENT");
                    ushort registerAddress = 24;
                    var scenarioType = runData.RunDataValue("Scenarios");
                    //Publish ESS_KVAR_NAMEPLATE data from DC Sim for the Run ID  ESS_KVAR_NAMEPLATE =  Run ID ESS_KVAR_NAMEPLATE
                    ConsumeDCSimRest.UpdateSiteData("current", runDataEss_Current);
                    //Send a Modbus request to the ISO server to read the input register (ESS_KVAR_NAMEPLATE)
                    ushort[] values = ModbusClient.ReadInputRegister(GlobalVariables.SSHhostname, registerAddress, 1);
                    ESS_current = (Int16)values[0];

                    //Verify response Modbus message register value is equal to the Run ID *ESS_KW_NAMEPLATE*column value
                    // If Run ID ESS_KVAR_NAMEPLATE valid:   Run id ESS_KVAR_NAMEPLATE , if Run ID ESS_KVAR_NAMEPLATE invalid:   equal to initial ESS_KVAR_NAMEPLATE

                    if (scenarioType == "Valid") //Valid Scenarios
                    {
                        if (runDataEss_Current == ESS_current.ToString()) //Valid Scenarios
                        {
                            Assert.IsTrue(runDataEss_Current == ESS_current.ToString(), "response Modbus message register value is equal to the Run ID" + runDataEss_Current + "column value");
                            string RestGetresult = ConsumeDCSimRest.GetRestResponseByName("SiteInfo", "GetCurrent");
                            StringAssert.Contains(runDataEss_Current.ToString(), RestGetresult);
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_CURRENT-" + ESS_current + " Actual ESS_CURRENT-" + runDataEss_Current + "response Modbus message register value is equal to the Run ID" + ESS_current + "column value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_CURRENT-" + ESS_current + " Actual ESS_CURRENT-" + runDataEss_Current + "response Modbus message register value is not equal to the Run ID" + ESS_current + "column value");
                           throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + " Expected ESS_CURRENT-" + ESS_current + " Actual ESS_CURRENT-" + runDataEss_Current + "response Modbus message register value is not equal to the Run ID" + ESS_current + "column value");
                        }
                    }
                    if (scenarioType == "Invalid") // invalid Scenarios
                    {
                        if (runDataEss_Current != ESS_current.ToString()) // invalid Scenarios
                        {
                            Assert.IsTrue(runDataEss_Current != ESS_current.ToString(), "Run ID ESS_CURRENT invalid: so does not equal to Run Value");
                            _test.Pass("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runDataEss_Current + " is invalid, so does not equal to Run Value");
                        }
                        else
                        {
                            _test.Fail("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runDataEss_Current + " is invalid, bec Run Value should not be equal to invalid user input");
                            throw new Exception("scenarioType- " + scenarioType + " RunID-" + runData.RunRow + "Run ID " + runDataEss_Current + " is invalid, bec Run Value should not be equal to invalid user input");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _test.Fail(ex);
                }
                finally
                {
                    runData.RunRow++;
                }
            }
            
        }
    }
}